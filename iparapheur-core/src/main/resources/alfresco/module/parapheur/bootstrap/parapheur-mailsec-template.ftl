Bonjour,
Veuillez trouver ci-joint le dossier ${document.properties['cm:title']}.

Cordialement,
${person.properties.firstName}<#if person.properties.lastName?exists> ${person.properties.lastName}</#if>
