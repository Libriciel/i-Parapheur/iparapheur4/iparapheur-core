/*
 * Version 3.2
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.bl.iparapheur;

import com.atolcd.parapheur.repo.MetadataService;
import com.atolcd.parapheur.repo.ParapheurService;
import java.util.Properties;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;
import org.quartz.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

/**
 * une collection de singletons, initialises par Spring (blex-context.xml),
 * representant l'environnement d'exécution
 */
public class EnvBeans  implements InitializingBean {

    public static interface K {
        /** nom du bean dans blex-context.xml */
        String springName="envBeans";
    }

    private Logger logger = LoggerFactory.getLogger(EnvBeans.class.getName());

    private AuthenticationComponent authenticationComponent;
    public AuthenticationComponent getAuthenticationComponent() { return authenticationComponent; }
    public void setAuthenticationComponent(AuthenticationComponent authenticationComponent) { this.authenticationComponent = authenticationComponent; }

    private Properties configuration;
    public Properties getConfiguration() { return configuration; }
    public void setConfiguration(Properties configuration) { this.configuration = configuration; }

    private ContentService contentService;
    public ContentService getContentService() { return contentService; }
    public void setContentService(ContentService contentService) { this.contentService = contentService; }

    private FileFolderService fileFolderService;
    public FileFolderService getFileFolderService() { return fileFolderService; }
    public void setFileFolderService(FileFolderService fileFolderService) { this.fileFolderService = fileFolderService; }

    private NamespaceService namespaceService;
    public NamespaceService getNamespaceService() { return namespaceService; }
    public void setNamespaceService(NamespaceService namespaceService) { this.namespaceService = namespaceService; }

    private NodeService nodeService;
    public NodeService getNodeService() { return nodeService; }
    public void setNodeService(NodeService nodeService) { this.nodeService = nodeService; }

    private ParapheurService parapheurService;
    public ParapheurService getParapheurService() { return parapheurService; }
    public void setParapheurService(ParapheurService parapheurService) { this.parapheurService = parapheurService; }

    private SearchService searchService;
    public SearchService getSearchService() { return searchService; }
    public void setSearchService(SearchService searchService) { this.searchService = searchService; }

    private TenantAdminService tenantAdminService;
    public TenantAdminService getTenantAdminService() { return tenantAdminService; }
    public void setTenantAdminService(TenantAdminService tenantAdminService) { this.tenantAdminService = tenantAdminService; }

    private TenantService tenantService;
    public TenantService getTenantService() { return tenantService;}
    public void setTenantService(TenantService tenantService) { this.tenantService = tenantService; }

    private TransactionService transactionService;
    public TransactionService getTransactionService() { return transactionService; }
    public void setTransactionService(TransactionService transactionService) { this.transactionService = transactionService; }
    
    private MetadataService metadataService;
    public MetadataService getMetadataService() { return metadataService; }
    public void setMetadataService(MetadataService metadataService) { this.metadataService = metadataService; }

    private Scheduler scheduler;
    public Scheduler getScheduler() { return scheduler; }
    public void setScheduler(Scheduler scheduler) { this.scheduler = scheduler; }

    public EnvBeans() {
        logger.info("creation instance");
    }

    /**
     * Appelle par Spring apres construction de l'objet et set de toutes
     * les proprietes
     *
     * @see InitializingBean#afterPropertiesSet()
     */
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(authenticationComponent, "authenticationComponent");
        Assert.notNull(configuration, "configuration");
        Assert.notNull(contentService, "contentService");
        Assert.notNull(fileFolderService, "fileFolderService");
        Assert.notNull(namespaceService, "namespaceService");
        Assert.notNull(nodeService, "nodeService");
        Assert.notNull(parapheurService, "parapheurService");
        Assert.notNull(searchService, "searchService");
        Assert.notNull(tenantAdminService, "tenantAdminService");
        Assert.notNull(tenantService, "tenantService");
        Assert.notNull(transactionService, "transactionService");
        Assert.notNull(metadataService, "metadataService");
        Assert.notNull(scheduler, "scheduler");
    }

}
