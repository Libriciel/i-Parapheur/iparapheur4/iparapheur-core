package fr.bl.iparapheur.xwv;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Lorsque le serveur XWV (Xemelios-Web-Viewer) n'est pas contactable, alors 
 * qu'il devrait l'être, cette classe est responsable de le contacter 
 * régulièrement jusqu'à ce qu'il réponde.
 *                                                                             <br>
 * Sitôt que le serveur aura répondu, la configuration sera mis à jour.                                                                            
 *                                                                             <br><br>
 * Cette classe n'est invoquée que si un serveur XWV a été déclaré dans 
 * la configuration du parapheur lue par {@link BlexServletContextListener},
 */
class ServerObserver {
	
	private static Log logger = LogFactory.getLog(ServerObserver.class);

	private TimerTask task;
	private Timer timer;

	private XwvConfig config;
	
	/**
	 * Création d'un observeur, pour une configuration donnée
	 * 
	 * @param config
	 *     la configuration observée, dont le statut sera mis à jour
	 *     lorsque le serveur répondra
	 */
	public ServerObserver setConfig(XwvConfig config) {
		this.config=config;
		return this;
	}

	private long scheduleRateMs=1000*30; // 30s par défaut

	/**
	 * Fixe le délai entre 2 tentatives de connexion au serveur XWV.
	 * 
	 * @param scheduleRateMs
	 *     délai en ms
	 *    
	 * @return this
	 */
	ServerObserver setScheduleRateMs(long scheduleRateMs) {
		this.scheduleRateMs=scheduleRateMs;
		return this;
	}

	/**
	 * Début des observations.
	 *                                                                         <br><br>
	 * Programme une tâche {@link XwvCheckerTask} qui "ping" le serveur 
	 * à intervales réguliers. 
	 *                                                                        
	 * @return this
	 */
	ServerObserver doStartObservations() {
		timer=new Timer();
		task=new XwvCheckerTask(this);
		timer.scheduleAtFixedRate(
				task, 
				0,             
				scheduleRateMs);
		return this;
	}

	/**
	 * La tâche qui tente de ping-er le serveur et met à jour
	 * le context XWV en conscéquence                                          
	 *                                                                         <ul><li>
	 * tente un ping                                                           </li><li>
	 * lit la conf si le serveur répond au ping                                </li><li>
	 * arrête {@link ServerObserver} sitôt que le serveur est considéré 
	 * comme utilisable                                                        </li></ul>
	 */
	private static class XwvCheckerTask extends TimerTask {

		private ServerObserver observer;
		XwvCheckerTask(ServerObserver observer) {
			this.observer=observer;
		}

		@Override
		public void run() {
			logger.debug("essai contact serveur xwv");
			observer.config.doCheckServer();
			if (observer.config.isServerUsable()) {
				observer.timer.cancel();
				return;
			}
		}


	}

}
