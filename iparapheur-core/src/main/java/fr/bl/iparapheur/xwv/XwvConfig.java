package fr.bl.iparapheur.xwv;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * La configuration courante du serveur XWV
 *                                                                             <br><br>
 */
public class XwvConfig {
	private static Log logger = LogFactory.getLog(XwvConfig.class);

	
	/**
	 * Indique si un serveur Xemelios-Web-Viewer est actuellement reputé 
	 * utilisable.
	 *                                                                         <br><br>
	 * 
	 * @return                                                                 <ul><li><b>
	 *    true                                                                 </b>
	 *    si le serveur a pu être accédé sans problèmes la 
	 *    dernière fois qu'on lui a parlé, et qu'il nous a donné une
	 *    configuration qui avait l'air de marcher.
	 *                                                                         </li><li><b>
	 *    false                                                                </b> 
	 *    si le serveur n'est pas déclaré dans la la configuration             
	 *    du parapheur, ou s'il ne peut pas être contacté, ou si la 
	 *    configuration qu'il expose n'est pas correcte (e.g. le répertoire 
	 *    partagé connu de XWV n'est pas accessible depuis le parapheur)           
	 *                                                                         </il></ul>
	 */
	public boolean isServerUsable() {
		if (serverUrl==null) return false;
		return serverUsable;
	}


	/**
	 * Construction d'un "constructeur de session de visualisation"
	 * basé sur cette configuration
	 * 
	 * @return
	 *     un utilitaire pour construire une session de visualisation
	 *     d'un flux PES
	 */
	public ViewSessionBuilder buildViewer() {
		return new ViewSessionBuilder(this);
	}
	
	private String serverUrl=null;
	File sharedDirectory=null;
	
	private boolean serverUsable=false;
	
	/**
	 * problème d'accès au serveur (pas de ping, erreur de configuration, ...)
	 * déclare le serveur comme inutilisable
	 */
	void onCheckServerFail(Exception e) {
		if (isServerUsable()) {
			// le serveur était utilisable, il ne l'est plus
			logger.warn("le serveur XWV ne repond plus ou est mal configure", e);
		} else  if (lastServerFailException==null) {
			// c'est la première fois qu'on accédait au serveur
			logger.warn(
					"Le serveur XWV ne repond pas ou est mal configure : "+getServerUrl(), 
					e);
		}
		synchronized (this) {
			this.serverUsable=false;
			lastServerFailException=e;
			sharedDirectory=null;
		}
		onServerShutdownDetection();
	}

	/**
	 * Déclare le serveur comme utilisable
	 * <br><br>
	 * side effect : interruption d'un éventuel observeur en cours
	 */
	void onCheckServerSucces() {
		// server (re)starts !
		if (! isServerUsable()) {
			logger.info("Le serveur XWV est utilisable : sharedPath="+sharedDirectory);
		}
		synchronized (this) {
			if (runningServerObserver!=null) {
				runningServerObserver=null;
			}
			this.serverUsable=true;
		}
	}

	/** le dernière exception qui fait que le serveur est réputé inutilisable */
	Exception lastServerFailException=null;

	
	/**
	 * Contruction d'une configuration
	 *  
	 * @param url
	 *     L'URL permettant de contacter le serveur                            <br>
	 *     La valeur null est légale : elle signifie qu'aucun serveur
	 *     n'est déclaré.
	 */
	public XwvConfig(String serverUrl)  {
		this.serverUrl=serverUrl;
	}

	/**
	 * Donne le nom de l'url auprès de laquelle on contacte le serveur
	 * 
	 * @return 
	 *     une url, par exemple : "http://172.16.113.117/bl-xemwebviewer"      <br>
	 *     null, si aucun serveur n'est configuré
	 */
	public String getServerUrl() {
		return serverUrl;
	}


	/**
	 * Donne le nom du répertoire partagé dans lequel on dépose les flux
	 * PES à visualiser et le fichier de paramétrage attendu par le serveur.
	 * 
	 * @return 
	 *     un répertoire, par exemple : 
	 *     /etc/bl-xemwebviewer/xwv-shared
	 * 
	 */
	public File getSharedDirectory() {
		return sharedDirectory;
	}

	/**
	 * Vérification de l'état du serveur :                                     <ul><li>
	 * contact du serveur (ping)                                               </li><li>
	 * lecture de la configuration exposée par le serveur                      </li><li>
	 * mise à jour de la configuration (this)                                  </li></ul>
	 */
	public void doCheckServer() {
		if (getServerUrl()==null) return; // aucun serveur déclaré
		
		new ServerChecker()
		.setConfig(this)
		.doCheck();
		
	}
	
	ServerObserver runningServerObserver=null;
	
	/**
	 * Une classe utilisatrice du service déclare que le serveur vient très 
	 * probablement de se planter, ou bien il n'a jamais 
	 * démarré (alors que la paramétrage du parapheur indique qu'il existe)
	 *                                                                         <br><br>
	 * Cette, méthode va programmer une observation régulière de l'état
	 * du serveur, avec mise à jour de la configuration.
	 */
	public void onServerShutdownDetection() {
		if (getServerUrl()==null) return; // aucun serveur déclaré

		synchronized (this) {
			if (runningServerObserver!=null) return;
		}
		
		logger.warn("Le serveur XWV est probablement non joignable. Observation en cours sur "+getServerUrl());
		synchronized (this) {
			runningServerObserver = new ServerObserver()
			.setScheduleRateMs(30*1000)
			.setConfig(this)
			.doStartObservations();
		}
	}

	public String toString() {
		return "{ "
		+"serverUrl="+this.serverUrl
		+" }";
	}
}
