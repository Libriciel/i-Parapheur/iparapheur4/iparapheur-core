package fr.bl.iparapheur.xwv;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Un utilitaire pour construire une session de consultation d'un
 * flux PES pour XWV (Xemelios-Web-Viewer).
 *                                                                             <br><br>
 * Utilisation typique : 
<pre>
String redirectUrl=BlexContext.getInstance().getXwvConfig()
  .buildViewer()           //  construction
  .setDatasStream(...)     // parametrage
  .doPrepare()             // preparation 
  .getRedirectUrl();       // recuperation URL de redirection

// redirection effective
FacesContext.getCurrentInstance().getExternalContext()
  .redirect(redirectUrl);
</pre>
 *
 */
public class ViewSessionBuilder {
	private static Log logger = LogFactory.getLog(ViewSessionBuilder.class);

	/**
	 * Construction d'un constructeur de session pour un contexte particulier <br>
	 * voir {@link XwvConfig#buildViewer()}
	 * 
	 * @param context 
	 *    le contexte d'accès au serveur XWV
	 */
	ViewSessionBuilder(XwvConfig context) {
		this.context=context;
	}
	private XwvConfig context;


	/**
	 * Déclaration des données à visualiser
	 * 
	 * @param datas
	 *     un flux de données représentant le fichier PES xml
	 *     
	 * @return this
	 */
	public ViewSessionBuilder setDatasStream(InputStream datas) {
		this.datas=datas;
		return this;
	}
	private InputStream datas;


	private String redirectUrl;
	/**
	 * Retourne l'URL sur laquelle il faut positionner la naviagateur
	 * de l'utilisateur, afin de commencer à naviguer dans le flux
	 * 
	 * @return
	 *     une URL pointant vers XWV
	 */
	public String getRedirectUrl() {
		return redirectUrl;
	}

	/**
	 * Préparation effective de la session de consultation
	 *                                                                         <br><br>
	 * @return this
	 * 
	 * @throws Exception
	 *     si, pour une raison ou une autre, il est impossible de préparer
	 *     la session
	 */
	public ViewSessionBuilder doPrepare() throws Exception {
		try {

			// check paramters and context
			{
				if (context==null) throw new IllegalStateException("parametre non renseigne : context");
				if (datas==null)  throw new IllegalStateException("parametre non renseigne : datas");
				if (! context.isServerUsable()) {
					throw new IllegalStateException("Le serveur XWV est actuellement inutilisable");
				}
			}

			// store pes file in shared folder
			String targetFileName;
			{
				targetFileName=UUID.randomUUID().toString()+".xml";
				File file=new File(context.sharedDirectory, targetFileName);

				logger.debug("ecriture PES sur "+file.getCanonicalPath());

				OutputStream out;
				out = new FileOutputStream(file);
				byte[] buffer = new byte[1024];
				InputStream datas=this.datas;
				int len = datas.read(buffer);
				while (len != -1) {
					out.write(buffer, 0, len);
					len = datas.read(buffer);
				}
				datas.close();
				out.close();
			}

			// contact server and prepare session 
			HttpMethod call;
			String serverResponse;
			try {

				logger.debug("ecriture PES ok");

				Properties ppt=new Properties();
				ppt.setProperty("pesDocumentPath", targetFileName);
				File pptFile=new File(context.sharedDirectory, UUID.randomUUID().toString()+".properties");

				logger.debug("ecriture PROPERTIES : "+pptFile);

				ppt.store(new FileOutputStream(pptFile),null);

				String url=context.getServerUrl()+"/prepare";

				logger.debug("get : "+url);

				HttpClient client = new HttpClient();
				call = new GetMethod(url);

				String query="propertiesDocumentPath"+"="+pptFile.getName();
				logger.debug("query : "+query);
				call.setQueryString(query);

				client.executeMethod(call);
				serverResponse=call.getResponseBodyAsString();
				call.releaseConnection();

				// if not OK and not SYNTAX ERROR, it's probably a server shutdown 
				if (call.getStatusCode()!=HttpServletResponse.SC_OK
						&& call.getStatusCode()!=HttpServletResponse.SC_BAD_REQUEST) {
					throw new Exception("Erreur retournee par le serveur XWV : "+call.getStatusCode()+"  : ");
				}
				
			} catch (Exception e) {
				// if we have an error here, it's PROBABLY because the server is
				// actually down
				context.onServerShutdownDetection();
				throw e;
			} 

			// parse and extract redirect URL
			{
				logger.debug("status   : "+call.getStatusCode());

				if (call.getStatusCode()!=HttpServletResponse.SC_OK) {
					throw new Exception("Erreur retournee par le serveur XWV : "+call.getStatusCode()+"  : ");
				}

				logger.debug("response : "+serverResponse);
				// eg : {"redirectUrl":"http://127.0.0.1:9080/bl-xemwebviewer/view?xwvSession=453f18a0","exception":null}

				JSONTokener tokener=new JSONTokener(serverResponse);
				JSONObject jsonObject=new JSONObject(tokener);
				Object redirectUrl=jsonObject.get("redirectUrl");

				logger.debug("redirectUrl = "+redirectUrl);

				if (redirectUrl==null) {
					throw new Exception("impossible de retrouver l'url de redirection: "+call.getStatusCode()+"  : ");		
				}

				this.redirectUrl=redirectUrl.toString();
			}

		} catch (Exception e) {
			throw e;
		}
		return this;
	}

}
