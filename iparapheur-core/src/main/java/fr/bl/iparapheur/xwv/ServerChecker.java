package fr.bl.iparapheur.xwv;

import java.io.File;
import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Vérification de la connexion au serveur XWV (Xemelios-Web-Viewer)
 *                                                                             <br><br>
 * Cette classe utilitaire est responsable de la vérification de la connexion 
 * au serveur XWV et de la configuration qu'il expose.
 *                                                                             <br>
 * La vérification de la configuration consiste par exemple à s'assurer
 * que le répertoire partagé connu du serveur XWV est accessible en lecture 
 * depuis ici.
 *                                                                             <br><br>
 * L'appel à {@link #doCheck()} provoque un appel au serveur XWV et la 
 * vérification. 
 * La configuration courante est mise à jour selon le résultat :
 * {@link XwvConfig#onCheckServerSucces()} 
 * {@link XwvConfig#onCheckServerFail(Exception)}                                                                             
 */
class ServerChecker {
	private static Log logger = LogFactory.getLog(ServerChecker.class);

	/**
	 * Déclaration de la configuration à vérifier
	 * 
	 * @param config
	 *   une configuration XWV
	 *   
	 * @return
	 *   this
	 */
	ServerChecker setConfig(XwvConfig config) {
		this.config=config;
		return this;
	}

	/** la configuration XWV que l'on vérifie */
	private XwvConfig config;


	/**
	 * Vérification de l'état du serveur, et mise à jour de la configuration
	 * en conscéquence
	 */
	void doCheck() {
		if (config==null) throw new IllegalStateException("parametre config non declare");

		try {

			// ping
			{
				String url=config.getServerUrl()+"/ping";
				logger.debug("get : "+url);

				HttpClient client = new HttpClient();
				HttpMethod call = new GetMethod(url);
				client.executeMethod(call);
				call.getResponseBody();
				call.releaseConnection();

				logger.debug("resp. status : "+call.getStatusCode());
				if (call.getStatusCode()!=200) {
					throw new Exception("erreur ping : status : "+call.getStatusCode());
				}

				String response=call.getResponseBodyAsString();
				// pong

				logger.debug("resp. text : "+response);
				if (response==null || !response.startsWith("pong")) {
					throw new Exception("erreur ping : reponse non attendue : "+response);
				}
			}


			if (config.sharedDirectory==null) {
				// readConf

				String url=config.getServerUrl()+"/conf";

				logger.debug("get : "+url);

				HttpClient client = new HttpClient();
				HttpMethod call = new GetMethod(url);
				client.executeMethod(call);
				call.getResponseBody();
				call.releaseConnection();

				logger.debug("resp. status : "+call.getStatusCode());
				if (call.getStatusCode()!=200) {
					throw new Exception("erreur conf : status : "+call.getStatusCode());
				}

				String response=call.getResponseBodyAsString();
				// {"xemelios":{...},"context":{...,"sharedPath":"/etc/bl-xemwebviewer/xwv-shared",...}}
				logger.debug("resp. text : "+response);

				JSONTokener tokener=new JSONTokener(response);
				JSONObject jsonObject=new JSONObject(tokener);

				Object jsonContext=jsonObject.get("context");
				if (jsonContext==null || !(jsonContext instanceof JSONObject)) {
					throw new Exception("conf incorecte : manque 'context'");
				}

				Object jsonSharedPath=((JSONObject) jsonContext).get("sharedPath");
				if (jsonSharedPath==null || !(jsonSharedPath instanceof String)) {
					throw new Exception("conf incorecte : manque 'sharedPath'");
				}
				logger.debug("sharedPath = "+jsonSharedPath);

				File sharedDirectory=new File(jsonSharedPath.toString());

				if (!sharedDirectory.exists()) {
					throw new Exception("conf incorecte : 'sharedPath' innexitant : "+sharedDirectory);
				}

				if (!sharedDirectory.isDirectory()) {
					throw new Exception("conf incorecte : 'sharedPath' n'indique pas un repertoire : "+sharedDirectory);
				}

				try { 
					File.createTempFile("ipara-check-write-access-", ".tmp", sharedDirectory);
				} catch (Exception e) {
					throw new Exception("conf incorecte : impossible d'ecrire dans "+sharedDirectory, e);
				}

				config.sharedDirectory=sharedDirectory;
			}

			config.onCheckServerSucces();

		} catch (HttpException e) {
			config.onCheckServerFail(new Exception("erreur ping : impossible de contacter le serveur", e));
		} catch (IOException e) {
			config.onCheckServerFail(new Exception("erreur ping : impossible de contacter le serveur", e));
		} catch (Exception e) {
			config.onCheckServerFail(e);
		}

	}
	

}
