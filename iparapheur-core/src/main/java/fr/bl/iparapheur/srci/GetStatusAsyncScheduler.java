package fr.bl.iparapheur.srci;

import fr.bl.iparapheur.rcc.IParaCommandRegistry;
import fr.bl.iparapheur.EnvBeans;
import com.atolcd.parapheur.repo.ParapheurService;
import fr.bl.gbox.bean.BBean;
import fr.bl.gbox.rcc.RccMessage;
import fr.bl.iparapheur.web.BlexContext;
import fr.bl.tdtproxysrci.TPS;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.camel.CamelContext;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

/**
 * Planificateur d'interrogation du statut d'un dossier sur le TDT
 */
class GetStatusAsyncScheduler {

    private Logger logger = LoggerFactory.getLogger(GetStatusAsyncScheduler.class.getName());

    NodeRef dossierRef;
    public GetStatusAsyncScheduler setNodeRef(NodeRef nodeRef) {
        this.dossierRef = nodeRef;
        return this;
    }

    EnvBeans envBeans;
    public GetStatusAsyncScheduler setEnvBeans(EnvBeans srciBeans) {
        this.envBeans = srciBeans;
        return this;
    }

    CamelContext camelContext;
    public GetStatusAsyncScheduler setCamelContext(CamelContext camelContext) {
        this.camelContext = camelContext;
        return this;
    }

    private static interface K {
        String jobGroup="SRCI_MESSAGE_STATUS";
    }

    /**
     * Lancement d'une tache recurente d'interrogation aynchrone de l'avancement
     * d'un dossier sur le TDT (proxy)
     */
    void schedule() throws Exception {

        SrciConfig srciConfig=BlexContext.getInstance().getSrciConfig();

        String jobName=dossierRef.getId();
        String jobGroup=K.jobGroup;

        // TODO : voir comment reduire la periodicite si "ca n'avance pas assez vite"
        
        ParapheurService parapheurService=envBeans.getParapheurService();
        Scheduler scheduler=envBeans.getScheduler();
        NodeService nodeService=envBeans.getNodeService();

        long timeUnit = 1000*srciConfig.getScheduleStatusUnitSec();

        // relance toutes les 30 timeUnit
        long maxRepeatInterval = 30 * timeUnit;

        // delai de lancement au hasard entre dans 1 timeUnit et 30 timeUnit
        long delay = (long) (Math.random() * maxRepeatInterval) + timeUnit;

        String docuName=nodeService.getProperty(dossierRef, ContentModel.PROP_NAME).toString();

        if (logger.isInfoEnabled()) {
            int id = (int) (delay / timeUnit);
            int ir = (int) (maxRepeatInterval / timeUnit);
            logger.info(
                    "planification etat dossier SRCI : "
                    + "" + dossierRef.getId() + " [" + docuName + "]"
                    + ", delay : " + id + " tu"
                    + ", repeat : " + ir + " tu"
                    + ", tu : " + srciConfig.getScheduleStatusUnitSec() + " s"
                    );
        }

        // Trigger: Execute in X minutes, and repeat every 'repeatInterval' minutes
        Trigger trigger = new SimpleTrigger(
                jobName, jobGroup,
                new Date(System.currentTimeMillis() + delay),
                null,
                SimpleTrigger.REPEAT_INDEFINITELY,
                maxRepeatInterval);

        JobDetail jobDetails = new JobDetail(
                jobName, jobGroup,
                GetStatusAsyncJob.class);

        Map<String, String> ppt  = new TenantSrciProperties()
                .setSrciBeans(envBeans)
                .getPropertiesHelios();

        String tdtDocName;
        {
            Object value=nodeService.getProperty(dossierRef, SrciService.K.property_srciTransaction_documentName);
            Assert.notNull(value, "interrogation etat ["+docuName+"] : propriete manquante : "+SrciService.K.property_srciTransaction_documentName);
            tdtDocName=value.toString();
        }

        String userLogin=parapheurService.getActeurCourantBLSRCI(dossierRef);

        // preparation du message a envoyer au proxy TDT pour interrogation statut
        RccMessage rccMessage=new RccMessage()
            .setPayload(
                TPS.GetStatus.name,
                new BBean()
                    .set(TPS.GetStatus.in.account, ppt.get(TenantSrciProperties.K.helios.proxy_account) )
                    .set(TPS.GetStatus.in.docName, tdtDocName)
                    .set(TPS.GetStatus.in.replyToUri, srciConfig.getInboxFolderUri()))
            .setCallback(
                SrciConfig.K.registry.onPesStatusNotification,
                srciConfig.getInboxFolderUri(),
                new BBean()
                    .set(OnPesStatusNotification.K.in.nodeRef,   dossierRef.toString())
                    .set(OnPesStatusNotification.K.in.userLogin, userLogin));

        BBean par=new BBean().setDescription("parametres lancement interrogation statut")
            .set(GetStatusAsyncJob.K.rccMessage, rccMessage)
            .set(GetStatusAsyncJob.K.logMessage, "interrogation etat ["+docuName+"]")
            .set(GetStatusAsyncJob.K.camelContext, camelContext)
            .set(GetStatusAsyncJob.K.nodeRef, dossierRef)
            .set(GetStatusAsyncJob.K.envBeans, envBeans)
            .set(GetStatusAsyncJob.K.userLogin, userLogin)
            .set(GetStatusAsyncJob.K.jobName, jobName)
            .set(GetStatusAsyncJob.K.jobGroup, jobGroup)
        ;

        Map<String, Object> jobDataMap = new HashMap<String, Object>();
        jobDataMap.put("parameters", par);
        jobDetails.setJobDataMap(new JobDataMap(jobDataMap));

        {
            if (scheduler.getJobDetail(jobName, jobGroup) != null) {
                // possible dans le cas ou le dossier est supprime puis recree sous le meme nom
                // entre 2 schedulings (!)
                logger.warn("suppression du job redondant [{}-{}]", jobName, jobGroup);
                scheduler.unscheduleJob(jobName, jobGroup);
            }
        }

        scheduler.scheduleJob(jobDetails, trigger);
    }


    static class Checker {
        
    }


}
