package fr.bl.iparapheur.srci;

import fr.bl.gbox.command.CommandRegistry;
import fr.bl.iparapheur.rcc.RccConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * La configuration courante du server-proxy SRCI
 */
public class SrciConfig extends RccConfig {

    public static interface K {
        public static String directProxyUri="direct:tdtproxysrci";
        public static interface registry {
            public static String onPesStatusNotification="onPesStatusNotification";
        }
    }

    @Override
    public String getServerInboxUri() {
        return K.directProxyUri;
    }

    
    private Logger logger = LoggerFactory.getLogger(SrciConfig.class.getName());

    int scheduleStatusUnitSec=60; // 1 mn

    /**
     * La frequence a laquelle la tache planifiee recurente qui s'enquiert
     * de l'etat d'un dossier est declenchee
     *
     * @return une unite de temps, en secondes
     */
    public int getScheduleStatusUnitSec() {
        return scheduleStatusUnitSec;
    }

    public void setScheduleStatusUnitSec(int scheduleStatusUnit) {
        this.scheduleStatusUnitSec = scheduleStatusUnit;
    }

    @Override
    public void populateCommandRegistry(CommandRegistry commandRegistry) {
        commandRegistry.put(K.registry.onPesStatusNotification, OnPesStatusNotification.class);
    }


}
