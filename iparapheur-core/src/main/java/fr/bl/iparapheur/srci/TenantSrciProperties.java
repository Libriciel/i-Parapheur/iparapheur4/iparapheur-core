package fr.bl.iparapheur.srci;

import fr.bl.iparapheur.EnvBeans;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.UserTransaction;

import org.alfresco.error.AlfrescoRuntimeException;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Acces aux proprietes SRCI du tenant, stockees dans un fichier XML
 */
public class TenantSrciProperties {

    /** le fichier XML de configuration SRCI */
    public static interface K {

        /** l'emplacement ou sont stockes les fichiers de confguration du tenant  pour BLEX*/
        public static String blexConfigPath = "/app:company_home/app:dictionary/ph:blex";

        /**
         * le nom du fichier de configuration SRCI, comme un noeud
         * tel que declare dans blex-bootstrap.xml : "(...)/cm:srci_configuration"
         */
        public static String configNodePath = blexConfigPath + "/cm:srci_configuration";

        /** le nom du fichier de configuration SRCI, comme un noeud  : "srci_configuration.xml" */
        public static String configFilename = "srci_configuration.xml";

        /** xpath vers le parametrage HELIOS  dans le fichier XML */
        public String xpath_helios = "/srci/helios";

        /** xpath vers le parametrage ACTES dans le fichier XML */
        public String xpath_actes = "/srci/actes";

        /** parametrage HELIOS */
        public static interface helios {

            /** nom du compte SRCI/PES sur le proxy */
            public String proxy_account = "proxy_account";

            /** les proprietes ayant a avoir avec le parametrage de la signature */
            public String[] sign_properties  = new String [] {
                "pPolicyIdentifierID",
                "pPolicyIdentifierDescription",
                "pPolicyDigest",
                "pSPURI",
                "pCity",
                "pPostalCode",
                "pCountryName",
                "pClaimedRole"
            };

        }
    }


    private Logger logger = LoggerFactory.getLogger(TenantSrciProperties.class.getName());

    private EnvBeans srciBeans;
    public TenantSrciProperties setSrciBeans(EnvBeans srciBeans) {
        this.srciBeans = srciBeans;
        return this;
    }

    /**
     * lecture d'un fichier dans l'espace de configuration du tenant
     *
     * @param nameFile le nom du fichier a recuperer
     * @return un flux ouvert sur ce fichier
     * @throws Exception
     */
    private InputStream readFile(String nameFile) throws Exception {

        UserTransaction trx = srciBeans.getTransactionService().getUserTransaction();
        InputStream inputStream = null;
        try {
            trx.begin();

            // Recherche du noeud Certificats dans Dictionary
            String xpath = K.blexConfigPath;
            ResultSet result = srciBeans.getSearchService().query(
                    new StoreRef(
                        this.srciBeans.getConfiguration().getProperty("spaces.store")),
                        SearchService.LANGUAGE_XPATH,
                        xpath);

            if (result.length() > 0) {
                NodeRef nodeCertificat = result.getNodeRef(0);
                List<ChildAssociationRef> childs = srciBeans.getNodeService().getChildAssocs(nodeCertificat);
                NodeRef file = null;

                // Récupération des fichiers
                for (ChildAssociationRef child : childs) {
                    String name = srciBeans.getNodeService().getProperty(child.getChildRef(), ContentModel.PROP_NAME).toString();
                    if (name.equals(nameFile)) {
                        // Sélection du fichier demandé
                        file = child.getChildRef();
                    }
                }
                logger.debug("file={}",file);
                if (file != null) {
                    ContentReader content = srciBeans.getFileFolderService().getReader(file);
                    inputStream = content.getContentInputStream();
                }
            }
            trx.commit();
            return inputStream;
        } catch (Exception e) {
            try {
                trx.rollback();
            } catch (Exception ex) {
                logger.error("erreur au rollback", ex);
            }
            throw e;
        }
    }

    /**
     * Recuperation des proprietes liees au tenant, relatives a la configuration HELIOS
     */
    Map<String, String> getPropertiesHelios() throws Exception {
        try {
            Map<String, String> props = new HashMap<String, String>();
            SAXReader reader = new SAXReader();
            Document document = reader.read(readFile(K.configFilename));

            // recuperation de toutes les proprietes
            // List<Node> nodes = document.selectNodes(K.config.xpath_helios);
            // .elements()
            List<Node> nodes = ((Element)document.selectNodes(K.xpath_helios).get(0)).elements();
            for (Node node : nodes) {
                props.put(node.getName(), node.getText());
            }
            logger.debug("props={}", props);
            return props;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new AlfrescoRuntimeException(ex.getMessage(), ex);
        }
    }



}
