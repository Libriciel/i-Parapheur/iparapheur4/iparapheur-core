package fr.bl.iparapheur.srci;

import fr.bl.iparapheur.rcc.IParaCommand;
import com.atolcd.parapheur.repo.EtapeCircuit;
import fr.bl.gbox.bean.*;
import fr.bl.gbox.rcc.RccMessage;
import fr.bl.tdtproxysrci.TPS;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import javax.transaction.UserTransaction;
import org.adullact.iparapheur.status.StatusMetier;

/**
 * Notification de l'avancement d'un dossier sur le TDT proxy
 *
 * Cette command est exécutéee par un appel depuis TDT proxy, en réponse
 * à une demande du parapheur.
 */
public class OnPesStatusNotification extends IParaCommand {

    public static interface K {
        public static interface in extends TPS.GetStatus.out {
            /** version textuelle de la reference vers le dossier traite */
            public String nodeRef="nodeRef";

            /** le login de l'utilisateur sous l'identite duquel le dossier est traite */
            public String userLogin="userLogin";

            /**
             * Boolean indiquant s'il s'agit d'une notification reçue suite
             * a la demande d'envoi d'un dossier sur le TDT.
             * facultative - default to false
             */
            public String isResponseToSendCommand="isResponseToSendCommand";
        }
    }

    String nodeRefString;
    String userLogin;

    @Override
    protected StringList doBuildMandatoryParams() {
        return super.doBuildMandatoryParams()
                .add(K.in.nodeRef)
                .add(K.in.userLogin);
    }

    @Override
    protected void doCheckParameters() throws Exception {
        super.doCheckParameters();

        nodeRefString=par().get(K.in.nodeRef, String.class);

        if (! NodeRef.isNodeRef(nodeRefString)) {
            throw new BeanException(
                    par(),
                    K.in.nodeRef,
                    "n'est pas un nodeRef : ["+nodeRefString+"]");
        }


        userLogin=par().get(K.in.userLogin, String.class);

    }


    @Override
    protected void doExecute() throws Exception {
        if (! par().isEmpty(RccMessage.K.rccExtraParams.error)) {
            // erreur "grave" notifiée par le proxy - par exemple  s'il n'arrive
            // pas à contacter le TDT

            if (!par().isEmpty(K.in.isResponseToSendCommand) && par().get(K.in.isResponseToSendCommand, Boolean.class)) {
                // On vient juste d'envoyer le dossier au TDT :  il faut le considerer
                // comme non envoye au TDT :  on simule un statut "TPS.Status.error"
                String message="erreur cote serveur lors de l'envoi : "+par().get(RccMessage.K.rccExtraParams.error);

                getLogger().warn(message);

                par().set(K.in.message, message);
                par().set(K.in.status, TPS.Status.error);

            } else {
                // Dans les autres cas, l'erreur n'apporte pas d'information sur le
                // statut du dossier. Donc, on trace et va plan.
            getLogger().error(
                    "erreur cote serveur : "+par().get(RccMessage.K.rccExtraParams.error));
            return;
        }
        }

        // le proxy doit nous notifier un message et un statut
        par().assertNotEmpty(K.in.message, K.in.status);

        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {
            @Override
            public Object doWork() throws Exception {
                UserTransaction utx = envBeans.getTransactionService().getUserTransaction();
                    try {
                        utx.begin();
                        OnPesStatusNotification.this.doWorkUT();
                        utx.commit();
                    } catch (Exception ex) {
                        try { utx.rollback(); } catch (Exception rollbackEx) { getLogger().warn("rollback", rollbackEx); }
                        throw ex;
                    }
                    return null;
            }
        },
        userLogin);
        // parapheurService.auditTransmissionTDT(dossier, StatusMetier.STATUS_EN_COURS_TRANSMISSION, "Dossier en suivi");
    }

    /**
     * travail effectif enrobe par une transaction utilisateur
     *
     * voir S2lowServiceImpl.getInfosS2low()
     *
     * @throws Exception
     */
    private void doWorkUT() throws Exception {

        NodeRef nodeRef=new NodeRef(nodeRefString);

        if (! envBeans.getParapheurService().isDossier(nodeRef)) {
            getLogger().warn("le noeud ne designe pas un dossier existant : {}", nodeRef);
            return;
        }

        String statusCode=par().get(K.in.status, String.class);
        String message=par().get(K.in.message, String.class);

        if (TPS.Status.sentToTdt.equals(statusCode)) {
            // ce statut n'est envoye qu'une fois (en reponse a
            // l'envoi initial du dossier - il peut donc etre
            // trace sans risque de doublons)
            envBeans.getParapheurService().auditTransmissionTDT(nodeRef, StatusMetier.STATUS_EN_COURS_TRANSMISSION, "Dossier en cours");

        } else if (TPS.Status.notHandledByTdt.equals(statusCode)) {
            // ce statut est renoye potentiellement plusieurs fois : on ne trace rien

        } else if (TPS.Status.handledByTdt.equals(statusCode)) {
            // ce statut correspond a deroulement normal : on ne trace rien

        } else if (TPS.Status.sentToTarget.equals(statusCode)) {
            // ce statut correspond a deroulement normal : on ne trace rien

        } else if (TPS.Status.error.equals(statusCode)) {
            // en cas de rejet, annotation publique obligatoire
            envBeans.getParapheurService().setAnnotationPublique(nodeRef, "Dossier en erreur sur le TDT - message : " + message);
            envBeans.getParapheurService().reject(nodeRef);
            envBeans.getParapheurService().auditTransmissionTDT(
                    nodeRef,
                    StatusMetier.STATUS_TRANSMISSION_KO,
                    "Dossier en erreur sur le TDT - message : "+message);

        } else if (TPS.Status.canceled.equals(statusCode)) {
            // en cas de rejet, annotation publique obligatoire
            envBeans.getParapheurService().setAnnotationPublique(nodeRef, "Dossier annule sur le TDT - message : " + message);
            envBeans.getParapheurService().reject(nodeRef);
            envBeans.getParapheurService().auditTransmissionTDT(
                    nodeRef,
                    StatusMetier.STATUS_TRANSMISSION_KO,
                    "Dossier annule sur le TDT - message : "+message);

        } else if (TPS.Status.ackTechReceived.equals(statusCode)) {
            // ce statut correspond a deroulement normal : on ne trace rien

        } else if (TPS.Status.nackTechReceived.equals(statusCode)) {
            envBeans.getParapheurService().auditTransmissionTDT(
                    nodeRef, 
                    StatusMetier.STATUS_TRANSMISSION_KO,
                    "Dossier en erreur sur le TDT - NACK technique");
            // en cas de rejet, annotation publique obligatoire
            envBeans.getParapheurService().setAnnotationPublique(nodeRef, "Dossier en erreur sur le TDT - NACK technique");
            envBeans.getParapheurService().reject(nodeRef);

        } else if (TPS.Status.ackFuncReceived.equals(statusCode)) {
            // Obtenir et stocker le fichier retour Helios        
            String retDocAckRef = par().get(TPS.GetStatus.out.docAckRef, String.class);
            if (retDocAckRef != null && !retDocAckRef.isEmpty()) {
                SrciService.downloadHeliosAckNack(nodeRef, envBeans, retDocAckRef);
            }
            // je fais comme pour S*LOW, mais sans trop comprendre comment
            // on pourrait etre a l'etape d'archivage 
            if (!envBeans.getParapheurService().getCurrentEtapeCircuit(nodeRef).getActionDemandee()
                    .equals(EtapeCircuit.ETAPE_ARCHIVAGE)) {
                envBeans.getParapheurService().approve(nodeRef);
            }

        } else if (TPS.Status.nackFuncReceived.equals(statusCode)) {
            // Obtenir et stocker le fichier retour Helios        
            String retDocAckRef = par().get(TPS.GetStatus.out.docAckRef, String.class);
            if (retDocAckRef != null && !retDocAckRef.isEmpty()) {
                SrciService.downloadHeliosAckNack(nodeRef, envBeans, retDocAckRef);
            }
            envBeans.getParapheurService().auditTransmissionTDT(
                    nodeRef,
                    StatusMetier.STATUS_TRANSMISSION_KO,
                    "Dossier en erreur sur le TDT - NACK fonctionnel");
            // en cas de rejet, annotation publique obligatoire
            envBeans.getParapheurService().setAnnotationPublique(nodeRef, "Dossier en erreur sur le TDT - NACK fonctionnel");
            envBeans.getParapheurService().reject(nodeRef);
        } else {
            getLogger().warn("statut non reconnu : "+statusCode);
        }

        // mise a jour du statut en base
        envBeans.getNodeService().setProperty(nodeRef,
                SrciService.K.property_srciTransaction_status,
                statusCode);
        /*
         * surtout pas : donne automatiquement "l'aspect" S*LOW
        envBeans.getNodeService().setProperty(nodeRef,
                ParapheurModel.PROP_STATUS,
                SrciService.localizeStatus(statusCode));
        */

    }



}
