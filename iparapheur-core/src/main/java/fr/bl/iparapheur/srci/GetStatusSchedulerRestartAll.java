package fr.bl.iparapheur.srci;

import fr.bl.iparapheur.EnvBeans;
import com.atolcd.parapheur.repo.EtapeCircuit;

import java.util.ArrayList;


import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.repo.tenant.Tenant;

import org.quartz.JobExecutionException;



import com.atolcd.parapheur.repo.ParapheurService;

import java.util.List;

import javax.transaction.UserTransaction;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;
import org.apache.camel.CamelContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * replanifie toutes les taches d'interrogation du statut des dossiers sur
 * le TDT
 * lancee au demarrage de l'application
 */
public class GetStatusSchedulerRestartAll  {

    private Logger logger = LoggerFactory.getLogger(GetStatusSchedulerRestartAll.class.getName());

    EnvBeans envBeans;
    public GetStatusSchedulerRestartAll setSrciBeans(EnvBeans srciBeans) {
        this.envBeans = srciBeans;
        return this;
    }

    CamelContext camelContext;
    public GetStatusSchedulerRestartAll setCamelContext(CamelContext camelContext) {
        this.camelContext = camelContext;
        return this;
    }

    public void execute() throws JobExecutionException {

        List<Tenant> tenants = envBeans.getTenantAdminService().getAllTenants();
        List<String> admins = new ArrayList<String>();
        admins.add("admin"); // Execution in root tenant
        for (Tenant tenant : tenants) {
            if (tenant.isEnabled()) {
                admins.add(
                        envBeans.getTenantService().getDomainUser("admin", tenant.getTenantDomain()));
            }
        }

        final TransactionService transactionService = envBeans.getTransactionService();
        final SearchService searchService = envBeans.getSearchService();
        final NodeService nodeService = envBeans.getNodeService();
        final NamespaceService namespaceService = envBeans.getNamespaceService();
        final ParapheurService parapheurService = envBeans.getParapheurService();

        for (String admin : admins) {

            final String userLogin=admin;

            AuthenticationUtil.runAs(new RunAsWork() {

                public Object doWork() throws Exception {
                    UserTransaction utx = transactionService.getUserTransaction();
                    try {
                        utx.begin();

                        String dossiersXPath = "//*[subtypeOf('ph:dossier')]";
                        List<NodeRef> dossiers = searchService.selectNodes(
                                nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                                dossiersXPath,
                                null,
                                namespaceService,
                                false);

                        boolean first=true;
                        for (NodeRef dossier : dossiers) {
                            EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossier);

                            if (etape != null
                                    && etape.getActionDemandee().equals(EtapeCircuit.ETAPE_TDT)
                                    && nodeService.hasAspect(dossier, SrciService.K.aspect_srciTransaction)) {

                                boolean schedulable=GetStatusAsyncJob.isSchedulingRelevant(envBeans, dossier);

                                if (schedulable) {
                                    if (first) {
                                        logger.info("planification interrogation dossiers SRCI pour {}", userLogin);
                                        first=false;
                                    }

                                    try {
                                        new GetStatusAsyncScheduler()
                                                .setEnvBeans(envBeans)
                                                .setCamelContext(camelContext)
                                                .setNodeRef(dossier)
                                                .schedule();
                                    } catch (Exception any) {
                                        // en cas d'erreur, on loggue, et on continue a
                                        // tacher de planifier les autres ...
                                        logger.warn("erreur de planification", any);
                                    }

                                }
                            }
                        }

                        utx.commit();
                    } catch (Exception e) {
                        try { utx.rollback(); } catch (Exception roll) { logger.error("", roll); }
                        throw new JobExecutionException(e);
                    }

                    return null;
                }
            },
            admin);
        }

    }
}
