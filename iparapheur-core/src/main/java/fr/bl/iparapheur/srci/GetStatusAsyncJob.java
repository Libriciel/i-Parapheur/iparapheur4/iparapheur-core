package fr.bl.iparapheur.srci;

import fr.bl.gbox.rcc.camel.Sender;
import org.apache.camel.CamelContext;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import fr.bl.gbox.bean.*;
import fr.bl.gbox.rcc.RccMessage;
import fr.bl.iparapheur.EnvBeans;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import javax.transaction.UserTransaction;
import org.alfresco.model.ContentModel;


/**
 * Tache planifiee : envoi d'une commande asynchrone d'interrogation du
 * statut d'un dossier
 */
public class GetStatusAsyncJob implements Job {

    public static interface K {
        String rccMessage="rccMessage";
        String logMessage="logMessage";
        String camelContext="camelContext";

        String envBeans="envBeans";
        String nodeRef="nodeRef";
        String userLogin="userLogin";
        String jobName="jobName";
        String jobGroup="jobGroup";

    }

    private static Logger logger = LoggerFactory.getLogger(GetStatusAsyncJob.class.getName());

    /**
     * Determine s'il est pertinent d'interroger le TDT pour ce dossier.
     */
    public static boolean isSchedulingRelevant(EnvBeans envBeans, NodeRef nodeRef) {

        if (! envBeans.getNodeService().exists(nodeRef)) {
            // il a pu etre supprime a la main par l'administrateur
            logger.warn("le noeud [{}] n'existe pas",nodeRef);
            return false;
        }

        if (! envBeans.getParapheurService().isDossier(nodeRef)) {
            // je ne sais pas trop dans quel cas ca pourrait se produire
            // mais ca ne mange pas de pain
            logger.error("le noeud [{}] n'est pas un dossier",nodeRef);
            return false;
        }

        {
            // si pas de statut : on suppose que l'on n'a pas encore eu de reponse
            // du TDT ... et qu'il est donc pertinent d'interroger
            Object status=envBeans.getNodeService().getProperty(nodeRef,
                        SrciService.K.property_srciTransaction_status);
            if (status==null) {
                return true;
            }
        }
        
        {
            // si le statut courant du dossier indique une fin de traitement
            // cote SRCI : pas la peine de reposer la question
            String status=envBeans.getNodeService().getProperty(nodeRef,
                        SrciService.K.property_srciTransaction_status)
                        .toString();

            if (SrciService.getStatusCodeEndOfTdtJob().contains(status)) {
                if (logger.isInfoEnabled()) {
                    String dossierName=envBeans.getNodeService().getProperty(nodeRef, ContentModel.PROP_TITLE).toString();
                    logger.info("fin transaction SRCI pour [{}] : [{}]", dossierName, status);
                }
                return false;
            }
        }

        return true;
    }

    @Override
    public void execute(final JobExecutionContext ctx) throws JobExecutionException {

        BBean par=(BBean) ctx.getJobDetail().getJobDataMap().get("parameters");
        try {

            final String userLogin = par.get(K.userLogin, String.class);

            boolean goOn=true;
            goOn=
                AuthenticationUtil.runAs(
                    new GoOnChecker()
                        .setParameters(par),
                    userLogin);

            if (!goOn) {
                String jobName  = ctx.getJobDetail().getName();
                String jobGroup = ctx.getJobDetail().getGroup();
                logger.debug("suppression du job [{}-{}]", jobName, jobGroup);
                ctx.getScheduler().unscheduleJob(jobName, jobGroup);
                return;
            }


            // -----------------------------------------------------------------
            // envoi de la demande asynchrone de verification de l'etat
            // du dossier
            
            RccMessage rccMessage = par.get(K.rccMessage, RccMessage.class);
            String logMessage = par.get(K.logMessage, String.class);
            CamelContext camelContext = par.get(K.camelContext, CamelContext.class);

            logger.info(logMessage);
            Sender.def().send(
                    camelContext, SrciConfig.K.directProxyUri, rccMessage);

        } catch (Exception ex) {
            logger.warn("erreur lors du lancement de l'interrogation du TDT", ex);
        }
    }


    /**
     * Quelques trucs a verifier avant de lancer effectivement l'interrogation
     * asynchrone du TDT
     */
    class GoOnChecker implements AuthenticationUtil.RunAsWork<Boolean> {
        
        BBean parameters;
        protected BBean par() { return parameters; }
        public GoOnChecker setParameters(BBean parameters) {
            this.parameters=parameters;
            return this;
        }

        /**
         * Verifie s'il est bien necessaire de lancer l'interrogation du statut
         * @return true ssi il faut continuer dans cette voie
         */
        @Override
        public Boolean doWork() throws Exception {

            final EnvBeans envBeans = par().get(K.envBeans, EnvBeans.class);
            final NodeRef nodeRef = par().get(K.nodeRef, NodeRef.class);

            boolean goOn=true;
            UserTransaction utx = envBeans.getTransactionService().getUserTransaction();
            try {
                utx.begin();
                goOn=isSchedulingRelevant(envBeans, nodeRef);
                /*
                checkGoOn: {
                    goOn=false;

                    if (! envBeans.getNodeService().exists(nodeRef)) {
                        // il a pu etre supprime a la main par l'administrateur
                        logger.warn("le noeud [{}] n'existe plus  - arret interrogation SRCI",nodeRef);
                        break checkGoOn;
                    }

                    if (! envBeans.getParapheurService().isDossier(nodeRef)) {
                        // je ne sais pas trop dans quel cas ca pourrait se produire
                        // mais ca ne mange pas de pain
                        logger.error("le noeud [{}] n'est pas un dossier - arret interrogation SRCI",nodeRef);
                        break checkGoOn;
                    }

                    {
                        // si le statut courant du dossier indique une fin de traitement
                        // cote SRCI : cesser d'interroger
                        String status=envBeans.getNodeService().getProperty(nodeRef,
                                    SrciService.K.property_srciTransaction_status)
                                    .toString();

                        if (SrciService.getStatusCodeEndOfTdtJob().contains(status)) {
                            if (logger.isInfoEnabled()) {
                                String dossierName=envBeans.getNodeService().getProperty(nodeRef, ContentModel.PROP_NAME).toString();
                                logger.info("fin transaction SRCI pour [{}] : [{}]", dossierName, status);
                            }
                            break checkGoOn;
                        }
                    }

                    goOn=true;
                }
                 */

                utx.commit();
            } catch (Exception any) {
                try { utx.rollback(); } catch (Exception rollbackEx) { logger.error("rollback", rollbackEx); }
                throw any;
            }

            return goOn;

        }

    }


}
