package fr.bl.iparapheur.srci;

import com.atolcd.parapheur.model.ParapheurModel;
import fr.bl.gbox.bean.BBean;
import fr.bl.gbox.bean.StringList;
import fr.bl.gbox.bean.io.JsonReader;
import fr.bl.gbox.rcc.RccMessage;
import fr.bl.gbox.rcc.camel.Sender;
import fr.bl.iparapheur.EnvBeans;
import fr.bl.iparapheur.web.BlexContext;
import fr.bl.tdtproxysrci.TPS;
import org.adullact.iparapheur.status.StatusMetier;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.filestore.FileContentWriter;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.camel.CamelContext;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.ConnectException;
import java.util.*;

/**
 * Quelques services SRCI exposes au monde (i.e. InterfaceParapheurImpl) dans
 * une classe instanciee par Spring
 */
public class SrciService  implements InitializingBean {

    private static Logger logger = LoggerFactory.getLogger(SrciService.class.getName());
    Logger getLogger() {
        return logger;
    }

    private EnvBeans envBeans;
    public EnvBeans getEnvBeans() { return envBeans; }
    public void setEnvBeans(EnvBeans srciBeans) { this.envBeans = srciBeans; }

     /**
      * Appelle par Spring apres construction de l'objet et set de toutes
      * les proprietes
      *
      * @see InitializingBean#afterPropertiesSet()
      */
    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(envBeans, "srciBeans");
    }

    public static interface K {

        /**
         * "nom" de ce TDT
         * attention : cette valeur est referencee implicitement dans
         * editType.get.html.ftl et editType.post.js
         */
        public static String tdtName="SRCI";


        /** 
         * noms des protocoles, au sens de
         * nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_PROTOCOLE);
         */
        public static interface protocol {
            public static String helios="HELIOS";
            // public static String actes="ACTES";
        }


        /**
         * "aspect" indiquant qu'un dossier fait l'objet d'une transaction SRCI,
         * a utiliser comme  getNodeService().hasAspect(nodeRef, ...)
         * - attention cette valeur se retrouve dans parapheurModel.xml
         */
        QName aspect_srciTransaction = QName.createQName(
                ParapheurModel.PARAPHEUR_MODEL_URI,
                "srciTransaction");

        /**
         * nom du document en transit, vu de SRCI (un nom de unique, se terminant par .xml)
         * - usage : nodeService.getProperty(dossierRef, ...)
         * - attention cette valeurse retrouve dans parapheurModel.xml
         */
        QName property_srciTransaction_documentName = QName.createQName(
                ParapheurModel.PARAPHEUR_MODEL_URI,
                "srciTransaction_documentName");

        /**
         * statut du dossier (code)
         * - attention cette valeur se retrouve dans parapheurModel.xml
         */
        QName property_srciTransaction_status = QName.createQName(
                ParapheurModel.PARAPHEUR_MODEL_URI,
                "srciTransaction_status");

        /**
         * fichier retourné par Helios (contenu xml)
         * - attention cette valeur se retrouve dans parapheurModel.xml
         */
        QName property_srciTransaction_nackHeliosXml = QName.createQName(
                ParapheurModel.PARAPHEUR_MODEL_URI,
                "srciTransaction_nackHeliosXml");
        
    }

    public SrciService() {
        logger.info("creation instance SrciService");
    }

    /**
     * Recuperation des proprietes liees au tenant, utiles a la signature d'un fichier PES
     */
    public Properties getXadesSignatureProperties() {
        try {
            // recuperation de toutes les proprietes
            Map<String, String> propertiesHelios =
                    new TenantSrciProperties()
                        .setSrciBeans(envBeans)
                        .getPropertiesHelios();

            // extraction des proprietes en rapport avec l'action de signature
            Properties props = new Properties();
            for (String k : TenantSrciProperties.K.helios.sign_properties) {
                String v=propertiesHelios.get(k);
                props.setProperty(k, v);
            }
            logger.debug("props={}", props);

            return props;
            
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new AlfrescoRuntimeException(ex.getMessage(), ex);
        }
    }

    /**
     * Envoi d'un dossier HELIOS au TDT, par l'intermediaire du proxy
     * 
     * Une commande d'envoi de dossier est envoyée au TDT proxy, qui nous
     * repondra par callback en invoquant une command OnPesStatusNotification.
     *
     * @param dossier le dossier a expedier
     * @throws IOException s'il n'est pas possible de constuire la commande
     *  ou de l'expedier au proxy
     */
    public void sendHelios(NodeRef dossier) throws Exception {
        Assert.isTrue(envBeans.getParapheurService().isDossier(dossier), "NodeRef doit etre de type ph:dossier");
        Assert.isTrue(!envBeans.getNodeService().hasAspect(dossier, K.aspect_srciTransaction), "Le dossier a deja ete envoye au TDT SRCI");

        // le nom du document au sens de la transaction SRCI
        String documentName=UUID.randomUUID().toString()+".xml";

        // le nom du dossier au sens du parapheur
        String dossierName=envBeans.getNodeService().getProperty(dossier, ContentModel.PROP_TITLE).toString();

        File pesFile;
        {   // Récupération DU document dans un fichier
            List<NodeRef> documents = envBeans.getParapheurService().getMainDocuments(dossier);

            // cette version d'API ne permet l'envoi que d'un fichier XML (le premier doc du dossier)
            ContentReader reader = envBeans.getContentService().getReader(documents.get(0), ContentModel.PROP_CONTENT);
            String mimeType = reader.getMimetype().trim();

            logger.debug("documents.size()={}", documents.size());
            logger.debug("mainDocMimeType={}", mimeType);

            if (!mimeType.equalsIgnoreCase(MimetypeMap.MIMETYPE_XML)
                    && !mimeType.contains("text/xml")
                    && !mimeType.contains("application/xml")
                    && !mimeType.contains("application/readerpesv2")
                    && !mimeType.equalsIgnoreCase("application/vnd.xemelios-xml")) {
                throw new RuntimeException("Echec d'envoi: la plateforme SRCI pour HELIOS attend un fichier XML");
            }

            {
                SrciConfig config=BlexContext.getInstance().getSrciConfig();
                pesFile = new File(config.getServerInboxFolder(), documentName);
                // Si le type MIME correspond, on l'envoie tel quel
                if (mimeType.equals(reader.getMimetype())) {
                    reader.getContent(pesFile);
                } else { // Sinon, on commence par le transformer
                    FileContentWriter tmpWriter = new FileContentWriter(pesFile);
                    tmpWriter.setMimetype(mimeType);
                    tmpWriter.setEncoding(reader.getEncoding());
                    envBeans.getContentService().transform(reader, tmpWriter);
                }
            }

        }

        //
        // 
        Map<String, String> ppt  =
                new TenantSrciProperties()
                        .setSrciBeans(envBeans)
                        .getPropertiesHelios();


        // construction de la requete asynchrone

        SrciConfig srciConfig=BlexContext.getInstance().getSrciConfig();

        BBean callbackContext=new BBean()
                .set(OnPesStatusNotification.K.in.nodeRef,   dossier.toString())
                .set(OnPesStatusNotification.K.in.userLogin, envBeans.getParapheurService().getActeurCourantBLSRCI(dossier))
                .set(OnPesStatusNotification.K.in.isResponseToSendCommand, Boolean.TRUE.toString())
                ;

        RccMessage om=new RccMessage()
            .setPayload(
                TPS.SendPes.name,
                new BBean()
                    .set(TPS.SendPes.in.account, ppt.get(TenantSrciProperties.K.helios.proxy_account))
                    .set(TPS.SendPes.in.docName, documentName)
                    .set(TPS.SendPes.in.docRef, pesFile.getAbsolutePath())
                )
            .setCallback(
                SrciConfig.K.registry.onPesStatusNotification,
                srciConfig.getInboxFolderUri(),
                callbackContext
            )
        ;

        logger.info("envoi du dossier [{}] au TDT sur [{}]", dossierName, SrciConfig.K.directProxyUri+" ("+documentName+")");

        // proprietes persistantes

        Map<QName, Serializable> pptes = new HashMap<QName, Serializable>();
        pptes.put(K.property_srciTransaction_documentName, documentName);
        envBeans.getNodeService().addAspect(dossier, K.aspect_srciTransaction, pptes);
        envBeans.getParapheurService().auditTransmissionTDT(dossier, StatusMetier.STATUS_EN_COURS_TRANSMISSION, "Dossier en télétransmission");

        // considerer le dossier comme "nouvellement envoye", meme s'il a deja
        // fait un tour sur le TDT (ce qui est legal en cas d'erreur sur le
        // TDT : on peut toujours reexpedier un dossier)
        {
            Object status=envBeans.getNodeService().getProperty(dossier, SrciService.K.property_srciTransaction_status);
            if (status!=null) {
                logger.info("envoi du dossier {}, raz de '"+SrciService.K.property_srciTransaction_status+"', qui etait a : [{}]", dossierName, status);
                envBeans.getNodeService().removeProperty(dossier, SrciService.K.property_srciTransaction_status);
            }
        }

        // envoi effectif
        CamelContext camelContext=BlexContext.getInstance().getCamelContext();
        Sender.def().send(camelContext, SrciConfig.K.directProxyUri, om);

        // observation reguliere du statut 
        new GetStatusAsyncScheduler()
                .setEnvBeans(envBeans)
                .setCamelContext(camelContext)
                .setNodeRef(dossier)
                .schedule();

    }

    /**
     * Interrogation synchrone du proxy pour obtention de l'etat d'un dossier
     * (texte)
     *
     * Mime S2lowServiceImpl.statutS2lowToString
     * 
     * @return un statut en francais
     */
    public String getTdtStatusText(NodeRef nodeRef) throws Exception {
        try {
            return tdtStatusCodeToText(getTdtStatusCode(nodeRef));

        } catch (ConnectException ex) {
            throw new Exception("impossible de se connecter au proxy SRCI - "+ex.getMessage(), ex);
        }
    }

    /**
     * La liste des statuts qui indiquent une fin de traitement
     * cote TDT
     */
    public static StringList getStatusCodeEndOfTdtJob() {
        return new StringList()
                .add(TPS.Status.ackFuncReceived)
                .add(TPS.Status.nackFuncReceived)
                .add(TPS.Status.nackTechReceived)
                .add(TPS.Status.canceled)
                .add(TPS.Status.error)
                ;
    }

    /**
     * Traduction d'un statut TDT code en texte
     *
     * Voir S3lowServiceImpl.statutS2lowToString
     * @param statusCode
     * @return
     * @throws Exception
     */
    public static String tdtStatusCodeToText(String statusCode)  {
        if (TPS.Status.sentToTdt.equals(statusCode)) {
            return "En traitement";
        } else if (TPS.Status.notHandledByTdt.equals(statusCode)) {
            return "En traitement";
        } else if (TPS.Status.handledByTdt.equals(statusCode)) {
            return "Posté";
        } else if (TPS.Status.error.equals(statusCode)) {
            return "Erreur";
        } else if (TPS.Status.canceled.equals(statusCode)) {
            return "Annulé";
        } else if (TPS.Status.sentToTarget.equals(statusCode)) {
            return "Transmis";
        } else if (TPS.Status.ackTechReceived.equals(statusCode)) {
            return "Transmis";
        } else if (TPS.Status.nackTechReceived.equals(statusCode)) {
            return "Erreur";
        } else if (TPS.Status.ackFuncReceived.equals(statusCode)) {
            return "Acquittement reçu";
        } else if (TPS.Status.nackFuncReceived.equals(statusCode)) {
            return "Refusé";
        } else {
            logger.warn("statut SRCI inconnu : "+statusCode);
            return statusCode;
        }
    }

    /**
     * Interrogation synchrone du proxy pour obtention de l'etat d'un dossier
     * (code)
     *
     * @return un code de status {@link TPS }.Status
     */
    String getTdtStatusCode(NodeRef nodeRef) throws Exception {
        logger.debug("nodeRef={}", nodeRef);

        HttpClient httpclient = new HttpClient();

        SrciConfig srciConfig=BlexContext.getInstance().getSrciConfig();
        httpclient.getHostConfiguration().setHost(srciConfig.getServerUrlHost(), srciConfig.getServerUrlPort());

        String pesName=envBeans.getNodeService().getProperty(nodeRef, K.property_srciTransaction_documentName).toString();
        String account=new TenantSrciProperties()
                .setSrciBeans(envBeans)
                .getPropertiesHelios()
                .get(TenantSrciProperties.K.helios.proxy_account);

        RccMessage rccMessage = new RccMessage().setPayload(
                TPS.GetStatus.name,
                new BBean()
                    .set(TPS.GetStatus.in.account, account)
                    .set(TPS.GetStatus.in.docName, pesName)
                    .set(TPS.GetStatus.in.replyToUri, srciConfig.getInboxFolderUri())
                    );

        if (logger.isInfoEnabled()) {
            String dossierName=envBeans.getNodeService().getProperty(nodeRef, ContentModel.PROP_TITLE).toString();
            logger.info("demande d'etat du dossier sur TDT SRCI : srciDossier=" + pesName 
                    + "; parapheurDossier=" + dossierName 
                    + "; account=" + account);
        }
        
        GetMethod get = Sender.def().buildGetMethod(
                rccMessage,
                TPS.httpDirect.executeRelativeUri);

        httpclient.executeMethod(get);

        BBean ret = JsonReader.def().readBean(get.getResponseBodyAsString());
        String status = ret.get(TPS.GetStatus.out.status, String.class);
        // Obtenir et stocker le fichier retour Helios
        String retDocAckRef = ret.get(TPS.GetStatus.out.docAckRef, String.class);
        if (retDocAckRef != null && !retDocAckRef.isEmpty()) {
            downloadHeliosAckNack(nodeRef, envBeans, retDocAckRef);
        }
        // Note : le job éventuel s'interrompra lui-même lorsqu'il détectera qu'un résultat a déjà été obtenu.

        if (logger.isInfoEnabled()) {
            logger.info("reception d'etat du dossier sur TDT SRCI : srciDossier=" + pesName
                    + "; status=" + status);
        }
        
        return status;
    }


    /**
     * Un dossier est "emis" dans le parapheur.
     * Si le dossier a deja ete envoye a SRCI (et qu'il etait en erreur - j'imagine)
     * : suppression de l'aspect SRCI
     */
    public void onEmission(NodeRef dossier) {
        if (envBeans.getNodeService().hasAspect(dossier, K.aspect_srciTransaction)) {
            Object actualStatus=envBeans.getNodeService().getProperty(dossier, K.property_srciTransaction_status);
            envBeans.getNodeService().removeAspect(dossier, K.aspect_srciTransaction);
            envBeans.getNodeService().removeProperty(dossier, K.property_srciTransaction_documentName);
            envBeans.getNodeService().removeProperty(dossier, K.property_srciTransaction_status);
            envBeans.getNodeService().removeProperty(dossier, K.property_srciTransaction_nackHeliosXml);
            logger.info(
                    "emission a nouveau de [{}] - suppression de l'aspect et des proprietes SRCI - etait [{}]",
                    envBeans.getNodeService().getProperty(dossier, ContentModel.PROP_NAME),
                    actualStatus);
        }
    }

    public void notImplemented(String message) throws Exception {
        throw new Exception(message);
    }

    /**
     * Obtenir et stocker le fichier retour Helios
     * Note : ce traitement doit être encadré par une transaction Alfresco.
     */
    public static void downloadHeliosAckNack(NodeRef dossier, EnvBeans envBeans, String docAckRef) throws Exception {
        File retFile = new File(docAckRef);
        if (logger.isInfoEnabled()) {
            logger.info("Reponse Srci : filename=" + retFile.getAbsolutePath());
        }

        ContentWriter writer =  envBeans.getContentService().getWriter(dossier, SrciService.K.property_srciTransaction_nackHeliosXml, true);
        writer.setMimetype(MimetypeMap.MIMETYPE_XML);
        writer.putContent(retFile);
    }
    
}
