/*
 * Version 3.2
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.bl.iparapheur.web;

import fr.bl.gbox.bean.BBean;
import fr.bl.gbox.bean.StringList;
import fr.bl.gbox.bean.getter.GetDirectory;
import fr.bl.iparapheur.srci.GetStatusSchedulerRestartAll;
import fr.bl.iparapheur.rcc.IParaCamelContext;
import fr.bl.iparapheur.rcc.IParaCommandRegistry;
import fr.bl.iparapheur.EnvBeans;
import fr.bl.iparapheur.rcc.RccConfig;
import fr.bl.iparapheur.srci.SrciConfig;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


import fr.bl.iparapheur.xwv.XwvConfig;
import java.util.ArrayList;
import java.util.List;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Context Listener spécifique aux extensions BL (blex)
 *                                                                             <br><br>
 * <h3>Serveur XWV</h3> 
 * 
 * Cette classe permet de déclarer la présence d'un serveur XWV
 * (Xémélios Web Viewer) à contacter pour afficher les flux PES.
 *                                                                             <br><br>
 * Si un serveur XWV est réputé présent, son URL sera indiquée par la 
 * variable d'environnement JNDI :                                             <br><b>
 * iparapheur-blex/bl-xemwebviewer-url                                         </b>
 *                                                                             <br><br>
 * 
 * Cette variable peut être paramétrée dans le fichier :                       <br><tt>
 * [tomcat]/conf/Catalina/localhost/alfresco.xml                               </tt>
 *                                                                             <br>
 * ajouter, typiquement :                                                      <br><tt>     
 * &lt;Environment                                                             <br>&nbsp;
 * name="iparapheur-blex/bl-xemwebviewer-url"                                  <br>&nbsp;
 * value="http://172.16.113.117/bl-xemwebviewer"                               <br>&nbsp;
 * type="java.lang.String"                                                     <br>&nbsp;
 * override="false"/>                                                          </tt><br>
 * 
 * <h3>Utilisation</h3> 
 * 
 * Pour être invoqée, cette classe doit être déclarée dans web.xml :           <br><tt>&nbsp;
 * &lt;listener>                                                               <br>&nbsp;
 * &lt;listener-class>fr.bl.iparapheur.web.BlexServletContextListener&lt;/listener-class> <br>&nbsp;
 * &lt;/listener>                                                              </tt>
 * 
 * 
 */
public class BlexServletContextListener implements ServletContextListener {

    private static String appName = "iparapheur-blex";


    private static interface K {
        String bl_tdtproxysrci_server_url="bl-tdtproxysrci-server-url";
        String bl_tdtproxysrci_server_inbox="bl-tdtproxysrci-server-inbox";
        String bl_rcc_inbox="bl-rcc-inbox";
        String bl_tdtproxysrci_schedule_status_unit_seconds="bl-tdtproxysrci-schedule-status-unit-seconds";

    }

    public void contextDestroyed(ServletContextEvent sce) {
    }

    public void contextInitialized(ServletContextEvent sce) {


        Logger logger = LoggerFactory.getLogger(this.getClass().getName());


        Context envCtx = null;

        // ---------------------------------------------------------------
        // recuperation contexte
        // ---------------------------------------------------------------
        try {
            Context initCtx = new InitialContext();
            envCtx = (Context) initCtx.lookup("java:comp/env/iparapheur-blex");
        } catch (Exception any) {
            logger.info(appName + " : configuration BLEX inactive");
        }

        // ---------------------------------------------------------------
        // setup serveur XWV
        // ---------------------------------------------------------------
        {
            // l'URL auprès de laquelle on peut contacter le service XWV
            // null indique une absence de configuration (ce qui est une situation legale)
            String xwvServerUrl = null;
            if (envCtx != null) {
                try {
                    String key = "bl-xemwebviewer-url";
                    Object value = null;
                    try {
                        value = envCtx.lookup(key);
                    } catch (NameNotFoundException nameNotFoundException) {
                    }
                    if (value != null) {
                        xwvServerUrl = value.toString();
                        logger.info(appName + " : serveur XWV configure sur : " + xwvServerUrl);
                    } else {
                        logger.info(appName + " : serveur XWV non configure");
                    }
                } catch (NameNotFoundException nameNotFoundException) {
                    logger.info(appName + " : serveur XWV non configure");
                } catch (Exception ignore) {
                }
            }
            // construction du contexte XWV - dans tous les cas
            BlexContext.getInstance().setXwvConfig(new XwvConfig(xwvServerUrl));
            // verification du serveur associé au contexte XWV - le cas echeant
            if (xwvServerUrl != null) {
                BlexContext.getInstance().getXwvConfig().doCheckServer();
            }
        }

        // ---------------------------------------------------------------------
        // setups RCC
        // ---------------------------------------------------------------------

        List<RccConfig> rccConfigs=new ArrayList<RccConfig>();

        // ---------------------------------------------------------------------
        // setup proxy TDT SRCI
        // ---------------------------------------------------------------------

        // lecture conf RCC
        SrciConfig srciConfig=readSrciConfig(logger, envCtx);
        if (srciConfig!=null) {
            rccConfigs.add(srciConfig);
            BlexContext.getInstance().setSrciConfig(srciConfig);
        }
        
        if (rccConfigs.size()>0) {

            // -----------------------------------------------------------------
            // registre des commandes
            // -----------------------------------------------------------------
            ApplicationContext springContext =
                    WebApplicationContextUtils.getRequiredWebApplicationContext(sce.getServletContext());
            EnvBeans envBeans = (EnvBeans) springContext.getBean(EnvBeans.K.springName);
            BlexContext.getInstance().setCommandRegistry(
                    new IParaCommandRegistry().setEnvBeans(envBeans));

            for (RccConfig rccConfig : rccConfigs) {
                rccConfig.populateCommandRegistry(BlexContext.getInstance().getCommandRegistry());
            }

            // -----------------------------------------------------------------
            // construction des routes CAMEL
            // -----------------------------------------------------------------
            IParaCamelContext camelContext = new IParaCamelContext();
            BlexContext.getInstance().setCamelContext(camelContext);

            for (RccConfig rccConfig : rccConfigs) {
                try {
                    camelContext.buildRoutes(rccConfig);
                } catch (Exception e) {
                    logger.error("erreur lors de la mise en place CAMEL ", e);
                }
            }

            // -----------------------------------------------------------------
            // démarrage des routes CAMEL
            // -----------------------------------------------------------------
            try {
                camelContext.start();
            } catch (Exception any) {
                logger.error("erreur de parametrage  : impossible de lancer le contexte CAMEL", any);
            }

            // -----------------------------------------------------------------
            // SRCI : replanification de l'observateur des statuts
            // -----------------------------------------------------------------
            if (srciConfig!=null) {
                try {
                    new GetStatusSchedulerRestartAll().setSrciBeans(envBeans).setCamelContext(camelContext).execute();
                } catch (JobExecutionException ex) {
                    logger.error("impossible de planifer l'interogation du serveur SRCI", ex);
                }
            }
        }


    }

    void readAndSet(Context envCtx, String key, BBean target) {
        if (envCtx == null) {
            return;
        }
        try {
            // URL de contact pour acces synchrone
            Object value = null;
            value = envCtx.lookup(key);
            if (value != null) {
                target.set(key, value);
            }
        } catch (Exception ignore) {
        }
    }


    /**
     * Lecture de l'eventuelle configuration SRCI
     * @return null si pas
     */
    SrciConfig readSrciConfig(Logger logger, Context envCtx) {
        /* 
        ------------------------------------------------------------------------
        Exemple typique de configuration
        ------------------------------------------------------------------------
        <Environment
        name="iparapheur-blex/bl-tdtproxysrci-server-url"
        value="http://localhost:8086"
        type="java.lang.String"
        override="false"/>
        
        <Environment
        name="iparapheur-blex/bl-tdtproxysrci-server-inbox"
        value="/opt/tdtproxysrci/work/inbox"
        type="java.lang.String"
        override="false"/>
        
        <Environment
        name="iparapheur-blex/bl-rcc-inbox"
        value="/opt/iParapheur/blex/rcc/inbox"
        type="java.lang.String"
        override="false"/>
        ------------------------------------------------------------------------
         */

        try {
            StringList mandatoryKeys = new StringList()
                    .add(K.bl_rcc_inbox)
                    .add(K.bl_tdtproxysrci_server_inbox)
                    .add(K.bl_tdtproxysrci_server_url);

            StringList keys = new StringList()
                    .add(mandatoryKeys)
                    .add(K.bl_tdtproxysrci_schedule_status_unit_seconds);

            BBean b = new BBean().setDescription("configuration SRCI");

            // valeurs par defaut
            b.set(K.bl_tdtproxysrci_schedule_status_unit_seconds, "60");

            // lecture des valeurs possibles
            for (String k : keys) {
                readAndSet(envCtx, k, b);
            }

            boolean hasConfiguration=
                    b.containsKey(K.bl_tdtproxysrci_server_url)
                    || b.containsKey(K.bl_tdtproxysrci_schedule_status_unit_seconds)
                    || b.containsKey(K.bl_tdtproxysrci_server_inbox);

            if (hasConfiguration) {
                logger.info("configuration SRCI : {}", b);
            SrciConfig srciConfig = new SrciConfig();
            b.assertNotEmpty(mandatoryKeys);

            srciConfig.setInboxFolder(GetDirectory.def().getValue(b, K.bl_rcc_inbox));
            srciConfig.setServerInboxFolder(GetDirectory.def().getValue(b, K.bl_tdtproxysrci_server_inbox));
            srciConfig.setServerUrl(b.get(K.bl_tdtproxysrci_server_url, String.class));
            srciConfig.setScheduleStatusUnitSec(b.get(K.bl_tdtproxysrci_schedule_status_unit_seconds, Integer.class));

                return srciConfig;

            } else {
                logger.info("pas de configuration SRCI");
            }

        } catch (Exception any) {
            logger.warn("erreur lors de parametrage proxy SRCI", any);
        }

        return null;
    }

        }
