package fr.bl.iparapheur.rcc;

import fr.bl.gbox.command.CommandRegistry;
import java.io.File;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * La configuration d'un serveur RCC, vu du parapheur
 */
public abstract class RccConfig {

    private Logger logger = LoggerFactory.getLogger(RccConfig.class.getName());

    private String serverUrl = null;

    /**
     * L'url aupres de laquelle on contacte le serveur distant par
     * appel HTTP
     * 
     * @return 
     *     une url, par exemple : "http://localhost:8086"
     */
    public String getServerUrl() {
        return serverUrl;
    }

    /**
     * Déclaration de l'url aupres de laquelle on contacte le serveur distant par
     * appel HTTP
     */
    public RccConfig setServerUrl(String serverUrl) throws Exception {
        URL url = new URL(serverUrl);
        serverUrlHost = url.getHost();
        serverUrlPort = url.getPort();
        this.serverUrl = serverUrl;
        return this;
    }

    String serverUrlHost;

    /**
     * L'hote de l'url aupres de laquelle on contacte le serveur distant par
     * appel HTTP
     *
     * @return
     *     un hote, par exemple : "localhost"
     */
    public String getServerUrlHost() {
        return serverUrlHost;
    }

    int serverUrlPort;

    /**
     * Le numero de port de l'url aupres de laquelle on contacte le serveur
     * distant par appel HTTP.
     *
     * @return
     *     un numero de port
     */
    public int getServerUrlPort() {
        return serverUrlPort;
    }

    /**
     * Indique si le serveur supporte le protocole RCC "envoi de commande
     * pas depot de message dans un repertoire".
     */
    public boolean isFolderProtocolSupported() {
        return getServerInboxFolder() != null;
    }

    File serverInboxFolder;

    /**
     * le repertoire dans lequel on depose les messages (asynchrones) destines
     * au serveur distant
     */
    public File getServerInboxFolder() {
        return serverInboxFolder;
    }

    /**
     * Déclaration du repertoire dans lequel on depose les messages asynchrones 
     * destines au serveur distant
     */
    public RccConfig setServerInboxFolder(File proxyInboxFolder) {
        this.serverInboxFolder = proxyInboxFolder;
        return this;
    }

    File inboxFolder;

    /**
     * Déclaration du repertoire dans lequel JE releve les message asynchrones 
     * qui me sont destines (des callback du serveur distant, a priori)
     */
    public RccConfig setInboxFolder(File inboxFolder) {
        this.inboxFolder = inboxFolder;
        return this;
    }

    /**
     * Déclaration du repertoire dans lequel JE releve les message asynchrones 
     * qui me sont destines (des callback du serveur distant, a priori)
     */
    public RccConfig setInboxFolder(String inboxFolder) {
        this.inboxFolder = new File(inboxFolder);
        return this;
    }

    /**
     * Le repertoire dans lequel JE releve les message asynchrones qui me
     * sont destines (des callback du serveur distant, a priori)
     */
    public File getInboxFolder() {
        return inboxFolder;
    }

    /**
     * Le repertoire dans lequel JE releve les message asynchrones qui me
     * sont destines (des callback du serveur)
     *
     * @return une URI (file://...)
     */
    public String getInboxFolderUri() {
        return inboxFolder.toURI().toASCIIString();
    }

    public RccConfig setServerInboxFolder(String proxyInboxFolder) {
        this.serverInboxFolder = new File(proxyInboxFolder);
        return this;
    }

    /**
     * L'URI sur laquelle un composant peux poster des trucs a destination
     * du serveur. 
     * Utilisé par 
     * {@link IParaCamelContext#buildRoutes(fr.bl.iparapheur.rcc.RccConfig) IParaCamelContext }
     * pour construire une route vers 
     * {@link #getServerInboxFolder() getServerInboxFolder()}
     *
     * @return  une URI a usage interne, du genre "direct:serveur_pour_ceci_et_cela"
     */
    public abstract String getServerInboxUri();


    /**
     * Enregistrement des commandes proposées au monde, que l'on pourra
     * traiter ici.
     * 
     * @param commandRegistry Le registre dans lequel on inscrit les commandes
     */
    public abstract void populateCommandRegistry(CommandRegistry commandRegistry);
}
