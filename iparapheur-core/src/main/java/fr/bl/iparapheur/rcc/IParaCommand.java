package fr.bl.iparapheur.rcc;

import fr.bl.iparapheur.EnvBeans;
import fr.bl.gbox.command.Command;


/**
 * Une commande executable sur le parapheur par appel a distance depuis
 * un tiers.
 * Possede un paquet de beans d'environnement (EnvBeans)
 */
public abstract class IParaCommand extends Command {

    protected EnvBeans envBeans;
    IParaCommand setEnvBeans(EnvBeans  srciBeans) {
        this.envBeans = srciBeans;
        return this;
    }

}
