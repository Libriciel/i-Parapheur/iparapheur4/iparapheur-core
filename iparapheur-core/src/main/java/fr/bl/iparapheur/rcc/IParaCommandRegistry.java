package fr.bl.iparapheur.rcc;

import fr.bl.iparapheur.EnvBeans;
import fr.bl.gbox.command.Command;
import fr.bl.gbox.command.CommandRegistry;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Les registre des commandes appelables de l'exterieur
 */
public class IParaCommandRegistry extends CommandRegistry  {

    private Logger logger = LoggerFactory.getLogger(IParaCommandRegistry.class.getName());

    private EnvBeans envBeans;
    
    /**
     * Declaration du bean d'environnement
     */
    public IParaCommandRegistry setEnvBeans(EnvBeans srciBeans) {
        this.envBeans = srciBeans;
        return this;
    }

    @Override
    protected Command doBuildCommand(String name, Map<String, Object> params) throws Exception {
        Command command = super.doBuildCommand(name, params);

        if (command instanceof IParaCommand) {
            ((IParaCommand)command)
                    .setEnvBeans(envBeans);
        }

        return command;
    }

    /**
     * Création du registre
     */
    public IParaCommandRegistry() {
        
        // lancera des exceptions si une commande demandée n'existe pas 
        super.setThrowNotFoundExceptions(true);
    }

}