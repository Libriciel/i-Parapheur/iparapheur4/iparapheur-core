package fr.bl.iparapheur.rcc;

import fr.bl.gbox.rcc.camel.FolderProtocol;
import fr.bl.iparapheur.web.BlexContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.bl.gbox.rcc.camel.ExceptionProtocol;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

/**
 * Un contexte Camel pour le parapheur
 */
public class IParaCamelContext extends DefaultCamelContext {


    String inboxUri;
    /**
     * URI du répertoire dans lequel on lit les messages entrant
     * @return une URI undiquant un répertoire
     */
    public String getInboxUri() {
        return inboxUri;
    }

    private static int ID=0;
    private int id=++ID;

    @Override
    public String toString() {
      return "IParaCamelContext{id:"+id+", routes:"+routes+"}";
    }

    private int routes=0;

    private static Logger logger=LoggerFactory.getLogger(IParaCamelContext.class.getName());

    List<String> exceptionProtocolInboxFolders=new ArrayList<String>();
    List<String> folderProtocolInboxFolders=new ArrayList<String>();
    List<String> folderProtocolServerUris=new ArrayList<String>();

    /**
     * Construction des routes Camel :
     * <ul>
     * <li> ecoute sur le repertoire d'entree </li>
     * <li> pseudo uri pour l'emission des messages </li>
     * <li> stockage des exceptions dans le repertoire <code>.error</code> </li>
     * </ul>
     */
    public void buildRoutes(final RccConfig config) throws Exception {

        logger.debug("construction des routes Camel pour {}", this);

        super.addRoutes(new RouteBuilder() {

            @Override
            public void configure() throws Exception {

                if (config.getInboxFolder()!=null) {

                    String inboxFolder=config.getInboxFolder().toString();

                    // exceptions dans un sous repertoire du repertoire d'entree
                    {
                        if (exceptionProtocolInboxFolders.contains(inboxFolder)) {
                            logger.debug("pas d'ajout ExceptionProtocol : deja supporte : {}", inboxFolder);
                        } else {
                            logger.debug("ajout ExceptionProtocol : {}", inboxFolder);
                            new ExceptionProtocol()
                                    .buildRoute(this, new File(config.getInboxFolder(), ".error"));
                            routes+=1;
                        }
                        exceptionProtocolInboxFolders.add(inboxFolder);
                    }

                    // ecoute sur le repertoire d'entree
                    {
                        if (folderProtocolInboxFolders.contains(inboxFolder)) {
                            logger.debug("pas d'ajout FolderProtocol ecoute : deja supporte : {}", inboxFolder);
                        } else {
                            logger.debug("ajout FolderProtocol ecoute : {}", inboxFolder);
                            new FolderProtocol()
                                    .setCommandFactory(BlexContext.getInstance().getCommandRegistry())
                                    .buildRoute(this, config.getInboxFolder())
                                    ;
                            routes+=1;
                            folderProtocolInboxFolders.add(inboxFolder);
                        }
                    }

                    // pseudo uri pour l'emission des messages
                    if (config.getServerInboxUri()!=null) {
                        String serverInboxUri=config.getServerInboxUri();
                        if (folderProtocolServerUris.contains(serverInboxUri)) {
                            logger.debug("pas d'ajout FolderProtocol emission : deja supporte : {}", inboxFolder);
                        } else {
                            logger.debug("ajout FolderProtocol emission : {}", serverInboxUri);
                            new FolderProtocol()
                                    .setCommandFactory(BlexContext.getInstance().getCommandRegistry())
                                    .buildSendRoute(this,
                                        serverInboxUri,
                                        config.getServerInboxFolder())
                                    ;
                            routes+=1;
                            folderProtocolServerUris.add(serverInboxUri);
                        }
                    }
                }
                    

                logger.debug("routes crees");

            }
        });
    }


}
