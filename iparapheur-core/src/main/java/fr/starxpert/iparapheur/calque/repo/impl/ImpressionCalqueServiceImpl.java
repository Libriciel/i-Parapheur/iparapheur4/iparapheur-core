/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2013, StarXpert, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by StarXpert and ADULLACT-projet
 * Maintained by ADULLACT-Projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.starxpert.iparapheur.calque.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import coop.libriciel.pdfstamp.StampException;
import coop.libriciel.utils.poppler.PopplerUtils;
import coop.libriciel.utils.poppler.TagSearchResult;
import fr.starxpert.iparapheur.calque.model.CalqueModel;
import fr.starxpert.iparapheur.calque.repo.CalqueService;
import fr.starxpert.iparapheur.calque.repo.ImpressionCalqueService;
import coop.libriciel.pdfstamp.PdfStampService;
import coop.libriciel.pdfstamp.model.*;
import lombok.extern.log4j.Log4j;
import org.adullact.iparapheur.util.CollectionsUtils;
import org.adullact.utils.StringUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.repo.template.BaseTemplateProcessorExtension;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.tika.io.IOUtils;
import org.dom4j.io.SAXReader;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classe implementant le ImpressionCalqueService
 *
 * @author kmezyaoui - StarXpert
 * @author Stephane Vast - Adullact Projet
 */
@Log4j
public class ImpressionCalqueServiceImpl extends BaseTemplateProcessorExtension implements ImpressionCalqueService {

    /**
     * Le tag qui permet de localiser l'image scannee de signature
     * Nouveauté 4.3 : le '#signature#' est le tag par défaut, qu'on peut modifier en fichier de propriété
     * Il y a aussi possibilité de surcharger pour un tenant particulier
     */
    @Value("${parapheur.libersign.tag.signature.name}")
    private String signatureTag = "#signature#";
    private Map<String, String> signatureTagMap = new HashMap<>();

    /**
     * Nouveauté 4.5, le tag pour cachet serveur #cachet#
     */
    @Value("${parapheur.libersign.tag.cachet.name}")
    private String cachetTag = "#cachet#";
    private Map<String, String> cachetTagMap = new HashMap<>();
    @Autowired
    private ServiceRegistry serviceRegistry;
    @Autowired
    private CalqueService calqueService;
    @Autowired
    private ParapheurService parapheurService;
    @Autowired
    private MetadataService metadataService;
    @Autowired
    private TenantService tenantService;
    @Autowired
    private PdfStampService pdfStampService;
    @Autowired
    private WorkflowService workflowService;

    public void setSignatureTagMapString(String signatureTagMapString) {
        try {
            JSONObject obj = new JSONObject(signatureTagMapString);
            JSONArray names = obj.names();
            if (names != null) {
                for (int i = 0; i < names.length(); i++) {
                    String key = (String) names.get(i);
                    if (!obj.getString(key).isEmpty()) {
                        this.signatureTagMap.put(key, new String(obj.getString(key).getBytes("ISO_8859_1")));
                    }
                }
            }
        } catch (JSONException e) {
            log.warn("Attention, propriété 'parapheur.libersign.tag.signature.name.tenants' introuvable ou invalide : " + e.getMessage());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void setCachetTagMapString(String cachetTagMapString) {
        try {
            JSONObject obj = new JSONObject(cachetTagMapString);
            JSONArray names = obj.names();
            if (names != null) {
                for (int i = 0; i < names.length(); i++) {
                    String key = (String) names.get(i);
                    if (!obj.getString(key).isEmpty()) {
                        this.cachetTagMap.put(key, new String(obj.getString(key).getBytes("ISO_8859_1")));
                    }
                }
            }
        } catch (JSONException e) {
            log.warn("Attention, propriété 'parapheur.libersign.tag.cachet.name.tenants' introuvable ou invalide : " + e.getMessage());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    // <editor-fold desc="Methodes privees">

    /**
     * Recherche de l'espace qui contient les previsualisations des calques
     *
     * @return le noeud de l'espace de previsualisation
     */
    private NodeRef getEspacePreview() {
        String query = "PATH:\"" + CalqueModel.PATH_CALQUE_PREVIEW + "\"";
        ResultSet resultat = serviceRegistry.getSearchService().query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_LUCENE,
                query);
        NodeRef spacePreview = null;
        if ((resultat != null) && (resultat.length() > 0)) {
            spacePreview = resultat.getNodeRef(0);
        } else {
            log.error("L'espace \"Preview\" n'existe pas.");
        }
        if (resultat != null) {
            resultat.close();
        }
        // NodeRef spacePreview = getNodeService().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);

        return spacePreview;
    }

    /**
     * @param props     tableau de propriétés
     * @param sourceRef NodeRef sur lequel tester les propriétés
     * @return le NodeRef pointé par la dernière propriété si et seulement si toutes les propriétés
     * précédentes (dans props) ne sont pas nulles.
     */
    private NodeRef applyIFF(String[] props, NodeRef sourceRef) {
        boolean verified = true;
        for (int i = 1; i < props.length - 1; i++) {
            if (serviceRegistry.getNodeService().getProperty(sourceRef, QName.createQName(props[i], serviceRegistry.getNamespaceService())) == null) {
                verified = false;
                break;
            }
        }
        return verified ?
                (NodeRef) serviceRegistry.getNodeService().getProperty(sourceRef, QName.createQName(props[props.length - 1], serviceRegistry.getNamespaceService()))
                : null;
    }

    /**
     * Returns first not-null property
     * @param props     Property names ("ph:title")
     * @param sourceRef Source node
     * @return NodeRef or String
     */
    private NodeRef applyOR(String[] props, NodeRef sourceRef) {
        NodeRef res = null;
        int i = 1;
        while (i < props.length && res == null) {
            res = (NodeRef) serviceRegistry.getNodeService().getProperty(sourceRef, QName.createQName(props[i], serviceRegistry.getNamespaceService()));
            i++;
        }
        return res;
    }

    /**
     * Peut être du type "[FONCTION]|[TDT]|[property]|[...]|property" où les propriétés intermédiaire réfèrent des noderef. On récupère
     * donc la valeur de la propriété sur le noderef trouvé (grâce aux propriétés intermédiaires).
     * On part du NodeRef du dossier. le dernier élément (FONCTION) est utilisé pour appliquer une fonction sur la valeur de la
     * propriété trouvée (utilisé pour récupérer l'empreinte de la signature électronique).
     * TDT est un mot-clé permettant de récupérer le noderef de la dernière étape de TDT. (peut être aussi SIGNATURE).
     * Prérequis : - il doit y avoir au moins une propriété en dernière position (tout le reste est optionnel).
     *             - le paramètre FONCTION doit se trouver au début.
     *
     * Pour une propriété intermédiare, on peut appliquer des opérateurs dessus (OU, IFF) en séparant deux propriété par un ";"
     * et en le préfixant de l'opérateur.
     * Ces opération sont utilses pour la délégation (ex. le role du signataire se trouve dans le noderef "ph:delegue" si il y
     * a délégation, sinon dans "ph:passe-par".
     *
     * @return Retrieved value
     */
    private @NotNull String retrieveTargetMetadataValue(@NotNull NodeRef sourceRef, @Nullable String sourceMD, @NotNull String meta,
                                                        @Nullable CustomMetadataType type) {

        NodeRef finalSource = sourceRef;
        String textToAdd = "";
        String fonction = "";

        if (sourceMD != null) {
            String[] keys = sourceMD.split("\\|");

            int i = 0;
            while (i < keys.length && finalSource != null) {

                if ("SHA256".equals(keys[i]) || "DELEGATION".equals(keys[i])) {
                    fonction = keys[i];
                } else if (EtapeCircuitImpl.ETAPE_TDT.equals(keys[i]) || EtapeCircuitImpl.ETAPE_SIGNATURE.equals(keys[i]) || EtapeCircuitImpl.ETAPE_VISA.equals(keys[i])) {
                    List<EtapeCircuit> circuit = parapheurService.getCircuit(finalSource);
                    // On récupère la dernière étape correspondant au mot clé (TDT ou SIGNATURE)
                    for (EtapeCircuit etape : circuit) {
                        if (keys[i].equals(etape.getActionDemandee())) {
                            if(etape.isApproved()) {
                                finalSource = etape.getNodeRef();
                            }
                        }
                    }
                } else {
                    String[] keys2 = keys[i].split(";");

                    if (keys2.length == 1) { // pas d'opération logique.
                        finalSource = (NodeRef) serviceRegistry.getNodeService().getProperty(finalSource, QName.createQName(keys[i], serviceRegistry.getNamespaceService()));
                    } else {
                        if ("OR".equals(keys2[0])) {
                            finalSource = applyOR(keys2, finalSource);
                        } else if ("IFF".equals(keys2[0])) {
                            finalSource = applyIFF(keys2, finalSource);
                        }
                    }
                }

                i++;
            }
        }

        QName metadata = QName.createQName(meta);
        String propertyValue = null;

        if (finalSource != null) {
            Serializable s = serviceRegistry.getNodeService().getProperty(finalSource, metadata);
            if(s instanceof Date) {
                propertyValue = new SimpleDateFormat("dd/MM/yyyy").format((Date) s);
            } else {
                propertyValue = java.lang.String.valueOf(s);
            }
        }

        if (propertyValue != null) {

            if ("SHA256".equals(fonction)) {
                try {
                    textToAdd = StringToSHA256(propertyValue);
                } catch (NoSuchAlgorithmException e) {
                    log.warn("Erreur lors de l'application de la fonction " + fonction +
                                "sur la métadonnée pour le calque de signature.", e);
                }
            } else if ("DELEGATION".equals(fonction)) {
                textToAdd = "Par délégation de " + propertyValue;
            } else {

                // Dates

                if ((type != null) && type.equals(CustomMetadataType.DATE) && NumberUtils.isNumber(propertyValue)) {
                    try { textToAdd = new SimpleDateFormat("dd/MM/yyyy").format(propertyValue); }
                    catch (IllegalArgumentException e) { textToAdd = propertyValue; }
                } else if((type != null) && type.equals(CustomMetadataType.BOOLEAN)) {
                    try { textToAdd = Boolean.valueOf(propertyValue) ? "Oui" : "Non"; }
                    catch (IllegalArgumentException e) { textToAdd = propertyValue; }
                } else {
                    textToAdd = propertyValue;
                }

                // User title

                if (meta.contentEquals(String.valueOf(ParapheurModel.PROP_VALIDATOR))) {

                    String delegueTitle = null;
                    String passeParTitle = null;
                    String validatorTitre = null;

                    NodeRef delegueNode = (NodeRef) serviceRegistry.getNodeService().getProperty(finalSource, ParapheurModel.PROP_DELEGUE);
                    NodeRef passeParNode = (NodeRef) serviceRegistry.getNodeService().getProperty(finalSource, ParapheurModel.PROP_PASSE_PAR);
                    NodeRef validatorNodeRef = serviceRegistry.getPersonService().getPerson(propertyValue);

                    if (delegueNode != null) {
                        delegueTitle = (String) serviceRegistry.getNodeService().getProperty(delegueNode, ContentModel.PROP_TITLE);
                    }

                    if (passeParNode != null) {
                        passeParTitle = (String) serviceRegistry.getNodeService().getProperty(passeParNode, ContentModel.PROP_TITLE);
                    }

                    if (validatorNodeRef != null) {
                        String validatorMetadata = (String) serviceRegistry.getNodeService().getProperty(validatorNodeRef, ContentModel.PROP_ORGID);
                        Map<String, String> validatorMetadataMap = CollectionsUtils.parseMetadata(validatorMetadata);
                        validatorTitre = validatorMetadataMap.get("TITRE");
                    }

                    textToAdd = StringUtils.firstNotEmpty(validatorTitre, delegueTitle, passeParTitle, propertyValue);
                }
            }
        }

        return textToAdd;
    }

    /**
     * Trouver le un noeud Alfresco a partir d'une requete lucene
     *
     * @param query : la requete a executer
     * @return Le NodeRef trouve
     */
    private NodeRef findNodeRef(String query) {
        NodeRef node = null;
        ResultSet resultat = serviceRegistry.getSearchService().query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE,
                SearchService.LANGUAGE_LUCENE, query);
        if ((resultat != null) && (resultat.length() > 0)) {
            node = resultat.getNodeRef(0);
        } else {
            log.error("Erreur lors de l'execution de la requete : " + query);
        }
        if (resultat != null) {
            resultat.close();
        }
        return node;
    }

    private String StringToSHA256(String propertyValue) throws NoSuchAlgorithmException {


        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(javax.xml.bind.DatatypeConverter.parseBase64Binary(propertyValue));
        byte[] byteData = md.digest();

        //convert the byte to hex format
        StringBuilder sb = new StringBuilder();
        for (byte byteDatum : byteData) {
            sb.append(Integer.toString((byteDatum & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    /**
     * Ajout des calques sur le fichier PDF
     *
     * @param listElements   : calques a ajouter
     * @param userSignature  : liste des signatures qui peuvent etre ajoutees dans le fichier
     * @param preview        : booleen indiquant si on est en mode previsualisation ou non
     * @param dossierRef     : le noeud du dossier, il me semble, non?
     * @return le fichier comprenant les elements des calques
     */
    private List<Stamp> createStampsFromCalques(List<ElementCalque> listElements, Map<Integer, NodeRef> userSignature, boolean preview, NodeRef dossierRef) {

        List<Stamp> stamps = new ArrayList<>();

        List<CustomMetadataDef> listeMetadataDefs;
        listeMetadataDefs = listElements.isEmpty() ? null : metadataService.getMetadataDefs();

        boolean isSigned = preview || parapheurService.isSigned(dossierRef);

        for (ElementCalque element : listElements) {
            Stamp el = addElementIntoPDF(element,userSignature, preview,
                    isSigned, dossierRef, listeMetadataDefs);
            if(el != null) {
                stamps.add(el);
            }
        }

        return stamps;
    }

    /**
     * Ajoute un element de calque dans le document PDF
     *
     * @param userSignature : liste des signatures scannees
     * @param preview       : indique si c'est un apercu ou non
     * @param dossierRef    : noeud du dossier correspondant
     */
    private Stamp addElementIntoPDF(@NotNull ElementCalque element, @Nullable Map<Integer, NodeRef> userSignature,
                                   boolean preview, boolean isSigned,
                                   @Nullable NodeRef dossierRef,
                                   @Nullable List<CustomMetadataDef> listeMetadataDefs) {

        // Test du type de donnees a ajouter

        Serializable sizeSerializable = serviceRegistry.getNodeService().getProperty(element.getNodeRef(), CalqueModel.PROP_TAILLE_POLICE);
        int fontSize = NumberUtils.toInt(String.valueOf(sizeSerializable), 12);

        Stamp stamp = new Stamp();
        stamp.setPage(element.getPage());
        stamp.setDrawRectangle(false);
        stamp.setOpacity(1);
        stamp.setFontSize(fontSize);

        Position position = new Position();
        position.setPlacement(Position.Placement.BOTTOM_LEFT);
        position.setX(Integer.parseInt(serviceRegistry.getNodeService().getProperty(element.getNodeRef(), CalqueModel.PROP_COORDONNEE_X).toString()));
        position.setY(Integer.parseInt(serviceRegistry.getNodeService().getProperty(element.getNodeRef(), CalqueModel.PROP_COORDONNEE_Y).toString()));

        stamp.setPosition(position);


        QName typeElement = serviceRegistry.getNodeService().getType(element.getNodeRef());

        // Filtering post-signature Calques, when the document is not signed

        boolean isPostSignature = Boolean.valueOf(String.valueOf(serviceRegistry.getNodeService().getProperty(element.getNodeRef(), CalqueModel.PROP_POSTSIG)));
        if (!isSigned && isPostSignature && !preview) {
            return null;
        }

        //

        if (typeElement.equals(CalqueModel.TYPE_COMMENTAIRE) || typeElement.equals(CalqueModel.TYPE_METADATA)) {
            stamp.setRows(Collections.singletonList(addTextElementIntoPDF(element, preview, dossierRef, listeMetadataDefs, typeElement)));
        } else if (typeElement.equals(CalqueModel.TYPE_IMAGE) || (typeElement.equals(CalqueModel.TYPE_SIGNATURE))) {
            stamp.setRows(Collections.singletonList(addImageElementIntoPdf(element, userSignature, preview,typeElement, position)));
        }

        return stamp;
    }

    private Row addTextElementIntoPDF(@NotNull ElementCalque element,
                                       boolean preview, @Nullable NodeRef dossierRef, @Nullable List<CustomMetadataDef> metadataDefsList,
                                       @NotNull QName typeElement) {

        NodeRef nodeRef = element.getNodeRef();
        // Default values

        String textToAdd = "Ajout de la valeur de la méta-donnée ici.";

        Serializable colorSerializable = serviceRegistry.getNodeService().getProperty(nodeRef, CalqueModel.PROP_COULEUR_TEXTE);
        String color = StringUtils.equalsIgnoreCase(String.valueOf(colorSerializable), "rouge") ? "red" : "black";

        // Retrieve Metadata Type

        CustomMetadataType metadataType = null;
        if (typeElement.equals(CalqueModel.TYPE_METADATA)) {

            String meta = serviceRegistry.getNodeService().getProperty(nodeRef, CalqueModel.PROP_QNAME_MD).toString();
            QName metadata = QName.createQName(meta);

            if (metadataDefsList != null) {
                for (CustomMetadataDef metadataDef : metadataDefsList) {
                    if (metadataDef.getName().equals(metadata)) {
                        metadataType = metadataDef.getType();
                        break;
                    }
                }
            }
        }

        // Retrieve text value

        if (typeElement.equals(CalqueModel.TYPE_COMMENTAIRE)) {
            textToAdd = serviceRegistry.getNodeService().getProperty(nodeRef, CalqueModel.PROP_TEXTE).toString();
        } else if (typeElement.equals(CalqueModel.TYPE_METADATA) && (!preview) && (dossierRef != null)) {
            String sourceMD = (String) serviceRegistry.getNodeService().getProperty(nodeRef, CalqueModel.PROP_SOURCE_MD);
            String meta = serviceRegistry.getNodeService().getProperty(nodeRef, CalqueModel.PROP_QNAME_MD).toString();
            textToAdd = retrieveTargetMetadataValue(dossierRef, sourceMD, meta, metadataType);
        }

        return createTextRow(textToAdd, color);
    }

    private Row createTextRow(String text, String color) {
        Row row = new Row();
        row.setColor(color);
        row.setTitle(text);
        return row;
    }

    private Row addImageElementIntoPdf(@NotNull ElementCalque element,
                                        @Nullable Map<Integer, NodeRef> userSignature, boolean preview, @NotNull QName typeElement, Position position) {

        Row row = new Row();
        Logo logo = new Logo();

        row.setLogo(logo);


        NodeRef nodeRef = element.getNodeRef();
        // Ajout d'elements de type image
        FileContentReader imageReader = null;
        String nomImage = "";

        if (typeElement.equals(CalqueModel.TYPE_IMAGE)) {
            // Ajout d'une image
            imageReader = (FileContentReader) serviceRegistry.getContentService().getReader(nodeRef, CalqueModel.PROP_CONTENT_IMAGE);
            nomImage = serviceRegistry.getNodeService().getProperty(nodeRef, CalqueModel.PROP_NOM_IMAGE).toString();
        } else if (typeElement.equals(CalqueModel.TYPE_SIGNATURE)) {

            // Ajout d'une signature
            if (preview) {
                // Rechercher l'image de signature par defaut
                String querySignature = "PATH:\"" + CalqueModel.PATH_DEFAULT_SIGNATURE + "\"";
                NodeRef nodeImage = findNodeRef(querySignature);
                imageReader = (FileContentReader) serviceRegistry.getContentService().getReader(nodeImage, ContentModel.PROP_CONTENT);
                nomImage = serviceRegistry.getNodeService().getProperty(nodeImage, ContentModel.PROP_NAME).toString();

            } else if (userSignature != null) {

                // Parcourir la liste des userSignatures pour prendre la signature correspondant au rang de l'element
                Integer leRang = (Integer) serviceRegistry.getNodeService().getProperty(nodeRef, CalqueModel.PROP_RANG);
                NodeRef nodeUser = userSignature.get(leRang);
                if (log.isDebugEnabled()) {
                    log.info("Sig de rang " + leRang);
                    log.debug("  Taille de map des signatures scannées: " + userSignature.size());
                    log.debug("  Liste des rangs dans la map des signs: " + userSignature.keySet().toString());
                }

                if (nodeUser != null) {

                    if (serviceRegistry.getNodeService().hasAspect(nodeUser, ParapheurModel.ASPECT_SIGNATURESCAN)) {
                        List<ChildAssociationRef> children = serviceRegistry.getNodeService().getChildAssocs(nodeUser);
                        for (ChildAssociationRef child : children) {
                            if (child.getTypeQName().equals(ParapheurModel.ASSOC_SIGNATURE_SCAN)) {
                                imageReader = (FileContentReader) serviceRegistry.getContentService().getReader(child.getChildRef(), ContentModel.PROP_CONTENT);
                            }
                        }

                    }

                    //imageReader = getContentService().getReader(nodeUser, ParapheurModel.PROP_USER_SIGNATURE);
                    if (imageReader == null) {
                        log.warn("Pas de signature scannée associée avec l'utilisateur");
                    } else {
                        position.setCentered(true);
                        position.setOnText(getSignatureTag(leRang));
                    }
                    nomImage = serviceRegistry.getNodeService().getProperty(nodeUser, ContentModel.PROP_USERNAME).toString();
                } else {
                    log.warn("Signataire de rang '" + leRang + "' introuvable??");
                }

            }
        }

        try {
            if (!nomImage.equals("") && imageReader != null) {
                // Creer un fichier temporaire de l'image
                File imageFile = new File(nomImage);
                imageReader.getContent(imageFile);
                // Recuperer l'image
                Image image = Image.getInstance(nomImage);
                if (typeElement.equals(CalqueModel.TYPE_SIGNATURE)) {

                    // Manage and rescale properly HD images
                    if (image.getDpiX() > 72 && image.getDpiY() > 72) {
                        float densityRatioX = 72.0f / image.getDpiX();
                        float densityRatioY = 72.0f / image.getDpiY();
                        image.scaleAbsolute(image.getScaledWidth() * densityRatioX, image.getScaledHeight() * densityRatioY);
                    }

                    // Resize if signature is too big
                    if (image.getScaledWidth() > 250.0f || image.getScaledWidth() > 200.0f) {
                        image.scaleToFit(250.0f, 200.0f);
                    }
                }


                logo.setData(new String(Base64.getEncoder().encode(IOUtils.toByteArray(new FileInputStream(imageFile)))));
                logo.setWidth((int) image.getScaledWidth());
                position.setWidth((int) image.getScaledWidth());
                position.setHeight((int) image.getScaledHeight() + 5);

                // Cleanup

                boolean tempFileDeleted = imageFile.delete();
                if (!tempFileDeleted) {
                    log.warn("Could not delete temporary file : " + imageFile.getAbsolutePath());
                }
            }

        } catch (BadElementException | IOException elementException) {
            log.error("Erreur lors de la lecture de l'image.", elementException);
        }

        return row;
    }

    private File createEmptyPdfFile(@NotNull String sourcePath, int pageNumber) {

        Document document = new Document();
        ByteArrayOutputStream byteArrayOutputSteam = new ByteArrayOutputStream();
        PdfWriter pdfwriter;

        try {
            pdfwriter = PdfWriter.getInstance(document, byteArrayOutputSteam);
            pdfwriter.setPdfVersion(PdfWriter.VERSION_1_7);
        } catch (DocumentException e) {
            log.warn("Error creating empty PDF : " + e.getLocalizedMessage());
            return null;
        }

        // Printing every page

        document.open();
        for (int i=0; i<pageNumber; i++) {
            document.newPage();
            pdfwriter.setPageEmpty(false);
        }
        document.close();

        // Write file on disk

        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(sourcePath);
            fileOutputStream.write(byteArrayOutputSteam.toByteArray());
            fileOutputStream.close();
        } catch (IOException e) {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e1) {
                    log.warn("Error closing empty PDF file stream : " + e1.getLocalizedMessage());
                }
            }
        }

        // Cleanup

        try {
            byteArrayOutputSteam.close();
        } catch (IOException e) {
            log.warn("Error closing empty PDF data stream : " + e.getLocalizedMessage());
        }

        pdfwriter.close();

        return new File(sourcePath);
    }

    private String getSignatureTag(int numSignature) {
        // Depuis la 4.3, on peut définir un tag de signature par tenant... On tente de le récupérer ici !
        String sigTag = signatureTag;
        String domain = tenantService.getCurrentUserDomain();
        if(!domain.isEmpty() && signatureTagMap.containsKey(domain)) {
            sigTag = signatureTagMap.get(domain);
        }

        return sigTag.replace("%n", String.valueOf(numSignature));
    }

    @Override
    public TagSearchResult scanForTag(String path, boolean cachet, int numSignature) {
        // Depuis la 4.3, on peut définir un tag de signature par tenant... On tente de le récupérer ici !
        String sigTag = cachet ? cachetTag : signatureTag;
        Map<String, String> tagMap = cachet ? cachetTagMap : signatureTagMap;
        String domain = tenantService.getCurrentUserDomain();
        if(!domain.isEmpty() && tagMap.containsKey(domain)) {
            sigTag = tagMap.get(domain);
        }

        return new PopplerUtils().getTextLocation(path, sigTag.replace("%n", String.valueOf(numSignature)));
    }

    // </editor-fold desc="Methodes privees">

    @Override
    public NodeRef previewCalque(String idCalque) {

        // Liste des calques
        List<NodeRef> listCalques = new ArrayList<>();
        listCalques.add(new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, idCalque));

        // Creer le noeud du document PDF
        ChildAssociationRef child = serviceRegistry.getNodeService().createNode(getEspacePreview(), ContentModel.ASSOC_CONTAINS,
                ContentModel.ASSOC_CONTAINS, ContentModel.PROP_CONTENT);
        NodeRef nodePDF = child.getChildRef();

        // Creation du document ItextPDF vierge
        String filename = nodePDF.getId() + "_preview.pdf";

        List<ElementCalque> listElements = getListElements(listCalques);

        // Creer un fichier local qui contient le contenu du fichier d'origine
        StampList stampList = new StampList();
        stampList.setStamps(createStampsFromCalques(listElements, null, true, null));

        int maximumPage = 1;

        if (!listElements.isEmpty() && listElements.get(listElements.size() - 1).getPage() > 0) {
            maximumPage = listElements.get(listElements.size() - 1).getPage();
        }

        File filePDF = createEmptyPdfFile(filename, maximumPage);
        try {
            File stampedPdf = pdfStampService.doStamp(filePDF, stampList);
            if(stampedPdf != null) filePDF = stampedPdf;
        } catch (StampException e) {
            e.printStackTrace();
        }

        if(filePDF != null) {
            // Ecrire dans le noeud PDF
            ContentWriter fileWriter = serviceRegistry.getContentService().getWriter(nodePDF, ContentModel.PROP_CONTENT, true);
            fileWriter.setMimetype(serviceRegistry.getMimetypeService().guessMimetype(filename));
            fileWriter.putContent(filePDF);

            boolean isTempFileDeleted = filePDF.delete();
            if (!isTempFileDeleted) {
                log.warn("Error deleting temp preview PDF file : " + filePDF.getAbsolutePath());
            }
        }

        return nodePDF;
    }

    private NodeRef getNodeRefWithXPath(String xpath) {
        List<NodeRef> results = serviceRegistry.getSearchService().selectNodes(
                serviceRegistry.getNodeService().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                xpath,
                null,
                serviceRegistry.getNamespaceService(),
                false);

        if (results.isEmpty()) {
            throw new IllegalArgumentException("No Results for query: " + xpath);
        }

        return results.get(0);
    }

    /**
     * Cree un fichier en local a partir d'un noeud Alfresco
     *
     * @param node        : Le noderef du document Alfresco
     * @param extension   : l'extension du fichier destination
     * @param typeContent : le type du contenu Alfresco
     * @return Le nom du fichier cree en local
     */
    private String createFileFromAlfresco(NodeRef node, String extension, QName typeContent) {
        String fileName = node.getId() + extension;
        File file = new File(fileName);
        ContentReader fileReader = serviceRegistry.getContentService().getReader(node, typeContent);
        fileReader.getContent(file);
        return fileName;
    }

    /**
     * Supprime un fichier en local
     *
     * @param fileName : le nom du fichier local
     */
    private void deleteLocalFile(String fileName) {
        File file = new File(fileName);
        file.delete();
    }


    /**
     * Recupere un SousType a partir de son ID depuis un fichier XML
     *
     * @param xmlFile fichier contenant les sousTypeMetier
     * @param soutype le nom du sousType recherche
     * @return Un element dom4j contenant toutes les balises de ce noeud
     */
    private org.dom4j.Element getSousTypeMetierFromXML(String xmlFile, String soutype) {
        SAXReader saxReader = new SAXReader();
        org.dom4j.Document document;
        org.dom4j.Element elementSousType = null;
        try {
            document = saxReader.read(xmlFile);
            List<org.dom4j.Node> nodes = document.selectNodes("//MetierSousTypes/MetierSousType");
            for (org.dom4j.Node node : nodes) {
                org.dom4j.Element element = (org.dom4j.Element) node;
                if (element.elementText("ID").equals(soutype)) {
                    elementSousType = element;
                }
            }
        } catch (org.dom4j.DocumentException e) {
            log.error("Document XML non trouvable ! " + e.getLocalizedMessage());
        }
        return elementSousType;
    }

    /**
     * Rcupere le circuit associe au dossier
     *
     * @param el : element dom4j contenant le sousType
     * @return : le Nom du circuit
     */
    private String getCircuitFromSousType(org.dom4j.Element el) {
        return el.elementText("Circuit");
    }

    /**
     * Recupere la liste des calques associes au SousType
     *
     * @param el element dom4j contenant le sousType
     * @return la list des calques
     */
    private @NotNull ArrayList<NodeRef> getListCalquesFromSousType(@NotNull org.dom4j.Element el, boolean forAnnexes) {

        ArrayList<NodeRef> listCalque = new ArrayList<>();
        org.dom4j.Element elementCalque = el.element(forAnnexes ? "calquesAnnexes" : "calques");

        if (elementCalque != null) {
            List<org.dom4j.Element> listElementsCalques = elementCalque.elements("calque");

            for (org.dom4j.Element element : listElementsCalques) {
                NodeRef calqueNodeRef = new NodeRef(element.getTextTrim());
                listCalque.add(calqueNodeRef);
            }
        }

        return listCalque;
    }

    /**
     * @param idDossier
     * @return Generated PDF node
     * @deprecated
     */
    @Override
    public NodeRef printDossier(String idDossier) {

        // <editor-fold defaultstate="collapsed" desc="Folding Deprecated method">
        // Récupérer le NodeRef du dossier
        NodeRef nodeDossier = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, idDossier);

        // Déclaration des Types et Sous Types Metier
        String typeMetier = serviceRegistry.getNodeService().getProperty(nodeDossier, ParapheurModel.PROP_TYPE_METIER).toString();
        String sousTypeMetier = serviceRegistry.getNodeService().getProperty(nodeDossier, ParapheurModel.PROP_SOUSTYPE_METIER).toString();
        NodeRef xmlTypeMetier = getNodeRefWithXPath("/app:company_home/app:dictionary/cm:metiersoustype"
                + "/*[@cm:name='" + typeMetier + ".xml']");
        // Trouver le fichier XML du ssType à partir du TypeMetier
        //NodeRef xmlTypeMetier = findNodeRef("@cm\\:name:\"" + typeMetier + ".xml\"");

        // Lire le fichier a partir d'Alfresco
        String xmlSousTypeFileName = createFileFromAlfresco(xmlTypeMetier, ".xml", ContentModel.PROP_CONTENT);

        // Recuperer l'element correspondant au sous type recherche
        org.dom4j.Element sousTypeElement = getSousTypeMetierFromXML(xmlSousTypeFileName, sousTypeMetier);

        // Puis recuperer la liste des calques
        // utiliser type service ?!
        List<NodeRef> listCalques = getListCalquesFromSousType(sousTypeElement, false);

        // Recuperer le nom du circuit
        String circuitName = getCircuitFromSousType(sousTypeElement);

        NodeRef circuitNode = workflowService.getWorkflowByName(circuitName);

        NodeRef emetteur = parapheurService.getEmetteur(nodeDossier);
        SavedWorkflow workflow = workflowService.getSavedWorkflow(circuitNode, true, emetteur, nodeDossier);

        List<EtapeCircuit> etapes = workflow.getCircuit();
        Map<Integer, NodeRef> parapheurAvecRangSignature = new HashMap<>();

        int _rang = 0;
        for (EtapeCircuit etape : etapes) {
            if (etape.getActionDemandee().equals(EtapeCircuit.ETAPE_SIGNATURE)) {
                parapheurAvecRangSignature.put(_rang, etape.getParapheur());
                _rang++;
            }
        }

        Map<Integer, NodeRef> userSignature = new HashMap<>();
        for (Integer rang : parapheurAvecRangSignature.keySet()) {
            NodeRef parapheur = parapheurAvecRangSignature.get(rang);
            // recuperer le userName proprietaire du parapheur
            String userName = serviceRegistry.getNodeService().getProperty(parapheur, ParapheurModel.PROP_PROPRIETAIRE_PARAPHEUR).toString();
            NodeRef proprietaire = serviceRegistry.getPersonService().getPerson(userName);
            userSignature.put(rang, proprietaire);
        }

        // Recuperer le fichier
        List<ChildAssociationRef> children = serviceRegistry.getNodeService().getChildAssocs(nodeDossier);
        NodeRef documentOrigine = children.get(1).getChildRef();

        // Creer un fichier local qui contient le contenu du fichier d'origine
        String filename = createFileFromAlfresco(documentOrigine, ".pdf", ContentModel.PROP_CONTENT);
        File filePDF = new File(documentOrigine.getId() + "_print.pdf");

        List<ElementCalque> listElements = getListElements(listCalques);

        // Creer un fichier local qui contient le contenu du fichier d'origine
        StampList stampList = new StampList();
        stampList.setStamps(createStampsFromCalques(listElements, userSignature, false, nodeDossier));

        try {
            File stampPdf = pdfStampService.doStamp(filePDF, stampList);
            if(stampPdf != null) filePDF = stampPdf;
        } catch (StampException e) {
            e.printStackTrace();
        }

        // Creer le noeud du document PDF (celui qui va etre proposé à l'utilisateur pour l'impression)
        ChildAssociationRef child = serviceRegistry.getNodeService().createNode(getEspacePreview(), ContentModel.ASSOC_CONTAINS,
                ContentModel.ASSOC_CONTAINS, ContentModel.PROP_CONTENT);
        NodeRef nodePDF = child.getChildRef();

        // Ecrire dans le noeud PDF
        ContentWriter fileWriter = serviceRegistry.getContentService().getWriter(nodePDF, ContentModel.PROP_CONTENT, true);
        fileWriter.setMimetype(serviceRegistry.getMimetypeService().guessMimetype(filename));
        fileWriter.putContent(filePDF);

        // Supprimer le fichier, normalement il est placer dans le alf_home
        filePDF.delete();
        deleteLocalFile(filename);
        deleteLocalFile(xmlSousTypeFileName);

        // Retourne le PDF avec les calques
        return nodePDF;
// </editor-fold>
    }

    @Override
    public File applyClaques(NodeRef dossier, File doc, List<NodeRef> listCalques) throws StampException {
        List<EtapeCircuit> etapes = parapheurService.getCircuit(dossier);

        Map<Integer, NodeRef> userSignature = prepareUserSignatures(etapes, dossier);

        List<ElementCalque> listElements = getListElements(listCalques);

        // Creer un fichier local qui contient le contenu du fichier d'origine
        StampList stampList = new StampList();
        stampList.setStamps(createStampsFromCalques(listElements, userSignature, false, dossier));

        return pdfStampService.doStamp(doc, stampList);
    }

    private List<ElementCalque> getListElements(List<NodeRef> listCalques) {
        List<ElementCalque> listElements = new ArrayList<>();
        for (NodeRef calque : listCalques) {
            if(serviceRegistry.getNodeService().exists(calque)) { // It might have been deleted since
                listElements.addAll(calqueService.getElementsForCalque(calque));
            }
        }

        Collections.sort(listElements);

        return listElements;
    }

    private Map<Integer, NodeRef> prepareUserSignatures(List<EtapeCircuit> etapes, NodeRef dossier) {
        Map<Integer, NodeRef> userSignature = new HashMap<>();

        Map<Integer, EtapeCircuit> etapeAvecRangSignature = new HashMap<>();

        /*
         * Releve les etapes de signature effectuees, et note les rangs
         */
        int _rang = 0;
        for (EtapeCircuit etape : etapes) {
            if (!etape.isApproved()) {
                break;
            }
            if (etape.getActionDemandee().equals(EtapeCircuit.ETAPE_SIGNATURE)) {
                etapeAvecRangSignature.put(_rang, etape);
                _rang++;
            }
        }

        for (Integer rang : etapeAvecRangSignature.keySet()) {
            // recuperer le userName du proprietaire du parapheur qui à validé l'étape.

            // String statutMetier = (String)serviceRegistry.getNodeService().getProperty(dossier, ParapheurModel.PROP_STATUS_METIER);

            // Sur le dernier 'rang' il y a un décalage de 1 par rapport à _rang
            // En cas de rejet sur cette étape, on n'applique pas de SCAN.
            if (rang == (_rang - 1)
                    && ((String) serviceRegistry.getNodeService().getProperty(dossier, ParapheurModel.PROP_STATUS_METIER)).startsWith("Rejet")) {
                break;
            } else {
                String userName = etapeAvecRangSignature.get(rang).getValidator();
                NodeRef proprietaire = serviceRegistry.getPersonService().getPerson(userName);
                userSignature.put(rang, proprietaire);
            }
        }
        return userSignature;
    }

}
