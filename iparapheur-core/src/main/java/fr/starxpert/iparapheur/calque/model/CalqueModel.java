/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2011, StarXpert, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by StarXpert and ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.starxpert.iparapheur.calque.model;

import org.alfresco.service.namespace.QName;

public interface CalqueModel {

    String CALQUE_MODEL_URI = "http://www.starxpert.fr/alfresco/model/calque/1.0";

    String CALQUE_MODEL_PREFIX = "cal";

    String PATH_CALQUE = "/app:company_home/app:dictionary/cal:calques";
    String PATH_CALQUE_PREVIEW = "/app:company_home/app:dictionary/cal:calques/cal:previewCalque";
    String PATH_DEFAULT_SIGNATURE = "/app:company_home/app:dictionary/cal:calques/cal:previewCalque/cal:defaultSignature";

    //------- Declaration des types

    /* Type Coordonnees */
    QName TYPE_COORDONNEES = QName.createQName(CALQUE_MODEL_URI, "coordonnees");
    QName PROP_COORDONNEE_X = QName.createQName(CALQUE_MODEL_URI, "coordonneeX");
    QName PROP_COORDONNEE_Y = QName.createQName(CALQUE_MODEL_URI, "coordonneeY");
    QName PROP_PAGE = QName.createQName(CALQUE_MODEL_URI, "page");
    QName PROP_POSTSIG = QName.createQName(CALQUE_MODEL_URI, "postSignature");

    /* Type Signature */
    QName TYPE_SIGNATURE = QName.createQName(CALQUE_MODEL_URI, "signature");
    QName PROP_RANG = QName.createQName(CALQUE_MODEL_URI, "rang");

    /* Type Commentaire */
    QName TYPE_COMMENTAIRE = QName.createQName(CALQUE_MODEL_URI, "commentaire");
    QName PROP_TEXTE = QName.createQName(CALQUE_MODEL_URI, "texte");
    QName PROP_TAILLE_POLICE = QName.createQName(CALQUE_MODEL_URI, "taillePolice");
    QName PROP_COULEUR_TEXTE = QName.createQName(CALQUE_MODEL_URI, "couleurTexte");

    /* Type Image */
    QName TYPE_IMAGE = QName.createQName(CALQUE_MODEL_URI, "image");
    QName PROP_NOM_IMAGE = QName.createQName(CALQUE_MODEL_URI, "nomImage");
    QName PROP_CONTENT_IMAGE = QName.createQName(CALQUE_MODEL_URI, "contenuImage");

    /* Type MetaData */
    QName TYPE_METADATA = QName.createQName(CALQUE_MODEL_URI, "metaData");
    QName PROP_QNAME_MD = QName.createQName(CALQUE_MODEL_URI, "qnameMD");
    QName PROP_SOURCE_MD = QName.createQName(CALQUE_MODEL_URI, "sourceMD");

    /* Type Calque */
    QName TYPE_CALQUE = QName.createQName(CALQUE_MODEL_URI, "calque");
    QName PROP_NOM_CALQUE = QName.createQName(CALQUE_MODEL_URI, "nomCalque");
    QName ASSOC_CALQUE = QName.createQName(CALQUE_MODEL_URI, "elementsCalque");
}
