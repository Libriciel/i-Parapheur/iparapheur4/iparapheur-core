/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2011, StarXpert, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by StarXpert and ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package fr.starxpert.iparapheur.calque.repo;

import fr.starxpert.iparapheur.calque.repo.impl.ElementCalque;
import java.util.List;

import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.extensions.surf.util.InputStreamContent;

/**
 * @author cpiassale - StarXpert
 *
 * Interface permettant de creer les calques
 */
public interface CalqueService {

    /**
     * Recherche de l'espace "Calques" qui contiendra tous les calques de l'application
     * @return le noeud de l'espace "Calques"
     */
    public NodeRef getEspaceCalques();

    /**
     * Creation du calque
     * @param nomCalque : nom du nouveau calque
     * @return le noeud du calque cree
     */
    public NodeRef creerCalque(String nomCalque);

    /**
     * Creation d'un nouveau calque a partir d'un autre
     * @param idCalque : uuid du noeud a copier
     * @return le nouveau noeud du calque
     */
    public NodeRef copyCalque(String idCalque);

    /**
     * Suppression du calque
     * @param idCalque : uuid du noeud du calque a supprimer
     */
    public void suppressionCalque(String idCalque);

    /**
     * Ajout des coordonnees
     * @param element : noeud de l'element qui peut etre
     *         - une signature
     *         - un commentaire
     *         - une image
     *         - une meta donnee
     * @param x : coordonnee x de l'emplacement de l'element dans la page PDF (reference d'une page A4 en mode paysage)
     * @param y : coordonnee y de l'emplacement de l'element dans la page PDF (reference d'une page A4 en mode paysage)
     * @param numeroPage : page du PDF
     * @param postSignature : element à ajouter seulement en cas de signature
     */
    public void addCoordonnees(NodeRef element, float x, float y, int numeroPage, boolean postSignature);

    /**
     * Ajout d'une signature au calque
     * @param idCalque : uuid du noeud du calque parent
     * @param rang : rang de la signature
     * @return le noeud de la signature
     */
    public NodeRef addSignature(String idCalque, int rang);

    /**
     * Ajout d'un commentaire au calque
     * @param idCalque : uuid du noeud du calque parent
     * @param commentaire : texte du commentaire
     * @return le noeud du commentaire
     */
    //public NodeRef addCommentaire(final String idCalque, final String commentaire);
    public NodeRef addCommentaire(final String idCalque, final String commentaire, final int taillePolice, final String coulText);

    /**
     * Ajout d'une image au calque
     * @param idCalque : uuid du noeud du calque parent
     * @param fichierImage : fichier à importer dans alfresco
     * @return le noeud de l'image
     */
    public NodeRef addImage(String idCalque, String NomeImage, String fichierImage);

    /**
     * Ajout d'une meta-donnee au calque
     * @param idCalque : uuid du noeud du calque parent
     * @param qname : qname de la meta-donnee
     * @return le noeud de la meta-donnee
     */
    public NodeRef addMetadata(String idCalque, String qname, int fontSize);

    /**
     * Liste les signatures associees a un calque
     * @param idCalque : uuid du noeud du calque
     * @return la liste des noeuds des signatures
     */
    public List<NodeRef> listSignatures(String idCalque);

    /**
     * Liste les commentaires associes a un calque
     * @param idCalque : uuid du noeud du calque
     * @return la liste des noeuds des commentaires
     */
    public List<NodeRef> listCommentaires(String idCalque);

    /**
     * Liste les images associees a un calque
     * @param idCalque : uuid du noeud du calque
     * @return la liste des noeuds des images
     */
    public List<NodeRef> listImages(String idCalque);

    /**
     * Liste les meta-donnees associees a un calque
     * @param idCalque : uuid du noeud du calque
     * @return la liste des noeuds des meta-donnees
     */
    public List<NodeRef> listMetadata(String idCalque);

    public List<ElementCalque> getElementsForCalque(NodeRef calque);

    /**
     * Suppression de la signature
     * @param idSignature : uuid du noeud de la signature
     */
    public void suppressionSignature(String idSignature);

    /**
     * Suppression du commentaire
     * @param idCommentaire : uuid du noeud du commentaire
     */
    public void suppressionCommentaire(String idCommentaire);

    /**
     * Suppression de l'image
     * @param idImage : uuid du noeud de l'image
     */
    public void suppressionImage(String idImage);

    /**
     * Suppression de la meta-donnee
     * @param idMetaDa : uuid du noeud de la meta-donnee
     */
    public void suppressionMetadata(String idMetaDa);

}
