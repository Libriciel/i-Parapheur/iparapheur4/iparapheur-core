/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2011, StarXpert, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by StarXpert and ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.starxpert.iparapheur.calque.repo.impl;

import fr.starxpert.iparapheur.calque.model.CalqueModel;
import fr.starxpert.iparapheur.calque.repo.CalqueService;
import org.adullact.utils.StringUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.template.BaseTemplateProcessorExtension;
import org.alfresco.service.cmr.dictionary.InvalidTypeException;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.extensions.surf.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.*;


/**
 * @author cpiassale
 *         StarXpert
 *         <p/>
 *         Classe implementant le CalqueService
 */
public class CalqueServiceImpl extends BaseTemplateProcessorExtension implements CalqueService {

    /* logger */
    private static Logger logger = Logger.getLogger(CalqueServiceImpl.class);

    /* Services */
    NodeService nodeService;
    SearchService searchService;
    CopyService copyService;
    ContentService contentService;
    MimetypeService mimetypeService;
    NamespaceService namespaceService;

    private StoreRef storeRef;

    // <editor-fold desc="Private methods">

    /**
     * Recherche des fils d'un calque par type
     *
     * @param parent : noeud du calque
     * @param type   : qname du type du fils recherche
     *               - signature
     *               - commentaire
     *               - image
     *               - metaData
     * @return
     */
    private List<NodeRef> getChildrenByType(NodeRef parent, QName type) {
        // Definir les types recherches
        Set<QName> types = new TreeSet<QName>();
        types.add(type);
        if (logger.isDebugEnabled()) {
            logger.debug("Type de fils recherché : " + type.toString());
        }

        List<NodeRef> nodeChildren = new ArrayList<NodeRef>();
        if (parent != null) {
            // Recherche des fils
            List<ChildAssociationRef> children = getNodeService().getChildAssocs(parent, types);

            // Creation de la liste des fils
            for (ChildAssociationRef child : children) {
                nodeChildren.add(child.getChildRef());
            }
        } else {
            logger.error("GetChildrenByType - Le noeud parent n'existe pas.");
        }

        return nodeChildren;
    }

    private Integer getPageForCalqueElement(NodeRef calqueElement) {
        Serializable s_page = getNodeService().getProperty(calqueElement, CalqueModel.PROP_PAGE);
        return new Integer(s_page.toString());
    }

    // </editor-fold desc="Private methods">

    @Override
    public NodeRef getEspaceCalques() {
        NodeRef spaceCalque = null;
        String query = "PATH:\"" + CalqueModel.PATH_CALQUE + "\"";
        ResultSet resultat = getSearchService().query(storeRef, SearchService.LANGUAGE_LUCENE, query);

        if ((resultat != null) && (resultat.length() > 0)) {
            spaceCalque = resultat.getNodeRef(0);
        } else {
            logger.error("Impossible d'ajouter un calque : le dossier \"Calques\" n'existe pas.");
        }

        if (resultat != null) {
            resultat.close();
        }

        return spaceCalque;
    }

    @Override
    public NodeRef creerCalque(String nomCalque) {
        // Recherche du repertoire parent des calques
        NodeRef calques = getEspaceCalques();

        // Creation du noeud calque vierge pour le moment
        NodeRef calque = null;
        try {
            // On crée concrétement le calque à partir du "calqueHome"
            ChildAssociationRef child = getNodeService().createNode(calques, ContentModel.ASSOC_CONTAINS,
                    ContentModel.ASSOC_CONTAINS, CalqueModel.TYPE_CALQUE);
            calque = child.getChildRef();

            // Proprietes par defaut, le nom du calque
            getNodeService().setProperty(calque, CalqueModel.PROP_NOM_CALQUE, nomCalque);
        } catch (InvalidNodeRefException invalidNodeRef) {
            logger.error("Le noeud parent est invalide.", invalidNodeRef);
        } catch (InvalidTypeException invalidType) {
            logger.error("La référence du type du noeud n'est pas reconnu.", invalidType);
        }

        return calque;
    }

    @Override
    public NodeRef copyCalque(String idCalque) {
        // Recuperation du noeud du calque à partir de l'id
        NodeRef nodeCalqueToCopy = new NodeRef(storeRef, idCalque);
        if (logger.isDebugEnabled()) {
            logger.debug("Id du calque à copier : " + idCalque);
        }
        // Creation d'un nouveau calque
        NodeRef newNode = getCopyService().copyAndRename(nodeCalqueToCopy, getEspaceCalques(), ContentModel.ASSOC_CONTAINS,
                ContentModel.ASSOC_CONTAINS, true);

        return newNode;
    }

    @Override
    public void suppressionCalque(String idCalque) {
        // Recuperation du noeud du calque
        NodeRef nodeCalque = new NodeRef(storeRef, idCalque);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque à supprimer : " + idCalque);
        }

        // Suppression du noeud
        getNodeService().deleteNode(nodeCalque);
    }

    @Override
    public void addCoordonnees(NodeRef element, float x, float y, int numeroPage, boolean postSignature) {
        // Si la coordonnee x n'est pas indique la valeur par defaut sera : 12 (en cm)
        if (x == 0) {
            x = 12;
        }
        // Si la coordonnee y n'est pas indique la valeur par defaut sera : 24 (en cm)
        if (y == 0) {
            y = 24;
        }

        // Creation de la map de proprietes
        Map<QName, Serializable> proprietes = getNodeService().getProperties(element);
        proprietes.put(CalqueModel.PROP_COORDONNEE_X, x);
        proprietes.put(CalqueModel.PROP_COORDONNEE_Y, y);
        proprietes.put(CalqueModel.PROP_PAGE, numeroPage);
        proprietes.put(CalqueModel.PROP_POSTSIG, postSignature);
        getNodeService().setProperties(element, proprietes);
    }

    @Override
    public NodeRef addSignature(String idCalque, int rang) {
        // Creation de la map de proprietes
        Map<QName, Serializable> proprietes = new HashMap<QName, Serializable>();
        proprietes.put(CalqueModel.PROP_RANG, rang);

        // Recuperation du noeud du calque
        NodeRef nodeCalque = new NodeRef(storeRef, idCalque);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque à enrichir : " + idCalque);
        }

        // Creation du noeud signature
        NodeRef signature = null;
        try {
            ChildAssociationRef child = getNodeService().createNode(nodeCalque, CalqueModel.ASSOC_CALQUE,
                    CalqueModel.ASSOC_CALQUE, CalqueModel.TYPE_SIGNATURE, proprietes);
            signature = child.getChildRef();
        } catch (InvalidNodeRefException invalidNodeRef) {
            logger.error("Le noeud parent est invalide.", invalidNodeRef);
        } catch (InvalidTypeException invalidType) {
            logger.error("La référence du type du noeud n'est pas reconnu.", invalidType);
        }

        // retourne le noeud de la signature qui vient d'etre cree
        return signature;
    }

    @Override
    public NodeRef addCommentaire(String idCalque, String texte, int taillePolice, String coulText) {

        // Creation de la map de proprietes
        Map<QName, Serializable> proprietes = new HashMap<QName, Serializable>();
        proprietes.put(CalqueModel.PROP_TEXTE, texte);
        proprietes.put(CalqueModel.PROP_TAILLE_POLICE, taillePolice);
        proprietes.put(CalqueModel.PROP_COULEUR_TEXTE, StringUtils.equalsIgnoreCase(coulText.trim(), "rouge") ? "rouge" : "noir");

        // Recuperation du noeud du calque
        NodeRef nodeCalque = new NodeRef(storeRef, idCalque);
        logger.debug("Id calque à enrichir : " + idCalque);

        // Creation du noeud commentaire
        NodeRef commentaire = null;
        try {
            ChildAssociationRef child = getNodeService().createNode(nodeCalque, CalqueModel.ASSOC_CALQUE,
                    CalqueModel.ASSOC_CALQUE, CalqueModel.TYPE_COMMENTAIRE, proprietes);
            commentaire = child.getChildRef();
        } catch (InvalidNodeRefException invalidNodeRef) {
            logger.error("Le noeud parent est invalide.", invalidNodeRef);
        } catch (InvalidTypeException invalidType) {
            logger.error("La référence du type du noeud n'est pas reconnu.", invalidType);
        }

        // retourne le noeud du commentaire qui vient d'etre cree
        return commentaire;
    }

    @Override
    public NodeRef addImage(String idCalque, String NomeImage, String fichierImage) {
        // Creation de la map de proprietes
        Map<QName, Serializable> proprietes = new HashMap<QName, Serializable>();
        proprietes.put(CalqueModel.PROP_NOM_IMAGE, NomeImage);

        // Recuperation du noeud du calque
        NodeRef nodeCalque = new NodeRef(storeRef, idCalque);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque à enrichir : " + idCalque);
        }

        InputStream is = new ByteArrayInputStream(Base64.decode(fichierImage));

        NodeRef image = null;
        try {
            // Creation du noeud image
            ChildAssociationRef child = getNodeService().createNode(nodeCalque, CalqueModel.ASSOC_CALQUE,
                    CalqueModel.ASSOC_CALQUE, CalqueModel.TYPE_IMAGE, proprietes);
            image = child.getChildRef();
            // Ajout du contenu de l'image
            ContentWriter writer = contentService.getWriter(image, CalqueModel.PROP_CONTENT_IMAGE, true);
            writer.putContent(is);

        } catch (InvalidNodeRefException invalidNodeRef) {
            logger.error("Le noeud parent est invalide.", invalidNodeRef);
        } catch (InvalidTypeException invalidType) {
            logger.error("La référence du type du noeud n'est pas reconnu.", invalidType);
        }

        // retourne le noeud de l'image qui vient d'etre cree
        return image;
    }

    @Override
    public NodeRef addMetadata(String idCalque, String value, int fontSize) {

        // Creation de la map de proprietes
        Map<QName, Serializable> proprietes = new HashMap<QName, Serializable>();
        int pipeLastOccurence = value.lastIndexOf("|");
        String qname = value;
        if (pipeLastOccurence != -1) {
            qname = qname.substring(pipeLastOccurence + 1);
            proprietes.put(CalqueModel.PROP_SOURCE_MD, value.substring(0, pipeLastOccurence));
        }

        proprietes.put(CalqueModel.PROP_TAILLE_POLICE, fontSize);
        proprietes.put(CalqueModel.PROP_QNAME_MD, QName.createQName(qname, namespaceService));

        // Recuperation du noeud du calque
        NodeRef nodeCalque = new NodeRef(storeRef, idCalque);
        logger.debug("Id calque à enrichir : " + idCalque);

        // Creation du noeud meta-data
        NodeRef metadata = null;
        try {
            ChildAssociationRef child = getNodeService().createNode(nodeCalque, CalqueModel.ASSOC_CALQUE,
                    CalqueModel.ASSOC_CALQUE, CalqueModel.TYPE_METADATA, proprietes);
            metadata = child.getChildRef();
        } catch (InvalidNodeRefException invalidNodeRef) {
            logger.error("Le noeud parent est invalide.", invalidNodeRef);
        } catch (InvalidTypeException invalidType) {
            logger.error("La référence du type du noeud n'est pas reconnu.", invalidType);
        }

        // retourne le noeud de la meta-donnee qui vient d'etre cree
        return metadata;
    }

    @Override
    public List<NodeRef> listSignatures(String idCalque) {
        // Recuperation du noeud du calque
        NodeRef nodeCalque = new NodeRef(storeRef, idCalque);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque : " + idCalque);
        }

        // Recherche des noeuds du type signature
        return getChildrenByType(nodeCalque, CalqueModel.TYPE_SIGNATURE);
    }

    @Override
    public List<NodeRef> listCommentaires(String idCalque) {
        // Recuperation du noeud du calque
        NodeRef nodeCalque = new NodeRef(storeRef, idCalque);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque : " + idCalque);
        }

        // Recherche des noeuds du type commentaire
        return getChildrenByType(nodeCalque, CalqueModel.TYPE_COMMENTAIRE);
    }

    @Override
    public List<NodeRef> listImages(String idCalque) {
        // Recuperation du noeud du calque
        NodeRef nodeCalque = new NodeRef(storeRef, idCalque);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque : " + idCalque);
        }

        // Recherche des noeuds du type image
        return getChildrenByType(nodeCalque, CalqueModel.TYPE_IMAGE);
    }

    @Override
    public List<NodeRef> listMetadata(String idCalque) {
        // Recuperation du noeud du calque
        NodeRef nodeCalque = new NodeRef(storeRef, idCalque);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque : " + idCalque);
        }

        // Recherche des noeuds du type meta-donnee
        return getChildrenByType(nodeCalque, CalqueModel.TYPE_METADATA);
    }

    @Override
    public List<ElementCalque> getElementsForCalque(NodeRef calque) {
        List<ElementCalque> retVal = new ArrayList<ElementCalque>();

        String idCalque = calque.getId();
        // Récupérer les noeuds du calque
        List<NodeRef> listCommentaires = listCommentaires(idCalque);
        List<NodeRef> listImages = listImages(idCalque);
        List<NodeRef> listSignatures = listSignatures(idCalque);
        List<NodeRef> listMetaData = listMetadata(idCalque);

        // Creation de la liste de tous les elements trie par page
        List<ElementCalque> listElements = new ArrayList<ElementCalque>();

        // Liste des pages des images
        for (NodeRef nodeRef : listImages) {
            retVal.add(new ElementCalque(nodeRef, getPageForCalqueElement(nodeRef)));
        }
        // Liste des pages des signatures
        for (NodeRef nodeRef : listSignatures) {
            retVal.add(new ElementCalque(nodeRef, getPageForCalqueElement(nodeRef)));
        }
        // Liste des pages des commentaires
        for (NodeRef nodeRef : listCommentaires) {
            retVal.add(new ElementCalque(nodeRef, getPageForCalqueElement(nodeRef)));
        }
        // Liste des pages des meta-donnees
        for (NodeRef nodeRef : listMetaData) {
            retVal.add(new ElementCalque(nodeRef, getPageForCalqueElement(nodeRef)));
        }

        return retVal;
    }

    @Override
    public void suppressionSignature(String idSignature) {
        // Recuperation du noeud de la signature
        NodeRef nodeSignature = new NodeRef(storeRef, idSignature);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque à supprimer : " + idSignature);
        }
        // Suppression de la signature
        getNodeService().deleteNode(nodeSignature);
    }

    @Override
    public void suppressionCommentaire(String idCommentaire) {
        // Recuperation du noeud du commentaire
        NodeRef nodeCommentaire = new NodeRef(storeRef, idCommentaire);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque à supprimer : " + idCommentaire);
        }

        // Suppression du commentaire
        getNodeService().deleteNode(nodeCommentaire);
    }

    @Override
    public void suppressionImage(String idImage) {
        // Recuperation du noeud de l'image
        NodeRef nodeImage = new NodeRef(storeRef, idImage);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque à supprimer : " + idImage);
        }

        // Suppression de l'image
        getNodeService().deleteNode(nodeImage);
    }

    @Override
    public void suppressionMetadata(String idMetaDa) {
        // Recuperation du noeud de la meta-donnee
        NodeRef nodeMetaDa = new NodeRef(storeRef, idMetaDa);
        if (logger.isDebugEnabled()) {
            logger.debug("Id calque à supprimer : " + idMetaDa);
        }

        // Modification du qname de la meta-donnee
        getNodeService().deleteNode(nodeMetaDa);
    }

    // <editor-fold desc="Getters - Setters">

    public NodeService getNodeService() {
        return nodeService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public SearchService getSearchService() {
        return searchService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public CopyService getCopyService() {
        return copyService;
    }

    public void setCopyService(CopyService copyService) {
        this.copyService = copyService;
    }

    public ContentService getContentService() {
        return contentService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public MimetypeService getMimetypeService() {
        return mimetypeService;
    }

    public void setMimetypeService(MimetypeService mimetypeService) {
        this.mimetypeService = mimetypeService;
    }

    public void setStoreRef(StoreRef storeRef) {
        this.storeRef = storeRef;
    }

    public NamespaceService getNamespaceService() {
        return namespaceService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    // </editor-fold desc="Getters - Setters">
}
