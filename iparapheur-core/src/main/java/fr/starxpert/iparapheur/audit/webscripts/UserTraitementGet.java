package fr.starxpert.iparapheur.audit.webscripts;

import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;
import fr.starxpert.iparapheur.audit.repo.AuditParapheurQueryParameters;
import fr.starxpert.iparapheur.audit.repo.AuditQueryResult;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.util.Pair;
import org.apache.log4j.Logger;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.io.Serializable;
import java.util.*;

/**
 * Created by lhameury on 24/03/16.
 */

public class UserTraitementGet extends AbstractDossierGet {

    private static Logger logger = Logger.getLogger(UserTraitementGet.class);

    private String auditParapheurKey = AuditParapheurService.AUDIT_REJET_PARAPHEUR;
    private String type;
    private String sstype;

    @Override
    protected Pair<String, Serializable> addParapheurKeyVal(String val) {
        return new Pair<String, Serializable>(auditParapheurKey, new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, val));
    }

    @Override
    protected Pair<String, Serializable> addSubtypeKeyVal(String val) {
        sstype = val;
        return null;
    }

    @Override
    protected Pair<String, Serializable> addTypeKeyVal(String val) {
        type = val;
        return null;
    }

    @Override
    protected String getApplicationName() {
        return AuditParapheurService.AUDIT_REJET_APPLICATION;
    }

    /**
     * Methode principale du webscript construit, effectue et formate les données
     *
     * @param req    la requete http
     * @param status
     * @param cache
     * @return un modèle qui sera fournit au freemarker.
     */
    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {

        try {
            boolean verbose = Boolean.parseBoolean(req.getParameter("verbose"));

            // Debut de la periode d'audit
            Long fromTime = Long.parseLong(req.getParameter("fromTime"));

            // Fin de la periode d'audit
            Long toTime = Long.parseLong(req.getParameter("toTime"));

            // Choix du mode de comptage
            int cumul = Integer.parseInt(req.getParameter("cumul"));

            AuditParapheurQueryParameters parameters = new AuditParapheurQueryParameters();

            // Liste des options

            parameters.setFromTime(fromTime);
            parameters.setToTime(toTime);
            parameters.setForward(true);
            parameters.setApplicationName(getApplicationName());
            auditParapheurKey = AuditParapheurService.AUDIT_REJET_PARAPHEUR;
            List<Pair<String, Serializable>> tokens = tokenizeOptions(req.getParameter("options"));

            // Set type / subtype
            if(sstype != null) {
                tokens.add(new Pair<String, Serializable>(AuditParapheurService.AUDIT_REJET_SOUSTYPEMETIER, sstype));
            }
            if(type != null) {
                tokens.add(new Pair<String, Serializable>(AuditParapheurService.AUDIT_REJET_TYPEMETIER, type));
            }

            parameters.setSearchKeyValues(tokens);

            postParametersHook(parameters);

            // Lancement de la requete d'audit - Pour les dossiers Rejetés
            AuthenticationUtil.setRunAsUserSystem();
            List<Map<String, Object>> entries = auditParapheurService.query(parameters);
            entries = postQueryHook(entries);

            AuditQueryResult queryResult = auditParapheurService.countAverageHandleTime(parameters, entries, AuditParapheurService.AUDIT_REJET_NODEREF);
            Map<Date, Serializable> resultMap = (Map<Date, Serializable>) queryResult.getResponses();


            // Lancement de la requete d'audit - Pour les dossiers Traités
            parameters.setApplicationName(AuditParapheurService.AUDIT_INSTRUIT_APPLICATION);
            auditParapheurKey = AuditParapheurService.AUDIT_INSTRUIT_PARAPHEUR;
            tokens = tokenizeOptions(req.getParameter("options"));
            // Set type / subtype
            if(sstype != null) {
                tokens.add(new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_SOUSTYPEMETIER, sstype));
            }
            if(type != null) {
                tokens.add(new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_TYPEMETIER, type));
            }


            parameters.setSearchKeyValues(tokens);
            entries = auditParapheurService.query(parameters);
            entries = postQueryHook(entries);

            queryResult = auditParapheurService.countAverageHandleTime(parameters, entries, AuditParapheurService.AUDIT_INSTRUIT_NODEREF);
            resultMap.putAll((Map<Date,Serializable>) queryResult.getResponses());


            // Date de début et de fin de la période d'audit
            Calendar fromDate = Calendar.getInstance();
            fromDate.setTime(new Date(fromTime));
            Calendar toDate = Calendar.getInstance();
            toDate.setTime(new Date(toTime));
            Map<String, Long> periodeMap = auditParapheurService.parseCumulHandleTime(cumul, resultMap, fromDate, toDate);

            // Modèle pour le freemarker
            Map<String, Object> model = new HashMap<String, Object>();

            if (queryResult.hasStats()) {
                queryResult.bindToModel(model);
            }

            model.put("dossiers", periodeMap);

            return model;
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebScriptException("[UserTraitementGet] Erreur durant l'execution de la fonction:\n" + e.toString());
        }
    }
}
