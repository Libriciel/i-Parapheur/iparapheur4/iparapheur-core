package fr.starxpert.iparapheur.audit.webscripts;

import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;
import fr.starxpert.iparapheur.audit.repo.AuditParapheurQueryParameters;
import fr.starxpert.iparapheur.audit.repo.AuditQueryResult;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.util.Pair;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.io.Serializable;
import java.util.*;

/**
 * DossiersEmisGet
 *
 * @author Lukas Hameury - Adullact Projet
 */
public class DossiersEmisGet extends AbstractDossierGet {

    @Override
    protected Pair<String, Serializable> addParapheurKeyVal(String val) {
        NodeRef parapheurRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, val);
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_PARAPHEUR, parapheurRef);
    }

    @Override
    protected Pair<String, Serializable> addSubtypeKeyVal(String val) {
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_SOUSTYPEMETIER, val);
    }

    @Override
    protected Pair<String, Serializable> addTypeKeyVal(String val) {
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_TYPEMETIER, val);
    }

    @Override
    protected String getApplicationName() {
        return AuditParapheurService.AUDIT_INSTRUIT_APPLICATION;
    }

    /**
     * Methode principale du webscript construit, effectue et formate les données
     *
     * @param req    la requete http
     * @param status
     * @param cache
     * @return un modèle qui sera fournit au freemarker.
     */
    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {

        try {
            // Debut de la periode d'audit
            Long fromTime = Long.parseLong(req.getParameter("fromTime"));

            // Fin de la periode d'audit
            Long toTime = Long.parseLong(req.getParameter("toTime"));

            // Choix du mode de comptage
            int cumul = Integer.parseInt(req.getParameter("cumul"));

            AuditParapheurQueryParameters parameters = new AuditParapheurQueryParameters();

            // Liste des options

            parameters.setFromTime(fromTime);
            parameters.setToTime(toTime);
            parameters.setForward(true);
            parameters.setApplicationName(getApplicationName());

            List<Pair<String, Serializable>> tokens = tokenizeOptions(req.getParameter("options"));

            parameters.setSearchKeyValues(tokens);

            postParametersHook(parameters);

            // Lancement de la requete d'audit

            List<Map<String, Object>> entries = auditParapheurService.query(parameters);

            entries = postQueryHook(entries);

            AuditQueryResult queryResult = auditParapheurService.countEntriesWithoutDuplication(parameters, entries, "/auditInstruit/approve/args/dossier/noderef");

            Map<Date, Serializable> resultMap = (Map<Date, Serializable>) queryResult.getResponses();

            // Date de début et de fin de la période d'audit
            Calendar fromDate = Calendar.getInstance();
            fromDate.setTime(new Date(fromTime));
            Calendar toDate = Calendar.getInstance();
            toDate.setTime(new Date(toTime));
            Map<String, Integer> periodeMap = auditParapheurService.parseCumul(cumul, resultMap, fromDate, toDate);

            // Modèle pour le freemarker
            Map<String, Object> model = new HashMap<String, Object>();

            if (queryResult.hasStats()) {
                queryResult.bindToModel(model);
            }

            model.put("dossiers", periodeMap);

            return model;
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebScriptException("[DossiersEmisGet] Erreur durant l'execution de la fonction:\n" + e.toString());
        }
    }
}