/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2011, StarXpert, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by StarXpert and ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.starxpert.iparapheur.audit.repo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.audit.AuditService.AuditQueryCallback;
import org.alfresco.service.cmr.repository.NodeService;

import com.atolcd.parapheur.repo.ParapheurService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.alfresco.util.Pair;

public class AuditParapheurServiceCallback implements AuditQueryCallback {
    /* Services */
    private NodeService nodeService;
    private ParapheurService parapheurService;
    private TenantService tenantService;
    private AuthenticationService authenticationService;
    /* Proprietes */
    private boolean verbose;
    private Map<String, String> options;
    private List<Map<String, Object>> entries;
    private AuditParapheurQueryParameters parameters;

    /* Constructeur */
    public AuditParapheurServiceCallback(AuditParapheurQueryParameters parameters) {
        this.parameters = parameters;
        this.entries = new ArrayList<Map<String, Object>>();
    }

    @Override
    public boolean valuesRequired() {
        return true;
    }

    @Override
    public boolean handleAuditEntry(Long entryId, String applicationName, String user, long time,
                                    Map<String, Serializable> values) {
        Map<String, Object> entry = new HashMap<String, Object>();
        entry.put("entryId", entryId);
        entry.put("date", time);
        entry.put("applicationName", applicationName);

        entry.putAll(values);

        // filtrage sur le tenant
        if (tenantService.isEnabled()) {
            String userDomain = tenantService.getUserDomain(user);
            String currentUser = authenticationService.getCurrentUserName();
            String currentUserDomain = tenantService.getUserDomain(currentUser);

            // si le domaine de l'utilisateur est différent du domaine de l'admin alors on ignore l'entrée
            if (currentUserDomain.equals(userDomain)) {
                // S'il y a des options on teste si cette entrée doit etre ajouté à la liste des resultats
                if (filterAudit(entry)) {
                    entries.add(entry);
                }
            }
        } else {
            if (filterAudit(entry)) {
                entries.add(entry);
            }
        }


        return true;
    }

    @Override
    public boolean handleAuditEntryError(Long entryId, String errorMsg, Throwable error) {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * Test les options indiquées par l'utilisateur pour filtrer les résultats de l'audit
     *
     * @param entry : entrée de l'audit à tester
     * @return
     */
    private boolean filterAudit(Map<String, Object> entry) {
        boolean isResultat = true;

        for (Pair<String, Serializable> searchKeyValue : parameters.getSearchKeyValues()) {
            Object valueForKey = entry.get(searchKeyValue.getFirst());

            if (valueForKey != null) {
                if (!valueForKey.equals(searchKeyValue.getSecond())) {
                    isResultat = false;
                }
            }
        }
        return isResultat;
    }

    //-------------------------------------------------------------------------- Getters - Setters des attributs

    /**
     * @return the entries
     */
    public List<Map<String, Object>> getEntries() {
        return entries;
    }

    /**
     * @param entries the entries to set
     */
    public void setEntries(List<Map<String, Object>> entries) {
        this.entries = entries;
    }

    /**
     * @return the nodeService
     */
    public NodeService getNodeService() {
        return nodeService;
    }

    /**
     * @param nodeService the nodeService to set
     */
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public AuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public TenantService getTenantService() {
        return tenantService;
    }

    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }
}
