/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2011, StarXpert, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by StarXpert and ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.starxpert.iparapheur.audit.cmr;


import fr.starxpert.iparapheur.audit.repo.AuditParapheurQueryParameters;
import fr.starxpert.iparapheur.audit.repo.AuditQueryResult;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

/**
 * StarXpert
 *
 * @author cpiassale - StarXpert
 * @author Emmanuel Peralta - Adullact Projet
 * @author Lukas Hameury - Adullact Projet
 *         <p/>
 *         Interface listant les différentes requêtes à implémenter.
 */
public interface AuditParapheurService {
    /* ApplicationName */
    public static String AUDIT_CREATION_APPLICATION = "AuditCreation";
    public static String AUDIT_INSTRUIT_APPLICATION = "AuditInstruit";
    public static String AUDIT_REJET_APPLICATION = "AuditRejet";

    /* Path AuditRejet*/
    public static String AUDIT_REJET_NODEREF = "/auditRejet/reject/args/dossier/noderef";
    public static String AUDIT_REJET_PARAPHEUR = "/auditRejet/reject/args/dossier/parapheur";
    public static String AUDIT_REJET_TYPEMETIER = "/auditRejet/reject/args/dossier/typeMetier";
    public static String AUDIT_REJET_SOUSTYPEMETIER = "/auditRejet/reject/args/dossier/sousTypeMetier";
    public static String AUDIT_REJET_DUREE = "/auditRejet/reject/args/dossier/duree";
    public static String AUDIT_REJET_DUREE_ETAPE = "/auditRejet/reject/args/dossier/dureeEtape";
    public static String AUDIT_REJET_EMETTEUR = "/auditRejet/reject/args/dossier/emetteur";
    public static String AUDIT_REJET_CAUSE = "/auditRejet/reject/args/dossier/cause";
    public static String AUDIT_REJET_TITLE = "/auditRejet/reject/args/dossier/titre";
    /* Path AuditInstruit */
    public static String AUDIT_INSTRUIT_NODEREF = "/auditInstruit/approve/args/dossier/noderef";
    public static String AUDIT_INSTRUIT_PARAPHEUR = "/auditInstruit/approve/args/dossier/parapheur";
    public static String AUDIT_INSTRUIT_STATUSMETIER = "/auditInstruit/approve/args/dossier/statusMetier";
    public static String AUDIT_INSTRUIT_TYPEMETIER = "/auditInstruit/approve/args/dossier/typeMetier";
    public static String AUDIT_INSTRUIT_SOUSTYPEMETIER = "/auditInstruit/approve/args/dossier/sousTypeMetier";
    public static String AUDIT_INSTRUIT_DUREE = "/auditInstruit/approve/args/dossier/duree";
    public static String AUDIT_INSTRUIT_DUREE_ETAPE = "/auditInstruit/approve/args/dossier/dureeEtape";
    /* Path AuditCreation */
    public static String AUDIT_CREATION_PARAPHEUR = "/auditCreation/creation/args/parapheur/parapheur";
    public static String AUDIT_CREATION_PROPERTIES = "/auditCreation/creation/args/properties/properties";
    public static String AUDIT_CREATION_NODEREF = "/auditCreation/creation/result/noderef";
    /* Options */
    public static String OPTION_PARAPHEUR = "parapheur";
    public static String OPTION_TYPE = "typeMetier";
    public static String OPTION_SOUS_TYPE = "soustypeMetier";


    /**
     * Effectue une requete d'audit via @see query et utilise @see countEntries pour générer
     * la valeur de retour
     *
     * @param parameters les parametres pour la requête sur les entrées d'audit
     * @return un objet de type AuditQueryResult contenant les différents résultats de la requête.
     */
    public AuditQueryResult countQuery(AuditParapheurQueryParameters parameters);

    /**
     * Compte le nombre de dossiers
     *
     * @param entries    Entrées d'audit récupéré via query.
     * @param parameters paramètres de la requete d'audit.
     * @return le nombre de dossiers correspondants aux parametres
     * @throws DataAccessException
     */
    public AuditQueryResult countEntries(AuditParapheurQueryParameters parameters, List<Map<String, Object>> entries);

    /**
     * Compte le nombre de dossiers, en évitant les doublons
     *
     * @param entries           Entrées d'audit récupéré via query.
     * @param parameters        paramètres de la requete d'audit.
     * @param propertyToTest    La propriété sur laquelle éviter les doublons.
     * @return le nombre de dossiers correspondants aux parametres
     * @throws DataAccessException
     */
    public AuditQueryResult countEntriesWithoutDuplication(AuditParapheurQueryParameters parameters, List<Map<String, Object>> entries, String propertyToTest);

    /**
     * Compte le temps de traitement moyen des dossiers, en évitant les doublons
     *
     * @param entries           Entrées d'audit récupéré via query.
     * @param parameters        paramètres de la requete d'audit.
     * @param propertyToTest    La propriété sur laquelle éviter les doublons.
     * @return le temps de traitement moyen de dossiers correspondants aux parametres
     * @throws DataAccessException
     */
    public AuditQueryResult countAverageHandleTime(AuditParapheurQueryParameters parameters, List<Map<String, Object>> entries, String propertyToTest);


    /**
     * Récupère les causes de rejet des entrée d'audit
     *
     * @param entries           Entrées d'audit récupéré via query.
     * @param parameters        paramètres de la requete d'audit.
     * @param propertyToTest    La propriété sur laquelle éviter les doublons.
     * @return les causes de rejet et titres de dossiers correspondants aux parametres
     * @throws DataAccessException
     */
    public AuditQueryResult getCausesForReject(AuditParapheurQueryParameters parameters, List<Map<String, Object>> entries, String propertyToTest);

    /**
     * Effectue une requête sur les tables d'audit
     *
     * @param parameters
     * @return
     */
    public List<Map<String, Object>> query(AuditParapheurQueryParameters parameters);

    /**
     * @param cumul     : choix du cumul de l'utilisateur
     * @param resultats : liste des résultats par date
     * @param fromTime  : date de démmarage
     * @param toTime    : date de fin
     * @return un tableau de résultats cumulant les données suivant le choix de l'utilisateur :
     *         - par jour
     *         - par semaine
     *         - par mois
     */
    public Map<String, Integer> parseCumul(int cumul, Map<Date, Serializable> resultats, Calendar fromTime, Calendar toTime);

    Map<String, Long> parseCumulHandleTime(int cumul, Map<Date, Serializable> resultats, Calendar fromTime, Calendar toTime);
}
