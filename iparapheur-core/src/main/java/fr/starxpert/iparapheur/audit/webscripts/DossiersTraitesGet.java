package fr.starxpert.iparapheur.audit.webscripts;

import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.util.Pair;
import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * DossiersTraitesGet
 *
 * @author Emmanuel Peralta - Adullact Projet
 *         Date: 12/12/11
 *         Time: 15:57
 */
public class DossiersTraitesGet extends AbstractDossierGet {

   private static Logger logger = Logger.getLogger(DossiersCreesGet.class);


    @Override
    protected Pair<String, Serializable> addParapheurKeyVal(String val) {
        NodeRef parapheurRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, val);
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_PARAPHEUR, parapheurRef);
    }

    @Override
    protected Pair<String, Serializable> addSubtypeKeyVal(String val) {
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_SOUSTYPEMETIER, val);
    }

    @Override
    protected Pair<String, Serializable> addTypeKeyVal(String val) {
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_TYPEMETIER, val);
    }

    @Override
    protected String getApplicationName() {
        return AuditParapheurService.AUDIT_INSTRUIT_APPLICATION;
    }
}