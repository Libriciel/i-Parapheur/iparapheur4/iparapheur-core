package fr.starxpert.iparapheur.audit.webscripts;

import com.atolcd.parapheur.model.ParapheurModel;
import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;
import fr.starxpert.iparapheur.audit.repo.AuditParapheurQueryParameters;
import fr.starxpert.iparapheur.audit.repo.AuditQueryResult;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.extensions.webscripts.*;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.*;

/**
 * AbstractDossierGet
 *
 * @author Emmanuel Peralta - Adullact Projet
 *         Date: 15/12/11
 *         Time: 14:25
 */
public abstract class AbstractDossierGet extends DeclarativeWebScript implements InitializingBean {

    protected AuditParapheurService auditParapheurService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(auditParapheurService);
    }

    public AuditParapheurService getAuditParapheurService() {
        return auditParapheurService;
    }

    public void setAuditParapheurService(AuditParapheurService auditParapheurService) {
        this.auditParapheurService = auditParapheurService;
    }

    /**
     * Callback de gestion de l'option de filtrage Parapheur
     * à implémenter dans les sous classes
     *
     * @param val
     * @return une paire à ajouter aux options de filtrage de la requete
     *         si null l'option n'est pas ajoutée.
     */
    protected abstract Pair<String, Serializable> addParapheurKeyVal(String val);

    /**
     * Callback de gestion de l'option de filtrage Sous type
     * à implémenter dans les sous classes
     *
     * @param val
     * @return une paire à ajouter aux options de filtrage de la requete
     *         si null l'option n'est pas ajoutée.
     */
    protected abstract Pair<String, Serializable> addSubtypeKeyVal(String val);

    /**
     * Callback de gestion de l'option de filtrage Type
     * à implémenter dans les sous classes
     *
     * @param val
     * @return une paire à ajouter aux options de filtrage de la requete
     *         si null l'option n'est pas ajoutée.
     */
    protected abstract Pair<String, Serializable> addTypeKeyVal(String val);

    /**
     * Retourne le nom de l'application d'audit utilisée pour récuperer les entrées d'audit
     *
     * @return une chaine contenant le nom de l'application d'audit @see AuditParapheurService.
     */
    protected abstract String getApplicationName();

    /**
     * Hook appellé en post query, utilisé pour faire des operations post requête
     * (Par exemple du filtrage supplémentaire).
     *
     * @param entries
     * @return une liste d'entitées d'audit filtrée.
     */
    protected List<Map<String, Object>> postQueryHook(List<Map<String, Object>> entries) {
        return entries;
    }

    /**
     * Hook pour ajouter des options de filtrage supplémentaires (par exemple le statut)
     *
     * @param parameters les paramètres de la requête
     */
    protected void postParametersHook(AuditParapheurQueryParameters parameters) {

    }

    /**
     * Tokenize la chaine d'option passée par la vue du webscript
     *
     * @param options la chaine passée dans la requette http
     * @return une liste de paire clé / valeur
     */
    protected List<Pair<String, Serializable>> tokenizeOptions(String options) {
        List<Pair<String, Serializable>> retVal = new ArrayList<Pair<String, Serializable>>();

        String type = null;
        String sstype = null;
        boolean hasType = false;
        boolean hasSousType = false;
        if (options != null) {
            String[] elements = options.split("/");
            for (String element : elements) {

                if (element.contains(";")) {
                    String key = element.split(";")[0];
                    String value = element.split(";")[1];

                    if (key.equals(AuditParapheurService.OPTION_PARAPHEUR)) {
                        Pair<String, Serializable> pair = addParapheurKeyVal(value);
                        if (pair != null)
                            retVal.add(pair);
                    } else if (key.equals(AuditParapheurService.OPTION_TYPE)) {
                        Pair<String, Serializable> pair = addTypeKeyVal(value);
                        hasType = true;
                        if (pair != null)
                            retVal.add(pair);
                    } else if (key.equals(AuditParapheurService.OPTION_SOUS_TYPE)) {
                        Pair<String, Serializable> pair = addSubtypeKeyVal(value);
                        hasSousType = true;
                        if (pair != null)
                            retVal.add(pair);
                    } else {
                        throw new RuntimeException("Cette option de filtrage n'existe pas.");
                    }

                }
            }
        }
        if(!hasType) {
            addTypeKeyVal(null);
        }
        if(!hasSousType) {
            addSubtypeKeyVal(null);
        }
        return retVal;
    }

    /**
     * Methode principale du webscript construit, effectue et formate les données
     *
     * @param req    la requete http
     * @param status
     * @param cache
     * @return un modèle qui sera fournit au freemarker.
     */
    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {

        try {
            boolean verbose = Boolean.parseBoolean(req.getParameter("verbose"));

            // Debut de la periode d'audit
            Long fromTime = Long.parseLong(req.getParameter("fromTime"));

            // Fin de la periode d'audit
            Long toTime = Long.parseLong(req.getParameter("toTime"));

            // Choix du mode de comptage
            int cumul = Integer.parseInt(req.getParameter("cumul"));

            AuditParapheurQueryParameters parameters = new AuditParapheurQueryParameters();

            // Liste des options

            parameters.setFromTime(fromTime);
            parameters.setToTime(toTime);
            parameters.setForward(true);
            parameters.setApplicationName(getApplicationName());

            List<Pair<String, Serializable>> tokens = tokenizeOptions(req.getParameter("options"));

            parameters.setSearchKeyValues(tokens);

            postParametersHook(parameters);

            // Lancement de la requete d'audit

            List<Map<String, Object>> entries = auditParapheurService.query(parameters);

            entries = postQueryHook(entries);

            AuditQueryResult queryResult = auditParapheurService.countEntries(parameters, entries);

            Map<Date, Serializable> resultMap = (Map<Date, Serializable>) queryResult.getResponses();

            // Date de début et de fin de la période d'audit
            Calendar fromDate = Calendar.getInstance();
            fromDate.setTime(new Date(fromTime));
            Calendar toDate = Calendar.getInstance();
            toDate.setTime(new Date(toTime));
            Map<String, Integer> periodeMap = new TreeMap<String, Integer>();
            periodeMap = auditParapheurService.parseCumul(cumul, resultMap, fromDate, toDate);

            // Modèle pour le freemarker
            Map<String, Object> model = new HashMap<String, Object>();

            if (queryResult.hasStats()) {
                queryResult.bindToModel(model);
            }

            model.put("dossiers", periodeMap);

            return model;
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebScriptException("[AbstractDossierGet] Erreur durant l'execution de la fonction:\n" + e.toString());
        }
    }
}
