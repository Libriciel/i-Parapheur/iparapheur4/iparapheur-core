package fr.starxpert.iparapheur.audit.webscripts;

import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;
import fr.starxpert.iparapheur.audit.repo.AuditParapheurQueryParameters;
import fr.starxpert.iparapheur.audit.repo.AuditQueryResult;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.util.Pair;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.io.Serializable;
import java.util.*;

/**
 * Created by lhameury on 24/03/16.
 */

public class CauseRefusGet extends AbstractDossierGet {

    private static Logger logger = Logger.getLogger(CauseRefusGet.class);

    @Override
    protected Pair<String, Serializable> addParapheurKeyVal(String val) {
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_REJET_PARAPHEUR, new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, val));
    }

    @Override
    protected Pair<String, Serializable> addSubtypeKeyVal(String val) {
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_REJET_SOUSTYPEMETIER, val);
    }

    @Override
    protected Pair<String, Serializable> addTypeKeyVal(String val) {
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_REJET_TYPEMETIER, val);
    }

    @Override
    protected String getApplicationName() {
        return AuditParapheurService.AUDIT_REJET_APPLICATION;
    }

    /**
     * Methode principale du webscript construit, effectue et formate les données
     *
     * @param req    la requete http
     * @param status
     * @param cache
     * @return un modèle qui sera fournit au freemarker.
     */
    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {

        try {
            boolean verbose = Boolean.parseBoolean(req.getParameter("verbose"));

            // Debut de la periode d'audit
            Long fromTime = Long.parseLong(req.getParameter("fromTime"));

            // Fin de la periode d'audit
            Long toTime = Long.parseLong(req.getParameter("toTime"));

            AuditParapheurQueryParameters parameters = new AuditParapheurQueryParameters();

            // Liste des options

            parameters.setFromTime(fromTime);
            parameters.setToTime(toTime);
            parameters.setForward(true);
            parameters.setApplicationName(getApplicationName());
            List<Pair<String, Serializable>> tokens = tokenizeOptions(req.getParameter("options"));

            parameters.setSearchKeyValues(tokens);

            postParametersHook(parameters);

            // Lancement de la requete d'audit - Pour les dossiers Rejetés
            List<Map<String, Object>> entries = auditParapheurService.query(parameters);
            entries = postQueryHook(entries);

            AuditQueryResult queryResult = auditParapheurService.getCausesForReject(parameters, entries, AuditParapheurService.AUDIT_REJET_NODEREF);
            List<Serializable> resultMap = (List<Serializable>) queryResult.getResponses();

            // Modèle pour le freemarker
            Map<String, Object> model = new HashMap<String, Object>();

            if (queryResult.hasStats()) {
                queryResult.bindToModel(model);
            }

            JSONArray arr = new JSONArray(resultMap);

            model.put("dossiers", arr.toString());

            return model;
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebScriptException("[CauseRefusGet] Erreur durant l'execution de la fonction:\n" + e.toString());
        }
    }

    @Override
    protected List<Map<String, Object>> postQueryHook(List<Map<String, Object>> entries) {
        List<Map<String, Object>> entriesMutable = new ArrayList<Map<String, Object>>(entries.size());
        entriesMutable.addAll(entries);

        for (Map<String, Object> entry : entriesMutable) {
            if(!entry.containsKey(AuditParapheurService.AUDIT_REJET_CAUSE)) {
                entries.remove(entry);
            }
        }

        return entries;
    }
}
