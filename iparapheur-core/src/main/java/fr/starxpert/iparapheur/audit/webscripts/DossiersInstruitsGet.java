/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by StarXpert
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.starxpert.iparapheur.audit.webscripts;

import fr.starxpert.iparapheur.audit.repo.AuditParapheurQueryParameters;

import java.io.Serializable;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.util.Pair;
import org.apache.log4j.Logger;

import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;
import org.adullact.iparapheur.status.StatusMetier;

/**
 * DossiersInstruitsGet
 *
 * @author cpiassale - StarXpert
 * @author Emmanuel Peralta - Adullact Projet
 *         <p/>
 *         Classe controleur du webscript d'audit sur dossiers instruits.
 */
public class DossiersInstruitsGet extends AbstractDossierGet {

    private static Logger logger = Logger.getLogger(DossiersCreesGet.class);

    @Override
    protected Pair<String, Serializable> addParapheurKeyVal(String val) {
        NodeRef parapheurRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, val);
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_PARAPHEUR, parapheurRef);
    }

    @Override
    protected Pair<String, Serializable> addSubtypeKeyVal(String val) {
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_SOUSTYPEMETIER, val);
    }

    @Override
    protected Pair<String, Serializable> addTypeKeyVal(String val) {
        return new Pair<String, Serializable>(AuditParapheurService.AUDIT_INSTRUIT_TYPEMETIER, val);
    }

    @Override
    protected String getApplicationName() {
        return AuditParapheurService.AUDIT_INSTRUIT_APPLICATION;
    }

    @Override
    protected void postParametersHook(AuditParapheurQueryParameters parameters) {
        super.postParametersHook(parameters);
        parameters.addSearchKeyValues(AuditParapheurService.AUDIT_INSTRUIT_STATUSMETIER, StatusMetier.STATUS_ARCHIVE);
    }

}
