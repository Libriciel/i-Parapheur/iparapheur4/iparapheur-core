/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2011, StarXpert, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by StarXpert and ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.starxpert.iparapheur.audit.repo.extractor;

import com.atolcd.parapheur.model.ParapheurModel;

import java.io.Serializable;

import org.alfresco.repo.audit.extractor.AbstractDataExtractor;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;

public class StatusMetierDataExtractor extends AbstractDataExtractor {

    /* Logger */
    private static Logger logger = Logger.getLogger(StatusMetierDataExtractor.class);

    /* Services */
    private NodeService nodeService;

    //-------------------------------------------------------------------------- Méthodes publiques

    /**
     * @param data : donnée transmise
     * @return <tt>true</tt> si la donnée est un {@link NodeRef}
     */
    @Override
    public boolean isSupported(Serializable data) {
        return (data != null && data instanceof NodeRef);
    }

    /**
     * @param value : donnée transmise
     * @return la valeur du status metier du noeud
     */
    @Override
    public Serializable extractData(Serializable value) throws Throwable {
        NodeRef nodeRef = (NodeRef) value;
        Serializable statusMetier = null;
        if (!getNodeService().exists(nodeRef)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Impossible d'extraire une valeur d'un noeud inexistant : " + nodeRef);
            }
        } else {
            statusMetier = getNodeService().getProperty(nodeRef, ParapheurModel.PROP_STATUS_METIER);
        }

        return statusMetier;
    }

    //-------------------------------------------------------------------------- Getters - Setters des services

    /**
     * @return the nodeService
     */
    public NodeService getNodeService() {
        return nodeService;
    }

    /**
     * @param nodeService the nodeService to set
     */
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

}
