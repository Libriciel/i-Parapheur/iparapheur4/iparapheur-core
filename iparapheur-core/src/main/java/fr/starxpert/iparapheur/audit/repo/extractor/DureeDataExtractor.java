/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2011, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package fr.starxpert.iparapheur.audit.repo.extractor;

import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;
import fr.starxpert.iparapheur.audit.repo.AuditParapheurServiceCallback;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.audit.extractor.AbstractDataExtractor;
import org.alfresco.service.cmr.audit.AuditQueryParameters;
import org.alfresco.service.cmr.audit.AuditService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;

/**
 * @author Emmanuel Peralta - Adullact Projet
 */
public class DureeDataExtractor extends AbstractDataExtractor {

    private NodeService nodeService;
    private AuditService auditService;
    private String auditPath;
    private String auditApp;
    private long currentTime;
    private boolean hasExternalTime = false;

    public String getAuditApp() {
        return auditApp;
    }

    public void setAuditApp(String auditApp) {
        this.auditApp = auditApp;
    }

    public AuditService getAuditService() {
        return auditService;
    }

    public void setAuditService(AuditService auditService) {
        this.auditService = auditService;
    }

    public String getAuditPath() {
        return auditPath;
    }

    public void setAuditPath(String auditPath) {
        this.auditPath = auditPath;
    }

    public NodeService getNodeService() {
        return nodeService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public long getCurrentTime() {
        if (hasExternalTime) {
            return currentTime;
        }

        return System.currentTimeMillis();
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
        hasExternalTime = true;
    }

    @Override
    public boolean isSupported(Serializable data) {
        return data != null && data instanceof NodeRef;
    }

    @Override
    public Serializable extractData(Serializable data) throws Throwable {
        NodeRef nodeRef = (NodeRef) data;

        long timestamp = ((Date) nodeService.getProperty(nodeRef, ContentModel.PROP_CREATED)).getTime();

        long retVal = 0;

        /* Si le timeStamp est égal à 0 cela signifie que nous n'avons
         * pas dans les traces d'audit la création du document donc
         * plutot que d'avoir le getCurrentTime en guise de durée on la
         * met à 0.
         */
        if (timestamp != 0) {
            retVal = getCurrentTime() - timestamp;
            retVal /= 1000; // Conversion ms en s
        }

        return retVal;
    }

    private class AuditTimeCallback implements AuditService.AuditQueryCallback {

        private long date;

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }

        @Override
        public boolean valuesRequired() {
            return false;
        }

        @Override
        public boolean handleAuditEntry(Long l, String string, String string1, long l1, Map<String, Serializable> map) {
            this.date = l1;
            return false;
        }

        @Override
        public boolean handleAuditEntryError(Long l, String string, Throwable thrwbl) {
            return true;
        }

    }

}
