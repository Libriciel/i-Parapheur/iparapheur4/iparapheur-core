package fr.starxpert.iparapheur.audit.repo;

import org.alfresco.util.Pair;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * AuditParapheurQueryParameters classe permettant de spécifier
 * les paramètres de la requête d'audit sur les différentes applications.
 * Possibilité de spécifier les options de filtrage.
 *
 * @author Emmanuel Peralta - Adullact Projet
 *         Date: 12/12/11
 *         Time: 16:03
 */
public class AuditParapheurQueryParameters {
    private boolean forward;
    private String applicationName;
    private String user;
    private Long fromId;
    private Long toId;
    private Long fromTime;
    private Long toTime;
    private List<Pair<String, Serializable>> searchKeyValues;


    public AuditParapheurQueryParameters() {
        searchKeyValues = new ArrayList<Pair<String, Serializable>>();
    }

    public boolean isForward() {
        return forward;
    }

    public void setForward(boolean forward) {
        this.forward = forward;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getFromId() {
        return fromId;
    }

    public void setFromId(Long fromId) {
        this.fromId = fromId;
    }

    public Long getToId() {
        return toId;
    }

    public void setToId(Long toId) {
        this.toId = toId;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

    public Long getToTime() {
        return toTime;
    }

    public void setToTime(Long toTime) {
        this.toTime = toTime;
    }

    public List<Pair<String, Serializable>> getSearchKeyValues() {
        return searchKeyValues;
    }

    public void setSearchKeyValues(List<Pair<String, Serializable>> searchKeyValues) {
        this.searchKeyValues = searchKeyValues;
    }

    public void addSearchKeyValues(String key, Serializable value) {
        this.searchKeyValues.add(new Pair<String, Serializable>(key, value));
    }
}
