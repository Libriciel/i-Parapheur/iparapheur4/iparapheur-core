/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2011, StarXpert, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by StarXpert and ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.starxpert.iparapheur.audit.webscripts;

import fr.starxpert.iparapheur.audit.repo.AuditParapheurQueryParameters;
import fr.starxpert.iparapheur.audit.repo.AuditQueryResult;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.util.Assert;

import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;

public class DossiersEnRetardGet extends DeclarativeWebScript implements InitializingBean {

    /* Logger */
    private static Logger logger = Logger.getLogger(DossiersCreesGet.class);

    /* Services */
    private AuditParapheurService auditParapheurService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(auditParapheurService);
    }

    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
        try {
            boolean verbose = Boolean.parseBoolean(req.getParameter("verbose"));

            // Debut de la periode d'audit
            Long fromTime = Long.parseLong(req.getParameter("fromTime"));

            // Fin de la periode d'audit
            Long toTime = Long.parseLong(req.getParameter("toTime"));

            // Choix du mode de comptage
            int cumul = Integer.parseInt(req.getParameter("cumul"));

            // Liste des options
            Map<String, String> queryOptions = new HashMap<String, String>();
            String options = req.getParameter("options");
            if (options != null) {
                String[] elements = options.split("/");
                for (String element : elements) {
                    String key = element.split(";")[0];
                    String value = element.split(";")[1];
                    queryOptions.put(key, value);
                }
            }

            AuditParapheurQueryParameters parameters = new AuditParapheurQueryParameters();
            // Lancement de la requete d'audit
            logger.error(" before countDossiers");
            AuditQueryResult queryResult = auditParapheurService.countQuery(parameters);
            Map<Date, Serializable> resultMap = (Map<Date, Serializable>) queryResult.getResponses();
            logger.error(" after countDossiers");

            // Date de début et de fin de la période d'audit
            Calendar fromDate = Calendar.getInstance();
            fromDate.setTime(new Date(fromTime));
            Calendar toDate = Calendar.getInstance();
            toDate.setTime(new Date(toTime));
            Map<String, Integer> periodeMap = new TreeMap<String, Integer>();
            periodeMap = auditParapheurService.parseCumul(cumul, resultMap, fromDate, toDate);

            // Modèle pour le freemarker
            Map<String, Object> model = new HashMap<String, Object>();
            model.put("dossiersEnRetard", periodeMap);

            return model;
        } catch (Exception e) {
            throw new WebScriptException("[DossiersEnRetardGet] Erreur durant l'execution de la fonction");
        }
    }

    //-------------------------------------------------------------------------- Getters - Setters des services

    /**
     * @return the auditParapheurService
     */
    public AuditParapheurService getAuditParapheurService() {
        return auditParapheurService;
    }

    /**
     * @param auditParapheurService the auditParapheurService to set
     */
    public void setAuditParapheurService(AuditParapheurService auditParapheurService) {
        this.auditParapheurService = auditParapheurService;
    }

}
