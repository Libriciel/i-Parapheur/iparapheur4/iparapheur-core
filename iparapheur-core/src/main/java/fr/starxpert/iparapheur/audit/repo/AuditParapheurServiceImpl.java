/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2011, StarXpert, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by StarXpert and ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.starxpert.iparapheur.audit.repo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

import org.alfresco.service.cmr.audit.AuditQueryParameters;
import org.alfresco.service.cmr.audit.AuditService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;

import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.security.AuthenticationService;

/**
 * Classe implémentant les différentes requêtes d'audit ainsi que les méthodes
 * d'accès à la base.
 *
 * @author cpiassale - StarXpert
 * @author Emmanuel Peralta - Adullact Projet
 */
public class AuditParapheurServiceImpl extends HibernateDaoSupport implements AuditParapheurService {

    private static Logger logger = Logger.getLogger(AuditParapheurServiceImpl.class);
    private AuditService auditService;
    private TenantService tenantService;
    private AuthenticationService authenticationService;

    // ------------------------------------------------------------------------- Méthodes privées
    private Map<String, Integer> cumulWithFormat(SimpleDateFormat format, Map<Date, Serializable> data) {
        Map<String, Integer> retVal = new TreeMap<String, Integer>();
        for (Date key : data.keySet()) {
            String formatedDate = format.format(key);
            if (retVal.containsKey(formatedDate)) {
                // On cumule avec les resultats de ce jour déjà présent dans la liste
                int compteur = retVal.get(formatedDate);
                retVal.put(formatedDate, compteur + 1);
            } else {
                retVal.put(formatedDate, 1);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("RetVal after projection (%s): [%d]", format, retVal.keySet().size()));
        }

        return retVal;
    }

    private Map<String, Long> cumulWithAverage(SimpleDateFormat format, Map<Date, Serializable> data) {
        Map<String, Long> retVal = new TreeMap<String, Long>();
        Map<String, Integer> totalCount = new HashMap<String, Integer>();
        for (Date key : data.keySet()) {
            String formatedDate = format.format(key);
            if (retVal.containsKey(formatedDate)) {
                // On cumule avec les resultats de ce jour déjà présent dans la liste
                Long tempsTraitement = retVal.get(formatedDate);
                retVal.put(formatedDate, tempsTraitement + (Long) data.get(key));

                int compteur = totalCount.get(formatedDate);
                totalCount.put(formatedDate, compteur + 1);
            } else {
                retVal.put(formatedDate, (Long) data.get(key));
                totalCount.put(formatedDate, 1);
            }
        }

        for (Map.Entry<String, Long> entry : retVal.entrySet())
        {
            retVal.put(entry.getKey(), entry.getValue() / totalCount.get(entry.getKey()));
        }

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("RetVal after projection (%s): [%d]", format, retVal.keySet().size()));
        }

        return retVal;
    }

    private void paddMapWithZerosInt(Map<String, Integer> resultatsCumules, Calendar fromTime, Calendar toTime, SimpleDateFormat format, int type) {
        toTime.add(type, 1);

        for (Calendar curseur = (Calendar) (fromTime.clone());
             curseur.before(toTime);
             curseur.add(type, 1)) {
            String curDateString = format.format(curseur.getTime());

            if (!resultatsCumules.containsKey(curDateString)) {
                resultatsCumules.put(curDateString, 0);
            }
        }

    }

    private void paddMapWithZerosLong(Map<String, Long> resultatsCumules, Calendar fromTime, Calendar toTime, SimpleDateFormat format, int type) {
        toTime.add(type, 1);

        for (Calendar curseur = (Calendar) (fromTime.clone());
                curseur.before(toTime);
                curseur.add(type, 1)) {
            String curDateString = format.format(curseur.getTime());

            if (!resultatsCumules.containsKey(curDateString)) {
                resultatsCumules.put(curDateString, 0L);
            }
        }

    }

    // ------------------------------------------------------------------------- Méthodes publiques
    @Override
    public List<Map<String, Object>> query(AuditParapheurQueryParameters parameters) {
        // Execution de la requete
        AuditParapheurServiceCallback auditCallback = new AuditParapheurServiceCallback(parameters);

        auditCallback.setAuthenticationService(authenticationService);
        auditCallback.setTenantService(tenantService);

        AuditQueryParameters queryParameters = new AuditQueryParameters();
        queryParameters.setApplicationName(parameters.getApplicationName());
        queryParameters.setFromTime(parameters.getFromTime());
        queryParameters.setToTime(parameters.getToTime());
        queryParameters.setForward(parameters.isForward());
        // it only works for one search key (using parapheur)
        //parameters = getFilterParametersFromOptions(applicationName, options, queryParameters);
        //options.remove("parapheur");

        getAuditService().auditQuery(auditCallback, queryParameters, 0);
        return auditCallback.getEntries();
    }

    public AuditQueryResult countEntriesWithoutDuplication(AuditParapheurQueryParameters parameters, List<Map<String, Object>> entries, String propertyToTest) {
        AuditQueryResult retVal = new AuditQueryResult();
        // Calcul du resultat
        if (logger.isDebugEnabled()) {
            logger.debug("Audit : " + parameters.getApplicationName());
            logger.debug("Filtres : " + parameters.getSearchKeyValues());
            logger.debug("Nombre de réponses : " + entries.size());
        }

        double moyenne = 0;
        long duree = 0;
        String dureeKey = "";
        List<String> dossiers = new ArrayList<String>();

        if (parameters.getApplicationName().equals(AuditParapheurService.AUDIT_INSTRUIT_APPLICATION)) {
            dureeKey = AuditParapheurService.AUDIT_INSTRUIT_DUREE;
        } else if (parameters.getApplicationName().equals(AuditParapheurService.AUDIT_REJET_APPLICATION)) {
            dureeKey = AuditParapheurService.AUDIT_REJET_DUREE;
        }


        List<Long> durees = new ArrayList<Long>();

        Map<Date, Serializable> responses = new TreeMap<Date, Serializable>();
        for (Map<String, Object> entry : entries) {
            String node = entry.get(propertyToTest).toString();
            if(!dossiers.contains(node)) {
                dossiers.add(node);
                Date date = new Date(Long.parseLong(entry.get("date").toString()));
                responses.put(date, 1);

                if (entry.get(dureeKey) != null) {
                    duree = (Long) entry.get(dureeKey);
                    if (duree > 0) {
                        durees.add(duree);
                        moyenne += duree;

                    }
                }
            }

        }

        if (!dureeKey.equals("")) {
            double variance = 0;
            double ecartType = 0;

            if (durees.size() > 0) {
                moyenne /= (double) durees.size();

                for (long x : durees) {
                    variance += Math.pow(x - moyenne, 2);
                }

                variance /= durees.size();

                ecartType = Math.sqrt(variance);
            }

            retVal.setMoyenne(moyenne);
            retVal.setEcartType(ecartType);
            retVal.setVariance(variance);
            retVal.setHasStats(true);
        }

        retVal.setResponses((Serializable) responses);
        return retVal;
    }

    public AuditQueryResult countEntries(AuditParapheurQueryParameters parameters, List<Map<String, Object>> entries) {
        AuditQueryResult retVal = new AuditQueryResult();
        // Calcul du resultat
        if (logger.isDebugEnabled()) {
            logger.debug("Audit : " + parameters.getApplicationName());
            logger.debug("Filtres : " + parameters.getSearchKeyValues());
            logger.debug("Nombre de réponses : " + entries.size());
        }

        double moyenne = 0;
        long duree = 0;
        String dureeKey = "";

        if (parameters.getApplicationName().equals(AuditParapheurService.AUDIT_INSTRUIT_APPLICATION)) {
            dureeKey = AuditParapheurService.AUDIT_INSTRUIT_DUREE;
        } else if (parameters.getApplicationName().equals(AuditParapheurService.AUDIT_REJET_APPLICATION)) {
            dureeKey = AuditParapheurService.AUDIT_REJET_DUREE;
        }


        List<Long> durees = new ArrayList<Long>();

        Map<Date, Serializable> responses = new TreeMap<Date, Serializable>();
        for (Map<String, Object> entry : entries) {
            Date date = new Date(Long.parseLong(entry.get("date").toString()));
            responses.put(date, 1);

            if (entry.get(dureeKey) != null) {
                duree = (Long) entry.get(dureeKey);
                if (duree > 0) {
                    durees.add(duree);
                    moyenne += duree;

                }
            }

        }

        if (!dureeKey.equals("")) {
            double variance = 0;
            double ecartType = 0;

            if (durees.size() > 0) {
                moyenne /= (double) durees.size();

                for (long x : durees) {
                    variance += Math.pow(x - moyenne, 2);
                }

                variance /= durees.size();

                ecartType = Math.sqrt(variance);
            }

            retVal.setMoyenne(moyenne);
            retVal.setEcartType(ecartType);
            retVal.setVariance(variance);
            retVal.setHasStats(true);
        }

        retVal.setResponses((Serializable) responses);
        return retVal;
    }

    public AuditQueryResult countAverageHandleTime(AuditParapheurQueryParameters parameters, List<Map<String, Object>> entries, String propertyToTest) {
        AuditQueryResult retVal = new AuditQueryResult();
        // Calcul du resultat
        if (logger.isDebugEnabled()) {
            logger.debug("Audit : " + parameters.getApplicationName());
            logger.debug("Filtres : " + parameters.getSearchKeyValues());
            logger.debug("Nombre de réponses : " + entries.size());
        }

        String dureeKey = "";
        List<String> dossiers = new ArrayList<String>();

        if (parameters.getApplicationName().equals(AuditParapheurService.AUDIT_INSTRUIT_APPLICATION)) {
            dureeKey = AuditParapheurService.AUDIT_INSTRUIT_DUREE_ETAPE;
        } else if (parameters.getApplicationName().equals(AuditParapheurService.AUDIT_REJET_APPLICATION)) {
            dureeKey = AuditParapheurService.AUDIT_REJET_DUREE_ETAPE;
        }


        Map<Date, Serializable> responses = new TreeMap<Date, Serializable>();
        for (Map<String, Object> entry : entries) {
            String node = entry.get(propertyToTest).toString();
            //if(!dossiers.contains(node)) {
                Date date = new Date(Long.parseLong(entry.get("date").toString()));
                Long dureeEtape = entry.containsKey(dureeKey) ? (Long) entry.get(dureeKey) : 0L;
                if(dureeEtape > 0L) {
                    dossiers.add(node);
                    responses.put(date, dureeEtape/1000);
                }
           // }

        }

        retVal.setResponses((Serializable) responses);
        return retVal;
    }

    public AuditQueryResult getCausesForReject(AuditParapheurQueryParameters parameters, List<Map<String, Object>> entries, String propertyToTest) {
        AuditQueryResult retVal = new AuditQueryResult();
        // Calcul du resultat
        if (logger.isDebugEnabled()) {
            logger.debug("Audit : " + parameters.getApplicationName());
            logger.debug("Filtres : " + parameters.getSearchKeyValues());
            logger.debug("Nombre de réponses : " + entries.size());
        }

        List<Serializable> responses = new ArrayList<Serializable>();
        for (Map<String, Object> entry : entries) {
            String node = entry.get(propertyToTest).toString();
            Date date = new Date(Long.parseLong(entry.get("date").toString()));
            String annotation = (String) entry.get(AUDIT_REJET_CAUSE);
            String title = (String) entry.get(AUDIT_REJET_TITLE);
            if(annotation != null && !annotation.isEmpty()
               && title != null && !title.isEmpty()) {
                HashMap<String, Serializable> data = new HashMap<String, Serializable>();
                data.put("cause", annotation);
                data.put("title", title);
                data.put("date", date.getTime());
                data.put("emetteur", ((NodeRef) entry.get(AUDIT_REJET_EMETTEUR)).getId());
                data.put("parapheur", ((NodeRef) entry.get(AUDIT_REJET_PARAPHEUR)).getId());
                responses.add(data);
            }
        }

        retVal.setResponses((Serializable) responses);
        return retVal;
    }

    public AuditQueryResult countQuery(AuditParapheurQueryParameters parameters) {
        List<Map<String, Object>> entries = query(parameters);

        return countEntries(parameters, entries);
    }

    @Override
    public Map<String, Integer> parseCumul(int cumul, Map<Date, Serializable> resultats, Calendar fromTime, Calendar toTime) {
        Map<String, Integer> resultatsCumules = new TreeMap<String, Integer>();

        switch (cumul) {
            case 1: // Jour
                SimpleDateFormat formatDay = new SimpleDateFormat("yyyy-MM-dd");
                resultatsCumules = cumulWithFormat(formatDay, resultats);
                paddMapWithZerosInt(resultatsCumules, fromTime, toTime, formatDay, Calendar.DAY_OF_MONTH);
                break;
            case 2: // Semaine
                SimpleDateFormat formatWeek = new SimpleDateFormat("yyyy-ww");
                resultatsCumules = cumulWithFormat(formatWeek, resultats);
                paddMapWithZerosInt(resultatsCumules, fromTime, toTime, formatWeek, Calendar.WEEK_OF_MONTH);
                break;
            case 3: // Mois
                SimpleDateFormat formatMonth = new SimpleDateFormat("yyyy-MM");
                resultatsCumules = cumulWithFormat(formatMonth, resultats);
                paddMapWithZerosInt(resultatsCumules, fromTime, toTime, formatMonth, Calendar.MONTH);
                break;
            case 4: // Année
                SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");
                resultatsCumules = cumulWithFormat(formatYear, resultats);
                paddMapWithZerosInt(resultatsCumules, fromTime, toTime, formatYear, Calendar.YEAR);
                break;
        }
        return resultatsCumules;
    }


    @Override
    public Map<String, Long> parseCumulHandleTime(int cumul, Map<Date, Serializable> resultats, Calendar fromTime, Calendar toTime) {
        Map<String, Long> resultatsCumules = new TreeMap<String, Long>();

        switch (cumul) {
            case 1: // Jour
                SimpleDateFormat formatDay = new SimpleDateFormat("yyyy-MM-dd");
                resultatsCumules = cumulWithAverage(formatDay, resultats);
                paddMapWithZerosLong(resultatsCumules, fromTime, toTime, formatDay, Calendar.DAY_OF_MONTH);
                break;
            case 2: // Semaine
                SimpleDateFormat formatWeek = new SimpleDateFormat("yyyy-ww");
                resultatsCumules = cumulWithAverage(formatWeek, resultats);
                paddMapWithZerosLong(resultatsCumules, fromTime, toTime, formatWeek, Calendar.WEEK_OF_MONTH);
                break;
            case 3: // Mois
                SimpleDateFormat formatMonth = new SimpleDateFormat("yyyy-MM");
                resultatsCumules = cumulWithAverage(formatMonth, resultats);
                paddMapWithZerosLong(resultatsCumules, fromTime, toTime, formatMonth, Calendar.MONTH);
                break;
            case 4: // Année
                SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");
                resultatsCumules = cumulWithAverage(formatYear, resultats);
                paddMapWithZerosLong(resultatsCumules, fromTime, toTime, formatYear, Calendar.YEAR);
                break;
        }
        return resultatsCumules;
    }

    //----------------------------------------------------------------------------- Getter - Setters des services
    /**
     * @return the auditService
     */
    public AuditService getAuditService() {
        return auditService;
    }

    /**
     * @param auditService the auditService to set
     */
    public void setAuditService(AuditService auditService) {
        this.auditService = auditService;
    }

    public AuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public TenantService getTenantService() {
        return tenantService;
    }

    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }
}
