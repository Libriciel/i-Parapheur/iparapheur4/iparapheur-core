/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2011, StarXpert, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by StarXpert and ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package fr.starxpert.iparapheur.audit.repo;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * Object to Hold the results and the stats
 * 
 * @author Emmanuel Peralta - ADULLACT-Projet
 * @author Stephane Vast - ADULLACT-Projet
 */
public class AuditQueryResult {
    private Serializable responses;
    private double moyenne;
    private double variance;
    private double ecartType;

    private boolean hasStats;

    public AuditQueryResult() {
    }

    public Serializable getResponses() {
        return responses;
    }

    public void setResponses(Serializable responses) {
        this.responses = responses;
    }

    public double getEcartType() {
        return ecartType;
    }

    public void setEcartType(double ecartType) {
        this.ecartType = ecartType;
    }

    public double getMoyenne() {
        return moyenne;
    }

    public void setMoyenne(double moyenne) {
        this.moyenne = moyenne;
    }

    public double getVariance() {
        return variance;
    }

    public void setVariance(double variance) {
        this.variance = variance;
    }

    public boolean hasStats() {
        return hasStats;
    }

    public void setHasStats(boolean hasStats) {
        this.hasStats = hasStats;
    }

    public void bindToModel(Map<String, Object> model) {
        model.put("ecartType", formatDuree(ecartType));
        model.put("moyenne", formatDuree(moyenne));
        model.put("variance", variance);
    }

    private String formatDuree(double nbSecondes) {
        int secondes = (int)(nbSecondes % 60);
        int minutes = (int)((nbSecondes / 60) % 60);
        int heures = (int)((nbSecondes / 3600) % 24);
        int jours = (int)(nbSecondes / (3600*24));
        String ficelle = (jours == 0) ? "": (jours + " jour" + ((jours > 1)? "s ":" "));
        ficelle += (heures == 0) ? "": (heures + " heure" + ((heures > 1)? "s ":" "));
        ficelle += (minutes == 0) ? "": (minutes + " minute" + ((minutes > 1)? "s ":" "));
        ficelle += (secondes == 0) ? "": (secondes + " seconde" + ((secondes > 1)? "s":""));
        return ficelle;
    }

}
