
package com.atolcd.archiland.wsmodel._1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeDocument.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeDocument">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="deliberation"/>
 *     &lt;enumeration value="annexeDeliberation"/>
 *     &lt;enumeration value="signatureDeliberation"/>
 *     &lt;enumeration value="arreteReglementaire"/>
 *     &lt;enumeration value="annexeArreteReglementaire"/>
 *     &lt;enumeration value="signatureArreteReglementaire"/>
 *     &lt;enumeration value="arreteIndividuel"/>
 *     &lt;enumeration value="annexeArreteIndividuel"/>
 *     &lt;enumeration value="signatureArreteIndividuel"/>
 *     &lt;enumeration value="contratConvention"/>
 *     &lt;enumeration value="annexeContratConvention"/>
 *     &lt;enumeration value="documentBudgetaire"/>
 *     &lt;enumeration value="annexeDocumentBudgetaire"/>
 *     &lt;enumeration value="autreActe"/>
 *     &lt;enumeration value="annexeAutreActe"/>
 *     &lt;enumeration value="pes"/>
 *     &lt;enumeration value="pesacknack"/>
 *     &lt;enumeration value="elementPaye"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeDocument")
@XmlEnum
public enum TypeDocument {

    @XmlEnumValue("deliberation")
    DELIBERATION("deliberation"),
    @XmlEnumValue("annexeDeliberation")
    ANNEXE_DELIBERATION("annexeDeliberation"),
    @XmlEnumValue("signatureDeliberation")
    SIGNATURE_DELIBERATION("signatureDeliberation"),
    @XmlEnumValue("arreteReglementaire")
    ARRETE_REGLEMENTAIRE("arreteReglementaire"),
    @XmlEnumValue("annexeArreteReglementaire")
    ANNEXE_ARRETE_REGLEMENTAIRE("annexeArreteReglementaire"),
    @XmlEnumValue("signatureArreteReglementaire")
    SIGNATURE_ARRETE_REGLEMENTAIRE("signatureArreteReglementaire"),
    @XmlEnumValue("arreteIndividuel")
    ARRETE_INDIVIDUEL("arreteIndividuel"),
    @XmlEnumValue("annexeArreteIndividuel")
    ANNEXE_ARRETE_INDIVIDUEL("annexeArreteIndividuel"),
    @XmlEnumValue("signatureArreteIndividuel")
    SIGNATURE_ARRETE_INDIVIDUEL("signatureArreteIndividuel"),
    @XmlEnumValue("contratConvention")
    CONTRAT_CONVENTION("contratConvention"),
    @XmlEnumValue("annexeContratConvention")
    ANNEXE_CONTRAT_CONVENTION("annexeContratConvention"),
    @XmlEnumValue("documentBudgetaire")
    DOCUMENT_BUDGETAIRE("documentBudgetaire"),
    @XmlEnumValue("annexeDocumentBudgetaire")
    ANNEXE_DOCUMENT_BUDGETAIRE("annexeDocumentBudgetaire"),
    @XmlEnumValue("autreActe")
    AUTRE_ACTE("autreActe"),
    @XmlEnumValue("annexeAutreActe")
    ANNEXE_AUTRE_ACTE("annexeAutreActe"),
    @XmlEnumValue("pes")
    PES("pes"),
    @XmlEnumValue("pesacknack")
    PESACKNACK("pesacknack"),
    @XmlEnumValue("elementPaye")
    ELEMENT_PAYE("elementPaye");
    private final String value;

    TypeDocument(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeDocument fromValue(String v) {
        for (TypeDocument c: TypeDocument.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
