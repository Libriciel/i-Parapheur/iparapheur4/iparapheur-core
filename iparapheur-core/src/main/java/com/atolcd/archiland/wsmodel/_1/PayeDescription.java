
package com.atolcd.archiland.wsmodel._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PayeDescription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayeDescription">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="collectivite" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="service" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datePaye" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;sequence>
 *           &lt;element name="fichiers" type="{http://www.atolcd.com/archiland/wsmodel/1.0}Document" maxOccurs="unbounded"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayeDescription", propOrder = {
    "collectivite",
    "service",
    "datePaye",
    "fichiers"
})
public class PayeDescription {

    @XmlElement(required = true)
    protected String collectivite;
    protected String service;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datePaye;
    @XmlElement(required = true)
    protected List<Document> fichiers;

    /**
     * Gets the value of the collectivite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectivite() {
        return collectivite;
    }

    /**
     * Sets the value of the collectivite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectivite(String value) {
        this.collectivite = value;
    }

    /**
     * Gets the value of the service property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getService() {
        return service;
    }

    /**
     * Sets the value of the service property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setService(String value) {
        this.service = value;
    }

    /**
     * Gets the value of the datePaye property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatePaye() {
        return datePaye;
    }

    /**
     * Sets the value of the datePaye property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatePaye(XMLGregorianCalendar value) {
        this.datePaye = value;
    }

    /**
     * Gets the value of the fichiers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fichiers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFichiers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Document }
     * 
     * 
     */
    public List<Document> getFichiers() {
        if (fichiers == null) {
            fichiers = new ArrayList<Document>();
        }
        return this.fichiers;
    }

}
