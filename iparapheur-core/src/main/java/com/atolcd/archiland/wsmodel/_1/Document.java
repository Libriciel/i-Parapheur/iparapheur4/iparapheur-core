
package com.atolcd.archiland.wsmodel._1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{http://www.atolcd.com/archiland/wsmodel/1.0}TypeDocument"/>
 *         &lt;element name="fichier" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="copy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", propOrder = {

})
public class Document {

    @XmlElement(required = true)
    protected String nom;
    @XmlElement(required = true)
    protected TypeDocument type;
    @XmlElement(required = true)
    protected byte[] fichier;
    @XmlElementRef(name = "copy", namespace = "http://www.atolcd.com/archiland/wsmodel/1.0", type = JAXBElement.class)
    protected JAXBElement<Boolean> copy;

    /**
     * Gets the value of the nom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets the value of the nom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom(String value) {
        this.nom = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link TypeDocument }
     *     
     */
    public TypeDocument getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDocument }
     *     
     */
    public void setType(TypeDocument value) {
        this.type = value;
    }

    /**
     * Gets the value of the fichier property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFichier() {
        return fichier;
    }

    /**
     * Sets the value of the fichier property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFichier(byte[] value) {
        this.fichier = ((byte[]) value);
    }

    /**
     * Gets the value of the copy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getCopy() {
        return copy;
    }

    /**
     * Sets the value of the copy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setCopy(JAXBElement<Boolean> value) {
        this.copy = ((JAXBElement<Boolean> ) value);
    }

}
