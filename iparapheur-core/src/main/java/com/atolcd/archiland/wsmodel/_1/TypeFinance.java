
package com.atolcd.archiland.wsmodel._1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeFinance.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeFinance">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="recette"/>
 *     &lt;enumeration value="depense"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeFinance")
@XmlEnum
public enum TypeFinance {

    @XmlEnumValue("recette")
    RECETTE("recette"),
    @XmlEnumValue("depense")
    DEPENSE("depense");
    private final String value;

    TypeFinance(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeFinance fromValue(String v) {
        for (TypeFinance c: TypeFinance.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
