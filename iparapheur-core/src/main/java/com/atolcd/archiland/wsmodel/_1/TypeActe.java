
package com.atolcd.archiland.wsmodel._1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeActe.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeActe">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="deliberation"/>
 *     &lt;enumeration value="arreteReglementaire"/>
 *     &lt;enumeration value="arreteIndividuel"/>
 *     &lt;enumeration value="contratConvention"/>
 *     &lt;enumeration value="documentBudgetaire"/>
 *     &lt;enumeration value="autreActe"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeActe")
@XmlEnum
public enum TypeActe {

    @XmlEnumValue("deliberation")
    DELIBERATION("deliberation"),
    @XmlEnumValue("arreteReglementaire")
    ARRETE_REGLEMENTAIRE("arreteReglementaire"),
    @XmlEnumValue("arreteIndividuel")
    ARRETE_INDIVIDUEL("arreteIndividuel"),
    @XmlEnumValue("contratConvention")
    CONTRAT_CONVENTION("contratConvention"),
    @XmlEnumValue("documentBudgetaire")
    DOCUMENT_BUDGETAIRE("documentBudgetaire"),
    @XmlEnumValue("autreActe")
    AUTRE_ACTE("autreActe");
    private final String value;

    TypeActe(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeActe fromValue(String v) {
        for (TypeActe c: TypeActe.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
