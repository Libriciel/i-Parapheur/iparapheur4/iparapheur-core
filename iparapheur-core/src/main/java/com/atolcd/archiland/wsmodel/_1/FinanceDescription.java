
package com.atolcd.archiland.wsmodel._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FinanceDescription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinanceDescription">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="collectivite" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="service" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="budget" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{http://www.atolcd.com/archiland/wsmodel/1.0}TypeFinance"/>
 *         &lt;element name="dateRecuperation" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="dateGeneration" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="dateFinance" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;sequence>
 *           &lt;element name="fichiers" type="{http://www.atolcd.com/archiland/wsmodel/1.0}Document" maxOccurs="unbounded"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinanceDescription", propOrder = {
    "collectivite",
    "service",
    "nom",
    "budget",
    "type",
    "dateRecuperation",
    "dateGeneration",
    "dateFinance",
    "fichiers"
})
public class FinanceDescription {

    @XmlElement(required = true)
    protected String collectivite;
    protected String service;
    @XmlElement(required = true)
    protected String nom;
    @XmlElement(required = true)
    protected String budget;
    @XmlElement(required = true)
    protected TypeFinance type;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateRecuperation;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateGeneration;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateFinance;
    @XmlElement(required = true)
    protected List<Document> fichiers;

    /**
     * Gets the value of the collectivite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectivite() {
        return collectivite;
    }

    /**
     * Sets the value of the collectivite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectivite(String value) {
        this.collectivite = value;
    }

    /**
     * Gets the value of the service property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getService() {
        return service;
    }

    /**
     * Sets the value of the service property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setService(String value) {
        this.service = value;
    }

    /**
     * Gets the value of the nom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets the value of the nom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom(String value) {
        this.nom = value;
    }

    /**
     * Gets the value of the budget property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBudget() {
        return budget;
    }

    /**
     * Sets the value of the budget property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBudget(String value) {
        this.budget = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link TypeFinance }
     *     
     */
    public TypeFinance getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFinance }
     *     
     */
    public void setType(TypeFinance value) {
        this.type = value;
    }

    /**
     * Gets the value of the dateRecuperation property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateRecuperation() {
        return dateRecuperation;
    }

    /**
     * Sets the value of the dateRecuperation property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateRecuperation(XMLGregorianCalendar value) {
        this.dateRecuperation = value;
    }

    /**
     * Gets the value of the dateGeneration property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateGeneration() {
        return dateGeneration;
    }

    /**
     * Sets the value of the dateGeneration property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateGeneration(XMLGregorianCalendar value) {
        this.dateGeneration = value;
    }

    /**
     * Gets the value of the dateFinance property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateFinance() {
        return dateFinance;
    }

    /**
     * Sets the value of the dateFinance property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateFinance(XMLGregorianCalendar value) {
        this.dateFinance = value;
    }

    /**
     * Gets the value of the fichiers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fichiers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFichiers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Document }
     * 
     * 
     */
    public List<Document> getFichiers() {
        if (fichiers == null) {
            fichiers = new ArrayList<Document>();
        }
        return this.fichiers;
    }

}
