
package com.atolcd.archiland.ws._1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.atolcd.archiland.ws._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.atolcd.archiland.ws._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateFinance }
     * 
     */
    public CreateFinance createCreateFinance() {
        return new CreateFinance();
    }

    /**
     * Create an instance of {@link SimpleResponse }
     * 
     */
    public SimpleResponse createSimpleResponse() {
        return new SimpleResponse();
    }

    /**
     * Create an instance of {@link CreateActe }
     * 
     */
    public CreateActe createCreateActe() {
        return new CreateActe();
    }

    /**
     * Create an instance of {@link CreatePaye }
     * 
     */
    public CreatePaye createCreatePaye() {
        return new CreatePaye();
    }

    /**
     * Create an instance of {@link UpdateStatus }
     * 
     */
    public UpdateStatus createUpdateStatus() {
        return new UpdateStatus();
    }

}
