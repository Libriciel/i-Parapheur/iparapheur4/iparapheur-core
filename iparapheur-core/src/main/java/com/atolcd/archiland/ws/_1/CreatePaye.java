
package com.atolcd.archiland.ws._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.atolcd.archiland.wsmodel._1.PayeDescription;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requete" type="{http://www.atolcd.com/archiland/wsmodel/1.0}PayeDescription"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requete"
})
@XmlRootElement(name = "createPaye")
public class CreatePaye {

    @XmlElement(required = true)
    protected PayeDescription requete;

    /**
     * Gets the value of the requete property.
     * 
     * @return
     *     possible object is
     *     {@link PayeDescription }
     *     
     */
    public PayeDescription getRequete() {
        return requete;
    }

    /**
     * Sets the value of the requete property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayeDescription }
     *     
     */
    public void setRequete(PayeDescription value) {
        this.requete = value;
    }

}
