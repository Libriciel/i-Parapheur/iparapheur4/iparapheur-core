
package com.atolcd.archiland.ws._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.atolcd.archiland.wsmodel._1.ArchiveTransferReply;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requete" type="{http://www.atolcd.com/archiland/wsmodel/1.0}ArchiveTransferReply"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requete"
})
@XmlRootElement(name = "updateStatus")
public class UpdateStatus {

    @XmlElement(required = true)
    protected ArchiveTransferReply requete;

    /**
     * Gets the value of the requete property.
     * 
     * @return
     *     possible object is
     *     {@link ArchiveTransferReply }
     *     
     */
    public ArchiveTransferReply getRequete() {
        return requete;
    }

    /**
     * Sets the value of the requete property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArchiveTransferReply }
     *     
     */
    public void setRequete(ArchiveTransferReply value) {
        this.requete = value;
    }

}
