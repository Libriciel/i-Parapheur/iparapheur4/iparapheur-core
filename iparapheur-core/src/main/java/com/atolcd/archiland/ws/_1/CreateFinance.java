
package com.atolcd.archiland.ws._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.atolcd.archiland.wsmodel._1.FinanceDescription;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requete" type="{http://www.atolcd.com/archiland/wsmodel/1.0}FinanceDescription"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requete"
})
@XmlRootElement(name = "createFinance")
public class CreateFinance {

    @XmlElement(required = true)
    protected FinanceDescription requete;

    /**
     * Gets the value of the requete property.
     * 
     * @return
     *     possible object is
     *     {@link FinanceDescription }
     *     
     */
    public FinanceDescription getRequete() {
        return requete;
    }

    /**
     * Sets the value of the requete property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinanceDescription }
     *     
     */
    public void setRequete(FinanceDescription value) {
        this.requete = value;
    }

}
