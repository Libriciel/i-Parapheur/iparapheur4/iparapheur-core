package com.atolcd.parapheur.repo.annotations.impl;

import com.atolcd.parapheur.repo.AnnotationService;
import com.atolcd.parapheur.repo.annotations.Annotation;
import com.atolcd.parapheur.repo.annotations.Rect;
import org.alfresco.repo.security.authentication.AuthenticationUtil;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

/**
 * AnnotationImpl
 * @author Emmanuel Peralta
 */
public class AnnotationImpl implements Annotation, Serializable {
    private static final long serialVersionUID = 1L;

    private UUID uuid;
    private String type;
    private String author;
    private Date created;
    private Rect rect;
    private Integer page;
    private boolean secretaire;
    private HashMap<String, Serializable> data;

    public AnnotationImpl(String type, Integer page, String fullname) {
        this.uuid = UUID.randomUUID();
        this.created = new Date();
        this.author = fullname;
        this.data = new HashMap<String, Serializable>();
        this.secretaire = false;
        this.type = type;
        this.page = page;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public UUID getUUID() {
        return uuid;
    }

    @Override
    public void setUUID(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public Date getDate() {
        return this.created;
    }

    @Override
    public void updateDate() {
        this.created = new Date();
    }

    @Override
    public String getPenColor() {
        return (String) data.get(AnnotationService.PEN_COLOR);
    }

    @Override
    public void setPenColor(String penColor) {
        this.data.put(AnnotationService.PEN_COLOR, penColor);
    }

    @Override
    public String getFillColor() {
        return (String) data.get(AnnotationService.PEN_COLOR);
    }

    @Override
    public void setFillColor(String fillColor) {
        this.data.put(AnnotationService.FILL_COLOR, fillColor);
    }

    @Override
    public Rect getRect() {
        return rect;
    }

    @Override
    public void setRect(Rect rect) {
        this.rect = rect;
    }

    @Override
    public String getText() {
        return (String) data.get(AnnotationService.TEXT);
    }

    @Override
    public void setText(String text) {
        this.data.put(AnnotationService.TEXT, text);
    }

    @Override
    public String getPicto() {
        return (String)this.data.get(AnnotationService.PICTO);

    }

    @Override
    public void setPicto(String picto) {
        this.data.put(AnnotationService.PICTO, picto);
    }

    @Override
    public boolean isSecretaire() {
        return secretaire;
    }

    @Override
    public void setSecretaire(boolean secretaire) {
        this.secretaire = secretaire;
    }

    @Override
    public String toString() {
        return "(" + getText() + ")";
    }
}
