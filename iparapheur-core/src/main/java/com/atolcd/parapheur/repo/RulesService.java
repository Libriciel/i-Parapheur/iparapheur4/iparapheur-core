/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atolcd.parapheur.repo;

import java.util.List;
import java.util.Map;

import org.adullact.iparapheur.rules.bean.CustomProperty;

/**
 *
 * @author viv
 */
public interface RulesService {

    Map<String, Object> selectWorkflowWithoutPropertiesError(String script, List<CustomProperty<? extends Object>> properties);

    Map<String, Object> selectWorkflow(String script, List<CustomProperty<? extends Object>> properties);

}
