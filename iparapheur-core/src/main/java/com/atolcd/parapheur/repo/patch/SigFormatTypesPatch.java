/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo.patch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author Vivien Barousse
 */
public class SigFormatTypesPatch extends XPathBasedPatch {

    public static final String SUBTYPES_XPATH_QUERY = "/app:company_home"
            + "/app:dictionary/cm:metiertype/*";

    private static final Logger logger = Logger.getLogger(SigFormatTypesPatch.class);

    private ContentService contentService;

    private void patchNode(NodeRef nodeRef) {
        try {
            ContentReader reader = contentService.getReader(nodeRef, ContentModel.PROP_CONTENT);
            SAXReader saxreader = new SAXReader();
            Document docXml = saxreader.read(reader.getContentInputStream());
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierTypes")) {
                Iterator<Element> itMetierType = rootElement.elementIterator("MetierType");
                for (Iterator<Element> ie = itMetierType; ie.hasNext();) {
                    Element mtype = ie.next();
                    if (mtype.element("sigFormat") == null) {
                        String sigFormat = "PKCS#7/single";
                        if ("HELIOS".equals(mtype.element("TdT").element("Protocole").getTextTrim())) {
                            sigFormat = "XAdES/enveloped";
                        }
                        mtype.addElement("sigFormat").setText(sigFormat);
                    }
                }
            }

            ByteArrayOutputStream out = new ByteArrayOutputStream();

            ContentWriter contentWriter = contentService.getWriter(nodeRef, ContentModel.PROP_CONTENT, true);
            XMLWriter xmlWriter = new XMLWriter(out);
            xmlWriter.write(docXml);

            contentWriter.putContent(new ByteArrayInputStream(out.toByteArray()));
        } catch (Exception ex) {
            throw new RuntimeException("Unable to patch " + nodeRef, ex);
        }
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    @Override
    protected String getXPathQuery() {
        return SUBTYPES_XPATH_QUERY;
    }

    @Override
    protected void patch(NodeRef nodeToPatch) {
        patchNode(nodeToPatch);
    }

}
