/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo.job;

import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.cmr.repository.NodeRef;

public abstract class AbstractBatchJob extends AbstractJob {

    @Override
    public Void doWork() {
        for (NodeRef lockedNode : lockedNodes) {

            RetryingTransactionHelper transactionHelper = transactionService.getRetryingTransactionHelper();
            final NodeRef fLockedNode = lockedNode;
            transactionHelper.setMaxRetries(5);
            transactionHelper.setRetryWaitIncrementMs(1);
            transactionHelper.setMinRetryWaitMs(1000);

            RetryingTransactionHelper.RetryingTransactionCallback<Void> callback = new RetryingTransactionHelper.RetryingTransactionCallback<Void>() {
                @Override
                public Void execute() throws Throwable {
                    doWorkUnitInTransaction(fLockedNode);
                    return null;  //To change body of implemented methods use File | Settings | File Templates.
                }
            };

            try {
                transactionHelper.doInTransaction(callback, false, true);
            } catch (Exception e) {
                putExceptionForDossier(e, lockedNode);
            } finally {
                /* always unlock on exception , on success it depends on shall unlock */
                //TODO: Find Why shallUnlock value is set to false (and why it doesn't unlock at all...
                if (shallUnlock || exceptionMap.get(lockedNode) != null) {
                    logger.debug("Unlocking Node");
                    try {
                        jobService.unlockNodeInTransaction(lockedNode);
                    } catch (RuntimeException e) {
                        //TODO: handle it in a properway
                        e.printStackTrace();

                    }
                }
            }
        }
        return null;
    }

    protected abstract Void doWorkUnitInTransaction(NodeRef dossier);


}
