/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.repo;

import com.atolcd.parapheur.repo.job.JobInterface;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;

/**
 * This service handles the posting of jobs to the workqueue for later execution.
 * It also handles the locking and unlocking of the nodes that are modified by a job.
 *
 * @author Emmanuel Peralta - ADULLACT Projet
 * @since 3.3.0
 */
public interface JobService {


    /**
     * Locks a NodeRef by setting IN_BATCH_QUEUE property to true
     * @param nodeRef the nodeRef to lock
     * @since 3.3.0
     */
    public void lockNode(NodeRef nodeRef);

    /**
     * Unlocks a NodeRef by removing IN_BATCH_QUEUE property
     * @see com.atolcd.parapheur.model.ParapheurModel.PROP_IN_BATCH_QUEUE
     * @param nodeRef the nodeRef to unlock
     * @since 3.3.0
     */
    public void unlockNode(NodeRef nodeRef);

    /**
     * Unlocks a NodeRef by removing IN_BATCH_QUEUE property
     * inside a transaction.
     * @see com.atolcd.parapheur.model.ParapheurModel.PROP_IN_BATCH_QUEUE
     * @since 3.3.2
     */
    public void unlockNodeInTransaction(NodeRef nodeRef);

    /**
     * Posts a job to the internal workqueue to be runned later
     * @param j the job to be posted to the workqueue
     * @see com.atolcd.parapheur.repo.job.AbstractJob
     * @since 3.3.0
     */
    public void postJob(JobInterface j);

    /**
     * Higher level version of postJob this method takes a job and a list of nodeRefs
     * to lock.
     * @param job the job to be posted
     * @param nodeRefs the list of nodeRef to lock
     * @since 3.3.0
     */
    public void postJobAndLockNodes(JobInterface job, List<NodeRef> nodeRefs);

    /**
     * Unlock all the nodeRefs passed as parameter
     * @param nodeRefs the list of the nodeRefs to unlock
     * @since 3.3.0
     */
    public void unlockNodes(List<NodeRef> nodeRefs);

    /**
     * Generate and send a report to the user about the batch Operation.
     * @param job the job that has been run.
     */
    public void sendReportToUser(JobInterface job);

    /**
     * Returns the current working mode of JobService
     * @return true if the background thread is enabled false otherwise.
     * @since 3.3.1p
     */
    public boolean isBackgroundWorkEnabled();
}
