package com.atolcd.parapheur.repo;
/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * contact@atolcd.com
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

/**
 * ParapheurUserPreferences
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 13/04/12
 *         Time: 10:06
 */
public interface ParapheurUserPreferences {
    public static final String NOTIFICATION_ENABLED_PREFERENCE = "org.adullact.iparapheur.notifications.enabled";
    public static final String MOBILE_NOTIFICATIONS_ENABLED_PREFERENCE = "org.adullact.iparapheur.notifications.mobile.enabled";
    public static final String DAILY_DIGEST_ENABLED_PREFERENCE = "org.adullact.iparapheur.notifications.dailydigest.enabled";
    public static final String MOBILE_DEVICE_TOKEN_PREFERENCE = "org.adullact.iparapheur.notifications.mobile.token";
    public static final String DIGEST_CRON_PREFERENCE = "org.adullact.iparapheur.notifications.digest.cron";
    public static final String NOTIFICATION_MAIL_PREFERENCE = "org.adullact.iparapheur.notifications.mail";

    public void enableNotificationsForUsername(String username);
    public void disableNotificationsForUsername(String username);
    public Boolean isNotificationsEnabledForUsername(String username);

    public void enableMobileNotificationsForUsername(String username);
    public void disableMobileNotificationsForUsername(String username);
    public Boolean isMobileNotificationsEnabledForUsername(String username);

    public void enableDailyDigestForUsername(String username);
    public void disableDailyDigestForUsername(String username);
    public Boolean isDailyDigestEnabledForUsername(String username);

    public void setMobileUUIDDeviceForUsername(String username, String mobileUUID);
    public String getMobileUUIDDeviceForUsername(String username);

    public void setDigestCronForUsername(String username, String cronexp);
    public String getDigestCronForUsername(String username);
    
    public String[] getNotificationMailForUsername(String username);
    public void setNotificationMailForUsername(String username, String mail);

}
