package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.namespace.QName;

/**
 * SignatureScanMigrationPatch
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 14/03/12
 *         Time: 15:16
 */
public class SignatureScanMigrationPatch extends XPathBasedPatch {
    private ContentService contentService;

    @Override
    protected void patch(NodeRef nodeToPatch) throws Exception {
        
        ContentReader imageReader = null;
        if (getNodeService().getProperty(nodeToPatch, ParapheurModel.PROP_USER_SIGNATURE) != null) {
            if (getNodeService().hasAspect(nodeToPatch, ParapheurModel.ASPECT_SIGNATURESCAN)) {
                getNodeService().removeProperty(nodeToPatch, ParapheurModel.PROP_USER_SIGNATURE);
            } else {
                /* Ajout de l'aspect afin de pouvoir utiliser l'association ASSOC_SIGNATURE_SCAN */
                getNodeService().addAspect(nodeToPatch, ParapheurModel.ASPECT_SIGNATURESCAN, null);

                /* Création du nœud fils avec pour type d'association ASSOC_SIGNATURE_SCAN */
                ChildAssociationRef childAssociationRef = getNodeService().createNode(nodeToPatch,
                        ParapheurModel.ASSOC_SIGNATURE_SCAN,
                        QName.createQName("cm:signature"),
                        ContentModel.TYPE_CONTENT);

                /* Lecture de la signature depuis la propriété PROP_USER_SIGNATURE */
                imageReader = contentService.getReader(nodeToPatch, ParapheurModel.PROP_USER_SIGNATURE);

                if (imageReader.getSize() > 0) {

                    ContentWriter imageWriter = contentService.getWriter(childAssociationRef.getChildRef(),
                            ContentModel.PROP_CONTENT, true);
                    /* Ecriture du contenu de la signature dans PROP_CONTENT du nouveau nœud */
                    imageWriter.putContent(imageReader);
                }

                /* Supression de la "vielle property" */
                getNodeService().removeProperty(nodeToPatch, ParapheurModel.PROP_USER_SIGNATURE);
            }
        }
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }
    
    @Override
    protected String getXPathQuery() {
        return "//*[subtypeOf('cm:person')]";
    }
}

