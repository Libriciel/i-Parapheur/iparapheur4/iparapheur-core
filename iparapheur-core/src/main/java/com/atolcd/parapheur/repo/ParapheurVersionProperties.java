/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2012, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.repo;

import java.io.IOException;
import java.util.Properties;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

public class ParapheurVersionProperties implements InitializingBean
{
   private static final String PARAPHEUR_VERSION_NUMBER = "ph_version";
   private static final String PARAPHEUR_BUILD_NUMBER = "ph_build";
   
   private Properties parapheurProperties;
   
   /**
    * Charge les propriétés du parapheur depuis un fichier
    * 
    * @param descriptorResource  ressource contenant les méta-données du parapheur
    * @throws IOException
    */
   public void setDescriptor(Resource descriptorResource)
       throws IOException
   {
       this.parapheurProperties = new Properties();
       this.parapheurProperties.load(descriptorResource.getInputStream());
   }
   
   public String getVersionNumber()
   {
      return (String) this.parapheurProperties.get(PARAPHEUR_VERSION_NUMBER);
   }
   
   public String getBuildNumber()
   {
      return (String) this.parapheurProperties.get(PARAPHEUR_BUILD_NUMBER);
   }

   /**
    * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
    */
   @Override
   public void afterPropertiesSet() throws Exception
   {
      Assert.notNull(getVersionNumber(), "Aucun numéro de version trouvé");
      Assert.notNull(getBuildNumber(), "Aucun numéro de build trouvé");
      
   }
}
