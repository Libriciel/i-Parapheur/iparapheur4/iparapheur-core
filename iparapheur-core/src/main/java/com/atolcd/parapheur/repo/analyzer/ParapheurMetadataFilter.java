/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package com.atolcd.parapheur.repo.analyzer;

import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;
import org.apache.lucene.analysis.Token;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardTokenizer;

/**
 * ParapheurMetadataFilter
 *
 * @author Emmanuel Peralta - Adullact Projet
 *         Date: 19/12/11
 *         Time: 11:43
 */
public class ParapheurMetadataFilter extends TokenFilter {

    /** Construct filtering <i>in</i>. */
    public ParapheurMetadataFilter(TokenStream in)
    {
        super(in);
    }

    private static final String APOSTROPHE_TYPE = StandardTokenizer.TOKEN_TYPES[StandardTokenizer.APOSTROPHE];

    private static final String ACRONYM_TYPE = StandardTokenizer.TOKEN_TYPES[StandardTokenizer.ACRONYM];

    private static final String HOST_TYPE = StandardTokenizer.TOKEN_TYPES[StandardTokenizer.HOST];

    private static final String ALPHANUM_TYPE = StandardTokenizer.TOKEN_TYPES[StandardTokenizer.ALPHANUM];

    private Queue<Token> hostTokens = null;

    /**
     * Returns the next token in the stream, or null at EOS.
     * <p>
     * Removes <tt>'s</tt> from the end of words.
     * <p>
     * Removes dots from acronyms.
     * <p>
     * Splits host names ...
     */
    @Override
    public final org.apache.lucene.analysis.Token next() throws java.io.IOException
    {
        if (hostTokens == null)
        {
            org.apache.lucene.analysis.Token t = input.next();

            if (t == null)
                return null;

            String text = t.termText();
            String type = t.type();

            if (type.equals(APOSTROPHE_TYPE) && // remove 's
                    (text.endsWith("'s") || text.endsWith("'S")))
            {
                return new Token(text.substring(0, text.length() - 2), t.startOffset(), t
                        .endOffset(), type);

            }
            else if (type.equals(ACRONYM_TYPE))
            { // remove dots
                StringBuffer trimmed = new StringBuffer();
                for (int i = 0; i < text.length(); i++)
                {
                    char c = text.charAt(i);
                    if (c != '.')
                        trimmed.append(c);
                }
                return new Token(trimmed.toString(), t.startOffset(), t.endOffset(), type);

            }
            else if (type.equals(HOST_TYPE))
            {
                // <HOST: <ALPHANUM> ("." <ALPHANUM>)+ >
                // There must be at least two tokens ....
                hostTokens = new LinkedList<org.apache.lucene.analysis.Token>();
                StringTokenizer tokeniser = new StringTokenizer(text, ".");
                int start = t.startOffset();
                int end;
                while (tokeniser.hasMoreTokens())
                {
                    String token = tokeniser.nextToken();
                    end = start + token.length();
                    hostTokens.offer(new Token(token, start, end, ALPHANUM_TYPE));
                    start = end + 1;
                }
                // check if we have an acronym ..... yes a.b.c ends up here ...

                if (text.length() == hostTokens.size() * 2 - 1)
                {
                    hostTokens = null;
                    // acronym
                    StringBuffer trimmed = new StringBuffer();
                    for (int i = 0; i < text.length(); i++)
                    {
                        char c = text.charAt(i);
                        if (c != '.')
                            trimmed.append(c);
                    }
                    return new Token(trimmed.toString(), t.startOffset(), t.endOffset(),
                            ALPHANUM_TYPE);
                }
                else
                {
                    return hostTokens.remove();
                }
            }
            else
            {
                return t;
            }
        }
        else
        {
            Token token = hostTokens.remove();
            if (hostTokens.isEmpty())
            {
                hostTokens = null;
            }
            return token;
        }
    }
}
