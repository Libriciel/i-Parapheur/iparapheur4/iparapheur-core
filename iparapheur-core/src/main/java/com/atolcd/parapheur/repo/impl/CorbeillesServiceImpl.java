/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import org.adullact.iparapheur.repo.amq.MessagesSender;
import org.adullact.iparapheur.repo.notification.NotificationService;
import org.adullact.iparapheur.repo.worker.WorkerService;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.policy.Behaviour.NotificationFrequency;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEvent;
import org.springframework.extensions.surf.util.AbstractLifecycleBean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Vivien Barousse
 */
public class CorbeillesServiceImpl extends AbstractLifecycleBean implements CorbeillesService {
    private static boolean ISINIT = false;
    private static Logger logger = Logger.getLogger(CorbeillesService.class);
    private NodeService nodeService;
    private PolicyComponent policyComponent;
    private ParapheurService parapheurService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private MessagesSender messagesSender;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    @Qualifier("tenantAdminService")
    private TenantAdminService tenantAdminService;
    @Autowired
    private TenantService tenantService;

    @Autowired
    private SearchService searchService;

    public void init() {
        if (!ISINIT) {
            policyComponent.bindClassBehaviour(
                    NodeServicePolicies.OnCreateNodePolicy.QNAME,
                    ParapheurModel.TYPE_DOSSIER,
                    new JavaBehaviour(this, "onDossierCreated", NotificationFrequency.TRANSACTION_COMMIT));

            policyComponent.bindClassBehaviour(
                    NodeServicePolicies.OnDeleteNodePolicy.QNAME,
                    ParapheurModel.TYPE_DOSSIER,
                    new JavaBehaviour(this, "onDossierDeleted"));

            policyComponent.bindClassBehaviour(
                    NodeServicePolicies.OnAddAspectPolicy.QNAME,
                    ParapheurModel.ASPECT_LU,
                    new JavaBehaviour(this, "onDossierAddAspect", NotificationFrequency.FIRST_EVENT));
            ISINIT = true;
        }
    }

    @Override
    protected void onBootstrap(ApplicationEvent applicationEvent) {
        logger.setLevel(Level.INFO);
        logger.info("Updating late folders...");
        try {
            //Initialisation des consumers et producers
            List<Tenant> tenants = tenantAdminService.getAllTenants();
            if (!tenants.isEmpty()) {
                //Si parapheur multi-tenant, création des queues par tenant
                for (Tenant t : tenants) {
                    if(t.isEnabled()) {
                        String adminUser = tenantService.getDomainUser("admin", t.getTenantDomain());
                        AuthenticationUtil.runAs(() -> {
                                transactionService.getRetryingTransactionHelper().doInTransaction(() -> {
                                    List<NodeRef> folders = findLate();
                                    if(folders.size() > 500) {
                                        logger.warn(String.format("There are %d late folders, please consider using a longer validation period", folders.size()));
                                    }
                                    for (NodeRef result : folders) {
                                        // On ajoute le dossier dans la bannette en retard (de l'émetteur et du bureau courant) s'il n'y est pas déjà.
                                        updateFolderLateRelationship(result);
                                        // Le mail passe maintenant par le service de notification (appelé dans updateLate)
                                    }
                                    return null;
                                });
                            return null;
                        }, adminUser);
                    }
                }
            }
            AuthenticationUtil.runAs(() -> {
                transactionService.getRetryingTransactionHelper().doInTransaction(() -> {
                    List<NodeRef> folders = findLate();
                    if(folders.size() > 500) {
                        logger.warn(String.format("There are %d late folders, please consider using a longer validation period", folders.size()));
                    }
                    for (NodeRef result : folders) {
                        // On ajoute le dossier dans la bannette en retard (de l'émetteur et du bureau courant) s'il n'y est pas déjà.
                        updateFolderLateRelationship(result);
                        // Le mail passe maintenant par le service de notification (appelé dans updateLate)
                    }
                    return null;
                });
                return null;
            }, AuthenticationUtil.getSystemUserName());

            //création de seulement une queue pour le tenant root
            logger.info("Late folders update complete");
        } catch (Exception e) {
            logger.error("Late folders update error", e);
        }
        init();
    }

    @Override
    protected void onShutdown(ApplicationEvent applicationEvent) {
        // Nothing to do on shutdown
    }

    public void onDossierCreated(ChildAssociationRef car) {
        try {
            JSONObject onDossierCreatedObj = new JSONObject();
            onDossierCreatedObj.put(WorkerService.PARENT, car.getParentRef().getId());
            onDossierCreatedObj.put(WorkerService.TYPE, WorkerService.TYPE_EVENT);
            onDossierCreatedObj.put(WorkerService.ACTION, DossierService.ACTION_DOSSIER.CORBEILLE_EVENT);
            onDossierCreatedObj.put(WorkerService.ID, car.getChildRef().getId());
            onDossierCreatedObj.put(WorkerService.USERNAME, AuthenticationUtil.getRunAsUser());
            onDossierCreatedObj.put(WorkerService.EVENT, "onDossierCreated");

            messagesSender.sendWorker(onDossierCreatedObj.toString());
        } catch(Exception e) {
            logger.error("Cannot update corbeille count :" + e.getMessage());
        }
    }

    @Override
    public void moveDossier(NodeRef node, NodeRef oldParent, NodeRef newParent) {
        insertIntoCorbeillesVirtuelles(node);

        Integer currentCount = (Integer) nodeService.getProperty(oldParent, ParapheurModel.PROP_CHILD_COUNT);
        nodeService.setProperty(oldParent, ParapheurModel.PROP_CHILD_COUNT, currentCount - 1);

        currentCount = (Integer) nodeService.getProperty(newParent, ParapheurModel.PROP_CHILD_COUNT);
        nodeService.setProperty(newParent, ParapheurModel.PROP_CHILD_COUNT, currentCount + 1);
    }

    public void onDossierDeleted(ChildAssociationRef car, boolean bln) {
        try {
            JSONObject onDossierDeletedObj = new JSONObject();
            onDossierDeletedObj.put(WorkerService.PARENT, car.getParentRef().getId());
            onDossierDeletedObj.put(WorkerService.TYPE, WorkerService.TYPE_EVENT);
            onDossierDeletedObj.put(WorkerService.ACTION, DossierService.ACTION_DOSSIER.CORBEILLE_EVENT);
            onDossierDeletedObj.put(WorkerService.ID, car.getChildRef().getId());
            onDossierDeletedObj.put(WorkerService.USERNAME, AuthenticationUtil.getRunAsUser());
            onDossierDeletedObj.put(WorkerService.EVENT, "onDossierDeleted");

            messagesSender.sendWorker(onDossierDeletedObj.toString());
        } catch(Exception e) {
            logger.error("Cannot update corbeille count :" + e.getMessage());
        }
    }

    public void onDossierAddAspect(NodeRef nr, QName qname) {
        if (qname.equals(ParapheurModel.ASPECT_LU)) {
            try {
                JSONObject onDossierAddAspectObj = new JSONObject();
                onDossierAddAspectObj.put(WorkerService.TYPE, WorkerService.TYPE_EVENT);
                onDossierAddAspectObj.put(WorkerService.ACTION, DossierService.ACTION_DOSSIER.CORBEILLE_EVENT);
                onDossierAddAspectObj.put(WorkerService.ID, nr.getId());
                onDossierAddAspectObj.put(WorkerService.USERNAME, AuthenticationUtil.getRunAsUser());
                onDossierAddAspectObj.put(WorkerService.EVENT, "onDossierAddAspect");

                messagesSender.sendWorker(onDossierAddAspectObj.toString());
            } catch(Exception e) {
                logger.error("Cannot update corbeille count :" + e.getMessage());
            }
        }
    }

    /**
     * Insère le dossier donné dans les corbeilles virtuelles auxquelles il
     * appartient.
     * <p/>
     * Les corbeilles auxquelles le dossier appartient sont calculées
     * automatiquement en fonction de l'état du dossier.
     * <p/>
     * cette méthode supprime les anciennes associations vers d'autres
     * corbeilles virtuelles. Les nouvelles associations sont recalculées
     * intégralement.
     *
     * @param dossier Le dossier à insérer dans les corbeilles virtuelles.
     */
    public void insertIntoCorbeillesVirtuelles(NodeRef dossier) {
        deleteVirtuallyContainsAssociations(dossier);
        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);
        EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(circuit);
        if (!circuit.get(0).isApproved() || etapeCourante == null) {
            // Si le circuit n'est pas emis, aucune association à créer.
            return;
        }

        boolean isRejete = parapheurService.isRejete(dossier);

        Date dateLimite = (Date) nodeService.getProperty(dossier, ParapheurModel.PROP_DATE_LIMITE);
        NodeRef emetteur = circuit.get(0).getParapheur();
        List<NodeRef> bureauxTraites = new ArrayList<NodeRef>();
        for (EtapeCircuit etape : circuit) {
            if (etape.isApproved() || isRejete) {
                if (etape.getDelegue() != null && !bureauxTraites.contains(etape.getDelegue())) {
                    nodeService.createAssociation(
                            parapheurService.getCorbeille(etape.getDelegue(), ParapheurModel.NAME_TRAITES),
                            dossier,
                            ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
                    raiseCorbeilleChildCount(parapheurService.getCorbeille(etape.getDelegue(), ParapheurModel.NAME_TRAITES));
                    bureauxTraites.add(etape.getDelegue());
                }
                if (!bureauxTraites.contains(etape.getParapheur())) {
                    nodeService.createAssociation(
                            parapheurService.getCorbeille(etape.getParapheur(), ParapheurModel.NAME_TRAITES),
                            dossier,
                            ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
                    raiseCorbeilleChildCount(parapheurService.getCorbeille(etape.getParapheur(), ParapheurModel.NAME_TRAITES));
                    bureauxTraites.add(etape.getParapheur());
                }
            }
        }
        Boolean signaturePapier = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_PAPIER);

        if (!parapheurService.isTermine(dossier) && !isRejete) {
            nodeService.createAssociation(
                    parapheurService.getCorbeille(emetteur, ParapheurModel.NAME_EN_COURS),
                    dossier,
                    ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
            raiseCorbeilleChildCount(parapheurService.getCorbeille(emetteur, ParapheurModel.NAME_EN_COURS));
        }

        if ((dateLimite != null) && (new Date().after(dateLimite)) && !parapheurService.isTermine(dossier) && !isRejete) {
            NodeRef corbeilleRetard = parapheurService.getCorbeille(emetteur, ParapheurModel.NAME_EN_RETARD);
            notificationService.notifierPourCorbeilleVirtuelle(
                    dossier,
                    corbeilleRetard,
                    ParapheurModel.NAME_EN_RETARD.getLocalName(),
                    true);
            insertIntoCorbeilleEnRetard(dossier, corbeilleRetard);

            if (!emetteur.equals(etapeCourante.getParapheur()) && !isRejete) {
                corbeilleRetard = parapheurService.getCorbeille(etapeCourante.getParapheur(), ParapheurModel.NAME_EN_RETARD);
                notificationService.notifierPourCorbeilleVirtuelle(
                        dossier,
                        corbeilleRetard,
                        ParapheurModel.NAME_EN_RETARD.getLocalName(),
                        true);
                insertIntoCorbeilleEnRetard(dossier, corbeilleRetard);
            }
        }

        if (EtapeCircuit.ETAPE_SIGNATURE.equals(etapeCourante.getActionDemandee())
                && Boolean.TRUE.equals(signaturePapier)) {
            nodeService.createAssociation(
                    parapheurService.getCorbeille(emetteur, ParapheurModel.NAME_A_IMPRIMER),
                    dossier,
                    ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
            raiseCorbeilleChildCount(parapheurService.getCorbeille(emetteur, ParapheurModel.NAME_A_IMPRIMER));
        }

        EtapeCircuit etapePrecedente = null;
        List<NodeRef> avenirCreated = new ArrayList<NodeRef>();
        for (EtapeCircuit etape : circuit) {
            if (!etape.isApproved() && !avenirCreated.contains(etape.getParapheur()) &&
                    !etape.equals(etapeCourante)) {

                if (etape.getParapheur() != null) {

                    nodeService.createAssociation(
                            parapheurService.getCorbeille(etape.getParapheur(), ParapheurModel.NAME_A_VENIR),
                            dossier,
                            ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
                    avenirCreated.add(etape.getParapheur());
                    raiseCorbeilleChildCount(parapheurService.getCorbeille(etape.getParapheur(), ParapheurModel.NAME_A_VENIR));
                } else {
                    Map<QName, Serializable> properties = nodeService.getProperties(dossier);

                    String creatorName = (String) properties.get(ContentModel.PROP_CREATOR);
                    String modifierName = (String) properties.get(ContentModel.PROP_MODIFIER);
                    String dossierName = (String) properties.get(ContentModel.PROP_TITLE);
                    logger.warn(String.format("Attention le dossier %s est dans un circuit contentant une etape (%s) sans parapheur[creator: %s, modifier:%s]", dossierName, etape.getActionDemandee(), creatorName, modifierName));
                    logger.warn("L'association dossier/corbeille à venir n'a pas pu être réalisée");
                }
            }

            // L'etape precedente est la derniere etape validee
            if (etape.isApproved()) {
                etapePrecedente = etape;
            }
        }

        // Récupération du premier document principal pour lecture
        NodeRef docPpal = parapheurService.getMainDocuments(dossier).get(0);
        boolean lu = nodeService.hasAspect(docPpal, ParapheurModel.ASPECT_LU);

        String lastAction = etapePrecedente.getActionDemandee();

        if (!lu && lastAction.equals(EtapeCircuit.ETAPE_VISA) && !isRejete && !parapheurService.isTermine(dossier)) {

            if (etapePrecedente.getParapheur() != null) {
                nodeService.createAssociation(
                        parapheurService.getCorbeille(etapePrecedente.getParapheur(), ParapheurModel.NAME_RECUPERABLES),
                        dossier,
                        ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
                raiseCorbeilleChildCount(parapheurService.getCorbeille(etapePrecedente.getParapheur(), ParapheurModel.NAME_RECUPERABLES));
                NodeRef delegue = etapePrecedente.getDelegue();
                if (delegue != null) {
                    nodeService.createAssociation(
                            parapheurService.getCorbeille(delegue, ParapheurModel.NAME_RECUPERABLES),
                            dossier,
                            ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
                    raiseCorbeilleChildCount(parapheurService.getCorbeille(delegue, ParapheurModel.NAME_RECUPERABLES));
                }

            } else {
                Map<QName, Serializable> properties = nodeService.getProperties(dossier);

                String creatorName = (String) properties.get(ContentModel.PROP_CREATOR);
                String modifierName = (String) properties.get(ContentModel.PROP_MODIFIER);
                String dossierName = (String) properties.get(ContentModel.PROP_TITLE);
                logger.warn(String.format("Attention le dossier %s est dans un circuit dont la dernière étape (%s) validée est sans parapheur [creator: %s, modifier:%s]", dossierName, etapePrecedente.getActionDemandee(), creatorName, modifierName));
                logger.warn("L'association dossier/corbeille récuperable n'a pas pu être réalisée");
            }
        }

        // Delegation
        // Mise à jour de l'ancien chemin de délégation
        for (NodeRef delegueOnPath : parapheurService.getParapheursOnDelegationPath(etapePrecedente.getParapheur())) {
            updateCorbeilleChildCount(parapheurService.getCorbeille(delegueOnPath, ParapheurModel.NAME_DOSSIERS_DELEGUES));
        }
        // Mise à jour du nouveau chemin de délégation
        NodeRef parapheur = parapheurService.getParentParapheur(dossier);
        if (parapheur != null) {
            NodeRef delegue = parapheurService.getDelegation(parapheur);
            if (delegue != null) {
                nodeService.createAssociation(
                        parapheurService.getCorbeille(delegue, ParapheurModel.NAME_DOSSIERS_DELEGUES),
                        dossier,
                        ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
                for (NodeRef delegueOnPath : parapheurService.getParapheursOnDelegationPath(parapheur)) {
                    updateCorbeilleChildCount(parapheurService.getCorbeille(delegueOnPath, ParapheurModel.NAME_DOSSIERS_DELEGUES));
                }
            }
        }
    }

    public void deleteVirtuallyContainsAssociations(NodeRef dossier) {
        List<AssociationRef> oldAssocs = nodeService.getSourceAssocs(dossier, ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
        for (AssociationRef oldAssoc : oldAssocs) {
            notificationService.notifierPourCorbeilleVirtuelle(
                    oldAssoc.getTargetRef(),
                    oldAssoc.getSourceRef(),
                    nodeService.getPrimaryParent(oldAssoc.getSourceRef()).getQName().getLocalName(),
                    false);
            nodeService.removeAssociation(oldAssoc.getSourceRef(),
                    oldAssoc.getTargetRef(),
                    oldAssoc.getTypeQName());
            reduceCorbeilleChildCount(oldAssoc.getSourceRef());
        }
        if (nodeService.exists(dossier) && nodeService.hasAspect(dossier, ParapheurModel.ASPECT_EN_RETARD)) {
            nodeService.removeAspect(dossier, ParapheurModel.ASPECT_EN_RETARD);
        }
    }

    public void removeDossierFromRecuperables(NodeRef dossier) {
        if(nodeService.exists(dossier)) {
            List<AssociationRef> oldAssocs = nodeService.getSourceAssocs(dossier, ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
            for (AssociationRef oldAssoc : oldAssocs) {
                NodeRef corbeille = oldAssoc.getSourceRef();
                NodeRef parapheur = parapheurService.getParentParapheur(corbeille);
                NodeRef corbeilleRecuperable = parapheurService.getCorbeille(parapheur, ParapheurModel.NAME_RECUPERABLES);

                if (corbeille.equals(corbeilleRecuperable)) {
                    nodeService.removeAssociation(oldAssoc.getSourceRef(),
                            oldAssoc.getTargetRef(),
                            oldAssoc.getTypeQName());
                }
            }
        }
    }

    private boolean shallICountTheFolder(NodeRef dossier) {
        boolean count = false;
        if (nodeService.exists(dossier)) {
            Boolean isInBatchQueue = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_IN_BATCH_QUEUE);
            count = isInBatchQueue == null || !isInBatchQueue;
        }


        return count;
    }

    private boolean checkForLate(NodeRef dossier) {
        Date limite = (Date) nodeService.getProperty(dossier, ParapheurModel.PROP_DATE_LIMITE);
        if (limite != null && new Date().after(limite)) {
            return true;
        }
        return false;
    }

    private void modifyCorbeilleChildCount(NodeRef corbeille, int modifier) {
        Integer currentCount = (Integer) nodeService.getProperty(corbeille, ParapheurModel.PROP_CHILD_COUNT);
        nodeService.setProperty(corbeille, ParapheurModel.PROP_CHILD_COUNT, currentCount + modifier);
    }

    @Override
    public void reduceCorbeilleChildCount(NodeRef corbeille) {
        modifyCorbeilleChildCount(corbeille, -1);
    }

    @Override
    public void raiseCorbeilleChildCount(NodeRef corbeille) {
        modifyCorbeilleChildCount(corbeille, 1);
    }

    @Override
    public void regenerateAllCorbeilleCount() {

        RetryingTransactionHelper rtx = new RetryingTransactionHelper();
        rtx.setMaxRetries(5);

        rtx.doInTransaction(new RetryingTransactionHelper.RetryingTransactionCallback<Object>() {
            @Override
            public Object execute() throws Throwable {
                List<NodeRef> parapheurs = parapheurService.getParapheurs();

                for(NodeRef parapheur: parapheurs) {
                    List<NodeRef> corbeilles = parapheurService.getCorbeilles(parapheur);
                    String parapheurTitle = (String) nodeService.getProperty(parapheur, ContentModel.PROP_TITLE);

                    logger.info("regenerateAllCorbeilleCount -- " + parapheurTitle);

                    for(NodeRef corbeille: corbeilles) {
                        updateCorbeilleChildCount(corbeille);
                    }
                }
                return null;
            }
        });


    }

    @Override
    public void updateCorbeilleChildCount(NodeRef corbeille) {
        int count = 0;
        boolean late = false;

        if (nodeService.getType(corbeille).equals(ParapheurModel.TYPE_CORBEILLE_VIRTUELLE)) {
            for (AssociationRef assoc : nodeService.getTargetAssocs(corbeille, ParapheurModel.ASSOC_VIRTUALLY_CONTAINS)) {
                NodeRef dossier = assoc.getTargetRef();
                if (shallICountTheFolder(dossier)) {
                    late = late || checkForLate(dossier);
                    count++;
                }
            }
            for (AssociationRef assoc : nodeService.getTargetAssocs(corbeille, ParapheurModel.ASSOC_VIRTUALLY_REFERS)) {
                count += (Integer) nodeService.getProperty(assoc.getTargetRef(), ParapheurModel.PROP_CHILD_COUNT);
            }
        } else {
            for (ChildAssociationRef assoc : nodeService.getChildAssocs(corbeille, ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL)) {
                NodeRef dossier = assoc.getChildRef();
                if (shallICountTheFolder(dossier)) {
                    late = late || checkForLate(dossier);
                    count++;
                }
            }
        }

        nodeService.setProperty(corbeille, ParapheurModel.PROP_CHILD_COUNT, count);
        nodeService.setProperty(corbeille, ParapheurModel.PROP_CORBEILLE_LATE, late);
    }

    /**
     * Renvoie la liste des dossiers à imprimer pour un parapheur donné.
     *
     * @param parapheur le parapheur courant
     * @return une liste des dossiers à imprimer
     */
    @Override
    public List<NodeRef> getDossiersAImprimer(NodeRef parapheur) {
        return getDossiersFromCorbeilleVirtuelle(parapheur, ParapheurModel.NAME_A_IMPRIMER);
    }

    /**
     * Renvoie la liste des dossiers en cours pour un parapheur donné.
     *
     * @param parapheur le parapheur courant
     * @return la liste des dossiers en cours
     */
    @Override
    public List<NodeRef> getDossiersEnCours(NodeRef parapheur) {
        return getDossiersFromCorbeilleVirtuelle(parapheur, ParapheurModel.NAME_EN_COURS);
    }

    /**
     * Renvoie la liste des dossiers traités pour un parapheur donné.
     *
     * @param parapheur le parapheur courant
     * @return la liste des dossiers traités
     */
    @Override
    public List<NodeRef> getDossiersTraites(NodeRef parapheur) {
        return getDossiersFromCorbeilleVirtuelle(parapheur, ParapheurModel.NAME_TRAITES);
    }

    /**
     * Renvoie la liste des dossiers en retard pour un parapheur donné.
     *
     * @param parapheur le parapheur courant
     * @return la liste des dossiers en retard
     */
    @Override
    public List<NodeRef> getDossiersEnRetard(NodeRef parapheur) {
        return getDossiersFromCorbeilleVirtuelle(parapheur, ParapheurModel.NAME_EN_RETARD);
    }

    /**
     * Renvoie la liste des dossiers à venir pour un parapheur donné.
     *
     * @param parapheur le parapheur courant
     * @return la liste des dossiers à venir
     */
    @Override
    public List<NodeRef> getDossiersAVenir(NodeRef parapheur) {
        return getDossiersFromCorbeilleVirtuelle(parapheur, ParapheurModel.NAME_A_VENIR);
    }

    /**
     * Renvoie la liste des dossiers délégués pour un parapheur donné.
     *
     * @param parapheur le parapheur courant
     * @return la liste des dossiers délégués
     */
    @Override
    public List<NodeRef> getDossiersDelegues(NodeRef parapheur) {
        return getDossiersFromCorbeilleVirtuelle(parapheur, ParapheurModel.NAME_DOSSIERS_DELEGUES);
    }

    /**
     * Renvoie la liste des dossiers contenus dans une corbeille virtuelle.
     * <p/>
     * La corbeille virtuelle à requeter est identifiée par son nom ainsi que
     * par le parapheur qui la contient.
     *
     * @param parapheur Le parapheur contenant la corbeille.
     * @param name      Le nom de la corbeille
     * @return La liste des dossiers contenus dans la corbeille.
     */
    @Override
    public List<NodeRef> getDossiersFromCorbeilleVirtuelle(NodeRef parapheur, QName name) {
        return getDossiersFromCorbeilleVirtuelle(parapheurService.getCorbeille(parapheur, name));
    }

    @Override
    public void insertIntoCorbeilleAImprimer(NodeRef dossier) {
        EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(dossier);
        Boolean signaturePapier = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_PAPIER);
        NodeRef parapheurCourant = etapeCourante.getParapheur();

        if (EtapeCircuit.ETAPE_SIGNATURE.equals(etapeCourante.getActionDemandee())
                && Boolean.TRUE.equals(signaturePapier)) {
            nodeService.createAssociation(
                    parapheurService.getCorbeille(parapheurCourant, ParapheurModel.NAME_A_IMPRIMER),
                    dossier,
                    ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
            raiseCorbeilleChildCount(parapheurService.getCorbeille(parapheurCourant, ParapheurModel.NAME_A_IMPRIMER));
        }
    }

    @Override
    public void regenerateDossierRelationships(NodeRef dossier) {
        insertIntoCorbeillesVirtuelles(dossier);
    }

    /**
     * @see com.atolcd.parapheur.repo.CorbeillesService#getDossiersFromCorbeilleVirtuelle(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public List<NodeRef> getDossiersFromCorbeilleVirtuelle(NodeRef corbeille) {
        List<AssociationRef> assocs = nodeService.getTargetAssocs(corbeille, RegexQNamePattern.MATCH_ALL);
        List<NodeRef> dossiers = new ArrayList<NodeRef>();
        for (AssociationRef assoc : assocs) {
            if (assoc.getTypeQName().equals(ParapheurModel.ASSOC_VIRTUALLY_CONTAINS)) {
                dossiers.add(assoc.getTargetRef());
            }
            if (assoc.getTypeQName().equals(ParapheurModel.ASSOC_VIRTUALLY_REFERS)) {
                // On suit les références.
                NodeRef corbeilleReferencee = assoc.getTargetRef();
                dossiers.addAll(getDossiersFromCorbeilleVirtuelle(corbeilleReferencee));
            }
        }

        return Collections.unmodifiableList(dossiers);
    }

    /**
     * Lucene Query to execute
     *
     * PATH:"//*" +TYPE:"{http://www.atolcd.com/alfresco/model/parapheur/1.0}dossier" +@\{http\://www.atolcd.com/alfresco/model/parapheur/1.0\}dateLimite:["1950-01-01T00:00:00.000Z" TO "yyyy-MM-dd'T'HH:mm:ss.000'Z'"]
     */
    private static final String QUERY = "PATH:\"//\\*\"+TYPE:\"{http://www.atolcd.com/alfresco/model/parapheur/1.0}dossier\"+@\\{http\\://www.atolcd.com/alfresco/model/parapheur/1.0\\}dateLimite:[\"1950-01-01T00:00:00.000Z\" TO \"{0}\"]+@\\{http\\://www.atolcd.com/alfresco/model/parapheur/1.0\\}termine:false";
    private static final DateFormat QUERY_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");

    @Override
    public List<NodeRef> findLate() {
        String query = QUERY.replace("{0}", QUERY_DATE_FORMAT.format(new Date()));
        ResultSet results = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_LUCENE, query);

        return results.getNodeRefs();
    }

    public void updateFolderLateRelationship(NodeRef dossier) {
        // Here, we just have to update late relationship with folders
        Date dateLimite = (Date) nodeService.getProperty(dossier, ParapheurModel.PROP_DATE_LIMITE);


        if (dateLimite != null && new Date().after(dateLimite)) {
            List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);
            NodeRef emetteur = circuit.get(0).getParapheur();
            EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(circuit);

            NodeRef corbeilleRetardEmetteur = parapheurService.getCorbeille(emetteur, ParapheurModel.NAME_EN_RETARD);

            // On part du dossier par soucis d'efficacité
            boolean found = false;
            List<AssociationRef> assocs = nodeService.getSourceAssocs(dossier, ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
            for (AssociationRef assoc : assocs) {
                if (assoc.getSourceRef().equals(corbeilleRetardEmetteur)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                insertIntoCorbeilleEnRetard(dossier, corbeilleRetardEmetteur);
            }

            if (!emetteur.equals(etapeCourante.getParapheur())) {
                NodeRef corbeilleRetardCourant = parapheurService.getCorbeille(etapeCourante.getParapheur(), ParapheurModel.NAME_EN_RETARD);
                found = false;
                for (AssociationRef assoc : assocs) {
                    if (assoc.getSourceRef().equals(corbeilleRetardCourant)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    insertIntoCorbeilleEnRetard(dossier, corbeilleRetardCourant);
                }
            }
        }
    }

    @Override
    public void updateLate(NodeRef dossier) {
        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);
        EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(circuit);
        Date dateLimite = (Date) nodeService.getProperty(dossier, ParapheurModel.PROP_DATE_LIMITE);
        NodeRef emetteur = circuit.get(0).getParapheur();

        if (dateLimite != null && new Date().after(dateLimite)) {
            NodeRef corbeilleRetardEmetteur = parapheurService.getCorbeille(emetteur, ParapheurModel.NAME_EN_RETARD);
            notificationService.notifierPourRetard(dossier, corbeilleRetardEmetteur, true, false);

            if (!emetteur.equals(etapeCourante.getParapheur())) {
                NodeRef corbeilleRetardCourant = parapheurService.getCorbeille(etapeCourante.getParapheur(), ParapheurModel.NAME_EN_RETARD);
                notificationService.notifierPourRetard(dossier, corbeilleRetardCourant, true, false);
            }
        }
    }

    private void insertIntoCorbeilleEnRetard(NodeRef dossier, NodeRef corbeille) {
        nodeService.createAssociation(
                corbeille,
                dossier,
                ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
        nodeService.addAspect(dossier, ParapheurModel.ASPECT_EN_RETARD, null);
        raiseCorbeilleChildCount(corbeille);
        notificationService.notifierPourRetard(dossier, corbeille, false);
    }

    // <editor-fold defaultstate="collapsed" desc="Setters for DI">
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setPolicyComponent(PolicyComponent policyComponent) {
        this.policyComponent = policyComponent;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }
    // </editor-fold>
}
