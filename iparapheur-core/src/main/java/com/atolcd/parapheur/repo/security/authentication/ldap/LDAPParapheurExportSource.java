/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.repo.security.authentication.ldap;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.importer.ExportSource;
import org.alfresco.repo.importer.ExportSourceImporterException;
import org.alfresco.repo.security.authentication.ldap.LDAPInitialDirContextFactory;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.io.XMLWriter;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

public class LDAPParapheurExportSource implements ExportSource
{
   private static Log logger = LogFactory.getLog(LDAPParapheurExportSource.class);
   
   private NamespaceService namespaceService;
   private ParapheurService parapheurService;
   private NodeService nodeService;
   private LDAPInitialDirContextFactory ldapInitialContextFactory;
   private String searchBase;
   private String parapheurQuery = "(objectclass=parapheur)";
   private String parapheurIdAttributeName;
   private boolean errorOnMissingUID;
   private Map<String, String> attributeMapping;
   
   private Map<String, NodeRef> associationMap; 
   
   /**
    * @param namespaceService The namespaceService to set.
    */
   public void setNamespaceService(NamespaceService namespaceService)
   {
      this.namespaceService = namespaceService;
   }
   
   /**
    * @param parapheurService The parapheurService to set.
    */
   public void setParapheurService(ParapheurService parapheurService)
   {
      this.parapheurService = parapheurService;
   }

   /**
    * @param nodeService The nodeService to set.
    */
   public void setNodeService(NodeService nodeService)
   {
      this.nodeService = nodeService;
   }

   /**
    * @param ldapInitialContextFactory The ldapInitialContextFactory to set.
    */
   public void setLDAPInitialDirContextFactory(LDAPInitialDirContextFactory ldapInitialDirContextFactory)
   {
       this.ldapInitialContextFactory = ldapInitialDirContextFactory;
   }
   
   /**
    * @param searchBase The searchBase to set.
    */
   public void setSearchBase(String searchBase)
   {
      this.searchBase = searchBase;
   }
   
   /**
    * @param parapheurQuery The parapheurQuery to set.
    */
   public void setParapheurQuery(String parapheurQuery)
   {
      this.parapheurQuery = parapheurQuery;
   }
   
   /**
    * @param parapheurIdAttributeName The parapheurIdAttributeName to set.
    */
   public void setParapheurIdAttributeName(String parapheurIdAttributeName)
   {
      this.parapheurIdAttributeName = parapheurIdAttributeName;
   }
   
   /**
    * @param errorOnMissingUID The errorOnMissingUID to set.
    */
   public void setErrorOnMissingUID(boolean errorOnMissingUID)
   {
      this.errorOnMissingUID = errorOnMissingUID;
   }
   
   /**
    * @param attributeMapping The attributeMapping to set.
    */
   public void setAttributeMapping(Map<String, String> attributeMapping)
   {
      this.attributeMapping = attributeMapping;
   }
   
   /**
    * @see org.alfresco.repo.importer.ExportSource#generateExport(org.dom4j.io.XMLWriter)
    */
    @Override
   public void generateExport(XMLWriter writer)
   {
      QName nodeUUID = QName.createQName("sys:node-uuid", namespaceService);
      
      Collection<String> prefixes = namespaceService.getPrefixes();
      
      QName childQName = QName.createQName(NamespaceService.REPOSITORY_VIEW_PREFIX, "childName", namespaceService);
      
      try
      {
         writer.startDocument();
         for (String prefix : prefixes)
         {
            if (!prefix.equals("xml"))
            {
               String uri = namespaceService.getNamespaceURI(prefix);
               writer.startPrefixMapping(prefix, uri);
            }
         }
         
         writer.startElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "view",
               NamespaceService.REPOSITORY_VIEW_PREFIX + ":" + "view", new AttributesImpl());
         
         InitialDirContext ctx = null;
         try
         {
            ctx = ldapInitialContextFactory.getDefaultIntialDirContext();
            
            // Authentication has been successful.
            // Set the current user, they are now authenticated.
            
            SearchControls userSearchCtls = new SearchControls();
            userSearchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            
            userSearchCtls.setCountLimit(Integer.MAX_VALUE);
            
            NamingEnumeration<SearchResult> searchResults = ctx.search(searchBase, parapheurQuery, userSearchCtls);
            
            associationMap = new HashMap<String, NodeRef>();
            
            while (searchResults.hasMoreElements())
            {
               SearchResult result = searchResults.next();
               Attributes attributes = result.getAttributes();
               Attribute uidAttribute = attributes.get(parapheurIdAttributeName);
               if (uidAttribute == null)
               {
                  if(errorOnMissingUID)
                  {
                     throw new ExportSourceImporterException(
                           "Parapheur returned by search does not have mandatory parapheur id attribute " + attributes);
                  }
                  else
                  {
                     logger.warn("Parapheur returned by search does not have mandatory parapheur id attribute " + attributes);
                     continue;
                  }
               }
               String uid = (String) uidAttribute.get(0);
               
               if (logger.isDebugEnabled())
               {
                  logger.debug("Adding parapheur for " + uid);
               }
               
               AttributesImpl attrs = new AttributesImpl();
               attrs.addAttribute(NamespaceService.REPOSITORY_VIEW_1_0_URI, childQName.getLocalName(), childQName
                     .toPrefixString(), null, ParapheurModel.TYPE_PARAPHEUR.toPrefixString(namespaceService) + "-" + uid);
               
               writer.startElement(ParapheurModel.TYPE_PARAPHEUR.getNamespaceURI(), ParapheurModel.TYPE_PARAPHEUR
                     .getLocalName(), ParapheurModel.TYPE_PARAPHEUR.toPrefixString(namespaceService), attrs);
               
               // permissions
               AttributesImpl tmpAttrs = new AttributesImpl();
               tmpAttrs.addAttribute(NamespaceService.REPOSITORY_VIEW_PREFIX, "inherit", "view:inherit", null, "false");
               writer.startElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "acl", "view:acl", tmpAttrs);
               
               tmpAttrs = new AttributesImpl();
               tmpAttrs.addAttribute(NamespaceService.REPOSITORY_VIEW_PREFIX, "access", "view:access", null, "ALLOWED");
               writer.startElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "ace", "view:ace", tmpAttrs);
               
               writer.startElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "authority", "view:authority", new AttributesImpl());
               writer.characters("GROUP_EVERYONE".toCharArray(), 0, 14);
               writer.endElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "authority", "view:authority");
               
               writer.startElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "permission", "view:permission", new AttributesImpl());
               writer.characters("Consumer".toCharArray(), 0, 8);
               writer.endElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "permission", "view:permission");
               
               writer.endElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "ace", "view:ace");
               
               writer.endElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "acl", "view:acl");
               
               // uifacets
               writer.startElement(NamespaceService.APP_MODEL_PREFIX, "uifacets", "app:uifacets", new AttributesImpl());
               writer.endElement(NamespaceService.APP_MODEL_PREFIX, "uifacets", "app:uifacets");
               
               // Icône
               writer.startElement(NamespaceService.APP_MODEL_PREFIX, "icon", "app:icon", new AttributesImpl());
               writer.characters("space-icon-default".toCharArray(), 0, 18);
               writer.endElement(NamespaceService.APP_MODEL_PREFIX, "icon", "app:icon");
               
               for (String key : attributeMapping.keySet())
               {
                  QName keyQName = QName.createQName(key, namespaceService);
                  
                  String attributeName = attributeMapping.get(key);
                  if (attributeName != null)
                  {
                     Attribute attribute = attributes.get(attributeName);
                     if (attribute != null)
                     {
                        if (!attributeName.equals("hierarchie"))
                        {
                           int size = attribute.size();
                           if (size == 1)
                           {
                              String value = (String) attribute.get(0);
                              
                              writer.startElement(keyQName.getNamespaceURI(), keyQName.getLocalName(), keyQName
                                    .toPrefixString(namespaceService), new AttributesImpl());
                              
                              if (value != null && !value.equals("null"))
                              {
                                 writer.characters(value.toCharArray(), 0, value.length());
                              }
                              else
                              {
                                 writer.characters("".toCharArray(), 0, 0);
                              }
                              
                              writer.endElement(keyQName.getNamespaceURI(), keyQName.getLocalName(), keyQName
                                    .toPrefixString(namespaceService));
                           }
                           else
                           {
                              writer.startElement(keyQName.getNamespaceURI(), keyQName.getLocalName(), keyQName
                                    .toPrefixString(namespaceService), new AttributesImpl());
                              
                              for (int i=0 ; i<attribute.size() ; i++)
                              {
                                 String value = (String) attribute.get(i);
                                 if (value != null && !value.equals("null"))
                                 {
                                    writer.startElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "value", "view:value", new AttributesImpl());
                                    writer.characters(value.toCharArray(), 0, value.length());
                                    writer.endElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "value", "view:value");
                                 }
                              }
                              
                              writer.endElement(keyQName.getNamespaceURI(), keyQName.getLocalName(), keyQName
                                    .toPrefixString(namespaceService));
                           }
                        }
                        else
                        {
                           // On enregistre les associations à ajouter
                           String value = (String) attribute.get(0);
                           if (value != null)
                           {
                              NodeRef resp = parapheurService.parapheurFromName(value);
                              associationMap.put(uid, resp);
                           }
                        }
                     }
                  }
                  
               }
               
               NodeRef paraphRef = parapheurService.parapheurFromName(uid);
               // Le parapheur existe déjà, on met à jour les propriétés
               if (paraphRef != null)
               {
                  String uguid = paraphRef.getId();
                  
                  writer.startElement(nodeUUID.getNamespaceURI(), nodeUUID.getLocalName(), nodeUUID
                        .toPrefixString(namespaceService), new AttributesImpl());
                  
                  writer.characters(uguid.toCharArray(), 0, uguid.length());
                  
                  writer.endElement(nodeUUID.getNamespaceURI(), nodeUUID.getLocalName(), nodeUUID
                        .toPrefixString(namespaceService));
               }
               else
               {
                  // Création des corbeilles
                  createCorbeilles(writer);
               }
               
               writer.endElement(ParapheurModel.TYPE_PARAPHEUR.getNamespaceURI(), ParapheurModel.TYPE_PARAPHEUR
                     .getLocalName(), ParapheurModel.TYPE_PARAPHEUR.toPrefixString(namespaceService));
               
            }
            
            // Ajout des associations
            if (associationMap != null && associationMap.size() > 0)
            {
               for (String id : associationMap.keySet())
               {
                  // Récupérer le noderef du noeud source
                  NodeRef ref = parapheurService.parapheurFromName(id);
                  if (ref != null)
                  {
                     List<AssociationRef> assocList = nodeService.getTargetAssocs(ref, ParapheurModel.ASSOC_HIERARCHIE);
                     if (assocList != null && assocList.size() == 1)
                     {
                        nodeService.removeAssociation(ref, assocList.get(0).getTargetRef(), ParapheurModel.ASSOC_HIERARCHIE);
                     }
                  }
                  
                  NodeRef resp = associationMap.get(id);
                  if (resp != null)
                  {
                     AttributesImpl tmpAttrs = new AttributesImpl();
                     tmpAttrs.addAttribute(NamespaceService.REPOSITORY_VIEW_PREFIX, "pathref", "view:pathref", null, "ph:parapheur-"+id);
                     writer.startElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "reference", "view:reference", tmpAttrs);
                     writer.startElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "associations", "view:associations", new AttributesImpl());
                     writer.startElement(ParapheurModel.PARAPHEUR_MODEL_PREFIX, "hierarchie", "ph:hierarchie", new AttributesImpl());
                  
                     logger.debug("Association (hierarchie) entre : ");
                     logger.debug("  - id = " + id);
                     String path = nodeService.getPath(resp).toPrefixString(namespaceService);
                     logger.debug("  - path = "+path);

                     tmpAttrs = new AttributesImpl();
                     tmpAttrs.addAttribute(NamespaceService.REPOSITORY_VIEW_PREFIX, "pathref", "view:pathref", null, path);
                     
                     writer.startElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "reference", "view:reference", tmpAttrs);
                     writer.endElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "reference", "view:reference");
                     writer.endElement(ParapheurModel.PARAPHEUR_MODEL_PREFIX, "hierarchie", "ph:hierarchie");
                     
                     writer.endElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "associations", "view:associations");
                     writer.endElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "reference", "view:reference");
                  }
               }
            }
            
         }
         catch (NamingException e)
         {
            throw new ExportSourceImporterException("Failed to import parapheurs.", e);
         }
         finally
         {
            if (ctx != null)
            {
               try
               {
                  ctx.close();
               }
               catch (NamingException e)
               {
                  throw new ExportSourceImporterException("Failed to import parapheurs.", e);
               }
            }
         }
         
         for (String prefix : prefixes)
         {
            if (!prefix.equals("xml"))
            {
               writer.endPrefixMapping(prefix);
            }
         }
         
         writer.endElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "view", NamespaceService.REPOSITORY_VIEW_PREFIX
               + ":" + "view");
         
         writer.endDocument();
      }
      catch (SAXException e)
      {
         throw new ExportSourceImporterException("Failed to create file for import.", e);
      }
   }
   
   private void createCorbeilles(XMLWriter writer) throws SAXException
   {
      writer.startElement(ContentModel.ASSOC_CONTAINS.getNamespaceURI(), ContentModel.ASSOC_CONTAINS.getLocalName(), ContentModel.ASSOC_CONTAINS.toPrefixString(namespaceService), new AttributesImpl());
      
      createCorbeille(writer, ParapheurModel.TYPE_CORBEILLE, ParapheurModel.NAME_EN_PREPARATION, "Dossiers à transmettre", "Dossiers en cours de préparation ou prêts à émettre.");
      createCorbeille(writer, ParapheurModel.TYPE_CORBEILLE, ParapheurModel.NAME_A_TRAITER, "Dossiers à traiter", "Dossiers à viser ou à signer.");
      createCorbeille(writer, ParapheurModel.TYPE_CORBEILLE, ParapheurModel.NAME_A_ARCHIVER, "Dossiers à archiver", "Dossiers signés qu'il convient désormais d'archiver.");
      createCorbeille(writer, ParapheurModel.TYPE_CORBEILLE, ParapheurModel.NAME_RETOURNES, "Dossiers retournés", "Dossiers rejetés lors du circuit de signature/visa.");
      createCorbeille(writer, ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, ParapheurModel.NAME_EN_COURS, "Dossiers en cours", "Dossiers qui ont été émis.");
      createCorbeille(writer, ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, ParapheurModel.NAME_TRAITES, "Dossiers traités", "Dossiers qui ont été traités.");
      createCorbeille(writer, ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, ParapheurModel.NAME_RECUPERABLES, "Dossiers récupérables", "Dossiers sur lesquels on peut exercer un droit de remords.");
      createCorbeille(writer, ParapheurModel.TYPE_CORBEILLE, ParapheurModel.NAME_SECRETARIAT, "Dossiers à relire - annoter", "Dossiers envoyés au secrétariat pour relecture - annotation.");
      
      writer.endElement(ContentModel.ASSOC_CONTAINS.getNamespaceURI(), ContentModel.ASSOC_CONTAINS.getLocalName(), ContentModel.ASSOC_CONTAINS.toPrefixString(namespaceService));
   }
   
   private void createCorbeille(XMLWriter writer, QName type, QName childName, String name, String desc) throws SAXException
   {
      AttributesImpl attrs = new AttributesImpl();
      QName childQName = QName.createQName(NamespaceService.REPOSITORY_VIEW_PREFIX, "childName", namespaceService);
      attrs.addAttribute(NamespaceService.REPOSITORY_VIEW_PREFIX, childQName.getLocalName(), childQName.toPrefixString(), null, childName.toPrefixString(namespaceService));
      writer.startElement(type.getNamespaceURI(), type.getLocalName(), type.toPrefixString(namespaceService), attrs);
      
      // uifacets
      writer.startElement(NamespaceService.APP_MODEL_PREFIX, "uifacets", "app:uifacets", new AttributesImpl());
      writer.endElement(NamespaceService.APP_MODEL_PREFIX, "uifacets", "app:uifacets");
      
      // Icône
      writer.startElement(NamespaceService.APP_MODEL_PREFIX, "icon", "app:icon", new AttributesImpl());
      writer.characters("space-icon-default".toCharArray(), 0, 18);
      writer.endElement(NamespaceService.APP_MODEL_PREFIX, "icon", "app:icon");
      
      // Nom
      writer.startElement(ContentModel.PROP_NAME.getNamespaceURI(), ContentModel.PROP_NAME.getLocalName(), ContentModel.PROP_NAME.toPrefixString(namespaceService), new AttributesImpl());
      writer.characters(name.toCharArray(), 0, name.length());
      writer.endElement(ContentModel.PROP_NAME.getNamespaceURI(), ContentModel.PROP_NAME.getLocalName(), ContentModel.PROP_NAME.toPrefixString(namespaceService));
      
      // Titre
      writer.startElement(ContentModel.PROP_TITLE.getNamespaceURI(), ContentModel.PROP_TITLE.getLocalName(), ContentModel.PROP_TITLE.toPrefixString(namespaceService), new AttributesImpl());
      writer.characters(name.toCharArray(), 0, name.length());
      writer.endElement(ContentModel.PROP_TITLE.getNamespaceURI(), ContentModel.PROP_TITLE.getLocalName(), ContentModel.PROP_TITLE.toPrefixString(namespaceService));
      
      // Description
      writer.startElement(ContentModel.PROP_DESCRIPTION.getNamespaceURI(), ContentModel.PROP_DESCRIPTION.getLocalName(), ContentModel.PROP_DESCRIPTION.toPrefixString(namespaceService), new AttributesImpl());
      writer.characters(desc.toCharArray(), 0, desc.length());
      writer.endElement(ContentModel.PROP_DESCRIPTION.getNamespaceURI(), ContentModel.PROP_DESCRIPTION.getLocalName(), ContentModel.PROP_DESCRIPTION.toPrefixString(namespaceService));
      
      writer.endElement(type.getNamespaceURI(), type.getLocalName(), type.toPrefixString(namespaceService));
   }
}
