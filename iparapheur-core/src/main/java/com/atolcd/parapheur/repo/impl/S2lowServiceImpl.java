/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurErrorCode;
import com.atolcd.parapheur.model.ParapheurException;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.repo.impl.exceptions.UnknownMailsecTransactionException;
import org.adullact.iparapheur.repo.worker.WorkerService;
import org.adullact.iparapheur.status.StatusMetier;
import org.adullact.iparapheur.tdt.s2low.TransactionStatus;
import org.adullact.iparapheur.util.X509Util;
import org.adullact.utils.StringUtils;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.filestore.FileContentWriter;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.repo.template.DateCompareMethod;
import org.alfresco.repo.template.HasAspectMethod;
import org.alfresco.repo.template.I18NMessageMethod;
import org.alfresco.repo.template.TemplateNode;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.*;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.ssl.KeyMaterial;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.jetbrains.annotations.NotNull;
import org.quartz.*;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import org.xml.sax.InputSource;

import javax.net.ssl.SSLHandshakeException;
import javax.transaction.UserTransaction;
import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class S2lowServiceImpl implements S2lowService, InitializingBean {

    private static Logger logger = Logger.getLogger(S2lowService.class);

    /*
     * nouveauté API 1.3: MCA+BASIC parfois... alors il faut bien verifier
     */
//    private static final String URL_ADMIN_API_LIST_LOGIN = "/admin/users/api_list_login.php";
    private static final String URL_ADMIN_API_LIST_LOGIN = "/admin/users/api-list-login.php";

    /**
     * ACTES
     */
    private static final String URL_ACTES_TRANSAC_CREATE = "/modules/actes/actes_transac_create.php";
    // non implementé:                                      /modules/actes/actes_transac_submit.php
    // non implementé:                                      /modules/actes/actes_transac_cancel.php
    // non implementé:                                      /modules/actes/actes_transac_close.php
    // non implementé:                                      /modules/actes/actes_classification_request.php
    private static final String URL_ACTES_CLASSIFICATION_FETCH = "/modules/actes/actes_classification_fetch.php";
    private static final String URL_ACTES_TRANSAC_GET_STATUS = "/modules/actes/actes_transac_get_status.php";
    private static final String URL_ACTES_TRANSAC_GET_ENV_SERIAL = "/modules/actes/actes_transac_get_env_serial.php";
    private static final String URL_ACTES_TRANSAC_SET_ARCHIVE_URL = "/modules/actes/actes_transac_set_archive_url.php";
    // non implementé:                                               /modules/actes/actes_transac_get_document.php
    // non implementé:                                               /modules/actes/actes_transac_reponse_create.php
    // Recuperer l'acte tamponne (PDF)
    private static final String URL_ACTES_TRANSAC_GET_TAMPON = "/modules/actes/actes_transac_get_tampon.php";

    /*
     * HELIOS
     */
    private static final String URL_HELIOS_IMPORTER_FICHIER   = "/modules/helios/api/helios_importer_fichier.php";
    private static final String URL_HELIOS_TRANSAC_GET_STATUS = "/modules/helios/api/helios_transac_get_status.php";
    private static final String URL_HELIOS_DOWNLOAD_ACQUIT    = "/modules/helios/helios_download_acquit.php";
    private static final String URL_HELIOS_GET_LIST           = "/modules/helios/api/helios_get_list.php";
    private static final String URL_HELIOS_GET_RETOUR         = "/modules/helios/api/helios_get_retour.php";
    private static final String URL_HELIOS_CHANGE_STATUS      = "/modules/helios/api/helios_change_status.php";

    /*
     * Mail securisé
     */
    private static final String URL_MAILSEC_VERSION = "/modules/mail/api/version.php";
    private static final String URL_MAILSEC_USER_MAIL = "/modules/mail/api/user-mail.php";
    private static final String URL_MAILSEC_NB_MAIL = "/modules/mail/api/nb-mail.php";
    private static final String URL_MAILSEC_LIST_MAIL = "/modules/mail/api/list-mail.php";
    private static final String URL_MAILSEC_DETAIL_MAIL = "/modules/mail/api/detail-mail.php";
    private static final String URL_MAILSEC_SEND_MAIL = "/modules/mail/api/send-mail.php";
    private static final String URL_MAILSEC_DELETE_MAIL = "/modules/mail/api/delete-mail.php";

    public static final String STATUS_MAILSEC_NOT_CONFIRMED = "aucune confirmation";
    public static final String STATUS_MAILSEC_PARTIALY_CONFIRMED = "confirmé partiellement";
    public static final String STATUS_MAILSEC_FULLY_CONFIRMED = "confirmé";

    private static final String MESSAGE_MAILSEC_ANCHOR = "==message==";

    private static final String VALUE_MAILSEC_TRUE = "t";
    private static final String VALUE_MAILSEC_FALSE = "f";

    private static final String STATUS_MAILSEC_REQUEST_OK = "OK";
    private static final String STATUS_MAILSEC_REQUEST_ERROR = "ERROR";


    private Properties configuration;

    private NodeService nodeService;

    private ContentService contentService;

    private ParapheurService parapheurService;

    private SearchService searchService;

    private NamespaceService namespaceService;

    private FileFolderService fileFolderService;

    private TransactionService transactionService;

    private AuthenticationComponent authenticationComponent;

    private TenantService tenantService;

    private TenantAdminService tenantAdminService;

    private PersonService personService;

    private MetadataService metadataService;

    private ServiceRegistry serviceRegistry;

    private Scheduler scheduler;

    private TypesService typesService;

    private WorkerService workerService;

    private boolean enabled;

    private Integer intervalleInteger;

    public Properties getConfiguration() {
        return configuration;
    }

    public void setTypesService(TypesService typesService) {
        this.typesService = typesService;
    }

    public void setWorkerService(WorkerService workerService) {
        this.workerService = workerService;
    }
    /**
     * @param configuration The configuration to set.
     */
    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    /**
     * @param contentService The contentService to set.
     */
    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    /**
     * @param namespaceService The namespaceService to set.
     */
    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    /**
     * @param fileFolderService The fileFolderService to set.
     */
    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    /**
     * @param transactionService The transactionService to set.
     */
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * @param nodeService The nodeService to set.
     */
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    /**
     * @param searchService The searchService to set.
     */
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    /**
     * @param authenticationComponent The authenticationComponent to set.
     */
    public void setAuthenticationComponent(AuthenticationComponent authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }

    /**
     * @param tenantService The tenantService to set.
     */
    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    /**
     * @param tenantAdminService The tenantAdminService to set.
     */
    public void setTenantAdminService(TenantAdminService tenantAdminService) {
        this.tenantAdminService = tenantAdminService;
    }

    /**
     * @param personService The personService to set.
     */
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    /**
     * @param serviceRegistry the serviceRegistry to set.
     */
    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    /**
     * @param metadataService The metadataService to set.
     */
    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    /**
     * @param scheduler the Scheduler to set.
     */
    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    /**
     * @return the enabled
     */
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * (non-Javadoc) @see
     * org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(nodeService, "There must be a node service");
        Assert.notNull(searchService, "There must be a search service");
        Assert.notNull(contentService, "There must be a content service");
        Assert.notNull(namespaceService, "There must be a namespace service");
        Assert.notNull(parapheurService, "There must be a parapheur service");
        Assert.notNull(configuration, "There must be a configuration element");
    }

    @Override
    public void restartGetS2lowStatusJobs() {
        List<Tenant> tenants = tenantAdminService.getAllTenants();
        List<String> admins = new ArrayList<String>();
        admins.add("admin"); // Execution in root tenant
        for (Tenant tenant : tenants) {
            if (tenant.isEnabled()) {
                admins.add(tenantService.getDomainUser("admin", tenant.getTenantDomain()));
            }
        }

        for (String admin : admins) {
            AuthenticationUtil.runAs(new RunAsWork() {

                @Override
                public Object doWork() throws Exception {
                    restartGetS2lowStatusJob();
                    return null;
                }

            }, admin);
        }
    }

    @Override
    public void restartGetS2lowStatusJob() {
        UserTransaction utx = transactionService.getUserTransaction();
        try {
            utx.begin();

            String dossiersXPath = "//*[subtypeOf('ph:dossier')]";
            List<NodeRef> dossiers = searchService.selectNodes(
                    nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                    dossiersXPath,
                    null,
                    namespaceService,
                    false);

            for (NodeRef dossier : dossiers) {
                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossier);

                if (etape != null
                        && etape.getActionDemandee().equals(EtapeCircuit.ETAPE_TDT)
                        && nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW)) {
                    if (PROP_TDT_NOM_S2LOW.equals(nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_NOM))) {
                        startGetS2lowStatusJob(dossier);
                    }
                }
            }

            utx.commit();
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
                logger.error(ex);
            }
            throw new RuntimeException(e);
        }
    }

    @Override
    public void restartGetMailsecS2lowStatusJobs() {
        List<Tenant> tenants = tenantAdminService.getAllTenants();
        List<String> admins = new ArrayList<String>();
        admins.add("admin"); // Execution in root tenant
        for (Tenant tenant : tenants) {
            if (tenant.isEnabled()) {
                admins.add(tenantService.getDomainUser("admin", tenant.getTenantDomain()));
            }
        }

        for (String admin : admins) {
            AuthenticationUtil.runAs(new RunAsWork() {

                @Override
                public Object doWork() throws Exception {
                    restartGetMailsecS2lowStatusJob();
                    return null;
                }

            }, admin);
        }
    }

    public void restartGetMailsecS2lowStatusJob() {
        UserTransaction utx = transactionService.getUserTransaction();
        try {
            utx.begin();

            String dossiersXPath = "//*[subtypeOf('ph:dossier')]";
            List<NodeRef> dossiers = searchService.selectNodes(
                    nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                    dossiersXPath,
                    null,
                    namespaceService,
                    false);

            for (NodeRef dossier : dossiers) {
                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossier);

                String status = (String)nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_STATUS);

                if (etape != null
                        && status != null && !STATUS_MAILSEC_FULLY_CONFIRMED.equals(status)
                        && etape.getActionDemandee().equals(EtapeCircuit.ETAPE_MAILSEC)) {
                    startGetMailSecS2lowStatusJob(dossier);
                }
            }

            utx.commit();
        } catch (Exception e) {
            // e.printStackTrace();
            try {
                utx.rollback();
            } catch (Exception ex) {
                logger.error(ex);
            }
            throw new RuntimeException(e);
        }
    }

    protected Map<String, String> getPropertiesFromConfigFile(String rootElement, String typeDossier, boolean forceOverride) {
        assert(rootElement != null && !rootElement.equals(""));
        Map<String, String> properties = new HashMap<String, String>();

        try {
            // Map<String, String> propertiesActes = new HashMap<String, String>();
            SAXReader reader = new SAXReader();
            Document document;
            boolean letsOverride = false;
            if (typeDossier == null) {
                document = reader.read(readFile("s2low_configuration.xml"));
            } else {
                // Déterminer si la surcharge par le type de dossier est active!
                letsOverride = false;
                SAXReader saxreader = new SAXReader();
                InputStream istream = parapheurService.getSavedXMLTypes();
                if (null == istream) {
                    throw new DocumentException("impossible d'acceder a la liste des types");
                }
                Document docXml = saxreader.read(istream);
                Element rootElt = docXml.getRootElement();
                if (rootElt.getName().equalsIgnoreCase("MetierTypes")) {
                    Iterator<Element> itMetierType = rootElt.elementIterator("MetierType");
                    String tID;
                    for (Iterator<Element> ie = itMetierType; ie.hasNext(); ) {
                        Element mtype = ie.next();
                        tID = mtype.element("ID").getTextTrim();
                        if (tID.equals(typeDossier)) {
                            if (mtype.element("TdT").element("override") != null
                                    && (forceOverride || mtype.element("TdT").element("override").getTextTrim().equals("true"))) {
                                letsOverride = true;
                            }
                            break;
                        }
                    }
                }

                if (letsOverride) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("getPropertiesFromConfigFile(" + rootElement + "," + typeDossier + "): let's override!");
                    }
                    InputStream is = readFile(typeDossier + "_s2low_properties.xml", "/app:company_home/app:dictionary/cm:metiertype");
                    if(is != null) {
                        document = reader.read(is);
                    } else {
                        document = reader.read(readFile("s2low_configuration.xml"));
                    }
                } else {
                    document = reader.read(readFile("s2low_configuration.xml"));
                }
            }
            if(document.getRootElement().element(rootElement) == null) {
                NodeRef file = null;
                if (letsOverride) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("getPropertiesFromConfigFile(" + rootElement + "," + typeDossier + "): let's override!");
                    }
                    file = findFile(typeDossier + "_s2low_properties.xml", "/app:company_home/app:dictionary/cm:metiertype");
                }
                if(file == null) {
                    file = findFile("s2low_configuration.xml", null);
                }
                if(rootElement.equals("pades")) {
                    addPadesSnippet(file);
                } else if(rootElement.equals("xades")) {
                    addSigSnippet(file);
                }
                return getPropertiesFromConfigFile(rootElement, typeDossier, forceOverride);
            }
            List<Node> nodes = document.getRootElement().element(rootElement).elements();
            for (Node node : nodes) {
                properties.put(node.getName(), node.getText());
            }
            return properties;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new AlfrescoRuntimeException(ex.getMessage(), ex);
        }
    }

    private void addPadesSnippet(NodeRef file) {
        ContentReader reader = contentService.getReader(file, ContentModel.PROP_CONTENT);
        SAXReader saxReader = new SAXReader();
        Document doc;
        try {
            doc = saxReader.read(reader.getContentInputStream());

        } catch (DocumentException ex) {
            // log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }

        List<Node> configNodes = doc.selectNodes("/s2low/pades");


        if (configNodes.isEmpty()) {
            InputStream viewStream = getClass().getClassLoader().getResourceAsStream("alfresco/module/parapheur/s2low/pades-snippet.xml");
            SAXReader saxReaderSnipp = new SAXReader();
            Document snippet;
            try {
                snippet = saxReaderSnipp.read(viewStream);
            } catch (DocumentException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
            doc.getRootElement().add(snippet.getRootElement());

            ContentWriter writer = contentService.getWriter(file, ContentModel.PROP_CONTENT, true);

            writer.putContent(doc.asXML());
        }
    }

    private void addSigSnippet(NodeRef file) {
        ContentReader reader = contentService.getReader(file, ContentModel.PROP_CONTENT);
        SAXReader saxReader = new SAXReader();
        Document doc;
        try {
            doc = saxReader.read(reader.getContentInputStream());

        } catch (DocumentException ex) {
            // log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }

        List<Node> configNodes = doc.selectNodes("/s2low/xades");


        if (configNodes.isEmpty()) {
            InputStream viewStream = getClass().getClassLoader().getResourceAsStream("alfresco/module/parapheur/s2low/sig-snippet.xml");
            SAXReader saxReaderSnipp = new SAXReader();
            Document snippet;
            try {
                snippet = saxReaderSnipp.read(viewStream);
            } catch (DocumentException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
            doc.getRootElement().add(snippet.getRootElement());

            ContentWriter writer = contentService.getWriter(file, ContentModel.PROP_CONTENT, true);

            writer.putContent(doc.asXML());
        }
    }

//    public Map<String, String> getPropertiesActes() {
//       return getPropertiesFromConfigFile("actes", null);
//    }

//    public Map<String, String> getPropertiesHelios() {
//        return getPropertiesFromConfigFile("helios", null);
//    }

    public Map<String, String> getPropertiesSecureMail() {
        return getPropertiesFromConfigFile("mailsec", null, false);
    }

    /**
     * charge les propriétés de TDT pour ACTES selon le type passé en paramètre.
     *
     * @param typeDossier le type de dossier
     * @return une map des différentes propriétés
     */
    private Map<String, String> getPropertiesActes(String typeDossier) {
       return getPropertiesFromConfigFile("actes", typeDossier, false);
    }

    private Map<String, String> getPropertiesHelios(String typeDossier) {
        return getPropertiesFromConfigFile("helios", typeDossier, false);
    }

    private Map<String, String> getPropertiesSecureMail(String typeDossier) {
        return getPropertiesFromConfigFile("mailsec", typeDossier, false);
    }

    private Map<String, String> getPropertiesPades(String typeDossier) {
        return getPropertiesFromConfigFile("pades", typeDossier, true);
    }

    private Map<String, String> getPropertiesXades(String typeDossier) {
        return getPropertiesFromConfigFile("xades", typeDossier, false);
    }

    /**
     * @see
     * com.atolcd.parapheur.repo.S2lowService#getXadesSignatureProperties(String)
     */
    @Override
    public Properties getXadesSignatureProperties(String type) {
        Map<String, String> propertiesHelios = getPropertiesHelios(type);
        Properties props = new Properties();
        props.setProperty("pPolicyIdentifierID", propertiesHelios.get("pPolicyIdentifierID"));
        props.setProperty("pPolicyIdentifierDescription", propertiesHelios.get("pPolicyIdentifierDescription"));
        props.setProperty("pPolicyDigest", propertiesHelios.get("pPolicyDigest"));
        props.setProperty("pSPURI", propertiesHelios.get("pSPURI"));
        if(propertiesHelios.containsKey("pCity")) {
            props.setProperty("pCity", propertiesHelios.get("pCity"));
        }
        props.setProperty("pPostalCode", propertiesHelios.get("pPostalCode"));
        props.setProperty("pCountryName", propertiesHelios.get("pCountryName"));
        props.setProperty("pClaimedRole", propertiesHelios.get("pClaimedRole"));
        if (logger.isDebugEnabled()) {
            logger.debug("getXadesSignatureProperties" + props.toString());
        }
        return props;
    }

    /**
     * @see
     * com.atolcd.parapheur.repo.S2lowService#getPadesSignatureProperties(String)
     */
    @Override
    public Properties getPadesSignatureProperties(String type) {
        Map<String, String> propertiesPades = getPropertiesPades(type);
        Properties props = new Properties();
        if(propertiesPades.containsKey("pCity")) {
            props.setProperty("pCity", propertiesPades.get("pCity"));
        }
        props.setProperty("showStamp", propertiesPades.get("showStamp"));
        props.setProperty("stampPage", propertiesPades.get("stampPage"));
        props.setProperty("stampCoordX", propertiesPades.get("stampCoordX"));
        props.setProperty("stampCoordY", propertiesPades.get("stampCoordY"));
        props.setProperty("stampWidth", propertiesPades.get("stampWidth"));
        props.setProperty("stampHeight", propertiesPades.get("stampHeight"));
        props.setProperty("stampFontSize", propertiesPades.get("stampFontSize"));
        if (logger.isDebugEnabled()) {
            logger.debug("getPadesSignatureProperties" + props.toString());
        }
        return props;
    }

    /**
     * @see
     * com.atolcd.parapheur.repo.S2lowService#getCustomXadesSignatureProperties(String)
     */
    @Override
    public Properties getCustomXadesSignatureProperties(String type) {
        Map<String, String> propertiesHelios = getPropertiesXades(type);
        Properties props = new Properties();
        props.setProperty("pPolicyIdentifierID", propertiesHelios.get("pPolicyIdentifierID"));
        props.setProperty("pPolicyIdentifierDescription", propertiesHelios.get("pPolicyIdentifierDescription"));
        props.setProperty("pPolicyDigest", propertiesHelios.get("pPolicyDigest"));
        props.setProperty("pSPURI", propertiesHelios.get("pSPURI"));
        if(propertiesHelios.containsKey("pCity")) {
            props.setProperty("pCity", propertiesHelios.get("pCity"));
        }
        props.setProperty("pCity", propertiesHelios.get("pCity"));
        props.setProperty("pPostalCode", propertiesHelios.get("pPostalCode"));
        props.setProperty("pCountryName", propertiesHelios.get("pCountryName"));
        props.setProperty("pClaimedRole", propertiesHelios.get("pClaimedRole"));
        if (logger.isDebugEnabled()) {
            logger.debug("getCustomXadesSignatureProperties" + props.toString());
        }
        return props;
    }

    @Override
    public Properties getPadesSignaturePropertiesWithDossier(NodeRef dossier) {
        throw new RuntimeException("Ne doit pas être accédée en direct");
    }

    @Override
    public Properties getCustomXadesSignaturePropertiesWithDossier(NodeRef dossier) {
        throw new RuntimeException("Ne doit pas être accédée en direct");
    }

    @Override
    public Properties getXadesSignaturePropertiesWithDossier(NodeRef dossier) {
        throw new RuntimeException("Ne doit pas être accédée en direct");
        // Question: OK, mais à quoi ça sert alors???
    }

    /**
     * @see
     * com.atolcd.parapheur.repo.S2lowService#getS2lowActesClassificationNodeRef()
     */
    @Override
    public NodeRef getS2lowActesClassificationNodeRef() {
        NodeRef res = null;

        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/"
                + this.configuration.getProperty("spaces.dictionary.childname") + "/"
                + this.configuration.getProperty("spaces.configs2low.childname");

        List<NodeRef> results;
        try {
            results = searchService.selectNodes(
                    nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))),
                    xpath,
                    null,
                    namespaceService,
                    false);
            if (results != null && results.size() == 1) {
                // found what we were looking for
                logger.debug("getS2lowClassificationNodeRef: Found 1 node");
                res = results.get(0);
            }
        } catch (AccessDeniedException err) {
            // I Still haven't found what I was looking for....
            // ...so ignore and return null
            logger.debug("getS2lowClassificationNodeRef: Access denied Exception");
            return null;
        }

        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#getS2lowActesNatures()
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<Integer, String> getS2lowActesNatures() {
        Map<Integer, String> mapRes = new HashMap<Integer, String>();

        if (logger.isDebugEnabled()) {
            logger.debug("getS2lowActesNatures: BEGIN");
        }
        NodeRef configRef = getS2lowActesClassificationNodeRef();
        if (configRef == null) {
            logger.error("getS2lowActesNatures: configRef is null");
            return null;
        }
        ContentReader contentreader = contentService.getReader(configRef, ContentModel.PROP_CONTENT);
        if (contentreader == null) {
            logger.error("getS2lowActesNatures: contentreader is null");
            return null;
        }
        SAXReader saxreader = new SAXReader();

        try {
            InputSource is = new InputSource(contentreader.getContentInputStream());
            is.setEncoding("ISO-8859-1");
            Document document = saxreader.read(is);
            Element rootElement = document.getRootElement();
            Element natures = rootElement.element("NaturesActes");
            if (natures != null) {
                Iterator<Element> elementIterator = (Iterator<Element>) natures.elementIterator("NatureActe");
                for (Iterator<Element> i = elementIterator; i.hasNext();) {
                    Element nature = i.next();
                    mapRes.put(Integer.parseInt(nature.attribute("CodeNatureActe").getValue()),
                            nature.attribute("Libelle").getValue().toString());
                }
            }
        } catch (DocumentException e) {
            if (logger.isDebugEnabled()) {
                logger.debug(e.getMessage(), e);
            }
            return null;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getS2lowActesNatures: number of nature Elts="+mapRes.size());
        }
        return mapRes;
    }

    /**
     * @see
     * com.atolcd.parapheur.repo.S2lowService#getS2lowActesClassifications()
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, String> getS2lowActesClassifications() {
        Map<String, String> mapRes = new HashMap<String, String>();

        NodeRef configRef = getS2lowActesClassificationNodeRef();
        if (configRef == null) {
            logger.error("getS2lowActesClassifications: configRef is null");
            return null;
        }
        ContentReader contentreader = contentService.getReader(configRef, ContentModel.PROP_CONTENT);
        if (contentreader == null) {
            logger.error("getS2lowActesClassifications: contentreader is null");
            return null;
        }
        SAXReader saxreader = new SAXReader();

        try {
            InputSource is = new InputSource(contentreader.getContentInputStream());
            is.setEncoding("ISO-8859-1");
            Document document = saxreader.read(is);
            Element rootElement = document.getRootElement();
            Element matieres1 = rootElement.element("Matieres");
            if (matieres1 != null) {
                for (Iterator<Element> i1 = matieres1.elementIterator("Matiere1"); i1.hasNext();) {
                    Element matiere1 = i1.next();
                    String id1 = matiere1.attribute("CodeMatiere").getValue().toString();
                    mapRes.put(id1, matiere1.attribute("Libelle").getValue().toString());

                    for (Iterator<Element> i2 = matiere1.elementIterator("Matiere2"); i2.hasNext();) {
                        Element matiere2 = i2.next();
                        String id2 = id1 + "-" + matiere2.attribute("CodeMatiere").getValue().toString();
                        mapRes.put(id2, matiere2.attribute("Libelle").getValue().toString());

                        for (Iterator<Element> i3 = matiere2.elementIterator("Matiere3"); i3.hasNext();) {
                            Element matiere3 = i3.next();
                            String id3 = id2 + "-" + matiere3.attribute("CodeMatiere").getValue().toString();
                            mapRes.put(id3, matiere3.attribute("Libelle").getValue().toString());

                            for (Iterator<Element> i4 = matiere3.elementIterator("Matiere4"); i4.hasNext();) {
                                Element matiere4 = i4.next();
                                String id4 = id3 + "-" + matiere4.attribute("CodeMatiere").getValue().toString();
                                mapRes.put(id4, matiere4.attribute("Libelle").getValue().toString());

                                for (Iterator<Element> i5 = matiere4.elementIterator("Matiere5"); i5.hasNext();) {
                                    Element matiere5 = i5.next();
                                    String id5 = id4 + "-" + matiere5.attribute("CodeMatiere").getValue().toString();
                                    mapRes.put(id5, matiere5.attribute("Libelle").getValue().toString());
                                }
                            }
                        }
                    }
                }
            }
        } catch (DocumentException e) {
            return null;
        }

        return mapRes;
    }

    @Override
    public int updateS2lowActesClassifications() throws IOException {
        Map<String, String> propertiesActes = getPropertiesActes(null);

        /**
         * Pas la peine d'aller plus loin si le connecteur n'est pas utilisé
         */
        if (!isActesEnabled()) {
            return 1; // cas désactivé
        }

        HttpClient client = createConnexionHTTPS(propertiesActes);
        if (client == null) {
            throw new IOException("updateS2lowActesClassifications: HTTPS impossible.");
        }
        GetMethod get = new GetMethod(URL_ACTES_CLASSIFICATION_FETCH +"?api=1");
        get.setDoAuthentication(true);

        try {
            int result = client.executeMethod(get);
            if (result != HttpStatus.SC_OK) {
                throw new IOException("Unexpected return code: " + result);
            }

            NodeRef contentRef = getS2lowActesClassificationNodeRef();
            ContentWriter contentWriter = contentService.getWriter(contentRef, ContentModel.PROP_CONTENT, true);

            String classification = get.getResponseBodyAsString();

            if (classification.startsWith("KO\n")) {
                throw new IOException("Unexpected error returned from server: " + classification.substring(3));
            }

            contentWriter.putContent(classification);

            logger.info("S2LOW-Actes classifications updated");
            return 0;
        } catch (IOException e) {
            logger.error("Error while fetching ACTES classifications: " + e.getMessage(), e);
        }
        return -1;
    }

    /**
     * @see
     * com.atolcd.parapheur.repo.S2lowService#setS2lowActesArchiveURL(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public String setS2lowActesArchiveURL(NodeRef archive) throws IOException {
        Assert.isTrue(this.nodeService.hasAspect(archive, ParapheurModel.ASPECT_S2LOW), "Le dossier n'a pas été envoyé à la plate-forme S2LOW");
        Map<String, String> propertiesActes = getPropertiesActes((String)this.nodeService.getProperty(archive, ParapheurModel.PROP_TYPE_METIER));
        String res = null;
        String baseURL = propertiesActes.get("baseUrlArchivage");
        if (baseURL == null) {
            baseURL = "http://localhost/alfresco/";
            if (logger.isDebugEnabled()) {
                logger.debug("---- /!\\Impossible de prendre le parametre de config: s2low.baseUrlArchivage");
            }
        }

        String trsId = this.nodeService.getProperty(archive, ParapheurModel.PROP_TRANSACTION_ID).toString();
        if (logger.isDebugEnabled()) {
            logger.debug("setS2lowArchiveURL: trsId=" + trsId);
        }
        // On ne peut renseigner l'archive que si la transaction existe
        Assert.notNull(trsId, "Le dossier n'a pas été envoyé à la plate-forme S2LOW");

        HttpClient client = createConnexionHTTPS(propertiesActes);
        if (client == null) {
            throw new IOException("setS2lowActesArchiveURL: HTTPS impossible.");
        }
        PostMethod post = new PostMethod(URL_ACTES_TRANSAC_SET_ARCHIVE_URL);
        post.setDoAuthentication(true);

        String urlArchive = baseURL + "wcs/parapheur/archives/s2low.html?trsId=" + trsId;
        if (logger.isDebugEnabled()) {
            logger.debug("setS2lowArchiveURL: baseURL=[" + baseURL + "], URL1=" + urlArchive);
        }

        // Remplissage des paramètres POST
        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("id", trsId));
        parts.add(new StringPart("url", urlArchive));
        parts.add(new StringPart("api", "1"));

        Part[] partsTab = new Part[parts.size()];
        for (int i = 0; i < parts.size(); i++) {
            partsTab[i] = parts.get(i);
        }

        post.setRequestEntity(new MultipartRequestEntity(partsTab, post.getParams()));
        // Exécution de la méthode POST
        try {
            int status = client.executeMethod(post);
            if (HttpStatus.SC_OK == status) {
                String reponse = post.getResponseBodyAsString();
                String[] tab = reponse.split("\n");
                if (tab.length == 1 || "KO".equals(tab[0])) {
                    StringBuilder errorBuilder = new StringBuilder("Erreur retournée par la plate-forme s2low : ");
                    for (int i = 1; i < tab.length; i++) {
                        errorBuilder.append(tab[i]);
                    }
                    throw new IOException(errorBuilder.toString());
                }
                // La transaction s'est bien passée, on sort gentiment
                if (logger.isDebugEnabled()) {
                    logger.debug("C'est OK!");
                }
                res = urlArchive;
            } else {
                throw new IOException("Echec de la récupération de la connexion à la plate-forme : statut = " + status);
            }
        } finally {
            post.releaseConnection();
        }

        // ça semble assez débile et hasardeux de se reposer sur le client Web
        // TODO  --> étudier le webscript, plus simple; mais attention à l'authentification (wcs?)
        return res;
    }

    /**
     * @see
     * com.atolcd.parapheur.repo.S2lowService#getInfosS2low(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public int getInfosS2low(NodeRef dossier) throws IOException {
        Assert.isTrue(this.parapheurService.isDossier(dossier), "Node Ref doit être de type ph:dossier");
        Assert.isTrue(this.nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW),
                "Le dossier "+nodeService.getProperty(dossier, ContentModel.PROP_TITLE)+" n'a pas été envoyé à la plate-forme S2LOW");

        int result;
        if ("HELIOS".equals(this.nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_PROTOCOLE))) {
            result = getInfosS2lowHelios(dossier);
        } else {
            result = getInfosS2lowActes(dossier);
        }


        if (!(result == TransactionStatus.STATUS_POSTE
                || result == TransactionStatus.STATUS_EN_ATTENTE
                || result == TransactionStatus.STATUS_TRANSMIS
                || result == TransactionStatus.STATUS_EN_TRAITEMENT)
                && (parapheurService.getCurrentEtapeCircuit(dossier).getActionDemandee().equals(EtapeCircuit.ETAPE_TDT)
                || parapheurService.getCurrentEtapeCircuit(dossier).getActionDemandee().equals(EtapeCircuit.ETAPE_ARCHIVAGE))) {

            if (result == TransactionStatus.STATUS_ERROR || result == TransactionStatus.STATUS_NACK) { // Erreur ou NACK
                // Pour ce cas TRES particulier on audite le transmissionKO
                parapheurService.auditTransmissionTDT(
                        dossier,
                        StatusMetier.STATUS_TRANSMISSION_KO,
                        "Réponse TDT: " + statutS2lowToString(result));

                parapheurService.setAnnotationPublique(dossier, "Réponse TDT: " + statutS2lowToString(result));
                parapheurService.reject(dossier);
            } else { // C'est OK  (ACK ou INFOS_DISPONIBLES): on continue le circuit
                if (!parapheurService.getCurrentEtapeCircuit(dossier).getActionDemandee().equals(EtapeCircuit.ETAPE_ARCHIVAGE)) {
                    parapheurService.approveV4(dossier, parapheurService.getCurrentParapheur());
                    if(parapheurService.isTermine(dossier) &&
                            typesService.hasToAttestSignature(
                                    (String) nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER),
                                    (String) nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER))) {
                        workerService.getAttestByWorker(dossier);
                    }
                }
            }
            try {
            	// BLEX
                if (logger.isInfoEnabled()) {
                    logger.info (
                        "unschedule S2lowGetInfosJob : "
                            + "dossier:" + dossier.getId() + " \"" + nodeService.getProperty(dossier, ContentModel.PROP_TITLE) + "\"");
                }
                scheduler.unscheduleJob(
                        (String) nodeService.getProperty(dossier, ContentModel.PROP_NAME),
                        "S2LOW");
            } catch (SchedulerException ex) {
                logger.error("Exception canceling Job", ex);
            }
        }

        return result;
    }

    /**
     * Récupération de la réponse de S²lOW quant à l'état de l'acte
     *
     * @param dossier
     * @return
     * @throws IOException
     */
    private int getInfosS2lowActes(NodeRef dossier) throws IOException {
        Map<String, String> propertiesActes = getPropertiesActes((String)this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER));
        String res;
        int codeRetour = -1;

        HttpClient client = createConnexionHTTPS(propertiesActes);
        if (client == null) {
            throw new IOException("getInfosS2lowActes: HTTPS impossible.");
        }
        GetMethod get = new GetMethod(URL_ACTES_TRANSAC_GET_STATUS + "?transaction="
                + this.nodeService.getProperty(dossier, ParapheurModel.PROP_TRANSACTION_ID));
        get.setDoAuthentication(true);

        // Exécution de la méthode GET
        try {
            int status = client.executeMethod(get);
            if (HttpStatus.SC_OK == status) {
                String reponse = get.getResponseBodyAsString();
                String[] tab = reponse.split("\n");
                if (tab.length == 1 || "KO".equals(tab[0])) {
                    StringBuilder error = new StringBuilder("Erreur retournée par la plate-forme s2low : ");
                    for (int i = 1; i < tab.length; i++) {
                        error.append(tab[i]);
                    }
                    throw new RuntimeException(error.toString());
                }

                // La méthode a changé en 1.0.5, logger ce qu'on a...
                for (int i = 0; i < tab.length; i++) {
                    logger.debug("Réponse Statut S2LOW: [" + tab[i] + "]");
                }

                // La  transaction s'est bien passée, on renvoie le statut (ligne 2)
                codeRetour = Integer.parseInt(tab[1]);
                res = statutS2lowToString(codeRetour);
                // On enregistre le statut renvoyé
                this.nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS, res);

                // Avec S2LOW 1.0.5 ACTES: Dans les cas 3,4 et 6, le status est accompagné du message de retour envoyé par la préfecture
                // Seuls les cas 4-Ack et 6-Nack sont pertinents ici
                if (tab.length > 2
                        && (codeRetour == TransactionStatus.STATUS_ACK
                        || codeRetour == TransactionStatus.STATUS_NACK)) {
                    String ARActeXml = "";
                    for (int i = 2; i < tab.length; i++) {
                        ARActeXml += tab[i] + "\n";
                    }
                    // On enregistre dans PROP_ARACTE_XML
                    ContentWriter arWriter = this.contentService.getWriter(dossier, ParapheurModel.PROP_ARACTE_XML, true);
                    arWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
                    arWriter.setEncoding("UTF-8");  // FIXME: c'est peut-etre ISO-8859-1
                    arWriter.putContent(ARActeXml);
                }
            } else {
                throw new RuntimeException("Echec de la récupération de la connexion à la plate-forme : statut = " + status);
            }
        } finally {
            get.releaseConnection();
        }

        return codeRetour;
    }

    /**
     * Récupération de la réponse de S²lOW quant à l'état du PES Helios
     *
     * @param dossier
     * @return
     * @throws IOException
     */
    private int getInfosS2lowHelios(NodeRef dossier) throws IOException {
        String dossierName = nodeService.getProperty(dossier, ContentModel.PROP_TITLE).toString();
        String transactionId = this.nodeService.getProperty(dossier, ParapheurModel.PROP_TRANSACTION_ID).toString();
        String typeMetier = this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER).toString();

        // BLEX
        if (logger.isInfoEnabled()) {
            logger.info("obtenir l'etat du dossier sur TDT S2low : dossier=" + dossierName + "; transactionId=" + transactionId);
        }

        Map<String, String> propertiesHelios = getPropertiesHelios(typeMetier);
        int codeRetour = -1;

        HttpClient client = createConnexionHTTPS(propertiesHelios);
        if (client == null) {
            throw new IOException("getInfosS2lowHelios: HTTPS impossible.");
        }
        GetMethod get = new GetMethod(URL_HELIOS_TRANSAC_GET_STATUS + "?transaction=" + transactionId);

        // BLEX
        if (logger.isDebugEnabled()) {
          String uri=get.getURI().toString();
          logger.debug("dossier : "+dossier.toString()+" : uri = "+uri);
        }

        get.setDoAuthentication(true);

        // Exécution de la méthode GET
        try {
          // BLEX
          if (logger.isDebugEnabled()) {
            logger.debug("dossier : "+dossier.toString()+" : executeMethod ... ");
          }

            int status = client.executeMethod(get);
            if (HttpStatus.SC_OK == status) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Reponse s2low=[" + get.getResponseBodyAsString() + "]");
                }
                InputStream reponse = get.getResponseBodyAsStream();
                //            <transaction>
                //              <id> numéro de la transaction  </id>
                //              <resultat> OK ou KO </resultat>
                //              <status> status </status>
                //              <message> message complémentaire </message>
                //            </transaction>
                SAXReader saxreader = new SAXReader();
                try {
                    Document document = saxreader.read(reponse);
                    Element rootElement = document.getRootElement();
                    Element xid = rootElement.element("id");//.getTextTrim();
                    Element xresultat = rootElement.element("resultat");
                    Element xstatus = rootElement.element("status");
                    Element xmessage = rootElement.element("message");
                    if (xid == null || xresultat == null || xstatus == null || "KO".equals(xresultat.getTextTrim())) {
                        String error = "Erreur retournée par la plate-forme s2low : ";
                        if (xmessage != null) {
                            error += xmessage.getTextTrim();
                        }
                        throw new RuntimeException(error);
                    }
                    // La  transaction s'est bien passée, on enregistre le statut
                    codeRetour = Integer.parseInt(xstatus.getTextTrim());

                    // BLEX
                    if (logger.isInfoEnabled()) {
                        logger.info (
                    		"retour s2low : "
                    		+"dossier:"+dossier.getId()+" \""+dossierName+"\""
                    		+", s2lowTransaction:"+transactionId
                    		+", s2lowRetour:"+codeRetour+" (\""+statutS2lowToString(codeRetour)+"\")"
                                // + logCodeRetourConv
                        );
                    }

                    this.nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS, statutS2lowToString(codeRetour));

                    // Si ACK, NACK ou INFOSDISPO: téléchargement de l'acquittement.
                    if (codeRetour == TransactionStatus.STATUS_NACK
                            || codeRetour == TransactionStatus.STATUS_ACK
                            || codeRetour == TransactionStatus.STATUS_INFORMATIONS_DISPONIBLES) {
                        downloadS2lowHeliosAckNack(dossier);

                    }
                } catch (DocumentException e) {
                    throw new RuntimeException("Echec de la récupération de la réponse du TdT", e);
                }
            } else {
                throw new RuntimeException("Echec de la récupération de la connexion à la plate-forme : statut = " + status);
            }
        } finally {
            get.releaseConnection();
        }

        return codeRetour;
    }

    /**
     * Recupération du ACK ou NACK en cas d'acceptation ou de rejet du document
     * pour mise à disposition des applications metier, et afficher erreur à
     * l'utilisateur si besoin
     *
     * @param dossier  le NodeRef du dossier correspondant
     * @throws IOException
     */
    private void downloadS2lowHeliosAckNack(NodeRef dossier) throws IOException {
        Map<String, String> propertiesHelios = getPropertiesHelios((String)this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER));

        HttpClient client = createConnexionHTTPS(propertiesHelios);
        if (client == null) {
            throw new IOException("downloadS2lowHeliosNAck: HTTPS impossible.");
        }
        GetMethod get = new GetMethod(URL_HELIOS_DOWNLOAD_ACQUIT + "?id="
                + this.nodeService.getProperty(dossier, ParapheurModel.PROP_TRANSACTION_ID));
        get.setDoAuthentication(true);

        try {
            int status = client.executeMethod(get);
            if (HttpStatus.SC_OK == status) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Reponse s2low=[" + get.getResponseBodyAsString() + "]");
                }
                InputStream reponse = get.getResponseBodyAsStream();

                ContentWriter writer = contentService.getWriter(dossier, ParapheurModel.PROP_NACKHELIOS_XML, true);
                writer.setMimetype(MimetypeMap.MIMETYPE_XML);
                writer.setEncoding("ISO-8859-1");
                writer.putContent(reponse);
            }
        } finally {
            get.releaseConnection();
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#statutS2lowToString(int)
     */
    @Override
    public String statutS2lowToString(int code) {
        String res = null;
        switch (code) {
            case TransactionStatus.STATUS_ERROR:
                res = "Erreur";
                break;
            case TransactionStatus.STATUS_ANNULE:
                res = "Annulé";
                break;
            case TransactionStatus.STATUS_POSTE:
                res = "Posté";
                break;
            case TransactionStatus.STATUS_EN_ATTENTE:
                res = "En attente de transmission";
                break;
            case TransactionStatus.STATUS_TRANSMIS:
                res = "Transmis";
                break;
            case TransactionStatus.STATUS_ACK:
                res = "Acquittement reçu";
                break;
            case TransactionStatus.STATUS_VALIDE:
                res = "Validé";
                break;
            case TransactionStatus.STATUS_NACK:
                res = "Refusé";
                break;
            case TransactionStatus.STATUS_EN_TRAITEMENT:
                res = "En traitement";
                break;
            case TransactionStatus.STATUS_INFORMATIONS_DISPONIBLES:
                res = "Informations disponibles";
                break;
        }
        return res;
    }

    /**
     * A usage interne (a priori): recupere la liste de login(s) associés à un
     * certificat de connexion S2LOW.
     *
     * @param client la connexion HTTPS _déjà_ pré-remplie avec le minimum
     * @return la liste des logins associés à la connexion. Si pas de login, la liste est vide.
     * @throws IOException
     */
    private List<String> getListLogin(HttpClient client) throws IOException {
        List<String> result = new ArrayList<String>();
        GetMethod get = new GetMethod(URL_ADMIN_API_LIST_LOGIN);
        get.setDoAuthentication(true);
        try {
            int status = client.executeMethod(get);
            if (status == HttpStatus.SC_OK) {
                String reponse = get.getResponseBodyAsString().trim();
                // S2LOW a l'habitude de finir par un retour chariot, inutile...
                if (reponse.endsWith("\n")) {
                    reponse = reponse.substring(0, reponse.lastIndexOf("\n"));
                }
                if (!reponse.isEmpty()) {
                    result = Arrays.asList(reponse.split("\n"));
                }

                logger.debug("getListLogin: Reponse s2low(" + URL_ADMIN_API_LIST_LOGIN + ")=" + result.size() + "elts, [" +
                             reponse.replace('\n', '/') + "]");
            } else {
                logger.error("we had a problem, http status=" + status);
                throw new IOException("Bad request, http status = " + status);
            }
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
            return null;
        } finally {
            get.releaseConnection();
        }
        return result;
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#isCertificateAbleToConnect(java.lang.String, java.lang.String)
     */
    @Override
    public boolean isCertificateAbleToConnect(String protocole, String typeDossier) throws IOException {
//        return (!this.getListLoginForType(protocole, typeDossier).isEmpty());
        return (this.getListLoginForType(protocole, typeDossier) != null);
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#getListLoginForType(java.lang.String, java.lang.String)
     */
    @Override
    public List<String> getListLoginForType(String protocole, String typeDossier) throws IOException {
        List<String> result = new ArrayList<String>();
        Map<String, String> propertiesTDT = null;
        if (typeDossier == null) {
            if ("ACTES".equals(protocole)) {
                propertiesTDT = getPropertiesActes(null);
            } else if ("HELIOS".equals(protocole)) {
                propertiesTDT = getPropertiesHelios(null);
            } else if ("MAILSEC".equals(protocole)) {
                propertiesTDT = getPropertiesSecureMail();
            }
        } else {
            // read the typeDossier properties: server + port + certificate + password
            if ("ACTES".equals(protocole)) {
                propertiesTDT = getPropertiesActes(typeDossier);
            } else if ("HELIOS".equals(protocole)) {
                propertiesTDT = getPropertiesHelios(typeDossier);
            } else if ("MAILSEC".equals(protocole)) {
                propertiesTDT = getPropertiesSecureMail(typeDossier);
            }
        }

        // on vire les éléments userlogin/userpassword éventuels
        if (propertiesTDT != null) {
            propertiesTDT.remove("userlogin");
            propertiesTDT.remove("userpassword");
            if (logger.isDebugEnabled()) {
                logger.debug("propertiesTDT AFTER: " + propertiesTDT.keySet() + "\n\t" + propertiesTDT.values());
            }
        }
        // zou, essai de connexion
        HttpClient client = createConnexionHTTPS(propertiesTDT);
        if (client == null) {
            throw new IOException("isCertificateAbleToConnectActes: HTTPS impossible.");
        }
        try {
            result = this.getListLogin(client);
        } catch (IOException ex) {
        }
        return result;
    }

    private boolean isConnectionOK(HttpClient client) {
        boolean res = false;
        /*
         * ici on triche: il n'y a pas de methode ECHO, alors on utilise le plus approchant
         */
        GetMethod get = new GetMethod(URL_ACTES_TRANSAC_GET_ENV_SERIAL);
        get.setDoAuthentication(true);
        try {
            int status = client.executeMethod(get);
            if (HttpStatus.SC_OK == status) {
                String reponse = get.getResponseBodyAsString();
                if (logger.isDebugEnabled()) {
                    logger.debug("Reponse s2low(" + URL_ACTES_TRANSAC_GET_ENV_SERIAL + ")=[" + reponse + "]");
                }
                // tout se qui nous interesse: communication OK  :-)
                res = true;
            } else {
                logger.error("we had a problem, http status=" + status);
                throw new IOException("Bad request, http status = " + status);
            }
        } catch (IOException ex) {
            logger.error("Impossible de se connecter à S²low", ex);
        } finally {
            get.releaseConnection();
        }
        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#isConnectionOK(String, String)
     */
    @Override
    public boolean isConnectionOK(String protocole, String typeDossier) throws IOException {
        // get the base configuration
        Map<String, String> propertiesTDT = null;
        if (typeDossier == null) {
            if ("ACTES".equals(protocole)) {
                propertiesTDT = getPropertiesActes(null);
            } else if ("HELIOS".equals(protocole)) {
                propertiesTDT = getPropertiesHelios(null);
            } else if ("MAILSEC".equals(protocole)) {
                propertiesTDT = getPropertiesSecureMail();
            }
        } else {
            // read the typeDossier properties: server + port + certificate + password
            if ("ACTES".equals(protocole)) {
                propertiesTDT = getPropertiesActes(typeDossier);
            } else if ("HELIOS".equals(protocole)) {
                propertiesTDT = getPropertiesHelios(typeDossier);
            } else if ("MAILSEC".equals(protocole)) {
                propertiesTDT = getPropertiesSecureMail(typeDossier);
            }
        }

        // zou, essai de connexion
        HttpClient client = createConnexionHTTPS(propertiesTDT);
        if (client == null) {
            throw new IOException("isCertificateAbleToConnectActes: HTTPS impossible.");
        }

        return isConnectionOK(client);
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#envoiS2lowActes(org.alfresco.service.cmr.repository.NodeRef, String, String, String, String, String)
     */
    @Override
    public void envoiS2lowActes(NodeRef dossier, String nature, String classification, String numero, String objet, String date)
            throws IOException, ParapheurException {

        Assert.isTrue(this.parapheurService.isDossier(dossier), "Node Ref doit être de type ph:dossier");
        Assert.isTrue(!this.nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW), "Le dossier a déjà été envoyé à la plate-forme S2LOW");
        Map<String, String> propertiesActes = getPropertiesActes((String)this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER));

        HttpClient client = createConnexionHTTPS(propertiesActes);
        if (client == null) {
            throw new IOException("envoiS2lowActes: HTTPS impossible.");
        }
        PostMethod post = new PostMethod(URL_ACTES_TRANSAC_CREATE);
        post.setDoAuthentication(true);

        // Récupération des fichiers
        List<NodeRef> documents = this.parapheurService.getDocuments(dossier);
        ArrayList<FilePart> fileParts = new ArrayList<FilePart>();
        // Premier fichier: Fichier PDF de l'ACTE
        ContentReader mainDocReader = this.contentService.getReader(documents.get(0), ContentModel.PROP_CONTENT);
        logger.debug("envoiS2low (" + documents.size() + " docs): ajout mainDoc type " + mainDocReader.getMimetype());

        String fileName = (String) this.nodeService.getProperty(documents.get(0), ContentModel.PROP_NAME);
        File ficPdf = ensureMimeType(mainDocReader, numero, MimetypeMap.MIMETYPE_PDF);
        // fileParts.add(new FilePart("acte_pdf_file", ficPdf));
        fileParts.add(new FilePart("acte_pdf_file", fileName, ficPdf));

        // Signature Electronique de l'ACTE
        if (MimetypeMap.MIMETYPE_PDF.equals(mainDocReader.getMimetype())) {
            List<EtapeCircuit> etapes = this.parapheurService.getCircuit(dossier);
            if (etapes.get(0).getActionDemandee() == null) {   // compatibilité ascendante
                byte[] signature = this.parapheurService.getSignature(dossier);
                if (signature != null && signature.length > 0) {
                    logger.debug("envoiS2low: (oldschool) signature PDF presente");
                    File ficSign = TempFileProvider.createTempFile("s2low", "p7s");
                    FileOutputStream os = null;
                    try {
                        os = new FileOutputStream(ficSign);
                        os.write(signature);
                    } finally {
                        if (os != null) {
                            os.close();
                        }
                    }
                    fileParts.add(new FilePart("acte_pdf_file_sign", ficSign));
                }
            } else {  // on n'envoie QUE la dernière signature.
                EtapeCircuit lastSigEtape = null;
                for (EtapeCircuit etape : etapes) {
                    if (etape.getActionDemandee().trim().equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE) && this.parapheurService.getSignature(etape) != null) {
                        lastSigEtape = etape;
                    }
                }
                if (lastSigEtape != null) {
                    byte[] signature = this.parapheurService.getSignature(lastSigEtape);
                    if (signature != null && signature.length > 0) {
                        logger.debug("envoiS2low: signature PDF presente");
                        File ficSign = TempFileProvider.createTempFile("s2low", "p7s");
                        FileOutputStream os = new FileOutputStream(ficSign);
                        os.write(signature);
                        os.close();
                        fileParts.add(new FilePart("acte_pdf_file_sign", fileName + "_sig.p7s", ficSign));
                    }
                }
            }
        }

        // Suivants
        // TODO Pour l'instant les PJ sont envoyées sans signature
        for (int i = 1; i < documents.size(); i++) {
            NodeRef doc = documents.get(i);
            // Correction bug #1869 pièces attachées ne sont pas converties: autorisé PDF JPG PNG
            ContentReader attReader = this.contentService.getReader(doc, ContentModel.PROP_CONTENT);
            if (MimetypeMap.MIMETYPE_IMAGE_JPEG.equalsIgnoreCase(attReader.getMimetype()) || MimetypeMap.MIMETYPE_IMAGE_PNG.equalsIgnoreCase(attReader.getMimetype())) {
                // authorized formats JPG/PNG: no need for PDF conversion
                logger.debug("envoiS2low (doc num " + i + "): ajout piece jointe img: " + attReader.getMimetype());
                File ficPdfx = TempFileProvider.createTempFile(numero + "-Att" + i, null);
                attReader.getContent(ficPdfx);
                fileParts.add(new FilePart("acte_attachments[]",
                        (String)this.nodeService.getProperty(doc, ContentModel.PROP_NAME),
                        ficPdfx));
            } else {
                // convert to PDF if needed
                logger.debug("envoiS2low (doc num " + i + "): ajout piece jointe type " + attReader.getMimetype());
                File ficPdfx = ensureMimeType(attReader, numero + "-Att" + i, MimetypeMap.MIMETYPE_PDF);
                fileParts.add(new FilePart("acte_attachments[]", ficPdfx));
            }
//         File ficPdfx = TempFileProvider.createTempFile("s2low-Att"+i, attReader.getMimetype());
//         attReader.getContent(ficPdfx);
//         fileParts.add(new FilePart("acte_attachments[]", ficPdfx));
        }

        // Lecture de la classification
        ArrayList<StringPart> classifParts = new ArrayList<StringPart>();
        String[] classifications = classification.split("-");
        for (int i = 1; i <= classifications.length; i++) {
            classifParts.add(new StringPart("classif" + i, classifications[i - 1]));
        }

        // Remplissage des paramètres POST
        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("api", "1"));
        parts.add(new StringPart("nature_code", nature));
        for (StringPart classif : classifParts) {
            parts.add(classif);
        }
        parts.add(new StringPart("number", new String(numero.getBytes(), "ISO-8859-1")));
        parts.add(new StringPart("decision_date", new String(date.getBytes(), "ISO-8859-1")));

        if (logger.isDebugEnabled()) {
            // FIXME l'encodage de l'objet est pourri!!
            logger.debug("envoiS2low: number [" + numero + "], [" + new String(numero.getBytes(), "ISO-8859-1") + "]");
            logger.debug("envoiS2low: decision_date [" + date + "], [" + new String(date.getBytes(), "ISO-8859-1") + "]");
            logger.debug("envoiS2low: objet [" + objet + "], [" + new String(objet.getBytes(), "ISO-8859-1") + "]");
            if (Charset.isSupported("ISO-8859-1")) {
                logger.debug("  Default charset: " + Charset.defaultCharset() + ", ISO-8859-1 is supported");
            }
            logger.debug("   Request Charset: " + post.getRequestCharSet());
        }

        parts.add(new StringPart("subject", objet, "ISO-8859-1"));
        for (FilePart ficPart : fileParts) {
            parts.add(ficPart);
        }
        Part[] partsTab = new Part[parts.size()];
        for (int i = 0; i < parts.size(); i++) {
            partsTab[i] = parts.get(i);
        }

        post.setRequestEntity(new MultipartRequestEntity(partsTab, post.getParams()));
        // Exécution de la méthode POST
        try {
            // logger.debug("QueryString=[[" + post.toString() + "]]");
            int status = client.executeMethod(post);
            if (HttpStatus.SC_OK == status) {
                String reponse = post.getResponseBodyAsString();
                String[] tab = reponse.split("\n");
                if (tab.length == 1 || "KO".equals(tab[0])) {
                    StringBuilder error = new StringBuilder("Erreur retournée par la plate-forme s2low : ");
                    for (int i = 1; i < tab.length; i++) {
                        error.append(tab[i]);
                    }
                    throw new RuntimeException(error.toString());
                }

                // La  transaction s'est bien passée, on renvoie son identifiant
                int res = Integer.parseInt(tab[1]);
                // On enregistre le numéro de transaction attribué par la plate-forme
                Map<QName, Serializable> pptes = new HashMap<QName, Serializable>();
                pptes.put(ParapheurModel.PROP_TRANSACTION_ID, res);
                this.nodeService.addAspect(dossier, ParapheurModel.ASPECT_S2LOW, pptes);
                parapheurService.auditTransmissionTDT(dossier, StatusMetier.STATUS_EN_COURS_TRANSMISSION, "Dossier en télétransmission");

                /*
                 * Positionnement du nom utilisateur dans l'étape courante pour
                 * recuperation par le trigger.
                 */
                parapheurService.setCurrentValidator(dossier, AuthenticationUtil.getRunAsUser());
                startGetS2lowStatusJob(dossier);
            } else {
                logger.error("Echec envoi S2LOW: statut=" + status + ", " + HttpStatus.getStatusText(status));
                throw new RuntimeException("Echec de la récupération de la connexion à la plate-forme : statut = "
                        + status + ", " + HttpStatus.getStatusText(status));
            }
        } catch (SSLHandshakeException ex) {

            // Generating a proper error on expired certificate.
            boolean isCertExpired = StringUtils.equalsIgnoreCase("Received fatal alert: certificate_expired", ex.getMessage());
            // S2low might not return the appropriate error, so we check locally too.
            isCertExpired = isCertExpired || isCertificatExpired(propertiesActes.get("name"), propertiesActes.get("password"));

            if (isCertExpired) {
                throw new ParapheurException(ParapheurErrorCode.TDT_CERTIFICATE_EXPIRED);
            }

            throw ex;
        } finally {
            post.releaseConnection();
        }
    }

    /**
     * @see
     * com.atolcd.parapheur.repo.S2lowService#envoiS2lowHelios(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void envoiS2lowHelios(NodeRef dossier) throws IOException {
        Assert.isTrue(this.parapheurService.isDossier(dossier), "Node Ref doit être de type ph:dossier");
        Assert.isTrue(!this.nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW), "Le dossier a déjà été envoyé à la plate-forme S2LOW");
        Map<String, String> propertiesHelios = getPropertiesHelios((String)this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER));

        //        // Initialisation des objets HttpClient
        //        try {      // COMMONS-SSL
        //            EasySSLProtocolSocketFactory easy = new EasySSLProtocolSocketFactory();
        //            KeyMaterial km = new KeyMaterial(readFile(propertiesHelios.get("name")), propertiesHelios.get("password").toCharArray());
        //            easy.setKeyMaterial(km);
        //            Protocol easyhttps = new Protocol("https", (ProtocolSocketFactory) easy, Integer.parseInt(port));
        //            Protocol.registerProtocol("https", easyhttps);
        //        } catch (Exception e) {
        //            throw new IOException("envoiS2lowHelios: HTTPS impossible.");
        //        }
        //        // HTTPCLIENT
        //        HttpClient client = new HttpClient();
        //        PostMethod post = new PostMethod("https://" + serverAddress + ":" + port + URL_HELIOS_IMPORTER_FICHIER);

        HttpClient client = createConnexionHTTPS(propertiesHelios);
        if (client == null) {
            throw new IOException("envoiS2lowHelios: HTTPS impossible.");
        }
        // API S2LOW-Helios v2.0.1 du 9/9/2009
        PostMethod post = new PostMethod(URL_HELIOS_IMPORTER_FICHIER);
        post.setDoAuthentication(true);

        // Récupération des fichiers
        List<NodeRef> documents = this.parapheurService.getMainDocuments(dossier);
        ArrayList<FilePart> fileParts = new ArrayList<FilePart>();
        ArrayList<Part> parts = new ArrayList<Part>();

        // Premier fichier; Cette version d'API ne permet l'envoi que d'un fichier XML (le premier doc du dossier)
        ContentReader mainDocReader = this.contentService.getReader(documents.get(0), ContentModel.PROP_CONTENT);
        String mainDocMimeType = mainDocReader.getMimetype().trim();
        if (logger.isDebugEnabled()) {
            logger.debug("envoiS2lowHelios (" + documents.size() + " docs): ajout mainDoc type " + mainDocMimeType);
        }
        if (!mainDocMimeType.equalsIgnoreCase(MimetypeMap.MIMETYPE_XML)
                && !mainDocMimeType.contains("text/xml")
                && !mainDocMimeType.contains("application/xml")
                && !mainDocMimeType.contains("application/readerpesv2")
                && !mainDocMimeType.equalsIgnoreCase("application/vnd.xemelios-xml")) {
            throw new RuntimeException("Echec d'envoi: La plateforme HELIOS attend un fichier XML.");
        }
        // File ficXml = ensureMimeType(mainDocReader, mainDocMimeType);
        // FilenameUtils.removeExtension(docname)
        String docname = (String) this.nodeService.getProperty(documents.get(0), ContentModel.PROP_NAME);
        File ficXml = TempFileProvider.createTempFile(FilenameUtils.removeExtension(docname) , ".xml");
        mainDocReader.getContent(ficXml);
        fileParts.add(new FilePart("enveloppe", docname, ficXml));

        for (FilePart ficPart : fileParts) {
            parts.add(ficPart);
        }
        Part[] partsTab = new Part[parts.size()];
        for (int i = 0; i < parts.size(); i++) {
            partsTab[i] = parts.get(i);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("   Request Charset: " + post.getRequestCharSet());
        }
        post.setRequestEntity(new MultipartRequestEntity(partsTab, post.getParams()));
        // Exécution de la méthode POST
        String error = null;
        try {
            // logger.debug("QueryString=[[" + post.toString() + "]]");
            int status = client.executeMethod(post);
            if (HttpStatus.SC_OK == status) {
                String sreponse = post.getResponseBodyAsString();
                if (logger.isDebugEnabled()) {
                    logger.debug("Reponse s2low=[" + sreponse + "]");
                }
                if (sreponse.startsWith("KO")) {
                    error = "Erreur retournée par la plate-forme s2low : " + sreponse.replace('\n', ':');
                } else {
                    InputStream reponse = post.getResponseBodyAsStream();
                    // Le retour est un message formaté XML
                    //           <import>
                    //             <id> numéro de la transaction créée </id>
                    //             <resultat> OK ou KO </resultat>
                    //             <message> message complémentaire </message>
                    //           </import>
                    SAXReader saxreader = new SAXReader();
                    try {
                        Document document = saxreader.read(reponse);
                        Element rootElement = document.getRootElement();
                        Element id = rootElement.element("id");//.getTextTrim();
                        Element resultat = rootElement.element("resultat");
                        Element message = rootElement.element("message");
                        if (id == null || resultat == null || "KO".equals(resultat.getTextTrim())) {
                            error = "Erreur retournée par la plate-forme s2low" + ((message != null) ? " : " + message.getTextTrim() : "");
                        } else {
                            // La  transaction s'est bien passée, on enregistre le numéro de transaction
                            int res = Integer.parseInt(id.getTextTrim());
                            Map<QName, Serializable> pptes = new HashMap<QName, Serializable>();
                            pptes.put(ParapheurModel.PROP_TRANSACTION_ID, res);
                            this.nodeService.addAspect(dossier, ParapheurModel.ASPECT_S2LOW, pptes);

                            // En cas d'étape auto, on modifie le message
                            String auditMessage = "Dossier en télétransmission";
                            if (nodeService.hasAspect(dossier, ParapheurModel.ASPECT_ETAPE_TDT_AUTO)) {
                                auditMessage += " (par envoi automatique)";
                            }

                            parapheurService.auditTransmissionTDT(dossier,
                                    StatusMetier.STATUS_EN_COURS_TRANSMISSION,
                                    auditMessage);
                            /* Positionnement du nom utilisateur dans l'étape courante pour recuperation par le trigger. */
                            parapheurService.setCurrentValidator(dossier, AuthenticationUtil.getRunAsUser());

                            startGetS2lowStatusJob(dossier);
                        }
                    } catch (DocumentException e) {
                        throw new RuntimeException("Echec de la récupération de la réponse du TdT", e);
                    }
                }
                if (error != null) {
                    // Pas d'exception, on rejette le dossier avec pour motif l'erreur renvoyée par S2low
                    logger.warn(error); // BLEX
                    if (logger.isDebugEnabled()) {
                        logger.debug("Rejet du dossier pour correction.");
                    }
                    parapheurService.auditTransmissionTDT(
                            dossier,
                            StatusMetier.STATUS_TRANSMISSION_KO,
                            error);
                    parapheurService.setAnnotationPublique(dossier, error);
                    parapheurService.reject(dossier);
                }
            } else {
                throw new RuntimeException("Echec de la récupération de la connexion à la plate-forme : statut = " + status);
            }
        } catch (SSLHandshakeException ex) {
            // Si certificat expiré, on rend le message plus lisible...
            if("Received fatal alert: certificate_expired".equals(ex.getMessage())) {
                throw new SSLHandshakeException("cause certificat P12 expiré. Veuillez contacter votre administrateur");
            }
            throw ex;
        } finally {
            post.releaseConnection();
        }
    }

    /**
     * BLEX : intervalle de temps pour le job startGetS2lowStatusJob
     * Lecture de la valeur de l'intervalle temps entre interrogations du statut du dossier sur tdt s2low
     * La valeur est définie dans le fichier s2low.properties. Elle doit être de type entier représentant un nombre de minutes
     * @return Integer nombre de minutes
     */
    private Integer getStatutJobIntervalle() {
        if (intervalleInteger == null) {
            String intervalle = configuration.getProperty(ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_KEY);
            if (intervalle == null) {
                logger.warn("Propriete " + ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_KEY + " non trouvee, valeur utilisee par defaut = " + ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_DEFAUT);
                intervalle = ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_DEFAUT;
            } else {
                logger.info("Propriete " + ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_KEY + " trouvee = " + intervalle);
            }
            // La valeur trouvée doit être de type entier (nombre de minutes)
            try {
                intervalleInteger = Integer.parseInt(intervalle);
            } catch (NumberFormatException ex) {
                intervalleInteger = Integer.parseInt(ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_DEFAUT);
                logger.warn("Propriete " + ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_KEY + " mal typee : doit être un entier (nombre de minutes), valeur utilisee par defaut : " + ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_DEFAUT);
            }
            // La valeur doit être supérieure à 0
            if (intervalleInteger.intValue() <= 0) {
                logger.warn("Propriete " + ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_KEY + "  doit être supérieure à 0 (nombre de minutes), valeur utilisee par defaut : " + ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_DEFAUT);
                intervalleInteger = Integer.parseInt(ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_DEFAUT);
            }
        }
        return intervalleInteger;
    }

    /**
     * Lancement de la tache planifiee qui interroge le TDT pour savoir ou en
     * est un dossier en particulier
     *
     * Contribution BLEX : Changement de la date/heure de declenchement pour
     * eviter que toutes les taches se declenchent en meme temps lors d'un
     * redémarrage de l'application, et changement de la periodicite (5 min
     * c'est trop fréquent, et ça met le serveur à genoux)
     *
     * @param dossierRef  le dossier pour lequel on doit interroger S2LOW
     */
    protected void startGetS2lowStatusJob(NodeRef dossierRef) {
        try {
            // BLEX
            long oneMinute = 60 * 1000;

            // BLEX : relance toutes les X min
            // BLEX : Externalisation du délai d'interrogation du TDT dans les fichiers de properties (s2low.properties)
            long maxRepeatInterval = getStatutJobIntervalle() * oneMinute;

            // BLEX : delai de lancement au hasard entre dans 1 minute et X minutes
            long delay=(long)(Math.random()* maxRepeatInterval) + oneMinute ;

            String infoMessage=null;
            if (logger.isInfoEnabled()) {
                infoMessage=
                    "dossier :"
                    +" ["+parapheurService.getCurrentValidator(dossierRef)+"]"
                    +" ["+nodeService.getProperty(dossierRef, ContentModel.PROP_TITLE)+"]"
                        + " [" + dossierRef.getId() + "]";
                int id=(int) (delay/oneMinute);
                int ir=(int) (maxRepeatInterval/oneMinute);
                logger.info (
                    "schedule S2lowGetInfosJob : "
                    +infoMessage
                    +", delay : "+id+"mn"
                        + ", repeat : " + ir + "mn");
            }

            // Trigger: Execute in X minutes, and repeat every 'repeatInterval' minutes
            Trigger trigger = new SimpleTrigger(
                    (String) nodeService.getProperty(dossierRef, ContentModel.PROP_NAME),
                    "S2LOW",
                    // new Date(System.currentTimeMillis() + (2 * 60 * 1000)), // Now + 2 minutes
                    new Date(System.currentTimeMillis() + delay), // BLEX
                    null,
                    SimpleTrigger.REPEAT_INDEFINITELY,
                    maxRepeatInterval);           // was:   5 * 60 * 1000); // 5 minutes

            JobDetail details = new JobDetail(
                    (String) nodeService.getProperty(dossierRef, ContentModel.PROP_NAME),
                    "S2LOW",
                    S2lowGetInfosJob.class);

            Map<String, Object> jobDataMap = new HashMap<String, Object>();
            jobDataMap.put("s2lowService", this);
            jobDataMap.put("transactionService", transactionService);
            jobDataMap.put("dossier", dossierRef);
            jobDataMap.put("runAs", parapheurService.getCurrentValidator(dossierRef));

            // BLEX
            jobDataMap.put(S2lowGetInfosJob.K.infoMessage, infoMessage);

            details.setJobDataMap(new JobDataMap(jobDataMap));

            scheduler.scheduleJob(details, trigger);
        } catch (Exception e) {
            throw new RuntimeException("Error scheduling job", e);
        }
    }

    protected void startGetMailSecS2lowStatusJob(NodeRef dossierRef) {
        try {
            // BLEX
            long oneMinute = 60 * 1000;

            // BLEX : relance toutes les X min
            // Externalisation du délai d'interrogation du TDT dans les fichiers de properties (s2low.properties)
            long maxRepeatInterval = getStatutJobIntervalle() * oneMinute; // 1 * oneMinute;

            // BLEX : delai de lancement au hasard entre dans 1 minute et X minutes
            long delay=(long)(Math.random()* maxRepeatInterval) + oneMinute ;

            if (logger.isInfoEnabled()) {
                String infoMessage=
                    "dossier :"
                    +" ["+parapheurService.getCurrentValidator(dossierRef)+"]"
                    +" ["+nodeService.getProperty(dossierRef, ContentModel.PROP_TITLE)+"]"
                        + " [" + dossierRef.getId() + "]";
                int id=(int) (delay/oneMinute);
                int ir=(int) (maxRepeatInterval/oneMinute);
                logger.info (
                    "schedule S2lowGetInfosJob : "
                    +infoMessage
                    +", delay : "+id+"mn"
                        + ", repeat : " + ir + "mn");
            }

            // Trigger: Execute in X minutes, and repeat every 'repeatInterval' minutes
            Trigger trigger = new SimpleTrigger(
                    (String) nodeService.getProperty(dossierRef, ContentModel.PROP_NAME),
                    "S2LOW-MAILSEC",
                    // new Date(System.currentTimeMillis() + (2 * 60 * 1000)), // Now + 2 minutes
                    new Date(System.currentTimeMillis() + delay), // BLEX
                    null,
                    SimpleTrigger.REPEAT_INDEFINITELY,
                    maxRepeatInterval);           // was:   5 * 60 * 1000); // 5 minutes

            JobDetail details = new JobDetail(
                    (String) nodeService.getProperty(dossierRef, ContentModel.PROP_NAME),
                    "S2LOW-MAILSEC",
                    S2lowMailSecGetInfosJob.class);

            Map<String, Object> jobDataMap = new HashMap<String, Object>();
            jobDataMap.put("s2lowService", this);
            jobDataMap.put("transactionService", transactionService);
            jobDataMap.put("dossier", dossierRef);
            jobDataMap.put("runAs", parapheurService.getCurrentValidator(dossierRef));


            details.setJobDataMap(new JobDataMap(jobDataMap));

            scheduler.scheduleJob(details, trigger);
        } catch (Exception e) {
            throw new RuntimeException("Error scheduling job", e);
        }
    }


    /**
     * @see com.atolcd.parapheur.repo.S2lowService#getS2lowHeliosListePES_Retour()
     */
    @SuppressWarnings("unchecked")
    @Override
    public void getS2lowHeliosListePES_Retour() throws IOException {
        // get liste des ids de pes_retour selon numéro de collectivité

        if (this.isEnabled()) {
            if (logger.isDebugEnabled()) {
                logger.debug("getS2lowHeliosListePES_Retour Debut");
            }

            // TODO : rendre ça compatible avec l'override par les TYPES
            String typeMetier = null;
            Map<String, String> propertiesHelios = getPropertiesHelios(typeMetier);
            /**
             * Pas la peine d'aller plus loin si le connecteur n'est pas utilisé
             */
            if (!isHeliosEnabled()) {
                if (logger.isInfoEnabled()) {
                    logger.info("Don't do HeliosListePES_Retour, disabled.");
                }
                return;
            }

            String idCollectivite = propertiesHelios.get("collectivite");
            String nomParapheur = propertiesHelios.get("parapheur");

            HttpClient client = createConnexionHTTPS(propertiesHelios);
            if (client == null) {
                throw new IOException("getS2lowHeliosListePES_Retour: HTTPS impossible.");
            }
            GetMethod  get = new GetMethod(URL_HELIOS_GET_LIST + "?collectivite=" + idCollectivite);
            get.setDoAuthentication(true);

            // Exécution de la méthode GET
            try {
                if (logger.isDebugEnabled()) {
                    logger.debug("Debut: try");
                }
                int status = client.executeMethod(get);
                if (logger.isDebugEnabled()) {
                    logger.debug("After helios_get_list.php?collectivite=" + idCollectivite);
                }
                if (HttpStatus.SC_OK == status) {
                    String sreponse = get.getResponseBodyAsString();
                    if (logger.isDebugEnabled()) {
                        logger.debug("Reponse s2low=[" + sreponse + "]");
                    }
                    if (sreponse.startsWith("error")) {
                        throw new RuntimeException("Erreur retournée par la plate-forme s2low: " + sreponse);
                    } else if (sreponse.trim().isEmpty()) {
                        throw new RuntimeException("Erreur: S2LOW renvoie une chaine VIDE !");
                    }
                    InputStream reponse = get.getResponseBodyAsStream();
                    //     <liste>
                    //             <idColl> identifiant de la collectivité </idColl>
                    //             <dateDemande> date de demande de la liste </dateDemande>
                    //             <resultat> OK ou KO </resultat>
                    //             <message> message complémentaire </message>
                    //             <pes_retour>
                    //                 <id> identifiant du pes_retour  </id>
                    //                 <nom> nom </nom>
                    //                 <date> date de réception du pes_retour par le tdt </date>
                    //             </pes_retour>
                    //             <pes_retour>
                    //                 <id> identifiant du pes_retour  </id>
                    //                 <nom> nom </nom>
                    //                 <date> date de réception du pes_retour par le tdt </date>
                    //             </pes_retour>
                    //             ...
                    //     </liste>

                    SAXReader saxreader = new SAXReader();
                    try {
                        Document document = saxreader.read(reponse);
                        Element rootElement = document.getRootElement();
                        Element xresultat = rootElement.element("resultat");
                        Element xmessage = rootElement.element("message");
                        if (xresultat == null || "KO".equals(xresultat.getTextTrim())) {
                            String error = "Erreur retournée par la plate-forme s2low : ";
                            if (xmessage != null) {
                                error += xmessage.getTextTrim();
                            }
                            logger.error(error);
                            throw new RuntimeException(error);
                        }
                        // La  transaction s'est bien passée, on traite la liste
                        Iterator<Element> elementIterator = (Iterator<Element>) rootElement.elementIterator("pes_retour");
                        for (Iterator<Element> i = elementIterator; i.hasNext();) {
                            Element pes_retour = i.next();
                            Element eltIdPes = pes_retour.element("id");
                            Element eltNom = pes_retour.element("nom");
                            Element eltDate = pes_retour.element("date");
                            if (eltIdPes == null || eltIdPes.getTextTrim().isEmpty()) {
                                throw new DocumentException("Traitement liste pes_retour: Element 'id' attendu, mais absent ou vide.");
                            }
                            if (eltNom == null || eltNom.getTextTrim().isEmpty()) {
                                throw new DocumentException("Traitement liste pes_retour: Element 'nom' attendu, mais absent ou vide.");
                            }
                            if (eltDate== null) {
                                throw new DocumentException("Traitement liste pes_retour: Element 'date' attendu, mais absent.");
                            }
                            if (logger.isDebugEnabled()) {
                                logger.debug("Trouvé: id=" + eltIdPes + ", nom=" + eltNom + ", date=" + eltDate);
                            }
                            SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // exemple: "2010-03-25 14:39:08"
                            Date datePes = null;
                            try {
                                datePes = sdformat.parse(eltDate.getTextTrim());
                            } catch (ParseException e) {
                                throw new DocumentException("Traitement liste pes_retour: Element 'date' attendu, mais mal formatté.", e);
                            }
                            // TODO
                            // 1- récup. PES_Retour
                            String idPes = eltIdPes.getTextTrim();
                            String nomPes = eltNom.getTextTrim();
                            InputStream xmlPesRetour = getS2lowHeliosPES_Retour(idPes, typeMetier);
                            // 2- créer dossier PES.    WARN: dans quel parapheur? info à choper dans fichier conf.: nomParapheur
                            NodeRef dossierCree = null;
                            if (!parapheurService.isNomDossierAlreadyExists(nomPes)) {
                                dossierCree = this.parapheurService.createDossierPesRetour(nomPes, nomParapheur, xmlPesRetour, datePes);
                            }
                            // 3- si ok: changer status à "lu" sur s2low: helios_change_status.php
                            if (null != dossierCree) {
                                setS2lowHeliosChangeStatus(idPes, typeMetier);
                            } else {
                                logger.error(idCollectivite + ": Dossier '"+ nomPes +"' non cree dans parapheur '"+ nomParapheur +"', verifier configuration.");
                            }

                        }

                    } catch (DocumentException e) {
                        throw new RuntimeException("Echec de la récupération de la réponse du TdT", e);
                    }
                } else {
                    throw new RuntimeException("Echec de la récupération de la connexion à la plate-forme : statut = " + status);
                }
            } finally {
                get.releaseConnection();
            }
        } // if (this.isEnabled())
    }



    @Override
    public String getSecureMailVersion() throws IOException {
        String retVal = "";
        Map<String, String> propertiesSecureMail = getPropertiesSecureMail();

        HttpClient client = createConnexionHTTPS(propertiesSecureMail);
        if (client == null) {
            throw new IOException("getS2lowMailVersion: HTTPS impossible.");
        }
        GetMethod get = new GetMethod(URL_MAILSEC_VERSION);
        get.setDoAuthentication(true);

        try {
            int status = client.executeMethod(get);

            String response = get.getResponseBodyAsString();

            String[] resp_split = response.split("\n");

            retVal = String.format("%s", resp_split[0]);

        } finally {
            get.releaseConnection();
        }

        return retVal;
    }

    @Override
    public int getSecureMailCount() throws IOException {
        Map<String, String> propertiesSecureMail = getPropertiesSecureMail();
        int nbMail = 0;

        HttpClient client = createConnexionHTTPS(propertiesSecureMail);
        if (client == null) {
            throw new IOException("getNbMail: HTTPS impossible.");
        }
        GetMethod get = new GetMethod(URL_MAILSEC_NB_MAIL);
        get.setDoAuthentication(true);
        try {
            int status = client.executeMethod(get);

            if (status == HttpStatus.SC_OK) {
                String response = get.getResponseBodyAsString();
                nbMail = Integer.valueOf(response.trim());
            } else {
                throw new RuntimeException(String.format("HttpStatus != 200 is %s", HttpStatus.getStatusText(status)));
            }
        } finally {
            get.releaseConnection();
        }

       return nbMail;
    }

    //FIXME: Finish Implementation
    @Override
    public Map<Integer, SecureMailDetail> getSecureMailList(int limit, int offset) throws IOException {
        Map<String, String> propertiesSecureMail = getPropertiesSecureMail();
        Map<Integer, SecureMailDetail> retVal = new HashMap<Integer, SecureMailDetail>();
        int nbMail = 0;

        HttpClient client = createConnexionHTTPS(propertiesSecureMail);
        if (client == null) {
            throw new IOException("getNbMail: HTTPS impossible.");
        }

        GetMethod get = new GetMethod(URL_MAILSEC_LIST_MAIL);

        get.setDoAuthentication(true);

        NameValuePair[] params = {
                new NameValuePair("limit", String.valueOf(limit)),
                new NameValuePair("offset", String.valueOf(offset))
        };
        get.setQueryString(params);
        client.executeMethod(get);

        String response = get.getResponseBodyAsString();

        // put quotes around dates to avoid parsing error
        response = response.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\\+02", "'$0'");

        String[] lines = response.split("\n");

        for (int i = 0; i < lines.length; i++) {

            List<String> colonSeparatedValues = mySplit(lines[i]);

            Integer id = Integer.parseInt(colonSeparatedValues.get(0));
            colonSeparatedValues.get(2);
            colonSeparatedValues.get(3);

            SecureMailDetail detail = new SecureMailDetail();

            detail.id = Integer.parseInt(colonSeparatedValues.get(0));

            detail.date_envoi = parseStringDate(colonSeparatedValues.get(1));

            detail.status = colonSeparatedValues.get(2);
            detail.objet = colonSeparatedValues.get(3);

            retVal.put(detail.id, detail);
        }
        return retVal;
    }

    @Override
    public void getSecureMailInfos(NodeRef dossier) {
        Integer mail_id = (Integer) nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID);

        SecureMailDetail detail;

        try {
            if (logger.isDebugEnabled()) {
                logger.debug(
                        "dossier:" + dossier.getId() + " \"" + nodeService.getProperty(dossier, ContentModel.PROP_TITLE) + "\"");
            }
            detail = this.getSecureMailDetail(mail_id);
        } catch (IOException e) {
            logger.error("unable to contact s2low host",e);
            return;
        } catch (UnknownMailsecTransactionException e) {
            parapheurService.setAnnotationPublique(dossier, e.getMessage());
            parapheurService.reject(dossier);
            try {
                if (logger.isInfoEnabled()) {
                    logger.info(
                            "unschedule S2lowMailSecGetInfosJob : "
                                    + "dossier:" + dossier.getId() + " \"" + nodeService.getProperty(dossier, ContentModel.PROP_TITLE) + "\"");
                }
                scheduler.unscheduleJob(
                        (String) nodeService.getProperty(dossier, ContentModel.PROP_NAME),
                        "S2LOW-MAILSEC");
            } catch (SchedulerException ex) {
                logger.error("Exception canceling Job", ex);
            }
            return;
        }

        if (detail != null && detail.status.equals(STATUS_MAILSEC_FULLY_CONFIRMED)) {
            nodeService.setProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_STATUS, detail.status);

            String mailsecDigest = S2lowServiceImpl.getAnnotation(detail);

            parapheurService.setAnnotationPublique(dossier, mailsecDigest);
            parapheurService.approveV4(dossier, null);

            try {
                if (logger.isInfoEnabled()) {
                    logger.info(
                            "unschedule S2lowMailSecGetInfosJob : "
                            + "dossier:" + dossier.getId() + " \"" + nodeService.getProperty(dossier, ContentModel.PROP_TITLE) + "\"");
                }
                scheduler.unscheduleJob(
                        (String) nodeService.getProperty(dossier, ContentModel.PROP_NAME),
                        "S2LOW-MAILSEC");
            } catch (SchedulerException ex) {
                logger.error("Exception canceling Job", ex);
            }

        }
    }

    static public String getAnnotation(SecureMailDetail detail) {
        Locale fr = Locale.FRENCH;
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG, fr);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        String mailsecDigest = "";
        mailsecDigest += "Envoyé le " + dateFormat.format(detail.date_envoi) + ".\n";

        for (EmissionDetail d : detail.emis) {
            if(d.confirmed) {
                mailsecDigest += d.getEmail() + " reçu le " + dateFormat.format(d.getConfirmationDate()) + ".\n";
            } else {
                mailsecDigest += "Non confirmé par " + d.getEmail() + ".\n";
            }

        }
        return mailsecDigest;
    }

    @Override
    public String cancelMailSecJob(NodeRef dossier) throws IOException {
        Integer mail_id = (Integer) nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID);
        if (logger.isDebugEnabled()) {
            logger.debug("dossier:" + dossier.getId() + " \"" + nodeService.getProperty(dossier, ContentModel.PROP_TITLE) + "\"");
        }
        if (mail_id == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("mail_id = null => no job to cancel");
            }
            return "";
        }
        SecureMailDetail detail = null;
        String mailsecDigest = "";
        try {
            detail = this.getSecureMailDetail(mail_id);
        } catch (IOException e) {
            logger.error("unable to contact s2low host",e);
            return null;
        } catch (UnknownMailsecTransactionException e) {
            // Empty
        }
        if (detail != null && !detail.status.equals(STATUS_MAILSEC_FULLY_CONFIRMED)) {
            // let's cancel the job: unschedule !

            for (EmissionDetail d : detail.emis) {
                Locale fr = Locale.FRENCH;
                DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG, fr);
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                if (d.getConfirmationDate() != null) {
                    mailsecDigest += d.getEmail() + " reçu le " + dateFormat.format(d.getConfirmationDate()) + "\n";
                } else {
                    mailsecDigest += d.getEmail() + " non reçu\n ";
                }
            }

            try {
                if (logger.isInfoEnabled()) {
                    logger.info(
                            "unschedule S2lowMailSecGetInfosJob : "
                            + "dossier:" + dossier.getId() + " \"" + nodeService.getProperty(dossier, ContentModel.PROP_TITLE) + "\"");
                }
                scheduler.unscheduleJob(
                        (String) nodeService.getProperty(dossier, ContentModel.PROP_NAME),
                        "S2LOW-MAILSEC");
            } catch (SchedulerException ex) {
                logger.error("Exception canceling Job", ex);
            }

        } else if (detail != null) {
            throw new IOException("Incohérence: la réception est confirmée, pas possible d'annuler.");
        } else {
            // detail est null, on purge le job quand meme.
            try {
                if (logger.isInfoEnabled()) {
                    logger.info(
                            "unschedule S2lowMailSecGetInfosJob : "
                            + "dossier:" + dossier.getId() + " \"" + nodeService.getProperty(dossier, ContentModel.PROP_TITLE) + "\"");
                }
                scheduler.unscheduleJob(
                        (String) nodeService.getProperty(dossier, ContentModel.PROP_NAME),
                        "S2LOW-MAILSEC");
            } catch (SchedulerException ex) {
                logger.error("Exception canceling Job", ex);
            }
        }
        return mailsecDigest;
    }

    @Override
    public boolean isMailSecJobCancelable(NodeRef dossier) {
        Integer mail_id = (Integer) nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID);
        if (logger.isDebugEnabled()) {
            logger.debug("dossier:" + dossier.getId() + " \"" + nodeService.getProperty(dossier, ContentModel.PROP_TITLE) + "\"");
        }
        SecureMailDetail detail = null;
        if (mail_id != null) {
            try {
                detail = this.getSecureMailDetail(mail_id);
            } catch (IOException e) {
                logger.debug("unable to contact s2low host : " + e.getMessage());
                return false;
            } catch (UnknownMailsecTransactionException e) {
                logger.debug(e.getMessage());
                return true;
            }
        }
        return (detail != null && detail.status != null && !detail.status.equals(STATUS_MAILSEC_FULLY_CONFIRMED));
    }

    public class EmissionDetail {
        public String email;
        public String input_fields;
        public boolean confirmed;
        public Date confirmationDate;
        public String timeZoneOffset;

        public String getEmail() {
            return email;
        }

        public String getInput_fields() {
            return input_fields;
        }

        public boolean isConfirmed() {
            return confirmed;
        }

        public Date getConfirmationDate() {
            return confirmationDate;
        }
    }

    public class FileDetail {
        Integer size;
        String type;
        String name;

        public Integer getSize() {
            return size;
        }

        public String getType() {
            return type;
        }

        public String getName() {
            return name;
        }
    }

    /**
     * inner class that holds the mail's details
     */
    public class SecureMailDetail {
        public Integer id;
        public Date date_envoi;
        public String password;
        public String fn_download;
        public String status;
        public String objet;
        public List<EmissionDetail> emis;
        public List<FileDetail> files;
        public String message;

        @Override
        public String toString() {
            return String.format("%d | %s | %s | %s | %s| %s| %s | %s", id, date_envoi, password, fn_download, status, objet, emis, message);
        }

        public SecureMailDetail() {
            emis = new ArrayList<EmissionDetail>();
            files = new ArrayList<FileDetail>();
        }

        public Integer getId() {
            return id;
        }

        public Date getDateEnvoi() {
            return date_envoi;
        }

        public String getPassword() {
            return password;
        }

        public String getFn_download() {
            return fn_download;
        }

        public String getStatus() {
            return status;
        }

        public String getObjet() {
            return objet;
        }

        public List<EmissionDetail> getEmis() {
            return emis;
        }

        public String getMessage() {
            return message;
        }
    }

    //protected

    protected class NodeRefPartSource implements PartSource {

        private String name;
        private NodeRef nodeRef;
        // private ByteBuffer content;
        private ContentData contentData;
        private InputStream contentInputStream;
        public NodeRefPartSource(NodeRef source) throws IOException {
            nodeRef = source;
            ContentReader tmpReader = contentService.getReader(nodeRef, ContentModel.PROP_CONTENT);
            contentData = tmpReader.getContentData();
            name = (String)nodeService.getProperty(nodeRef, ContentModel.PROP_NAME);
            contentInputStream = tmpReader.getContentInputStream();
        }

        @Override
        public long getLength() {
            return contentData.getSize();
        }

        @Override
        public String getFileName() {
            return name;
        }

        @Override
        public InputStream createInputStream() throws IOException {
            return contentInputStream;
        }

        public String getEncoding() {
            return contentData.getEncoding();
        }
    }

    protected List<String> mySplit(String s) {
        List<String> retVal = new ArrayList<String>();

        String buffer = "";
        boolean inQuotes = false;
        for (int i=0; i < s.length(); i++) {
            switch (s.charAt(i)) {
                case ':':
                    if (!inQuotes) {
                        retVal.add(buffer);
                        buffer = "";
                        continue;
                    }
                    break;
                case '\'':
                    inQuotes = !inQuotes;
                    continue;
            }
            buffer = buffer + s.charAt(i);
        }

        if (buffer.length() > 0) {
            retVal.add(buffer);
        }


        return retVal;
    }

    /**
     * Converts a list of string to a comma separated string
     *
     * @param mails
     * @return a comma separated string representing the list
     */
    protected String emailListToString(List<String> mails) {
        String retVal = null;
        if (mails != null) {
            retVal = "";
            for (int i=0; i<mails.size()-1; i++) {
                retVal += mails.get(i) + ", ";
            }
            retVal += mails.get(mails.size()-1);
        }
        return retVal;
    }

    @Override
    public SecureMailDetail getSecureMailDetail(int id) throws IOException, UnknownMailsecTransactionException {
        SecureMailDetail secureMailDetail = null;
        Map<String, String> propertiesSecureMail = getPropertiesSecureMail();
        HttpClient client = createConnexionHTTPS(propertiesSecureMail);

        if (client == null) {
            throw new IOException("getSecureMailDetail: HTTPS impossible.");
        }

        GetMethod get = new GetMethod(URL_MAILSEC_DETAIL_MAIL);

        get.setDoAuthentication(true);

        NameValuePair[] params = {
                new NameValuePair("id", String.valueOf(id))
        };
        get.setQueryString(params);

        // Exécution de la méthode GET
        try {

            int status = client.executeMethod(get);

            if (HttpStatus.SC_OK == status) {
                String response = get.getResponseBodyAsString();

                // Dirty Hack
                if (response.contains("ERROR: cette transaction n'existe pas")) {
                    logger.error("La transaction mail-SEC " + id + " n'existe pas sur S2LOW");
                    throw new UnknownMailsecTransactionException("La transaction mail-SEC " + id + " n'existe pas sur S2LOW");
                }
                else {
                    secureMailDetail = new SecureMailDetail();
                    // put quotes around dates to avoid parsing error
                    response = response.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\\+[0-9]{2}", "'$0'");
                    if (logger.isDebugEnabled()) {
                        logger.debug("S2LOW's response=\""  + response + "\"");
                    }

                    String[] lines = response.split("\n");

                    for (int i=0; i<lines.length; i++) {
                        String line = lines[i];
                        // Split String on the first colon
                        String[] keyvalPair = line.split(":", 2);
                        String key = keyvalPair[0].trim();

                        if (key.equals("emis")) {
                            List<String> columnSeparatedValues = mySplit(keyvalPair[1]);
                            EmissionDetail emissionDetail = new EmissionDetail();

                            emissionDetail.email = columnSeparatedValues.get(0);
                            emissionDetail.input_fields = columnSeparatedValues.get(1);
                            emissionDetail.confirmed = columnSeparatedValues.get(2).equals(VALUE_MAILSEC_TRUE);

                            if (emissionDetail.confirmed) {
                                emissionDetail.confirmationDate = parseStringDate(columnSeparatedValues.get(3));
                                emissionDetail.timeZoneOffset = extractTimeZoneOffset(columnSeparatedValues.get(3));
                            }
                            secureMailDetail.emis.add(emissionDetail);
                            continue;
                        }

                        if (key.equals("file")) {
                            List<String> columnSeparatedValues = mySplit(keyvalPair[1]);
                            FileDetail fileDetail = new FileDetail();

                            fileDetail.size = Integer.parseInt(columnSeparatedValues.get(0));
                            fileDetail.type = columnSeparatedValues.get(1);
                            fileDetail.name = columnSeparatedValues.get(2);
                            continue;
                        }

                        if (key.equals(MESSAGE_MAILSEC_ANCHOR)) {
                            String message = "";
                            // skipline:
                            for (int k=i+2; k< lines.length; k++) {
                                message += lines[k] + "\n";
                            }

                            secureMailDetail.message = message;
                            break;
                        }

                        if (key.length() == 0) {
                            continue;
                        }

                        String value = "";

                        Field field;
                        if (keyvalPair.length > 1) {
                            value = keyvalPair[1].trim();
                        }

                        try {
                            field = secureMailDetail.getClass().getField(key);
                        } catch (NoSuchFieldException e) {
                            field = null;
                            logger.error("secureMailDetail(id="+ String.valueOf(id)
                                    + ") NoSuchFieldException with key=" + key, e);
                            logger.error("Maybe because S2low's response=\"" + response + "\" is non standard?");
                        }

                        if (field != null) {
                            try {
                                Class typeOfField = field.getType();
                                Object tValue;
                                if (typeOfField.equals(Integer.class)) {
                                tValue = Integer.parseInt(value);
                                } else if (typeOfField.equals(Date.class)) {
                                    //FIXME: brutal unquoting
                                    value=value.substring(1, value.length()-1);
                                    tValue = parseStringDate(value);
                                } else {
                                    tValue = value;
                                }

                                field.set(secureMailDetail, tValue);
                            } catch (IllegalAccessException e) {
                                //FIXME: log as debug.
                                logger.error("secureMailDetail IllegalAccessException", e);// e.printStackTrace();
                            }
                        }
                    }
                }
            } else {
                throw new RuntimeException("Echec de la récupération de la connexion à la plate-forme : statut = " + status);
            }
        } finally {
            get.releaseConnection();
        }
        return secureMailDetail;
    }

    protected String extractTimeZoneOffset(String stringDate) {
        return "GMT"+stringDate.substring(stringDate.length() - 3, stringDate.length());
    }

    protected Date parseStringDate(String stringDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        Date retVal = null;
        try {
            //FIXME: prendre en compte la timezone
            String dateString = stringDate.substring(0, stringDate.length() - 3);

            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

            retVal = simpleDateFormat.parse(dateString);

        } catch (ParseException e) {
            logger.error("parseStringDate ParseException sur: " + stringDate, e);//e.printStackTrace();
        }

        return retVal;

    }

    private byte[] getBytesFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        // Get the size of the file
        long length = file.length();

        // You can't create an array using a long type. It needs to be an int.
        // Before converting to an int type, check to ensure that file is not
        //  larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
            throw new IOException("File too big, exceeds 2GB limit");
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        // Close the input stream and return bytes
        is.close();
        return bytes;
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#sendSecureMail(java.util.List, java.util.List, java.util.List, String, String, String, boolean, org.alfresco.service.cmr.repository.NodeRef, java.util.List)
     */
    @Override
    public int sendSecureMail(List<String> mailto, List<String> mailcc, List<String> mailcci,
                              String objet, String message, String password,
            boolean send_password, NodeRef dossierRef, List<NodeRef> attachments, boolean includeFirstPage) throws IOException, Exception {

        List<NodeRef> mainDocuments = parapheurService.getMainDocuments(dossierRef);
        List<FilePart> parts = new ArrayList<FilePart>();
        // ContentReader tmpReader = null;
        // Map<String, ByteBuffer> attachments = new HashMap<String, ByteBuffer>();


        int k = 0;
        String mainDocName = "document principal";
        /**
         * First Attachment is the recap doc, a.k.a "PDF d'impression", without annexes.
         */
        File myPDFile = parapheurService.genererDossierPDF(dossierRef, false, null, includeFirstPage);
        parts.add(new FilePart("uploadFile" + (k + 1),
                nodeService.getProperty(dossierRef, ContentModel.PROP_TITLE) + " (copie).pdf",
                myPDFile, "application/pdf", "utf-8"));
        k++;
        /**
         * builds parts from docRef list.
         */
        for (NodeRef docRef : mainDocuments) {
            try {
                if (k==1) {
                    // for now, only one signature file one the first document
                    mainDocName = (String) nodeService.getProperty(docRef, ContentModel.PROP_NAME);
                }
                NodeRefPartSource source = new NodeRefPartSource(docRef);
                parts.add(new FilePart("uploadFile" + (k + 1), source, null, source.getEncoding()));
                k++;
            } catch (IOException e) {
                logger.error("sendSecureMail IOException sur: " + docRef, e);
            }
        }
        for (NodeRef docRef : attachments) {
            try {
                NodeRefPartSource source = new NodeRefPartSource(docRef);
                parts.add(new FilePart("uploadFile" + (k + 1), source, null, source.getEncoding()));
                k++;
            } catch (IOException e) {
                logger.error("sendSecureMail IOException sur: " + docRef, e);
            }
        }
        /**
         * Give the signatures
         */
        String signatureFormat = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT);
        if ("PKCS#7/multiple".equals(signatureFormat)) {
            if (logger.isDebugEnabled()) {
                logger.debug("CMS-AllInOne - prendre une seule signature");
            }
            byte[] signature = parapheurService.getSignature(dossierRef);
            if (signature != null) {
                File tmpFile = TempFileProvider.createTempFile(mainDocName + "_SIG", "p7s");
                if (tmpFile != null) {
                    OutputStream out = new FileOutputStream(tmpFile);
                    out.write(signature);
                    out.close();
                    parts.add(new FilePart("uploadFile" + (k + 1),
                            mainDocName + "_SIG.p7s", tmpFile,
                            "application/pkcs7-signature", "utf-8"));
                }
            }
        } else if (signatureFormat != null && !StringUtils.startsWithIgnoreCase(signatureFormat, "PAdES")) {
            File zipFic = parapheurService.produceZipSignatures(mainDocName, dossierRef);
            if (zipFic != null) {
                parts.add(new FilePart("uploadFile" + (k + 1),
                        mainDocName + "_SIGs.zip", zipFic,
                        "application/zip", "utf-8"));
                k++;
            }
        }


        Integer mail_id = sendSecureMail(mailto, mailcc, mailcci, objet, message, password, send_password, parts);

        // storing the mail id in the folder properties
        nodeService.setProperty(dossierRef, ParapheurModel.PROP_MAILSEC_MAIL_ID, mail_id);
        nodeService.setProperty(dossierRef, ParapheurModel.PROP_MAILSEC_MAIL_STATUS, STATUS_MAILSEC_NOT_CONFIRMED);
        nodeService.setProperty(dossierRef, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_EN_COURS_MAILSEC);

        // This action can be cancelled
        nodeService.setProperty(dossierRef, ParapheurModel.PROP_RECUPERABLE, Boolean.TRUE);

        List<Object> list = new ArrayList<Object>();
        list.add(StatusMetier.STATUS_EN_COURS_MAILSEC);
        parapheurService.auditWithNewBackend("ParapheurServiceCompat", "Envoyé par mail sécurisé.", dossierRef, list);


        /*
         * Positionnement du nom utilisateur dans l'étape courante pour
         * recuperation par le trigger.
         */
        parapheurService.setCurrentValidator(dossierRef, AuthenticationUtil.getRunAsUser());

        // starting the checkJob
        startGetMailSecS2lowStatusJob(dossierRef);

        return mail_id;
    }

    /**
     * WARN : il en manque la moitié (property set, et startJob). Cf au-dessus.
     *
     * @param mailto
     * @param mailcc
     * @param mailcci
     * @param objet
     * @param message
     * @param password
     * @param send_password
     * @param files
     *
     * @return ID Transaction de S2LOW
     * @throws IOException
     */
    @Override
    public int sendSecureMail(List<String> mailto, List<String> mailcc, List<String> mailcci,
                              String objet, String message, String password,
                              boolean send_password, List<FilePart> files) throws IOException {
        PostMethod post = null;
        List<Part> parts = new ArrayList<Part>();
        int retval = -1;
        try {
            String _mailto = emailListToString(mailto);
            String _mailcc = emailListToString(mailcc);
            String _mailcci = emailListToString(mailcci);
            Map<String, String> propertiesSecureMail = getPropertiesSecureMail();

            HttpClient client = createConnexionHTTPS(propertiesSecureMail);
            if (client == null) {
                throw new IOException("getS2lowMailVersion: HTTPS impossible.");
            }

            post = new PostMethod(URL_MAILSEC_SEND_MAIL);

            parts.add(new StringPart("mailto", _mailto));
            //post.setParameter("mailto", _mailto);

            if (_mailcc != null) {
                 parts.add(new StringPart("mailcc", _mailcc));
            }

            if (_mailcci != null) {
                 parts.add(new StringPart("mailcci", _mailcci));
            }

            parts.add(new StringPart("objet", objet));
            parts.add(new StringPart("message", message));

            if (password != null) {
                 parts.add(new StringPart("password", password));
                 parts.add(new StringPart("send_password", send_password ? "1" : "0"));
            }

            if (files != null && files.size() > 0) {
                parts.addAll(files);
            }

            Part[] partsArray = new Part[parts.size()];

            partsArray = parts.toArray(partsArray);

            /*
             * Forcing ISO-8859-1 encoding of string parts
             */
            //FIXME: find out if we should use this encoding
            for (int i=0; i<partsArray.length; i++) {
                if (partsArray[i] instanceof StringPart) {
                    ((StringPart)partsArray[i]).setCharSet("ISO-8859-1");
                }
            }

            post.setRequestEntity(new MultipartRequestEntity(parts.toArray(partsArray), post.getParams()));

            client.executeMethod(post);
            String response = post.getResponseBodyAsString();

            if (response.startsWith("OK:")) {
                String id_mail_str = response.substring(3, response.length());
                retval = Integer.valueOf(id_mail_str.trim());
            } else {
                throw new RuntimeException(response);
            }
        } finally {
            if (post != null) {
                post.releaseConnection();
            }
        }

        return retval;
    }

    @Override
    public boolean isMailServiceEnabled() {
        Map<String, String> props = getPropertiesSecureMail();
        String password = props.get("password");

        return !"_off_".equals(password);
    }

    @Override
    public boolean isActesEnabled() {
        return "true".equals(getPropertiesActes(null).get("active"));
    }

    @Override
    public boolean isHeliosEnabled() {
        return "true".equals(getPropertiesHelios(null).get("active"));
    }


    @Override
    public int sendSecureMail(List<String> mailto, String objet, String message) throws IOException {
        return sendSecureMail(mailto, null, null, objet, message, null, false, (List<FilePart>) null);
    }

    @Override
    public boolean deleteSecureMail(Integer id) throws IOException {
        Map<String, String> propertiesSecureMail = getPropertiesSecureMail();
        boolean retVal;
        HttpClient client = createConnexionHTTPS(propertiesSecureMail);
        if (client == null) {
            throw new IOException("getS2lowMailVersion: HTTPS impossible.");
        }

        GetMethod get = new GetMethod(URL_MAILSEC_DELETE_MAIL);

        NameValuePair[] params = {new NameValuePair("id", String.valueOf(id))};

        get.setQueryString(params);

        client.executeMethod(get);
        String response = get.getResponseBodyAsString();

        if (response.startsWith("OK:")) {
            retVal = true;
        } else {
            retVal = false;
            //throw new RuntimeException(response);
        }

        return retVal;
    }

    protected Map<String, Object> buildModelForTemplate(NodeRef dossier) {
        Map<String, Object> model = new HashMap<String, Object>(8, 1.0f);

        NodeRef person = personService.getPerson(AuthenticationUtil.getRunAsUser());
        model.put("person", new TemplateNode(person, serviceRegistry, null));
        model.put("document", new TemplateNode(dossier, serviceRegistry, null));
        NodeRef parent = nodeService.getPrimaryParent(dossier).getParentRef();
        model.put("space", new TemplateNode(parent, serviceRegistry, null));

        // current date/time is useful to have and isn't supplied by FreeMarker by default
        model.put("date", new Date());

        // add custom method objects
        model.put("hasAspect", new HasAspectMethod());
        model.put("message", new I18NMessageMethod());
        model.put("dateCompare", new DateCompareMethod());

        return model;

    }
    @Override
    public String getSecureMailTemplate(NodeRef dossier) {

        String xpath = "app:company_home/app:dictionary/app:email_templates/cm:parapheur-mailsec-template.ftl";
        NodeRef rootNode = nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        List<NodeRef> results = searchService.selectNodes(rootNode,
                xpath,
                null,
                namespaceService,
                false);

        assert(!results.isEmpty());
        NodeRef templateRef = results.get(0);

        Map<String, Object> model = buildModelForTemplate(dossier);
        String text = serviceRegistry.getTemplateService().processTemplate("freemarker", templateRef.toString(), model);

        return text;
    }

    @Override
    public String getSecureMailMessageWithTemplate(NodeRef dossier, String template) {
       Map<String, Object> model = buildModelForTemplate(dossier);
       return serviceRegistry.getTemplateService().processTemplateString("freemarker", template, model);
    }

    private InputStream getS2lowHeliosPES_Retour(String idRetour, String typeMetier) throws IOException {
        Map<String, String> propertiesHelios = getPropertiesHelios(typeMetier);

        HttpClient client = createConnexionHTTPS(propertiesHelios);
        if (client == null) {
            throw new IOException("getS2lowHeliosPES_Retour: HTTPS impossible.");
        }
        GetMethod  get = new GetMethod(URL_HELIOS_GET_RETOUR + "?idRetour=" + idRetour);
        get.setDoAuthentication(true);

        // Exécution de la méthode GET
        InputStream res = null;
        try {
            int status = client.executeMethod(get);
            if (HttpStatus.SC_OK == status) {
                res = get.getResponseBodyAsStream();
            } else {
                throw new RuntimeException("Echec de la récupération de la connexion à la plate-forme : statut = " + status);
            }
            // Copy result to temporary file.
            File tempFile = TempFileProvider.createTempFile("helios-" + idRetour, ".xml");
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(tempFile);
                byte[] buffer = new byte[4096];
                int readed;
                while ((readed = res.read(buffer)) >= 0) {
                    out.write(buffer, 0, readed);
                }
                out.flush();
            } finally {
                if (out != null) {
                    out.close();
                }
            }

            res = new FileInputStream(tempFile);
        } finally {
            get.releaseConnection();
        }
        return res;
    }

    private void setS2lowHeliosChangeStatus(String idRetour, String typeMetier) throws IOException {
        Map<String, String> propertiesHelios = getPropertiesHelios(typeMetier);
        //        String serverAddress = propertiesHelios.get("server");
        //        String port = propertiesHelios.get("port");
        //        // Initialisation des objets HttpClient
        //        try {        // COMMONS-SSL
        //            EasySSLProtocolSocketFactory easy = new EasySSLProtocolSocketFactory();
        //            KeyMaterial km = new KeyMaterial(readFile(propertiesHelios.get("name")), propertiesHelios.get("password").toCharArray());
        //
        //            easy.setKeyMaterial(km);
        //            Protocol easyhttps = new Protocol("https", (ProtocolSocketFactory) easy, Integer.parseInt(port));
        //            Protocol.registerProtocol("https", easyhttps);
        //        } catch (Exception e) {
        //            logger.warn("Erreur lors de la modification du protocole https");
        //            logger.warn(e.getMessage(), e);
        //            throw new IOException("setS2lowHeliosChangeStatus: HTTPS impossible.");
        //        }
        //        // HTTPCLIENT
        //        HttpClient client = new HttpClient();
        //        GetMethod get = new GetMethod("https://" + serverAddress + ":" + port +
        //                URL_HELIOS_CHANGE_STATUS + "?idRetour=" + idRetour);

        HttpClient client = createConnexionHTTPS(propertiesHelios);
        if (client == null) {
            throw new IOException("setS2lowHeliosChangeStatus: HTTPS impossible.");
        }
        GetMethod  get = new GetMethod(URL_HELIOS_CHANGE_STATUS + "?idRetour=" + idRetour);
        get.setDoAuthentication(true);

        // Exécution de la méthode GET
        InputStream reponse;
        try {
            int status = client.executeMethod(get);
            if (HttpStatus.SC_OK == status) {
                reponse = get.getResponseBodyAsStream();
                if (logger.isDebugEnabled()) {
                    logger.debug("Reponse s2low=[" + get.getResponseBodyAsString().toString() + "]");
                }
                //            <change>
                //              <id> numéro de la transaction  </id>
                //              <resultat> OK ou KO </resultat>
                //              <message> message complémentaire </message>
                //            </change>
                SAXReader saxreader = new SAXReader();
                try {
                    Document document = saxreader.read(reponse);
                    Element rootElement = document.getRootElement();
                    Element xresultat = rootElement.element("resultat");
                    Element xmessage = rootElement.element("message");
                    if (xresultat == null || "KO".equals(xresultat.getTextTrim())) {
                        String error = "Erreur retournée par la plate-forme s2low : ";
                        if (xmessage != null) {
                            error += xmessage.getTextTrim();
                        }
                        throw new RuntimeException(error);
                    }
                } catch (DocumentException e) {
                    throw new RuntimeException("Echec de la récupération de la réponse du TdT");
                }
            } else {
                throw new RuntimeException("Echec de la récupération de la connexion à la plate-forme : statut = " + status);
            }
        } finally {
            get.releaseConnection();
        }
    }

    @Override
    public List<String> isCertificateOk(Map<String, String> properties, InputStream is) {
        try {
            HttpClient client = is != null ? createConnexionHTTPS(properties, is) : createConnexionHTTPS(properties);
            if(client != null) {
                return this.getListLogin(client);
            }
        } catch (IOException ex) {
        }
        return null;
    }

    @Override
    public boolean isConnexionOK(Map<String, String> properties, InputStream is) {
        HttpClient client = is != null ? createConnexionHTTPS(properties, is) : createConnexionHTTPS(properties);
        return client != null && isConnectionOK(client);
    }

    /**
     * Initialise un client HTTPS selon les paramètres donnés en entrée.
     *
     * @param properties  devant contenir: server, port (pour le host), name et
     *    password (certificat). Peut aussi contenir proxyhost, etc.
     * @return une instance de HttpClient ou null en cas de probleme.
     */
    public HttpClient createConnexionHTTPS(Map<String, String> properties) {
        try {
            InputStream is = readFile(properties.get("name"));
            if (is == null) {
                logger.warn("No certificate found, null.");
                return null;
            }
            return createConnexionHTTPS(properties, is);
        } catch (UnrecoverableKeyException ex) {
            logger.error("HTTPS impossible, UnrecoverableKeyException", ex);
        } catch (NoSuchAlgorithmException ex) {
            logger.error("HTTPS impossible, NoSuchAlgorithmException", ex);
        } catch (KeyStoreException ex) {
            logger.error("HTTPS impossible, KeyStoreException", ex);
        } catch (KeyManagementException ex) {
            logger.error("HTTPS impossible, KeyManagementException", ex);
        } catch (IOException ex) {
            logger.error("HTTPS impossible, IOException", ex);
        } catch (CertificateException ex) {
            logger.error("HTTPS impossible, CertificateException", ex);
        } catch (Exception ex) {
            logger.error("HTTPS impossible, autre Exception", ex);
        }
        return null;
    }

    /**
     * Initialise un client HTTPS selon les paramètres donnés en entrée.
     *
     * @param properties  devant contenir: server, port (pour le host), name et
     *    password (certificat). Peut aussi contenir proxyhost, etc.
     * @return une instance de HttpClient ou null en cas de probleme.
     */
    @Override
    public HttpClient createConnexionHTTPS(Map<String, String> properties, InputStream is) {
        try {
            // Voir pour le futur:      AuthSSLProtocolSocketFactory
            //  http://svn.apache.org/viewvc/httpcomponents/oac.hc3x/trunk/src/contrib/org/apache/commons/httpclient/contrib/ssl/AuthSSLProtocolSocketFactory.java?view=markup
            EasySSLProtocolSocketFactory easy = new EasySSLProtocolSocketFactory();
            // System.out.println("password=" + properties.get("password"));
            KeyMaterial km;
            km = new KeyMaterial(is, properties.get("password").toCharArray());
            easy.setKeyMaterial(km);

            int port = Integer.parseInt(properties.get("port"));
            Protocol myhttps = new Protocol("https", (ProtocolSocketFactory) easy, port);
            HttpClient httpclient = new HttpClient();
            httpclient.getHostConfiguration().setHost(properties.get("server"), port, myhttps);

            /**
             * Nouveau dans S2LOW 1.3: authentification MCA+BASIC est possible.
             * Par contre , elle n'est pas obligatoire!
             */
            if (properties.containsKey("userlogin") &&
                    properties.get("userlogin")!=null &&
                    !properties.get("userlogin").trim().isEmpty()) {
                httpclient.getParams().setAuthenticationPreemptive(true);
                if (logger.isDebugEnabled()) {
                    logger.debug("userlogin : " + properties.get("userlogin"));
                    //logger.debug("userpassword : " + properties.get("userpassword"));
                }
                httpclient.getState().setCredentials(AuthScope.ANY,
                        new UsernamePasswordCredentials(properties.get("userlogin"), properties.get("userpassword")));
            }

            /**
             * Valeurs de proxy: proxyhost, etc.
             */
            setProxyingParams(httpclient, properties);
            return httpclient;
//            GetMethod httpget = new GetMethod("/");
//            try {
//                httpclient.executeMethod(httpget);
//                System.out.println(httpget.getStatusLine());
//            } finally {
//                httpget.releaseConnection();
//            }
        } catch (UnrecoverableKeyException ex) {
            logger.error("HTTPS impossible, UnrecoverableKeyException", ex);
        } catch (NoSuchAlgorithmException ex) {
            logger.error("HTTPS impossible, NoSuchAlgorithmException", ex);
        } catch (KeyStoreException ex) {
            logger.error("HTTPS impossible, KeyStoreException", ex);
        } catch (KeyManagementException ex) {
            logger.error("HTTPS impossible, KeyManagementException", ex);
        } catch (IOException ex) {
            logger.error("HTTPS impossible, IOException", ex);
        } catch (CertificateException ex) {
            logger.error("HTTPS impossible, CertificateException", ex);
        } catch (Exception ex) {
            logger.error("HTTPS impossible, autre Exception", ex);
        }
        return null;
    }

    private void setProxyingParams(HttpClient httpClient, Map<String, String> properties) {
        String proxyHost = properties.get("proxyhost");
        String proxyPort = properties.get("proxyport");
        String proxyUserName = properties.get("proxyusername");
        String proxyPassword = properties.get("proxypassword");
        String hostName = properties.get("hostname");
        String domainName = properties.get("domainname");
        boolean doIt = false;

        if (proxyHost != null && !proxyHost.trim().isEmpty()
                && proxyPort != null && !proxyPort.trim().isEmpty()) {
            httpClient.getHostConfiguration().setProxy(proxyHost, Integer.parseInt(proxyPort));
            doIt = true;
        }

        Credentials credentials = null;

        if (proxyUserName !=  null && !proxyUserName.trim().isEmpty() && proxyPassword != null) {
            if (hostName != null && domainName != null) {
                credentials = new NTCredentials(proxyUserName, proxyPassword, hostName, domainName);
            } else {
                credentials = new UsernamePasswordCredentials(proxyUserName, proxyPassword);
            }
        }

        if (credentials != null && doIt) {
            AuthScope authScope = new AuthScope(proxyHost, Integer.parseInt(proxyPort), properties.get("realm"));
            // httpClient.getState().setProxyCredentials(properties.get("realm"), proxyHost, credentials);
            httpClient.getState().setProxyCredentials(authScope, credentials);
            // httpClient.getState().setAuthenticationPreemptive(true);
            httpClient.getParams().setAuthenticationPreemptive(true);
        }
    }

    private InputStream readFile2 (String nameFile) throws Exception {
        InputStream inputStream = null;
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                "/app:company_home/app:dictionary/ph:certificats/cm:" + org.alfresco.webservice.util.ISO9075.encode(nameFile),
                null,
                namespaceService,
                false);

        if (nodes.size() != 1) {
            throw new RuntimeException("readFile: Can't find the file '" + nameFile + "', SIZE=" + nodes.size());
        }
        NodeRef fileNodeRef = nodes.get(0);
        ContentReader reader = contentService.getReader(fileNodeRef, ContentModel.PROP_CONTENT);
        inputStream = reader.getContentInputStream();
        return inputStream;
    }

    private InputStream readFile(String nameFile) throws Exception {
        return readFile(nameFile, null);
    }

    private boolean isCertificatExpired(@NotNull String certificateName, @NotNull String password) {
        boolean result = false;

        try {
            InputStream is = readFile(certificateName);
            String certTest = X509Util.checkPasswordForCertificate(is, password);
            result = StringUtils.startsWithIgnoreCase(certTest, "ex");
            if (is != null) {
                is.close();
            }
        } catch (Exception e) { /* No need */ }

        return result;
    }

    private NodeRef findFile(String nameFile, String xPathString) throws Exception {
        UserTransaction trx = transactionService.getUserTransaction(true);
        try {
            trx.begin();

            String xpath;
            if (xPathString != null && xPathString.startsWith("/app:company_home/app:dictionary/")) {
                xpath = xPathString;
            } else {
                // Recherche du noeud Certificats dans Dictionary
                xpath = "/app:company_home/app:dictionary/ph:certificats";
            }
            ResultSet result = searchService.query(new StoreRef(this.configuration.getProperty("spaces.store")), SearchService.LANGUAGE_XPATH, xpath);
            NodeRef file = null;

            if (result.length() > 0) {
                NodeRef nodeFolder = result.getNodeRef(0);
                List<ChildAssociationRef> childs = this.nodeService.getChildAssocs(nodeFolder);

                // Récupération des fichiers concernant le certificat
                for (ChildAssociationRef child : childs) {
                    String name = this.nodeService.getProperty(child.getChildRef(), ContentModel.PROP_NAME).toString();
                    if (name.equals(nameFile)) {
                        // Sélection du fichier demandé
                        file = child.getChildRef();
                    }
                }
            }
            trx.commit();
            return file;
        } catch (Exception e) {
            try {
                trx.rollback();
            } catch (Exception ex) {
                logger.error("Error during rollback", ex);
            }
            throw e;
        }
    }

    private InputStream readFile(String nameFile, String xPathString) throws Exception {
        UserTransaction trx = transactionService.getUserTransaction(true);
        InputStream inputStream = null;
        try {
            trx.begin();

            String xpath;
            if (xPathString != null && xPathString.startsWith("/app:company_home/app:dictionary/")) {
                xpath = xPathString;
            } else {
                // Recherche du noeud Certificats dans Dictionary
                xpath = "/app:company_home/app:dictionary/ph:certificats";
            }
            ResultSet result = searchService.query(new StoreRef(this.configuration.getProperty("spaces.store")), SearchService.LANGUAGE_XPATH, xpath);

            if (result.length() > 0) {
                NodeRef nodeFolder = result.getNodeRef(0);
                List<ChildAssociationRef> childs = this.nodeService.getChildAssocs(nodeFolder);
                NodeRef file = null;

                // Récupération des fichiers concernant le certificat
                for (ChildAssociationRef child : childs) {
                    String name = this.nodeService.getProperty(child.getChildRef(), ContentModel.PROP_NAME).toString();
                    if (name.equals(nameFile)) {
                        // Sélection du fichier demandé
                        file = child.getChildRef();
                    }
                }

                if (file != null) {
                    ContentReader content = fileFolderService.getReader(file);
                    inputStream = content.getContentInputStream();
                }
            }
            trx.commit();
            return inputStream;
        } catch (Exception e) {
            try {
                trx.rollback();
            } catch (Exception ex) {
                logger.error("Error during rollback", ex);
            }
            throw e;
        }
    }

    /**
     * Convertir un éventuel fichier dans un format attendu.
     * Intérêt limité à ACTES pour s'assurer d'avoir du PDF.
     * 
     * @param reader
     * @param mimeType
     * @return 
     */
    private File ensureMimeType(ContentReader reader, String prefixe, String mimeType) {
        File fic;
        if (MimetypeMap.MIMETYPE_PDF.equalsIgnoreCase(mimeType)) {
            fic = TempFileProvider.createTempFile(prefixe, ".pdf");
        } else {
            fic = TempFileProvider.createTempFile(prefixe, null);
        }
        // Si le type MIME correspond, on l'envoie tel quel
        if (mimeType.equals(reader.getMimetype())) {
            reader.getContent(fic);
        } // Sinon, on commence par le transformer
        else {
            FileContentWriter tmpWriter = new FileContentWriter(fic);
            tmpWriter.setMimetype(mimeType);
            tmpWriter.setEncoding(reader.getEncoding());
            this.contentService.transform(reader, tmpWriter);
        }

        return fic;
    }


    /**
     * Tell if the module is activated.
     *
     * @param properties
     * @return true if activated, false otherwise
     */
    private boolean isActivated(Map<String, String> properties) {
        if (properties != null && properties.get("password") != null
                && !properties.get("password").isEmpty()
                && !properties.get("password").trim().equalsIgnoreCase("_off_")) {
            return true;
        } else {
            return false;
        }
    }
}
