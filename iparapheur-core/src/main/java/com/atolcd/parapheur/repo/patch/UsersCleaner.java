package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.NotificationCenter;
import org.alfresco.repo.module.AbstractModuleComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.QName;

import java.util.Set;

/**
 * UsersCleaner
 *
 * @author manz
 *         Date: 12/03/12
 *         Time: 10:13
 */
public class UsersCleaner extends AbstractModuleComponent {
    private PersonService personService;
    private NodeService nodeService;

    @Override
    protected void executeInternal() throws Throwable {
        AuthenticationUtil.RunAsWork<Void> work = new AuthenticationUtil.RunAsWork<Void>() {
            @Override
            public Void doWork() throws Exception {
                Set<NodeRef> people = personService.getAllPeople();

                for (NodeRef person : people) {
                    nodeService.removeAspect(person, QName.createQName(ParapheurModel.PARAPHEUR_MODEL_URI, "digest-notification"));
                    nodeService.removeProperty(person, QName.createQName(ParapheurModel.PARAPHEUR_MODEL_URI, "digest-notification-list"));
                    nodeService.removeProperty(person, QName.createQName(ParapheurModel.PARAPHEUR_MODEL_URI, "digest-notification-enabled"));
                }

                return null;
            }
        };

        AuthenticationUtil.runAs(work, AuthenticationUtil.getAdminUserName());

    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }
}


