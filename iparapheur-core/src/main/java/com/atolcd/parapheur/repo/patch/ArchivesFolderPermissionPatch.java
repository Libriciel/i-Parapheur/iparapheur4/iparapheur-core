package com.atolcd.parapheur.repo.patch;

import java.util.List;

import org.alfresco.repo.admin.patch.AbstractPatch;
import org.alfresco.repo.importer.ImporterBootstrap;
import org.alfresco.service.cmr.admin.PatchException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.NamespaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ArchivesFolderPermissionPatch extends AbstractPatch
{
   private static Log logger = LogFactory.getLog(ArchivesFolderPermissionPatch.class);
   
   private ImporterBootstrap spacesBootstrap;
   private PermissionService permissionService;
   
   /**
    * @param namespaceService The namespaceService to set.
    */
   public void setNamespaceService(NamespaceService namespaceService)
   {
      this.namespaceService = namespaceService;
   }

   /**
    * @param nodeService The nodeService to set.
    */
   public void setNodeService(NodeService nodeService)
   {
      this.nodeService = nodeService;
   }

   /**
    * @param permissionService The permissionService to set.
    */
   public void setPermissionService(PermissionService permissionService)
   {
      this.permissionService = permissionService;
   }

   /**
    * @param searchService The searchService to set.
    */
   public void setSearchService(SearchService searchService)
   {
      this.searchService = searchService;
   }

   /**
    * @param spacesBootstrap The spacesBootstrap to set.
    */
   public void setSpacesBootstrap(ImporterBootstrap spacesBootstrap)
   {
      this.spacesBootstrap = spacesBootstrap;
   }

   /**
    * @see org.alfresco.repo.admin.patch.AbstractPatch#applyInternal()
    */
   @Override
   protected String applyInternal() throws Exception
   {
      String msg = "Patch Parapheur 1 applique avec succes";
      
      // Récupération du répertoire des archives
      List<NodeRef> results = null;
      String archivesFolderPath = "/app:company_home/ph:archives";
      NodeRef rootNodeRef = nodeService.getRootNode(spacesBootstrap.getStoreRef());
      results = searchService.selectNodes(rootNodeRef, archivesFolderPath, null, namespaceService, false);
      
      if (results == null || results.size() != 1)
      {
         msg = "Impossible de trouver le répertoire d'archives";
         throw new PatchException(msg);
      }
      NodeRef archivesRef = results.get(0);
      
      permissionService.setInheritParentPermissions(archivesRef, false);
      permissionService.setPermission(archivesRef, "GROUP_EVERYONE", PermissionService.CONTRIBUTOR,true);
      
      return msg;
   }
   
   protected void checkProperties()
   {
      try
      {
         super.checkProperties();
      }
      catch (Exception e)
      {
         logger.error("Erreur de pptes dans le patch 1",e);
      }
   }
}
