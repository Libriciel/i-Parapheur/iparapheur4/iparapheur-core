/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atolcd.parapheur.repo.patch;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;

/**
 *
 * @author Vivien Barousse
 */
public class RenameArchiveCorbeillePatch extends XPathBasedPatch {

    public static final String SEARCH_XPATH = "//ph:a-archiver";

    @Override
    protected String getXPathQuery() {
        return SEARCH_XPATH;
    }

    @Override
    protected void patch(NodeRef nodeToPatch) {
        NodeService nodeService = getNodeService();
        nodeService.setProperty(nodeToPatch, ContentModel.PROP_NAME, "Dossiers en fin de circuit");
        nodeService.setProperty(nodeToPatch, ContentModel.PROP_TITLE, "Dossiers en fin de circuit");
        nodeService.setProperty(nodeToPatch, ContentModel.PROP_DESCRIPTION, "Dossiers ayant terminé leur circuit de validation.");
    }

}
