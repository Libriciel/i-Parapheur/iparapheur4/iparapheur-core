/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2015, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 * contact@atolcd.com
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.NotificationCenter;
import com.atolcd.parapheur.repo.ParapheurUserPreferences;
import com.atolcd.parapheur.repo.notifications.NotificationCenterObserver;
import org.adullact.iparapheur.domain.NotificationsDAO;
import org.adullact.iparapheur.repo.notification.Notification;
import org.adullact.iparapheur.repo.notification.socket.SocketMessage;
import org.adullact.iparapheur.repo.notification.socket.SocketServer;
import org.adullact.utils.StringUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.NoSuchPersonException;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64Encoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.Serializable;
import java.util.*;

/**
 * NotificationCenterImpl
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 13/02/12
 *         Time: 14:28
 */
public class NotificationCenterImpl implements NotificationCenter {

    // use the CamelCased services those are the ones with proxies (should we ?)
    @Autowired
    private NodeService nodeService;

    @Autowired
    private PersonService personService;

    @Autowired
    private ParapheurUserPreferences parapheurUserPreferences;

    @Autowired
    private SearchService searchService;

    @Autowired
    private NotificationsDAO notificationsDAO;

    @Autowired
    private NamespaceService namespaceService;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private SocketServer socketServer;

    private static final Logger logger = Logger.getLogger(NotificationCenter.class);

    private List<NotificationCenterObserver> observers;

    public static final String FOOTER_LIBRICIEL = "Ce message a été envoyé par l'application i-Parapheur de Libriciel SCOP, logiciel libre sous licences AGPLv3/CeCILLv2. "
            + "Pour plus d'informations, accédez à la page suivante : "
            + "<a  href=\"http://adullact.net/projects/paraphelec/\" style=\"color: rgb(59, 89, 152); text-decoration: none;\" target=\"_blank\">http://adullact.net/projects/paraphelec/</a>.";
    public static final String FOOTER_BLEX = "Ce message a été envoyé par l'application i-Parapheur de Berger-Levrault";

    private String baseurl;
    private String baseurlsecure;
    private String mailPrefixe;
    private String targetversion;
    private String habillage;
    private String defaultFrom;
    private boolean forceHttps;
    private boolean forceFrom;

    private NodeRef getNotificationRef(NodeRef person) {
        //FIXME: is this the more efficient way to get a child based on his type ?
        List<ChildAssociationRef> children = nodeService.getChildAssocs(person);
        for (ChildAssociationRef child : children) {
            if (child.getTypeQName().equals(ParapheurModel.ASSOC_NOTIFICATION)) {
                return child.getChildRef();
            }
        }

        return null;
    }

    private List<Notification> getUnreadNotificationsList(String username) {
        return notificationsDAO.getUnreadNotificationsForUser(username);

    }

    /* Public methods */
    public NotificationCenterImpl() {
        this.observers = new ArrayList<NotificationCenterObserver>();
    }

    @Override
    public void markNotificationAsRead(String username, Notification notification) {
        notificationsDAO.setNotificationAsRead(username, notification.getUUID());
    }

    @Override
    public void markNotificationsAsRead(String username) {
        List<Notification> unReadNotifications = getUnreadNotificationsForUser(username);

        for (Notification n : unReadNotifications) {
            markNotificationAsRead(username, n);
        }
    }

    @Override
    public void postNotification(String username, Notification notification) {
        // Here, prevent same notification to be sent twice
        boolean hasToAdd = getUnreadNotificationsForUser(username).stream().noneMatch(n ->
                n.getPayload().get("reason").equals(Reason.retard.name()) && notification.getPayload().get("reason").equals(Reason.retard.name()) &&
                        n.getPayload().get("nodeRef").equals(notification.getPayload().get("nodeRef")));
        if(hasToAdd) {
            final Notification fNotif = notification.getNotificationForUser(username);
            AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Void>() {
                @Override
                public Void doWork() throws Exception {
                    notificationsDAO.addNotification(fNotif);
                    // observer pattern
                    notifyObservers(fNotif);
                    return null;
                }
            }, username);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.NotificationCenter#hasCertificate(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean hasCertificate(NodeRef user) {
        boolean res = false;
        if (user != null) {
            String certifString = (String) nodeService.getProperty(user, ParapheurModel.PROP_ID_CERTIFICAT);
            res = (certifString != null && !certifString.trim().isEmpty());
        }
        return res;
    }

    /**
     *
     * @see com.atolcd.parapheur.repo.NotificationCenter#getValidAddressForUser(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public String[] getValidAddressForUser(NodeRef user) {

        String username = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
        String[] preferencesMail = parapheurUserPreferences.getNotificationMailForUsername(username);
        String[] userMail = new String[]{(String) nodeService.getProperty(user, ContentModel.PROP_EMAIL)};

        return StringUtils.firstValidMailAddresses(null, preferencesMail, userMail);
    }

    /**
     * @see com.atolcd.parapheur.repo.NotificationCenter#sendMailToUsers(org.adullact.iparapheur.repo.notification.Notification, java.util.List, java.util.Map)
     */
    @Override
    public void sendMailToUsers(Notification notification, List<String> users, Map<String, File> attachments) {

        // set recipient
        List<String> destinatairesHTTP = new ArrayList<String>();
        List<String> destinatairesHTTPS = new ArrayList<String>();
        for (String user : users) {

            if (personService.personExists(user)) {
                NodeRef person = personService.getPerson(user);
                String[] address = getValidAddressForUser(person);
                if (address != null) {
                    for(String email : address) {
                        if (hasCertificate(person)) {
                            destinatairesHTTPS.add(email.trim());
                        } else {
                            destinatairesHTTP.add(email.trim());
                        }
                    }
                }
            }
        }

        if (!destinatairesHTTP.isEmpty()) {
            sendMailToAddresses(destinatairesHTTP, notification, attachments, "http" + (forceHttps ? "s" : "") +"://" + baseurl);
        }
        if (!destinatairesHTTPS.isEmpty()) {
            sendMailToAddresses(destinatairesHTTPS, notification, attachments, "https://" + baseurlsecure);
        }

    }

    public void sendResetPassword(String username, String usermail, String uuid) {

        String baseURL = "http" + (forceHttps ? "s" : "") +"://" + baseurl;

        Notification notif = new Notification(
                Notification.Target.current.toString(),
                Reason.resetpassword,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
                );

        MimeMessagePreparator mailPreparer = mimeMessage -> {

            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);

            message.setTo(usermail);
            // set subject line
            message.setSubject(mailPrefixe + " " + getSubject(notif));

            // See if an email template has been specified
            NodeRef templateRef = getTemplateRef(notif);
            if (templateRef != null) {
                // build the email template model
                Map<String, Serializable> model = new HashMap<>();

                // Custom fields i-Parapheur
                if (habillage.equals(ParapheurModel.PARAPHEUR_HABILLAGE_VALEUR_DEFAUT)) {
                    model.put(MODEL_FOOTER, FOOTER_LIBRICIEL);
                } else {
                    model.put(MODEL_FOOTER, FOOTER_BLEX);
                }

                String fullURL = baseURL + "/resetpassword?uuid=" + uuid + "&u=" + Base64.getEncoder().encodeToString(username.getBytes());
                model.put(MODEL_URL, fullURL);

                // process the template against the model
                String text = templateService.processTemplate("freemarker", templateRef.toString(), model);

                message.setText(text, true);

                // Getting "from" addresses
                String fromUserMail = "";
                String fallbackFrom = "ne-pas-repondre@iparapheur-template-adress.org";

                // Setting appropriate "from" address

                String finalAddress = StringUtils.firstValidMailAddress(logger, fromUserMail, defaultFrom, fallbackFrom);
                message.setFrom(finalAddress);

                logger.debug("fromUserMail                 : " + fromUserMail);
                logger.debug("defaultFrom                  : " + defaultFrom);
                logger.debug("fallbackFrom                 : " + fallbackFrom);
                logger.debug("                            => " + finalAddress);

                //
            }
        };

        try {
            // Send the message
            javaMailSender.send(mailPreparer);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    protected void sendMailToAddresses(final List<String> destinataires, final Notification notification, final Map<String, File> attachments, final String baseURL) {
        MimeMessagePreparator mailPreparer = new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws MessagingException {

                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);

                message.setTo(destinataires.toArray(new String[destinataires.size()]));
                // set subject line
                message.setSubject(mailPrefixe + getSubject(notification));

                // See if an email template has been specified
                NodeRef templateRef = getTemplateRef(notification);
                if (templateRef != null) {
                    // build the email template model
                    Map<String, Serializable> model = notification.getPayload();

                    model.put(MODEL_NOM_ACTION_DEMANDEE, getNomAction(notification.getActionDemandee()));
                    model.put(MODEL_NOM_ACTION_EFFECTUEE, getNomActionPassee(notification.getActionEffectuee()));
                    model.put(MODEL_AS_SECRETARY, notification.getTarget().equals(Notification.Target.secretariat));

                    // Custom fields i-Parapheur
                    if (habillage.equals(ParapheurModel.PARAPHEUR_HABILLAGE_VALEUR_DEFAUT)) {
                        model.put(MODEL_FOOTER, FOOTER_LIBRICIEL);
                    } else {
                        model.put(MODEL_FOOTER, FOOTER_BLEX);
                    }

                    String fullURL = baseURL;
                    if ("4".equals(targetversion)) {
                        if (notification.getType().equals(NotificationCenter.VALUE_TYPE_BUREAU)) { // redirection vers le dashboard du bureau
                            fullURL += "/#/dashboard/" + notification.getBureauNotifie().getId();

                            if (!CorbeillesService.AUCUNE.equals(notification.getBanette())) { // redirection vers la banette du bureau (filtres)
                                fullURL += "/" + notification.getBanette();
                            }
                        }
                        else if (notification.getType().equals(NotificationCenter.VALUE_TYPE_DOSSIER)) {
                            fullURL += "/#/apercu/" + notification.getDossier().getId();
                        }
                    }
                    else if (notification.getType().equals(NotificationCenter.VALUE_TYPE_DOSSIER)) { // redirection vers l'apercu du dossier
                        fullURL += "/navigate/browse/" + StoreRef.STORE_REF_WORKSPACE_SPACESSTORE + "/" + notification.getDossier().getId();
                    }

                    model.put(MODEL_URL, fullURL);

                    if ((notification.getReason() == Reason.delegation) && (notification.getTarget() == Notification.Target.current)){
                        // On récupère le nombre de dossiers en délégation.
                        // On ne passe pas par les fonctions de parapheurService et corbeillesService pour éviter les dépendances.
                        // Si ces dépendances sont rajoutées, on pourra modifier le code ci-dessous
                        List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(notification.getBureauNotifie(), ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_DOSSIERS_DELEGUES);
                        if (childAssocs.size() == 1) {
                            NodeRef banetteDelegues = childAssocs.get(0).getChildRef();
                            Integer nbDossiersDelegues = (Integer) nodeService.getProperty(banetteDelegues, ParapheurModel.PROP_CHILD_COUNT);
                            model.put(MODEL_NB_DOSSIERS, "" + nbDossiersDelegues);
                        }
                    }

                    // process the template against the model
                    String text = templateService.processTemplate("freemarker", templateRef.toString(), model);

                    message.setText(text, true);

                    // Getting "from" addresses
                    String fromUserMail = "";
                    if(!forceFrom) {
                        try {
                            NodeRef valideur = personService.getPerson(notification.getUserValideur(), false);
                            fromUserMail = (String) nodeService.getProperty(valideur, ContentModel.PROP_EMAIL);
                        } catch(NoSuchPersonException nspe) {
                            // Keep going...
                        }
                    }
                    String fallbackFrom = "ne-pas-repondre@iparapheur-template-adress.org";

                    // Setting appropriate "from" address

                    String finalAddress = StringUtils.firstValidMailAddress(logger, fromUserMail, defaultFrom, fallbackFrom);
                    message.setFrom(finalAddress);

                    logger.debug("fromUserMail                 : " + fromUserMail);
                    logger.debug("defaultFrom                  : " + defaultFrom);
                    logger.debug("fallbackFrom                 : " + fallbackFrom);
                    logger.debug("                            => " + finalAddress);

                    //

                    if (attachments != null) {
                        for (Map.Entry<String, File> attachment : attachments.entrySet()) {
                            message.addAttachment(attachment.getKey(), attachment.getValue());
                        }
                    }
                }
            }
        };

        try {
            // Send the message
            javaMailSender.send(mailPreparer);
        } catch (Throwable e) {
            // don't stop the action but let admins know email is not getting sent
            String to = "[";
            for (int i = 0; i < destinataires.size(); i++) {
                if (i > 0) {
                    to += "; ";
                }
                to += destinataires.get(i);
            }
            to += "]";

            logger.error("Erreur lors de l'envoi de mail à " + to, e);
        }
    }

    private NodeRef getTemplateRef(Notification notification) {
        String templateName;
        if (notification.getTarget() == Notification.Target.tiers) {
            templateName = "cm:parapheur-mail-tiers.ftl";
        }
        else {
            templateName = "cm:parapheur-mail-" + notification.getReason().name() + ".ftl";
        }

        NodeRef templateRef = null;
        List<NodeRef> results;
        // Récupération du modèle de mail (ftl)
        try {
            results = searchService.selectNodes(
                    nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                    "app:company_home/app:dictionary/app:email_templates/" + templateName,
                    null,
                    namespaceService,
                    false);

        } catch (AccessDeniedException e) {
            logger.warn("Impossible d'accéder au répertoire de templates.");
            return null;
        }
        if (results != null && !results.isEmpty()) {
            templateRef = results.get(0);
        }
        else {
            logger.warn("Le modèle d'email \"" + templateName + "\" est introuvable");
        }
        return templateRef;

    }

    private String getSubject(Notification notification) {
        String subject = "";
        Notification.Target target = notification.getTarget();
        switch (notification.getReason()) {
            case approve :
                if (target == Notification.Target.diff) {
                    if (EtapeCircuit.ETAPE_ARCHIVAGE.equals(notification.getActionEffectuee())) {
                        subject = "Dossier archivé";
                    } else {
                        subject = "Dossier en cours";
                    }
                }
                else {
                    subject = "Dossier à traiter";
                }
                break;
            case reject:
                subject = "Dossier rejeté";
                break;
            case remord:
                subject = "Dossier repris";
                break;
            case reviewing:
                if (target == Notification.Target.secretariat) {
                    subject = "Dossier à relire/annoter";
                }
                else {
                    subject = "Dossier relu/annoté";
                }
                break;
            case print:
                subject = "Dossier à imprimer";
                break;
            case deleteAdmin:
                subject = "Dossier supprimé";
                break;
            case moveAdmin:
                subject = "Dossier transféré";
                break;
            case delegation:
                if (target == Notification.Target.previous) {
                    subject = "Fin de délégation";
                }
                else {
                    subject = "Nouvelle délégation";
                }
                break;
            case retard:
                subject = "Dossier en retard";
                break;
            case resetpassword:
                subject = "Réinitialisation de mot de passe";
                break;
            default :
                subject = "Dossier";
                break;
        }
        if (notification.getType() == VALUE_TYPE_DOSSIER) {
            subject += " : \"" + notification.getDossierName() + "\"";
        }
        return subject;
    }

    private String getNomAction(String action) {
        String actionName;
        switch(action) {
            case EtapeCircuit.ETAPE_ARCHIVAGE:
                actionName = "archivage";
                break;
            case EtapeCircuit.ETAPE_SIGNATURE:
                actionName = "signature";
                break;
            case EtapeCircuit.ETAPE_TDT:
                actionName = "envoi au TdT";
                break;
            case EtapeCircuit.ETAPE_MAILSEC:
            case EtapeCircuit.ETAPE_MAILSEC_PASTELL:
                actionName = "envoi par mail sécurisé";
                break;
            case EtapeCircuit.ETAPE_VISA:
                actionName = "visa";
                break;
            case EtapeCircuit.ETAPE_CACHET:
                actionName = "cachet serveur";
                break;
            default:
                actionName = "action";
        }
        return actionName;
    }

    private String getNomActionPassee(String action) {
        String actionName;
        if (EtapeCircuit.ETAPE_ARCHIVAGE.equals(action)) {
            actionName = "archivé";
        }
        else if (EtapeCircuit.ETAPE_SIGNATURE.equals(action)) {
            actionName = "signé";
        }
        else if (EtapeCircuit.ETAPE_TDT.equals(action)) {
            actionName = "télétransmis";
        }
        /*else if (EtapeCircuit.ETAPE_DIFF_EMAIL.equals(action)) {

        }*/
        else if (EtapeCircuit.ETAPE_MAILSEC.equals(action)) {
            actionName = "envoyé par mail sécurisé";
        }
        else if (EtapeCircuit.ETAPE_VISA.equals(action)) {
            actionName = "visé";
        }
        else {
            actionName = "traité";
        }
        return actionName;
    }

    @Override
    public void broadcastNotification(List<String> users, Notification notification) {
        broadcastNotification(users, notification, true, true);
    }

    public void broadcastNotification(List<String> users, Notification notification, boolean digest, boolean forceMail) {
        // On prend un set pour ne pas notifier d'utilisateurs en double.
        Set<String> usersToNotify = new HashSet<String>();
        usersToNotify.addAll(users);
        List<String> usersToMail = new ArrayList<String>();

        for (String user : usersToNotify) {
            if (parapheurUserPreferences.isNotificationsEnabledForUsername(user)) {
                // Si les notifications groupées sont activées, on enregistre la notification
                if(parapheurUserPreferences.isDailyDigestEnabledForUsername(user)) {
                    postNotification(user, notification);
                } else if(!digest || forceMail) {
                    usersToMail.add(user);
                }
            }
        }
        if (!usersToMail.isEmpty()) {
            sendMailToUsers(notification, usersToMail, null);
        }

        broadcastSocketNotification(users, notification);
    }

    /**
     * @see com.atolcd.parapheur.repo.NotificationCenter#broadcastSocketNotification(java.util.List, org.adullact.iparapheur.repo.notification.Notification)
     */
    @Override
    public void broadcastSocketNotification(List<String> users, Notification notification) {
        SocketMessage message = getSocketMessageFromNotification(users, notification);
        switch (notification.getReason()) {
            case approve:
            case remord:
            case reviewing:
            case reject:
            case raz:
            case chain:
            case moveAdmin:
            case delegation:
                if (notification.getTarget() == Notification.Target.previous) {
                    message.setState(SocketMessage.State.END);
                }
                else { // current, delegues et secretariat
                    message.setState(SocketMessage.State.NEW);
                }
                break;
            case print:
            case create:
            case retard:
                message.setState(SocketMessage.State.NEW);
                break;
            case archive:
            case delete:
            case signInfo:
            case deleteAdmin:
                message.setState(SocketMessage.State.END);
                break;
            case emit:
                message.setState(SocketMessage.State.EMITTED);
                break;
            case edit:
                message.setState(SocketMessage.State.EDITED);
                break;
            case error:
                message.setState(SocketMessage.State.ERROR);
                break;
        }
        socketServer.notify(message);
    }

    private SocketMessage getSocketMessageFromNotification(List<String> users, Notification notification) {
        ArrayList<String> banettes = new ArrayList<String>(1);
        banettes.add(notification.getBanette());

        String id = notification.getDossier() != null ? notification.getDossier().getId() : notification.getId();
        String idNotifie = notification.getBureauNotifie() != null ? notification.getBureauNotifie().getId() : null;
        return new SocketMessage(
                users, // À renseigner après l'appel de cette fonction
                idNotifie,
                notification.getAnnotation(),
                id,
                notification.getActionEffectuee(),
                notification.getDossierName(),
                null, // État à renseigner après l'appel de cette fonction
                banettes);
    }

    private List<String> getBannettesForState(SocketMessage.State state, Notification notification) {
        List<String> banettes = new ArrayList<String>();
        switch (state) {
            case NEW:
                switch (notification.getTarget()) {
                    case owner:
                        banettes.add(CorbeillesService.EN_PREPARATION);
                    case current:
                        banettes.add(CorbeillesService.A_TRAITER);
                        break ;
                    case delegues:
                        banettes.add(CorbeillesService.DELEGUES);
                        break;
                    case actors:
                        break;
                    case secretariat:
                        break;
                    case next:
                        break;
                    case batch_complete:
                        break;
                }
            break;
            case END:
                switch (notification.getTarget()) {
                    case owner:
                        banettes.add(CorbeillesService.EN_PREPARATION);
                }
                break;
            case CREATED:
                break;
            case REJECTED:
                break;
            case EMITTED:
                break;
            case ERROR:
                break;
            case SENT:
                break;
        }
        return banettes;
    }

    @Override
    public void clearNotifications(String username) {
        // delete things
        //NodeRef person = personService.getPerson(username);
        //clearNotifications(person);
    }

    @Override
    public void enableNotificationsForUser(String username) {
        NodeRef person = personService.getPerson(username);

        if (!nodeService.hasAspect(person, ParapheurModel.ASPECT_NOTIFICATION)) {
            nodeService.addAspect(person, ParapheurModel.ASPECT_NOTIFICATION, null);
            enableNotificationsInPreferences(username);
        } else {
            enableNotificationsInPreferences(username);
        }
    }

    @Override
    public void disableNotificationsForUser(String username) {
        if (parapheurUserPreferences.isNotificationsEnabledForUsername(username)) {
            parapheurUserPreferences.disableNotificationsForUsername(username);
        }
    }

    @Override
    public List<Notification> getUnreadNotificationsForUser(String username) {
        return notificationsDAO.getUnreadNotificationsForUser(username);
    }


    @Override
    public List<Notification> getNotificationsForUser(String username) {
        return notificationsDAO.getNotificationsForUser(username);
    }

    @Override
    public Notification getNotificationForUUID(String username, UUID uuid) {
        return notificationsDAO.getNotificationWithUsername(username, uuid);
    }

    /* observer pattern */
    @Override
    public void registerObserver(NotificationCenterObserver observer, NotificationObserverType type) {
        this.observers.add(observer);
    }

    @Override
    public void unregisterObserver(NotificationCenterObserver observer) {
        this.observers.remove(observer);
    }

    /* TODO: since no write occurs we can safely run it in background
           FIXME: if notifications are too late they're useless ...
        */
    @Override
    public void notifyObservers(Notification notification) {
        for (NotificationCenterObserver observer : observers) {
            observer.notify(notification);
        }
    }

    public void enableNotificationsInPreferences(String username) {
        parapheurUserPreferences.enableNotificationsForUsername(username);
    }

    @Override
    public void deleteNotification(Notification notification) {
        notificationsDAO.removeNotification(notification);
    }

    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
    }

    public void setBaseurlsecure(String baseurlsecure) {
        this.baseurlsecure = baseurlsecure;
    }

    public void setMailPrefixe(String mailPrefixe) {
        this.mailPrefixe = mailPrefixe;
    }

    public void setTargetversion(String targetversion) {
        this.targetversion = targetversion;
    }

    public void setHabillage(String habillage) {
        this.habillage = habillage;
    }

    public void setDefaultFrom(String defaultFrom) {
        this.defaultFrom = defaultFrom;
    }

    public void setForceHttps(String forceHttps) {
        this.forceHttps = Boolean.parseBoolean(forceHttps);
    }

    public void setForceFrom(String forceFrom) {
        this.forceFrom = Boolean.parseBoolean(forceFrom);
    }
}
