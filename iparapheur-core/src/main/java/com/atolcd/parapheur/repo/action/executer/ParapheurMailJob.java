/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.action.executer;

import com.atolcd.parapheur.repo.CorbeillesService;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.transaction.TransactionService;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.ArrayList;
import java.util.List;

/**
 * Identifier tous les dossiers "en retard", et envoyer des emails aux
 * personnes concernées.
 * 
 * @author Vivien Barousse - ADULLACT Projet
 * @author Stephane Vast - ADULLACT Projet
 */
public class ParapheurMailJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap map = context.getJobDetail().getJobDataMap();

        final TenantService tenantService = (TenantService) map.get("tenantService");
        final TenantAdminService tenantAdminService = (TenantAdminService) map.get("tenantAdminService");
        final TransactionService transactionService = (TransactionService) map.get("transactionService");
        final CorbeillesService corbeillesService = (CorbeillesService) map.get("corbeillesService");

        List<String> runAsUsers = new ArrayList<>();
        runAsUsers.add("admin");

        for (Tenant t : tenantAdminService.getAllTenants()) {
            // We don't need to (and we can't) run in disabled tenants.
            if (t.isEnabled()) {
                runAsUsers.add(tenantService.getDomainUser("admin", t.getTenantDomain()));
            }
        }

        for (String runAsUser : runAsUsers) {
            AuthenticationUtil.runAs(() -> {
                transactionService.getRetryingTransactionHelper().doInTransaction(() -> {
                    try {
                        for (NodeRef result : corbeillesService.findLate()) {
                            // On ajoute le dossier dans la bannette en retard (de l'émetteur et du bureau courant) s'il n'y est pas déjà.
                            corbeillesService.updateLate(result);
                            // Le mail passe maintenant par le service de notification (appelé dans updateLate)
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    return null;
                });
                return null;
            }, runAsUser);
        }
    }

}
