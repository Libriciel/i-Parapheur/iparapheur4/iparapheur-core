/*
 * Version 2.1
 * CeCILL Copyright (c) 2008-2012, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 * 
 */
package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import java.io.Serializable;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;

/**
 * Patch permettant de copier la valeur d'une propriété dans une autre seulement
 * si cette deuxième est nulle.
 * 
 * @author jmaire
 */
public class CopyPropertyPatch extends XPathBasedPatch {
    
    private int nbDossier;
    private QName propertySource;
    private QName propertyDest;
    private String xPathQuery;
    
    final int CONTENT_MODEL = 0;
    final int USER_MODEL = 1;
    final int PARAPHEUR_MODEL = 2;
    
    /**
     * Constructeur permettant d'indiquer les deux propriétés (et leur modèle)
     * à utiliser ainsi que la requête XPATH permettant de recherchant les
     * noeuds à patcher.
     * le model peut être : CONTENT_MODEL (0), USER_MODEL (1), PARAPHEUR_MODEL (2).
     * @param propertySrc le nom de la propriété à copier
     * @param propertyDst le nom de la propriété à remplir
     * @param modelSrc entier représentant le model de la propriété source
     * @param modelDst entier représentant le model de la propriété dest
     * @param xPath la requête xPath permettant de rechercher les noeuds à patcher
     */
    public CopyPropertyPatch(String propertySrc, String propertyDst, int modelSrc, int modelDst, String xPath) {
        this.nbDossier = 1;
        this.propertySource = getQName(modelSrc, propertySrc);
        this.propertyDest = getQName(modelDst, propertyDst);
        this.xPathQuery = xPath;
    }

    @Override
    protected String getXPathQuery() {
        return xPathQuery;
    }

    @Override
    protected void patch(NodeRef nodeToPatch) {
        if(nbDossier%100 == 0) {
            System.out.println("CopyPropertyPatch - Dossiers traités : " + nbDossier);
        }
        if (getNodeService().getProperty(nodeToPatch, propertyDest) == null) {
            Serializable prop = getNodeService().getProperty(nodeToPatch, propertySource);
            getNodeService().setProperty(nodeToPatch, propertyDest, prop);
        }
        nbDossier++;
    }
    
    private QName getQName(int model, String property) {
        QName ret;
        switch (model) {    
            case USER_MODEL :
                ret = QName.createQName(ContentModel.USER_MODEL_URI, property);
                break;
            case PARAPHEUR_MODEL :
                ret = QName.createQName(ParapheurModel.PARAPHEUR_MODEL_URI, property);
                break;
            default : // default to CONTENT_MODEL
                ret = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, property);
                break;
        }
        return ret;
    }
}