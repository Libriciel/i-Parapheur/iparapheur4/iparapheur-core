package com.atolcd.parapheur.repo.annotations;

import java.io.Serializable;

/**
 * Rect
 *
 * @author Emmanuel Peralta
 */
public class Rect implements Serializable {
    private static final long serialVersionUID = 1L;

    Point topLeftCorner;
    Point bottomRightCorner;

    public Rect(Double x, Double y, Double w, Double h) {
        this.topLeftCorner = new Point(x, y);
        this.bottomRightCorner = new Point(x + w, y + h);
    }

    public Rect(Point topLeftCorner, Point bottomRightCorner) {
        this.topLeftCorner = topLeftCorner;
        this.bottomRightCorner = bottomRightCorner;
    }

    public Double getWidth() {
        return Math.abs(topLeftCorner.x - bottomRightCorner.x);
    }

    public Double getHeight() {
        return Math.abs(topLeftCorner.x - bottomRightCorner.x);
    }

    public Point getTopLeftCorner() {
        return topLeftCorner;
    }

    public Point getBottomRightCorner() {
        return bottomRightCorner;
    }

    @Override
    public boolean equals(Object o) {
        return ((Rect) o).topLeftCorner.equals(topLeftCorner) && ((Rect) o).bottomRightCorner.equals(bottomRightCorner);
    }
}
