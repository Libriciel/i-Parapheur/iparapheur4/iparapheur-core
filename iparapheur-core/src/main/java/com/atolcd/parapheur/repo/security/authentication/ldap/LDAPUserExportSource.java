/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.repo.security.authentication.ldap;

import java.util.Collection;
import java.util.Map;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import net.sf.acegisecurity.providers.encoding.PasswordEncoder;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.importer.ExportSource;
import org.alfresco.repo.importer.ExportSourceImporterException;
import org.alfresco.repo.security.authentication.ldap.LDAPInitialDirContextFactory;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.io.XMLWriter;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

public class LDAPUserExportSource implements ExportSource
{
   private static Log logger = LogFactory.getLog(LDAPUserExportSource.class);
   
   private NamespaceService namespaceService;
   private PasswordEncoder passwordEncoder;
   private LDAPInitialDirContextFactory ldapInitialContextFactory;
   private String searchBase;
   private String userQuery = "(objectclass=inetOrgPerson)";
   private String userIdAttributeName;
   private boolean errorOnMissingUID;
   private Map<String, String> attributeMapping;
   
   /**
    * @param namespaceService The namespaceService to set.
    */
   public void setNamespaceService(NamespaceService namespaceService)
   {
      this.namespaceService = namespaceService;
   }
   
   /**
    * @param ldapInitialContextFactory The ldapInitialContextFactory to set.
    */
   public void setLDAPInitialDirContextFactory(LDAPInitialDirContextFactory ldapInitialDirContextFactory)
   {
       this.ldapInitialContextFactory = ldapInitialDirContextFactory;
   }
   
   /**
    * @param passwordEncoder The passwordEncoder to set.
    */
   public void setPasswordEncoder(PasswordEncoder passwordEncoder)
   {
      this.passwordEncoder = passwordEncoder;
   }

   /**
    * @param searchBase The searchBase to set.
    */
   public void setSearchBase(String searchBase)
   {
      this.searchBase = searchBase;
   }
   
   /**
    * @param userQuery The userQuery to set.
    */
   public void setUserQuery(String parapheurQuery)
   {
      this.userQuery = parapheurQuery;
   }
   
   /**
    * @param parapheurIdAttributeName The parapheurIdAttributeName to set.
    */
   public void setUserIdAttributeName(String userIdAttributeName)
   {
      this.userIdAttributeName = userIdAttributeName;
   }
   
   /**
    * @param errorOnMissingUID The errorOnMissingUID to set.
    */
   public void setErrorOnMissingUID(boolean errorOnMissingUID)
   {
      this.errorOnMissingUID = errorOnMissingUID;
   }
   
   /**
    * @param attributeMapping The attributeMapping to set.
    */
   public void setAttributeMapping(Map<String, String> attributeMapping)
   {
      this.attributeMapping = attributeMapping;
   }
   
   /**
    * @see org.alfresco.repo.importer.ExportSource#generateExport(org.dom4j.io.XMLWriter)
    */
    @Override
   public void generateExport(XMLWriter writer)
   {
      Collection<String> prefixes = namespaceService.getPrefixes();
      
      QName childQName = QName.createQName(NamespaceService.REPOSITORY_VIEW_PREFIX, "childName", namespaceService);
      
      try
      {
         writer.startDocument();
         for (String prefix : prefixes)
         {
            if (!prefix.equals("xml"))
            {
               String uri = namespaceService.getNamespaceURI(prefix);
               writer.startPrefixMapping(prefix, uri);
            }
         }
         
         writer.startElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "view",
               NamespaceService.REPOSITORY_VIEW_PREFIX + ":" + "view", new AttributesImpl());
         
         InitialDirContext ctx = null;
         try
         {
            ctx = ldapInitialContextFactory.getDefaultIntialDirContext();
            
            // Authentication has been successful.
            // Set the current user, they are now authenticated.
            
            SearchControls userSearchCtls = new SearchControls();
            userSearchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            
            userSearchCtls.setCountLimit(Integer.MAX_VALUE);
            
            NamingEnumeration<SearchResult> searchResults = ctx.search(searchBase, userQuery, userSearchCtls);
            
            while (searchResults.hasMoreElements())
            {
               SearchResult result = searchResults.next();
               Attributes attributes = result.getAttributes();
               Attribute uidAttribute = attributes.get(userIdAttributeName);
               if (uidAttribute == null)
               {
                  if(errorOnMissingUID)
                  {
                     throw new ExportSourceImporterException(
                           "Parapheur returned by search does not have mandatory parapheur id attribute " + attributes);
                  }
                  else
                  {
                     logger.warn("Parapheur returned by search does not have mandatory parapheur id attribute " + attributes);
                     continue;
                  }
               }
               String uid = (String) uidAttribute.get(0);
               
               if (logger.isDebugEnabled())
               {
                  logger.debug("Adding user for " + uid);
               }
               AttributesImpl attrs = new AttributesImpl();
               attrs.addAttribute(NamespaceService.REPOSITORY_VIEW_1_0_URI, childQName.getLocalName(), childQName
                     .toPrefixString(), null, ContentModel.TYPE_USER.toPrefixString(namespaceService) + "-" + uid);
               
               writer.startElement(ContentModel.TYPE_USER.getNamespaceURI(), ContentModel.TYPE_USER
                     .getLocalName(), ContentModel.TYPE_USER.toPrefixString(namespaceService), attrs);
               
               for (String key : attributeMapping.keySet())
               {
                  QName keyQName = QName.createQName(key, namespaceService);
                  
                  String attributeName = attributeMapping.get(key);
                  if (attributeName != null)
                  {
                     Attribute attribute = attributes.get(attributeName);
                     if (attribute != null)
                     {
                        String value = null;
                        if (attributeName.equals("userPassword"))
                        {
                           try
                           {
                              value = (String) attribute.get(0);
                           }
                           catch(Exception e)
                           {
                              byte[] tab = (byte[])attribute.get(0);
                              value = new String(tab);
                           }
                           
                           value = passwordEncoder.encodePassword(value, null);
                        }
                        else
                        {
                           value = (String) attribute.get(0);
                        }
                        
                        writer.startElement(keyQName.getNamespaceURI(), keyQName.getLocalName(), keyQName
                              .toPrefixString(namespaceService), new AttributesImpl());
                        
                        if (value != null && !value.equals("null"))
                        {
                           writer.characters(value.toCharArray(), 0, value.length());
                        }
                        else
                        {
                           writer.characters("".toCharArray(), 0, 0);
                        }
                        
                        writer.endElement(keyQName.getNamespaceURI(), keyQName.getLocalName(), keyQName
                              .toPrefixString(namespaceService));
                     }
                  }
                  
               }
               
               // Attributs communs
               writer.startElement(ContentModel.USER_MODEL_PREFIX, "usr", "usr:enabled", new AttributesImpl());
               writer.characters("true".toCharArray(), 0, 4);
               writer.endElement(ContentModel.USER_MODEL_PREFIX, "usr", "usr:enabled");
               
               writer.startElement(ContentModel.USER_MODEL_PREFIX, "usr", "usr:accountExpires", new AttributesImpl());
               writer.characters("false".toCharArray(), 0, 5);
               writer.endElement(ContentModel.USER_MODEL_PREFIX, "usr", "usr:accountExpires");
               
               writer.startElement(ContentModel.USER_MODEL_PREFIX, "usr", "usr:credentialsExpire", new AttributesImpl());
               writer.characters("false".toCharArray(), 0, 5);
               writer.endElement(ContentModel.USER_MODEL_PREFIX, "usr", "usr:credentialsExpire");
               
               writer.startElement(ContentModel.USER_MODEL_PREFIX, "usr", "usr:accountLocked", new AttributesImpl());
               writer.characters("false".toCharArray(), 0, 5);
               writer.endElement(ContentModel.USER_MODEL_PREFIX, "usr", "usr:accountLocked");
               
               
               writer.endElement(ContentModel.TYPE_USER.getNamespaceURI(), ContentModel.TYPE_USER
                     .getLocalName(), ContentModel.TYPE_USER.toPrefixString(namespaceService));
               
            }
         }
         catch (NamingException e)
         {
            throw new ExportSourceImporterException("Failed to import parapheurs.", e);
         }
         finally
         {
            if (ctx != null)
            {
               try
               {
                  ctx.close();
               }
               catch (NamingException e)
               {
                  throw new ExportSourceImporterException("Failed to import parapheurs.", e);
               }
            }
         }
         
         for (String prefix : prefixes)
         {
            if (!prefix.equals("xml"))
            {
               writer.endPrefixMapping(prefix);
            }
         }
         
         writer.endElement(NamespaceService.REPOSITORY_VIEW_PREFIX, "view", NamespaceService.REPOSITORY_VIEW_PREFIX
               + ":" + "view");
         
         writer.endDocument();
      }
      catch (SAXException e)
      {
         throw new ExportSourceImporterException("Failed to create file for import.", e);
      }
   }
}
