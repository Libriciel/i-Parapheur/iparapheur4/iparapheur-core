/*
 * Copyright (C) 2006 Vardar Software, Consultancy, Training Ltd.Sti.
 * http://vardar.biz.tr
 *
 * Licensed under the Mozilla Public License version 1.1. 
 * You may obtain a copy of the License at
 *
 *   http://www.mozilla.org/MPL/MPL-1.1.txt
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the
 * License.
 */
package com.atolcd.parapheur.repo.emailListener;

import java.util.Date;
/**
 * This class keeps the headers of an E-Mail message
 * 
 * @author turgayz
 */
public class EmailMessageHeaders {
	private String from;
	private String to;
	private String cc;
	private String bcc;
	private String subject;
	private Date sentDate;
	private Date receivedDate;
	
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public Date getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	public Date getSentDate() {
		return sentDate;
	}
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getBcc() {
		return bcc;
	}
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("From:").append(from).append("\n");
		sb.append("To:").append(to).append("\n");
		sb.append("Cc:").append(cc).append("\n");
		sb.append("Bcc:").append(bcc).append("\n");
		sb.append("Subject:").append(subject).append("\n");
		sb.append("Sent:").append(sentDate).append("\n");
		sb.append("Received:").append(receivedDate).append("\n");
		return sb.toString();
	}

}
