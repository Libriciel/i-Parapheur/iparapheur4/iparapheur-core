/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.repo.job;

import com.atolcd.parapheur.repo.JobService;
import java.util.List;
import java.util.Map;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.transaction.TransactionService;

/**
 * @author Emmanuel Peralta - ADULLACT Projet
 */
public interface JobInterface extends Runnable {

    /**
     * Tests if the job is currently running
     *
     * @return a boolean, true if the job is running, false otherwise.
     * @since 3.3.0
     */
    public boolean isRunning();

    /**
     * Sets the job running state to the boolean passed as parameter.
     *
     * @param running
     * @since 3.3.0
     */
    public void setRunning(boolean running);

    /**
     * Main method of the job, that is runned by the JobRunner
     *
     * @see com.atolcd.parapheur.repo.JobService
     * @see JobRunner#run()
     * @since 3.3.0
     */
    @Override
    public void run();

    /**
     * Sets the locked NodeRef list
     *
     * @param nodeRefList the list of nodeRef locked by the job
     * @see com.atolcd.parapheur.repo.JobService#postJobAndLockNodes(JobInterface, java.util.List)
     * @since 3.3.0
     */
    public void setLockedNodeRefs(List<NodeRef> nodeRefList);

    /**
     * Gets the list of nodes locked by the job
     *
     * @return a list of noderefs
     * @since 3.3.0
     */
    public List<NodeRef> getLockedNodeRefs();

    /**
     * Saves the exception that occured for dossier
     * @param e the exception to save
     * @param dossier the NodeRef that caused the exception
     * @since 3.3.0
     */
    public void putExceptionForDossier(Exception e, NodeRef dossier);

    /**
     * Gets the exception map built with putExceptionForDossier
     * @return the NodeRef/Exception Map
     * @since 3.3.0
     */
    public Map<NodeRef, Exception> getExceptionsMap();

    /**
     * Gets the username of the user that posted the job.
     * @return the username as a String
     * @since 3.3.0
     */
    public String getUsername();

    /**
     * Setter for JobService for Spring Use
     * @param jobService
     */
    public void setJobService(JobService jobService);

    /**
     * Setter for TransactionService for Spring Use
     * @param transactionService
     */
    public void setTransactionService(TransactionService transactionService);

}
