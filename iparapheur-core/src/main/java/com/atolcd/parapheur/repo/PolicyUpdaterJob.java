/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2009, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD S.A. & ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo;

import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.UserTransaction;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.util.Assert;

/**
 *
 * @author Vivien Barousse
 */
public class PolicyUpdaterJob implements Job {

    public static final Logger logger = Logger.getLogger(PolicyUpdaterJob.class);

    private TenantAdminService tenantAdminService;

    private TenantService tenantService;

    private NodeService nodeService;

    private NamespaceService namespaceService;

    private SearchService searchService;

    private ParapheurService parapheurService;

    private TransactionService transactionService;

    public void execute(JobExecutionContext ctx) throws JobExecutionException {
        JobDataMap dataMap = ctx.getJobDetail().getJobDataMap();

        tenantAdminService = (TenantAdminService) dataMap.get("tenantAdminService");
        tenantService = (TenantService) dataMap.get("tenantService");
        nodeService = (NodeService) dataMap.get("nodeService");
        namespaceService = (NamespaceService) dataMap.get("namespaceService");
        searchService = (SearchService) dataMap.get("searchService");
        parapheurService = (ParapheurService) dataMap.get("parapheurService");
        transactionService = (TransactionService) dataMap.get("transactionService");

        Assert.notNull(tenantAdminService, "tenantAdminService is mandatory");
        Assert.notNull(tenantService, "tenantService is mandatory");
        Assert.notNull(nodeService, "nodeService is mandatory");
        Assert.notNull(namespaceService, "namespaceService is mandatory");
        Assert.notNull(searchService, "searchService is mandatory");
        Assert.notNull(parapheurService, "parapheurService is mandatory");
        Assert.notNull(transactionService, "transactionService is mandatory");

        // Lists all users who should run the timer
        List<String> tenantsAdmins = new ArrayList<String>();

        // Global admin, for the default tenant
        tenantsAdmins.add("admin");

        for (Tenant tenant : tenantAdminService.getAllTenants()) {
            // We don't need to run the timer for disabled tenants.
            if (tenant.isEnabled()) {
                String tenantAdmin = tenantService.getDomainUser("admin", tenant.getTenantDomain());
                tenantsAdmins.add(tenantAdmin);
            }
        }

        // Now do the job, as many times as we need, once as each tenant admin
        for (String admin : tenantsAdmins) {
            UserTransaction utx = transactionService.getUserTransaction();
            try {
                utx.begin();
                logger.debug("Call getS2lowHeliosListePES_Retour as " + admin);
                AuthenticationUtil.runAs(new RunAsWork<Object>() {

                    /**
                     * Do the actual work.
                     *
                     * This method is invoked as each tenant admin.
                     *
                     * We don't need any return value, but implementing RunAsWork
                     * forces to have one. This methods always returns null.
                     *
                     * @return null
                     */
                    public Object doWork() {
                        String xpath = "/app:company_home/app:dictionary/ph:signaturePolicies/*";

                        List<NodeRef> policies = searchService.selectNodes(
                                nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                                xpath,
                                null,
                                namespaceService,
                                false);

                        for (NodeRef policy : policies) {
                            try {
                                parapheurService.updateSignaturePolicyHash(policy);
                            } catch (Exception e) {
                                logger.error("Error updating policy", e);
                            }
                        }

                        return null;
                    }

                }, admin);

                utx.commit();
            } catch (Exception e) {
                logger.error("Error updating policies", e);
                try {
                    utx.rollback();
                } catch (Exception ex) {
                    logger.error("Error durring rollback", ex);
                }
            }
        }

    }

}
