/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.patch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.transaction.UserTransaction;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.module.AbstractModuleComponent;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author Vivien Barousse
 */
public class TypesACL32Patch extends XPathBasedPatch {

    /**
     * Logger
     */
    private static final Logger logger = Logger.getLogger(TypesACL32Patch.class);

    public static String SOUS_TYPES_FILES_XPATH = "/app:company_home/app:dictionary/cm:metiersoustype/*";

    private ContentService contentService;

    @Override
    protected String getXPathQuery() {
        return SOUS_TYPES_FILES_XPATH;
    }

    protected void patch(NodeRef sousTypeRef) throws Exception {
        logger.debug("Types ACL patch: Creating reader");
        ContentReader contentReader = contentService.getReader(sousTypeRef, ContentModel.PROP_CONTENT);
        SAXReader saxReader = new SAXReader();
        logger.debug("Types ACL patch: Building DOM");
        Document xmlDocument = saxReader.read(contentReader.getContentInputStream());
        Iterator<Element> sousTypes = xmlDocument.getRootElement().elementIterator("MetierSousType");
        while (sousTypes.hasNext()) {
            Element sousType = sousTypes.next();
            logger.debug("Types ACL patch: Changing visibility on " + sousType.element("ID").getTextTrim());
            Attribute visibility = sousType.attribute("visibility");
            if (visibility == null) {
                sousType.addAttribute("visibility", "public");
            }
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        XMLWriter xmlWriter = new XMLWriter(out);
        xmlWriter.write(xmlDocument);

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());

        ContentWriter contentWriter = contentService.getWriter(sousTypeRef, ContentModel.PROP_CONTENT, true);
        contentWriter.putContent(in);
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

}
