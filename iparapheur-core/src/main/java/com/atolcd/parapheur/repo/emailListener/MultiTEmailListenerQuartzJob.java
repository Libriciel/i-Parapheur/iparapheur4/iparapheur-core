/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.emailListener;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.SavedWorkflow;
import com.atolcd.parapheur.repo.TypesService;
import com.atolcd.parapheur.repo.WorkflowService;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.NoSuchPersonException;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.util.Assert;

import javax.mail.*;
import javax.mail.internet.MimeUtility;
import javax.transaction.UserTransaction;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author Stephane Vast
 * @author Vivien Barousse
 */
public class MultiTEmailListenerQuartzJob implements Job {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger("MultiTEmailListenerQuartzJob");

    /**
     * Alfresco node service
     */
    private NodeService nodeService;

    /**
     * Alfresco content service
     */
    private ContentService contentService;

    /**
     * Alfresco file/folder service
     */
    private FileFolderService fileFolderService;

    /**
     * Alfresco search service
     */
    private SearchService searchService;

    /**
     * Alfresco namespace service
     */
    private NamespaceService namespaceService;

    /**
     * Alfresco tenant service
     */
    private TenantService tenantService;

    /**
     * Alfresco tenant admin service
     */
    private TenantAdminService tenantAdminService;

    /**
     * Alfresco transaction service
     */
    private TransactionService transactionService;

    /**
     * Alfresco authentication component
     */
    private AuthenticationComponent authenticationComponent;

    /**
     * Parapheur service
     */
    private ParapheurService parapheurService;
    
    /**
     * Dossier Service
     */
    private DossierService dossierService;

    /**
     * Type Service
     */
    private TypesService typesService;

    /**
     * Workflow Service
     */
    private WorkflowService workflowService;

    /**
     * Initializes the current Job
     * 
     * @param ctx
     */
    private void initialize(JobExecutionContext ctx) {
        JobDataMap dataMap = ctx.getJobDetail().getJobDataMap();

        nodeService = (NodeService) dataMap.get("nodeService");
        contentService = (ContentService) dataMap.get("contentService");
        fileFolderService = (FileFolderService) dataMap.get("fileFolderService");
        searchService = (SearchService) dataMap.get("searchService");
        namespaceService = (NamespaceService) dataMap.get("namespaceService");
        tenantService = (TenantService) dataMap.get("tenantService");
        tenantAdminService = (TenantAdminService) dataMap.get("tenantAdminService");
        transactionService = (TransactionService) dataMap.get("transactionService");
        authenticationComponent = (AuthenticationComponent) dataMap.get("authenticationComponent");
        parapheurService = (ParapheurService) dataMap.get("parapheurService");
        typesService = (TypesService) dataMap.get("typesService");
        workflowService = (WorkflowService) dataMap.get("workflowService");
        dossierService = (DossierService) dataMap.get("dossierService");

        Assert.notNull(nodeService, "nodeService required");
        Assert.notNull(contentService, "contentService required");
        Assert.notNull(fileFolderService, "fileFolderService required");
        Assert.notNull(searchService, "searchService required");
        Assert.notNull(namespaceService, "namespaceService required");
        Assert.notNull(tenantService, "tenantService required");
        Assert.notNull(tenantAdminService, "tenantAdminService required");
        Assert.notNull(transactionService, "transactionService required");
        Assert.notNull(authenticationComponent, "authenticationComponent required");
        Assert.notNull(parapheurService, "parapheurService required");
        Assert.notNull(typesService, "typesService required");
        Assert.notNull(workflowService, "workflowService required");
        Assert.notNull(dossierService, "dossierService required");
    }

    /**
     * Executes mail listening for each tenant available (and enabled) on the system.
     * 
     * @param ctx The job execution context
     * @see Job#execute(org.quartz.JobExecutionContext)
     */
    public void execute(JobExecutionContext ctx) {
        initialize(ctx);

        // List of tenants admins, as which the timer should run
        List<String> tenantsAdmins = new ArrayList<String>();

        // Global admin for default tenant execution
        tenantsAdmins.add("admin");

        for (Tenant tenant : tenantAdminService.getAllTenants()) {
            // We dont need to (and we can't) run in disabled tenants
            if (tenant.isEnabled()) {
                String tenantAdmin = tenantService.getDomainUser("admin", tenant.getTenantDomain());
                tenantsAdmins.add(tenantAdmin);
            }
        }

        // Run import for each tenant as tenant admin
        for (String tenantAdmin : tenantsAdmins) {
            EmailsImporter importer = new EmailsImporter();
            try {
                AuthenticationUtil.runAs(importer, tenantAdmin);
            } catch (Exception e) {
                String tenant = tenantService.getUserDomain(tenantAdmin);
                log.error("Error importing mails for tenant " + tenant, e);
            }
        }
    }

    /**
     * Work unit which is run multiple times, once for each tenant, as tenant admin.
     *
     * This class is the one which do the actual work.
     *
     * This class implements RunAsWork so it can be launched by {@link AuthenticationUtil}.
     *
     * @see AuthenticationUtil#runAs(org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork, java.lang.String)
     * @see RunAsWork
     */
    private class EmailsImporter implements RunAsWork<Object> {

        /**
         * Imports mails from the specified mail server.
         *
         * Mail server configuration is stored in a config file in the specific
         * tenant.
         *
         * Implementing RunAsWork forces to have a non-void return type. This
         * method always returns null.
         * 
         * @return null
         * @see RunAsWork#doWork()
         */
        public Object doWork() {
            UserTransaction tx = null;
            try {
                tx = transactionService.getNonPropagatingUserTransaction();
                tx.begin();

                Map<String, String> configuration = getConfiguration();
                if (configuration.size() == 7) {
                    if (configuration.get("enabled").equals("true")) {
                        checkMailBox(configuration);
                    } else {
                        if (log.isEnabledFor(Level.INFO)) {
                            log.info("Email Listener disabled.");
                        }
                    }
                } else {
                    log.error("checkMail failed, configuration items number should be 6, but is = "+configuration.size());
                }
                tx.commit();
            } catch (Exception ex) {
                if (tx != null) {
                    try {
                        if (tx.getStatus() == javax.transaction.Status.STATUS_ACTIVE) {
                            tx.rollback();
                        }
                    } catch (Exception rollbackException) {
                        log.error("Error during transaction rollback", rollbackException);
                    }
                }
                throw new RuntimeException(ex);
            }

            return null;
        }

        /**
         * Returns email configuration for the current tenant.
         *
         * Configuration is read from the email-listener.xml file.
         * 
         * @return Email listener configuration for the current tenant
         */
        private Map<String, String> getConfiguration() {
            NodeRef rootNode = nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
            String confPath = "/app:company_home/app:dictionary/ph:email_listener/ph:email_listener-configuration";
            List<NodeRef> nodes = searchService.selectNodes(rootNode, confPath, null, namespaceService, false);

            if (nodes.size() != 1) {
                throw new RuntimeException("Email listener configuration not found");
            }

            NodeRef confNode = nodes.get(0);

            ContentReader confReader = contentService.getReader(confNode, ContentModel.PROP_CONTENT);
            InputStream confInput = confReader.getContentInputStream();

            SAXReader reader = new SAXReader();
            Map<String, String> configuration = new HashMap<String, String>();
            try {
                Document document = reader.read(confInput);
                Element root = document.getRootElement();
                if (root.element("enabled") != null
                        && root.element("enabled").getTextTrim().equals("true")) {
                    configuration.put("enabled", "true");
                } else {
                    configuration.put("enabled", "false");
                }
                configuration.put("server", root.element("server").getTextTrim());
                configuration.put("protocol", root.element("protocole").getTextTrim());
                configuration.put("port", root.element("port").getTextTrim());
                configuration.put("folder", root.element("folder").getTextTrim());
                configuration.put("username", root.element("username").getTextTrim());
                configuration.put("password", root.element("password").getTextTrim());
            } catch (Exception e) {
                throw new RuntimeException("Error reading configuration", e);
            }
            return configuration;
        }

        private NodeRef findUserByEmail(String from) throws NoSuchPersonException {
            StoreRef spacesStoreRef = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");

            ResultSet results = null;
            String queryStr = "PATH:\"/sys:system/sys:people/*\" +@cm\\:email:" + from;
            results = searchService.query(spacesStoreRef, SearchService.LANGUAGE_LUCENE, queryStr);
            if (results.length() < 1) {
                throw new NoSuchPersonException("Email inconnu dans le Parapheur");
            }
            // XXX Quid s'il existe plusieurs utilisateurs avec cet email ?? Ici on prend le 1er
            return results.getNodeRef(0);
        }

        private NodeRef findCircuitRefFromName(String circuitName) {
            return parapheurService.getAllWorkflows().get(circuitName);
            // return parapheurService.getPublicWorkflows().get(circuitName);
        }

        private void checkMailBox(Map<String, String> configuration) {

            String emailFolder = configuration.get("folder");
            String protocol = configuration.get("protocol");
            String mailServer = configuration.get("server");
            int mailPort = NumberUtils.toInt(configuration.get("port"), 25);
            String emailUserName = configuration.get("username");
            String emailPassword = configuration.get("password");

            // 1 chercher les emails
            if (emailFolder == null || emailFolder.equals("") || protocol.equals("pop3")) {
                emailFolder = "INBOX";
            }
            log.debug("checkMailBox  " + protocol + "://" + emailUserName + "@" + mailServer + ":" + mailPort + "/" + emailFolder);
            URLName url = new URLName(protocol, mailServer, mailPort, emailFolder, emailUserName, emailPassword);
            Properties props = System.getProperties();
            Session session = Session.getDefaultInstance(props, null);
            session.setDebug(false);
            Store store = null;
            Folder folder = null;

            int savedMessagesCount = 0;

            UserTransaction tx = null;
            NodeRef dossierRef = null;

            tx = transactionService.getUserTransaction();
            try {
                // Connect to the e-mail store
                store = session.getStore(url);
                store.connect();

                folder = store.getFolder(emailFolder);
                if (folder == null) {
                    throw new MessagingException("Invalid folder");
                }

                // Open the folder in read-write mode as we will be modifying message properties for IMAP, and delete
                // messages for POP3
                folder.open(Folder.READ_WRITE);

                Message[] msgs = folder.getMessages();

                tx.begin();
                for (int i = 0; i < msgs.length; i++) {
                    // Try to understand if the message is new or not.
                    // This only works for IMAP. If the protocol is POP3, the message is considered as new.
                    boolean messageIsNew = true;
                    Flags flags = msgs[i].getFlags();
                    Flags.Flag[] sysFlags = flags.getSystemFlags();
                    for (Flags.Flag sysFlag : sysFlags) {
                        if (sysFlag.equals(Flags.Flag.SEEN)) {
                            messageIsNew = false;
                        }
                    }
                    if (messageIsNew) {
                        // Get message headers
                        EmailMessageHeaders headers = EmailUtils.extractHeaders(msgs[i]);

                        // Get body and attachments
                        Map<String, Part> attachments = new HashMap<String, Part>();
                        Map<String, String> body = new HashMap<String, String>();
                        body.put("body-text", "");
                        body.put("body-html", "");
                        EmailUtils.dumpPart(msgs[i], attachments, body, 0);

                        try {
                            String sujet = null;
                            if (headers != null && headers.getSubject() != null
                                    && !headers.getSubject().isEmpty()) {
                                sujet = MimeUtility.decodeText(headers.getSubject()).replace("<", "(").replace(">",
                                    ")").replace("@", " AT ").replace("\"", "'").replaceAll("[*\\\\?/:|\\xA3\\xAC%&+;]", "_");
                            }
                            String champFrom=null;
                            if (headers != null && headers.getFrom() != null
                                    && !headers.getFrom().isEmpty()) {
                                champFrom = MimeUtility.decodeText(headers.getFrom().replaceAll("^.*<", "").replaceAll(">$", ""));
                                log.debug("checkMailBox: FROM:[" + champFrom + "], sujet[" + sujet + "]");
                            } else {
                                // On sort !
                                log.error("Le champ FROM de email est vide.");
                                throw new DocumentException("le champ FROM de email est vide.");
                            }

                            NodeRef emetteurNodeRef = findUserByEmail(champFrom);
                            if (emetteurNodeRef == null) {
                                // On sort !
                                log.debug("emetteurNodeRef est NULL: l'émetteur email est inconnu dans le parapheur!");
                                throw new DocumentException("l'émetteur email est inconnu dans le parapheur!");
                            }

                            NodeRef parapheur = null;
                            NodeRef circuitRef = null;
                            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
                            String firstName = new String();
                            String lastName = new String();
                            String nomDocumentDossier = new String();
                            String annotations = new String();

                            // Traitement des propriétés contenues dans le fichier XML
                            if (attachments.containsKey("XML_Dossier_Description.xml")) {
                                /*
                                 * <ph:emailListener xmlns:ph="http://www.adullact.org/models/parapheur">
                                 * <ph:email_emetteur>stephane.vast@adullact-projet.coop</ph:email_emetteur>
                                 * <ph:type>Formulaire</ph:type>
                                 * <ph:soustype>Informatique</ph:soustype>
                                 * <ph:nom_dossier>Demande de Matériel ou Logiciel</ph:nom_dossier>
                                 * <ph:date_limite>2008-01-25</ph:date_limite> <ph:visibilite>PUBLIC</ph:visibilite>
                                 * <ph:signature_papier>false</ph:signature_papier> <ph:annotations_publiques><![CDATA[
                                 * Test d'annotation publique. ]]></ph:annotations_publiques>
                                 * <ph:document>exemple5pages.pdf</ph:document> <!--OPTIONNEL <ph:PJ></ph:PJ> -->
                                 * <ph:nom_circuit>formulaire demande materiel ou logiciel</ph:nom_circuit>
                                 * </ph:emailListener>
                                 */
                                // traiter le message
                                SAXReader saxreader = new SAXReader();
                                Document documentXML = saxreader.read(attachments.get("XML_Dossier_Description.xml").getInputStream());
                                // TODO Schema XML à implémenter
                                Element rootElement = documentXML.getRootElement();
                                //logger.debug("root=["+rootElement.getName()+"], nameSpace=["+ rootElement.getNamespace()+"], nameSpaceURI=["+ rootElement.getNamespaceURI()+"]");
                                if (rootElement.getName().equalsIgnoreCase("emailListener") && rootElement.getNamespaceURI().equalsIgnoreCase("http://www.adullact.org/models/parapheur")) {
                                    // emailEmetteur
                                    Element emailEmetteur = rootElement.element("email_emetteur");
                                    if (emailEmetteur != null) {
                                        champFrom = MimeUtility.decodeText(emailEmetteur.getText().replaceAll("^.*<", "").replaceAll(">$", ""));
                                        emetteurNodeRef = findUserByEmail(champFrom);
                                        if (emetteurNodeRef == null) {
                                            log.debug("emetteurNodeRef est NULL: l'émetteur email est inconnu dans le parapheur!");
                                            throw new DocumentException("XML_Dossier_Description.xml: email_emetteur est inconnu dans i-Parapheur");
                                        }
                                        String userName = (String) nodeService.getProperty(emetteurNodeRef, ContentModel.PROP_USERNAME);
                                        firstName = (String) nodeService.getProperty(emetteurNodeRef, ContentModel.PROP_FIRSTNAME);
                                        lastName = (String) nodeService.getProperty(emetteurNodeRef, ContentModel.PROP_LASTNAME);
                                        authenticationComponent.setCurrentUser(userName);

                                        //TODO put it in parapheur Service getUniqueOwnedParpaheur(userName)
                                        // if it's not unique throws a runtime exception ?
                                        //FIXME eerrrroooor
                                        List<NodeRef> parapheurs = parapheurService.getOwnedParapheurs(userName);
                                        if (parapheurs != null && !parapheurs.isEmpty()) {
                                            parapheur = parapheurs.get(0);
                                        }
                                        else {
                                            parapheur = null;
                                        }
                                        //parapheur = parapheurService.getOwnedParapheur(userName);

                                    } else {
                                        throw new DocumentException("XML_Dossier_Description.xml: email_emetteur absent");
                                    }
                                    // nomDossier
                                    Element nomDossier = rootElement.element("nom_dossier");
                                    if (nomDossier != null && !nomDossier.getTextTrim().isEmpty()) {
                                        properties.put(ContentModel.PROP_NAME, nomDossier.getTextTrim());
                                        properties.put(ContentModel.PROP_TITLE, nomDossier.getTextTrim());
                                    } else {
                                        throw new DocumentException("XML_Dossier_Description.xml: nom_dossier absent");
                                    }
                                    // type
                                    Element typeMetier = rootElement.element("type");
                                    String type = "";
                                    if (typeMetier != null && !typeMetier.getTextTrim().isEmpty()) {
                                        type = typeMetier.getTextTrim();
                                        properties.put(ParapheurModel.PROP_TYPE_METIER, type);
                                    } else {
                                        throw new DocumentException("XML_Dossier_Description.xml: type absent");
                                    }
                                    // soustype
                                    Element soustypeMetier = rootElement.element("soustype");
                                    String soustype = "";
                                    if (soustypeMetier != null && !soustypeMetier.getTextTrim().isEmpty()) {
                                        soustype = soustypeMetier.getTextTrim();
                                        properties.put(ParapheurModel.PROP_SOUSTYPE_METIER, soustype);
                                    } else {
                                        throw new DocumentException("XML_Dossier_Description.xml: soustype absent");
                                    }
                                    // circuit, lecture obligatoire, signature electronique obligatoire
                                    boolean digitalSignatureMandatory = false;
                                    try {
                                        circuitRef = typesService.getWorkflow(type, soustype);// , scriptCustomProperties);
                                        // ex        parapheurService.getCircuitRef(creerDossierRequest.getTypeTechnique(), creerDossierRequest.getSousType());
                                        properties.put(ParapheurModel.PROP_READING_MANDATORY, typesService.isReadingMandatory(type, soustype)); // Boolean.FALSE);
                                        digitalSignatureMandatory = typesService.isDigitalSignatureMandatory(type, soustype);
                                        properties.put(ParapheurModel.PROP_DIGITAL_SIGNATURE_MANDATORY, digitalSignatureMandatory);
                                    } catch (RuntimeException e) {
                                        if (log.isEnabledFor(Level.WARN)) {
                                            log.warn("typesService.getWorkflow loupé: " + e.getMessage() + "\n" + e.toString());
                                        }
                                        throw new DocumentException("circuit INCONNU pour ["
                                                + type + "][" + soustype
                                                + "] ");
                                    }
                                    if (circuitRef == null) {
                                        throw new DocumentException("circuit INCONNU pour ["
                                                + type + "]["
                                                + soustype + "] ");
                                    }

                                    // dateLimite
                                    Element dateLimite = rootElement.element("date_limite");
                                    if (dateLimite != null && !dateLimite.getTextTrim().isEmpty()) {
                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                        properties.put(ParapheurModel.PROP_DATE_LIMITE, dateFormat.parse(dateLimite.getText()));
                                    }
                                    // Visibilite
                                    Element visibilite = rootElement.element("visibilite");
                                    if (visibilite != null && !visibilite.getTextTrim().isEmpty()) {
                                        if (visibilite.getText().equalsIgnoreCase("Confidentiel")) {
                                            properties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.TRUE);
                                            properties.put(ParapheurModel.PROP_PUBLIC, Boolean.FALSE);
                                        } else if (visibilite.getText().equalsIgnoreCase("Public")) {
                                            properties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.FALSE);
                                            properties.put(ParapheurModel.PROP_PUBLIC, Boolean.TRUE);
                                        } else {
                                            properties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.FALSE);
                                            properties.put(ParapheurModel.PROP_PUBLIC, Boolean.FALSE);
                                        }
                                    } else {
                                        properties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.FALSE);
                                        properties.put(ParapheurModel.PROP_PUBLIC, Boolean.FALSE);
                                    }
                                    // Signature papier, seulement si c'est permis!
                                    if (!digitalSignatureMandatory) {
                                    Element signaturePapier = rootElement.element("signature_papier");
                                    if (signaturePapier != null && !signaturePapier.getTextTrim().isEmpty()) {
                                        properties.put(ParapheurModel.PROP_SIGNATURE_PAPIER, Boolean.parseBoolean(signaturePapier.getText()));
                                        }
                                    }
                                    // Annotations publiques
                                    Element annotations_publiques = rootElement.element("annotations_publiques");
                                    if (annotations_publiques != null) {
                                        annotations = annotations_publiques.getText();
                                    }
                                    // Récup document principal
                                    Element document = rootElement.element("document");
                                    if (document != null && !document.getTextTrim().isEmpty()) {
                                        nomDocumentDossier = document.getText();
                                    } else {
                                        throw new DocumentException("XML_Dossier_Description.xml: document non specifie");
                                    }

                                    // Récup nomCircuit
                                    Element nomCircuit = rootElement.element("nom_circuit");
                                    if (nomCircuit != null && !nomCircuit.getTextTrim().isEmpty()) {
                                        circuitRef = findCircuitRefFromName(nomCircuit.getTextTrim());
                                        if (circuitRef == null) {
                                            throw new DocumentException("XML_Dossier_Description.xml: nom_circuit INCONNU dans iParapheur");
                                        }
                                    }
//                                    else {
//                                        throw new DocumentException("XML_Dossier_Description.xml: nom_circuit absent");
//                                    }

                                    // -------------------------------------
                                    // Proprietes par defaut
                                    properties.put(ParapheurModel.PROP_SIGNATURE_FORMAT, "PKCS#7/single");

                                    // On a un document, des propriétés, un circuit: on peut déjà créer le dossier
                                    dossierRef = dossierService.createDossier(parapheur, properties);
                                    dossierService.setDossierPropertiesWithAuditTrail(dossierRef, parapheur, properties, true);
                                    // Document
                                    FileFolderService ffs = fileFolderService;
                                    FileInfo fileInfo = ffs.create(dossierRef, nomDocumentDossier, ContentModel.TYPE_CONTENT);
                                    NodeRef docNodeRef = fileInfo.getNodeRef();
                                    ContentService cs = contentService;
                                    ContentWriter cw = cs.getWriter(docNodeRef, ContentModel.PROP_CONTENT, Boolean.TRUE);
                                    Part documentDossier = attachments.get(nomDocumentDossier);
                                    if (nomDocumentDossier.endsWith(".pdf") || nomDocumentDossier.endsWith(".PDF")) {
                                        cw.setMimetype(MimetypeMap.MIMETYPE_PDF);
                                    } else {
                                        cw.setMimetype(documentDossier.getContentType());
                                    }
                                    log.debug("Document " + nomDocumentDossier + " de type " + documentDossier.getContentType() + "=>" + cw.getMimetype());
                                    cw.setEncoding("UTF-8");
                                    cw.putContent(documentDossier.getInputStream());

                                    //  traiter PJ:  <!--OPTIONNEL <ph:PJs></ph:PJs> -->
                                    Element PJs = rootElement.element("PJs");
                                    if (PJs != null) {
                                        Iterator<Element> eltIterator = (Iterator<Element>) PJs.elementIterator();
                                        for (Iterator<Element> ie = eltIterator; ie.hasNext();) {
                                            Element pjDocElt = ie.next();
                                            if (pjDocElt != null && !pjDocElt.getTextTrim().isEmpty()) {
                                                // Add Pièce jointe
                                                String nomPJ = pjDocElt.getTextTrim();
                                                FileInfo fileInfo2 = ffs.create(dossierRef, nomPJ, ContentModel.TYPE_CONTENT);
                                                NodeRef docPJNodeRef = fileInfo2.getNodeRef();
                                                ContentWriter cw2 = cs.getWriter(docPJNodeRef, ContentModel.PROP_CONTENT, Boolean.TRUE);
                                                Part pjDoc = attachments.get(pjDocElt.getText());
                                                if (nomPJ.endsWith(".pdf") || nomPJ.endsWith(".PDF")) {
                                                    cw2.setMimetype(MimetypeMap.MIMETYPE_PDF);
                                                } else {
                                                    cw2.setMimetype(pjDoc.getContentType());
                                                }
                                                log.debug("DocumentPJ " + nomPJ + " de type " + pjDoc.getContentType());
                                                cw2.setEncoding("UTF-8");
                                                cw2.putContent(pjDoc.getInputStream());
                                            }
                                        }
                                    }

                                } else {
                                    throw new DocumentException("Doc XML XML_Dossier_Description.xml invalide");
                                }

                            } else {
                                throw new RuntimeException("Message invalide: XML_Dossier_Description.xml absent!");
                            }

                            // ajout Annotation publique, et le circuit.
                            // Circuit
                            // DEPRECATED: SavedWorkflow circuit = parapheurService.loadSavedWorkflow(circuitRef);
                            SavedWorkflow circuit = workflowService.getSavedWorkflow(circuitRef);
                            parapheurService.setCircuit(dossierRef, circuit.getCircuit());
                            // Annotations
                            parapheurService.setAnnotationPublique(dossierRef, annotations);

                            // Emission automatique du dossier=> visa de l'émetteur, et approbation
                            parapheurService.setSignataire(dossierRef, firstName + ((lastName != null && lastName.length() > 0) ? " " + lastName : ""));
                            parapheurService.approveV4(dossierRef, parapheurService.getCurrentParapheur());


                        } catch (Exception e) {

                            log.error(e.getMessage(), e);
                        } finally {
                            // For POP3, delete the message from the message store
                            // For IMAP, mark the message as SEEN, so that it stays in the message store, but we will not
                            // process it again
                            if (protocol.equals("pop3")) {
                                msgs[i].setFlag(Flags.Flag.DELETED, true);
                            } else if (protocol.equals("imap")) {
                                msgs[i].setFlag(Flags.Flag.SEEN, true);
                            }

                            savedMessagesCount++;
                        }
                    }
                }
                tx.commit();

            } catch (Exception e) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    log.error(e1.getMessage(), e1);
                }
                log.error(e.getMessage());
            } finally {
                log.debug("  " + protocol + "://" + emailUserName + "@" + mailServer + ":" + mailPort + ": " + savedMessagesCount + " message(s) traité(s).");
                try {
                    if (folder != null) {
                        folder.close(true);
                    }
                    if (store != null) {
                        store.close();
                    }
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            }

        }

    }

}
