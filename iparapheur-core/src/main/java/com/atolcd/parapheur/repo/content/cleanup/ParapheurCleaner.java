/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.repo.content.cleanup;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.transaction.UserTransaction;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.archive.NodeArchiveService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.CachingDateFormat;
import org.alfresco.web.bean.repository.Repository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.queryParser.QueryParser;

public class ParapheurCleaner {

    private static Log logger = LogFactory.getLog(ParapheurCleaner.class);

    private NodeArchiveService nodeArchiveService;

    private SearchService searchService;

    private TransactionService transactionService;

    private int protectedDays = 7;

    /**
     * Reference to the archive store which should be cleaned.
     */
    private StoreRef archiveStoreRef;

    /**
     * @param nodeArchiveService The nodeArchiveService to set.
     */
    public void setNodeArchiveService(NodeArchiveService nodeArchiveService) {
        this.nodeArchiveService = nodeArchiveService;
    }

    /**
     * @param searchService The searchService to set.
     */
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    /**
     * @param transactionService the transactionService to set
     */
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public StoreRef getArchiveStoreRef() {
        return archiveStoreRef;
    }

    public void setArchiveStoreRef(StoreRef archiveStoreRef) {
        this.archiveStoreRef = archiveStoreRef;
    }

    /**
     * @param protectedDays The protectedDays to set.
     */
    public void setProtectedDays(int protectedDays) {
        this.protectedDays = protectedDays;
        if (logger.isDebugEnabled()) {
            if (this.protectedDays > 0) {
                logger.debug("Protection des elements avant la purge fixee a : " + protectedDays + " jours");
            } else {
                logger.debug("Purge automatique desactivee");
            }
        }
    }

    public void execute() {
        if (this.protectedDays > 0) {
            Date fromDate = new Date(0);
            Date toDate = new Date(new Date().getTime() - (1000L * 60L * 60L * 24L * new Long(this.protectedDays)));

            if (toDate == null) {
                throw new RuntimeException("Erreur lors de la recuperation de la date limite");
            }

            SimpleDateFormat df = CachingDateFormat.getDateFormat();
            String strFromDate = QueryParser.escape(df.format(fromDate));
            String strToDate = QueryParser.escape(df.format(toDate));
            StringBuilder buf = new StringBuilder(128);
            buf.append("@")
                    .append(Repository.escapeQName(ContentModel.PROP_ARCHIVED_DATE))
                    .append(":[")
                    .append(strFromDate)
                    .append(" TO ")
                    .append(strToDate)
                    .append("] ");

            String query = buf.toString();

            SearchParameters sp = new SearchParameters();
            sp.setLanguage(SearchService.LANGUAGE_LUCENE);
            sp.setQuery(query);

            sp.addStore(archiveStoreRef);
            if (logger.isDebugEnabled()) {
                logger.debug("Purge declenchee a partir de la requete suivante:");
                logger.debug(query);
            }

            UserTransaction tx = null;
            ResultSet results = null;
            try {
                tx = this.transactionService.getNonPropagatingUserTransaction(false);
                tx.begin();

                results = this.searchService.query(sp);
                this.nodeArchiveService.purgeArchivedNodes(results.getNodeRefs());

                tx.commit();
            } catch (Throwable err) {
                err.printStackTrace();
                if (logger.isWarnEnabled()) {
                    logger.warn("Erreur lors de la purge des elements : " + err.getMessage());
                }
                try {
                    if (tx != null) {
                        tx.rollback();
                    }
                } catch (Exception tex) {
                }
            } finally {
                if (results != null) {
                    results.close();
                }
            }
        }
    }

}
