/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.repo.WorkflowService;
import com.atolcd.parapheur.repo.SavedWorkflow;
import java.util.List;
import org.alfresco.repo.admin.patch.AbstractPatch;


/**
 * WorkflowPermissionPatch
 * Fix permissions on workflows by resaving them.
 * @author eperalta
 */
public class WorkflowPermissionsPatch extends AbstractPatch {

    private WorkflowService workflowService;

    @Override
    protected String applyInternal() throws Exception {
        assert(workflowService != null);

        List<SavedWorkflow> workflows = workflowService.getWorkflows();

        if (workflows != null) {
            for (SavedWorkflow workflow : workflows) {
                System.out.println(workflow.getName());
                workflowService.saveWorkflow(workflow.getName(), workflow.getCircuit(), workflow.getAclParapheurs(), workflow.getAclGroupes(), workflow.isPublic());
            }
        }

        return "Permissions fixed";
    }

    public WorkflowService getWorkflowService() {
        return workflowService;
    }

    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

}
