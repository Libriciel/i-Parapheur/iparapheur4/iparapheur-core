/*
 * Version 2.1
 * CeCILL Copyright (c) 2008-2012, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 * 
 */
package com.atolcd.parapheur.repo;

import com.atolcd.parapheur.model.ParapheurModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.UserTransaction;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.util.Assert;

/**
 *
 * @author jmaire
 */
public class DelegationUpdaterJob implements Job {

    public static final Logger logger = Logger.getLogger(DelegationUpdaterJob.class);

    private ParapheurService parapheurService;
    private TransactionService transactionService;
    private NodeService nodeService;

    @Override
    public void execute(JobExecutionContext jec) {

        logger.info("Execute job update des délégations");
        JobDataMap dataMap = jec.getJobDetail().getJobDataMap();

        TenantAdminService tenantAdminService = (TenantAdminService) dataMap.get("tenantAdminService");
        TenantService tenantService = (TenantService) dataMap.get("tenantService");
        parapheurService = (ParapheurService) dataMap.get("parapheurService");
        transactionService = (TransactionService) dataMap.get("transactionService");
        nodeService = (NodeService) dataMap.get("nodeService");

        Assert.notNull(tenantAdminService, "tenantAdminService is mandatory");
        Assert.notNull(tenantService, "tenantService is mandatory");
        Assert.notNull(parapheurService, "parapheurService is mandatory");
        Assert.notNull(transactionService, "transactionService is mandatory");
        Assert.notNull(nodeService, "nodeService is mandatory");

        // Lists all users who should run the timer
        List<String> tenantsAdmins = new ArrayList<>();

        // Global admin, for the default tenant
        tenantsAdmins.add("admin");

        for (Tenant tenant : tenantAdminService.getAllTenants()) {
            // We don't need to run the timer for disabled tenants.
            if (tenant.isEnabled()) {
                String tenantAdmin = tenantService.getDomainUser("admin", tenant.getTenantDomain());
                tenantsAdmins.add(tenantAdmin);
            }
        }

        // Now do the job, as many times as we need, once as each tenant admin
        for (String admin : tenantsAdmins) {
            executeForTenant(admin);
        }
        
        logger.info("Execution Job update des délégations terminée");
    }
    
    /**
     * Traite les délégations des bureaux d'un tenant.<br>
     * <br>
     * On fait en sorte de traiter d'abord les bureaux ayant une délégation qui se termine puis ensuite les autres.<br>
     * Ceci permet d'éviter une erreur "CyclicChildRelationshipException" dans le cas où, le même jour, une délégation se termine et une nouvelle commence entre deux même bureaux.<br>
     * Le traitement ne s'interrompt pas si une erreur est relevée, on essaie de traiter tous les bureaux.
     * @param adminTenant Nom d'utilisateur administrateur du tenant
     */
    private void executeForTenant(String adminTenant) {
        logger.info("Tenant : " + adminTenant);
        AuthenticationUtil.runAs(() -> {
            // Chargement des bureaux du tenances
            List<NodeRef> parapheurs = parapheurService.getParapheurs();

            // S'il y en a
            if (parapheurs != null) {
                Date now = new Date();
                List<NodeRef> parapheurDelegationTerminee = new ArrayList<>();
                List<NodeRef> parapheurAutres = new ArrayList<>();

                // On tri les bureaux pour pouvoir traiter d'abord ceux qui ont une délégation à supprimer
                for (NodeRef parapheur : parapheurs) {

                    Date fin = (Date) nodeService.getProperty(parapheur, ParapheurModel.PROP_DATE_FIN_DELEGATION);

                    if ((fin != null) && now.after(fin)) {
                        parapheurDelegationTerminee.add(parapheur);
                    } else {
                        parapheurAutres.add(parapheur);
                    }
                }

                if (!parapheurDelegationTerminee.isEmpty()) {
                    for (NodeRef parapheur : parapheurDelegationTerminee) {
                        UserTransaction utx = transactionService.getUserTransaction();
                        try {
                            utx.begin();
                            parapheurService.updateDelegation(parapheur);
                            utx.commit();
                        } catch (Exception e) {
                            logger.error("Erreur lors de l'update delegation", e);
                            try {
                                utx.rollback();
                            } catch (Exception ex) {
                                logger.error("Erreur lors du rollback", ex);
                            }
                        }
                    }
                }

                if (!parapheurAutres.isEmpty()) {
                    for (NodeRef parapheur : parapheurAutres) {
                        UserTransaction utx = transactionService.getUserTransaction();
                        try {
                            utx.begin();
                            parapheurService.updateDelegation(parapheur);
                            utx.commit();
                        } catch (Exception e) {
                            logger.error("Error updating delegations", e);
                            try {
                                utx.rollback();
                            } catch (Exception ex) {
                                logger.error("Error durring rollback", ex);
                            }
                        }
                    }
                }
            }
            return null;
        }, adminTenant);
    }
}
