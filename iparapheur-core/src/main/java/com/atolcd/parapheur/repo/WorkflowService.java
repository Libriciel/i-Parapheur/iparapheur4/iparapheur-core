/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Initial Developpment by AtolCD, maintained by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.repo;

import java.util.List;
import java.util.Set;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * Gère les interactions avec les circuits i-parapheur: lecture, ecriture...
 * 
 * @author Vivien Barousse, Stephane VAST - ADULLACT Projet
 */
public interface WorkflowService {
    
    public final String GROUP_GESTIONNAIRE_CIRCUITS = "GROUP_GESTIONNAIRE_CIRCUITS_IPARAPHEUR";

    public void saveWorkflow(String name, 
            List<EtapeCircuit> circuit,
            Set<NodeRef> acl,
            Set<String> groupes,
            boolean publicCircuit);

    public SavedWorkflow getSavedWorkflow(NodeRef ref);

    /**
     * Charge un circuit de validation sauvegardé en entrepot.
     *
     * @param ref  le nodeRef du circuit de validation
     * @param resolveHierarchy  - false: garder "superieur hierarchique",
     *     true: trouver le parapheur correspondant.
     * @param emetteurRef  - le nodeRef de l'émetteur du dossier
     * @param dossier - le nodeRef du dossier
     * @return la définition du circuit de validation
     */
    public SavedWorkflow getSavedWorkflow(NodeRef ref, boolean resolveHierarchy, NodeRef emetteurRef, NodeRef dossier);

    public List<SavedWorkflow> getReadOnlyWorkflows();
    public List<SavedWorkflow> getEditableWorkflows();

    /**
     * Returns all available workflows for the current user
     *
     * @return
     */
    public List<SavedWorkflow> getWorkflows();

    /**
     * Returns all available workflwos for the given user
     * 
     * @param userName
     * @return
     */
    public List<SavedWorkflow> getWorkflows(String userName);

    /**
     * Returns a workflow reference by its name
     *
     * @param name workflow name
     * @return workflow ref
     */
    public NodeRef getWorkflowByName(String name);

    /**
     * Remove a workflow
     *
     * @param ref workflow ref
     */
    void removeWorkflow(NodeRef ref);

    /**
     * Search workflows with title
     * @param title title of workflows to search
     * @return list of workflows
     */
    List<NodeRef> searchWorkflows(String title);
    
    List<NodeRef> searchWorkflows(String title, int start, int maxItems);
    
    int getWorkflowsSize(String title);

    /**
     * Is the workflow editable or not
     * @param workflow
     * @return
     */
    boolean isWorkflowEditable(SavedWorkflow workflow);
}
