package com.atolcd.parapheur.repo.action.executer;

import com.atolcd.parapheur.repo.ParapheurUserPreferences;
import com.atolcd.parapheur.repo.impl.DigestQuartzJobServiceImpl;
import lombok.Setter;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.transaction.TransactionService;
import org.quartz.*;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 17/08/12
 * Time: 09:08
 * To change this template use File | Settings | File Templates.
 */
@Setter
public class RestartDailyDigestMailUserJob implements Job {

    private TenantService tenantService;
    private TenantAdminService tenantAdminService;
    private PersonService personService;
    private TransactionService transactionService;
    private ParapheurUserPreferences parapheurUserPreferences;
    private NodeService nodeService;

    private DigestQuartzJobServiceImpl digestQuartzJobService;


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        List<String> runAsUsers = new ArrayList<>();
        runAsUsers.add("admin");

        for (Tenant t : tenantAdminService.getAllTenants()) {
            // We don't need to (and we can't) run in disabled tenants.
            if (t.isEnabled()) {
                runAsUsers.add(tenantService.getDomainUser("admin", t.getTenantDomain()));
            }
        }

        for (String runAsUser : runAsUsers) {
            AuthenticationUtil.runAs(() -> {
                transactionService.getRetryingTransactionHelper().doInTransaction(() -> {
                    try {
                        Set<NodeRef> people = personService.getAllPeople();
                        List<String> users = new ArrayList<>();

                        /* collect users */

                        for (NodeRef person : people) {
                            String username = (String) nodeService.getProperty(person, ContentModel.PROP_USERNAME);
                            if (parapheurUserPreferences.isDailyDigestEnabledForUsername(username)) {
                                users.add(username);
                            }
                        }

                        /* Schedule Jobs */
                        digestQuartzJobService.scheduleCronJobForUsers(users);

                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    return null;
                });

                return null;
            }, runAsUser);
        }
    }
}

