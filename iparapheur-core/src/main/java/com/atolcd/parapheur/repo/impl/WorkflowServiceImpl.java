/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Initial Developpment by AtolCD, maintained by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.SavedWorkflow;
import com.atolcd.parapheur.repo.WorkflowService;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;

import com.atolcd.parapheur.web.bean.wizard.batch.CakeFilter;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.LimitBy;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.*;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 * Service qui gère les accès aux modèles de circuit de validation
 * 
 * @author Vivien Barousse - ADULLACT Projet
 * @author Stephane Vast - ADULLACT Projet
 */
public class WorkflowServiceImpl implements WorkflowService {

    public static final String WORKFLOWS_HOME_XPATH = "/app:company_home"
            + "/app:dictionary"
            + "/ph:savedworkflows";

    public static final String ALL_WORKFLOW_XPATH = WORKFLOWS_HOME_XPATH
            + "/*";

    private static Logger logger = Logger.getLogger(WorkflowService.class);

    private NodeService nodeService;

    private SearchService searchService;

    private NamespaceService namespaceService;

    private PermissionService permissionService;

    private AuthenticationService authenticationService;

    private AuthorityService authorityService;

    private ContentService contentService;

    private String store;

    @Override
    public void saveWorkflow(String name,
            List<EtapeCircuit> circuit,
            Set<NodeRef> acl,
            Set<String> aclGroups,
            boolean publicCircuit) {

        NodeRef workflowsHome = getWorkflowsHome();
        NodeRef workflowRef = nodeService.getChildByName(workflowsHome, ContentModel.ASSOC_CONTAINS, name);
        if (workflowRef == null) {
            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, name);
            workflowRef = nodeService.createNode(workflowsHome,
                    ContentModel.ASSOC_CONTAINS,
                    QName.createQName("cm", name, namespaceService),
                    ParapheurModel.TYPE_SAVED_WORKFLOW,
                    properties).getChildRef();
        }
        else {
            /* Si workflowRef != null alors on edite le circuit donc il faut
             supprimer les anciennes permissions avant d'en poser des
             nouvelles */
            permissionService.deletePermissions(workflowRef);

            /* Mise à jour du cm:name si necessaire */
            nodeService.setProperty(workflowRef, ContentModel.PROP_NAME, name);
        }

        if (!publicCircuit) {
            permissionService.setInheritParentPermissions(workflowRef, false);

            String user = authenticationService.getCurrentUserName();
            String userAuthority = authorityService.getName(AuthorityType.USER, user);
            Set<String> groups = authorityService.getContainingAuthorities(null, userAuthority, true);
            boolean gotGroup = false;
            for (String group : groups) {
                if (logger.isDebugEnabled()) {
                    logger.debug(" group = " + group);
                }
                if (authorityService.getContainingAuthorities(AuthorityType.GROUP, group, false).contains(GROUP_GESTIONNAIRE_CIRCUITS)) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("\tTrouvé groupe! = '" + group + "'.");
                    }
                    permissionService.setPermission(workflowRef, group, PermissionService.COORDINATOR, true);
                    if (logger.isDebugEnabled()) {
                        logger.debug(" set Përmissions.COORDINATOR OK");
                    }
                }
            }
            if (!gotGroup && groups.contains(GROUP_GESTIONNAIRE_CIRCUITS)) {
                // Alors on met au moins le groupe "parent"
                if (logger.isDebugEnabled()) {
                    logger.debug("\tPar defaut : " + GROUP_GESTIONNAIRE_CIRCUITS);
                }
                permissionService.setPermission(workflowRef, GROUP_GESTIONNAIRE_CIRCUITS, PermissionService.COORDINATOR, true);
            }
            /*
             * A minima, permettre aux autres gestionnaires de lire le circuit?
             */
            permissionService.setPermission(workflowRef, GROUP_GESTIONNAIRE_CIRCUITS, PermissionService.READ, true);
            if (logger.isDebugEnabled()) {
                logger.debug(" set Përmissions READ pour les gestionnaires de circuit OK");
            }

            for (String group : aclGroups) {
                group = (String) nodeService.getProperty(new NodeRef(group), ContentModel.PROP_AUTHORITY_NAME);
                permissionService.setPermission(workflowRef, group, PermissionService.CONSUMER, true);
            }
            for (NodeRef parapheurRef : acl) {
                String roleName = "ROLE_PHOWNER_" + parapheurRef.getId();
                permissionService.setPermission(workflowRef, roleName, PermissionService.CONSUMER, true);
            }

            Map<QName, Serializable> privProps = new HashMap<QName, Serializable>();
            privProps.put(ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS, (Serializable) acl);
            privProps.put(ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_GROUPS, (Serializable) aclGroups);
            nodeService.addAspect(workflowRef, ParapheurModel.ASPECT_PRIVATE_WORKFLOW, privProps);
        } else if (nodeService.hasAspect(workflowRef, ParapheurModel.ASPECT_PRIVATE_WORKFLOW)) {
            nodeService.removeAspect(workflowRef, ParapheurModel.ASPECT_PRIVATE_WORKFLOW);
        }

        /**
         * Constitution du circuit en XML
         */
        Document doc = DocumentHelper.createDocument();
        Element root = doc.addElement("circuit").addAttribute("version", "3.1");

        //  droits d'usage si !=public
        if (!publicCircuit) {
            if (acl != null) {
                Element aclElt = root.addElement("acl");
                for (NodeRef elt : acl) {
                    aclElt.addElement("parapheur").addText(elt.toString());
                }
            }
            if (aclGroups != null) {
                Element grpElt = root.addElement("groupes");
                for (String elt : aclGroups) {
                    if (!elt.equalsIgnoreCase("GESTIONNAIRE_CIRCUITS_IPARAPHEUR")) {
                        grpElt.addElement("groupe").addText(elt);
                    }
                }
            }
        }
        // Etapes du circuit
        Element etapes = root.addElement("etapes");
        for (EtapeCircuit etape : circuit) {
            Element etapeElt = etapes.addElement("etape");
            // Objet: parapheur, 'chef de'
            etapeElt.addElement("transition").addText(etape.getTransition());
            if (EtapeCircuit.TRANSITION_PARAPHEUR.equals(etape.getTransition())) {
                etapeElt.addElement("parapheur").addText(etape.getParapheur().toString());
            }
            // Action demandée
            etapeElt.addElement("action-demandee").addText(etape.getActionDemandee());
            // liste de notification !
            Element diffusionElt = etapeElt.addElement("diffusion");
            Set<NodeRef> notifs = etape.getListeNotification();
            if(notifs != null) {
                for (NodeRef notifie : notifs) {
                    diffusionElt.addElement("noderef").addText(notifie.toString());
                }
            }
            // Liste des métadonnées obligatoires
            Element metadatasElt = etapeElt.addElement("metadatas");
            Set<String> metas = etape.getListeMetadatas();
            if(metas != null) {
                for (String meta : metas) {
                    metadatasElt.addElement("id").addText(meta);
                }
            }

            // Liste des métadonnées obligatoires refus
            Element metadatasRefusElt = etapeElt.addElement("metadatas-refus");
            Set<String> metasRefus = etape.getListeMetadatasRefus();
            if(metasRefus != null) {
                for (String meta : metasRefus) {
                    metadatasRefusElt.addElement("id").addText(meta);
                }
            }
        }

        try {
            StringWriter out = new StringWriter(1024);
            XMLWriter writerTmp = new XMLWriter(OutputFormat.createPrettyPrint());
            writerTmp.setWriter(out);
            writerTmp.write(doc);
            String xmlData = out.toString();

            ContentWriter writer = contentService.getWriter(workflowRef, ContentModel.PROP_CONTENT, true);
            writer.setMimetype("text/xml");
            writer.setEncoding("UTF-8");
            writer.putContent(xmlData);
        } catch (IOException ex) {
            throw new RuntimeException("Unexpected IO Exception", ex);
        } catch (Exception e) {
            logger.error("Something bad happened: " + e.getLocalizedMessage());
        }
    }

    @Override
    public List<SavedWorkflow> getWorkflows() {
        List<NodeRef> workflowsRef = getWorkflowsRef();

        return getSavedWorkflows(workflowsRef);
    }

    @Override
    public List<SavedWorkflow> getWorkflows(String userName) {
        return AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<List<SavedWorkflow>>() {

            @Override
            public List<SavedWorkflow> doWork() throws Exception {
                return getWorkflows();
            }

        }, userName);
    }

    private List<NodeRef> getWorkflowsRef() {
        List<NodeRef> workflowsRef = new ArrayList<NodeRef>();
        for (NodeRef workflow : getAllWorkflowsRef()) {
            AccessStatus status = permissionService.hasPermission(workflow, PermissionService.READ);
            if (status.equals(AccessStatus.ALLOWED)) {
                workflowsRef.add(workflow);
            }
        }
        return workflowsRef;
    }

    @Override
    public List<SavedWorkflow> getReadOnlyWorkflows() {
        List<NodeRef> readOnlyWorkflowsRef = getReadOnlyWorkflowsRef();
        return getSavedWorkflows(readOnlyWorkflowsRef);
    }
    protected List<NodeRef> getReadOnlyWorkflowsRef() {
        List<NodeRef> readOnlyWorkflowsRef = new ArrayList<NodeRef>();
        for (NodeRef workflow : getAllWorkflowsRef()) {
            AccessStatus status = permissionService.hasPermission(workflow, PermissionService.READ);
            if (status.equals(AccessStatus.ALLOWED)) {
                AccessStatus writeStatus = permissionService.hasPermission(workflow, PermissionService.WRITE);
                if (!writeStatus.equals(AccessStatus.ALLOWED)) {
                    readOnlyWorkflowsRef.add(workflow);
                }
            }
        }
        return readOnlyWorkflowsRef;
    }

    @Override
    public List<SavedWorkflow> getEditableWorkflows() {
        List<NodeRef> editableWorkflowsRef = getEditableWorkflowsRef();

        return getSavedWorkflows(editableWorkflowsRef);
    }

    protected List<NodeRef> getEditableWorkflowsRef() {
        List<NodeRef> editableWorkflowsRef = new ArrayList<NodeRef>();
        for (NodeRef workflow : getAllWorkflowsRef()) {
            AccessStatus status = permissionService.hasPermission(workflow, PermissionService.WRITE);
            if (status.equals(AccessStatus.ALLOWED)) {
                editableWorkflowsRef.add(workflow);
            }
        }
        return editableWorkflowsRef;
    }

    protected List<SavedWorkflow> getSavedWorkflows(List<NodeRef> refs) {
        List<SavedWorkflow> savedWorkflows = new ArrayList<SavedWorkflow>();
        for (NodeRef ref : refs) {
            savedWorkflows.add(getSavedWorkflow(ref));
        }
        return savedWorkflows;
    }

    @Override
    public SavedWorkflow getSavedWorkflow(NodeRef ref) {
        return getSavedWorkflow(ref, false, null, null);
    }

    @Override
    public void removeWorkflow(NodeRef ref) {
        //On vérifie qu'il s'agit bien d'un circuit
        if(getSavedWorkflow(ref) != null) {
            nodeService.deleteNode(ref);
        }
    }

    @Override
    public List<NodeRef> searchWorkflows(String title) {

        String path = "PATH:\"/app:company_home/app:dictionary/ph:savedworkflows/*\"";

        if(title != null && !title.isEmpty() && !title.equals("*") && !title.equals("**")) {
            path += " AND =cm:name:" + CakeFilter.luceneEscapeString(title);
        }

        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        searchParameters.setQuery(path);
        /* permission check ? eager search */
        searchParameters.setMaxPermissionChecks(0);
//        searchParameters.addSort("@cm:name", true);

        ResultSet resultSet = null;
        List<NodeRef> refs = null;
        try {
            resultSet = searchService.query(searchParameters);
            refs = resultSet.getNodeRefs();
        } catch (IllegalArgumentException e) {
            //e.printStackTrace();
            refs = new ArrayList<NodeRef>();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }

        return refs;
    }
    
    @Override
    public List<NodeRef> searchWorkflows(String title, int start, int maxItems) {

        String path = "PATH:\"/app:company_home/app:dictionary/ph:savedworkflows/*\"";

        if(title != null && !title.isEmpty() && !title.equals("*") && !title.equals("**")) {
            path += " AND @"+CakeFilter.luceneEscapeString(QName.createQName("cm:name", namespaceService).toString())+":\"" + CakeFilter.luceneEscapeString(title)+"\"";
        }

        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        searchParameters.setQuery(path);
        
        /* permission check ? eager search */
        searchParameters.setMaxPermissionChecks(0);
        searchParameters.addSort("@cm:name", true);

        searchParameters.setSkipCount(start);
        searchParameters.setMaxItems(maxItems);
        searchParameters.setLimitBy(LimitBy.FINAL_SIZE);
        
        ResultSet resultSet = null;
        List<NodeRef> refs = null;
        try {
            resultSet = searchService.query(searchParameters);
            refs = resultSet.getNodeRefs();
        } catch (IllegalArgumentException e) {
            //e.printStackTrace();
            refs = new ArrayList<NodeRef>();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }

        return refs;
    }

    @Override
    public int getWorkflowsSize(String title){
        String path = "PATH:\"/app:company_home/app:dictionary/ph:savedworkflows/*\"";

        if(title != null && !title.isEmpty() && !title.equals("*") && !title.equals("**")) {
            path += " AND @"+CakeFilter.luceneEscapeString(QName.createQName("cm:name", namespaceService).toString())+":\"" + CakeFilter.luceneEscapeString(title)+"\"";
        }


        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        searchParameters.setQuery(path);
        /* permission check ? eager search */
        searchParameters.setMaxPermissionChecks(0);

        ResultSet resultSet = null;
        List<NodeRef> refs = null;
        try {
            return searchService.query(searchParameters).length();
        } catch (IllegalArgumentException e) {
            //e.printStackTrace();
            refs = new ArrayList<NodeRef>();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return refs.size();

    }
            
    @Override
    public boolean isWorkflowEditable(SavedWorkflow workflow) {
        AccessStatus status = permissionService.hasPermission(workflow.getNodeRef(), PermissionService.READ);
        if (status.equals(AccessStatus.ALLOWED)) {
            AccessStatus writeStatus = permissionService.hasPermission(workflow.getNodeRef(), PermissionService.WRITE);
            return writeStatus.equals(AccessStatus.ALLOWED);
        }
        return false;
    }

    @Override
    public SavedWorkflow getSavedWorkflow(NodeRef ref, boolean resolveHierarchy, NodeRef emetteurRef, NodeRef dossier) {
        SavedWorkflowImpl res = new SavedWorkflowImpl();
        res.setNodeRef(ref);
        res.setName((String) nodeService.getProperty(ref, ContentModel.PROP_NAME));
        ContentReader contentreader = contentService.getReader(ref, ContentModel.PROP_CONTENT);
        SAXReader saxreader = new SAXReader();

        res.setPublic(permissionService.getInheritParentPermissions(ref));

        try {
            if(contentreader != null) {
                Document document = saxreader.read(new StringReader(contentreader.getContentString()));
                Element rootElement = document.getRootElement();
                {
                    // Droits d'usage: acl + groupes
                    Element aclElt = rootElement.element("acl");
                    if (aclElt != null) {
                        for (Iterator i = aclElt.elementIterator("parapheur"); i.hasNext();) {
                            Element parapheurElt = (Element) i.next();
                            res.addToAclParapheurs(new NodeRef(parapheurElt.getText().trim()));
                        }
                    }
                    Element groupesElt = rootElement.element("groupes");
                    if (groupesElt != null) {
                        for (Iterator i = groupesElt.elementIterator("groupe"); i.hasNext();) {
                            Element groupeElt = (Element) i.next();
                            res.addToAclGroupes(groupeElt.getText().trim());
                        }
                    }
                    // Etapes du circuit de validation
                    Element etapesElt = rootElement.element("etapes");
                    if (etapesElt != null) {
                        NodeRef acteurPrecedent = emetteurRef;
                        int variableStep = 0;
                        for (Iterator i = etapesElt.elementIterator("etape"); i.hasNext();) {
                            Element etapeElt = (Element) i.next();
                            EtapeCircuitImpl eci = new EtapeCircuitImpl();
                            NodeRef acteur = null;
                            eci.setTransition(etapeElt.element("transition").getText().trim());
                            if (EtapeCircuit.TRANSITION_PARAPHEUR.equals(eci.getTransition())) {
                                acteur = new NodeRef(etapeElt.element("parapheur").getText().trim());
                                eci.setParapheurName((String)nodeService.getProperty(acteur, ContentModel.PROP_TITLE));
                                eci.setParapheur(acteur);
                            } else if (resolveHierarchy) {
                                if (EtapeCircuit.TRANSITION_CHEF_DE.equals(eci.getTransition())) {
                                    // trouver le parapheur correspondant.
                                    // Si N+1 n'a pas de responsable, N est assigné à l'étape.
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("transition chef de ");
                                    }
                                    List<AssociationRef> hierarchie = nodeService.getTargetAssocs(acteurPrecedent, ParapheurModel.ASSOC_HIERARCHIE);
                                    NodeRef responsable = null;
                                    if (hierarchie.size() == 1) {
                                        responsable = hierarchie.get(0).getTargetRef();
                                    }
                                    acteur = (responsable != null ? responsable : acteurPrecedent);
                                } else if (EtapeCircuit.TRANSITION_EMETTEUR.equals(eci.getTransition())) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("transition emetteur");
                                    }
                                    acteur = emetteurRef;
                                } else if (EtapeCircuit.TRANSITION_VARIABLE.equals(eci.getTransition())) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("transition variable");
                                    }
                                    if(dossier != null) {
                                        acteur = null;
                                        List<NodeRef> acteursVariables = (List<NodeRef>) nodeService.getProperty(dossier, ParapheurModel.PROP_ACTEURS_VARIABLES);
                                        if(acteursVariables != null && acteursVariables.size() > variableStep) {
                                            acteur = acteursVariables.get(variableStep);
                                            variableStep++;
                                        }
                                    } else {
                                        acteur = emetteurRef;
                                    }
                                    if(acteur == null) {
                                        throw new RuntimeException("Impossible de résoudre le circuit variable");
                                    }
                                }
                                eci.setParapheur(acteur);
                                eci.setParapheurName((String)nodeService.getProperty(acteur, ContentModel.PROP_TITLE));
                            }
                            acteurPrecedent = acteur;
                            eci.setActionDemandee(etapeElt.element("action-demandee").getText().trim());
                            // Liste de diffusion
                            Element diffElt = etapeElt.element("diffusion");
                            if (diffElt != null) {
                                Set<NodeRef> liste = new HashSet<NodeRef>();
                                for (Iterator j = diffElt.elementIterator("noderef"); j.hasNext();) {
                                    Element nodeElt = (Element) j.next();
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("####### notifie : " + nodeElt.getTextTrim());
                                    }
                                    if (emetteurRef!=null && "workspace://SpacesStore/_emetteur_".equals(nodeElt.getTextTrim())) {
                                        liste.add(emetteurRef);
                                    } else {
                                        liste.add(new NodeRef(nodeElt.getText().trim()));
                                    }
                                }
                                eci.setListeDiffusion(liste);
                            }

                            // Liste de diffusion
                            Element metaElt = etapeElt.element("metadatas");
                            Set<String> liste = new HashSet<String>();
                            if (metaElt != null) {
                                for (Iterator j = metaElt.elementIterator("id"); j.hasNext();) {
                                    Element nodeElt = (Element) j.next();
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("####### metadata : " + nodeElt.getTextTrim());
                                    }
                                    liste.add(nodeElt.getText().trim());
                                }
                            }

                            // Liste de diffusion
                            Element metaRefusElt = etapeElt.element("metadatas-refus");
                            Set<String> listeRefus = new HashSet<String>();
                            if (metaRefusElt != null) {
                                for (Iterator j = metaRefusElt.elementIterator("id"); j.hasNext();) {
                                    Element nodeElt = (Element) j.next();
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("####### metadata : " + nodeElt.getTextTrim());
                                    }
                                    listeRefus.add(nodeElt.getText().trim());
                                }
                            }

                            eci.setListeMetadatas(liste);
                            eci.setListeMetadatasRefus(listeRefus);

                            res.addToCircuit(eci);
                        }
                    }
                }
            }
        } catch (DocumentException e) {
            //TODO
        }

        return res;
    }

    @Override
    public NodeRef getWorkflowByName(String name) {
        if (logger.isDebugEnabled()) {
            logger.debug("début methode avec name=" + name);
        }

        //NodeRef workflowRef = nodeService.getChildByName(getWorkflowsHome(), ContentModel.ASSOC_CONTAINS, name);

        return nodeService.getChildByName(getWorkflowsHome(), ContentModel.ASSOC_CONTAINS, name);

        // FIXME: Peut-être pas la méthode la plus rapide...
/*
        for (NodeRef workflow : getAllWorkflowsRef()) {
            if (name.equals(nodeService.getProperty(workflow, ContentModel.PROP_NAME))) {
                return workflow;
            }
        }
        return null;
*/

    }

    protected List<NodeRef> getAllWorkflowsRef() {
        /*List<NodeRef> workflows = searchService.selectNodes(getRootNode(),
                ALL_WORKFLOW_XPATH,
                null,
                namespaceService,
                false);*/

        return searchService.selectNodes(getRootNode(),
                ALL_WORKFLOW_XPATH,
                null,
                namespaceService,
                false);
    }

    protected NodeRef getWorkflowsHome() {
        List<NodeRef> candidates = searchService.selectNodes(getRootNode(),
                WORKFLOWS_HOME_XPATH,
                null,
                namespaceService,
                false);

        if (candidates.size() == 1) {
            return candidates.get(0);
        }

        if (candidates.isEmpty()) {
            throw new IllegalStateException("Workflows home not found");
        } else {
            throw new IllegalStateException("Multiple workflows home found");
        }
    }

    private NodeRef getRootNode() {
        return nodeService.getRootNode(new StoreRef(store));
    }

    /*
     * Auto generated setters, used by Spring for dependency injection
     * 
     * <editor-fold defaultstate="collapsed" desc="Auto generated setters, used by Spring for dependency injection">
     */
    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setStore(String store) {
        this.store = store;
    }

    // </editor-fold>
}
