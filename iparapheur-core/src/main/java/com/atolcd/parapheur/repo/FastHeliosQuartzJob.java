/*
 * Version 3.3
 * CeCILL Copyright (c) 2006-2012, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo;

import com.atolcd.parapheur.repo.impl.FastServiceImpl;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * FastHeliosQuartzJob
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 30/03/12
 *         Time: 10:00
 */
public class FastHeliosQuartzJob implements Job {
    private static Log logger = LogFactory.getLog(S2lowHeliosQuartzJob.class);

    private TenantAdminService tenantAdminService;
    private TenantService tenantService;
    private FastServiceImpl fastService;
    private String adminName;

    protected void initializeWithContext(JobExecutionContext jobExecutionContext) {
        JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        setFastService((FastServiceImpl) dataMap.get("fastService"));
        setTenantService((TenantService) dataMap.get("tenantService"));
        setTenantAdminService((TenantAdminService) dataMap.get("tenantAdminService"));
        setAdminName((String) dataMap.get("adminName"));
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {



        initializeWithContext(jobExecutionContext);

        if (!fastService.isEnabled()) {
            return;
        }

        logger.debug("Running Job as " + adminName);

        //List<String> tenantsAdmins = new ArrayList<String>();

        // Global admin, for the default tenant
        /*tenantsAdmins.add("admin");

        for (Tenant tenant : tenantAdminService.getAllTenants()) {
            // We don't need to run the timer for disabled tenants.
            if (tenant.isEnabled()) {
                String tenantAdmin = tenantService.getDomainUser("admin", tenant.getTenantDomain());
                tenantsAdmins.add(tenantAdmin);
            }
        } */
       // String admin = tenantService.getDomainUser("admin", tenantDomain);
        // Now do the job, as many times as we need, once as each tenant admin
        //for (String admin : tenantsAdmins) {
            AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {

                /**
                 * Do the actual work.
                 *
                 * This method is invoked as tenant admin.
                 *
                 * We don't need any return value, but implementing RunAsWork
                 * forces to have one. This methods always returns null.
                 *
                 * @return null
                 */
                @Override
                public Object doWork() {
                    try {

                        fastService.runGetFastStatusJob();
                    } catch (Exception e) {
                        logger.error("Can't retrieve FAST status [" +
                                tenantService.getUserDomain(AuthenticationUtil.getRunAsUser()) +
                                "]." + e.getMessage());
                    }
                    return null;
                }

            }, adminName);
        //}



    }

    public void setTenantAdminService(TenantAdminService tenantAdminService) {
        this.tenantAdminService = tenantAdminService;
    }

    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    public void setFastService(FastServiceImpl fastService) {
        this.fastService = fastService;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }
}
