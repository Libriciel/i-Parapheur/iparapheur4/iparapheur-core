/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.repo;

import com.atolcd.parapheur.model.ParapheurException;
import com.atolcd.parapheur.repo.impl.S2lowServiceImpl;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.atolcd.parapheur.repo.impl.exceptions.UnknownMailsecTransactionException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.multipart.FilePart;

public interface S2lowService {
    /**
     * BLEX
     * "nom" du TDT S2LOW tel qu'il est stocke dans la propriete
     * ParapheurModel.PROP_TDT_NOM
     */
    public static String PROP_TDT_NOM_S2LOW = "S²LOW";

    /**
     * Retourne la disponibilité du service
     *
     * @return true si la configuration demande l'utilisation du S2lowService
     */
    public boolean isEnabled();


    /**
     * Retourne la disponibilité du service Mail Securisé
     * @return true si la configuration demande l'utilisation du MailSécurisé de S2low.
     */
    public boolean isMailServiceEnabled();

    public boolean isActesEnabled();

    public boolean isHeliosEnabled();

    /**
     * Retourne les propriétés nécessaires à la signature XAdES
     *
     * @param typeDossier le type de Dossier concerné
     * @return un objet Properties
     */
    public Properties getXadesSignatureProperties(String typeDossier);

    /**
     * Retourne les propriétés de signatures en fonction du tdt
     * déclaré dans la propriété TDT_NOM du dossier.
     * @param dossier
     * @return un objet Properties
     */
    public Properties getXadesSignaturePropertiesWithDossier(NodeRef dossier);

    /**
     * Retourne la liste des natures d'actes de s2low
     *
     * @return une map contenant les couples clé/valeur des natures d'actes
     */
    public Map<Integer, String> getS2lowActesNatures();

    /**
     * Retourne la liste des classifications d'actes de s2low
     *
     * @return une map contenant les couples clé/valeur des classifications d'actes
     */
    public Map<String, String> getS2lowActesClassifications();

    /**
     * Récupère la nouvelle classification Actes depuis S2low et l'enregistre dans
     * le parapheur.
     *
     * @return 0 si OK, 1 si connecteur desactivé, -1 si erreur.
     */
    public int updateS2lowActesClassifications() throws IOException;

    /**
     * A utiliser pour valider la connexion par certificat seul.
     * 
     * @return true si connexion réussie, false sinon.
     * @throws IOException si impossibilité d'initier le client HTTPS
     */
    public boolean isCertificateAbleToConnect(String protocole, String typeDossier) throws IOException;

    /**
     * Avec S2LOW 1.3, on peut faire une connexion certificat + login/pwd
     * Cette methode renvoie la liste des logins déclarés sur S2LOW par rapport
     * au certificat paramétré sur le connecteur et/ou type (si surcharge).
     * 
     * @param protocole ACTES, HELIOS, MAILSEC
     * @param typeDossier le type de dossier
     * @return la liste des logins, ou une liste vide si connexion par certificat uniquement
     * @throws IOException si probleme de connexion HTTPS
     */
    public List<String> getListLoginForType(String protocole, String typeDossier) throws IOException;

    /**
     * Teste la connection vers TDT, en fonction de la typologie en parametre.
     * (oui: à partir de v3.4, la config TDT peut etre surchargee sur le type)
     * 
     * @param typeDossier le type de dossier concerné. si null: par défaut.
     * @return true si connection OK, false sinon.
     * @throws IOException 
     */
    public boolean isConnectionOK(String protocole, String typeDossier) throws IOException;

    /**
     * Transmet le dossier passé en paramètre à la plate-forme S2Low-ACTES et
     * retourne le numéro de transaction créé en propriété du dossier
     *
     * @param dossier
     * @param nature
     * @param classification
     * @param numero
     * @param objet
     * @param date
     * @throws IOException si une erreur survient durant la connexion à la plate-forme
     */
    public void envoiS2lowActes(NodeRef dossier, String nature, String classification, String numero, String objet, String date)
            throws IOException, ParapheurException;

    /**
     * Transmet le dossier passé en paramètre à la plate-forme S2Low et retourne le numéro de transaction créé
     *
     * @param dossier
     * @return l'URL d'archive
     * @throws IOException si une erreur survient durant la connexion à la plate-forme
     */
    public String setS2lowActesArchiveURL(NodeRef dossier) throws IOException;

    /**
     * Demande le statut du dossier passé en paramètre à la plate-forme S2LOW
     *
     * @param dossier le dossier à vérifier
     * @return le code retour de la plate-forme S2LOW, à convertir avec statutS2lowToString
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier, st null ou n'a pas été envoyé à la plate-forme S2LOW
     * @throws IOException              si une erreur survient durant la connexion à la plate-forme
     */
    public int getInfosS2low(NodeRef dossier) throws IOException;

    /**
     * Convertit le code retour de Statut S2LOW en chaine de caractères explicite
     *
     * @param code le statut numerique à convertir
     * @return une chaîne de caractères contenant la réponse de la plate-forme S2LOW au format texte intelligible
     */
    public String statutS2lowToString(int code);

    /**
     * Retourne la référence du noeud contenant les classifications S2low pour MAJ
     *
     * @return un NodeRef correspondant au fichier XML des classifications S2low
     */
    public NodeRef getS2lowActesClassificationNodeRef();

    /**
     * Transmet le dossier passé en paramètre à la plate-forme S2Low-HELIOS et retourne le numéro de transaction créé
     *
     * @param dossier
     * @throws IOException si une erreur survient durant la connexion à la plate-forme
     */
    public void envoiS2lowHelios(NodeRef dossier) throws IOException;

    /**
     * Récupère la liste des PES_RETOUR
     *
     * @throws IOException si une erreur survient durant la connexion à la plate-forme
     */
    public void getS2lowHeliosListePES_Retour() throws IOException;

    /**
     * Redémarre les Jobs de mise à jour du status TdT pour tous les dossiers
     * en étape de télétransmission.
     */
    public void restartGetS2lowStatusJobs();

    public void restartGetS2lowStatusJob();

    public void getSecureMailInfos(NodeRef dossier);

    /**
     * Annule le job de mise à jour du status, et récupere l'état courant sur
     * la plateforme S2LOW-mailSécurisé
     * 
     * @param dossier le noderef du dossier concerné par ce job
     * @return les états de confirmation sur S2LOW
     * @throws IOException si une erreur survient durant la connexion à la plate-forme
     */
    public String cancelMailSecJob(NodeRef dossier) throws IOException;
    
    /**
     * Indique si le job de mise à jour du status peut être arrêté.
     * @param dossier
     * @return vrai si le job de mise à jour du status peut être arrêté
     */
    public boolean isMailSecJobCancelable(NodeRef dossier);

    public void restartGetMailsecS2lowStatusJobs();

    public String getSecureMailVersion() throws IOException;

    /**
     * Envoie le dossier <i>dossierRef</i> au MailSecurisé de S2low.
     * 
     * @param mailto  la liste des destinataires
     * @param mailcc la liste des destinataires en copie
     * @param mailcci la liste des destinataires en copie cachée
     * @param objet l'objet du mail
     * @param message le contenu du mail
     * @param password le mot de passe qui verouille l'accès au mail
     * @param send_password doit on envoyer le mot de passe ou non
     * @param dossierRef le dossier à envoyer
     * @param attachments liste des annexes du dossier
     * @param includeFirstPage Inclut ou non la page de bordereau
     * @return un entier l'Id-transaction du mail Securisé créé.
     * @throws IOException
     */
    public int sendSecureMail(List<String> mailto, List<String> mailcc, List<String> mailcci,
                              String objet, String message, String password,
                              boolean send_password, NodeRef dossierRef,
                              List<NodeRef> attachments, boolean includeFirstPage) throws IOException, Exception;

    /**
     * Envoie un mail securisé avec une Liste de fichiers.
     * @param mailto la liste des destinataires
     * @param mailcc la liste des destinataires en copie
     * @param mailcci la liste des destinataires en copie cachée
     * @param objet l'objet du mail
     * @param message message le contenu du mai
     * @param password le mot de passe qui verouille l'accès au mail
     * @param send_password doit on envoyer le mot de passe ou non
     * @param files une liste de fichiers à transmettre
     * @return un entier, l'id du mail sécursisé crééé.
     * @throws IOException
     */
    public int sendSecureMail(List<String> mailto, List<String> mailcc, List<String> mailcci,
                              String objet, String message, String password,
                              boolean send_password, List<FilePart> files) throws IOException;

    public int sendSecureMail(List<String> mailto, String objet, String message) throws IOException;

    /**
     * Récupère le nombre de mails sécurisés actuellement stockés dans S2Low
     * 
     * @return le nombre de mails
     * @throws IOException
     */
    public int getSecureMailCount() throws IOException;

    /**
     * Récupère la liste des mails-sécurisés avec details
     * 
     * @param limit
     * @param offset
     * @return une map (id, details) des mails s2low
     * @throws IOException
     */
    public Map<Integer, S2lowServiceImpl.SecureMailDetail> getSecureMailList(int limit, int offset) throws IOException;

    /**
     * Récupère les details d'un mail
     * 
     * @param id l'id du mail à recuperer
     * @return une instance de SecureMailDetail qui représente le detail du mail
     * @throws IOException
     */
    public S2lowServiceImpl.SecureMailDetail getSecureMailDetail(int id) throws IOException, UnknownMailsecTransactionException;

    /**
     * Suppression d'un mail de la plateforme.
     * 
     * @param id  Identifiant de la transaction sur S2LOW
     * @return true si la suppression s'est bien passée
     * @throws IOException
     */
    public boolean deleteSecureMail(Integer id) throws IOException;

    /**
     * récupère le template de mail.
     * 
     * @param dossier
     * @return
     */
    public String getSecureMailTemplate(NodeRef dossier);

    /**
     * Instancie le modele avec les informations venant du dossier et de l'utilisateur courrant
     * @param dossier
     * @param template
     * @return le message
     */
    public String getSecureMailMessageWithTemplate(NodeRef dossier, String template);

    Properties getPadesSignatureProperties(String type);

    Properties getCustomXadesSignatureProperties(String type);

    Properties getPadesSignaturePropertiesWithDossier(NodeRef dossier);

    Properties getCustomXadesSignaturePropertiesWithDossier(NodeRef dossier);

    HttpClient createConnexionHTTPS(Map<String,String> properties, InputStream is);

    List<String> isCertificateOk(Map<String, String> properties, InputStream is);

    boolean isConnexionOK(Map<String,String> properties, InputStream is);
}
