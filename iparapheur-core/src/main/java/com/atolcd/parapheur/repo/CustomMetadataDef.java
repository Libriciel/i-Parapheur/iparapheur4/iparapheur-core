/*
 * Version 3.2
 * CeCILL Copyright (c) 2010-2013, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed and maintained by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.faces.model.SelectItem;
import org.alfresco.service.namespace.QName;

/**
 *
 * @author Vivien Barousse - ADULLACT Projet
 */
public class CustomMetadataDef {

    private QName name;
    private String title;
    private CustomMetadataType type;
    private List<String> values;
    private List<SelectItem> valuesList;
    private List<String> referencedBy;
    private boolean isDeletable;
    private boolean isAlphaOrdered;

    public CustomMetadataDef(QName name, String title, CustomMetadataType type, boolean isDeletable) {
        this.name = name;
        this.title = title;
        this.type = type;
        this.values = null;
        this.valuesList = new ArrayList<SelectItem>();
        this.isDeletable = isDeletable;
        this.isAlphaOrdered = true;
    }

    public CustomMetadataDef(QName name, String title, List<String> values, CustomMetadataType type, boolean isDeletable) {
        this.name = name;
        this.title = title;
        this.type = type;
        this.isAlphaOrdered = true;
        this.setEnumValues(values);
        this.isDeletable = isDeletable;
    }

    public CustomMetadataDef(QName name, String title, List<String> values, CustomMetadataType type, boolean isDeletable, boolean isAlphaOrdered) {
        this.name = name;
        this.title = title;
        this.type = type;
        this.isAlphaOrdered = isAlphaOrdered;
        this.setEnumValues(values);
        this.isDeletable = isDeletable;
    }

    public QName getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CustomMetadataType getType() {
        return type;
    }
    
    public void setEnumValues(List<String> values) {
        this.values = values;

        if(this.isAlphaOrdered) {
            Collections.sort(values);
        }
        
        this.valuesList = new ArrayList<SelectItem>(this.values.size());
        for (String value : values) {
            this.valuesList.add(new SelectItem(value, value));
        }
    }
    
    public List<String> getEnumValues() {
        return this.values;
    }
    
    public List<SelectItem> getEnumValuesList() {
        return this.valuesList;
    }

    public List<String> getReferencedBy() {
        return referencedBy;
    }

    public void setReferencedBy(List<String> referencedBy) {
        this.referencedBy = referencedBy;
    }

    public boolean isDeletable() {
        return isDeletable;
    }

    public boolean isAlphaOrdered() {
        return isAlphaOrdered;
    }

}
