/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.repo.security.permissions.dynamic;

import org.alfresco.repo.security.permissions.DynamicAuthority;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import java.util.Set;
import org.alfresco.repo.security.permissions.PermissionReference;

public class InitiateurDynamicAuthority implements InitializingBean, DynamicAuthority
{
	private ParapheurService parapheurService;
    private NodeService nodeService; 

   public ParapheurService getParapheurService()
   {
       return parapheurService;
   }
   public void setParapheurService(ParapheurService parapheurService)
   {
       this.parapheurService = parapheurService;
   }
   
   /**
    * @return Returns the nodeService.
    */
   public NodeService getNodeService()
   {
      return nodeService;
   }
   /**
    * @param nodeService The nodeService to set.
    */
   public void setNodeService(NodeService nodeService)
   {
      this.nodeService = nodeService;
   }
   
   public void afterPropertiesSet() throws Exception
   {
       Assert.notNull(parapheurService, "Il doit y avoir un service parapheur");
   }

   /**
    * 
    * @see org.alfresco.repo.security.permissions.DynamicAuthority#hasAuthority(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
    */
   public boolean hasAuthority(NodeRef nodeRef, String userName)
   {
      try
      {
         // Le rôle n'est donné que sur un dossier en cours de constitution, au propriétaire du parapheur
         // ou à la secrétaire si le dossier possède l'aspect secrétariat
         NodeRef dossier = parapheurService.getParentDossier(nodeRef);
         NodeRef parapheur = this.parapheurService.getEmetteur(dossier);
         
         if (!parapheurService.isEmis(dossier))
         {
            if (nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT)
                  && parapheurService.isParapheurSecretaire(parapheur, userName))
               return true;
            if (!nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT)
                  && parapheurService.isParapheurOwner(parapheur, userName))
               return true;
         }
         
         return false;
      }
      catch (IllegalArgumentException iae)
      {
         return false;
      }
   }

   public String getAuthority()
   {
       return "ROLE_PARAPHEUR_INITIATEUR";
   }

    public Set<PermissionReference> requiredFor() {
        return null;
    }
}
