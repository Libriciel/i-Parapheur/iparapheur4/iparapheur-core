/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.repo.TypesService;
import lombok.extern.log4j.Log4j;
import org.adullact.utils.StringUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.admin.patch.AbstractPatch;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Properties;

import static com.atolcd.parapheur.repo.S2lowService.PROP_TDT_NOM_S2LOW;

/**
 * Utilisé pour définir une propriété à tous les dossiers
 * Définir d'autre constructeurs afin de gérer d'autres types de propriétés
 * @author Lukas Hameury <lukas.hameury@adullact-projet.coop>
 */
@Log4j
@Component
public class AddNewPropertiesToTypesPatch extends AbstractPatch {

    @Autowired
    @Qualifier("s2lowServiceDecorator")
    private S2lowService s2lowService;

    @Autowired
    private ParapheurService parapheurService;

    @Autowired
    private TypesService typesService;

    @Autowired
    private ServiceRegistry serviceRegistry;

    private NodeRef getDefaultConfigNode() {
        List<NodeRef> nodes;
        String query = "/app:company_home/app:dictionary/ph:certificats/cm:s2low_properties.xml";
        nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                query,
                null,
                namespaceService,
                false);

        if (nodes.size() != 1) {
            throw new RuntimeException("Can't find S2low configuration file");
        }
        return nodes.get(0);
    }

    private NodeRef copyDefaultConfigTo(String type) {
        // trouver la source
        NodeRef sourceRef = this.getDefaultConfigNode();
        String typeQueryPrefix = "/app:company_home/app:dictionary/cm:metiertype";
        // trouver le répertoire cible   "/app:company_home/app:dictionary/cm:metiertype"
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                typeQueryPrefix,
                null,
                namespaceService,
                false);
        if (nodes.size() != 1) {
            throw new RuntimeException("Can't find typemetier target folder for s2low");
        }
        NodeRef targetParentRef = nodes.get(0);
        // Copier !
        try {
            final boolean[] fileExists = {false};
            final NodeRef[] filenode = {null};

            // Ne pas encoder ici (sinon double encodage de l'espace..  -> _x0020_ -> _x005f_x0020_)
            serviceRegistry.getFileFolderService().listFiles(targetParentRef).forEach((fileInfo -> {
                // Rename if bad case
                if(fileInfo.getName().equalsIgnoreCase(type + "_s2low_properties.xml") && !fileInfo.getName().equals(type + "_s2low_properties.xml")) {
                    try {
                        serviceRegistry.getFileFolderService().rename(fileInfo.getNodeRef(), type + "_s2low_properties.xml");
                        fileExists[0] = true;
                        filenode[0] = fileInfo.getNodeRef();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }));
            if(!fileExists[0]) {
                return serviceRegistry.getFileFolderService().copy(sourceRef, targetParentRef,
                        type + "_s2low_properties.xml").getNodeRef();
            } else {
                return filenode[0];
            }
        } catch (FileExistsException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException("Can't create config file for type=" + type);
        } catch (FileNotFoundException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException("Can't find target folder config file for type=" + type);
        }
    }

    protected NodeRef getConfigNode(String type) {

        if (type == null) {
            return getDefaultConfigNode();
        }
        String typeQueryPrefix = "/app:company_home/app:dictionary/cm:metiertype";

        // find it !
        String query = typeQueryPrefix + "/cm:" + org.alfresco.webservice.util.ISO9075.encode(type) + "_s2low_properties.xml";
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                query,
                null,
                namespaceService,
                false);
        if (nodes.size() != 1) {
            return moveNodeIfExistsInLowercaseOrGetDefault(type, typeQueryPrefix);
        }
        else {
            return nodes.get(0);
        }
    }

    private NodeRef moveNodeIfExistsInLowercaseOrGetDefault(String type, String typeQueryPrefix) {
        String query = typeQueryPrefix + "/cm:" + org.alfresco.webservice.util.ISO9075.encode(type).toLowerCase() + "_s2low_properties.xml";
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                query,
                null,
                namespaceService,
                false);
        // trouver le répertoire cible   "/app:company_home/app:dictionary/cm:metiertype"
        List<NodeRef> typenode = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                typeQueryPrefix,
                null,
                namespaceService,
                false);
        if (typenode.size() != 1) {
            throw new RuntimeException("Can't find typemetier target folder for s2low");
        }
        NodeRef targetParentRef = typenode.get(0);

        if (nodes.size() != 1) {
            // on considère qu'il n'existe pas encore. Alors on fait une copie de la source
            return copyDefaultConfigTo(type);
        } else {
            NodeRef result;
            // Move file to real position :
            try {
                result = serviceRegistry.getFileFolderService().move(nodes.get(0), targetParentRef, org.alfresco.webservice.util.ISO9075.encode(type) + "_s2low_properties.xml").getNodeRef();
            } catch (FileNotFoundException e) {
                throw new RuntimeException("Can't move file to default location");
            }
            return result;
        }
    }

    private void verifySignatureLocationElement(List<Element> types) {
        if(types.get(0).element(TypesService.TYPE_KEY_SIGNATURE_LOCATION) == null) {
            Element metierTypes = DocumentHelper.createElement("MetierTypes");
            Document doc = DocumentHelper.createDocument(metierTypes);

            // Get default parameter
            Properties defaultProp = s2lowService.getXadesSignatureProperties(null);

            for (Element type : types) {
                getConfigNode(type.element("ID").getTextTrim());
                if (StringUtils.startsWithIgnoreCase(type.element(TypesService.TYPE_KEY_SIGNATURE_FORMAT).getTextTrim(), "pades")) {
                    Properties properties = s2lowService.getPadesSignatureProperties(type.element("ID").getTextTrim());
                    Element signatureLocation = DocumentHelper.createElement(TypesService.TYPE_KEY_SIGNATURE_LOCATION);
                    signatureLocation.setText(properties.getProperty("pCity"));

                    Element signaturePostalCode = DocumentHelper.createElement(TypesService.TYPE_KEY_SIGNATURE_POSTALCODE);
                    signaturePostalCode.setText(defaultProp.getProperty("pPostalCode"));

                    type.add(signatureLocation);
                    type.add(signaturePostalCode);

                    metierTypes.add(type.createCopy());
                } else if (StringUtils.startsWithIgnoreCase(type.element(TypesService.TYPE_KEY_SIGNATURE_FORMAT).getTextTrim(), "xades")
                        || type.element(TypesService.TYPE_KEY_SIGNATURE_FORMAT).getTextTrim().equalsIgnoreCase("auto")) {
                    Properties properties;
                    if (type.element(TypesService.TYPE_KEY_TDT).element(TypesService.TYPE_KEY_PROTOCOLE).getTextTrim().equals("HELIOS")
                            && type.element(TypesService.TYPE_KEY_TDT).element("Nom").getTextTrim().equals(PROP_TDT_NOM_S2LOW)) {
                        properties = s2lowService.getXadesSignatureProperties(type.element("ID").getTextTrim());
                    } else {
                        properties = s2lowService.getCustomXadesSignatureProperties(type.element("ID").getTextTrim());
                    }

                    Element signatureLocation = DocumentHelper.createElement(TypesService.TYPE_KEY_SIGNATURE_LOCATION);
                    signatureLocation.setText(properties.getProperty("pCity"));

                    Element signaturePostalCode = DocumentHelper.createElement(TypesService.TYPE_KEY_SIGNATURE_POSTALCODE);
                    signaturePostalCode.setText(properties.getProperty("pPostalCode"));

                    type.add(signatureLocation);
                    type.add(signaturePostalCode);

                    metierTypes.add(type.createCopy());
                } else {
                    Element signatureLocation = DocumentHelper.createElement(TypesService.TYPE_KEY_SIGNATURE_LOCATION);
                    signatureLocation.setText(defaultProp.getProperty("pCity"));

                    Element signaturePostalCode = DocumentHelper.createElement(TypesService.TYPE_KEY_SIGNATURE_POSTALCODE);
                    signaturePostalCode.setText(defaultProp.getProperty("pPostalCode"));

                    type.add(signatureLocation);
                    type.add(signaturePostalCode);

                    metierTypes.add(type.createCopy());
                }
            }
            NodeRef node = parapheurService.getSavedXLMTypesNode();
            serviceRegistry.getContentService().getWriter(node, ContentModel.PROP_CONTENT, true).putContent(doc.asXML());
        }
    }

    @Override
    protected String applyInternal() throws Exception {
        log.warn("Patch types with new properties sigLocation and sigPostalCode");

        List<Element> types = typesService.getTypesElements();
        if(types != null && !types.isEmpty()) {
            verifySignatureLocationElement(types);
        }
        return "Fixes Types for 4.7.0";
    }
}
