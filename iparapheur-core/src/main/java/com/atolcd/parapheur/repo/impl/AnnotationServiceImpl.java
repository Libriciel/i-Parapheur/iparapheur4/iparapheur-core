package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.AnnotationService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.annotations.Annotation;
import com.atolcd.parapheur.repo.annotations.Rect;
import com.atolcd.parapheur.repo.annotations.impl.AnnotationImpl;
import org.adullact.iparapheur.util.CollectionsUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.Serializable;
import java.util.*;

/**
 * AnnotationServiceImpl
 *
 * @author Emmanuel Peralta
 */
public class AnnotationServiceImpl implements AnnotationService {

    public static final Logger logger = Logger.getLogger(AnnotationServiceImpl.class);

    @Autowired
    private ParapheurService parapheurService;

    @Qualifier("nodeService")
    @Autowired
    private NodeService nodeService;

    @Override
    public @Nullable UUID addAnnotation(@NotNull NodeRef dossier, @NotNull NodeRef document, @NotNull Annotation annotation) {

        //
        //Vérif droit d'ajout------
        String username = AuthenticationUtil.getRunAsUser();
        NodeRef parapheur = parapheurService.getParentParapheur(dossier);
        boolean owner = parapheurService.isParapheurOwnerOrDelegate(parapheur, username);
        boolean secretaire = parapheurService.isParapheurSecretaire(parapheur, username);
        boolean hasAspectSec = nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT);
        String creator = (String) nodeService.getProperty(dossier, ContentModel.PROP_CREATOR);
        NodeRef currentCorbeille = parapheurService.getParentCorbeille(dossier);

        if ((owner || (secretaire && hasAspectSec) || (creator.equals(username) && ParapheurModel.NAME_EN_PREPARATION.equals(nodeService.getPrimaryParent(currentCorbeille).getQName())))) {

            //CAN ADD
            EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(dossier);

            //Si l'utilisateur n'est pas le propriétaire du parapheur, on considère qu'il s'agit du secretaire
            if (!annotation.isSecretaire()) {
                annotation.setSecretaire(!owner);
            }

            addAnnotationForEtapeAndDocument(etapeCourante.getNodeRef(), document, annotation);
            return annotation.getUUID();
        }

        return null;
    }

    @Override
    public void updateAnnotation(@NotNull NodeRef dossier, @NotNull NodeRef document, @NotNull Annotation a) {

        Annotation annotation = getAnnotationForUUID(dossier, a.getUUID());

        if ((annotation != null) && (canModifyAnnotations(dossier, a))) {
            annotation.updateDate();
            removeAnnotation(dossier, document, a.getUUID());
            addAnnotation(dossier, document, a);
        }
    }

    @Override
    public void removeAnnotation(@NotNull NodeRef dossier, @NotNull NodeRef document, @NotNull UUID annotationUUID) {

        EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(dossier);
        HashMap<DocumentPage, ArrayList<Annotation>> annotationsMap = getAnnotationsForEtape(etapeCourante.getNodeRef(), document);

        // Remove annotation in map

        boolean isModified = false;

        for (Map.Entry<DocumentPage, ArrayList<Annotation>> annotationEntry : annotationsMap.entrySet()) {
            ArrayList<Annotation> annotations = annotationEntry.getValue();
            if (annotations != null) {
                Iterator<Annotation> it = annotations.iterator();
                while (it.hasNext()) {
                    Annotation annotation = it.next();
                    if (annotation.getUUID().equals(annotationUUID) && (canModifyAnnotations(dossier, annotation))) {
                        if (canModifyAnnotations(dossier, annotation)) {
                            it.remove();
                            isModified = true;
                        }
                        break;
                    }
                }
            }
        }

        // Save modified annotations

        if (isModified) {
            setAnnotationsForEtapeAndDocument(etapeCourante.getNodeRef(), document, annotationsMap);
        }
    }

    @Override
    public void removeAllCurrentAnnotations(@NotNull NodeRef dossier) {
        EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(dossier);

        if (etapeCourante != null) {
            for (NodeRef document : parapheurService.getMainDocuments(dossier)) {
                setAnnotationsForEtapeAndDocument(etapeCourante.getNodeRef(), document, null);
            }
        }
    }

    @Override
    public @Nullable Annotation getAnnotationForUUID(@NotNull NodeRef dossier, @NotNull UUID uuid) {

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);
        for (EtapeCircuit etapeCircuit : circuit) {

            HashMap<DocumentPage, ArrayList<Annotation>> etapeCircuitAnnotations = getAnnotationsForEtape(etapeCircuit.getNodeRef(), dossier);
            for (ArrayList<Annotation> etapeCircuitPageAnnotations : etapeCircuitAnnotations.values()) {

                for (Annotation etapeCircuitPageAnnotation : etapeCircuitPageAnnotations) {
                    if (etapeCircuitPageAnnotation.getUUID().equals(uuid)) {
                        return etapeCircuitPageAnnotation;
                    }
                }
            }
        }

        return null;
    }

    @Override
    public Annotation getNewTextAnnotationWith(Rect r, String picto, String text, Integer page, String fullname) {
        Annotation retVal = new AnnotationImpl(AnnotationService.TYPE_TEXT_ANNOT, page, fullname);
        retVal.setRect(r);
        retVal.setPicto(picto);
        retVal.setText(text);
        return retVal;
    }

    @Override
    public Annotation getNewRectAnnotationWith(Rect r, String penColor, String fillColor, String text, Integer page, String fullname) {
        Annotation retVal = new AnnotationImpl(AnnotationService.TYPE_RECT, page, fullname);
        retVal.setRect(r);
        retVal.setPenColor(penColor);
        retVal.setFillColor(fillColor);
        retVal.setText(text);
        return retVal;
    }

    @Override
    public Annotation getNewHighlightAnnotationWith(Rect r, String penColor, String fillColor, String text, Integer page, String fullname) {
        Annotation retVal = new AnnotationImpl(AnnotationService.TYPE_HIGHLIGHT, page, fullname);
        retVal.setRect(r);
        retVal.setPenColor(penColor);
        retVal.setFillColor(fillColor);
        retVal.setText(text);
        return retVal;
    }

    @Override
    public @NotNull HashMap<Integer, ArrayList<Annotation>> getAllDocumentAnnotations(@NotNull NodeRef dossier, @NotNull final NodeRef document) {

        HashMap<Integer, ArrayList<Annotation>> retVal = new HashMap<Integer, ArrayList<Annotation>>();
        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);

        for (EtapeCircuit etape : circuit) {
            HashMap<DocumentPage, ArrayList<Annotation>> etapeAnnotations = getAnnotationsForEtapeAndDocument(etape.getNodeRef(), document);

            for (Map.Entry<DocumentPage, ArrayList<Annotation>> etapeAnnotationEntry : etapeAnnotations.entrySet()) {

                ArrayList<Annotation> annotations = retVal.get(etapeAnnotationEntry.getKey().getPage());
                if (annotations == null) {
                    annotations = new ArrayList<Annotation>();
                }

                annotations.addAll(etapeAnnotationEntry.getValue());
                retVal.put(etapeAnnotationEntry.getKey().getPage(), annotations);
            }
        }

        return retVal;
    }

    @Override
    public @NotNull List<HashMap<Integer, ArrayList<Annotation>>> getDocumentAnnotationsByEtape(@NotNull NodeRef dossier, @NotNull final NodeRef document) {

        List<HashMap<Integer, ArrayList<Annotation>>> retVal = new ArrayList<HashMap<Integer, ArrayList<Annotation>>>();
        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);

        for (EtapeCircuit etape : circuit) {
            HashMap<DocumentPage, ArrayList<Annotation>> etapeAnnotations = getAnnotationsForEtapeAndDocument(etape.getNodeRef(), document);
            HashMap<Integer, ArrayList<Annotation>> resultEtapeMap = new HashMap<Integer, ArrayList<Annotation>>();

            for (Map.Entry<DocumentPage, ArrayList<Annotation>> etapeAnnotationEntry : etapeAnnotations.entrySet()) {
                resultEtapeMap.put(etapeAnnotationEntry.getKey().getPage(), etapeAnnotationEntry.getValue());
            }

            retVal.add(resultEtapeMap);
        }

        return retVal;
    }

    @Override
    public @NotNull List<HashMap<String, HashMap<Integer, ArrayList<Annotation>>>> getAnnotationsByEtape(@NotNull NodeRef dossier) {

        List<HashMap<String, HashMap<Integer, ArrayList<Annotation>>>> result = new ArrayList<HashMap<String, HashMap<Integer, ArrayList<Annotation>>>>();
        List<NodeRef> documents = parapheurService.getMainDocuments(dossier);

        for (NodeRef document : documents) {
            List<HashMap<Integer, ArrayList<Annotation>>> documentAnnotations = getDocumentAnnotationsByEtape(dossier, document);

            // Init result object

            if (result.isEmpty()) {
                for (int i=0; i<documentAnnotations.size(); i++) {
                    result.add(new HashMap<String, HashMap<Integer, ArrayList<Annotation>>>());
                }
            }

            // Map results

            for (int i=0; i<documentAnnotations.size(); i++) {
                result.get(i).put(document.getId(), documentAnnotations.get(i));
            }
        }

        return result;
    }

    private @NotNull HashMap<DocumentPage, ArrayList<Annotation>> getAnnotationsForEtapeAndDocument(@NotNull NodeRef etape, @NotNull final NodeRef document) {

        HashMap<DocumentPage, ArrayList<Annotation>> result = getAnnotationsForEtape(etape, document);

        // Filter by Document

        CollectionsUtils.filter(result, new Predicate() {
            @SuppressWarnings("unchecked")
            @Override
            public boolean evaluate(Object object) {
                try {
                    return ((Map.Entry<DocumentPage, ArrayList<Annotation>>) object).getKey().getDocumentId().contentEquals(document.toString());
                } catch (ClassCastException cce) {
                    return false;
                }
            }
        });

        return result;
    }

    @SuppressWarnings("unchecked")
    private @NotNull HashMap<DocumentPage, ArrayList<Annotation>> getAnnotationsForEtape(@NotNull NodeRef etape, @NotNull NodeRef document) {

        HashMap<DocumentPage, ArrayList<Annotation>> result = (HashMap<DocumentPage, ArrayList<Annotation>>) nodeService.getProperty(etape, ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES_MULTIDOC);

        // Fetch legacy annotations

        if (result == null) {

            HashMap<Integer, ArrayList<Annotation>> annotationsSingleDoc = (HashMap<Integer, ArrayList<Annotation>>) nodeService.getProperty(etape, ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES);

            // Set new Annotations model

            if (annotationsSingleDoc != null) {

                HashMap<DocumentPage, ArrayList<Annotation>> annotationsMultiDoc = new HashMap<DocumentPage, ArrayList<Annotation>>();
                for (Map.Entry<Integer, ArrayList<Annotation>> annotationSingleDocEntry : annotationsSingleDoc.entrySet()) {
                    annotationsMultiDoc.put(new DocumentPage(document.toString(), annotationSingleDocEntry.getKey()), annotationSingleDocEntry.getValue());
                }

                nodeService.setProperty(etape, ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES_MULTIDOC, annotationsMultiDoc);
                result = annotationsMultiDoc;
            }
        }

        if (result == null) {
            result = new HashMap<DocumentPage, ArrayList<Annotation>>();
        }

        // Debug log

        int count = 0;
        for (Map.Entry<DocumentPage, ArrayList<Annotation>> annotationEntry : result.entrySet()) {
            logger.debug("Finding " + annotationEntry);
            count += annotationEntry.getValue().size();
        }
        logger.debug("Found " + count + " annotation(s) for " + document);

        return result;
    }

    private boolean canModifyAnnotations(NodeRef dossier, Annotation a) {
        boolean res = false;
        NodeRef parapheur = parapheurService.getParentParapheur(dossier);
        String username = AuthenticationUtil.getRunAsUser();
        if (parapheurService.isParapheurOwnerOrDelegate(parapheur, username) ||
                (parapheurService.isParapheurSecretaire(parapheur, username) && a.isSecretaire())) {
            res = true;
        }
        return res;
    }

    private void setAnnotationsForEtapeAndDocument(@NotNull NodeRef etape, @NotNull NodeRef document, @Nullable HashMap<DocumentPage, ArrayList<Annotation>> annotations) {

        // Debug log

        int count = 0;
        if (annotations != null) {
            for (Map.Entry<DocumentPage, ArrayList<Annotation>> annotationEntry : annotations.entrySet()) {
                logger.debug("Saving " + annotationEntry);
                count += annotationEntry.getValue().size();
            }
        }
        logger.debug("Saved " + count + " annotation(s) for " + document);

        // Saving...

        nodeService.setProperty(etape, ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES_MULTIDOC, annotations);
        nodeService.setProperty(etape, ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES, null);
    }

    private void addAnnotationForEtapeAndDocument(NodeRef etape, NodeRef document, Annotation annotation) {
        HashMap<DocumentPage, ArrayList<Annotation>> annotationsMap = getAnnotationsForEtape(etape, document);

        Integer page = annotation.getPage();
        ArrayList<Annotation> annotations = annotationsMap.get(new DocumentPage(document.toString(), page));
        if (annotations == null) {
            annotations = new ArrayList<Annotation>();
        }

        annotations.add(annotation);
        annotationsMap.put(new DocumentPage(document.toString(), page), annotations);

        setAnnotationsForEtapeAndDocument(etape, document, annotationsMap);
    }

    // <editor-fold desc="Deprecated methods">

    @Deprecated
    @SuppressWarnings("unchecked")
    private HashMap<Integer, ArrayList<Annotation>> getAnnotationsForEtape(NodeRef etape) {
        return (HashMap<Integer, ArrayList<Annotation>>) nodeService.getProperty(etape, ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES);
    }

    @Deprecated
    public UUID addAnnotation(NodeRef dossier, Annotation annotation) {

        NodeRef document = parapheurService.getMainDocuments(dossier).get(0);
        if (document != null) {
            return addAnnotation(dossier, document, annotation);
        }

        return null;
    }

    @Deprecated
    public void removeAnnotation(NodeRef dossier, UUID annotation) {

        NodeRef document = parapheurService.getMainDocuments(dossier).get(0);
        if (document != null) {
            removeAnnotation(dossier, document, annotation);
        }
    }

    @Deprecated
    public void updateAnnotation(NodeRef dossier, Annotation annotation) {

        NodeRef document = parapheurService.getMainDocuments(dossier).get(0);
        if (document != null) {
            updateAnnotation(dossier, document, annotation);
        }
    }

    @Override
    @Deprecated
    public HashMap<Integer, ArrayList<Annotation>> getCurrentAnnotations(NodeRef dossier) {
        EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossier);
        return getAnnotationsForEtape(etape.getNodeRef());
    }

    @Override
    @Deprecated
    public HashMap<Integer, ArrayList<Annotation>> getAllAnnotations(NodeRef dossier) {

        HashMap<Integer, ArrayList<Annotation>> retVal = new HashMap<Integer, ArrayList<Annotation>>();

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);

        for (EtapeCircuit e : circuit) {
            HashMap<Integer, ArrayList<Annotation>> annotationsMap = (HashMap<Integer, ArrayList<Annotation>>) nodeService.getProperty(e.getNodeRef(), ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES);

            if (annotationsMap != null && annotationsMap.size() > 0) {

                for (Integer key : annotationsMap.keySet()) {
                    ArrayList<Annotation> annotations = retVal.get(key);

                    if (annotations == null) {
                        retVal.put(key, annotationsMap.get(key));
                    } else {
                        annotations.addAll(annotationsMap.get(key));
                        retVal.put(key, annotations);
                    }

                }

            }
        }

        return retVal;
    }

    // </editor-fold desc="Deprecated methods">
}

/**
 * Serializable document/page couple.
 * Inner classes can't be serialized, that's why it has to be here.
 */
class DocumentPage implements Serializable {

    private static final long serialVersionUID = 1007749295646561717L;
    private String mDocumentId;
    private Integer mPage;

    public DocumentPage(@NotNull String documentId, @NotNull Integer page) {
        mDocumentId = documentId;
        mPage = page;
    }

    public @NotNull String getDocumentId() {
        return mDocumentId;
    }

    public @NotNull Integer getPage() {
        return mPage;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(mDocumentId).append(mPage).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        } else if (!(obj instanceof DocumentPage)) {
            return false;
        }

        return ((mDocumentId.contentEquals(((DocumentPage) obj).getDocumentId()) && (mPage.equals(((DocumentPage) obj).getPage()))));
    }

    @Override
    public String toString() {
        return "Page " + mPage + "";
    }
}