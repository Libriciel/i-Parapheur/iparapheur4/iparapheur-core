/*
 * Version 3.4
 * CeCILL Copyright (c) 2012-2015, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developpment by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurErrorCode;
import com.atolcd.parapheur.model.ParapheurException;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.repo.action.executer.MailWithAttachmentActionExecuter;
import com.atolcd.parapheur.repo.action.executer.ParapheurMailActionExecuter;
import com.atolcd.parapheur.repo.action.executer.PatchedMailActionExecuter;
import com.atolcd.parapheur.repo.admin.UsersService;
import com.atolcd.parapheur.repo.content.Document;
import com.atolcd.parapheur.repo.job.AbstractBatchJob;
import com.atolcd.parapheur.repo.job.AbstractJob;
import com.atolcd.parapheur.web.bean.wizard.batch.CakeFilter;
import com.atolcd.parapheur.web.bean.wizard.batch.DossierFilter;
import com.atolcd.parapheur.web.bean.wizard.batch.Predicate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.exceptions.InvalidPdfException;
import coop.libriciel.service.UserUtilsService;
import coop.libriciel.signature.exception.SignatureNotFoundException;
import coop.libriciel.signature.model.Signature;
import coop.libriciel.signature.service.SignatureService;
import coop.libriciel.utils.poppler.PopplerUtils;
import coop.libriciel.utils.poppler.TagSearchResult;
import fr.bl.iparapheur.srci.SrciService;
import fr.bl.iparapheur.web.BlexContext;
import fr.starxpert.iparapheur.calque.repo.ImpressionCalqueService;
import org.adullact.iparapheur.repo.amq.MessagesSender;
import org.adullact.iparapheur.repo.jscript.seals.SealCertificate;
import org.adullact.iparapheur.repo.jscript.seals.SealModel;
import org.adullact.iparapheur.repo.notification.NotificationService;
import org.adullact.iparapheur.repo.worker.WorkerService;
import org.adullact.iparapheur.rules.bean.CustomProperty;
import org.adullact.iparapheur.status.StatusMetier;
import org.adullact.iparapheur.util.CollectionsUtils;
import org.adullact.iparapheur.util.EncryptUtils;
import org.adullact.iparapheur.util.PdfSealUtils;
import org.adullact.libersign.util.signature.*;
import org.adullact.spring_ws.iparapheur._1.LogDossier;
import org.adullact.utils.StringUtils;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.repo.domain.audit.AuditDAO;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.audit.AuditQueryParameters;
import org.alfresco.service.cmr.audit.AuditService;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.LimitBy;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.TempFileProvider;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.bean.search.SearchContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.ContainerAwareDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.surf.util.Base64;
import org.springframework.util.Assert;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.transaction.UserTransaction;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe permettant les manipulations de dossier.
 * Resultat du refactoring (en cours) du ParapheurServiceImpl devenu obese.
 * <p>
 * Initiator: Emmanuel Peralta Date: 30/08/12 Time: 11:03
 *
 * @author Emmanuel Peralta - Adullact Projet
 * @author Stephane Vast - Adullact Projet
 */
public class DossierServiceImpl implements DossierService {

    private static Logger logger = Logger.getLogger(DossierServiceImpl.class);
    private static Semaphore sem = new Semaphore(1);
    @Autowired
    @Qualifier("_parapheurService")
    private ParapheurService parapheurService;
    @Autowired
    private ParapheurVersionProperties parapheurVersionProperties;
    @Autowired
    @Qualifier("nodeService")
    private NodeService nodeService;
    @Qualifier("fileFolderService")
    @Autowired
    private FileFolderService fileFolderService;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Qualifier("contentService")
    @Autowired
    private ContentService contentService;
    @Autowired
    private TypesService typesService;
    @Autowired
    private AnnotationService annotationService;
    @Autowired
    private WorkflowService workflowService;
    @Autowired
    @Qualifier("searchService")
    private SearchService searchService;
    @Autowired
    @Qualifier("dictionaryService")
    private DictionaryService dictionaryService;
    @Autowired
    @Qualifier("namespaceService")
    private NamespaceService namespaceService;
    @Autowired
    private JobService jobService;
    @Autowired
    private CorbeillesService corbeillesService;
    @Autowired
    @Qualifier("auditService")
    private AuditService auditService;
    @Autowired
    private MetadataService metadataService;
    @Autowired
    @Qualifier("actionService")
    private ActionService actionService;
    @Autowired
    private S2lowService s2lowService;
    @Autowired
    private SrciService srciService;
    @Autowired
    @Qualifier("permissionService")
    private PermissionService permissionService;
    @Autowired
    @Qualifier("authorityService")
    private AuthorityService authorityService;
    @Autowired
    @Qualifier("transactionService")
    private TransactionService transactionService;
    @Autowired
    @Qualifier("auditDAO")
    private AuditDAO auditDAO;
    @Autowired
    private PersonService personService;
    @Autowired
    private UsersService usersService;
    @Autowired
    private ImpressionCalqueService impressionCalqueService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private MessagesSender messagesSender;
    @Autowired
    private SignatureService signatureService;
    @Autowired
    private UserUtilsService userUtilsService;

    @Value("${parapheur.read.pes.withpdf}")
    boolean pesReadingWithPdf;

    @Value("${parapheur.libersign.tampon.signature.info}")
    String padesStampInformations;

    @Value("${parapheur.ws.creerdossier.autoselectsigformat.ifunsigned.pdf}")
    String unsignedPdfFormat;

    @Value("${parapheur.ws.creerdossier.autoselectsigformat.ifunsigned}")
    String unsignedFormat;

    @Value("${parapheur.ihm.action.mail.allow}")
    boolean allowMail;

    private Properties configuration;
    /**
     * Les des types de documents autorisés
     */
    private List<String> authorizedMimeType;

    /**
     * Initialisation des types autorisés de documents
     */
    private void initAuthorizedMimeType() {
        if (authorizedMimeType == null) {
            authorizedMimeType = new ArrayList<>();
            authorizedMimeType.add(MimetypeMap.MIMETYPE_PDF); //PDF
            authorizedMimeType.add(MimetypeMap.MIMETYPE_WORD); //DOC
            authorizedMimeType.add("application/rtf"); //RTF
            authorizedMimeType.add("text/rtf"); //RTF
            authorizedMimeType.add("application/x-rtf"); //RTF
            authorizedMimeType.add(MimetypeMap.MIMETYPE_OPENDOCUMENT_TEXT); //ODT
            authorizedMimeType.add(MimetypeMap.MIMETYPE_HTML); //HTML
            authorizedMimeType.add(MimetypeMap.MIMETYPE_TEXT_PLAIN); //TXT
            authorizedMimeType.add(MimetypeMap.MIMETYPE_EXCEL); //XLS
            authorizedMimeType.add(MimetypeMap.MIMETYPE_OPENDOCUMENT_SPREADSHEET); //ODS
            authorizedMimeType.add(MimetypeMap.MIMETYPE_PPT); //PPT
            authorizedMimeType.add(MimetypeMap.MIMETYPE_OPENDOCUMENT_PRESENTATION); //ODP
            authorizedMimeType.add(MimetypeMap.MIMETYPE_IMAGE_GIF); //GIF
            authorizedMimeType.add(MimetypeMap.MIMETYPE_IMAGE_JPEG); //JPG
            authorizedMimeType.add(MimetypeMap.MIMETYPE_IMAGE_PNG); //PNG
            //TIF non présent dans MimetypeMap
            //authorizedMimeType.add(MimetypeMap.MIMETYPE_); //TIF
            authorizedMimeType.add(MimetypeMap.MIMETYPE_XML); //XML
            authorizedMimeType.add("application/xml"); //XML
        }
        if (configuration.getProperty("parapheur.document.openxml.accept").equals("true") &&
                !authorizedMimeType.contains(MimetypeMap.MIMETYPE_OPENXML_WORDPROCESSING)) {
            authorizedMimeType.add(MimetypeMap.MIMETYPE_OPENXML_WORDPROCESSING); //DOCX
            authorizedMimeType.add(MimetypeMap.MIMETYPE_OPENXML_SPREADSHEET); //XLSX
            authorizedMimeType.add(MimetypeMap.MIMETYPE_OPENXML_PRESENTATION); //PPTX
        }
    }

    @Override
    public void readDossier(NodeRef dossierRef, String username) {
        if(!hasReadDossier(dossierRef, username)) {
            List<NodeRef> lstDocs = parapheurService.getMainDocuments(dossierRef);
            // We need at least one main document before reading
            if (lstDocs.isEmpty()) return;
            if (nodeService.exists(lstDocs.get(0)) && dossierRef != null) {
                NodeRef parapheurRef = parapheurService.getParentParapheur(dossierRef);
                Boolean signaturePapier = (Boolean) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_PAPIER);
                if ((parapheurRef != null) && (parapheurService.isActeurCourant(dossierRef, username) || (parapheurService.isParapheurSecretaire(parapheurRef, username) && (signaturePapier)))) {
                    EtapeCircuit previousEtape = parapheurService.getPreviousEtapeCircuit(dossierRef);
                    if (previousEtape == null || ((isEmis(dossierRef) && !parapheurService.isParapheurOwnerOrDelegate(previousEtape.getParapheur(), username)) || !previousEtape.getActionDemandee().equals("VISA"))) {
                        logger.debug("OnContentRead : setProperty"); // BLEX warn->debug
                        nodeService.setProperty(dossierRef, ParapheurModel.PROP_RECUPERABLE, Boolean.FALSE);
                    }

                    if (!hasReadDocument(lstDocs.get(0), username) && (pesReadingWithPdf || !isPesViewEnabled(dossierRef))) {
                        logger.debug("OnContentRead : set read for user " + username); // BLEX warn->debug
                        new RetryingTransactionHelper().doInTransaction(() -> {
                            setDocumentLu(lstDocs.get(0), username);
                            return null;
                        });

                        // AUDIT sur lecture du dossier
                        if (!parapheurService.isTermine(dossierRef)) {
                            if (!lstDocs.isEmpty()) {
                                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossierRef);
                                String actDmdee = etape.getActionDemandee();
                                boolean hasToAudit = false;
                                if (actDmdee == null) {   /// compatibilité ascendante
                                    List<EtapeCircuit> leCircuit = parapheurService.getCircuit(dossierRef);
                                    hasToAudit = leCircuit.lastIndexOf(etape) == leCircuit.indexOf(etape);
                                } else {
                                    hasToAudit = actDmdee.trim().equalsIgnoreCase("SIGNATURE");
                                }

                                if(hasToAudit) {
                                    new RetryingTransactionHelper().doInTransaction(() -> {
                                        nodeService.setProperty(dossierRef, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_LU);
                                        return null;
                                    });

                                    List<Object> list = new ArrayList<>();
                                    list.add(StatusMetier.STATUS_LU);
                                    auditWithNewBackend("ParapheurServiceCompat", username + ": Dossier lu et prêt pour la signature", dossierRef, list);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public List<String> getAuthorizedMimeTypeList() {
        if (authorizedMimeType == null) {
            initAuthorizedMimeType();
        }
        return authorizedMimeType;
    }

    //------------------------------------------------------------\\
    //---                       ACTIONS                        ---\\
    //------------------------------------------------------------\\

    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    /**
     * Commence la creation d'un dossier
     *
     * @param parapheur Référence du bureau sur lequel l'utilisateur se trouve
     * @return NodeRef du dossier créé ou incomplet de l'utilisateur
     */
    @Override
    public NodeRef beginCreateDossier(NodeRef parapheur) {
        String currentUser = AuthenticationUtil.getRunAsUser();

        Map<QName, Serializable> props = new HashMap<QName, Serializable>();

        NodeRef dossierRef = dossierUnderCreation(parapheur);
        if (dossierRef == null) {
            props.put(ContentModel.PROP_TITLE, "Nouveau dossier");
            props.put(ParapheurModel.PROP_PUBLIC, false);
            props.put(ParapheurModel.PROP_CONFIDENTIEL, true);
            props.put(ParapheurModel.PROP_UNCOMPLETE, true);
            props.put(ParapheurModel.PROP_SIGNATURE_FORMAT, "PKCS#7/single");

            // propriétés par défaut
            props.put(ContentModel.PROP_CREATOR, currentUser);
            props.put(ContentModel.PROP_CREATED, new Date());
            props.put(ParapheurModel.PROP_DATE_LIMITE, null);
            props.put(ParapheurModel.PROP_SIGNATURE_PAPIER, false);
            props.put(ParapheurModel.PROP_TERMINE, false);
            props.put(ParapheurModel.PROP_RECUPERABLE, false);

            props.put(ParapheurModel.PROP_ORDERED_CHILDREN, true);

            dossierRef = createDossier(parapheur, props);

            //Launch Notification
            //Notification du bureau initiateur de l'action
            notificationService.notifierPourCreation(dossierRef, parapheur);
            //socketServer.notify(new SocketMessage(parapheurService.getParapheurOwners(parapheur), parapheur.getId(), "OK", dossierRef.getId(), "VISA", "Nouveau dossier", SocketMessage.STATE_NEW, getCorbeillesParent(dossierRef, parapheur)));
        }
        /*
         Map<QName, Serializable> pptes = new HashMap<QName, Serializable>();
         pptes.put(ParapheurModel.PROP_EFFECTUEE, false);
         pptes.put(ParapheurModel.PROP_PASSE_PAR, parapheurService.getParentParapheur(dossierRef));
         pptes.put(ParapheurModel.PROP_ACTION_DEMANDEE, "VISA");
         this.nodeService.createNode(dossierRef, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
         ParapheurModel.ASSOC_DOSSIER_ETAPE, ParapheurModel.TYPE_ETAPE_CIRCUIT, pptes);
         */

        return dossierRef;
    }

    @Override
    public NodeRef addSignature(NodeRef dossier, Document doc) {
        Assert.notNull(doc, "Le fichier de signature doit etre renseigné.");
        Assert.isTrue(parapheurService.isDossier(dossier), "Node Ref doit être un ph:dossier");
        Assert.isTrue(!isEmis(dossier), "Le dossier ne doit pas être émis.");
        // ------------------- Signature doc Principal (optionnel)
        if (doc != null) {
            try {
                String mimeType = getDocumentMimeType(doc);
                if (mimeType.equalsIgnoreCase("application/pkcs7-signature")
                        || mimeType.equalsIgnoreCase("application/x-pkcs7-signature")
                        || mimeType.equalsIgnoreCase("text/plain")
                        || mimeType.equalsIgnoreCase("application/octet-stream")
                        || mimeType.equalsIgnoreCase("application/xml")) {
                    // TODO  check signature (au moins la structure, mais pas la validité c'est trop tôt)
                    byte[] signature = doc.getContent();
                    String sigFormat;
                    if(mimeType.equalsIgnoreCase("application/xml")) {
                        sigFormat = "XAdES/detached";
                    } else {
                        sigFormat = "PKCS#7/single";
                    }
                    parapheurService.setSignature(dossier, signature, sigFormat);
                    parapheurService.setSignature(dossier, parapheurService.getCurrentEtapeCircuit(dossier), signature, sigFormat);
                }
            } catch (Exception e) {
                logger.warn("Impossible de mettre à jour la signature pour le dossier " + dossier.getId() + ", exception : " + e.getMessage());
            }
        }
        return dossier;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#setPermissionsDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void setPermissionsDossier(NodeRef dossier) {
        // Sur certains serveurs, le delete permissions génère une erreur...
        try {
            permissionService.deletePermissions(dossier);
        } catch (Exception e) {
            System.out.println("Impossible de supprimer les permissions pour le dossier " + dossier.getId() + ", exception : " + e.getMessage());
        }
        try {
            permissionService.setInheritParentPermissions(dossier, false);

            boolean confidentiel = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_CONFIDENTIEL);
            boolean publik = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_PUBLIC);

            if (publik) {
                permissionService.setPermission(dossier, PermissionService.ALL_AUTHORITIES, PermissionService.CONSUMER, true);
            } else if(!confidentiel) {
                // Groupe
                List<String> owners = parapheurService.getParapheurOwners(parapheurService.getParentParapheur(dossier));
                for (String owner : owners) {
                    Collection<String> groups = authorityService.getContainingAuthorities(AuthorityType.GROUP, authorityService.getName(AuthorityType.USER, owner), true);
                    for (String group : groups) {
                        permissionService.setPermission(dossier, group, PermissionService.CONSUMER, true);
                    }
                }
            }
            // Always set confidential permission to authorize actions
            setConfidentialPermissionsForDossier(dossier);
        } catch (Exception e) {
            logger.warn("Impossible de mettre à jour les permissions pour le dossier " + dossier.getId() + ", exception : " + e.getMessage());
        }
    }

    @Override
    public void setPermissionsArchive(NodeRef archive, NodeRef dossier) {
        Boolean isConfidentiel = (Boolean) this.nodeService.getProperty(dossier, ParapheurModel.PROP_CONFIDENTIEL);
        Boolean isPublic = (Boolean) this.nodeService.getProperty(dossier, ParapheurModel.PROP_PUBLIC);

        permissionService.setInheritParentPermissions(archive, false);

        if (isPublic) {
            permissionService.setPermission(archive, PermissionService.ALL_AUTHORITIES, PermissionService.READ, true);
        } else { // Confidentiel + groupe
            setConfidentialPermissionsForArchive(archive, dossier);

            if (!isConfidentiel) { // groupe

                List<String> emetteurs = parapheurService.getParapheurOwners(parapheurService.getEmetteur(dossier));
                for (String emetteur : emetteurs) {
                    Set<String> setGroupes = authorityService.getContainingAuthorities(AuthorityType.GROUP, emetteur, true);
                    for (String groupe : setGroupes) {
                        if (groupe != null && !groupe.isEmpty()) {
                            permissionService.setPermission(archive, groupe, PermissionService.READ, true);
                        }
                    }
                }
            }
        }
    }

    private void setConfidentialPermissionsForArchive(NodeRef archive, NodeRef dossier) {
        NodeRef etape = parapheurService.getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        while (etape != null && this.nodeService.exists(etape)) {
            NodeRef bureau = (NodeRef) nodeService.getProperty(etape, ParapheurModel.PROP_PASSE_PAR);
            if (bureau != null) {
                // PROPRIETAIRES
                setPermission(archive, "ROLE_PHOWNER_", bureau, PermissionService.READ);

                // SECRETARIAT
                setPermission(archive, "ROLE_PHSECRETARIAT_", bureau, PermissionService.READ);

                // NOTIFIES
                List<NodeRef> listeNotif = (ArrayList<NodeRef>) nodeService.getProperty(etape, ParapheurModel.PROP_LISTE_NOTIFICATION);
                if (listeNotif != null) {
                    for (NodeRef notifie : listeNotif) {
                        setPermission(archive, "ROLE_PHOWNER_", notifie, PermissionService.READ);
                    }
                }
            }
            etape = parapheurService.getFirstChild(etape, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#setConfidentialPermissionsForDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void setConfidentialPermissionsForDossier(NodeRef dossier) {

        NodeRef etape = parapheurService.getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);

        while (etape != null && this.nodeService.exists(etape)) {
            NodeRef bureau = (NodeRef) nodeService.getProperty(etape, ParapheurModel.PROP_PASSE_PAR);
            if (bureau != null) {

                // PROPRIETAIRES
                setPermission(dossier, "ROLE_PHOWNER_", bureau, PermissionService.FULL_CONTROL);

                // SECRETAIRES
                setPermission(dossier, "ROLE_PHSECRETARIAT_", bureau, PermissionService.FULL_CONTROL);

                //DELEGUES
                setPermission(dossier, "ROLE_PHDELEGATES_", bureau, PermissionService.FULL_CONTROL);

                // NOTIFIES
                List<NodeRef> listeNotif = (ArrayList<NodeRef>) nodeService.getProperty(etape, ParapheurModel.PROP_LISTE_NOTIFICATION);
                if (listeNotif != null) {
                    for (NodeRef notifie : listeNotif) {
                        setPermission(dossier, "ROLE_PHOWNER_", notifie, PermissionService.READ);
                    }
                }
            }
            etape = parapheurService.getFirstChild(etape, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
        }
    }

    private void setPermission(NodeRef dossier, String role, NodeRef bureau, String permission) {
        if (authorityService.authorityExists(role + bureau.getId())) {
            permissionService.setPermission(dossier, role + bureau.getId(), permission, true);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#createDossier(org.alfresco.service.cmr.repository.NodeRef,
     * java.util.Map)
     */
    @Override
    public NodeRef createDossier(NodeRef parapheur, Map<QName, Serializable> properties) {
        if (!properties.containsKey(ContentModel.PROP_NAME)) {
            properties.put(ContentModel.PROP_NAME, UUID.randomUUID().toString());
        }

        Assert.isTrue(properties.containsKey(ContentModel.PROP_TITLE), "La propriété \"cm:title\" n'a pas été passée en paramètre.");
        Assert.isTrue(parapheurService.isParapheur(parapheur), "Node Ref doit être un ph:parapheur");

        // On récupère la corbeille dans laquelle créer le dossier
        String creator = AuthenticationUtil.getRunAsUser();
        NodeRef corbeille;
        boolean secretaire = parapheurService.isParapheurSecretaire(parapheur, creator);
        if (secretaire) {
            corbeille = parapheurService.getCorbeille(parapheur, ParapheurModel.NAME_SECRETARIAT);
        } else {
            corbeille = parapheurService.getCorbeille(parapheur, ParapheurModel.NAME_EN_PREPARATION);
        }

        // On récupère le nom du dossier dans la Map
        String nom = (String) properties.get(ContentModel.PROP_NAME);
        NodeRef dossier = fileFolderService.searchSimple(corbeille, nom);
        if (dossier == null) {
            dossier = fileFolderService.create(corbeille, nom, ParapheurModel.TYPE_DOSSIER).getNodeRef();
        }

        // ajout des propriétés
        Map<QName, Serializable> props = new HashMap<QName, Serializable>();

        // propriétés par défaut
        props.put(ContentModel.PROP_CREATOR, creator);
        props.put(ContentModel.PROP_CREATED, new Date());
        props.put(ParapheurModel.PROP_DATE_LIMITE, null);
        props.put(ParapheurModel.PROP_CONFIDENTIEL, true);
        props.put(ParapheurModel.PROP_PUBLIC, false);
        props.put(ParapheurModel.PROP_SIGNATURE_PAPIER, false);
        props.put(ParapheurModel.PROP_TERMINE, false);
        props.put(ParapheurModel.PROP_FULL_CREATED, false);
        props.put(ParapheurModel.PROP_RECUPERABLE, false);
        // FIXME : propriété de Folder mais non présente dans ContModel.java, donc rajouté à ParapheurModel.java;
        // éventuellement à modifier si ca change dans les prochaines versions d'alfresco
        props.put(ParapheurModel.PROP_ORDERED_CHILDREN, true);
        // merge avec les propriétés du dossier (properties override props)
        props.putAll(properties);
        this.nodeService.setProperties(dossier, props);

        if (secretaire) {
            this.nodeService.addAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT, null);
        }

        Map<QName, Serializable> pptes = new HashMap<QName, Serializable>();
        pptes.put(ParapheurModel.PROP_EFFECTUEE, false);
        pptes.put(ParapheurModel.PROP_PASSE_PAR, parapheur);
        pptes.put(ParapheurModel.PROP_ACTION_DEMANDEE, "VISA");
        this.nodeService.createNode(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE, ParapheurModel.TYPE_ETAPE_CIRCUIT, pptes);

        // Define confidential permission
        permissionService.deletePermissions(dossier);
        permissionService.setInheritParentPermissions(dossier, false);
        // PROPRIETAIRES
        setPermission(dossier, "ROLE_PHOWNER_", parapheur, PermissionService.FULL_CONTROL);
        // SECRETAIRES
        setPermission(dossier, "ROLE_PHSECRETARIAT_", parapheur, PermissionService.FULL_CONTROL);
        //DELEGUES
        setPermission(dossier, "ROLE_PHDELEGATES_", parapheur, PermissionService.FULL_CONTROL);

        nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_NON_LU);

        List<Object> list = new ArrayList<Object>();
        list.add(StatusMetier.STATUS_NON_LU);
        auditWithNewBackend("ParapheurServiceCompat", "Création de dossier", dossier, list);
        return dossier;
    }

    private void auditWithNewBackend(String applicationName, String message, NodeRef dossier, List<Object> list) {

        if (logger.isDebugEnabled()) {
            logger.debug("auditWithNewBackend(" + applicationName + ", " + message + ", " + dossier + ", " + list + ")");
        }
        AuditDAO.AuditApplicationInfo application = auditDAO.getAuditApplication(applicationName);
        Calendar cal = Calendar.getInstance();

        Map<String, Serializable> values = new HashMap<String, Serializable>();

        values.put("message", message);
        values.put("dossier", dossier);
        values.put("list", (Serializable) list);
        auditDAO.createAuditEntry(application.getId(), cal.getTimeInMillis(), AuthenticationUtil.getRunAsUser(), values);

        //auditDAO.createAuditEntry(application.getId(), cal.getTimeInMillis(), this.authenticationService.getCurrentUserName(), values);
    }

    public Map<QName, Map<String, String>> getMetadatasMap(String type, String sstype) {
        Map<QName, Map<String, String>> retVal = new HashMap<QName, Map<String, String>>();

        if (type != null && sstype != null && !type.isEmpty() && !sstype.isEmpty()) {
            Map<String, Map<String, String>> mds = typesService.getMetadatasMap(type, sstype);
            if (mds != null) {
                for (String key : mds.keySet()) {
                    retVal.put(QName.createQName(key), mds.get(key));
                }
            }
        }

        return retVal;
    }

    @Override
    public NodeRef getCircuitRef(NodeRef dossier, String type, String sousType) {
        // DEPRECATED: return parapheurService.getCircuitRef(type, sousType);
        // Workflow for selected type / subtype
        Map<String, String> metadataValues = new HashMap<String, String>();
        List<CustomProperty<?>> customProperties = new ArrayList<CustomProperty<?>>();

        Map<QName, Map<String, Serializable>> metaMap = metadataService.getMetaDonneesDossierWithTypo(dossier, type, sousType);
        Map<QName, Map<String, String>> metaBase = getMetadatasMap(type, sousType);
        for (QName qName : metaBase.keySet()) {
            if (metaMap.containsKey(qName) && metaMap.get(qName).containsKey("value") && metaMap.get(qName).get("value") != null && !("" + metaMap.get(qName).get("value")).isEmpty()) {
                metadataValues.put(qName.getLocalName(), "" + metaMap.get(qName).get("value"));
            } else {
                metadataValues.put(qName.getLocalName(), metaBase.get(qName).get("default").isEmpty() ? null : metaBase.get(qName).get("default"));
            }
        }

        for (CustomMetadataDef def : metadataService.getMetadataDefs()) {
            /**
             * Ne renseigner la map QUE SI l'utilisateur a valorise
             * les champs de saisie.
             *
             * ET POUR LA SELECTION DE CIRCUIT AVEC SCRIPT ???
             * RAPPEL : Les métadonnées sont définies APRES la selection de type/sous-type
             */
            String tempVal = metadataValues.get(def.getName().getLocalName());
            if (tempVal != null) {
                if (!tempVal.isEmpty()) {
                    customProperties.add(new CustomProperty<Object>(def.getName().getLocalName(),
                            metadataService.getValueFromString(def.getType(), metadataValues.get(def.getName().getLocalName()))));
                }
            }
        }

        return typesService.getWorkflow(dossier, type, sousType, customProperties);
    }

    /**
     * Re-sélection de circuit après définition des metadonnées
     *
     * @param dossierRef
     * @param type
     * @param sousType
     */
    private void updateWorkflow(NodeRef dossierRef, String type, String sousType) {
        /* creation de l'étape d'emission */
        Map<QName, Serializable> proprietes = new HashMap<QName, Serializable>();
        SavedWorkflow savedWorkflow = workflowService.getSavedWorkflow(getCircuitRef(dossierRef, type, sousType));

        proprietes.put(ParapheurModel.PROP_WORKFLOW, savedWorkflow.getName());

        Map<QName, Serializable> originalProprietes = new HashMap<QName, Serializable>();

        originalProprietes.putAll(nodeService.getProperties(dossierRef));
        originalProprietes.putAll(proprietes);

        //proprietes.put(ParapheurModel.PROP_TDT_NOM, )
        nodeService.setProperties(dossierRef, originalProprietes);

        parapheurService.setCircuit(dossierRef, savedWorkflow.getCircuit());
    }

    /**
     * Défini le circuit pour le dossier donné
     *
     * @param dossierRef Référence du dossier
     * @param type       Type du circuit
     * @param sousType   Sous-Type du circuit
     */
    @Override
    public void setCircuit(NodeRef dossierRef, String type, String sousType) {
        /* creation de l'étape d'emission */
        Map<QName, Serializable> proprietes = new HashMap<QName, Serializable>();


        proprietes.put(ParapheurModel.PROP_TYPE_METIER, type);
        proprietes.put(ParapheurModel.PROP_SOUSTYPE_METIER, sousType);

        proprietes.put(ParapheurModel.PROP_TERMINE, Boolean.FALSE);
        proprietes.put(ParapheurModel.PROP_RECUPERABLE, Boolean.FALSE);


        Map<QName, Serializable> typageProps = parapheurService.getTypeMetierProperties(type);
        if (nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_TYPAGE_METIER)) {
            nodeService.removeAspect(dossierRef, ParapheurModel.ASPECT_TYPAGE_METIER);
        }
        nodeService.addAspect(dossierRef, ParapheurModel.ASPECT_TYPAGE_METIER, typageProps);
        //TODO: add actes support (documentPrincipal pdf)
        /*
         s2lowActes = ("S²LOW".equals(typageProps.get(ParapheurModel.PROP_TDT_NOM))
         && "ACTES".equals(typageProps.get(ParapheurModel.PROP_TDT_PROTOCOLE)));

         */
        boolean isDigitalSignatureMandatory = typesService.isDigitalSignatureMandatory(type, sousType);
        boolean readingMandatory = typesService.isReadingMandatory(type, sousType);
        boolean includeAttachments = typesService.areAttachmentsIncluded(type, sousType);

        SavedWorkflow savedWorkflow = workflowService.getSavedWorkflow(getCircuitRef(dossierRef, type, sousType));

        nodeService.setProperty(dossierRef, ParapheurModel.PROP_WORKFLOW, savedWorkflow.getName());
        nodeService.setProperty(dossierRef, ParapheurModel.PROP_DIGITAL_SIGNATURE_MANDATORY, isDigitalSignatureMandatory);
        /**
         * Si signature electronique obligatoire: on écrase l'éventuelle demande
         * de signature papier
         */
        //FIXME: FIX IT ! DEFAULTS TO FALSE FOR DEMO
        //TODO: READ THE FIXME !
        // if (isDigitalSignatureMandatory) {
        // }
        nodeService.setProperty(dossierRef, ParapheurModel.PROP_READING_MANDATORY, readingMandatory);
        nodeService.setProperty(dossierRef, ParapheurModel.PROP_INCLUDE_ATTACHMENTS, includeAttachments);


        proprietes.put(ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_NON_LU);
        proprietes.put(ParapheurModel.PROP_SIGNATURE_PAPIER, false);

        Map<QName, Serializable> originalProprietes = new HashMap<QName, Serializable>();

        originalProprietes.putAll(nodeService.getProperties(dossierRef));
        originalProprietes.putAll(proprietes);

        //proprietes.put(ParapheurModel.PROP_TDT_NOM, )
        nodeService.setProperties(dossierRef, originalProprietes);
        if (typesService.isTdtAuto(type, sousType)) {
            nodeService.addAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_TDT_AUTO, null);
        }
        if (typesService.isCachetAuto(type, sousType)) {
            nodeService.addAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_CACHET_AUTO, null);
        }
        // si le précédent sous-type prévoyait une étape auto, il faut l'enlever.
        else if (nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_CACHET_AUTO)) {
            nodeService.removeAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_CACHET_AUTO);
        }
        if (typesService.hasToAttestSignature(type, sousType)) {
            nodeService.addAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_ATTEST, null);
        }
        // si le précédent sous-type prévoyait une étape auto, il faut l'enlever.
        else if (nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_TDT_AUTO)) {
            nodeService.removeAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_TDT_AUTO);
        }

        parapheurService.setCircuit(dossierRef, savedWorkflow.getCircuit());
    }

    public boolean isPdfProtected(Document doc) throws Exception {
        if (doc != null) {
            initAuthorizedMimeType();

            String mimeType = getDocumentMimeType(doc);
            if (StringUtils.equalsIgnoreCase(mimeType, MimetypeMap.MIMETYPE_PDF)) {
                try {
                    File tmpFile = TempFileProvider.createTempFile("verify", ".pdf");
                    FileUtils.writeByteArrayToFile(tmpFile, doc.getContent());
                    boolean result = new PopplerUtils().isEncrypted(tmpFile.getAbsolutePath());
                    if(!tmpFile.delete()) {
                        logger.warn("Cannot delete tmp file on method DossierService::isPdfProtected");
                    }
                    return result;
                } catch (Exception e) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean hasToAcceptLockedPdf() {
        return configuration.get("parapheur.document.lockedPDF.accept").equals("true");
    }

    private Document isValidIfPdfOrUnlock(Document doc) throws Exception {
        if (doc != null) {
            initAuthorizedMimeType();

            String mimeType = getDocumentMimeType(doc);
            if (StringUtils.equalsIgnoreCase(mimeType, MimetypeMap.MIMETYPE_PDF)) {
                // Si on peut créer l'objet, le pdf n'est pas protégé par un mdp.
                // Si il y a un mdp, check de la configuration pour lecture 'non ethique' ou renvoi une IOException.
                try {
                    File tmpFile = TempFileProvider.createTempFile("verify", ".pdf");
                    FileUtils.writeByteArrayToFile(tmpFile, doc.getContent());

                    PopplerUtils utils =  new PopplerUtils();
                    if(!utils.isValid(tmpFile.getAbsolutePath())) {
                        throw new ParapheurException(ParapheurErrorCode.INVALID_PDF);
                    }
                    if (utils.isEncrypted(tmpFile.getAbsolutePath())) {
                        if (hasToAcceptLockedPdf()) {
                            File outTmpFile = TempFileProvider.createTempFile("unlock", ".pdf");
                            boolean result = new PopplerUtils().unlock(tmpFile.getAbsolutePath(), outTmpFile.getAbsolutePath());
                            if(!result) {
                                throw new ParapheurException(ParapheurErrorCode.PDF_PROTECTED_ERROR);
                            }

                            doc = new Document(IOUtils.toByteArray(new FileInputStream(outTmpFile)), doc.getFileName());
                            if(!outTmpFile.delete() || !tmpFile.delete()) {
                                logger.warn("Cannot delete tmp files on methode DossierService::isValidIfPdfOrUnlock");
                            }
                        } else {
                            throw new ParapheurException(ParapheurErrorCode.PDF_PROTECTED);
                        }
                    }
                } catch (Exception e) {
                    throw new ParapheurException(ParapheurErrorCode.INVALID_PDF);
                }
            }

        }
        return doc;
    }

    /**
     * Ajout d'un document annexe
     *
     * @param dossierRef Dossier auquel ajouter le document
     * @param doc        Document à ajouter
     * @param visudoc    Visuel du Document
     * @return NodeRef du document ajouté
     * @throws Exception
     */
    @Override
    public NodeRef addAnnexe(NodeRef dossierRef, Document doc, Document visudoc)
            throws Exception {
        doc = isValidIfPdfOrUnlock(doc);
        visudoc = isValidIfPdfOrUnlock(visudoc);
        if (isEmis(dossierRef)) {
            List<Object> list = new ArrayList<Object>();
            list.add(nodeService.getProperty(dossierRef, ParapheurModel.PROP_STATUS_METIER));
            auditWithNewBackend("ParapheurServiceCompat", "Document ajouté en annexe : " + doc.getFileName(), dossierRef, list);
        }
        return this.addDocument(dossierRef, doc, false, visudoc, false);
    }

    /**
     * Ajout d'un document principal
     *
     * @param dossierRef Dossier auquel ajouter le document
     * @param doc        Document à ajouter
     * @param visudoc    Visuel du Document
     * @return NodeRef du document ajouté
     * @throws Exception
     */
    @Override
    public NodeRef addDocumentPrincipal(NodeRef dossierRef, Document doc, Document visudoc)
            throws Exception {
        doc = isValidIfPdfOrUnlock(doc);
        visudoc = isValidIfPdfOrUnlock(visudoc);
        if (!isEmis(dossierRef)) {
            return this.addDocument(dossierRef, doc, true, visudoc, false);
        }
        return null;
    }

    @Override
    public NodeRef setVisuelDocument(NodeRef dossierRef, NodeRef document, Document visuel)
            throws Exception {
        Assert.notNull(dossierRef, "Le dossier n'est pas renseigné");
        Assert.notNull(document, "Le document n'est pas renseigné");
        Assert.notNull(visuel, "Aucun visuel à enregistrer");
        NodeRef docPrincipal = null;
        visuel = isValidIfPdfOrUnlock(visuel);
        if (!isEmis(dossierRef)) {
            initAuthorizedMimeType();
            String mimeType = getDocumentMimeType(visuel);
            if (mimeType.equals(MimetypeMap.MIMETYPE_PDF)) {
                //Re-calcul du nombre de pages
                nodeService.removeProperty(document, ParapheurModel.PROP_DOCUMENT_PAGE_COUNT);
                // get the node ref of the node that will contain the content
                docPrincipal = parapheurService.getMainDocuments(dossierRef).get(0);
                removeGeneratedDocuments(docPrincipal, dossierRef);

                ContentWriter pdfwriter = contentService.getWriter(docPrincipal, ParapheurModel.PROP_VISUEL_PDF, Boolean.TRUE);
                pdfwriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
                pdfwriter.setEncoding("UTF-8");
                pdfwriter.putContent(new ByteArrayInputStream(visuel.getContent()));
            } else {
                throw new Exception("Ce fichier est de type invalide : " + mimeType + ".");
            }
        }
        return docPrincipal;
    }

    @Override
    public NodeRef replaceDocumentPrincipal(NodeRef dossierRef, Document doc, Document visudoc) throws Exception {
        doc = isValidIfPdfOrUnlock(doc);
        visudoc = isValidIfPdfOrUnlock(visudoc);
        if (!isEmis(dossierRef)) {
            return this.addDocument(dossierRef, doc, true, visudoc, true);
        }
        return null;
    }

    private boolean checkAcceptableMimeTypeXML(String mimeTypeString) {
        if (mimeTypeString == null || mimeTypeString.trim().isEmpty()) {
            if (logger.isEnabledFor(org.apache.log4j.Level.WARN)) {
                logger.warn("checkAcceptableMimeTypeXML: no mimetype??");
            }
            return false;
        } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_XML)
                || mimeTypeString.equalsIgnoreCase("application/xml")
                || mimeTypeString.toLowerCase().contains("readerpesv2")) {
            if (logger.isEnabledFor(org.apache.log4j.Level.DEBUG)) {
                logger.debug("checkAcceptableMimeTypeXML: mimetype '" + mimeTypeString + "' accepted");
            }
            return true;
        }
        if (logger.isEnabledFor(org.apache.log4j.Level.DEBUG)) {
            logger.debug("checkAcceptableMimeTypeXML: mimetype '" + mimeTypeString + "' is not accepted as XML");
        }
        return false;
    }

    @Override
    public int getMaxMainDocuments() {
        String test = configuration.getProperty("parapheur.ihm.creerdossier.maindocuments.max");

        int maxMainDocuments;
        try {
            maxMainDocuments = Integer.valueOf(test);
        } catch (NumberFormatException exception) {
            maxMainDocuments = 6;
        }

        if (maxMainDocuments <= 0) {
            maxMainDocuments = 6;
        }
        return maxMainDocuments;
    }

    private NodeRef addDocument(NodeRef dossierRef, Document doc, boolean isDocumentPrincipal, Document visudoc, boolean replace)
            throws Exception {

        int maxSizeMo = Integer.valueOf(parapheurService.getUploadFileSizeLimit());
        double maxSize = maxSizeMo * 1024 * 1024;
        if (maxSizeMo != 0) {
            // Test max size of document
            if (doc.getContent().length > maxSize) {
                throw new Exception("Le taille du document principal depasse celle autorisee (" + maxSizeMo + " Mo)");
            }
            if (visudoc != null && visudoc.getContent().length > maxSize) {
                throw new Exception("Le taille du visuel PDF depasse celle autorisee (" + maxSizeMo + " Mo)");
            }
        }

        if (replace && nodeService.hasAspect(parapheurService.getMainDocuments(dossierRef).get(0), ParapheurModel.ASPECT_PENDING)) {
            throw new Exception("Le visuel du document principal est en cours de génération");
        }

        UserTransaction ut = transactionService.getNonPropagatingUserTransaction();

        NodeRef fileNodeRef = null;

        try {
            ut.begin();
            // Retrieving max authorized documents form alfresco-global.properties (6 by default)
            int maxMainDocuments = getMaxMainDocuments();
            doc.setFileName(StringEscapeUtils.unescapeHtml(doc.getFileName()).replaceAll("[\\p{M}]", ""));
            boolean isZipFile = false;
            String extension = "";
            int i = doc.getFileName().lastIndexOf('.');
            if (i > 0) extension = doc.getFileName().substring(i + 1);
            if (extension.equalsIgnoreCase("zip") && !isDocumentPrincipal && !replace) {
                // Accept ZIP file as annexe
                isZipFile = true;
            }

            initAuthorizedMimeType();
            String mimeType = getDocumentMimeType(doc);
            String mimeTypeVisu = MimetypeMap.MIMETYPE_PDF;
            if (visudoc != null) {
                mimeTypeVisu = getDocumentMimeType(visudoc);
            }
            if (!authorizedMimeType.contains(mimeType) && !isZipFile) {
                if (mimeType.equals(MimetypeMap.MIMETYPE_OPENXML_WORDPROCESSING) || //DOCX
                        mimeType.equals(MimetypeMap.MIMETYPE_OPENXML_SPREADSHEET) ||  //XLSX
                        mimeType.equals(MimetypeMap.MIMETYPE_OPENXML_PRESENTATION)) {
                    throw new ParapheurException(ParapheurErrorCode.XDOCUMENTS_NOT_ACCEPTED);
                } else {
                    throw new Exception(doc.getFileName() + " est de type invalide : " + mimeType + ".");
                }
            } else if (!authorizedMimeType.contains(mimeTypeVisu) && visudoc != null) {
                throw new Exception(visudoc.getFileName() + " est de type invalide : " + mimeTypeVisu + ".");
            } else if ((authorizedMimeType.contains(mimeType) && authorizedMimeType.contains(mimeTypeVisu)) || isZipFile) {
                // get the node ref of the node that will contain the content
                FileInfo fileInfo;
                if (replace) {
                    NodeRef docPrincipal = parapheurService.getMainDocuments(dossierRef).get(0);
                    removeGeneratedDocuments(docPrincipal, dossierRef);
                    deleteSwfPreviewsForDossier(dossierRef);
                    deleteCustomSignatureFromDocument(dossierRef, docPrincipal);
                    fileInfo = fileFolderService.move(
                            docPrincipal,
                            null,
                            doc.getFileName());
                    fileNodeRef = fileInfo.getNodeRef();
                    //Remplacement, suppression de la propriété Visuel PDF, qui devra être initialisée
                    nodeService.removeProperty(fileNodeRef, ParapheurModel.PROP_VISUEL_PDF);
                    //Re-calcul du nombre de pages
                    nodeService.removeProperty(fileNodeRef, ParapheurModel.PROP_DOCUMENT_PAGE_COUNT);
                } else {
                    List<FileInfo> files = fileFolderService.list(dossierRef);

                    // Checking main documents number limit

                    if (isDocumentPrincipal && (parapheurService.getMainDocuments(dossierRef).size() >= maxMainDocuments)) {
                        throw new Exception("Le dossier ne peut contenir plus de documents principaux (max " + maxMainDocuments + ")");
                    }

                    // On teste si un fichier du même nom n'existe pas déjà...

                    for (FileInfo fileElement : files) {
                        if (fileElement.getName().equals(doc.getFileName())) {
                            throw new ParapheurException(ParapheurErrorCode.DOCUMENT_ALREADY_EXISTS);
                        }
                    }

                    //Creation du node

                    fileInfo = fileFolderService.create(
                            dossierRef,
                            doc.getFileName(),
                            ContentModel.TYPE_CONTENT);
                    fileNodeRef = fileInfo.getNodeRef();
                }

                if (fileNodeRef != null) {
                    nodeService.setProperty(fileNodeRef, ContentModel.PROP_CREATOR, AuthenticationUtil.getRunAsUser());
                    if (isDocumentPrincipal || parapheurService.getMainDocuments(dossierRef).isEmpty()) {
                        nodeService.addAspect(fileNodeRef, ParapheurModel.ASPECT_MAIN_DOCUMENT, null);
                    }
                    NodeRef etapeCourante = parapheurService.getCurrentEtapeCircuit(dossierRef).getNodeRef();
                    nodeService.setProperty(fileNodeRef, ParapheurModel.PROP_DOCUMENT_DELETABLE, etapeCourante);

                    ByteArrayInputStream contentInputStream = new ByteArrayInputStream(doc.getContent());

                    // get a writer for the content and put the file
                    ContentWriter writer = contentService.getWriter(fileNodeRef, ContentModel.PROP_CONTENT, true);

                    if (checkAcceptableMimeTypeXML(mimeType)) {
                        String content = new String(doc.getContent());
                        Pattern encodingPattern = Pattern.compile("^<\\?([^>]*)encoding=(?:[\"'])([a-zA-Z0-9_-]+)(?:[\"'])([^>]*)\\?>");
                        Matcher matcher = encodingPattern.matcher(content);
                        if (matcher.find()) {
                            writer.setEncoding(matcher.group(2));
                        }
                    } else {
                        writer.setEncoding("UTF-8");
                    }
                    writer.setMimetype(mimeType);
                    writer.putContent(contentInputStream);

                    // 3.3.0 génération automatique d'un visuel PDF
                    if (!MimetypeMap.MIMETYPE_PDF.equals(mimeType) && !checkAcceptableMimeTypeXML(mimeType)) {
                        if (visudoc != null) {
                            ContentWriter pdfwriter = contentService.getWriter(fileNodeRef, ParapheurModel.PROP_VISUEL_PDF, Boolean.TRUE);
                            pdfwriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
                            pdfwriter.setEncoding("UTF-8");
                            pdfwriter.putContent(new ByteArrayInputStream(visudoc.getContent()));
                        } else {
                            // TODO -> TRANSFORM
                            Map<QName, Serializable> props = new HashMap<QName, Serializable>();
                            props.put(ParapheurModel.PROP_LOCKED, true);
                            nodeService.addAspect(fileNodeRef, ParapheurModel.ASPECT_PENDING, props);

                            ut.commit();

                            JSONObject transformObj = new JSONObject();
                            transformObj.put("mimetype", mimeType);
                            transformObj.put(WorkerService.ID, fileNodeRef.getId());
                            transformObj.put(WorkerService.TYPE, "document");
                            transformObj.put(WorkerService.ACTION, ACTION_DOSSIER.TRANSFORM);
                            transformObj.put(WorkerService.USERNAME, AuthenticationUtil.getRunAsUser());
                            messagesSender.sendWorker(transformObj.toString());
                        }
                    } else if (replace && (nodeService.getProperty(fileNodeRef, ParapheurModel.PROP_VISUEL_PDF) != null)) {
                        // On remplace un doc avec visuel pdf par un doc pdf -> supprimer
                        nodeService.removeProperty(fileNodeRef, ParapheurModel.PROP_VISUEL_PDF);
                    }

                    if(MimetypeMap.MIMETYPE_PDF.equals(mimeType)) {
                        PopplerUtils utils = new PopplerUtils();
                        FileContentReader reader = (FileContentReader) contentService.getReader(fileNodeRef, ContentModel.PROP_CONTENT);
                        if(utils.isSigned(reader.getFile().getAbsolutePath())) {
                            nodeService.addAspect(fileNodeRef, ParapheurModel.ASPECT_SIGNED, null);
                        } else {
                            nodeService.removeAspect(fileNodeRef, ParapheurModel.ASPECT_SIGNED);
                        }
                    }
                }
            }
            if (ut.getStatus() != 3) {
                ut.commit();
            }
        } catch (Exception e) {
            // Catch it, rollback, and throw it again !
            try {
                ut.rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            throw e;
        }
        return fileNodeRef;
    }

    /**
     * Suppression d'un document
     *
     * @param document      NodeRef du document à supprimer
     * @param dossier       NodeRef du dossier parent
     * @param bureauCourant Bureau courant de l'utilisateur
     * @return True Si la suppression à réussie
     */
    @Override
    public boolean removeDocument(NodeRef document, NodeRef dossier, NodeRef bureauCourant) {
        boolean ret = false;
        if (canRemoveDoc(document, dossier, bureauCourant)) {
            UserTransaction utx = transactionService.getNonPropagatingUserTransaction(false);
            try {
                utx.begin();
                String name = (String) nodeService.getProperty(document, ContentModel.PROP_NAME);
                //Suppression des images et des annotations
                removeGeneratedDocuments(document, dossier);
                signatureService.deleteDetachedSignature(document.getId());

                //Suppression du document
                nodeService.deleteNode(document);
                ret = true;
                utx.commit();

                if (isEmis(dossier)) {
                    List<Object> list = new ArrayList<Object>();
                    list.add(nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS_METIER));
                    auditWithNewBackend("ParapheurServiceCompat", "Document supprimé des annexes : " + name, dossier, list);
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    utx.rollback();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

        }
        return ret;
    }

    /**
     * Supprime les images et les annotations générée avec le document passé en
     * paramètre. Remarque : les previews flash ne sont pas supprimées car elles
     * sont associées au dossier et non au document (ce sont les previews du doc
     * principal), et on ne peut pas supprimmer un document principal, juste le
     * remplacer.
     *
     * @param document
     * @param dossier
     */
    private void removeGeneratedDocuments(NodeRef document, NodeRef dossier) {
        //On supprime les images générées
        deleteImagesPreviews(document, dossier);

        if (parapheurService.getMainDocuments(dossier).get(0).equals(document)) {
            annotationService.removeAllCurrentAnnotations(dossier);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#deleteImagesPreviews(org.alfresco.service.cmr.repository.NodeRef,
     * org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void deleteImagesPreviews(NodeRef document, NodeRef dossier) {
        NodeRef documentsImages = getDocumentImagesFolder(dossier, document, false);
        if (documentsImages != null) {
            try {
                fileFolderService.delete(documentsImages);
            } catch (Exception e) {
                logger.trace(e);
            }
        }
    }

    private void deleteImagesPreviews(NodeRef dossier) {
        NodeRef dossierImagesFolder = getDossierImagesFolder(dossier, false);
        if (dossierImagesFolder != null) {
            try {
                fileFolderService.delete(dossierImagesFolder);
            } catch (Exception e) {
                logger.trace(e);
            }
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#deleteSwfPreviews(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void deleteSwfPreviews(NodeRef document) {
        NodeRef dossier = parapheurService.getParentDossier(document);
        if (dossier != null) {
            deleteSwfPreviewsForDossier(dossier);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#deleteSwfPreviewsForDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void deleteSwfPreviewsForDossier(NodeRef dossier) {
        NodeRef previewNode = getPreviewNode(dossier, false);
        if (previewNode != null) {
            nodeService.deleteNode(previewNode);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#deleteDossier(org.alfresco.service.cmr.repository.NodeRef, boolean notify)
     */
    @Override
    public void deleteDossier(NodeRef dossier, boolean notify) {
        // La preview flash n'est pas un fils du dossier, il faut donc la supprimer à la main.
        deleteSwfPreviewsForDossier(dossier);
        deleteImagesPreviews(dossier);
        if (nodeService.exists(dossier)) {
            List<Object> list = new ArrayList<Object>();
            list.add("SuppressionDossier");
            list.add(nodeService.getProperty(dossier, ContentModel.PROP_TITLE));
            auditWithNewBackend("ParapheurServiceCompat", "Suppression du dossier", dossier, list);

            NodeRef emetteur = parapheurService.getParentParapheur(dossier);
            String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
            boolean isInSecretariat = nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT);
            NodeRef parentCorbeille = parapheurService.getParentCorbeille(dossier);
            for(NodeRef doc : parapheurService.getMainDocuments(dossier)) {
                signatureService.deleteAllDetachedSignature(doc.getId());
            }
            String id = dossier.getId();
            corbeillesService.deleteVirtuallyContainsAssociations(dossier);
            nodeService.deleteNode(dossier);

            /**
             * Et si le deleteNode plante ? D'ou le changement de signature de la fonction notifierPourSuppression. Le dossier n'est plus accessible.
             */

            if (notify)
                notificationService.notifierPourSuppression(emetteur, dossierTitre, isInSecretariat, parentCorbeille, id);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#deleteDossierAdmin(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void deleteDossierAdmin(final NodeRef dossier) {
        final boolean[] hasNotified = {false};
        transactionService.getRetryingTransactionHelper().doInTransaction(
                () -> {
                    // La preview flash n'est pas un fils du dossier, il faut donc la supprimer à la main.
                    deleteSwfPreviewsForDossier(dossier);
                    deleteImagesPreviews(dossier);
                    if (nodeService.exists(dossier)) {
                        if(!hasNotified[0]) {
                            ParapheurServiceImpl.TdtState state = parapheurService.getTdtState(dossier, false);
                            notificationService.notifierPourSuppressionAdmin(dossier, state.getStatus(), state.getAckDate());
                            hasNotified[0] = true;
                        }

                        List<Object> list = new ArrayList<>();
                        list.add("SuppressionDossier");
                        list.add(nodeService.getProperty(dossier, ContentModel.PROP_TITLE));
                        auditWithNewBackend("ParapheurServiceCompat", "Suppression du dossier", dossier, list);
                        corbeillesService.deleteVirtuallyContainsAssociations(dossier);
                        nodeService.deleteNode(dossier);

                        notificationService.sendPreparedNotificationsForDossier(dossier.getId());

                    }

                    return true;
                }, false, true);
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#getPreviewNode(org.alfresco.service.cmr.repository.NodeRef,
     * boolean)
     */
    @Override
    public NodeRef getPreviewNode(NodeRef dossier, boolean createIfNotExists) {
        NodeRef previewsNode = getPreviewsNode();

        List<ChildAssociationRef> previewCandidates = nodeService.getChildAssocs(previewsNode,
                ContentModel.PROP_CONTENT,
                QName.createQName("ph:" + dossier.getId()));

        if (previewCandidates.size() > 0) {
            return previewCandidates.get(0).getChildRef();
        }

        String previewXPath = "/app:company_home/app:dictionary/ph:previews/*[@cm:name='" + dossier.getId() + ".swf']";
        // /app:company_home/app:dictionary/ph:previews/*[@cm:name='473d170c-b88d-447f-9974-b775addd68f0.swf']

        List<NodeRef> previewNodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                previewXPath,
                null,
                namespaceService,
                false);
        NodeRef previewNode = null;

        if (previewNodes != null && previewNodes.size() > 0) {
            previewNode = previewNodes.get(0);
        } else if (createIfNotExists) {
            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, dossier.getId() + ".swf");
            properties.put(ContentModel.PROP_TITLE, "Preview for " + nodeService.getProperty(dossier, ContentModel.PROP_NAME));
            properties.put(ContentModel.PROP_DESCRIPTION, "");
            previewNode = nodeService.createNode(previewsNode,
                    ContentModel.ASSOC_CONTAINS,
                    QName.createQName("ph:" + dossier.getId(), namespaceService),
                    ContentModel.TYPE_CONTENT,
                    properties).getChildRef();
        }

        return previewNode;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#getPreviewsNode()
     */
    @Override
    public NodeRef getPreviewsNode() {
        String previewsXpath = "/app:company_home/app:dictionary/ph:previews";

        List<NodeRef> previewsCandidates = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                previewsXpath,
                null,
                namespaceService,
                false);

        if (previewsCandidates.size() > 0) {
            return previewsCandidates.get(0);
        }

        String dictionaryXpath = "/app:company_home/app:dictionary";
        NodeRef dictionary = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                dictionaryXpath,
                null,
                namespaceService,
                false).get(0);

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "Previews");
        properties.put(ContentModel.PROP_TITLE, "Content previews");
        properties.put(ContentModel.PROP_DESCRIPTION, "");
        NodeRef previewsHome = nodeService.createNode(dictionary,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("ph:previews", namespaceService),
                ContentModel.TYPE_FOLDER,
                properties).getChildRef();

        return previewsHome;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#getDocumentImagesFolder(org.alfresco.service.cmr.repository.NodeRef,
     * org.alfresco.service.cmr.repository.NodeRef,
     * boolean)
     */
    @Override
    public NodeRef getDocumentImagesFolder(NodeRef dossier, NodeRef document, boolean createIfNotExists) {
        NodeRef documentImagesFolder = null;
        UserTransaction tx = transactionService.getNonPropagatingUserTransaction();
        try {
            tx.begin();
            NodeRef dossierImagesFolder = getDossierImagesFolder(dossier, createIfNotExists);

            documentImagesFolder = fileFolderService.searchSimple(dossierImagesFolder, document.getId());

            if (documentImagesFolder == null) {
                documentImagesFolder = fileFolderService.create(dossierImagesFolder, document.getId(), ContentModel.TYPE_FOLDER).getNodeRef();
            }
            tx.commit();
        } catch (Exception e) {
            logger.trace("Erreur lors de la création du dossier ph:apercus_png", e);
            try {
                if (tx != null) {
                    tx.rollback();
                }
            } catch (Exception tex) {
            }
        }

        return documentImagesFolder;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#getImageForDocument(org.alfresco.service.cmr.repository.NodeRef,
     * org.alfresco.service.cmr.repository.NodeRef, int)
     */
    @Override
    public NodeRef getImageForDocument(NodeRef dossier, NodeRef document, int page) throws Exception {
        NodeRef image = null;
        NodeRef documentImagesFolder = getDocumentImagesFolder(dossier, document, true);
        if (documentImagesFolder != null) {
            image = fileFolderService.searchSimple(documentImagesFolder, page + ".png");
            if (image == null) {
                int imgNbr = getPageCountForDocument(document);
                if (page < imgNbr) {
                    image = generatePageImageForDocument(dossier, document, page);
                }
            }
        } else {
            logger.warn("Erreur lors de la récupération du dossier d'images pour le document : " + document.getId());
        }
        return image;
    }

    /**
     * Générate la page numéro 'page' de l'aperçu du document passé en paramètre et renvoie son NodeRef.
     *
     * @param document
     * @param dossier
     * @param page
     * @return le NodeRef de la page d'aperçu générée.
     */
    private synchronized NodeRef generatePageImageForDocument(NodeRef dossier, NodeRef document, int page) throws Exception {
        NodeRef imageRef = null;
        NodeRef documentImages = getDocumentImagesFolder(dossier, document, true);
        String sigFormat = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT);
        if (documentImages != null) {
            // On refait une recherche de l'image au cas où un thread était en attente et que l'image a été générée durant son attente.
            imageRef = fileFolderService.searchSimple(documentImages, page + ".png");
            if (imageRef == null) {

                FileContentReader reader = (nodeService.getProperty(document, ParapheurModel.PROP_VISUEL_PDF) != null && sigFormat != null) ?
                        (FileContentReader) contentService.getReader(document, ParapheurModel.PROP_VISUEL_PDF) :
                        (FileContentReader) contentService.getReader(document, ContentModel.PROP_CONTENT);
                initAuthorizedMimeType();

                int resolution = 150;
                try {
                    resolution = Integer.parseInt(configuration.getProperty("parapheur.ghostscript.dpi"));
                } catch (NumberFormatException e) {
                    logger.warn("La propriété 'parapheur.ghostscript.dpi' n'est pas correctement définie : " + configuration.getProperty("parapheur.ghostscript.dpi"));
                }

                File tempFile = TempFileProvider.createTempFile("pdf2multpng", ".png");

                boolean result = new PopplerUtils().getImage(reader.getFile().getAbsolutePath(), page, tempFile.getAbsolutePath(), resolution);
                if(!result) {
                    throw new Exception("Cannot extract image from document");
                }

                imageRef = fileFolderService.create(documentImages, page + ".png", ContentModel.TYPE_CONTENT).getNodeRef();

                ContentWriter contentWriter = contentService.getWriter(imageRef, ContentModel.PROP_CONTENT, true);
                contentWriter.setMimetype("image/png");
                contentWriter.setEncoding("UTF-8");
                contentWriter.putContent(tempFile);

                if(!tempFile.delete()) {
                    logger.warn("Cannot delete tmp file on method DossierService::generatePageImageForDocument");
                }
            }
        }
        return imageRef;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#getPageCountForDocument(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public int getPageCountForDocument(NodeRef document) {

        Integer pageCount = 0;
        try {
            pageCount = (Integer) nodeService.getProperty(document, ParapheurModel.PROP_DOCUMENT_PAGE_COUNT);
            if (pageCount == null) {
                pageCount = setPageCountForDocument(document);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pageCount != null ? pageCount : 0;
    }

    private int setPageCountForDocument(NodeRef document) {
        // On refait un getProperty au cas où un thread était en attente et que la propriété a été valuée lors de son attente.
        Integer[] pageCount = {(Integer) nodeService.getProperty(document, ParapheurModel.PROP_DOCUMENT_PAGE_COUNT)};
        if (pageCount[0] == null) {
            try {

                FileContentReader reader;
                if (nodeService.getProperty(document, ParapheurModel.PROP_VISUEL_PDF) != null) {
                    reader = (FileContentReader) contentService.getReader(document, ParapheurModel.PROP_VISUEL_PDF);
                } else {
                    reader = (FileContentReader) contentService.getReader(document, ContentModel.PROP_CONTENT);
                }

                pageCount[0] = new PopplerUtils().getPageCount(reader.getFile().getAbsolutePath());
                new RetryingTransactionHelper().doInTransaction(() -> {
                    nodeService.setProperty(document, ParapheurModel.PROP_DOCUMENT_PAGE_COUNT, pageCount[0]);
                    return null;
                });
            } catch (Exception e) {
                logger.error("Erreur lors de la récupération du nombre de page du document (pageCount:" + pageCount[0] + " docId:" + document.getId() + ") : " + e.getMessage());
                e.printStackTrace();
            } catch (NoClassDefFoundError nc) {
                logger.warn("Erreur lors de la récupération du nombre de page du document " + document.getId() + " : Le PDF semble être protégé");
            }
        }
        return (pageCount[0] == null ? 0 : pageCount[0]);
    }

    /**
     * Renvoie le dossier contenant les aperçus png des documents du dossier passé en paramètre.
     *
     * @param dossier
     * @param createIfNotExists si vrai, créé le dossier (cm:Folder) s'il n'existe pas.
     * @return le dossier contenant les aperçus png des documents du dossier.
     */
    private NodeRef getDossierImagesFolder(NodeRef dossier, boolean createIfNotExists) {

        NodeRef dossierImagesFolder = null;
        String xpath = "/app:company_home/app:dictionary/ph:apercus_png/ph:dossier" + dossier.getId();

        List<NodeRef> results = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                xpath,
                null,
                namespaceService,
                false);


        if (results.size() > 0) {
            dossierImagesFolder = results.get(0);
        } else if (createIfNotExists) {
            NodeRef imagesFolder = getImagesFolder();
            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, dossier.getId());
            properties.put(ContentModel.PROP_TITLE, "Images for " + dossier.getId());
            properties.put(ContentModel.PROP_DESCRIPTION, "Folder containing png images for " + dossier.getId());
            dossierImagesFolder = nodeService.createNode(imagesFolder,
                    ContentModel.ASSOC_CONTAINS,
                    QName.createQName("ph:dossier" + dossier.getId(), namespaceService),
                    ContentModel.TYPE_FOLDER,
                    properties).getChildRef();

        }

        return dossierImagesFolder;
    }

    /**
     * Renvoie le dossier contenant les aperçus png de tous les dossiers du parapheur
     *
     * @return le dossier contenant les aperçus png de tous les dossiers du parapheur
     */
    private NodeRef getImagesFolder() {

        NodeRef apercus = null;
        String apercusXpath = "/app:company_home/app:dictionary/ph:apercus_png";

        List<NodeRef> apercusSearch = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                apercusXpath,
                null,
                namespaceService,
                false);


        if (apercusSearch.size() > 0) {
            apercus = apercusSearch.get(0);
        } else {
            String dictionaryXpath = "/app:company_home/app:dictionary";
            NodeRef dictionary = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                    dictionaryXpath,
                    null,
                    namespaceService,
                    false).get(0);

            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, "ImagesFolder");
            properties.put(ContentModel.PROP_TITLE, "Images folder");
            properties.put(ContentModel.PROP_DESCRIPTION, "Folder containing png images for V4 document previews");
            apercus = nodeService.createNode(dictionary,
                    ContentModel.ASSOC_CONTAINS,
                    QName.createQName("ph:apercus_png", namespaceService),
                    ContentModel.TYPE_FOLDER,
                    properties).getChildRef();
        }
        return apercus;
    }


    protected List<String> splitEmails(String emails) {
        List<String> retVal = new ArrayList<String>();
        if (emails.contains(",")) {
            String[] tab = emails.split(",");
            for (int i = 0; i < tab.length; i++) {
                String email = tab[i].trim();
                if (email.length() > 0) {
                    retVal.add(email);
                }
            }
        } else {
            retVal.add(emails);
        }

        return retVal;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#sendMail(java.util.List, java.util.List, String, String, java.util.List, boolean, boolean)
     */
    @Override
    public void sendMail(List<NodeRef> dossiers, List<String> destinataires, String objet, String message, List<NodeRef> includedAnnexes, boolean annexesIncluded, boolean includeFirstPage) {
        HashMap<String, File> attachments = new HashMap<String, File>();
        String dossierName,
                body,
                subject;
        List<NodeRef> documents;
        File pdfFile = null;

        // Building Mails

        Action mailAction = actionService.createAction(MailWithAttachmentActionExecuter.NAME);

        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_TEMPLATE, "parapheur-dossier-email.ftl");
        if (destinataires.size() > 1) {
            mailAction.setParameterValue(PatchedMailActionExecuter.PARAM_TO_MANY, (ArrayList) destinataires);
        } else {
            mailAction.setParameterValue(PatchedMailActionExecuter.PARAM_TO, destinataires.get(0));
        }

        // Building from

        String userName = AuthenticationUtil.getRunAsUser();
        NodeRef user = personService.getPerson(userName, false);
        String userMail = (String) nodeService.getProperty(user, ContentModel.PROP_EMAIL); // We don't want the preferences mail here !
        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_FROM, userMail);

        // "Dirty hack" pour libellé  BLEX

        if ("blex".equals(parapheurService.getHabillage())) {
            mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_FOOTER, ParapheurMailActionExecuter.PARAM_FOOTER_BLEX);
        } else {
            mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_FOOTER, ParapheurMailActionExecuter.PARAM_FOOTER_LIBRICIEL);
        }

        for (NodeRef dossierRef : dossiers) {
            // generating PDF File
            //S'il n'y a qu'un dossier on peut choisir les index
            try {
                if (dossiers.size() == 1) {
                    pdfFile = parapheurService.genererDossierPDF(dossierRef, false, includedAnnexes, includeFirstPage);
                } else {
                    //Sinon on prend la valeur envoyée
                    pdfFile = parapheurService.genererDossierPDF(dossierRef, false, annexesIncluded, includeFirstPage);
                }
            } catch (NoTransformerException e) {
                logger.warn("La génération de la PJ du mail a échoué. On envoie le mail sans PJ PDF");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            if (!parapheurService.isDossier(dossierRef)) {
                throw new AlfrescoRuntimeException("SendDossierByMail called on a non-dossier element");
            }

            dossierName = (String) nodeService.getProperty(dossierRef, ContentModel.PROP_TITLE);
            //FIXME: do it in configuration !
            body = (!message.isEmpty()) ? message : "Veuillez trouver en pièce jointe le contenu du dossier \"" + dossierName + "\".\nJe vous souhaite bonne réception.";
            subject = (!objet.isEmpty()) ? objet : "Dossier : " + dossierName;

            documents = parapheurService.getAttachments(dossierRef);

            mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_SUBJECT, subject);
            mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_TEXT, body);

            attachments.clear();

            // Pas de PJ si pas de PDF
            if (pdfFile != null) {
                attachments.put(dossierName + ".pdf", pdfFile);
            }

            mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_ATTACHMENTS, attachments);
            actionService.executeAction(mailAction, dossierRef);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#sendTdtHelios(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef, String, String)
     */
    @Override
    public void sendTdtHelios(NodeRef bureauCourant, NodeRef dossierRef, String annotPub, String annotPriv) throws Exception {
        parapheurService.setAnnotationPublique(dossierRef, annotPub);
        parapheurService.setAnnotationPrivee(dossierRef, annotPriv);

        // Le signataire enregistré doit être le propriétaire du parapheur (cas de signature papier)
        NodeRef parapheurRef = this.parapheurService.getParentParapheur(dossierRef);

        String user = AuthenticationUtil.getRunAsUser();
        String fullname = parapheurService.getPrenomNomFromLogin(user);
        if (this.parapheurService.isParapheurSecretaire(parapheurRef, user)) {
            //NodeRef ownerRef = this.personService.getPerson();
            fullname = String.format("%s pour le compte de \"%s\"", fullname, parapheurService.getNomParapheur(parapheurRef));
        } else if (this.parapheurService.isParapheurDelegate(parapheurRef, user) && !(this.parapheurService.isParapheurOwner(parapheurRef, user) && parapheurRef.equals(bureauCourant))) {
            fullname = String.format(configuration.getProperty("parapheur.suppleance.text"), fullname + ", " + parapheurService.getNomParapheur(bureauCourant), parapheurService.getNomParapheur(parapheurRef));
        }

        parapheurService.setSignataire(dossierRef, fullname);

        boolean inTdtMode;
        try {
            inTdtMode = parapheurService.getCurrentEtapeCircuit(dossierRef)
                    .getActionDemandee()
                    .equals(EtapeCircuit.ETAPE_TDT);
        } catch (NullPointerException e) {
            // In some cases, one of the values can be null (ie, old workflows)
            inTdtMode = false;
        }
        if (inTdtMode) {
            // Teletransmission (le cas echeant)
            String typeMetier = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER);
            Map<QName, Serializable> typeProps = parapheurService.getTypeMetierProperties(typeMetier);
            String tdtName = (String) typeProps.get(ParapheurModel.PROP_TDT_NOM);
            String protocole = (String) typeProps.get(ParapheurModel.PROP_TDT_PROTOCOLE);

            if (tdtName.equals(S2lowService.PROP_TDT_NOM_S2LOW) || tdtName.equals(FastServiceImpl.PROP_TDT_NOM_FAST)) {
                if (ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS.equals(protocole)) {
                    logger.debug("Envoi protocole HELIOS");
                    try {
                        s2lowService.envoiS2lowHelios(dossierRef);
                    } catch (IOException ex) {
                        // TODO : rejeter le dossier!
                        throw new RuntimeException("Erreur de télétransmission : " + ex.getMessage(), ex);
                    }
                } else {
                    throw new Exception("Essai d'envoi sur Helios pour un dossier ayant pour protocole : " + protocole);
                }
            } // BLEX
            else if (SrciService.K.tdtName.equals(tdtName)) {

                if (SrciService.K.protocol.helios.equals(protocole)) {
                    srciService.sendHelios(dossierRef);
                    logger.debug("dossier envoye a SRCI : " + dossierRef);
                } else {
                    throw new Exception("Le protocole [" + protocole + "] n'est pas supporte par le TDT [" + SrciService.K.tdtName + "]");
                }
            }
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#sendTdtHelios(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef, String, String)
     */
    @Override
    public void doStamp(NodeRef bureauCourant, NodeRef dossierRef, String annotPub, String annotPriv, boolean automatic) throws Exception {
        parapheurService.setAnnotationPublique(dossierRef, annotPub);
        parapheurService.setAnnotationPrivee(dossierRef, annotPriv);

        // Le signataire enregistré doit être le propriétaire du parapheur (cas de signature papier)
        String user = AuthenticationUtil.getRunAsUser();
        String fullname = parapheurService.getPrenomNomFromLogin(user);

        parapheurService.setSignataire(dossierRef, fullname);

        boolean inCachetMode;
        try {
            inCachetMode = parapheurService.getCurrentEtapeCircuit(dossierRef)
                    .getActionDemandee()
                    .equals(EtapeCircuit.ETAPE_CACHET);
        } catch (NullPointerException e) {
            // In some cases, one of the values can be null (ie, old workflows)
            inCachetMode = false;
        }
        if (inCachetMode) {

            final GsonBuilder builder = new GsonBuilder();
            final Gson gson = builder.create();

            // Cachet (le cas echeant)
            String typeMetier = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER);
            String sousTypeMetier = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SOUSTYPE_METIER);

            // We get the cachet number
            int numCachet = 1;
            List<EtapeCircuit> circuit = parapheurService.getCircuit(dossierRef);
            for(EtapeCircuit etape : circuit) {
                numCachet += etape.isApproved() && etape.getActionDemandee().equalsIgnoreCase("cachet") ? 1 : 0;
            }

            // On récupère le certificat signataire
            NodeRef certificate = typesService.getCertificate(typeMetier, sousTypeMetier);
            // Verify that it is not deleted
            if (!nodeService.exists(certificate)) {
                throw new Exception("Impossible de sceller le document. Le certificat cachet serveur est introuvable.");
            }
            // Verify that it is not expired
            SealCertificate.CachetDescription cachetDescription = gson.fromJson((String) nodeService.getProperty(certificate, ContentModel.PROP_DESCRIPTION), SealCertificate.CachetDescription.class);
            if (cachetDescription.getNotAfter() < System.currentTimeMillis()) {
                throw new Exception("Impossible de sceller le document. Le certificat cachet serveur est expiré.");
            }
            ContentReader pkcs12Reader = contentService.getReader(certificate, ContentModel.PROP_CONTENT);
            ContentReader imageReader = contentService.getReader(certificate, SealModel.PROP_SEAL_IMAGE);

            com.itextpdf.text.Image image = null;
            if (imageReader != null) {
                try {
                    image = com.itextpdf.text.Image.getInstance(IOUtils.toByteArray(imageReader.getContentInputStream()));
                } catch (IOException ioe) {
                    logger.warn("Cannot use image as timestamp", ioe);
                }

            }

            String textToAdd = "";
            if(!automatic) {
                textToAdd = (String) nodeService.getProperty(certificate, SealModel.PROP_ADDITIONAL_TEXT);
                if(textToAdd != null) {
                    String role = userUtilsService.getRoleWithFolderAndDesk(user, dossierRef, this.parapheurService.getParentParapheur(dossierRef));

                    DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH'h'mm");
                    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    LocalDateTime now = LocalDateTime.now();
                    textToAdd = textToAdd
                            .replace("%n", fullname)
                            .replace("%h", hourFormatter.format(now))
                            .replace("%d", dateFormatter.format(now))
                            .replace("%r", role);
                }
            }

            String encryptedPassword = (String) nodeService.getProperty(certificate, ContentModel.PROP_PASSWORD);
            String pkcs12Password = EncryptUtils.decrypt(encryptedPassword, configuration.getProperty("parapheur.cachetserver.security.key"));

            Properties padesProperties = parapheurService.getPadesSignatureProperties(dossierRef);

            List<NodeRef> documents = parapheurService.getMainDocuments(dossierRef);

            for (NodeRef document : documents) {
                File tmpFile = TempFileProvider.createTempFile("cachet" + document.getId(), null);

                ContentReader pdfReader = null;

                // If we have a non-pdf document, we take the "visuel"
                ContentReader visuelReader = contentService.getReader(document, ParapheurModel.PROP_VISUEL_PDF);
                if (visuelReader != null) {
                    pdfReader = visuelReader;
                } else {
                    pdfReader = contentService.getReader(document, ContentModel.PROP_CONTENT);
                }

                PadesVisualPreparator preparator = new PadesVisualPreparator(padesProperties, document, numCachet);
                try {
                    preparator.invoke(true);
                } catch (InvalidPdfException e) {
                    e.printStackTrace();
                    throw new Exception("Impossible de sceller le document. Le fichier doit être en format PDF.");
                }

                com.itextpdf.text.Rectangle signrectangle = null;

                // Si pas de hauteur, par d'affichage
                com.itextpdf.awt.geom.Rectangle rectangle = new com.itextpdf.awt.geom.Rectangle();
                rectangle.x = preparator.getX();
                rectangle.y = preparator.getY();
                rectangle.width = preparator.getWidth();
                rectangle.height = preparator.getHeight();

                signrectangle = new com.itextpdf.text.Rectangle(rectangle);
                int page = preparator.getPage();

                PdfSealUtils.signPdf(
                        ((FileContentReader) pdfReader).getFile().getAbsolutePath(),
                        tmpFile.getAbsolutePath(),
                        ((FileContentReader) pkcs12Reader).getFile().getAbsolutePath(),
                        pkcs12Password,
                        pkcs12Password,
                        true,
                        preparator.getHeight() != 0,
                        signrectangle,
                        page,
                        image,
                        textToAdd);

                ContentWriter writer = contentService.getWriter(document, ContentModel.PROP_CONTENT, true);
                writer.putContent(tmpFile);
                writer.setMimetype(MimetypeMap.MIMETYPE_PDF);

                // Suppression du visuel, éventuellement
                nodeService.removeProperty(document, ParapheurModel.PROP_VISUEL_PDF);

                // Régénération des images d'apercu
                deleteImagesPreviews(document, dossierRef);
            }

            parapheurService.approveV4(dossierRef, bureauCourant);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#sendTdtActes(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef, String, String, String, java.util.Date, String, String, String)
     */
    @Override
    public void sendTdtActes(NodeRef bureauCourant, NodeRef dossierRef, String nature, String classification, String numero, Date date, String objet, String annotPub, String annotPriv) throws Exception {
        parapheurService.setAnnotationPublique(dossierRef, annotPub);
        parapheurService.setAnnotationPrivee(dossierRef, annotPriv);

        // Le signataire enregistré doit être le propriétaire du parapheur (cas de signature papier)
        NodeRef parapheurRef = this.parapheurService.getParentParapheur(dossierRef);

        String user = AuthenticationUtil.getRunAsUser();
        String fullname = parapheurService.getPrenomNomFromLogin(user);
        if (this.parapheurService.isParapheurSecretaire(parapheurRef, user)) {
            //NodeRef ownerRef = this.personService.getPerson();
            fullname = String.format("%s pour le compte de \"%s\"", fullname, parapheurService.getNomParapheur(parapheurRef));
        } else if (this.parapheurService.isParapheurDelegate(parapheurRef, user) && !(this.parapheurService.isParapheurOwner(parapheurRef, user) && parapheurRef.equals(bureauCourant))) {
            fullname = String.format(configuration.getProperty("parapheur.suppleance.text"), fullname + ", " + parapheurService.getNomParapheur(bureauCourant), parapheurService.getNomParapheur(parapheurRef));
        }

        parapheurService.setSignataire(dossierRef, fullname);

        boolean inTdtMode;
        try {
            inTdtMode = parapheurService.getCurrentEtapeCircuit(dossierRef)
                    .getActionDemandee()
                    .equals(EtapeCircuit.ETAPE_TDT);
        } catch (NullPointerException e) {
            // In some cases, one of the values can be null (ie, old workflows)
            inTdtMode = false;
        }
        if (inTdtMode) {
            // Teletransmission (le cas echeant)
            String typeMetier = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER);
            Map<QName, Serializable> typeProps = parapheurService.getTypeMetierProperties(typeMetier);
            String tdtName = (String) typeProps.get(ParapheurModel.PROP_TDT_NOM);
            if (tdtName.equals(S2lowService.PROP_TDT_NOM_S2LOW) || tdtName.equals(FastServiceImpl.PROP_TDT_NOM_FAST)) {
                String protocole = (String) typeProps.get(ParapheurModel.PROP_TDT_PROTOCOLE);

                if ("ACTES".equals(protocole)) {
                    Assert.notNull(date, "La date de la décision est obligatoire");
                    logger.debug("Envoi protocole ACTES");

                    Assert.isTrue(numero.matches("[A-Z0-9_]{1,15}"), "Le numero de l'acte contient des caractères interdits");
                    String actesDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
                    try {
                        nodeService.setProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_DATE, date);
                        nodeService.setProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_CLASSIFICATION, classification);
                        nodeService.setProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_NATURE, nature);
                        nodeService.setProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_OBJET, objet);

                        s2lowService.envoiS2lowActes(dossierRef, nature, classification, numero, objet, actesDate);
                    } catch (IOException ex) {
                        throw new RuntimeException("Erreur de télétransmission : " + ex.getMessage(), ex);
                    } catch (Exception e) {
                        logger.error("ooch, une exception", e);
                        // e.printStackTrace();
                        throw e;
                    }
                } else {
                    throw new Exception("Essai d'envoi sur ACTES pour un dossier ayant pour protocole : " + protocole);
                }
            } // BLEX
            else if (typeProps.get(ParapheurModel.PROP_TDT_NOM).equals(SrciService.K.tdtName)) {
                throw new ParapheurException(ParapheurErrorCode.ACTES_IS_NOT_SUPPORTED_IN_SELECTED_TDT);
            } else {
                throw new ParapheurException(ParapheurErrorCode.TDT_NOT_SET_IN_TYPE);
            }
        }
    }

    @Override
    @SuppressWarnings("empty-statement")
    public void sendMailSec(NodeRef _bureauCourant, List<NodeRef> dossiers, List<String> _destinataires, List<String> _destinatairesCC, List<String> _destinatairesCCI, String _objet, String _message, String _password, boolean _showPass, List<NodeRef> _includedAnnexes, boolean _annexesIncluded) {
        if (_destinatairesCC.isEmpty()) {
            _destinatairesCC = null;
        }
        if (_destinatairesCCI.isEmpty()) {
            _destinatairesCCI = null;
        }

        final NodeRef bureauCourant = _bureauCourant;
        final List<String> destinataires = _destinataires;
        final List<String> destinatairesCC = _destinatairesCC;
        final List<String> destinatairesCCI = _destinatairesCCI;
        final String objet = _objet;
        final String message = _message;
        final String password = _password;
        final boolean showPass = _showPass;
        final int dossiersSize = dossiers.size();
        final List<NodeRef> includedAnnexes = _includedAnnexes;
        final boolean annexesIncluded = _annexesIncluded;

        AbstractJob mailsecJob = new AbstractBatchJob() {
            @Override
            public Void doWorkUnitInTransaction(NodeRef dossierRef) {
                if (!parapheurService.isDossier(dossierRef)) {
                    throw new AlfrescoRuntimeException("SendDossierByMail called on a non-dossier element");
                }
                String dossierName,
                        body,
                        subject;

                List<NodeRef> selectedAttachments = null;

                dossierName = (String) nodeService.getProperty(dossierRef, ContentModel.PROP_TITLE);

                body = (!message.isEmpty()) ? message : s2lowService.getSecureMailTemplate(dossierRef);
                subject = (!objet.isEmpty()) ? objet : "Dossier : " + dossierName;

                try {
                    if (dossiersSize == 1) {
                        selectedAttachments = includedAnnexes;
                    } else {
                        if (annexesIncluded) {
                            selectedAttachments = parapheurService.getAttachments(dossierRef);
                        }
                    }
                    int mail_id = s2lowService.sendSecureMail(destinataires, destinatairesCC, destinatairesCCI, subject, body, password, showPass, dossierRef, selectedAttachments, true);

                    String user = AuthenticationUtil.getRunAsUser();
                    String fullname = parapheurService.getPrenomNomFromLogin(user);

                    if (parapheurService.isParapheurSecretaire(bureauCourant, user)) {
                        fullname = String.format("%s pour le compte de \"%s\"", fullname, parapheurService.getNomParapheur(bureauCourant));
                    }

                    parapheurService.setSignataire(dossierRef, fullname);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };

        // desactivate auto unlocking of handled NodeRefs.
        mailsecJob.setShallUnlock(true);
        jobService.postJobAndLockNodes(mailsecJob, dossiers);
    }

    /**
     * Permet l'envoie d'un tableau de byte représentant le fichier à imprimer
     *
     * @param dossier          Le dossier à joindre
     * @param includedAnnexes  Les annexes à inclure
     * @param includeFirstPage indique si le bordereau de signature doit être inclus
     */
    @Override
    public byte[] print(NodeRef dossier, List<NodeRef> includedAnnexes, boolean includeFirstPage) throws FileNotFoundException, IOException, Exception {
        File pdfFile;
        //        resp.setHeader("Content-disposition", "attachment; filename=" + URLEncoder.encode(docName, "UTF-8"));

        pdfFile = parapheurService.genererDossierPDF(dossier,
                false,
                includedAnnexes,
                includeFirstPage);
        logger.debug(pdfFile.length() + " bytes to send");

        FileInputStream fis = new FileInputStream(pdfFile);

        // Buffered streams are used for performance reasons.
        BufferedInputStream in = new BufferedInputStream(fis);

        byte[] encoded = IOUtils.toByteArray(in);

        in.close();
        fis.close();

        return encoded;
    }

    /**
     * Permet de vérifier que le document est supprimable par l'utilisateur en
     * cours lors de l'étape en cours
     *
     * @param document      Le document
     * @param dossier       Le dossier
     * @param bureauCourant Le bureau courant
     * @return true si le document est supprimable, false sinon
     */
    @Override
    public boolean canRemoveDoc(NodeRef document, NodeRef dossier, NodeRef bureauCourant) {
        NodeRef parapheur = parapheurService.getParentParapheur(document);
        String username = AuthenticationUtil.getRunAsUser();

        EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossier);
        if (etape == null) {
            return false;
        }
        NodeRef etapeCourante = etape.getNodeRef();
        String creator = (String) nodeService.getProperty(document, ContentModel.PROP_CREATOR);

        return (etapeCourante.equals(nodeService.getProperty(document, ParapheurModel.PROP_DOCUMENT_DELETABLE)) &&
                parapheurService.isOwnerOrDelegateOfDossier(bureauCourant, dossier) &&
                (parapheurService.isParapheurOwnerOrDelegate(parapheur, username) ||
                        (parapheurService.isParapheurSecretaire(parapheur, username) &&
                                parapheurService.isParapheurSecretaire(parapheur, creator))) &&
                !nodeService.hasAspect(document, ParapheurModel.ASPECT_PENDING));
    }

    /**
     * Défini les propriétés du dossier avec audit trail !
     *
     * @param dossierRef Dossier sur lequel définir les propriétés
     * @param properties Propriétés à définir
     */
    @Override
    public
    @NotNull
    String setDossierPropertiesWithAuditTrail(@NotNull NodeRef dossierRef, @Nullable NodeRef parapheur,
                                              @NotNull Map<QName, Serializable> properties, boolean onlyAudit) {

        UserTransaction utx = transactionService.getNonPropagatingUserTransaction(false);
        String result = null;

        try {
            utx.begin();
            nodeService.setProperty(dossierRef, ParapheurModel.PROP_FULL_CREATED, true);
            utx.commit();
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (!onlyAudit) {
            Map<String, Serializable> propertiesString = new HashMap<String, Serializable>();
            for (Map.Entry<QName, Serializable> entry : properties.entrySet()) {
                propertiesString.put(entry.getKey().toPrefixString(namespaceService), (entry.getValue() == null) ? null : entry.getValue());
            }
            result = setDossierProperties(dossierRef, propertiesString);
        }

        if (result == null) {
            result = "Erreur à la définition des propriétés du dossier.";
        }

        return result;
    }

    @Override
    public void setDossierQNameProperties(NodeRef dossier, Map<QName, Serializable> originalProperties) {
        UserTransaction ut = transactionService.getUserTransaction(false);
        try {
            ut.begin();
            nodeService.setProperties(dossier, originalProperties);
            ut.commit();
        } catch (Exception e) {
            try {
                ut.rollback();
            } catch (Exception e2) {
                e.printStackTrace();
                e2.printStackTrace();
            }
        }
    }

    /**
     * Défini les propriétés du dossier
     *
     * @param dossierRef       Dossier sur lequel définir les propriétés
     * @param propertiesString Propriétés à définir
     */
    @Override
    public String setDossierProperties(NodeRef dossierRef, Map<String, Serializable> propertiesString) {

        Map<QName, Serializable> newProperties = CollectionsUtils.createQNameProperties(propertiesString, dictionaryService, namespaceService);
        Map<QName, Serializable> dossierProperties = nodeService.getProperties(dossierRef);
        Boolean originalConfidentiel = (Boolean) dossierProperties.get(ParapheurModel.PROP_CONFIDENTIEL);
        Boolean originalPublic = (Boolean) dossierProperties.get(ParapheurModel.PROP_PUBLIC);

        // Check data integrity

        try {
            metadataService.areMetadataValid(newProperties);
        } catch (IllegalArgumentException ex) {
            return "Erreur - " + ex.getMessage();
        }

        // Update with given parameters

        for (Map.Entry<QName, Serializable> entry : newProperties.entrySet()) {
            if (entry.getValue() == null) {
                dossierProperties.remove(entry.getKey());
            } else {
                dossierProperties.put(entry.getKey(), entry.getValue());
            }
        }

        // Update other data

        Boolean newConfidentiel = (Boolean) newProperties.get(ParapheurModel.PROP_CONFIDENTIEL);
        Boolean newPublic = (Boolean) newProperties.get(ParapheurModel.PROP_PUBLIC);

        Map<QName, Serializable> typageProps = null;

        String type = (String) dossierProperties.get(ParapheurModel.PROP_TYPE_METIER);
        String sousType = (String) dossierProperties.get(ParapheurModel.PROP_SOUSTYPE_METIER);

        // Si on n'est pas en multi-document, on redéfini les pièces principales supplémentaires en annexes !
        boolean isMultiDocument = typesService.isMultiDocument(type, sousType);
        if (!isMultiDocument) {
            setUniDocument(dossierRef);
        }

        //Reselection du circuit, si script de selection et dossier brouillon
        boolean isEmis = isEmis(dossierRef);

        if (!isEmis) {

            dossierProperties.put(ParapheurModel.PROP_TERMINE, Boolean.FALSE);
            dossierProperties.put(ParapheurModel.PROP_RECUPERABLE, Boolean.FALSE);
            dossierProperties.put(ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_NON_LU);
            dossierProperties.put(ParapheurModel.PROP_SIGNATURE_PAPIER, newProperties.get(ParapheurModel.PROP_SIGNATURE_PAPIER));

            typageProps = parapheurService.getTypeMetierProperties(type);
            if (nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_TYPAGE_METIER)) {
                nodeService.removeAspect(dossierRef, ParapheurModel.ASPECT_TYPAGE_METIER);
            }
        }

        nodeService.setProperties(dossierRef, dossierProperties);

        boolean hasErrorCircuit = false;

        if (!isEmis) {
            String sigFormat = (String) typageProps.get(ParapheurModel.PROP_SIGNATURE_FORMAT);
            // Default, sign auto signature with PKCS#7
            if (sigFormat.equals("AUTO")) {
                // Find best signature format
                NodeRef emetteur = getFirstChild(dossierRef, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
                if(emetteur != null) {
                    boolean onlyPdf = true;
                    boolean hasPkcs7sig = false;
                    boolean hasXmlSig = false;
                    boolean hasDetachedSig = false;
                    List<NodeRef> documents = parapheurService.getMainDocuments(dossierRef);

                    for(NodeRef doc : documents) {
                        if(!hasXmlSig && !hasPkcs7sig) {
                            try {
                                Signature sig = signatureService.getDetachedSignature(doc.getId(), emetteur.getId());
                                if(sig != null) {
                                    hasDetachedSig = true;
                                    hasXmlSig = sig.getMediaType().getSubtype().equals("xml");
                                    hasPkcs7sig = sig.getMediaType().getSubtype().contains("pkcs7");
                                }
                            } catch(SignatureNotFoundException ex) {
                                // ignore
                            }
                        }
                        onlyPdf = onlyPdf && contentService.getReader(doc, ContentModel.PROP_CONTENT).getMimetype().equals(MimetypeMap.MIMETYPE_PDF);
                    }

                    boolean doAutoSelect = false;
                    String autoSelectFormat = onlyPdf && !hasDetachedSig ? unsignedPdfFormat : unsignedFormat;

                    if(hasXmlSig && !hasPkcs7sig) {
                        typageProps.put(ParapheurModel.PROP_SIGNATURE_FORMAT, ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_1_1);
                    } else if (hasPkcs7sig && !hasXmlSig) {
                        typageProps.put(ParapheurModel.PROP_SIGNATURE_FORMAT, ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7);
                    } else {
                        doAutoSelect = true;
                    }

                    if(doAutoSelect) {
                        switch(autoSelectFormat) {
                            case "xades":
                                typageProps.put(ParapheurModel.PROP_SIGNATURE_FORMAT, ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_1_1);
                                break;
                            case "pades":
                                typageProps.put(ParapheurModel.PROP_SIGNATURE_FORMAT, ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_PADES_ISO32000_1);
                                break;
                            case "pkcs7":
                                typageProps.put(ParapheurModel.PROP_SIGNATURE_FORMAT, ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7);
                                break;
                            default:
                                typageProps.put(ParapheurModel.PROP_SIGNATURE_FORMAT, ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7);
                        }
                    }

                } else {
                    typageProps.put(ParapheurModel.PROP_SIGNATURE_FORMAT, ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7);
                }
            }

            // Remove detached signature if attached signature next
            if(StringUtils.startsWithIgnoreCase(sigFormat, "pades") || sigFormat.equals(ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_ENV_PESv2)) {
                List<NodeRef> documents = parapheurService.getMainDocuments(dossierRef);

                documents.forEach(doc -> signatureService.deleteDetachedSignature(doc.getId()));
            }
            nodeService.addAspect(dossierRef, ParapheurModel.ASPECT_TYPAGE_METIER, typageProps);
            //TODO: add actes support (documentPrincipal pdf)
            /*
             s2lowActes = ("S²LOW".equals(typageProps.get(ParapheurModel.PROP_TDT_NOM))
             && "ACTES".equals(typageProps.get(ParapheurModel.PROP_TDT_PROTOCOLE)));

             */
            boolean isDigitalSignatureMandatory = typesService.isDigitalSignatureMandatory(type, sousType);
            boolean readingMandatory = typesService.isReadingMandatory(type, sousType);
            boolean includeAttachments = typesService.areAttachmentsIncluded(type, sousType);

            if (typesService.isTdtAuto(type, sousType)) {
                nodeService.addAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_TDT_AUTO, null);
            }
            if (typesService.isCachetAuto(type, sousType)) {
                nodeService.addAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_CACHET_AUTO, null);
            }
            // si le précédent sous-type prévoyait une étape auto, il faut l'enlever.
            else if (nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_TDT_AUTO)) {
                nodeService.removeAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_TDT_AUTO);
            }

            if (typesService.hasToAttestSignature(type, sousType)) {
                nodeService.addAspect(dossierRef, ParapheurModel.ASPECT_ETAPE_ATTEST, null);
            }

            nodeService.setProperty(dossierRef, ParapheurModel.PROP_DIGITAL_SIGNATURE_MANDATORY, isDigitalSignatureMandatory);
            /**
             * Si signature electronique obligatoire: on écrase l'éventuelle demande
             * de signature papier
             */
            //FIXME: FIX IT ! DEFAULTS TO FALSE FOR DEMO
            //TODO: READ THE FIXME !
            // if (isDigitalSignatureMandatory) {
            // }
            nodeService.setProperty(dossierRef, ParapheurModel.PROP_READING_MANDATORY, readingMandatory);
            nodeService.setProperty(dossierRef, ParapheurModel.PROP_INCLUDE_ATTACHMENTS, includeAttachments);

            try {
                updateWorkflow(dossierRef,
                        (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER),
                        (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SOUSTYPE_METIER));
            } catch (Exception e) {
                hasErrorCircuit = true;
                // Si les métadonnées ne sont toujours pas définies
                e.printStackTrace();
            }
        }

        if (((newConfidentiel != null) && (!newConfidentiel.equals(originalConfidentiel))) ||
                ((newPublic != null) && (!newPublic.equals(originalPublic)))) {
            setPermissionsDossier(dossierRef);
        }

        String res = "success";
        if (!isEmis) {
            res = finalizeCreateDossier(dossierRef);
        }

        notificationService.notifierPourEdition(dossierRef);

        if (res.equals("success") && hasErrorCircuit) {
            res = ("circuit INCONNU pour ["
                    + type + "][" + sousType
                    + "] et les meta-donnees fournies");
        }

        return res;
    }

    /**
     * Suppression de la liste de NodeRef
     *
     * @param filesToDelete Liste de NodeRef à supprimer
     */
    @Override
    public Map<String, String> deleteNodes(List<NodeRef> filesToDelete) {
        Map<String, String> ret = new HashMap<String, String>();
        String name;
        for (NodeRef nodeRef : filesToDelete) {
            name = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_TITLE);
            if (canDelete(nodeRef)) {
                if (parapheurService.isDossier(nodeRef)) {
                    deleteDossier(nodeRef, true);
                } else {
                    nodeService.deleteNode(nodeRef);
                }
                ret.put(name, "success");
            } else {
                //L'utilisateur n'a pas l'autorisation
                ret.put(name, "app.ajax.msg.autorisation");
            }
        }
        return ret;
    }

    @Override
    public List<NodeRef> getArchives(int count, int index, List<Predicate<?, ?>> filters, String propSort, boolean asc) {
        // Récupération du répertoire des archives
        String xpath = parapheurService.getConfiguration().getProperty("spaces.company_home.childname") + "/" + parapheurService.getConfiguration().getProperty("spaces.archives.childname");
        List<NodeRef> results = null;
        results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(parapheurService.getConfiguration().getProperty("spaces.store"))), xpath, null, namespaceService, false);
        if (results == null || results.size() != 1) {
            throw new RuntimeException("Il n'y a pas de dossier \"Archives\"");
        }
        NodeRef parent = results.get(0);

        CakeFilter filter = new CakeFilter();

        filter.setType("ph:archive");
        filter.setSearchPath(null);
        if (parent != null) {
            filter.setParent(parent);
        }
        //filter.setParent;

        filter.setClause(new Predicate<String, List<Predicate<?, ?>>>("and", filters));

        String query = filter.buildFilterQuery();
        /* SearchParameters Wizardry */
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        searchParameters.setQuery(query);

        /* permission check ? eager search */
        searchParameters.setMaxPermissionChecks(0);

        //SORTING
        searchParameters.addSort("@" + propSort, asc);
        //Si classement sur le type, on ajoute un classement sur le sous-type également
        if (propSort.equals("ph:typeMetier")) {
            searchParameters.addSort("@ph:soustypeMetier", asc);
        }

        /* if count is not equal to 0 then we limit the result set and apply index */
        if (count > 0) {
            searchParameters.setLimitBy(LimitBy.FINAL_SIZE);
            searchParameters.setLimit(count + 1);
            searchParameters.setMaxItems(count + 1);
            searchParameters.setSkipCount(index);
        }

        ResultSet resultSet = null;
        List<NodeRef> refs = null;
        try {
            resultSet = searchService.query(searchParameters);
            refs = resultSet.getNodeRefs();
        } catch (IllegalArgumentException e) {
            //e.printStackTrace();
            return null;

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        if (refs.isEmpty()) {
            return null;
        }
        return refs;
    }

    @Override
    public List<NodeRef> searchFolder(List<Predicate<?, ?>> filters) {
        CakeFilter filter = new CakeFilter();

        filter.setType("ph:dossier");
        filter.setSearchPath(null);

        filter.setClause(new Predicate<String, List<Predicate<?, ?>>>("and", filters));

        String query = filter.buildFilterQuery();

        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.setQuery(query);
        searchParameters.addStore(Repository.getStoreRef());

        ResultSet resultSet = null;
        List<NodeRef> refs = null;
        try {
            resultSet = searchService.query(searchParameters);
            refs = resultSet.getNodeRefs();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return refs;
    }

    private List<NodeRef> handleContentSearch(SearchParameters params) {
        ResultSet queryResults = searchService.query(params);

        List<NodeRef> results = new ArrayList<NodeRef>();

        List<NodeRef> resultsRef = null;
        try {
            resultsRef = queryResults.getNodeRefs();
        } finally {
            if (resultsRef == null) return null;
        }
        for (NodeRef resultRef : resultsRef) {
            /**
             * Transforms raw results into valid results. For example, documents
             * aren't directly displayed, dossiers containing them are displayed
             */
            NodeRef validResult = null;
            QName resultType = nodeService.getType(resultRef);
            if (ParapheurModel.TYPE_DOSSIER.equals(resultType)) {
                validResult = resultRef;
            } else if (ContentModel.TYPE_CONTENT.equals(resultType)) {
                NodeRef dossier = parapheurService.getParentDossier(resultRef);
                if (dossier != null) {
                    validResult = dossier;
                }
            }
            if (validResult != null) {
                results.add(validResult);
            }
        }
        return results;
    }

    private List<NodeRef> getDossiersDelegues(NodeRef bureauRef, int count, int index, List<Predicate<?, ?>> filters, String propSort, boolean asc) {
        List<NodeRef> result = corbeillesService.getDossiersDelegues(bureauRef);

        List<NodeRef> filtrateNodes = new ArrayList<NodeRef>();
        List<NodeRef> filtratePendingNodes = new ArrayList<NodeRef>();
        for (NodeRef nodeRef : result) {
            boolean isOK = true;
            if (filters != null) {
                for (Predicate<?, ?> and : filters) {
                    boolean passAnd = true;
                    for (Predicate or : (ArrayList<Predicate>) and.second()) {
                        boolean passOr = false;
                        if (or.second() instanceof ArrayList) {
                            for (Predicate filter : (ArrayList<Predicate>) or.second()) {
                                QName prop = (QName) filter.first();
                                Object value = (Object) filter.second();
                                Object nodeValue = nodeService.getProperty(nodeRef, prop);
                                if (prop.equals(ContentModel.PROP_NAME) || prop.equals(ContentModel.PROP_TITLE)) {
                                    passOr = passOr || (nodeValue != null && ((String) nodeValue).toLowerCase().contains(((String) value).replace("*", "").toLowerCase()));
                                } else {
                                    passOr = passOr || (nodeValue != null && nodeValue.equals(value));
                                }
                                if (passOr) {
                                    break;
                                }
                            }
                        } else if (or.second() instanceof String) {
                            QName prop = (QName) or.first();
                            String value = (String) or.second();
                            Calendar cal = Calendar.getInstance();

                            Date from = null;
                            Date to = null;

                            String[] dateStr = value.split(" ")[1].split("-");
                            if (dateStr.length == 3) {
                                cal.set(Integer.decode(dateStr[0]), Integer.decode(dateStr[1]) - 1, Integer.decode(dateStr[2]), 0, 0); //year is as expected, month is zero based, date is as expected
                                from = cal.getTime();
                            }

                            dateStr = value.split(" ")[3].split("-");
                            if (dateStr.length == 3) {
                                cal.set(Integer.decode(dateStr[0]), Integer.decode(dateStr[1]) - 1, Integer.decode(dateStr[2]), 23, 59); //year is as expected, month is zero based, date is as expected
                                to = cal.getTime();
                            }

                            Date nodeValue = (Date) nodeService.getProperty(nodeRef, prop);
                            boolean after = from == null ? true : nodeValue.after(from);
                            boolean before = to == null ? true : nodeValue.before(to);

                            passOr = passOr || (nodeValue != null && (after && before));
                        }
                        passAnd = passAnd && passOr;
                        if (!passAnd) {
                            break;
                        }
                    }
                    isOK = isOK && passAnd;
                }
            }
            if (isOK) {
                //Si l'aspect est présent, mis à part sur une autre liste
                if (nodeService.hasAspect(nodeRef, ParapheurModel.ASPECT_PENDING)) {
                    filtratePendingNodes.add(nodeRef);
                } else {
                    filtrateNodes.add(nodeRef);
                }
            }// AJOUT DU DOSSIER
        }
        //Tri des liste pending et non-pending
        Collections.sort(filtrateNodes, new NodeComparator(asc, QName.createQName(propSort, namespaceService)));
        Collections.sort(filtratePendingNodes, new NodeComparator(asc, QName.createQName(propSort, namespaceService)));

        //Ajout de la liste pending à la fin de la liste des dossiers
        filtrateNodes.addAll(filtratePendingNodes);

        int maxSize = filtrateNodes.size() < index + count + 1 ? filtrateNodes.size() : index + count + 1;
        int min = Math.min(index, maxSize);
        int max = Math.min(Math.max(index, maxSize), filtrateNodes.size());
        filtrateNodes = filtrateNodes.subList(min, max);
        if (filtrateNodes.isEmpty()) return null;
        return filtrateNodes;
    }

    @Override
    public List<NodeRef> getDossiers(NodeRef bureauRef, String parentName, int count, int index, List<Predicate<?, ?>> filters, String propSort, boolean asc, int numPage, int pendingFile) {
        if (!parapheurService.showAVenir(bureauRef) && ("a-venir".equals(parentName))) {
            throw new RuntimeException(String.format("La corbeille %s n'est pas consultable sur ce bureau.", parentName));
        }
        CakeFilter filter = new CakeFilter();

        NodeRef parent = null;
        filter.setSearchPath(null);

        Map<String, QName> corbeilles = new HashMap<String, QName>();

        corbeilles.put(CorbeillesService.EN_PREPARATION, ParapheurModel.NAME_EN_PREPARATION);
        corbeilles.put(CorbeillesService.A_TRAITER, ParapheurModel.NAME_A_TRAITER);
        corbeilles.put(CorbeillesService.A_ARCHIVER, ParapheurModel.NAME_A_ARCHIVER);
        corbeilles.put(CorbeillesService.RETOURNES, ParapheurModel.NAME_RETOURNES);
        corbeilles.put(CorbeillesService.EN_COURS, ParapheurModel.NAME_EN_COURS);
        corbeilles.put(CorbeillesService.TRAITES, ParapheurModel.NAME_TRAITES);
        corbeilles.put(CorbeillesService.A_VENIR, ParapheurModel.NAME_A_VENIR);
        corbeilles.put(CorbeillesService.DELEGUES, ParapheurModel.NAME_DOSSIERS_DELEGUES);
        corbeilles.put(CorbeillesService.RECUPERABLES, ParapheurModel.NAME_RECUPERABLES);
        corbeilles.put(CorbeillesService.SECRETARIAT, ParapheurModel.NAME_SECRETARIAT);
        corbeilles.put(CorbeillesService.EN_RETARD, ParapheurModel.NAME_EN_RETARD);
        corbeilles.put(CorbeillesService.A_IMPRIMER, ParapheurModel.NAME_A_IMPRIMER);

        if (parentName.equals("no-corbeille")) {
            HashMap<NodeRef, QName> parents = new HashMap<NodeRef, QName>();
            for (Map.Entry<String, QName> entry : corbeilles.entrySet()) {
                String string = entry.getKey();
                QName qName = entry.getValue();
                NodeRef p = parapheurService.getCorbeille(bureauRef, qName);
                parents.put(p, nodeService.getType(p));
            }
            filter.setParents(parents);
        } else if (parentName.equals("content")) {
            // C'est moche... Mais ce sont les filtres qui sont moches !
            String searchcontent = (String) ((Predicate) ((ArrayList) ((Predicate) ((ArrayList) ((Predicate) filters.toArray()[0]).second()).get(0)).second()).get(0)).second();

            SearchContext context = new SearchContext();

            context.setText(searchcontent);
            context.setContentType("cm:content");
            context.setFolderType("ph:dossier");

            SearchParameters params = new SearchParameters();
            params.setLanguage(SearchService.LANGUAGE_LUCENE);
            params.setQuery("TYPE:\"cm:content\" AND TEXT:\"" + searchcontent + "\"");
            params.addStore(Repository.getStoreRef());
            params.setMaxPermissionChecks(Integer.MAX_VALUE);
            params.setMaxPermissionCheckTimeMillis(Long.MAX_VALUE);

            List<NodeRef> result = handleContentSearch(params);
            List<NodeRef> filtrateNodes = new ArrayList<NodeRef>();
            List<NodeRef> filtratePendingNodes = new ArrayList<NodeRef>();
            for (NodeRef nodeRef : result) {
                //Si l'aspect est présent, mis à part sur une autre liste
                if (nodeService.hasAspect(nodeRef, ParapheurModel.ASPECT_PENDING)) {
                    filtratePendingNodes.add(nodeRef);
                } else {
                    filtrateNodes.add(nodeRef);
                }
            }
            //Tri des liste pending et non-pending
            Collections.sort(filtrateNodes, new NodeComparator(asc, QName.createQName(propSort, namespaceService)));
            Collections.sort(filtratePendingNodes, new NodeComparator(asc, QName.createQName(propSort, namespaceService)));

            //Ajout de la liste pending à la fin de la liste des dossiers
            filtrateNodes.addAll(filtratePendingNodes);

            int maxSize = filtrateNodes.size() < index + count + 1 ? filtrateNodes.size() : index + count + 1;
            int min = Math.min(index, maxSize);
            int max = Math.min(Math.max(index, maxSize), filtrateNodes.size());
            filtrateNodes = filtrateNodes.subList(min, max);
            if (filtrateNodes.isEmpty()) return null;
            return filtrateNodes;
        } else if (parentName.equals("dossiers-delegues")) {
            return getDossiersDelegues(bureauRef, count, index, filters, propSort, asc);
        } else if (!parentName.equals("no-bureau")) {

            QName parentQName = corbeilles.get(parentName);

            parent = parapheurService.getCorbeille(bureauRef, parentQName);

            if (parent == null) {
                throw new RuntimeException(String.format("La corbeille %s est inconnue.", parentName));
            }
        }

        filter.setType("ph:dossier");
        if (parent != null) {
            filter.setParent(parent);
            filter.setParentType(nodeService.getType(parent));
        }
        //filter.setParent;

        filter.setClause(new Predicate<String, List<Predicate<?, ?>>>("and", filters));
        filter.setAspect("ph:pending", false);

        String query = filter.buildFilterQuery();
        /* SearchParameters Wizardry */
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        searchParameters.setQuery(query);

        //SORTING
        searchParameters.addSort("@" + propSort, asc);

        //Si classement sur le type, on ajoute un classement sur le sous-type également
        if (propSort.equals("ph:typeMetier")) {
            // Hmm... to make ascendant sorting with lucene, we have to invert it... Because reasons...
            searchParameters.addSort("@ph:soustypeMetier", asc);
        }

        /* permission check ? eager search */
        searchParameters.setMaxPermissionChecks(0);

        /* if count is not equal to 0 then we limit the result set and apply index */
        if (count > 0) {
            searchParameters.setLimitBy(LimitBy.FINAL_SIZE);
            searchParameters.setLimit(count);
            searchParameters.setMaxItems(count);
            searchParameters.setSkipCount(index);
        }

        ResultSet resultSet = null;
        List<NodeRef> refs = null;
        try {
            resultSet = searchService.query(searchParameters);
            refs = resultSet.getNodeRefs();
        } catch (IllegalArgumentException e) {
            //e.printStackTrace();
            refs = new ArrayList<NodeRef>();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }

        if (refs.size() < count) {
            //TODO : Handle Pending folders
            refs.addAll(getDossiersPending(count - refs.size(), pendingFile, filter, searchParameters));
        }

        return refs;
    }

    private List<NodeRef> getDossiersPending(int count, int pendingFile, CakeFilter filter, SearchParameters searchParameters) {
        List<NodeRef> refs = new ArrayList<NodeRef>();
        String query;
        ResultSet resultSet;

        filter.setAspect("ph:pending", true);
        query = filter.buildFilterQuery();

        searchParameters.setQuery(query);

        /* if count is not equal to 0 then we limit the result set and apply index */
        if (count > 0) {
            searchParameters.setLimitBy(LimitBy.FINAL_SIZE);
            searchParameters.setLimit(count);
            searchParameters.setMaxItems(count);
            searchParameters.setSkipCount(pendingFile);
        }

        resultSet = null;
        try {
            resultSet = searchService.query(searchParameters);
            refs.addAll(resultSet.getNodeRefs());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return refs;
    }

    private List<NodeRef> getDossiersTraites(NodeRef bureauRef, int count, int index, List<Predicate<?, ?>> filters, String propSort, boolean asc) {
        List<NodeRef> dossiersTraites = new ArrayList<NodeRef>();
        //TODO traitement
        return dossiersTraites;
    }

    /**
     * Archive un dossier avec les annexes passées en paramètre.
     *
     * @param fileToArchive le dossier à archiver
     * @param _titreArchive le titre à donner à l'archive
     * @param _annexes      les annexes à inclure
     * @return "success" si l'archivage a réussi.
     */
    @Override
    public String archiveDossier(NodeRef fileToArchive, String _titreArchive, List<NodeRef> _annexes) {
        final List<NodeRef> filesToArchive = new ArrayList<NodeRef>();
        final String titreArchive = _titreArchive;
        final List<NodeRef> annexes = _annexes;
        filesToArchive.add(fileToArchive);
        AbstractJob archivageJob = new AbstractBatchJob() {
            @Override
            public Void doWorkUnitInTransaction(NodeRef dossier) {
                //System.out.println(getUniqueArchiveNameForNodeRef(dossier));
                try {
                    parapheurService.archiver(dossier, titreArchive, annexes);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };

        // desactivate auto unlocking of handled NodeRefs.
        archivageJob.setShallUnlock(false);
        jobService.postJobAndLockNodes(archivageJob, filesToArchive);
        try {
        } catch (Exception e) {
            return e.getMessage();
        }
        return "success";
    }

    /**
     * Archivage de dossier(s)
     *
     * @param _filesToArchive    Liste des dossiers à archiver
     * @param titresArchive      les titres des archives
     * @param bureauCourant      Bureau courant de l'utilisateur
     * @param includeAttachments Pour chaque dossier, indique si on doit inclure
     *                           les annexes
     */
    @Override
    public Map<String, String> archiveDossier(List<NodeRef> _filesToArchive, List<String> titresArchive, NodeRef bureauCourant, List<Boolean> includeAttachments) {
        final Map<String, String> ret = new HashMap<String, String>();
        final List<NodeRef> filesToArchive = new ArrayList<NodeRef>();
        final List<NodeRef> fichiers = _filesToArchive;
        final List<String> noms = titresArchive;
        final List<Boolean> includes = includeAttachments;
        String name;
        for (NodeRef nodeRef : _filesToArchive) {
            name = titresArchive.get(_filesToArchive.indexOf(nodeRef));
            if (canArchive(nodeRef, bureauCourant)) {
                if (archiveExist(name)) {
                    ret.put(name, "app.ajax.msg.fileExist");
                } else {
                    filesToArchive.add(nodeRef);
                    ret.put(name, "success");
                }
            } else {
                //L'utilisateur n'a pas l'autorisation
                ret.put(name, "app.ajax.msg.autorisation");
            }
        }
        AbstractJob archivageJob = new AbstractBatchJob() {
            @Override
            public Void doWorkUnitInTransaction(NodeRef dossier) {
                //System.out.println(getUniqueArchiveNameForNodeRef(dossier));
                int index = fichiers.indexOf(dossier);
                List<NodeRef> attachments = null;
                if (includes.get(index)) {
                    attachments = parapheurService.getAttachments(dossier);
                }
                try {
                    parapheurService.archiver(dossier, noms.get(index), attachments);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };

        // desactivate auto unlocking of handled NodeRefs.
        archivageJob.setShallUnlock(false);
        jobService.postJobAndLockNodes(archivageJob, filesToArchive);

//        corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(bureauCourant, ParapheurModel.NAME_A_ARCHIVER));

        return ret;
    }

    /**
     * Récupération de dossier(s)
     *
     * @param _filesToRemorse Liste des dossiers à récupérer
     * @param bureauCourant   Bureau courant de l'utilisateur
     */
    @Override
    public Map<String, String> remorseDossier(List<NodeRef> _filesToRemorse, NodeRef bureauCourant) {
        Map<String, String> ret = new HashMap<String, String>();
        List<NodeRef> filesToRemorse = new ArrayList<NodeRef>();

        String name;

        for (NodeRef nodeRef : _filesToRemorse) {
            name = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_TITLE);
            if (canRemorse(nodeRef, bureauCourant)) {
                filesToRemorse.add(nodeRef);
                ret.put(name, "success");
            } else {
                //L'utilisateur n'a pas l'autorisation
                ret.put(name, "app.ajax.msg.autorisation");
            }
        }

        jobService.postJobAndLockNodes(new AbstractBatchJob() {
            @Override
            protected Void doWorkUnitInTransaction(NodeRef dossier) {
                parapheurService.recupererDossier(dossier);
                if (jobService.isBackgroundWorkEnabled()) {
                    corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurService.getParentParapheur(dossier), ParapheurModel.NAME_A_TRAITER));
                }
                return null;
            }
        }, filesToRemorse);
        return ret;
    }

    /**
     * Remise à zero de dossier(s)
     *
     * @param _filesToRAZ   Liste de dossier à remettre à zero
     * @param bureauCourant Bureau courant de l'utilisateur
     */
    @Override
    public Map<String, String> RAZDossier(List<NodeRef> _filesToRAZ, NodeRef bureauCourant) {
        Map<String, String> ret = new HashMap<String, String>();
        List<NodeRef> filesToRAZ = new ArrayList<NodeRef>();

        String name;

        for (NodeRef nodeRef : _filesToRAZ) {
            name = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_TITLE);
            if (canRAZ(nodeRef, bureauCourant)) {
                filesToRAZ.add(nodeRef);
                ret.put(name, "success");
            } else {
                //L'utilisateur n'a pas l'autorisation
                ret.put(name, "app.ajax.msg.autorisation");
            }
        }

        jobService.postJobAndLockNodes(new AbstractBatchJob() {
            @Override
            protected Void doWorkUnitInTransaction(NodeRef dossier) {
                parapheurService.reprendreDossier(dossier);
                if (jobService.isBackgroundWorkEnabled()) {
                    corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurService.getParentParapheur(dossier), ParapheurModel.NAME_EN_PREPARATION));
                }
                return null;
            }
        }, filesToRAZ);
        return ret;
    }

    /**
     * Rejet de dossier(s)
     *
     * @param _filesToReject Liste de dossier à rejeter
     * @param pub            Annotation publique
     * @param priv           Annotation privée
     * @param bureauCourant  Bureau courant de l'utilisateur
     */
    @Override
    public Map<String, String> rejectDossier(List<NodeRef> _filesToReject, String pub, String priv, NodeRef bureauCourant) {
        Map<String, String> ret = new HashMap<String, String>();
        final String fPriv = priv;
        final String fPub = pub;

        String name;

        List<NodeRef> filesToReject = new ArrayList<NodeRef>();

        for (NodeRef nodeRef : _filesToReject) {
            name = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_TITLE);
            if (canReject(nodeRef, bureauCourant)) {
                if (!pub.isEmpty()) {
                    filesToReject.add(nodeRef);
                    ret.put(name, "success");
                } else {
                    //AnnotPub is mandatory
                    ret.put(name, "app.ajax.msg.annotpub");
                }
            } else {
                //L'utilisateur n'a pas l'autorisation
                ret.put(name, "app.ajax.msg.autorisation");
            }
        }

        AbstractJob rejectJob = new AbstractBatchJob() {
            @Override
            protected Void doWorkUnitInTransaction(NodeRef dossier) {
                parapheurService.setSignataire(dossier,
                        parapheurService.getPrenomNomFromLogin(
                                AuthenticationUtil.getRunAsUser()));

                parapheurService.setAnnotationPrivee(dossier, fPriv);
                parapheurService.setAnnotationPublique(dossier, fPub);
                parapheurService.reject(dossier);
                jobService.unlockNode(dossier);
                corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurService.getEmetteur(dossier), ParapheurModel.NAME_RETOURNES));
                return null;
            }
        };

        // desactivate auto unlocking of handled NodeRefs.
        rejectJob.setShallUnlock(false);
        jobService.postJobAndLockNodes(rejectJob, filesToReject);

        return ret;
    }

    /**
     * Handles batch sign
     *
     * @param _filesToSign
     * @param pub
     * @param priv
     * @param bureauCourant
     * @return
     */
    @Override
    public Map<String, String> signerDossier(List<NodeRef> _filesToSign, String pub, String priv, NodeRef bureauCourant, List<String> signature) throws Exception {
        Map<String, String> ret = new HashMap<String, String>();
        String name;
        final String fPriv = priv;
        final String fPub = pub;

        final NodeRef courant = bureauCourant;

        List<NodeRef> filesToSign = new ArrayList<NodeRef>();
        //Utilisation d'une Queue pour un accès des signatures dans le jobService simplifié
        final Queue signatures = new LinkedList();

        for (int i = 0; i < _filesToSign.size(); i++) {
            NodeRef nodeRef = _filesToSign.get(i);
            name = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_TITLE);
            if (canSign(nodeRef, bureauCourant)) {
                filesToSign.add(nodeRef);
                signatures.offer(signature.get(i));
                ret.put(name, "success");
            } else {
                //L'utilisateur n'a pas l'autorisation
                ret.put(name, "app.ajax.msg.autorisation");
            }
        }

        jobService.postJobAndLockNodes(new AbstractBatchJob() {
            @Override
            protected Void doWorkUnitInTransaction(NodeRef dossier) {
                SignatureInformations sinfo = new SignatureInformations(dossier, fPub, fPriv, courant);
                try {
                    sinfo.sign((String) signatures.poll());
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(DossierServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (jobService.isBackgroundWorkEnabled()) {
                    corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurService.getParentParapheur(dossier), ParapheurModel.NAME_RETOURNES));
                }
                return null;
            }
        }, filesToSign);

        return ret;
    }

    @Override
    public Map<String, String> secretariat(List<NodeRef> _filesToSec, String pub, String priv, NodeRef bureauCourant) {
        Map<String, String> ret = new HashMap<String, String>();
        String name;

        final String fPriv = priv;
        final String fPub = pub;
        final NodeRef parapheurCourant = bureauCourant;

        List<NodeRef> filesToSec = new ArrayList<NodeRef>();
        for (NodeRef nodeRef : _filesToSec) {
            name = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_TITLE);
            if (canSecretariat(nodeRef, bureauCourant)) {
                filesToSec.add(nodeRef);
                ret.put(name, "success");
            } else {
                //L'utilisateur n'a pas l'autorisation
                ret.put(name, "app.ajax.msg.autorisation");
            }
        }

        AbstractJob secretariatJob = new AbstractBatchJob() {
            @Override
            public Void doWorkUnitInTransaction(NodeRef dossier) {
                parapheurService.setSignataire(dossier,
                        parapheurService.getPrenomNomFromLogin(
                                AuthenticationUtil.getRunAsUser()));
                parapheurService.setAnnotationPrivee(dossier, fPriv);
                parapheurService.setAnnotationPublique(dossier, fPub);
                parapheurService.secretariat(dossier);
                //Unlock du NodeRef pour forcer le rafraichissement du compte des corbeilles
                jobService.unlockNode(dossier);
                corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurCourant, ParapheurModel.NAME_SECRETARIAT));
                corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurCourant, ParapheurModel.NAME_A_TRAITER));
                return null;
            }
        };

        // desactivate auto unlocking of handled NodeRefs.
        secretariatJob.setShallUnlock(false);
        jobService.postJobAndLockNodes(secretariatJob, filesToSec);

        return ret;
    }

    /**
     * Handles batch visa
     *
     * @param _filesToVisa
     * @param _pub
     * @param _priv
     * @throws Exception
     */
    @Override
    public Map<String, String> visaDossier(List<NodeRef> _filesToVisa, String _pub, String _priv, NodeRef bureauCourant, boolean _consecutiveSteps) {

        Map<String, String> ret = new HashMap<String, String>();
        final NodeRef parapheurCourant = bureauCourant;
        String name;
        List<NodeRef> filesToVisa = new ArrayList<NodeRef>();
        if (!_filesToVisa.isEmpty()) {
            for (NodeRef nodeRef : _filesToVisa) {
                //On vérifie si le dossier est complet
                name = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_TITLE);
                String msg = "success";

                //Si il peut être signé on l'ajoute à la liste, sinon, on lâche un message d'erreur
                if (canSign(nodeRef, bureauCourant)) {
                    filesToVisa.add(nodeRef);
                } else { //L'utilisateur n'a pas l'autorisation
                    msg = "app.ajax.msg.autorisation";
                }
                ret.put(name, msg);
            }

            String actionD = "";

            if (_filesToVisa.size() == 1) {
                List<EtapeCircuit> circuit = parapheurService.getCircuit(_filesToVisa.get(0));
                // Récupération de la prochaine étape
                EtapeCircuitImpl etapeCourante = null;
                for (EtapeCircuit etape : circuit) {
                    if (!etape.isApproved()) {
                        if (etapeCourante == null) {
                            etapeCourante = (EtapeCircuitImpl) etape;
                        }
                    }
                }
                assert etapeCourante != null;
                actionD = etapeCourante.getActionDemandee();
            }

            final String actionDemandee = actionD;
            final String fullname = parapheurService.getPrenomNomFromLogin(AuthenticationUtil.getRunAsUser());
            final boolean consecutiveSteps = _consecutiveSteps;
            final String pub = _pub;
            final String priv = _priv;
            jobService.postJobAndLockNodes(new AbstractBatchJob() {
                @Override
                protected Void doWorkUnitInTransaction(NodeRef dossier) {
                    UserTransaction ut = transactionService.getNonPropagatingUserTransaction();
                    try {
                        ut.begin();

                        if (!isEmis(dossier)) {
                            finalizeDossier(dossier);
                        }

                        int nbSteps = (consecutiveSteps) ? getConsecutiveSteps(dossier, username).size() : 1;
                        for (int i = 0; i < nbSteps; i++) {
                            if (i == (nbSteps - 1)) { // We put the annotation only on the last step
                                if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC) && !nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_STATUS).equals(S2lowServiceImpl.STATUS_MAILSEC_FULLY_CONFIRMED)) {
                                    Integer mail_id = (Integer) nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID);

                                    S2lowServiceImpl.SecureMailDetail detail = null;
                                    try {
                                        detail = s2lowService.getSecureMailDetail(mail_id);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    String mailsecDigest = S2lowServiceImpl.getAnnotation(detail);
                                    if (!pub.isEmpty()) mailsecDigest += "\n" + pub;
                                    parapheurService.setAnnotationPublique(dossier, mailsecDigest);
                                } else {
                                    parapheurService.setAnnotationPublique(dossier, pub);
                                    parapheurService.setAnnotationPrivee(dossier, priv);
                                }
                            }

                            // Le signataire enregistré doit être le propriétaire du parapheur (cas de signature papier)
                            NodeRef parapheurRef = parapheurService.getParentParapheur(dossier);

                            String signataire;
                            if (parapheurService.isParapheurSecretaire(parapheurRef, username)) {
                                //NodeRef ownerRef = this.personService.getPerson();
                                signataire = String.format("%s pour le compte de \"%s\"", fullname, parapheurService.getNomParapheur(parapheurRef));
                            } else if (parapheurService.isParapheurDelegate(parapheurRef, username)) {
                                signataire = String.format(configuration.getProperty("parapheur.suppleance.text"), fullname + ", " + parapheurService.getNomParapheur(bureauCourant), parapheurService.getNomParapheur(parapheurRef));
                            } else {
                                signataire = fullname;
                            }

                            parapheurService.setSignataire(dossier, signataire);

                            parapheurService.approveV4(dossier, parapheurCourant);

                        }
                        ut.commit();
                    } catch (Exception e) {
                        try {
                            ut.rollback();
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    }
                    return null;
                }
            }, filesToVisa);
            /*
             jobService.postJobAndLockNodes(new AbstractBatchJob() {
             @Override
             protected Void doWorkUnitInTransaction(NodeRef dossier) {

             return null;
             }
             }, filesToVisa);*/
        }
        return ret;
    }

    /**
     * Appelé lors d'un clic sur "Sauvegarder et envoyer", avant l'apparition de la popup du 1er visa
     * Si le dossier donné est complet, il est défini comme tel
     * (c) Perceval ?
     *
     * @param dossierRef Référence du dossier
     *                   appellante (voir visaDossier)
     */
    @Override
    public String finalizeCreateDossier(NodeRef dossierRef) {

        String ret = checkFinalize(dossierRef);

        boolean incomplete = !StringUtils.equalsIgnoreCase(ret, "success");
        nodeService.setProperty(dossierRef, ParapheurModel.PROP_UNCOMPLETE, incomplete);

        return ret;
    }

    /**
     * Finalise la création du dossier avant son émission.
     *
     * @param dossier Le NodeRef du dossier à traiter
     */
    @Override
    public void finalizeDossier(NodeRef dossier) {

        String type = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER);
        String subType = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER);
        List<NodeRef> mainDocuments = parapheurService.getMainDocuments(dossier);

        // Si le dossier est de type ACTES est n'a pas de pdf pour doc principal,
        // alors on place le visuel pdf en doc principal.

        Map<QName, Serializable> typageProps = parapheurService.getTypeMetierProperties(type);

        boolean actes = "ACTES".equals(typageProps.get(ParapheurModel.PROP_TDT_PROTOCOLE)) &&
                StringUtils.startsWithIgnoreCase((String) typageProps.get(ParapheurModel.PROP_SIGNATURE_FORMAT), "pades");

        if (actes) {
            for (NodeRef doc : mainDocuments) {
                saveContentAsPDF(doc);
            }
        }

        // If multiple documents have some "ph:mainDocument" metadata,
        // But the Type was changed to a single main-document one, we clean that.

        boolean isTypeMultiDoc = typesService.isMultiDocument(type, subType);
        if (!isTypeMultiDoc) {
            for (int i = 1; i < mainDocuments.size(); i++) {
                nodeService.removeAspect(mainDocuments.get(i), ParapheurModel.ASPECT_MAIN_DOCUMENT);
            }
        }
    }

    /**
     * Remplace le content du document par le visuel pdf si il y en a un (dans
     * le cas des documents non pdf donc).
     *
     * @param documentRef le document à modifier.
     */
    protected void saveContentAsPDF(NodeRef documentRef) {
        Serializable visuelPdf = nodeService.getProperty(documentRef, ParapheurModel.PROP_VISUEL_PDF);
        if (visuelPdf != null) {
            // Le doc principal n'est pas un pdf, on échange les propriété
            nodeService.setProperty(documentRef, ContentModel.PROP_CONTENT, visuelPdf);
            nodeService.removeProperty(documentRef, ParapheurModel.PROP_VISUEL_PDF);
        }
        String title = (String) nodeService.getProperty(documentRef, ContentModel.PROP_NAME);
        if (!title.endsWith(".pdf")) {
            title += ".pdf";
            nodeService.setProperty(documentRef, ContentModel.PROP_NAME, title);
        }
    }

    //----------------------------------------------------------\\
    //---               VERIFICATION ACTIONS                 ---\\
    //----------------------------------------------------------\\
    @Override
    public boolean canEdit(NodeRef dossierRef, NodeRef bureauCourant) {
        boolean res = false;
        if (canTest(dossierRef)) {
            NodeRef parapheur = parapheurService.getParentParapheur(dossierRef);
            String username = AuthenticationUtil.getRunAsUser();

            if (!isEmis(dossierRef) &&
                    (parapheur.equals(bureauCourant) || parapheurService.isParapheurDelegate(parapheur, bureauCourant))) {

                QName corbeilleQName = nodeService.getPrimaryParent(parapheurService.getParentCorbeille(dossierRef)).getQName();

                if (ParapheurModel.NAME_SECRETARIAT.equals(corbeilleQName)) {
                    res = parapheurService.isParapheurSecretaire(parapheur, username) &&
                            parapheurService.isParapheurSecretaire(parapheur, (String) nodeService.getProperty(dossierRef, ContentModel.PROP_CREATOR));
                } else if (ParapheurModel.NAME_EN_PREPARATION.equals(corbeilleQName)) {
                    res = parapheurService.isParapheurOwnerOrDelegate(parapheur, username);
                }
            }
        }
        return res;
    }

    @Override
    public boolean canDelete(NodeRef ref) {
        boolean res = false;

        if (nodeService.exists(ref)) {
            //Si c'est un dossier
            if (parapheurService.isDossier(ref)) {
                NodeRef corbeille = parapheurService.getParentCorbeille(ref);
                NodeRef parapheur = parapheurService.getParentParapheur(ref);
                String username = AuthenticationUtil.getRunAsUser();

                //Si il n'est pas emis et que l'utilisateur est le responsable
                //OU que l'utilisateur est secretaire et à créé le dossier en question
                if ((!isEmis(ref) && (parapheurService.isParapheurOwnerOrDelegate(parapheur, username)
                        || (parapheurService.isParapheurSecretaire(parapheur, username) && !parapheurService.isParapheurOwnerOrDelegate(parapheur, (String) nodeService.getProperty(ref, ContentModel.PROP_CREATOR)))))
                        || (parapheurService.isTermine(ref) && ParapheurModel.NAME_RETOURNES.equals(nodeService.getPrimaryParent(corbeille).getQName()))) {
                    if (!nodeService.hasAspect(ref, ParapheurModel.ASPECT_SECRETARIAT)) {
                        res = true;
                    }
                }
            } //Sinon
            else {
                NodeRef dossier = parapheurService.getParentDossier(ref);
                NodeRef corbeille = parapheurService.getParentCorbeille(dossier);
                NodeRef parapheur = parapheurService.getParentParapheur(dossier);
                NodeRef aTransmettre = parapheurService.getCorbeille(parapheur, ParapheurModel.NAME_EN_PREPARATION);
                String user = AuthenticationUtil.getRunAsUser();

                if (dossier != null && corbeille != null && parapheur != null) {
                    if (corbeille.equals(aTransmettre)
                            && (parapheurService.isParapheurOwnerOrDelegate(parapheur, user)
                            || (parapheurService.isParapheurSecretaire(parapheur, user) && !parapheurService.isParapheurOwnerOrDelegate(parapheur, (String) nodeService.getProperty(ref, ContentModel.PROP_CREATOR))))) {
                        res = true;
                        //Si document annexe
                        /*
                         List<ChildAssociationRef> children = nodeService.getChildAssocs(dossier, ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
                         if (!children.get(0).getChildRef().equals(ref)) {
                         res = true;
                         }*/
                    }
                }
            }
        }
        return res;
    }

    @Override
    public boolean canRemorse(NodeRef ref, NodeRef bureauCourant) {
        boolean res = false;
        if (canTest(ref)) {
            if (!nodeService.hasAspect(ref, ParapheurModel.ASPECT_SECRETARIAT)
                    && parapheurService.isRecuperable(ref)
                    && parapheurService.getPreviousEtapeCircuit(ref) != null
                    && parapheurService.getPreviousEtapeCircuit(ref).getActionDemandee().equals("VISA")) {
                List<EtapeCircuit> circuit = null;
                // cas d'un node créé par Alfresco et non par le code i-parapheur: le nodePropertyResolver n'existe pas
                circuit = parapheurService.getCircuit(ref);
                EtapeCircuit derniereEtape = null;
                for (EtapeCircuit etape : circuit) {
                    if (!etape.isApproved()) {
                        break;
                    } else {
                        derniereEtape = etape;
                    }
                }

                if (derniereEtape != null) {
                    NodeRef bureauPrec = derniereEtape.getParapheur();
                    NodeRef bureauDelegue = derniereEtape.getDelegue();
                    if (bureauPrec.equals(bureauCourant)
                            || ((bureauDelegue != null) && bureauDelegue.equals(bureauCourant))) {

                        res = parapheurService.isParapheurOwnerOrDelegate(derniereEtape.getParapheur(),
                                AuthenticationUtil.getRunAsUser());
                    }
                }
            }
        }
        return res;
    }

    @Override
    public boolean canReject(NodeRef ref, NodeRef bureauCourant) {
        boolean res = false;

        if (canTest(ref)) {
            //        List<EtapeCircuit> circuit = (List<EtapeCircuit>) node.getProperties().get("circuit");

            // BLEX if (nodeService.hasAspect(ref, ParapheurModel.ASPECT_S2LOW)){
            boolean hasAspectTdtTransaction =
                    nodeService.hasAspect(ref, ParapheurModel.ASPECT_S2LOW)
                            || nodeService.hasAspect(ref, SrciService.K.aspect_srciTransaction);
            if (hasAspectTdtTransaction) {
                List<EtapeCircuit> circuit = null;
                // cas d'un node créé par Alfresco et non par le code i-parapheur: le nodePropertyResolver n'existe pas
                circuit = parapheurService.getCircuit(ref);
                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(circuit);
                if (etape != null && etape.getActionDemandee().equals(EtapeCircuit.ETAPE_TDT)) {
                    return false;
                }
            }

            String username = AuthenticationUtil.getRunAsUser();
            NodeRef parapheur = parapheurService.getParentParapheur(ref);

            if (parapheur.equals(bureauCourant) || parapheurService.isParapheurDelegate(parapheur, bureauCourant)) {
                if (parapheurService.isParapheurOwnerOrDelegate(parapheur, username)) {
                    if (!nodeService.hasAspect(ref, ParapheurModel.ASPECT_SECRETARIAT)) {
                        if (!parapheurService.isTermine(ref) && isEmis(ref)) {
                            if (!nodeService.hasAspect(parapheurService.getCurrentEtapeCircuit(ref).getNodeRef(), ParapheurModel.ASPECT_ETAPE_COMPLEMENTAIRE)) {
                                res = true;
                            }
                        }
                    }
                }
            }
        }

        return res;
    }

    @Override
    public boolean canSecretariat(NodeRef ref, NodeRef bureauCourant) {
        boolean res = false;
        if (canTest(ref)) {
            boolean isInSecretariat = nodeService.hasAspect(ref, ParapheurModel.ASPECT_SECRETARIAT);
            String username = AuthenticationUtil.getRunAsUser();

            NodeRef parapheur = parapheurService.getParentParapheur(ref);

            boolean isSecretaire = false;
            boolean isOwner = false;
            if (!(isOwner = parapheurService.isParapheurOwnerOrDelegate(parapheur, username))) {
                isSecretaire = parapheurService.isParapheurSecretaire(parapheur, username);
            }
            if (parapheur.equals(bureauCourant) || parapheurService.isParapheurDelegate(parapheur, bureauCourant)) {
                if ((isOwner && !isInSecretariat) || (isSecretaire && isInSecretariat)) {
                    res = true;
                }
            }
        }
        return res;
    }

    //Différents test en fonction du type de signature (visa, emission, ...)
    @Override
    public boolean canSign(NodeRef ref, NodeRef bureauCourant) {
        boolean res = false;
        if (nodeService.getProperty(ref, ParapheurModel.PROP_SOUSTYPE_METIER) == null) {
            res = false;
        } else {
            boolean uncomplete = false;
            NodeRef parapheur = parapheurService.getParentParapheur(ref);
            String username = AuthenticationUtil.getRunAsUser();
            if (nodeService.getProperty(ref, ParapheurModel.PROP_UNCOMPLETE) != null) {
                uncomplete = (Boolean) nodeService.getProperty(ref, ParapheurModel.PROP_UNCOMPLETE);
            }
            if (uncomplete) {
                //if (parapheurService.isParapheurOwnerOrDelegate(parapheur, username)) {
                res = false;
                //}
            } else if (canTest(ref)) {
                List<EtapeCircuit> circuit = parapheurService.getCircuit(ref);

                //VISA, TDT ou MAILSEC
                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(circuit);
                if (etape == null) {
                    return false;
                }
                String currentStep = etape.getActionDemandee();

                if (parapheurService.isParapheurOwnerOrDelegate(parapheur, username) && (parapheur.equals(bureauCourant) || parapheurService.isParapheurDelegate(parapheur, bureauCourant))) {
                    //Cas VISA
                    if (currentStep.equals(EtapeCircuit.ETAPE_VISA)) {
                        Boolean signPapier = (Boolean) nodeService.getProperty(ref, ParapheurModel.PROP_SIGNATURE_PAPIER);
                        boolean derniereEtape = false;
                        if (!circuit.isEmpty()) {
                            if (!parapheurService.isTermine(ref)) {
                                if (circuit.size() > 1 && circuit.get(circuit.size() - 2).isApproved()) {
                                    derniereEtape = true;
                                }
                            }
                        }
                        // Dans le cas d'une dernière étape de signature papier, on passe sur une action spécialisée
                        if (!signPapier || !derniereEtape) {
                            if (parapheurService.isParapheurOwnerOrDelegate(parapheur, username)) {
                                if (!nodeService.hasAspect(ref, ParapheurModel.ASPECT_SECRETARIAT)) {
                                    if (!parapheurService.isTermine(ref)) {
                                        res = true;
                                    }
                                }
                            }
                        }
                    } //Cas TDT
                    else if (currentStep.equals(EtapeCircuit.ETAPE_TDT)) {
                        boolean hasAspectTdtTransaction =
                                nodeService.hasAspect(ref, ParapheurModel.ASPECT_S2LOW)
                                        || nodeService.hasAspect(ref, SrciService.K.aspect_srciTransaction);
                        if (hasAspectTdtTransaction
                                && currentStep.equals(EtapeCircuit.ETAPE_TDT)) {
                            res = false;
                        }
                    } //Cas MAILSEC
                    else if (currentStep.equals(EtapeCircuit.ETAPE_MAILSEC)) {
                        res = true;
                    } //Cas SIGNATURE
                    else if (currentStep.equals(EtapeCircuit.ETAPE_SIGNATURE)) {
                        if (hasReadDossier(ref, username)
                                || !parapheurService.isReadingMandatory(ref)) {
                            logger.debug("Aspect lu OK");
                            if (!circuit.isEmpty()) {
                                if (!parapheurService.isTermine(ref)) {
                                    logger.debug("Non-termine OK");
                                    if (circuit.get(0).isApproved()) {
                                        if (circuit.get(0).getActionDemandee() == null) {
                                            // compatibilité ascendante
                                            if (circuit.get(circuit.size() - 2).isApproved()) {
                                                logger.debug("Derniere etape OK");
                                                res = true;
                                            }
                                        } else {
                                            res = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return res;
    }

    @Override
    public boolean canRAZ(NodeRef ref, NodeRef bureauCourant) {
        boolean res = false;
        if (canTest(ref)) {
            NodeRef parapheur = parapheurService.getParentParapheur(ref);
            String username = AuthenticationUtil.getRunAsUser();

            if (parapheurService.isParapheurOwnerOrDelegate(parapheur, username) && parapheur.equals(bureauCourant)) {
                if (!nodeService.hasAspect(ref, ParapheurModel.ASPECT_SECRETARIAT)) {
                    NodeRef corbeille = parapheurService.getParentCorbeille(ref);
                    if (parapheurService.isTermine(ref)
                            && ParapheurModel.NAME_RETOURNES.equals(nodeService.getPrimaryParent(corbeille).getQName())) {
                        res = true;
                    }
                }
            }
        }
        return res;
    }

    /**
     * Test a effectuer à chaque vérification d'action Test si le noeud existe,
     * qu'il est un dossier et qu'il n'est pas en cours de traitement
     *
     * @param ref Dossier à tester
     * @return true si une action est possible
     */
    private boolean canTest(NodeRef ref) {
        boolean retVal = false;
        if (nodeService.exists(ref) && parapheurService.isDossier(ref)) {
            Boolean isInBatchQueue = (Boolean) nodeService.getProperty(ref, ParapheurModel.PROP_IN_BATCH_QUEUE);

            if (isInBatchQueue == null || !isInBatchQueue) {
                retVal = true;
            }

        }
        return retVal;
    }

    //----------------------------------------------------------\\
    //---                FONCTIONS DIVERSES                  ---\\
    //----------------------------------------------------------\\

    @Override
    public S2lowServiceImpl.SecureMailDetail getInfosMailSec(NodeRef dossierRef) {
        S2lowServiceImpl.SecureMailDetail ret = null;
        try {
            Integer mail_id = (Integer) nodeService.getProperty(dossierRef, ParapheurModel.PROP_MAILSEC_MAIL_ID);
            if (mail_id != null) {
                ret = s2lowService.getSecureMailDetail(mail_id);
            }

        } catch (Exception ex) {
            //
        }
        return ret;
    }

    /**
     * Permet l'obtention d'un nom unique pour un dossier
     *
     * @return Nom unique => "Sans titre" suivi d'un nombre
     */
    private String getUniqueDossierName() {
        String uniqueName = "Sans titre";

        return _getUniqueDossierName(uniqueName, 0);
    }

    /**
     * Fonctionne en accord avec getUniqueDossierName(). Ne doit pas être
     * appellée.
     */
    private String _getUniqueDossierName(String baseName, int it) {
        String uniqueName;

        if (it == 0) {
            uniqueName = baseName;
        } else {
            uniqueName = baseName + " " + it;
        }

        if (!isNameExist(uniqueName)) {
            return uniqueName;
        }
        return _getUniqueDossierName(baseName, it + 1);
    }

    @Override
    public String getPesPreviewUrl(NodeRef nodeRef) {
        String redirectUrl = null;
        try {

            logger.debug("nodeRef=" + nodeRef);

            {    // on verifie que le dossier est bien un PES
                NodeRef parentDossierNodeRef = parapheurService.getParentDossier(nodeRef);
                if (parentDossierNodeRef == null) {
                    throw new Exception("impossible de retrouver le dossier parent");
                }
                logger.debug("parentDossierNodeRef=" + parentDossierNodeRef);

                if (!ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS.equals(
                        nodeService.getProperty(parentDossierNodeRef, ParapheurModel.PROP_TDT_PROTOCOLE))) {
                    throw new Exception("demande d'acces a un noeud dont le dossier n'est pas de type "
                            + ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS + " : "
                            + nodeService.getProperty(parentDossierNodeRef, ParapheurModel.PROP_TDT_PROTOCOLE));
                }
            }

            ContentReader mainDocReader = contentService.getReader(nodeRef, ContentModel.PROP_CONTENT);

            // le nom du document PES (noeud)
            String documentName = (String) nodeService.getProperties(nodeRef).get(ContentModel.PROP_NAME);

            // appel helper pour contact serveur et récupération url de redirect
            String xwvUrl = BlexContext.getInstance().getXwvConfig()
                    .buildViewer()
                    .setDatasStream(mainDocReader.getContentInputStream())
                    .doPrepare()
                    .getRedirectUrl();

            redirectUrl = xwvUrl;

            // read the document when URL is generated
            this.setDocumentLu(nodeRef, AuthenticationUtil.getRunAsUser());

            logger.debug("redirect to : " + redirectUrl);

        } catch (Exception e) {
            String message = "Impossible de visualiser le flux PES : " + e.getMessage();
            System.out.println(e);
            logger.warn(e);
        }
        return redirectUrl;
    }

    @Override
    public String getPesPreviewUrlFromArchive(NodeRef nodeRef) {
        String redirectUrl = null;
        try {

            logger.debug("nodeRef=" + nodeRef);

            {    // on verifie que le dossier est bien un PES
                if (nodeRef == null) {
                    throw new Exception("impossible de retrouver le dossier parent");
                }
                logger.debug("parentDossierNodeRef=" + nodeRef);

                if (!ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS.equals(
                        nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_PROTOCOLE))) {
                    throw new Exception("demande d'acces a un noeud dont le dossier n'est pas de type "
                            + ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS + " : "
                            + nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_PROTOCOLE));
                }
            }

            ContentReader mainDocReader = contentService.getReader(nodeRef, ParapheurModel.PROP_ORIGINAL);

            // le nom du document PES (noeud)
            String documentName = (String) nodeService.getProperties(nodeRef).get(ParapheurModel.PROP_ORIGINAL_NAME);

            // appel helper pour contact serveur et récupération url de redirect
            String xwvUrl = BlexContext.getInstance().getXwvConfig()
                    .buildViewer()
                    .setDatasStream(mainDocReader.getContentInputStream())
                    .doPrepare()
                    .getRedirectUrl();

            redirectUrl = xwvUrl;

            logger.debug("redirect to : " + redirectUrl);

        } catch (Exception e) {
            String message = "Impossible de visualiser le flux PES : " + e.getMessage();
            System.out.println(e);
            logger.warn(e);
        }
        return redirectUrl;
    }

    @Override
    public boolean isPesViewEnabled(NodeRef nodeRef) {
        // serveur XMV accessible ?
        if (!BlexContext.getInstance().getXwvConfig().isServerUsable()) {
            return Boolean.FALSE;
        }
        // on verifie que le dossier est bien un PES
        if (nodeRef == null) {
            // impossible de retrouver le dossier parent
            return Boolean.FALSE;
        }

        if (!ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS.equals(
                nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_PROTOCOLE))) {
            return Boolean.FALSE;
        }

        // bon, ça doit être possible ...
        return Boolean.TRUE;
    }

    @Override
    public boolean isPesViewEnabled() {
        // On lance un check
        BlexContext.getInstance().getXwvConfig().doCheckServer();
        // serveur XMV accessible ?
        if (!BlexContext.getInstance().getXwvConfig().isServerUsable()) {

            if (!BlexContext.getInstance().getXwvConfig().isServerUsable()) {
                return Boolean.FALSE;
            }
        }
        // bon, ça doit être possible ...
        return Boolean.TRUE;
    }

    /**
     * Récupération du MimeType d'un document
     *
     * @param d Document
     * @return MimeType du document
     * @throws IOException
     */
    private String getDocumentMimeType(Document d) throws IOException, SAXException {
        byte[] content = d.getContent();
        InputStream byteArrayInputStream = TikaInputStream.get(content);
        Metadata md = new Metadata();
        md.set(Metadata.RESOURCE_NAME_KEY, d.getFileName());
        Detector detector = new ContainerAwareDetector(TikaConfig.getDefaultConfig().getMimeRepository());

        try {
            MediaType mediaType;
            mediaType = detector.detect(byteArrayInputStream, md);
            return mediaType.getType() + "/" + mediaType.getSubtype();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            byteArrayInputStream.close();
        }


        /*
         Metadata metadata = new Metadata();
         metadata.set(Metadata.RESOURCE_NAME_KEY, d.getFileName());
         */
        //Tika tika = new Tika(TikaConfig.getDefaultConfig().getMimeRepository());

        //tika.detect(byteArrayInputStream, metadata);

//        String type = null;
//        Tika tika = new Tika();
//        try {
//            type = tika.detect(byteArrayInputStream);
//            MediaType mediaType = MediaType.parse(type);
//            String mimeType = mediaType.getSubtype();
//            //parser.parse(byteArrayInputStream, new BodyContentHandler(), metadata, new ParseContext());

        // OOXMLParser parser = new OOXMLParser();


//        System.out.println("meta" + metadata.get(Metadata.CONTENT_TYPE));
        //return type;
    }

    /**
     * Récupération du dossier en cours de création pour l'utilisateur actuel
     *
     * @param bureauRef Bureau actuel de l'utilisateur
     * @return Le dossier en cours de création, null si aucun.
     */
    private NodeRef dossierUnderCreation(NodeRef bureauRef) {
        CakeFilter filter = new CakeFilter();

        NodeRef parent = parapheurService.getCorbeille(bureauRef, ParapheurModel.NAME_EN_PREPARATION);

        filter.setType("ph:dossier");
        filter.setSearchPath(null);
        if (parent != null) {
            filter.setParent(parent);
            filter.setParentType(nodeService.getType(parent));
        }
        //filter.setParent;

        List<Predicate<?, ?>> filters = new ArrayList<Predicate<?, ?>>();

        Predicate<?, ?> uncomplete = new Predicate<QName, String>(ParapheurModel.PROP_UNCOMPLETE, "true");
        Predicate<?, ?> creator = new Predicate<QName, String>(ContentModel.PROP_CREATOR, AuthenticationUtil.getRunAsUser());

        filters.add(creator);
        filters.add(uncomplete);

        filter.setClause(new Predicate<String, List<Predicate<?, ?>>>("and", filters));

        String query = filter.buildFilterQuery();
        /* SearchParameters Wizardry */
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        searchParameters.setQuery(query);

        /* permission check ? eager search */
        searchParameters.setMaxPermissionChecks(0);

        ResultSet resultSet = null;
        List<NodeRef> refs = null;
        try {
            resultSet = searchService.query(searchParameters);
            refs = resultSet.getNodeRefs();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        if (refs == null || refs.isEmpty()) {
            return null;
        }
        return refs.get(0);
    }

    /**
     * Test si le nom du dossier existe
     *
     * @param name Nom du dossier
     * @return true si le nom existe
     */
    private boolean isNameExist(String name) {
        DossierFilter dossierFilter = new DossierFilter();
        dossierFilter.setSearchPath(null);
        dossierFilter.setObjectType("ph:dossier");

        dossierFilter.addFilterElement(ContentModel.PROP_NAME.getPrefixedQName(namespaceService), name);

        ResultSet rs = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_LUCENE, dossierFilter.buildFilterQuery());

        List<NodeRef> list = rs.getNodeRefs();

        rs.close();

        if (list.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Vérifie si la finalization du dossier est possible
     *
     * @param ref Dossier à vérifier
     * @return Erreur sous forme de string permettant l'i18n, "success" en cas
     * de réussite
     */
    @Override
    public
    @NotNull
    String checkFinalize(NodeRef ref) {

        String ret = null;

        if (parapheurService.getCircuit(ref) == null) {
            ret = "app.ajax.msg.circuit";
        } else {
            String type = (String) nodeService.getProperty(ref, ParapheurModel.PROP_TYPE_METIER);
            String sstype = (String) nodeService.getProperty(ref, ParapheurModel.PROP_SOUSTYPE_METIER);

            if (type != null && sstype != null) {

                String fileTypeError = getWrongFileErrorMessage(ref);
                if (fileTypeError != null) {
                    ret = fileTypeError;
                } else {
                    Map<QName, Map<String, Serializable>> metaMap = metadataService.getMetaDonneesDossier(ref);
                    Map<String, Serializable> values = metadataService.getMetadataValues(metaMap);
                    for (CustomMetadataDef def : metadataService.getMetadatas(type, sstype)) {
                        if (values.containsKey(def.getName().getLocalName())) {
                            Serializable tempVal = values.get(def.getName().getLocalName());
                            if ("true".equals(metaMap.get(def.getName()).get("mandatory")) && (tempVal == null || tempVal.toString().isEmpty())) {
                                ret = "app.ajax.msg.metadata";
                            }
                        }
                    }
                }
            } else {
                ret = "app.ajax.msg.type";
            }

            if (parapheurService.getMainDocuments(ref).isEmpty()) {
                ret = String.valueOf(ParapheurErrorCode.MAIN_DOCUMENT_IS_MISSING.getCode());
            }
        }

        if (ret == null) {
            ret = "success";
        }

        return ret;
    }

    /**
     * Checks file types on folder emission.
     * If null, there is nothing wrong with the created folder.
     * <p>
     * Error code meanings are available here : http://aduwiki.extranet.adullact.org/doku.php?id=open_parapheur#code_d_erreurs
     *
     * @param dossier a valid nodeRef
     * @return An error message, or null.
     */
    private
    @Nullable
    String getWrongFileErrorMessage(@NotNull NodeRef dossier) {

        String type = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER);
        String subType = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER);
        boolean isMultidoc = typesService.isMultiDocument(type, subType);
        String protocol = typesService.getProtocol(type);
        String signatureFormat = typesService.getSignatureFormat(type);

        // We have to be careful here, metadata can be wrongly set during Dossier creation.
        // We have to check the Type/SubType for multi-doc compatibility and re-build document lists, to be sure.

        List<NodeRef> mainDocuments = new ArrayList<NodeRef>();
        List<NodeRef> annexes = new ArrayList<NodeRef>();

        if (isMultidoc) {
            mainDocuments = parapheurService.getMainDocuments(dossier);

            if (mainDocuments == null || mainDocuments.isEmpty()) {
                return String.valueOf(ParapheurErrorCode.MAIN_DOCUMENT_IS_MISSING.getCode());
            }
            List<NodeRef> annexesToAdd = parapheurService.getAttachments(dossier);
            if (annexesToAdd != null) {
                annexes.addAll(annexesToAdd);
            }
        } else {
            List<NodeRef> documents = parapheurService.getDocuments(dossier);

            if (documents == null || documents.isEmpty()) {
                return String.valueOf(ParapheurErrorCode.MAIN_DOCUMENT_IS_MISSING.getCode());
            }

            for (int i = 0; i < documents.size(); i++) {
                if (i == 0) {
                    mainDocuments.add(documents.get(i));
                } else {
                    annexes.add(documents.get(i));
                }
            }
        }

        // XML restrictions

        boolean isFormatCompatibleWithXml = StringUtils.isEmpty(signatureFormat) || StringUtils.startsWithIgnoreCase(signatureFormat, "XAdES") || StringUtils.startsWithIgnoreCase(signatureFormat, "XAdES");
        boolean isProtocolCompatibleWithXml = StringUtils.isEmpty(protocol) || StringUtils.startsWithIgnoreCase(protocol, "HELIOS") || StringUtils.startsWithIgnoreCase(protocol, "aucun");

        if ((!isFormatCompatibleWithXml) || (!isProtocolCompatibleWithXml)) {
            for (NodeRef documentRef : mainDocuments) {
                if (StringUtils.equalsIgnoreCase(getContentMimeType(documentRef), MimetypeMap.MIMETYPE_XML)) {
                    return String.valueOf(ParapheurErrorCode.XML_DOCUMENTS_FORBIDDEN_BY_TYPE_OR_SUBTYPE.getCode());
                }
            }
        }

        for (NodeRef documentRef : annexes) {
            if (StringUtils.equalsIgnoreCase(getContentMimeType(documentRef), MimetypeMap.MIMETYPE_XML)) {
                return String.valueOf(ParapheurErrorCode.XML_DOCUMENTS_FORBIDDEN_IN_ANNEXES.getCode());
            }
        }

        // PAdES restrictions

        boolean isPades = StringUtils.startsWithIgnoreCase(signatureFormat, "PAdES");

        if (isPades && !StringUtils.startsWithIgnoreCase(protocol, "ACTES")) {
            for (NodeRef documentRef : mainDocuments) {
                if (!StringUtils.equalsIgnoreCase(getContentMimeType(documentRef), MimetypeMap.MIMETYPE_PDF)) {
                    return String.valueOf(ParapheurErrorCode.PADES_ONLY_ACCEPTS_PDF_DOCUMENTS.getCode());
                }
            }
        }

        //

        return null;
    }

    private boolean archiveExist(String name) {
        boolean ret = false;
        // Récupération du répertoire des archives
        String xpath = parapheurService.getConfiguration().getProperty("spaces.company_home.childname") + "/" + parapheurService.getConfiguration().getProperty("spaces.archives.childname");
        List<NodeRef> results = null;
        results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(parapheurService.getConfiguration().getProperty("spaces.store"))), xpath, null, namespaceService, false);
        NodeRef archivesRef = results.get(0);

        NodeRef archive = fileFolderService.searchSimple(archivesRef, name);
        if (archive != null) {
            ret = true;
        }

        return ret;
    }

    @Override
    public boolean canArchive(NodeRef ref, NodeRef bureauCourant) {
        boolean res = false;
        if (canTest(ref)) {
            NodeRef parapheur = parapheurService.getParentParapheur(ref);
            NodeRef corbeille = parapheurService.getParentCorbeille(ref);
            String user = AuthenticationUtil.getRunAsUser();
            List<EtapeCircuit> circuit = null;

            // cas d'un node créé par Alfresco et non par le code i-parapheur: le nodePropertyResolver n'existe pas
            circuit = parapheurService.getCircuit(ref);
            /**
             * compatibilité ascendante, circuits pré-v3...
             */
            if (circuit.get(0).getActionDemandee() == null) { //
                if ((parapheurService.isParapheurOwnerOrDelegate(parapheur, user) || parapheurService.isParapheurSecretaire(parapheur, user)) && parapheur.equals(bureauCourant)) {
                    if (parapheurService.isTermine(ref)
                            && ParapheurModel.NAME_A_ARCHIVER.equals(nodeService.getPrimaryParent(corbeille).getQName())) {
                        res = true;
                    }
                }
            } /**
             * Depuis les nouveaux circuits (avec action demandée) on archive si
             * - en étape d'archivage ET currentUser pas null ET currentUser
             * propriétaire/secrétaire du parapheur courant détenant le dossier
             * ET dossier terminé ET dans la corbeille "Dossiers en fin de
             * circuit"
             */
            else if (parapheurService.getCurrentEtapeCircuit(circuit) != null && parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee().trim().equalsIgnoreCase(EtapeCircuit.ETAPE_ARCHIVAGE)
                    && (user != null && (parapheurService.isParapheurOwnerOrDelegate(parapheur, user) || parapheurService.isParapheurSecretaire(parapheur, user)))
                    && parapheurService.isTermine(ref)
                    && ParapheurModel.NAME_A_ARCHIVER.equals(nodeService.getPrimaryParent(corbeille).getQName())) {
                res = true;
            } /**
             * Since 3.3: On doit pouvoir archiver AUSSI les dossiers rejetés. -
             * L'info d'étape courante n'est plus pertinente... Il faut que:
             * currentUser pas null ET currentUser propriétaire/secrétaire du
             * parapheur courant détenant le dossier ET dossier terminé ET
             * dossier "rejeté" (cf statut métier commence par "Rejet") ET dans
             * la corbeille "Dossiers rejetés"
             */
            else if (user != null
                    && (parapheurService.isParapheurOwnerOrDelegate(parapheur, user) || parapheurService.isParapheurSecretaire(parapheur, user))
                    && parapheurService.isTermine(ref)
                    && parapheurService.isRejete(ref)
                    && ParapheurModel.NAME_RETOURNES.equals(nodeService.getPrimaryParent(corbeille).getQName())) {
                res = true;
            }
        }
        return res;
    }

    @Override
    public boolean isNextEtapeXadesSig(NodeRef dossier) {
        String sigFormat = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT);

        if (sigFormat != null && sigFormat.contains("XAdES")) {
            // Get hash in worker, for every signature step !
            EtapeCircuit currentEtape = parapheurService.getCurrentEtapeCircuit(dossier);

            return StringUtils.equalsIgnoreCase(currentEtape.getActionDemandee(), DossierService.ACTION_DOSSIER.SIGNATURE.toString());
        }
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, String> getSignatureInformations(NodeRef ref, NodeRef bureauCourant) {

        HashMap<String, String> infos = (HashMap<String, String>) nodeService.getProperty(ref, ParapheurModel.PROP_SIGN_INFO);

        if (infos == null || infos.isEmpty() || infos.get("hash").trim().isEmpty()
                || (infos.containsKey("format") &&
                (infos.get("format").toLowerCase().contains("pades") || infos.get("format").toLowerCase().contains("pkcs#1")))) {
            infos = (HashMap<String, String>) generateSignatureInformations(ref, bureauCourant);
        } else {
            logger.debug("SignInfo found for Dossier " + ref.getId());
            // Define role, city and postal code
            SignatureInformations sinfo = new SignatureInformations(ref, bureauCourant);
            infos.putAll(sinfo.getPesV2SignParameters());
        }

        return infos;
    }

    @Override
    public Map<String, String> generateSignatureInformations(NodeRef ref, NodeRef bureauCourant) {

        SignatureInformations sinfo = new SignatureInformations(ref, bureauCourant);
        Map<String, String> infos = sinfo.getInformations();

        if (!sinfo.getSignatureFormatPourApplet().equalsIgnoreCase("PKCS1_SHA256_RSA") &&
                // Pour la signature PAdES, le visuel doit toujours être ré-généré
                !sinfo.getSignatureFormatPourApplet().contains("PAdES")) {
            HashMap<String, String> infosSerializable = new HashMap<String, String>(infos);

            nodeService.setProperty(ref, ParapheurModel.PROP_SIGN_INFO, infosSerializable);
            notificationService.notifierPourSignInfo(ref, infosSerializable);
        }

        return infos;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#getConsecutiveSteps(org.alfresco.service.cmr.repository.NodeRef,
     * String)
     */
    @Override
    public List<EtapeCircuit> getConsecutiveSteps(NodeRef dossier, String user) {
        List<EtapeCircuit> consecutiveSteps = new ArrayList<EtapeCircuit>();
        if (isEmis(dossier)) { // l'étape d'émission doit être différenciée des étapes d'envoi (ici visa/signature)
            EtapeCircuit currentStep = parapheurService.getCurrentEtapeCircuit(dossier);
            while ((currentStep != null)
                    && currentStep.getActionDemandee().equalsIgnoreCase(EtapeCircuit.ETAPE_VISA)
                    && parapheurService.isParapheurOwnerOrDelegate(currentStep.getParapheur(), user)) {

                consecutiveSteps.add(currentStep);

                NodeRef nextStep = getFirstChild(currentStep.getNodeRef(), ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
                if ((nextStep != null) && this.nodeService.exists(nextStep)) {
                    currentStep = new EtapeCircuitImpl(nodeService, nextStep);
                } else {
                    currentStep = null;
                }
            }
        }
        return consecutiveSteps;
    }

    private NodeRef getFirstChild(NodeRef nodeRef, QName childAssocType) {
        List<ChildAssociationRef> childAssocs = this.nodeService.getChildAssocs(nodeRef, childAssocType,
                RegexQNamePattern.MATCH_ALL);
        return (childAssocs.size() == 1) ? childAssocs.get(0).getChildRef() : null;
    }

    @Override
    public List<LogDossier> getLogsDossier(NodeRef dossierRef) {
        final List<LogDossier> logsDossier = new ArrayList<LogDossier>();

        try {
            final AuditService fAuditService = auditService;
            final NodeRef fDossierRef = dossierRef;
            AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Void>() {
                @Override
                public Void doWork() throws Exception {
                    ParapheurServiceCompatCallback callback = new ParapheurServiceCompatCallback();
                    callback.setResponse(logsDossier);
                    AuditQueryParameters queryParameters = new AuditQueryParameters();
                    queryParameters.setApplicationName("ParapheurServiceCompat");
                    queryParameters.addSearchKey("dossier", fDossierRef);

                    try {
                        fAuditService.auditQuery(callback, queryParameters, 0);
                    } catch (Exception e) {
                        logger.error("Erreur lors de la récupération des logs du dossier : " + e);

                    }
                    if (callback.getRecordsReadCount() == 0) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("Ce dossier n'a pas de logs : " + fDossierRef);
                        }
                    }
                    return null;
                }
            }, AuthenticationUtil.getAdminUserName());
        } catch (Exception e) {
            logger.error("Erreur lors du AuthenticationUtil.runAs : " + e);
        }
        return logsDossier;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#hasReadDossier(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public boolean hasReadDossier(NodeRef dossier, String username) {
        boolean ret = false;
        try {
            ret = hasReadDocument(parapheurService.getMainDocuments(dossier).get(0), username);
        } catch (Exception e) {
        }
        return ret;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#hasReadDocument(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public boolean hasReadDocument(NodeRef document, String username) {
        List<String> lecteurs = (ArrayList<String>) nodeService.getProperty(document, ParapheurModel.PROP_LECTEURS);
        return ((lecteurs != null) && lecteurs.contains(username));
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#setDossierNonLu(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void setDossierNonLu(NodeRef dossier) {
        Assert.isTrue(parapheurService.isDossier(dossier), "NodeRef doit être un ph:dossier");
        List<NodeRef> docs = parapheurService.getMainDocuments(dossier);
        for (NodeRef tmpDoc : docs) {
            this.nodeService.removeAspect(tmpDoc, ParapheurModel.ASPECT_LU);
            this.nodeService.removeProperty(tmpDoc, ParapheurModel.PROP_LECTEURS);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#isDossierLu(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isDossierLu(NodeRef dossier) {
        boolean isLu = false;
        List<NodeRef> documents = parapheurService.getMainDocuments(dossier);
        if (documents.size() > 0) {
            NodeRef mainDoc = documents.get(0);
            if (nodeService.hasAspect(mainDoc, ParapheurModel.ASPECT_LU)) {
                isLu = true;
            }
        }
        return isLu;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#setDossierLu(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public void setDossierLu(NodeRef dossier, String lecteur) {
        List<NodeRef> documents = parapheurService.getMainDocuments(dossier);
        for (NodeRef document : documents) {
            setDocumentLu(document, lecteur);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#setDocumentLu(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public void setDocumentLu(NodeRef document, String lecteur) {
        ArrayList<String> lecteurs = (ArrayList<String>) nodeService.getProperty(document, ParapheurModel.PROP_LECTEURS);
        if (lecteurs == null) {
            lecteurs = new ArrayList<String>();
        }
        if (!lecteurs.contains(lecteur)) {
            lecteurs.add(lecteur);
        }
        Map<QName, Serializable> props = new HashMap<QName, Serializable>();
        props.put(ParapheurModel.PROP_LECTEURS, lecteurs);
        nodeService.addAspect(document, ParapheurModel.ASPECT_LU, props);
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#didSomeoneElseReadDocument(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public boolean didSomeoneElseReadDocument(NodeRef document, String username) {
        ArrayList<String> lecteurs = (ArrayList<String>) nodeService.getProperty(document, ParapheurModel.PROP_LECTEURS);
        return ((lecteurs != null) && ((lecteurs.size() > 1) || !lecteurs.contains(username)));
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#isDocumentPrincipal(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isDocumentPrincipal(NodeRef document) {
        return nodeService.hasAspect(document, ParapheurModel.ASPECT_MAIN_DOCUMENT);
    }

    private List<String> getNotifies(NodeRef dossier) {
        Set<String> notifies = new HashSet<String>();
        List<EtapeCircuit> etapes = parapheurService.getCircuit(dossier);

        for (EtapeCircuit etape : etapes) {
            List<NodeRef> listeNotif = (ArrayList<NodeRef>) nodeService.getProperty(etape.getNodeRef(), ParapheurModel.PROP_LISTE_NOTIFICATION);
            if (listeNotif != null) {
                for (NodeRef bureau : listeNotif) {
                    List<String> owners = parapheurService.getParapheurOwners(bureau);
                    if (owners != null) {
                        notifies.addAll(owners);
                    }
                }
            }
        }
        return new ArrayList<String>(notifies);
    }

    @Override
    public List<String> getCorbeillesParent(NodeRef dossier, NodeRef bureauCourant) {
        List<String> corbeilles = new ArrayList<String>();
        corbeilles.add((((Path.ChildAssocElement) (nodeService.getPath(parapheurService.getParentCorbeille(dossier)).last())).getRef().getQName().getLocalName()));

        List<AssociationRef> assocs = nodeService.getSourceAssocs(dossier, RegexQNamePattern.MATCH_ALL);

        for (AssociationRef assoc : assocs) {
            if (parapheurService.getParentParapheur(assoc.getSourceRef()).equals(bureauCourant)) {
                corbeilles.add((((Path.ChildAssocElement) (nodeService.getPath(assoc.getSourceRef()).last())).getRef().getQName().getLocalName()));
            }
        }
        return corbeilles;
    }

    @Override
    public List<NodeRef> getCircuit(NodeRef dossier) {
        List<NodeRef> circuit = new ArrayList<NodeRef>();

        NodeRef emetteur = getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        /*if (logger.isDebugEnabled()) {
            logger.debug(" GETFIRSTCHILD: getCircuit=" + emetteur);
        }*/
        if (emetteur == null || !this.nodeService.exists(emetteur)) {
            return null;
        }
        circuit.add(emetteur);

        for (NodeRef etape = getFirstChild(emetteur, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
             etape != null && this.nodeService.exists(etape);
             etape = getFirstChild(etape, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE)) {
            circuit.add(etape);
        }

        return circuit;
    }

    @Override
    public String getActionDemandee(NodeRef dossier) {
        List<NodeRef> circuit = getCircuit(dossier);

        if (circuit == null) {
            return null;
        }
        if (parapheurService.isTermine(dossier) && nodeService.getProperty(circuit.get(0), ParapheurModel.PROP_ACTION_DEMANDEE) == null) {
            return null;
        }
        boolean isRejete = parapheurService.isRejete(dossier);
        NodeRef current = null;

        for (NodeRef etape : circuit) {
            Boolean approved = (Boolean) nodeService.getProperty(etape, ParapheurModel.PROP_EFFECTUEE);
            if (approved != null && approved) {
                current = etape;
            } else {
                if (!isRejete) {
                    current = etape;
                    break;
                }
            }
        }
        return (String) nodeService.getProperty(current, ParapheurModel.PROP_ACTION_DEMANDEE);
    }

    @Override
    public List<ACTION_DOSSIER> getActions(NodeRef dossier, NodeRef bureauCourant) {
        List<ACTION_DOSSIER> actions = new ArrayList<DossierService.ACTION_DOSSIER>();

        if (!canTest(dossier)) {
            return actions;
        }

        ChildAssociationRef refCorbeille = nodeService.getPrimaryParent(dossier);
        ChildAssociationRef refParapheur = nodeService.getPrimaryParent(refCorbeille.getParentRef());
        QName corbeilleQName = refParapheur.getQName();
        NodeRef parapheur = refParapheur.getParentRef();
        String username = AuthenticationUtil.getRunAsUser();

        //TODO : Améliorer temps d'éxécution du getCircuit ?
        List<NodeRef> circuit = getCircuit(dossier);

        boolean inSecretariat = nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT);
        boolean hasAspectTdtTransaction = nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW)
                || nodeService.hasAspect(dossier, SrciService.K.aspect_srciTransaction);
        String createur = (String) nodeService.getProperty(dossier, ContentModel.PROP_CREATOR);
        boolean isEmis = circuit != null && !circuit.isEmpty() && (Boolean) nodeService.getProperty(circuit.get(0), ParapheurModel.PROP_EFFECTUEE);
        boolean isTermine = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_TERMINE);
        boolean isOwnerOrDelegate = parapheurService.isParapheurOwnerOrDelegate(parapheur, username);
        boolean isSecretaire = false;
        boolean isParapheurDelegate = false;
        List<String> secretaires = (List<String>) nodeService.getProperty(parapheur, ParapheurModel.PROP_SECRETAIRES);
        if (!isOwnerOrDelegate) {
            isSecretaire = secretaires != null && secretaires.contains(username);
        }
        if (!parapheur.equals(bureauCourant)) {
            isParapheurDelegate = parapheurService.isParapheurDelegate(parapheur, bureauCourant);
        }

        Boolean habilitationTraiter, habilitationTransmettre, habilitationArchiver, habilitationSecretariat, habilitationEnchainement;
        if (!nodeService.hasAspect(parapheur, ParapheurModel.ASPECT_HABILITATIONS)) {
            habilitationTransmettre = true;
            habilitationTraiter = true;
            habilitationArchiver = true;
            habilitationEnchainement = true;
            habilitationSecretariat = true;
        } else {
            habilitationTransmettre = (Boolean) nodeService.getProperty(parapheur, ParapheurModel.PROP_HAB_TRANSMETTRE);
            habilitationTraiter = (Boolean) nodeService.getProperty(parapheur, ParapheurModel.PROP_HAB_TRAITER);
            habilitationArchiver = (Boolean) nodeService.getProperty(parapheur, ParapheurModel.PROP_HAB_ARCHIVAGE);
            habilitationEnchainement = (Boolean) nodeService.getProperty(parapheur, ParapheurModel.PROP_HAB_ENCHAINEMENT);
            habilitationSecretariat = (Boolean) nodeService.getProperty(parapheur, ParapheurModel.PROP_HAB_SECRETARIAT);

            // this was added in 4.6.5
            habilitationEnchainement = habilitationEnchainement == null ? false : habilitationEnchainement;
        }

        /* IMPRESSION et EMAIL et JOURNAL (événements) */
        List<NodeRef> docs = parapheurService.getMainDocuments(dossier);
        if (!docs.isEmpty()) {
            actions.add(ACTION_DOSSIER.ENREGISTRER);
            actions.add(ACTION_DOSSIER.JOURNAL);
            if(this.allowMail) {
                actions.add(ACTION_DOSSIER.EMAIL);
            }
        }

        /* DROIT DE REMORD */
        NodeRef etapePrecedenteRef = null;
        NodeRef etapeCouranteRef = null;
        if (circuit != null) {
            for (int i = 0; i < circuit.size(); i++) {
                NodeRef current = circuit.get(i);
                Object effectue = nodeService.getProperty(current, ParapheurModel.PROP_EFFECTUEE);
                if (current != null && (effectue == null || (effectue != null && !(Boolean) effectue))) {
                    if (i > 0) {
                        etapePrecedenteRef = circuit.get(i - 1);
                    }
                    etapeCouranteRef = current;
                    break;
                }
            }
        }

        //isRecuperable non utilisé, car utilise getCircuit (qui est consommateur de temps
        boolean isRecuperable = false;
        if (isEmis && !isTermine && etapeCouranteRef != null) {
            if (nodeService.getProperty(etapeCouranteRef, ParapheurModel.PROP_ACTION_DEMANDEE).equals(EtapeCircuit.ETAPE_MAILSEC)) {
                isRecuperable = s2lowService.isMailSecJobCancelable(dossier);
            } else if (nodeService.getProperty(etapeCouranteRef, ParapheurModel.PROP_ACTION_DEMANDEE).equals(EtapeCircuit.ETAPE_MAILSEC_PASTELL)) {
                isRecuperable = nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID) != null;
            } else {
                isRecuperable = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_RECUPERABLE);
            }
        }


        /* SUPPRESSION
         * Si il n'est pas emis et que l'utilisateur est le responsable ou secretaire
         * sinon s'il est retourné et que l'utilisateur est acteur courant
         */
        if (!isEmis) {
            if (isOwnerOrDelegate || (isSecretaire && createur.equals(username))) {
                actions.add(ACTION_DOSSIER.SUPPRESSION);
            }
        } else if (isTermine
                && ParapheurModel.NAME_RETOURNES.equals(corbeilleQName)
                && isOwnerOrDelegate) {
            actions.add(ACTION_DOSSIER.SUPPRESSION);
        }

        /* RAZ (corriger et émettre à nouveau) */

        if (habilitationTransmettre &&
                isOwnerOrDelegate &&
                !inSecretariat &&
                isTermine &&
                parapheur.equals(bureauCourant) &&
                ParapheurModel.NAME_RETOURNES.equals(corbeilleQName)) {
            actions.add(ACTION_DOSSIER.RAZ);
        }

        /* EDITION */
        if (habilitationTransmettre &&
                !isEmis &&
                (parapheur.equals(bureauCourant) || isParapheurDelegate)) {

            if (ParapheurModel.NAME_SECRETARIAT.equals(corbeilleQName)) {
                if (isSecretaire &&
                        parapheurService.isParapheurSecretaire(parapheur, (String) nodeService.getProperty(dossier, ContentModel.PROP_CREATOR))) {
                    actions.add(ACTION_DOSSIER.EDITION);
                }
            } else if (ParapheurModel.NAME_EN_PREPARATION.equals(corbeilleQName) && isOwnerOrDelegate) {
                actions.add(ACTION_DOSSIER.EDITION);
            }
        }

        // SECRETARIAT (envoi ou retour)
        if (habilitationSecretariat &&
                (parapheur.equals(bureauCourant) || isParapheurDelegate) &&
                ((isOwnerOrDelegate && !inSecretariat) || (isSecretaire && inSecretariat)) &&
                secretaires != null && !secretaires.isEmpty() && !isTermine) {
            actions.add(ACTION_DOSSIER.SECRETARIAT);
        }


        /* Toutes les actions qui suivent demandent une vérification sur l'étape courante, donc s'il n'y en a pas, il
         * n'y a plus d'actions possibles.
         */
        if (etapeCouranteRef == null) {
            return actions;
        }

        String actionDemandee = (String) nodeService.getProperty(etapeCouranteRef, ParapheurModel.PROP_ACTION_DEMANDEE);

        if (!inSecretariat && isRecuperable
                && ((etapePrecedenteRef != null && nodeService.getProperty(etapePrecedenteRef, ParapheurModel.PROP_ACTION_DEMANDEE).equals(EtapeCircuit.ETAPE_VISA))
                || actionDemandee.equals(EtapeCircuit.ETAPE_MAILSEC) || actionDemandee.equals(EtapeCircuit.ETAPE_MAILSEC_PASTELL)
        )) {
            NodeRef bureauPrec = (NodeRef) nodeService.getProperty(etapePrecedenteRef, ParapheurModel.PROP_PASSE_PAR);
            NodeRef bureauDeleguePrec = (NodeRef) nodeService.getProperty(etapePrecedenteRef, ParapheurModel.PROP_DELEGUE);
            if ((bureauPrec.equals(bureauCourant) || ((bureauDeleguePrec != null) && bureauDeleguePrec.equals(bureauCourant))) &&
                    parapheurService.isParapheurOwnerOrDelegate(bureauPrec, username) || actionDemandee.equals(EtapeCircuit.ETAPE_MAILSEC) || actionDemandee.equals(EtapeCircuit.ETAPE_MAILSEC_PASTELL)) {

                actions.add(ACTION_DOSSIER.REMORD);
            }
        }

        // REJET
        if ((!hasAspectTdtTransaction || !actionDemandee.equals(EtapeCircuit.ETAPE_TDT)) && // pas en étape de TdT
                (parapheur.equals(bureauCourant) || isParapheurDelegate) &&
                isOwnerOrDelegate &&
                !inSecretariat &&
                !isTermine &&
                isEmis &&
                habilitationTraiter &&
                !nodeService.hasAspect(etapeCouranteRef, ParapheurModel.ASPECT_ETAPE_COMPLEMENTAIRE)) {
            actions.add(ACTION_DOSSIER.REJET);
        }

        boolean isRejete = ((String) nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS_METIER)).startsWith("Rejet");

        /* ARCHIVAGE */
        /**
         * compatibilité ascendante, circuits pré-v3...
         */
        if (circuit != null && nodeService.getProperty(circuit.get(0), ParapheurModel.PROP_ACTION_DEMANDEE) == null) {
            if ((isOwnerOrDelegate || isSecretaire) && parapheur.equals(bureauCourant)) {
                if (isTermine && ParapheurModel.NAME_A_ARCHIVER.equals(corbeilleQName)) {
                    actions.add(ACTION_DOSSIER.ARCHIVAGE);
                }
            }
        }
        /* Since 3.3:
         * On doit pouvoir archiver AUSSI les dossiers rejetés.
         *   - L'info d'étape courante n'est plus pertinente...
         * Il faut que: currentUser pas null
         *    ET  currentUser propriétaire/secrétaire du parapheur courant détenant le dossier
         *    ET  dossier terminé
         *    ET  dossier "rejeté" (cf statut métier commence par "Rejet")
         *        ET  dans la corbeille "Dossiers rejetés"
         *    ET Si pas rejeté, alors le dossier doit être en étape d'archivage
         *        ET dans la bannette "en fin de circuit".
         */
        else if ((isOwnerOrDelegate || isSecretaire) &&
                (parapheur.equals(bureauCourant) || isParapheurDelegate) &&
                isTermine &&
                ((isRejete &&
                        ParapheurModel.NAME_RETOURNES.equals(corbeilleQName))
                        ||
                        (actionDemandee.trim().equalsIgnoreCase(EtapeCircuit.ETAPE_ARCHIVAGE) &&
                                ParapheurModel.NAME_A_ARCHIVER.equals(corbeilleQName))
                )) {
            if(habilitationArchiver) {
                actions.add(ACTION_DOSSIER.ARCHIVAGE);
            }
            if (habilitationEnchainement && !(isRejete || ParapheurModel.NAME_RETOURNES.equals(corbeilleQName))) {
                actions.add(ACTION_DOSSIER.ENCHAINER_CIRCUIT);
            }
        }

        /* ACTIONS (VISA, SIGNATURE, ...) */
        // Si pas de soustype, pas d'action.
        if (nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER) == null) {
            return actions;
        }

        Boolean uncomplete = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_UNCOMPLETE);
        if (uncomplete == null) {
            uncomplete = false;
        }


        if (isOwnerOrDelegate && (parapheur.equals(bureauCourant) || isParapheurDelegate)) {
            boolean avisComplementaire = false;
            if (actionDemandee.equals(EtapeCircuit.ETAPE_VISA)) {
                /* VISA */

                if (!uncomplete && habilitationTransmettre && !(Boolean) nodeService.getProperty(circuit.get(0), ParapheurModel.PROP_EFFECTUEE)) {
                    actions.add(ACTION_DOSSIER.VISA);
                    avisComplementaire = true;
                } else {
                    Boolean signPapier = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_PAPIER);

                    boolean lastStepEffectue = false;
                    if (circuit.size() > 1) {
                        Object lastStepProp = nodeService.getProperty(circuit.get(circuit.size() - 2), ParapheurModel.PROP_EFFECTUEE);
                        lastStepEffectue = lastStepProp != null && (Boolean) lastStepProp;
                    }

                    boolean derniereEtape = !isTermine && lastStepEffectue;

                    // Dans le cas d'une dernière étape de signature papier, on passe sur une action spécialisée
                    if (habilitationTraiter &&
                            (!signPapier || !derniereEtape) &&
                            isOwnerOrDelegate &&
                            !inSecretariat &&
                            !isTermine) {
                        actions.add(ACTION_DOSSIER.VISA);
                        actions.add(ACTION_DOSSIER.TRANSFERT_ACTION);
                        avisComplementaire = true;
                    }
                }
            } /* TDT */ else if (actionDemandee.equals(EtapeCircuit.ETAPE_TDT)) {
                if (habilitationTraiter && !hasAspectTdtTransaction && !isRejete) {
                    String protocole = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_PROTOCOLE);
                    if (TypesService.TDT_PROTOCOLE_HELIOS.equals(protocole)) {
                        actions.add(ACTION_DOSSIER.TDT_HELIOS);
                    } else {
                        actions.add(ACTION_DOSSIER.TDT_ACTES);
                    }
                    avisComplementaire = true;
                }
            } else if (actionDemandee.equals(EtapeCircuit.ETAPE_CACHET)) {
                if (habilitationTraiter && !isRejete) {
                    actions.add(ACTION_DOSSIER.CACHET);
                }
                avisComplementaire = true;
            }/* Cas MAILSEC */ else if (actionDemandee.startsWith(EtapeCircuit.ETAPE_MAILSEC)) {
                String statusMetier = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS_METIER);
                if (habilitationTraiter && (statusMetier != null) && (!statusMetier.startsWith(StatusMetier.STATUS_EN_COURS))) {
                    actions.add(ACTION_DOSSIER.MAILSEC);
                    avisComplementaire = true;
                } else if (habilitationTraiter && (statusMetier != null)) {
                    actions.add(ACTION_DOSSIER.VISA);
                }

            } //Cas SIGNATURE
            else if (actionDemandee.equals(EtapeCircuit.ETAPE_SIGNATURE)) {
                List<String> isAlreadySignedBy = new ArrayList<>();
                boolean forbidSameSig = parapheurService.isForbidSameSig(dossier);
                if (circuit != null && forbidSameSig) {
                    circuit.forEach(etape -> {
                        String actionDemandeTmp = (String) nodeService.getProperty(etape, ParapheurModel.PROP_ACTION_DEMANDEE);
                        if(actionDemandeTmp.equals(EtapeCircuit.ETAPE_SIGNATURE)) {
                            Object effectueProp = nodeService.getProperty(etape, ParapheurModel.PROP_EFFECTUEE);
                            boolean stepeffectue = effectueProp != null && (Boolean) effectueProp;

                            // Folder is already signed by user
                            if(stepeffectue) {
                                isAlreadySignedBy.add((String) nodeService.getProperty(etape, ParapheurModel.PROP_VALIDATOR));
                            }
                        }
                    });
                }

                Boolean signPapier = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_PAPIER);

                boolean lastStepEffectue = false;
                if (circuit.size() > 1) {
                    Object lastStepProp = nodeService.getProperty(circuit.get(circuit.size() - 2), ParapheurModel.PROP_EFFECTUEE);
                    lastStepEffectue = lastStepProp != null && (Boolean) lastStepProp;
                }

                boolean derniereEtape = !isTermine && lastStepEffectue;

                if (habilitationTraiter &&
                        (!signPapier || !derniereEtape) &&
                        isOwnerOrDelegate &&
                        !inSecretariat &&
                        !isTermine) {
                    actions.add(ACTION_DOSSIER.VISA);
                    avisComplementaire = true;
                }

                if (habilitationTraiter &&
                        (!parapheurService.isReadingMandatory(dossier) || hasReadDossier(dossier, username)) &&
                        !isTermine &&
                        isEmis &&
                        ((nodeService.getProperty(circuit.get(0), ParapheurModel.PROP_ACTION_DEMANDEE) != null) || lastStepEffectue)) // compatibilité ascendante
                {
                    // Check samesig
                    if(!(forbidSameSig && isAlreadySignedBy.contains(username))) {
                        actions.add(ACTION_DOSSIER.SIGNATURE);
                    }
                    actions.add(ACTION_DOSSIER.TRANSFERT_ACTION);
                    avisComplementaire = true;
                }
            }
            if (avisComplementaire && isEmis) {
                actions.add(ACTION_DOSSIER.AVIS_COMPLEMENTAIRE);
            }

        }

        // On peut toujours effectuer cette action...
        actions.add(ACTION_DOSSIER.GET_ATTEST);

        return actions;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#isPrevEtapeLastSignature(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isPrevEtapeLastSignature(NodeRef dossier) {
        if (nodeService.hasAspect(dossier, ParapheurModel.ASPECT_ETAPE_ATTEST)) {
            for (EtapeCircuit e : parapheurService.getCircuit(dossier)) {
                if (e.getActionDemandee().equals("SIGNATURE") && !e.isApproved()) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#isCurrentTDTEtapeAuto(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isCurrentTDTEtapeAuto(NodeRef dossier) {
        if (nodeService.hasAspect(dossier, ParapheurModel.ASPECT_ETAPE_TDT_AUTO) &&
                ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS.equals(
                        nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_PROTOCOLE))) {

            EtapeCircuit currentEtape = parapheurService.getCurrentEtapeCircuit(dossier);

            return EtapeCircuit.ETAPE_TDT.equals(currentEtape.getActionDemandee());
        }
        return false;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#isCurrentTDTEtapeAuto(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isCurrentCachetEtapeAuto(NodeRef dossier) {
        if (nodeService.hasAspect(dossier, ParapheurModel.ASPECT_ETAPE_CACHET_AUTO)) {
            EtapeCircuit currentEtape = parapheurService.getCurrentEtapeCircuit(dossier);

            return EtapeCircuit.ETAPE_CACHET.equals(currentEtape.getActionDemandee());
        }
        return false;
    }

    @Override
    public SignatureInformations getSignatureInformations(NodeRef dossierRef, String annotPub, String annotPriv, NodeRef bureauCourant) {
        return new SignatureInformations(dossierRef, annotPub, annotPriv, bureauCourant);
    }

    @Override
    public boolean isEmis(NodeRef dossier) {
        NodeRef emetteur = getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        if (emetteur != null) {
            return (Boolean) this.nodeService.getProperty(emetteur, ParapheurModel.PROP_EFFECTUEE);
        } else {
            return false;
        }
    }

    private void setUniDocument(NodeRef dossier) {
        Assert.isTrue(parapheurService.isDossier(dossier), "NodeRef doit être un ph:dossier [" + dossier.getId() + "]");

        List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(dossier, ContentModel.ASSOC_CONTAINS,
                RegexQNamePattern.MATCH_ALL);

        if (childAssocs != null && childAssocs.size() > 0) {
            // Si c'est le premier élément, il vient d'être ajouté... Il doit être placé à la fin !
            ChildAssociationRef firstAssoc = null;
            for (ChildAssociationRef ref : childAssocs) {

                /* Afficher seulement les documents pas le dossier de preview mobile */
                NodeRef childRef = ref.getChildRef();
                QName childType = nodeService.getType(childRef);

                if (childType.equals(ContentModel.TYPE_CONTENT)) {
                    if (firstAssoc == null) {
                        firstAssoc = ref;
                    } else if (nodeService.hasAspect(childRef, ParapheurModel.ASPECT_MAIN_DOCUMENT)) {
                        nodeService.removeAspect(childRef, ParapheurModel.ASPECT_MAIN_DOCUMENT);
                    }
                }
            }
        }
    }

    @Override
    public void setOrderDocuments(NodeRef dossier, List<Map<String, Serializable>> documents) {
        Assert.isTrue(parapheurService.isDossier(dossier), "NodeRef doit être un ph:dossier [" + dossier.getId() + "]");

        List<ChildAssociationRef> primaryChildAssocs = new ArrayList<ChildAssociationRef>();
        List<ChildAssociationRef> annexesChildAssocs = new ArrayList<ChildAssociationRef>();
        List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(dossier, ContentModel.ASSOC_CONTAINS,
                RegexQNamePattern.MATCH_ALL);

        if (childAssocs != null && childAssocs.size() > 0) {
            // Récupération des associations suivant la liste...
            for (Map<String, Serializable> doc : documents) {
                //On cherche l'association :
                NodeRef docRef = null;
                ChildAssociationRef childRef = null;
                for (ChildAssociationRef ref : childAssocs) {
                    if (ref.getChildRef().getId().equals(doc.get("id"))) {
                        docRef = ref.getChildRef();
                        childRef = ref;
                    }
                }
                if (childRef != null) {
                    //On supprime - ajoute l'aspet MAIN_DOCUMENT
                    QName childType = nodeService.getType(docRef);
                    if (childType.equals(ContentModel.TYPE_CONTENT)) {
                        if ((Boolean) doc.get("isMainDocument")) {
                            nodeService.addAspect(docRef, ParapheurModel.ASPECT_MAIN_DOCUMENT, null);
                            primaryChildAssocs.add(childRef);
                        } else {
                            nodeService.removeAspect(docRef, ParapheurModel.ASPECT_MAIN_DOCUMENT);
                            annexesChildAssocs.add(childRef);
                        }
                    }
                }
            }
            // Si pas de doc principal, 1ere annexe = doc principal
            if (primaryChildAssocs.isEmpty()) {
                primaryChildAssocs.add(annexesChildAssocs.get(0));
                nodeService.addAspect(annexesChildAssocs.get(0).getChildRef(), ParapheurModel.ASPECT_MAIN_DOCUMENT, null);
                annexesChildAssocs.remove(0);
            }
            // Maintenant qu'on a séparé documents principaux et annexes, on ordonne !
            for (int i = 0; i < primaryChildAssocs.size(); i++) {
                // i+1 Car le premier enfant d'un dossier est le circuit !
                nodeService.setChildAssociationIndex(primaryChildAssocs.get(i), i + 1);
            }
            // On ordonne maintenant les annexes...
            for (int j = 0; j < annexesChildAssocs.size(); j++) {
                nodeService.setChildAssociationIndex(annexesChildAssocs.get(j), j + primaryChildAssocs.size() + 1);
            }
            // Les documents sont ordonnés, fin de fonction
        }
    }

    @Override
    public void reorderDocuments(NodeRef dossier) {

        UserTransaction ut = transactionService.getNonPropagatingUserTransaction(false);

        try {
            ut.begin();
            Assert.isTrue(parapheurService.isDossier(dossier), "NodeRef doit être un ph:dossier [" + dossier.getId() + "]");

            List<ChildAssociationRef> primaryChildAssocs = new ArrayList<ChildAssociationRef>();
            List<ChildAssociationRef> annexesChildAssocs = new ArrayList<ChildAssociationRef>();
            List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(dossier, ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);

            if (childAssocs != null && childAssocs.size() > 0) {
                // Si c'est le premier élément, il vient d'être ajouté... Il doit être placé à la fin !
                ChildAssociationRef firstAssoc = null;
                for (ChildAssociationRef ref : childAssocs) {

                    /* Afficher seulement les documents pas le dossier de preview mobile */
                    NodeRef childRef = ref.getChildRef();
                    QName childType = nodeService.getType(childRef);

                    if (childType.equals(ContentModel.TYPE_CONTENT)) {
                        if (firstAssoc == null) {
                            firstAssoc = ref;
                        } else if (nodeService.hasAspect(childRef, ParapheurModel.ASPECT_MAIN_DOCUMENT)) {
                            primaryChildAssocs.add(ref);
                        } else {
                            annexesChildAssocs.add(ref);
                        }
                    }
                }
                // ATTENTION ! Le premier élément vient d'être ajouté, on le repositionne à la fin...
                if (firstAssoc != null && nodeService.hasAspect(firstAssoc.getChildRef(), ParapheurModel.ASPECT_MAIN_DOCUMENT)) {
                    primaryChildAssocs.add(firstAssoc);
                } else {
                    annexesChildAssocs.add(firstAssoc);
                }
                // Maintenant qu'on a séparé documents principaux et annexes, on ordonne !
                for (int i = 0; i < primaryChildAssocs.size(); i++) {
                    // i+1 Car le premier enfant d'un dossier est le circuit !
                    nodeService.setChildAssociationIndex(primaryChildAssocs.get(i), i + 1);
                }
                // On ordonne maintenant les annexes...
                for (int j = 0; j < annexesChildAssocs.size(); j++) {
                    nodeService.setChildAssociationIndex(annexesChildAssocs.get(j), j + primaryChildAssocs.size() + 1);
                }
                // Les documents sont ordonnés, fin de fonction
            }

            ut.commit();
        } catch (Exception e) {
            try {
                ut.rollback();
            } catch (Exception e2) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void addCustomSignatureToDocument(NodeRef dossier, NodeRef document, int x, int y, int width, int height, int page) {

        java.awt.Rectangle rectangle = new Rectangle();
        rectangle.x = x;
        rectangle.y = y;
        rectangle.width = width;
        rectangle.height = height;

        List<NodeRef> maindocuments = parapheurService.getMainDocuments(dossier);
        if (maindocuments.contains(document)) {
            nodeService.setProperty(document, ParapheurModel.PROP_CUSTOM_SIGNATURE_POSITION_RECTANGLE, rectangle);
            nodeService.setProperty(document, ParapheurModel.PROP_CUSTOM_SIGNATURE_PAGE_NUMBER, page);
        }
    }

    @Override
    public void deleteCustomSignatureFromDocument(NodeRef dossier, NodeRef document) {

        List<NodeRef> maindocuments = parapheurService.getMainDocuments(dossier);
        if (maindocuments.contains(document)) {
            nodeService.removeProperty(document, ParapheurModel.PROP_CUSTOM_SIGNATURE_POSITION_RECTANGLE);
            nodeService.removeProperty(document, ParapheurModel.PROP_CUSTOM_SIGNATURE_PAGE_NUMBER);

            nodeService.removeProperty(document, ParapheurModel.PROP_SIGNATURE_TAG_POSITION_RECTANGLE);
            nodeService.removeProperty(document, ParapheurModel.PROP_SIGNATURE_TAG_PAGE_NUMBER);
        }
    }

    @Override
    public String getCustomSignaturesJsonPosition(NodeRef dossier, boolean cachet) {

        JSONStringer jsonStringer = new JSONStringer();

        // Retrieving infos

        Properties padesProperties = parapheurService.getPadesSignatureProperties(dossier);
        List<NodeRef> mainDocuments = parapheurService.getMainDocuments(dossier);

        // We get the cachet number
        int numStep = 1;
        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);
        String stepName = cachet ? "cachet" : "signature";
        for(EtapeCircuit etape : circuit) {
            numStep += etape.isApproved() && etape.getActionDemandee().equalsIgnoreCase(stepName) ? 1 : 0;
        }

        // Building result string

        try {
            jsonStringer.object();

            for (int i = 0; i < mainDocuments.size(); i++) {

                NodeRef document = mainDocuments.get(i);
                PadesVisualPreparator preparator = new PadesVisualPreparator(padesProperties, document, numStep);
                preparator.invoke(cachet);

                jsonStringer.key(String.valueOf(i));
                jsonStringer.object();
                jsonStringer.key("x").value(preparator.getX());
                jsonStringer.key("y").value(preparator.getY());
                jsonStringer.key("height").value(preparator.getHeight());
                jsonStringer.key("width").value(preparator.getWidth());
                jsonStringer.key("page").value(preparator.getPage());
                jsonStringer.endObject();
            }
            jsonStringer.endObject();
        } catch (JSONException ex) {
            jsonStringer = new JSONStringer();
        } catch (java.io.IOException ex) {
            return "Error retrieving signature position : " + ex.getLocalizedMessage();
        }

        return jsonStringer.toString();
    }

    /**
     * Method to get MimeType of a content passing it's nodeRef.
     *
     * @param documentRef a valid Document nodeId
     * @return MimeType
     */
    private
    @Nullable
    String getContentMimeType(@NotNull NodeRef documentRef) {
        ContentData contentData = (ContentData) nodeService.getProperty(documentRef, ContentModel.TYPE_CONTENT);
        return contentData.getMimetype();
    }

    private com.itextpdf.text.Image getSignatureImageForUser(String username) throws BadElementException, IOException {
        NodeRef personneRef = personService.getPerson(username);
        com.itextpdf.text.Image image = null;
        if (personneRef != null) {
            ContentReader imageReader = null;
            // On récupère l'image de signature de l'utilisateur
            if (nodeService.hasAspect(personneRef, ParapheurModel.ASPECT_SIGNATURESCAN)) {
                List<ChildAssociationRef> children = nodeService.getChildAssocs(personneRef);
                for (ChildAssociationRef child : children) {
                    if (child.getTypeQName().equals(ParapheurModel.ASSOC_SIGNATURE_SCAN)) {
                        imageReader = contentService.getReader(child.getChildRef(), ContentModel.PROP_CONTENT);
                    }
                }
            }
            if (imageReader != null) {
                // Creer un fichier temporaire de l'image
                File imageFile = TempFileProvider.createTempFile(username + "_SIGIMG", "png");
                imageReader.getContent(imageFile);
                // Recuperer l'image
                image = com.itextpdf.text.Image.getInstance(imageFile.getAbsolutePath());
            }
        }

        return image;
    }

    /**
     * Formatage des informations à afficher dans le tampon de signature
     * Normalement basé sur un modèle donné dans le fichier de configuration.
     *
     * @param name - Nom du signataire
     * @param timestampString  - Date de la signature
     * @param role  - Qualité du signataire (Titre de son bureau)
     * @return  Chaîne à afficher dans le cachet de signature
     */
    private String formatStampInfos(String name, String timestampString, String role) {
        return padesStampInformations.replaceAll("%N", name)
                .replaceAll("%D", timestampString)
                .replaceAll("%Q", role);
    }


    private class NodeComparator implements Comparator<NodeRef> {

        private int order = 1;
        private QName propSort;

        public NodeComparator(boolean ascending, QName propSort) {
            if (!ascending) {
                order = -1;
            }
            this.propSort = propSort;
        }

        public int compareType(NodeRef o1, NodeRef o2) {
            String valueType1 = (String) nodeService.getProperty(o1, ParapheurModel.PROP_TYPE_METIER);
            String valueType2 = (String) nodeService.getProperty(o2, ParapheurModel.PROP_TYPE_METIER);
            int comp = valueType1.toLowerCase().compareTo(valueType2.toLowerCase());
            if (comp == 0) {
                valueType1 = (String) nodeService.getProperty(o1, ParapheurModel.PROP_SOUSTYPE_METIER);
                valueType2 = (String) nodeService.getProperty(o2, ParapheurModel.PROP_SOUSTYPE_METIER);
                comp = valueType1.toLowerCase().compareTo(valueType2.toLowerCase());
            }
            return comp;
        }

        public int compareString(NodeRef o1, NodeRef o2) {
            return ((String) nodeService.getProperty(o1, ContentModel.PROP_TITLE)).toLowerCase().compareTo(((String) nodeService.getProperty(o2, ContentModel.PROP_TITLE)).toLowerCase());
        }

        public int compareComparable(NodeRef o1, NodeRef o2, QName propSort) {
            return ((Comparable) nodeService.getProperty(o1, propSort)).compareTo(nodeService.getProperty(o2, propSort));
        }

        public int test(NodeRef o1, NodeRef o2, QName propSort) {
            if (ParapheurModel.PROP_TYPE_METIER.equals(propSort)) {
                return compareType(o1, o2);
            } else {
                Object obj1 = nodeService.getProperty(o1, propSort);
                Object obj2 = nodeService.getProperty(o2, propSort);
                if (obj1 == null && obj2 == null) {
                    return 0;
                } else if (obj1 == null) {
                    return -1;
                } else if (obj2 == null) {
                    return 1;
                }
                if (obj1 instanceof String) {
                    return ((String) obj1).compareToIgnoreCase(((String) obj2));
                } else {
                    return ((Comparable) obj1).compareTo(obj2);
                }
            }
        }

        @Override
        public int compare(NodeRef o1, NodeRef o2) {
            return order * test(o1, o2, propSort);
        }

    }

    /**
     * Implémentation de l'interface AuditQueryCallback
     */
    class ParapheurServiceCompatCallback implements AuditService.AuditQueryCallback {

        private List<LogDossier> response;
        private int recordsRead;

        public ParapheurServiceCompatCallback() {
            response = null;
            recordsRead = 0;
        }

        public int getRecordsReadCount() {
            return this.recordsRead;
        }

        public List<LogDossier> getResponse() {
            return response;
        }

        public void setResponse(List<LogDossier> response) {
            this.response = response;
        }

        @Override
        public boolean valuesRequired() {
            return true;
        }

        @Override
        public boolean handleAuditEntry(Long entryId, String applicationName, String user, long time, Map<String, Serializable> values) {
            if (applicationName.equals("ParapheurServiceCompat")) {
                LogDossier log = new LogDossier();

                if (logger.isDebugEnabled()) {
                    logger.debug("HandlingAuditEntry(" + entryId + ", " + applicationName + ", " + user + ", " + time + ", " + values);
                    logger.debug(time + " # " + user + " # " + values.get("message"));
                }

                this.recordsRead++;


                String message = (String) (values.get("message"));

                String status;

                List<Object> list = (List<Object>) values.get("list");

                status = (String) list.get(0);

                /* FIXME: A priori n'est plus necessaire car list.get(0) renvoie bien le statut sans corchets.*/
                if (status.startsWith("[")) {  // virer les crochets de début et fin (bug sur audit alfresco?)
                    logger.debug("nettoyage sur '[' et ']'");
                    status = status.substring(1, status.length() - 1);
                }
                log.setStatus(status);
                // Timestamp
                GregorianCalendar gCalendar = new GregorianCalendar();
                Date dateFromTime = new Date(time);
                gCalendar.setTime(dateFromTime);

                XMLGregorianCalendar date;
                try {
                    date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
                } catch (DatatypeConfigurationException e) {
                    logger.debug(e.getMessage());
                    return false;
                }

                /*
                 * CMON : provoque des incoherences, par exemple un dossier
                 * dont la date de signature est posterieure a la date d'archivage, eg :
                 * <LogDossier>
                 *   <timestamp>2012-04-02T15:16:12.344+02:00</timestamp>
                 *   <status>Signe</status>
                 * </LogDossier>
                 * <LogDossier>
                 *   <timestamp>2012-04-02T15:16:12.200+02:00</timestamp>
                 *   <status>Archive</status>
                 * </LogDossier>
                 *

                 if (message.startsWith("Emission du dossier")) {
                 date.setMillisecond(1);
                 } else if (StatusMetier.STATUS_EN_COURS_TRANSMISSION.equalsIgnoreCase(status)
                 || StatusMetier.STATUS_ARCHIVE.equalsIgnoreCase(status)
                 || (StatusMetier.STATUS_NON_LU.equalsIgnoreCase(status) && message.startsWith("Dossier déposé pour"))) {
                 date.setMillisecond(200);
                 }
                 */
                log.setTimestamp(date);
                // Nom et Annotation
                String userIdentifier = user;
                String nom;
                if (userIdentifier.equalsIgnoreCase("System") && message.contains(":") && message.length() > message.indexOf(':') + 2) { //CAS PARTICULIER: ReadIndicator. Recup de l'acteur, et reformatage du texte d'annotation
                    if (logger.isDebugEnabled()) {
                        logger.debug("audit user = System; Recup de l'acteur, et reformatage du texte d'annotation");
                    }
                    userIdentifier = message.substring(0, message.indexOf(':'));
                    nom = parapheurService.getPrenomNomFromLogin(userIdentifier);
                    log.setNom((nom != null) ? nom : userIdentifier);
                    log.setAnnotation(message.substring(message.indexOf(':') + 2, message.length()));

                } else if (message.startsWith(userIdentifier) && message.contains(":") && message.length() > message.indexOf(':') + 2) { //CAS PARTICULIER: ReadIndicator. Recup de l'acteur, et reformatage du texte d'annotation
                    if (logger.isDebugEnabled()) {
                        logger.debug("Nettoyage de l'acteur, et reformatage du texte d'annotation");
                    }
                    userIdentifier = message.substring(0, message.indexOf(':'));
                    nom = parapheurService.getPrenomNomFromLogin(userIdentifier);
                    log.setNom((nom != null) ? nom : userIdentifier);
                    log.setAnnotation(message.substring(message.indexOf(':') + 2, message.length()));
                } else {
                    nom = parapheurService.getPrenomNomFromLogin(userIdentifier);
                    log.setNom((nom != null) ? nom : userIdentifier);
                    log.setAnnotation(message);
                }
                if (message.startsWith("Création de dossier")) {
                    response.add(0, log); //on force la creation de dossier en premier événement
                } else {
                    response.add(log);
                }
            }

            return true;
        }

        @Override
        public boolean handleAuditEntryError(Long entryId, String errorMsg, Throwable error) {
            //FIXME: Gérer les erreurs mieux.
            return true;
        }
    }

    public final class SignatureInformations {

        private String action = "parapheur_approve";
        private NodeRef dossierRef = null;
        private NodeRef bureauCourant = null;
        private String annotPub = "";
        private String annotPriv = "";
        private String p7sSignatureString = null;
        private String pPesId = null, pPolicyIdentifierID = null, pPolicyIdentifierDescription = null,
                pPolicyDigest = null, pSPURI = null, pCity = null,
                pPostalCode = null, pCountryName = null, pClaimedRole = null, pEncoding = null;

        public SignatureInformations(NodeRef dossierRef, String annotPub, String annotPriv, NodeRef bureauCourant) {
            this.dossierRef = dossierRef;
            this.annotPub = annotPub;
            this.annotPriv = annotPriv;
            this.bureauCourant = bureauCourant;
        }

        public SignatureInformations(NodeRef dossierRef) {
            this.dossierRef = dossierRef;
        }

        public SignatureInformations(NodeRef dossierRef, NodeRef bureauCourant) {
            this.dossierRef = dossierRef;
            this.bureauCourant = bureauCourant;
        }

        public Map<String, String> getInformations() {
            Map<String, String> ret = new HashMap<String, String>();
            ret.put("hash", getShaDigestToSign());
            ret.put("pesid", getPesId());
            ret.put("pespolicyid", getPolicyIdentifierID());
            ret.put("pespolicydesc", getPolicyIdentifierDescription());
            ret.put("pespolicyhash", getPolicyDigest());
            ret.put("pesspuri", getSPURI());
            ret.put("pescity", getCity());
            ret.put("pespostalcode", getPostalCode());
            ret.put("pescountryname", getCountryName());
            ret.put("pesclaimedrole", getClaimedRole());
            ret.put("p7s", getP7sSignatureString());
            ret.put("pesencoding", getEncoding());
            ret.put("format", getSignatureFormatPourApplet());
            return ret;
        }

        public String sign(String signature) throws Exception {
            if (!action.isEmpty()) {
                parapheurService.setAnnotationPublique(dossierRef, annotPub);
                parapheurService.setAnnotationPrivee(dossierRef, annotPriv);

                if (action.equals("parapheur_approve")
                        || action.equals("parapheur_sign")) {
                    X509CertificateHolder[] signatureCerts = null;//= certs;

                    if (isInSignatureMode()) {
                        byte[] laSignature; // = Base64.decode(signature);
                        /**
                         * en cas de XAdES enveloppé multi IDs, on a plusieurs
                         * signatures (par exemple si PESv2).
                         * Alors on ne decode pas tout de suite dans ce cas précis!!!
                         */
                        laSignature = signature.getBytes();

                        if (laSignature != null) {
                            String sigFormat = ((String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT));
                            System.out.print("##(" + sigFormat + ")## ");
                            /**
                             * Hop hop hop !!! Ajout 4.3, si on signe pades, les aperçus générés ne sont plus valides (il n'y a pas le tampon d'affiché....)
                             * On doit supprimer les aperçus générés ... Erf
                             */
                            List<NodeRef> documents = parapheurService.getMainDocuments(dossierRef);
                            if (StringUtils.startsWithIgnoreCase(sigFormat, "PAdES")) {
                                for (NodeRef doc : documents) {
                                    deleteImagesPreviews(doc, dossierRef);
                                    nodeService.removeProperty(doc, ParapheurModel.PROP_VISUEL_PDF);
                                }
                                deleteSwfPreviewsForDossier(dossierRef);
                            }
                            if (StringUtils.startsWithIgnoreCase( sigFormat, "pkcs#7") ||
                                    StringUtils.startsWithIgnoreCase(sigFormat, "PAdES")) {
                                String newSig = "";
                                boolean first = true;


                                String[] sigs = signature.split(",");
                                List<X509CertificateHolder> listCerts = new ArrayList<>();
                                for(int i = 0; i < sigs.length; i++) {
                                    String sig = sigs[i];
                                    if (!first) {
                                        newSig += ",";
                                    } else {
                                        first = false;
                                    }
                                    byte[] decodedSig = Base64.decode(sig);

                                    if(StringUtils.startsWithIgnoreCase(sigFormat, "pkcs#7")) {
                                        // Here, we have decoded the signature and we can add it
                                        signatureService.setDetachedSignature(documents.get(i).getId(), new ByteArrayInputStream(decodedSig), "application/pkcs7-signature");
                                    }

                                    newSig += new String(decodedSig);

                                    //System.out.println(new String());
                                    byte[] derSign = PKCS7VerUtil.pem2der(decodedSig, "-----BEGIN".getBytes(), "-----END".getBytes());
                                    listCerts.add(PKCS7VerUtil.getSignatureCertificateHolder(null, derSign));
                                }
                                signatureCerts = new X509CertificateHolder[listCerts.size()];
                                signatureCerts = listCerts.toArray(signatureCerts);
                                laSignature = newSig.getBytes();
                            } else if (StringUtils.startsWithIgnoreCase(sigFormat, "pkcs#1")) {
                                if (signature.contains(";") && signature.split(";").length == 2) {
                                    signatureCerts = new X509CertificateHolder[]{
                                            new X509CertificateHolder(Base64.decode(signature.split(";")[1].getBytes()))
                                    };
                                }
                            } else if (sigFormat.equalsIgnoreCase("xades/detached")) {

                                String[] sigs = signature.split(",");
                                for(int i = 0; i < sigs.length; i++) {
                                    signatureService.setDetachedSignature(documents.get(i).getId(), new ByteArrayInputStream(Base64.decode(sigs[i])), "application/xml");
                                }
                                signature = sigs[0];

                                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                                dbf.setNamespaceAware(true);
                                org.w3c.dom.Document doc = dbf.newDocumentBuilder().parse(new ByteArrayInputStream(Base64.decode(signature)));

                                // Find Signature element.
                                NodeList nl = doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");

                                Node sigNode = nl.item(0);

                                NodeList x509nl = doc.getElementsByTagNameNS(XMLSignature.XMLNS, "X509Certificate");

                                CertificateFactory fact = CertificateFactory.getInstance("X.509");
                                ByteArrayInputStream in = new ByteArrayInputStream(org.bouncycastle.util.encoders.Base64.decode(x509nl.item(0).getTextContent()));
                                X509Certificate cer = (X509Certificate) fact.generateCertificate(in);

                                signatureCerts = new X509CertificateHolder[]{
                                        new X509CertificateHolder(cer.getEncoded())
                                };
                                laSignature = Base64.decode(signature);
                            }
                            parapheurService.setSignature(dossierRef, laSignature, sigFormat);
                            parapheurService.setSignature(dossierRef, parapheurService.getCurrentEtapeCircuit(dossierRef), laSignature, sigFormat);
                        } else {
                            throw new RuntimeException("L'operation de signature n'a pas ete effectuee");
                        }
                    }

                    // Le signataire enregistré doit être le propriétaire du parapheur (cas de signature papier)
                    NodeRef parapheurRef = parapheurService.getParentParapheur(dossierRef);
                    String username = AuthenticationUtil.getRunAsUser();
                    String fullname = parapheurService.getPrenomNomFromLogin(username);

                    String signataire;
                    if (parapheurService.isParapheurSecretaire(parapheurRef, username)) {
                        //NodeRef ownerRef = this.personService.getPerson();
                        signataire = String.format("%s pour le compte de \"%s\"", fullname, parapheurService.getNomParapheur(parapheurRef));
                    } else if (parapheurService.isParapheurDelegate(parapheurRef, username) && !parapheurService.isParapheurOwner(parapheurRef, username)) {
                        signataire = String.format(configuration.getProperty("parapheur.suppleance.text"), fullname + ", " + parapheurService.getNomParapheur(bureauCourant), parapheurService.getNomParapheur(parapheurRef));
                    } else {
                        signataire = fullname;
                    }

                    parapheurService.setSignataire(dossierRef, signataire, signatureCerts);
                    parapheurService.approveV4(dossierRef, bureauCourant);
                } else if (action.equals("parapheur_secretariat")) {
                    parapheurService.secretariat(dossierRef);
                }
            }
            return null;
        }

        private List<byte[]> getBytesToSignForMultiDoc() {
            List<byte[]> hashes = new ArrayList<byte[]>();
            for (NodeRef doc : parapheurService.getMainDocuments(dossierRef)) {
                // Pour être sûr...
                if (nodeService.hasAspect(doc, ParapheurModel.ASPECT_MAIN_DOCUMENT)) {
                    ContentReader reader = contentService.getReader(doc, ContentModel.PROP_CONTENT);
                    //  on se limite à Integer.MAX_VALUE, quid si le fichier est plus gros ?
                    // Solution: on envoie à signer un HASH SHA1 !! (v2.1.5+)
                    byte[] bytesToSign = new byte[(int) reader.getSize()];
                    try {
                        reader.getContentInputStream().read(bytesToSign);
                    } catch (IOException e) {
                        bytesToSign = null;
                    }
                    hashes.add(bytesToSign);
                }
            }
            return hashes;
        }

        private byte[] removeWhiteSpace(byte[] buf) {
            ByteArrayOutputStream output;

            output = new ByteArrayOutputStream();
            for (byte aBuf : buf) {
                switch (aBuf) {
                    case '\r':
                    case '\n':
                    case 0x1A:
                        // ignore this character
                        break;

                    default:
                        output.write(aBuf);
                }
            }

            return output.toByteArray();
        }

        private byte[] getBytesToSign() {
            ContentReader reader = contentService.getReader(parapheurService.getMainDocuments(dossierRef).get(0),
                    ContentModel.PROP_CONTENT);
            //  on se limite à Integer.MAX_VALUE, quid si le fichier est plus gros ?
            // Solution: on envoie à signer un HASH SHA1 !! (v2.1.5+)
            byte[] bytesToSign = new byte[(int) reader.getSize()];
            try {
                reader.getContentInputStream().read(bytesToSign);
            } catch (IOException e) {
                bytesToSign = null;
            }
            return bytesToSign;
        }

        /**
         * Fournit le format de signature tel qu'attendu par LiberSign.
         * <p>
         * A partir des formats tels que connus de i-Parapheur: PKCS#7/single
         * PKCS#7/multiple XAdES/enveloped XAdES/DIA XAdES/detached
         * XAdES132/detached XAdES-T/enveloped
         *
         * @return format pour applet
         * @throws UnsupportedOperationException si format inconnu
         */
        public String getSignatureFormatPourApplet() throws UnsupportedOperationException {
            String format = "cms";
            String sigFormat = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT);

            if ("PKCS#7/single".equals(sigFormat)) {
                format = SignatureFormats.CMS_PKCS7; // "CMS";
            } else if ("PKCS#7/multiple".equals(sigFormat)) {
                format = SignatureFormats.CMS_PKCS7_Ain1; // "CMS-Allin1";
            } else if ("PAdES/basic".equals(sigFormat)) {
                format = SignatureFormats.PADES_ISO32000_1; // "PADES-basic";
            } else if ("PAdES/basicCertifie".equals(sigFormat)) {
                format = SignatureFormats.PADES_ISO32000_1; // "PADES-basic";
            } else if ("XAdES/enveloped".equals(sigFormat)) {
                format = SignatureFormats.XADES_EPES_ENV_PESv2_SHA256; // "XADES-env";
            } else if ("XAdES/DIA".equals(sigFormat)) {
                format = SignatureFormats.XADES_EPES_ENV_DIA; // "XADES-env-xpath";
            } else if ("XAdES/detached".equals(sigFormat)) {
                format = SignatureFormats.XADES_EPES_DET_1_1_1; // "XADES";
            } else if ("XAdES132/detached".equals(sigFormat)) {
                format = SignatureFormats.XADES_EPES_DET_1_3_2; // "XADES132";
            } else if ("XAdES-T/enveloped".equals(sigFormat)) {
                format = SignatureFormats.XADES_T_EPES_ENV_1_3_2; // "XADES-T-env";
            } else if ("PKCS#1/sha256".equals(sigFormat)) {
                format = SignatureFormats.PKCS1_SHA256_RSA; // "PKCS1_SHA256_RSA";
            } else {
                throw new UnsupportedOperationException("Unknown signature format: " + sigFormat);
            }

            return format;
        }

        /**
         * Calcule le haché d'un fichier et le restitue sous forme de tableau
         * d'octets.
         *
         * @param fileToDigest    le fichier à hacher
         * @param digestAlgorithm l'algorithme de hachage que l'on souhaite
         *                        utiliser : "SHA1", "MD5", etc...
         * @param bufSize         Taille du buffer de lecture
         * @return le hash du fichier
         * @throws java.io.IOException IOException
         *                             CryptoException
         */
        public final byte[] getFileDigest(final File fileToDigest, final String digestAlgorithm, final int bufSize) throws IOException {
            Security.addProvider(new BouncyCastleProvider());    // CryptoUtil.addBouncyCastle();
            FileInputStream givenStream = new FileInputStream(fileToDigest);
            MessageDigest digest;
            byte[] hash = null;
            try {
                digest = MessageDigest.getInstance(digestAlgorithm, "BC");
                // @SuppressWarnings("unused")
                int c = 0;
                final byte[] buffer = new byte[bufSize];
                while ((c = givenStream.read(buffer)) != -1) {
                    digest.update(buffer);
                }
                givenStream.close();
                hash = digest.digest();
            } catch (NoSuchAlgorithmException e) {
                logger.error("getFileDigest::NoSuchAlgorithmException ", e);
            } catch (NoSuchProviderException e) {
                logger.error("getFileDigest::NoSuchProviderException ", e);
            }
            return hash;
        }

        /**
         * Calcule le condensat selon l'algo passé en paramètre
         *
         * @param bytesToDigest
         * @param digestAlgorithm "SHA1" ou "SHA256" sont supportés
         * @return
         */
        public final byte[] getBytesDigest(final byte[] bytesToDigest, final String digestAlgorithm) {
            Security.addProvider(new BouncyCastleProvider());    // CryptoUtil.addBouncyCastle();

            MessageDigest digest;
            byte[] hash = null;
            try {
                digest = MessageDigest.getInstance(digestAlgorithm, "BC");
                digest.update(bytesToDigest);
                hash = digest.digest();
            } catch (NoSuchAlgorithmException e) {
                logger.error("getFileDigest::NoSuchAlgorithmException ", e);
            } catch (NoSuchProviderException e) {
                logger.error("getFileDigest::NoSuchProviderException ", e);
            }
            return hash;
        }

        public String getShaDigestToSign() {
            return getShaDigestToSign(false);
        }

        /**
         * Calcul du hash (condensat) necessaire à la signature electronique.
         *
         * @return le condensat formatté hexa (base64?)
         */
        public String getShaDigestToSign(boolean hasError) {

            byte[] myhash = null;

            boolean isMultiDoc = typesService.isMultiDocument(
                    (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER),
                    (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SOUSTYPE_METIER));

            Map<QName, Serializable> typeProps = parapheurService.getTypeMetierProperties((String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER));
            String signatureFormat = getSignatureFormatPourApplet();
            if (signatureFormat.startsWith("PADES")) {
                /**
                 * Cas de format: PAdES.
                 */
                logger.info("  On veut signer en PAdES ISO 32000-1...");
                // WARN: plus tard, on aura plusieurs doc principaux:  for (NodeRef docPrincipalRef : docPrincipalRefs) {
                List<NodeRef> docPrincipalRefs = parapheurService.getMainDocuments(dossierRef);

                // Récupération du bureau parent
                NodeRef bureauParent = parapheurService.getParentParapheur(dossierRef);
                String delegRole = "", ownerRole, city;
                // L'action est-elle faite en délégation ?
                boolean isDelegate = (bureauCourant != null && !bureauParent.equals(bureauCourant));
                String nomCourant = bureauCourant != null ? parapheurService.getNomParapheur(bureauCourant) : parapheurService.getNomParapheur(bureauParent);

                // We get the signature number
                int numSignature = 1;
                List<EtapeCircuit> circuit = parapheurService.getCircuit(dossierRef);
                for(EtapeCircuit etape : circuit) {
                    numSignature += etape.isApproved() && etape.getActionDemandee().equalsIgnoreCase("signature") ? 1 : 0;
                }

                boolean first = true;
                // serialisation des Digests
                StringBuilder hashesBuilder = new StringBuilder();
                for (NodeRef docPrincipalRef : docPrincipalRefs) {
                    if (!isMultiDoc && !first) {
                        break;
                    }
                    try {
                        String username = AuthenticationUtil.getRunAsUser();
                        Properties padesProperties = parapheurService.getPadesSignatureProperties(dossierRef);
                        //Si la propriété est à true dans le fichier de config
                        if (configuration.containsKey("parapheur.claimedRole.userMetadataBased") &&
                                configuration.get("parapheur.claimedRole.userMetadataBased").equals("true")) {
                            if (isDelegate) {
                                List<String> owners = parapheurService.getParapheurOwners(bureauParent);
                                if (owners.isEmpty() || owners.size() > 1) {
                                    delegRole = parapheurService.getNomParapheur(bureauParent);
                                } else {
                                    NodeRef deleg = personService.getPerson(owners.get(0));
                                    String metadatas = (String) nodeService.getProperty(deleg, ContentModel.PROP_ORGID);
                                    Map<String, String> metas = CollectionsUtils.parseMetadata(metadatas);
                                    delegRole = metas.containsKey("TITRE") ? metas.get("TITRE") : parapheurService.getNomParapheur(bureauParent);
                                }
                            }

                            NodeRef owner = personService.getPerson(username);
                            String metadatas = (String) nodeService.getProperty(owner, ContentModel.PROP_ORGID);
                            Map<String, String> metas = CollectionsUtils.parseMetadata(metadatas);
                            ownerRole = CollectionsUtils.getString(metas, "TITRE", nomCourant);
                            city = CollectionsUtils.getString(metas, "VILLE", (String) typeProps.get(ParapheurModel.PROP_SIGNATURE_LOCATION));
                        } else {
                            if (isDelegate) {
                                delegRole = parapheurService.getNomParapheur(bureauParent);
                            }
                            ownerRole = nomCourant;
                            city = (String) typeProps.get(ParapheurModel.PROP_SIGNATURE_LOCATION);
                        }
                        String role = isDelegate ?
                                String.format(configuration.getProperty("parapheur.suppleance.text"), ownerRole, delegRole) :
                                ownerRole;
                        String sigFormat = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT);
                        Calendar signDate = Calendar.getInstance();
                        // signDate.set(Calendar.HOUR_OF_DAY, 0);
                        // signDate.set(Calendar.MINUTE, 0);
                        signDate.set(Calendar.SECOND, 0);
                        signDate.set(Calendar.MILLISECOND, 0);
                        StringBuilder builder = new StringBuilder();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        dateFormat.setTimeZone(signDate.getTimeZone());
                        String timestampString = dateFormat.format(signDate.getTime());
                        builder.append(formatStampInfos(parapheurService.getPrenomNomFromLogin(username),
                                timestampString, role));
                        String creatorApplication = "i-Parapheur " + parapheurVersionProperties.getVersionNumber();

                        // on prépare le PDF à recevoir une signature: stocké en VISUEL PDF !
                        ContentWriter pdfwriter = contentService.getWriter(docPrincipalRef, ParapheurModel.PROP_SIGNATURE_PDF, Boolean.TRUE);
                        pdfwriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
                        pdfwriter.setEncoding("UTF-8");
                        OutputStream fout = pdfwriter.getContentOutputStream();

                        // On prépare le visuel PADES
                        PadesVisualPreparator padesVisualPreparator = new PadesVisualPreparator(padesProperties, docPrincipalRef, numSignature).invoke(false);
                        ContentReader tmpReader = contentService.getReader(docPrincipalRef, ContentModel.PROP_CONTENT);

                        // Do the hash !!
                        PadesServerHelper.DocumentHasher hasher = new PadesServerHelper.DocumentHasher(role,
                                city,
                                signDate,
                                builder.toString(),
                                creatorApplication,
                                tmpReader.getContentInputStream(),
                                fout,
                                getSignatureImageForUser(username));

                        hasher.setVisuel(
                                padesVisualPreparator.getX(),
                                padesVisualPreparator.getY(),
                                padesVisualPreparator.getWidth(),
                                padesVisualPreparator.getHeight(),
                                padesVisualPreparator.getFontSize(),
                                padesVisualPreparator.getPage());

                        // On certifie seulement si la typologie le prévoit !!! => Dossier bloqué pour autre(s) signature(s) !
                        hasher.setCertified(sigFormat.equals(ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_PADES_ISO32000_1_CERT));

                        myhash = hasher.hashDocument();
                        fout.close();

                        if (first) {
                            first = false;
                        } else {
                            hashesBuilder.append(",");
                        }

                        StringBuilder strBuilder = new StringBuilder();
                        if (myhash == null) {
                            hashesBuilder.append(" ");
                        } else {
                            for (byte dataByte : myhash) {
                                strBuilder.append(Integer.toHexString((dataByte & 0xF0) >> 4));
                                strBuilder.append(Integer.toHexString(dataByte & 0x0F));
                            }
                            hashesBuilder.append(strBuilder.toString());
                        }
                    } catch (IOException | DocumentException ex) {
                        logger.error(ex.getLocalizedMessage());
                        if(!hasError) {
                            File toUnlock = ((FileContentReader) contentService.getReader(docPrincipalRef, ContentModel.PROP_CONTENT)).getFile();
                            File target = TempFileProvider.createTempFile("unlocking", ".pdf");
                            PopplerUtils utils = new PopplerUtils();
                            boolean result = utils.unlock(toUnlock.getAbsolutePath(), target.getAbsolutePath());
                            if(!result) {
                                // return right now...
                                logger.error("Cannot unlock PDF document, continue...");
                                continue;
                            }
                            contentService.getWriter(docPrincipalRef, ContentModel.PROP_CONTENT, true).putContent(target);

                            if (!target.delete()) {
                                logger.error("Cannot delete tmp file when unlocking in getShaDigestToSign");
                            }
                            return getShaDigestToSign(true);
                        }
                    } catch (NoSuchAlgorithmException ex) {
                        logger.error(ex.getLocalizedMessage());
                        System.err.println("OUCH2");
                    } catch (NoSuchProviderException ex) {
                        logger.error(ex.getLocalizedMessage());
                        System.err.println("OUCH3");
                    }
                }

                return hashesBuilder.toString();
                // myhash = null;

            } else if (signatureFormat.equalsIgnoreCase("PKCS1_SHA256_RSA")) {
                // Un seul document à signer, on récupère le premier principal
                NodeRef maindoc = parapheurService.getMainDocuments(dossierRef).get(0);
                ContentReader reader = contentService.getReader(maindoc, ContentModel.PROP_CONTENT);
                if (reader == null) {
                    return null;
                }
                java.io.InputStream is = reader.getContentInputStream();
                if (is == null) {
                    return null;
                }

                String path = "";
                try {
                    String ticket = usersService.getCurrentUserTicket();

                    path = "https://" + configuration.getProperty("parapheur.mail.baseUrl").replace("/iparapheur",
                            "/alfresco/wcs/api/node/workspace/SpacesStore/" +
                                    maindoc.getId() +
                                    "/content/" +
                                    nodeService.getProperty(maindoc, ContentModel.PROP_NAME) +
                                    "?ticket=" +
                                    ticket);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return path;
            } else if (!signatureFormat.equalsIgnoreCase("pesv2") && (!signatureFormat.toLowerCase().startsWith("xades") || signatureFormat.toLowerCase().equals("xades"))) {
                /**
                 * Cas de formats: CMS, CMS-Allin1 ou XADES-detaché
                 */
                List<byte[]> bytesToSign = getBytesToSignForMultiDoc();
                boolean first = true;

                // serialisation des Digests
                StringBuilder hashesBuilder = new StringBuilder();
                for (byte[] aBytesToSign : bytesToSign) {
                    if (!isMultiDoc && !first) {
                        break;
                    }
                    if (first) {
                        first = false;
                    } else {
                        hashesBuilder.append(",");
                    }

                    if (signatureFormat.equalsIgnoreCase("xades")) {
                        myhash = getBytesDigest(aBytesToSign, "SHA256");
                    } else {
                        myhash = getBytesDigest(aBytesToSign, "SHA1");
                    }

                    StringBuilder builder = new StringBuilder();
                    if (myhash == null) {
                        hashesBuilder.append(" ");
                    } else {
                        for (byte dataByte : myhash) {
                            builder.append(Integer.toHexString((dataByte & 0xF0) >> 4));
                            builder.append(Integer.toHexString(dataByte & 0x0F));
                        }
                        hashesBuilder.append(builder.toString());
                    }
                }
                if (signatureFormat.equalsIgnoreCase("CMS-Allin1")) {
                    // Il faut donner la signature courante!
                    setP7sSignatureString(dossierRef);
                }
                return hashesBuilder.toString();
            } else { // cas: "PESv2", "XADES", "XADES-env" "XADES-env-xpath" "XADES-T-env" voir specs Libersign
                /**
                 * Formats: "XADES", "XADES-env" "XADES-env-xpath" "XADES-T-env".
                 */
                // Un seul document à signer, on récupère le premier principal
                ContentReader reader = contentService.getReader(parapheurService.getMainDocuments(dossierRef).get(0),
                        ContentModel.PROP_CONTENT);
                if (reader == null) {
                    return null;
                }
                java.io.InputStream is = reader.getContentInputStream();
                if (is == null) {
                    return null;
                }

                if (signatureFormat.equalsIgnoreCase("XADES-env-xpath")) {  // Cas particulier signature DIA
                    /**
                     * Pour ce cas TRES particulier, le flux va descendre en
                     * entier. On canonicalise, avant d'envoyer
                     */
                    try {
                        java.io.ByteArrayOutputStream baStream = new java.io.ByteArrayOutputStream();
                        nu.xom.Builder builder = new nu.xom.Builder();
                        nu.xom.canonical.Canonicalizer canonicalizer = new nu.xom.canonical.Canonicalizer(baStream,
                                nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);
                        logger.debug("c4 ");
                        nu.xom.Document document = builder.build(is);
                        if (null == document.getRootElement()) {
                            this.setPesV2SignParameters(null, dossierRef);
                            return null;
                        }
                        this.setPesV2SignParameters(document.getRootElement().getAttributeValue("Id"), dossierRef);
                        canonicalizer.write(document);
                        myhash = baStream.toByteArray();
                    } catch (nu.xom.ParsingException ex) {
                        logger.error(ex);
                        return null;
                    } catch (IOException ex) {
                        logger.error(ex);
                        return null;
                    }

                    NodeRef parapheurRef = parapheurService.getParentParapheur(dossierRef);
                    // On chope la DESCRIPTION construite ainsi: "pCodePostal pCity"
                    String descriptionString = (String) nodeService.getProperty(parapheurRef, ContentModel.PROP_DESCRIPTION);
                    if (descriptionString != null) {
                        descriptionString = descriptionString.trim();
                        if (descriptionString.length() > 5) {
                            pPostalCode = descriptionString.substring(0, 5);
                            pCity = descriptionString.substring(5).trim();
                        }
                    }

                } else if (signatureFormat.equalsIgnoreCase("XADES132")) {  // utilise pour ANTS-ECD
                    /*
                     * en attendant mieux, ici on impose le format SHA-256.
                     * C'est péremptoire, mais j'assume ce couplage fort.
                     *
                     * Amusant: on la joue comme pour PKCS7: on presume qu'il ne s'agit pas de XML...
                     */
                    // TODO : un jour, détecter le format source (XML vs non-XML) pour la canonicalisation éventuelle
                    // FIXME : pas besoin de signature policy ???
                    myhash = getBytesDigest(getBytesToSign(), "SHA-256");

                } else {  // Cas general de XAdES (non DIA, ni ANTS-ECD)
                    String xPath = parapheurService.getXPathSignature(dossierRef);
                    if (null == xPath) {
                        // ceci est un garde-fou, contre les fous qui oublient le xpath
                        xPath = ".";
                        //logger.error(signatureFormat + ": getXPathSignature est null???");
                        System.out.println(signatureFormat + ": getXPathSignature est null???");
                        nodeService.setProperty(dossierRef, ParapheurModel.PROP_XPATH_SIGNATURE, xPath);
                    }

                    //                        PesDigest pesDigest = DigestComputer.computeDigest(is,
                    //                                xPath);
                    //                        if (pesDigest == null) {
                    //                            this.setPesV2SignParameters(null, dossierRef);
                    //                            return null;
                    //                        }
                    //                        this.setPesV2SignParameters(pesDigest.getId(), dossierRef);
                    //                        myhash = pesDigest.getDigest();

                    /*
                     * Changement d'algo pour support Xpath multi-resultats
                     */
                    String xPathSignature = parapheurService.getXPathSignature(dossierRef);
                    System.out.println("xPathSignature = " + xPathSignature);
                    List<PesDigest> pesDigest = DigestComputer.computeDigests(is, xPathSignature);
                    if (pesDigest == null) {
                        this.setPesV2SignParameters(null, dossierRef);
                        return null;
                    } else {
                        int nbre = pesDigest.size();
                        boolean first = true;

                        // serialisation des ID de PES
                        StringBuilder idBuilder = new StringBuilder();
                        for (int i = 0; i < nbre; i++) {
                            if (first) {
                                first = false;
                            } else {
                                idBuilder.append(",");
                            }
                            idBuilder.append(pesDigest.get(i).getId());
                        }
                        this.setPesV2SignParameters(idBuilder.toString(), dossierRef);

                        // serialisation des Digests
                        StringBuilder hashesBuilder = new StringBuilder();
                        first = true;
                        for (int i = 0; i < nbre; i++) {
                            if (first) {
                                first = false;
                            } else {
                                hashesBuilder.append(",");
                            }

                            myhash = pesDigest.get(i).getDigest();

                            StringBuilder builder = new StringBuilder();
                            if (myhash == null) {
                                hashesBuilder.append(" ");
                            } else {
                                for (byte dataByte : myhash) {
                                    builder.append(Integer.toHexString((dataByte & 0xF0) >> 4));
                                    builder.append(Integer.toHexString(dataByte & 0x0F));
                                }
                                hashesBuilder.append(builder.toString());
                            }
                        }
                        return hashesBuilder.toString();
                    }

                }
            }

            StringBuilder builder = new StringBuilder();
            if (myhash != null) {
                for (byte dataByte : myhash) {  // Un octet est représenté par 2 caractères hexadécimaux
                    builder.append(Integer.toHexString((dataByte & 0xF0) >> 4));
                    builder.append(Integer.toHexString(dataByte & 0x0F));
                }
            }

            return builder.toString();
        }

        public String getP7sSignatureString() {
            return p7sSignatureString;
        }

        private void setP7sSignatureString(NodeRef dossierRef) {
            // Attraper la signature courante du dossier...
            byte[] p7Sig = parapheurService.getSignature(dossierRef);
            if (p7Sig != null) {
                //p7sSignatureString = new String(Base64.encodeBytes(p7Sig));
                p7sSignatureString = Base64.encodeBytes(p7Sig);
                //            StringBuilder builder=new StringBuilder();
                //            for(byte dataByte: p7Sig)
                //            {  // Un octet est représenté par 2 caractères hexadécimaux
                //               builder.append(Integer.toHexString((dataByte & 0xF0)>>4));
                //               builder.append(Integer.toHexString(dataByte & 0x0F));
                //            }
                //            p7sSignatureString = builder.toString();
            } else {
                p7sSignatureString = "null";
            }
        }

        private void setPesV2SignParameters(String docId, NodeRef dossierRef) {
            pPesId = docId;
            Properties props = parapheurService.getXadesSignatureProperties(dossierRef);
            pCountryName = props.getProperty("pCountryName");
            // Le ROLE du signataire dans une signature XAdES est le TITRE de son Parapheur.
            String typeMetier = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER);
            Map<QName, Serializable> typeProps = parapheurService.getTypeMetierProperties(typeMetier);
            String tdtName = (String) typeProps.get(ParapheurModel.PROP_TDT_NOM);

            if (tdtName != null && tdtName.equals(FastServiceImpl.PROP_TDT_NOM_FAST)) {
                pClaimedRole = props.getProperty("pClaimedRole");
                pCity = (String) typeProps.get(ParapheurModel.PROP_SIGNATURE_LOCATION);
                pPostalCode = (String) typeProps.get(ParapheurModel.PROP_SIGNATURE_POSTALCODE);
            } else {
                String delegRole = "", ownerRole = "", city, cp;
                // Récupération du bureau parent
                NodeRef bureauParent = parapheurService.getParentParapheur(dossierRef);
                // L'action est-elle faite en délégation ?
                boolean isDelegate = (bureauCourant != null && !bureauParent.equals(bureauCourant));

                String nomCourant = bureauCourant != null ? parapheurService.getNomParapheur(bureauCourant) : parapheurService.getNomParapheur(bureauParent);

                //Si la propriété est à true dans le fichier de config
                if (configuration.containsKey("parapheur.claimedRole.userMetadataBased") && configuration.get("parapheur.claimedRole.userMetadataBased").equals("true")) {
                    String username = AuthenticationUtil.getRunAsUser();
                    // Si il s'agit d'une signature en délégation
                    if (isDelegate) {
                        List<String> owners = parapheurService.getParapheurOwners(bureauParent);
                        if (owners.isEmpty() || owners.size() > 1) {
                            delegRole = parapheurService.getNomParapheur(bureauParent);
                        } else {
                            NodeRef deleg = personService.getPerson(owners.get(0));
                            String metadatas = (String) nodeService.getProperty(deleg, ContentModel.PROP_ORGID);
                            Map<String, String> metas = CollectionsUtils.parseMetadata(metadatas);
                            delegRole = metas.containsKey("TITRE") ? metas.get("TITRE") : parapheurService.getNomParapheur(bureauParent);
                        }
                    }
                    NodeRef owner = personService.getPerson(username);
                    String metadatas = (String) nodeService.getProperty(owner, ContentModel.PROP_ORGID);
                    Map<String, String> metas = CollectionsUtils.parseMetadata(metadatas);
                    ownerRole = metas.containsKey("TITRE") ? metas.get("TITRE") : nomCourant;
                    city = metas.containsKey("VILLE") ? metas.get("VILLE") : (String) typeProps.get(ParapheurModel.PROP_SIGNATURE_LOCATION);
                    cp = metas.containsKey("CODEPOSTAL") ? metas.get("CODEPOSTAL") : (String) typeProps.get(ParapheurModel.PROP_SIGNATURE_POSTALCODE);
                } else {
                    if (isDelegate) {
                        delegRole = parapheurService.getNomParapheur(bureauParent);
                    }
                    ownerRole = nomCourant;
                    city = (String) typeProps.get(ParapheurModel.PROP_SIGNATURE_LOCATION);
                    cp = (String) typeProps.get(ParapheurModel.PROP_SIGNATURE_POSTALCODE);
                }
                pClaimedRole = isDelegate ?
                        String.format(configuration.getProperty("parapheur.suppleance.text"), ownerRole, delegRole) :
                        ownerRole;
                pCity = city;
                pPostalCode = cp;
            }
            /* les lignes suivantes sont inutiles...
             * Map<String, String> policyProps = parapheurService.getSignaturePolicy(null, tdtName);
             * pPolicyIdentifierID = policyProps.get("pPolicyIdentifierID");
             * pPolicyIdentifierDescription = policyProps.get("pPolicyIdentifierDescription");
             * pSPURI = policyProps.get("pSPURI");
             * pPolicyDigest = policyProps.get("pPolicyDigest"); */
            pPolicyIdentifierID = props.getProperty("pPolicyIdentifierID");
            pPolicyIdentifierDescription = props.getProperty("pPolicyIdentifierDescription");
            pSPURI = props.getProperty("pSPURI");
            pPolicyDigest = props.getProperty("pPolicyDigest");

            pEncoding = getFileEncoding();
        }

        public Map<String, String> getPesV2SignParameters() {
            // HERE - Test if signature format is xades detached, if so, send document name as docId
            this.setPesV2SignParameters(null, this.dossierRef);

            HashMap<String, String> result = new HashMap<String, String>();

            result.put("pescity", this.pCity);
            result.put("pespostalcode", this.pPostalCode);
            result.put("pesclaimedrole", this.pClaimedRole);

            return result;
        }

        public String getPesId() {
            return pPesId;
        }

        public String getPolicyIdentifierID() {
            return pPolicyIdentifierID;
        }

        public String getPolicyIdentifierDescription() {
            return pPolicyIdentifierDescription;
        }

        public String getPolicyDigest() {
            return pPolicyDigest;
        }

        public String getSPURI() {
            return pSPURI;
        }

        public String getCity() {
            return pCity;
        }

        public String getPostalCode() {
            return pPostalCode;
        }

        public String getCountryName() {
            return pCountryName;
        }

        public String getClaimedRole() {
            return pClaimedRole;
        }

        public String getEncoding() {
            return pEncoding;
        }

        public String getFileEncoding() {
            // Dans le cas du PES ? un seul doc principal !
            ContentReader reader = contentService.getReader(parapheurService.getMainDocuments(dossierRef).get(0),
                    ContentModel.PROP_CONTENT);

            return reader.getEncoding();
        }

        /**
         * not used ? ça tombe bien!!
         *
         * @return
         * @deprecated
         */
        public String getBase64BytesToSign() {
            if (this.isInSignatureMode()) {
                return Base64.encodeBytes(getBytesToSign());
            } else {
                return null;
            }
        }

        //CanSign
        public boolean isInSignatureMode() {
            boolean res = false;
            logger.debug("Is in signature mode?");
            if (hasReadDossier(dossierRef, AuthenticationUtil.getRunAsUser())
                    || !parapheurService.isReadingMandatory(dossierRef)) {
                logger.debug("Aspect lu OK");
                if ("parapheur_approve".equals(action)) {
                    logger.debug("Approve OK");
                    ArrayList<EtapeCircuit> circuit = (ArrayList<EtapeCircuit>) parapheurService.getCircuit(dossierRef);
                    if (!circuit.isEmpty()) {
                        if (!parapheurService.isTermine(dossierRef)) {
                            logger.debug("Non-termine OK");
                            if (circuit.get(0).isApproved()) {
                                if (circuit.get(0).getActionDemandee() == null) {   // compatibilité ascendante
                                    if (circuit.get(circuit.size() - 2).isApproved()) {
                                        logger.debug("Derniere etape OK");
                                        res = true;
                                    }
                                } else {
                                    if (parapheurService.getCurrentEtapeCircuit(dossierRef).getActionDemandee().trim()
                                            .equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {
                                        logger.debug("Etape de Signature OK");
                                        res = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return res;
        }
    }

    /**
     * Classe permettant le calcul de la position de la signature PAdES
     * Dans l'ordre de priorité :
     * #1 -> Placement par l'utilisateur via l'IHM
     * #2 -> Placement via le tag de signature (#signature#)
     * #3 -> Placement par défaut, récupération des propriétés du type !
     */
    private class PadesVisualPreparator {
        private Properties padesProperties;
        private NodeRef documentRef;
        private int x;
        private int y;
        private int width;
        private int height;
        private int fontSize;
        private int page;
        private int numSignature;
        private int numPages;

        public PadesVisualPreparator(Properties padesProperties, NodeRef documentRef, int numSignature) {
            this.padesProperties = padesProperties;
            this.documentRef = documentRef;
            this.numSignature = numSignature;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public int getFontSize() {
            return fontSize;
        }

        public int getPage() {
            return page;
        }

        public PadesVisualPreparator invoke(boolean cachet) throws IOException {

            // Construction du visuel...
            x = y = width = height = fontSize = page = 0;

            if (Boolean.parseBoolean(padesProperties.getProperty("showStamp"))) {
                fontSize = Integer.parseInt(padesProperties.getProperty("stampFontSize"));

                if (!retrieveCustomSignatureMetadata()) {
                    if (!retrieveSignatureTagFromMetadata()) {
                        if (!retrieveSignatureTagFromPdf(cachet)) {
                            x = Integer.parseInt(padesProperties.getProperty("stampCoordX"));
                            y = Integer.parseInt(padesProperties.getProperty("stampCoordY"));
                            page = Integer.parseInt(padesProperties.getProperty("stampPage"));
                            if(page < 1) page = numPages;
                        }
                    }
                }
            }

            return this;
        }

        /**
         * Inflates x, y, ... from document custom signature property
         *
         * @return true if values were found, false otherwise
         */
        private boolean retrieveCustomSignatureMetadata() {
            boolean retrieved = false;

            // Getting metadata

            java.awt.Rectangle customCoordinatesRectangle = null;
            Integer customPage = null;
            try {
                customCoordinatesRectangle = (java.awt.Rectangle) nodeService.getProperty(documentRef, ParapheurModel.PROP_CUSTOM_SIGNATURE_POSITION_RECTANGLE);
                customPage = (Integer) nodeService.getProperty(documentRef, ParapheurModel.PROP_CUSTOM_SIGNATURE_PAGE_NUMBER);
            } catch (ClassCastException ex) {
                logger.debug("Invalid PAdES custom signature location : ", ex);
            } catch (InvalidNodeRefException ex) {
                logger.debug("Invalid PAdES custom signature location : ", ex);
            }

            // Parsing

            if ((customCoordinatesRectangle != null) && (customPage != null)) {

                width = customCoordinatesRectangle.width;
                height = customCoordinatesRectangle.height;
                x = customCoordinatesRectangle.x;
                y = customCoordinatesRectangle.y;
                page = customPage;

                retrieved = true;
                logger.debug("PAdES custom signature location found : dossierRef=" + documentRef.getId() + " x=" + x + " y=" + y + " width=" + width + " height=" + height);
            } else {
                logger.debug("PAdES custom signature location not found");
            }

            return retrieved;
        }

        /**
         * Inflates x, y, ... from document tag metadata.
         * Useful to fetch only once the costful #signature# from PDF.
         *
         * @return true if values were found, false otherwise
         * @throws IOException
         */
        private boolean retrieveSignatureTagFromMetadata() throws IOException {
            boolean retrieved = false;

            // Getting metadata

            java.awt.Rectangle customCoordinatesRectangle = null;
            Integer customPage = null;
            try {
                customCoordinatesRectangle = (java.awt.Rectangle) nodeService.getProperty(documentRef, ParapheurModel.PROP_SIGNATURE_TAG_POSITION_RECTANGLE);
                customPage = (Integer) nodeService.getProperty(documentRef, ParapheurModel.PROP_SIGNATURE_TAG_PAGE_NUMBER);
            } catch (ClassCastException ex) {
                logger.debug("Invalid PAdES signature tag location : ", ex);
            } catch (InvalidNodeRefException ex) {
                logger.debug("Invalid PAdES signature tag location : ", ex);
            }

            // Parsing

            if ((customCoordinatesRectangle != null) && (customPage != null)) {

                width = customCoordinatesRectangle.width;
                height = customCoordinatesRectangle.height;
                x = customCoordinatesRectangle.x;
                y = customCoordinatesRectangle.y;
                page = customPage;

                retrieved = true;
                logger.debug("PAdES signature tag metadata location found : dossierRef=" + documentRef.getId() + " x=" + x + " y=" + y + " width=" + width + " height=" + height);
            } else {
                logger.debug("PAdES signature tag metadata signature location not found");
            }

            return retrieved;
        }

        /**
         * Inflates x, y, ... from document #signature# tag.
         *
         * @return true if values were found, false otherwise
         * @throws IOException
         */
        private boolean retrieveSignatureTagFromPdf(boolean cachet) throws IOException {
            boolean retrieved = false;

            width = Integer.parseInt(padesProperties.getProperty("stampWidth"));
            height = Integer.parseInt(padesProperties.getProperty("stampHeight"));

            FileContentReader tmpReader = (FileContentReader) contentService.getReader(documentRef, ContentModel.PROP_CONTENT);
            if (tmpReader == null) {
                return false;
            }

            String path = tmpReader.getFile().getAbsolutePath();

            PopplerUtils popplerUtils = new PopplerUtils();
            numPages = popplerUtils.getPageCount(path);

            // Position du tag de signature
            TagSearchResult position = impressionCalqueService.scanForTag(path, cachet, numSignature);

            if (position != null) {
                page = position.page;
                x = position.rectangle.x - width/2 + position.rectangle.width/2;
                y = position.rectangle.y - height/2 + position.rectangle.height/2;
                retrieved = true;

                logger.debug("PAdES signature tag location found : dossierRef=" + documentRef.getId() + " x=" + x + " y=" + y + " width=" + width + " height=" + height);
                savePdfSignatureTagToMetadata();
            } else {
                logger.debug("PAdES signature tag location not found");
            }

            return retrieved;
        }

        /**
         * Saves already fetched x, y, ... from document #signature# tag to metadata.
         */
        private void savePdfSignatureTagToMetadata() {
            java.awt.Rectangle rectangle = new Rectangle();
            rectangle.x = x;
            rectangle.y = y;
            rectangle.width = width;
            rectangle.height = height;

            nodeService.setProperty(documentRef, ParapheurModel.PROP_SIGNATURE_TAG_POSITION_RECTANGLE, rectangle);
            nodeService.setProperty(documentRef, ParapheurModel.PROP_SIGNATURE_TAG_PAGE_NUMBER, page);
        }

    }
}
