/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.RulesService;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import groovy.lang.MissingPropertyException;
import org.adullact.iparapheur.rules.RuleManager;
import org.adullact.iparapheur.rules.bean.CustomProperty;
import org.adullact.iparapheur.rules.workflow.SelectWorkflowRule;
import org.adullact.iparapheur.rules.workflow.IsBureauRule;
import org.apache.log4j.Logger;

/**
 * Classe permettant de faire executer un script contenant une règle de choix
 * de circuit.
 * Utilisé avec les metadonnees
 *
 * @author Vivien Barousse - ADULLACT Projet
 * @author Stephane Vast - ADULLACT Projet
 * @author Lukas Hameury - Libriciel - SCOP
 */
public class RulesServiceImpl implements RulesService {

    private ParapheurService parapheurService;

    private static final Logger logger = Logger.getLogger(RulesServiceImpl.class);

    public ParapheurService getParapheurService() {
        return parapheurService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    @Override
    public  Map<String, Object> selectWorkflowWithoutPropertiesError(String script, List<CustomProperty<? extends Object>> properties) {
        script = "def propertyMissing( name ) {\n" +
                "  null\n" +
                "}\n" + script;

        return selectWorkflow(script, properties);
    }

    @Override
    public  Map<String, Object> selectWorkflow(String script, List<CustomProperty<? extends Object>> properties) {
        Map<String, Object> results = executeScript(SelectWorkflowRule.class,
                script,
                properties);

        if (!results.containsKey("workflow")) {
            logger.error("No workflow returned by script.");
            throw new RuntimeException("No workflow returned by script.");
        }
        if (logger.isDebugEnabled()) {
            logger.debug("workflow = " + (String) results.get("workflow"));
        }

        return results;
    }

    public int findNextClosingParenthesis(String toTest, int openedParenthesis) {
        int closingParenthesis = openedParenthesis;
        int counter = 1;
        while(counter > 0 && closingParenthesis < toTest.length()) {
            char c = toTest.charAt(++closingParenthesis);
            if(c == '(') {
                counter++;
            } else if(c == ')') {
                counter--;
            }
        }
        return closingParenthesis;
    }

    // C'est un peu moche, mais ça permet d'éviter tout un tas de fichiers de configurations encore plus moche
    private String transformFunctionsIsBureau(String script, List<CustomProperty<? extends Object>> properties) {
        // Before doing anything... We try to find any occurence of "isBureau" function
        Pattern pattern = Pattern.compile("isBureau\\s*\\(\\s*");
        Matcher matcher = pattern.matcher(script);
        while (matcher.find()) {
            int beginParenthesis = matcher.end();
            int closedParenthesis = findNextClosingParenthesis(script, beginParenthesis);
            String code = script.substring(matcher.start(), closedParenthesis+1);
            String bureauToTest = script.substring(beginParenthesis, closedParenthesis);

            // Pour chaque 'isBureau' trouvé, on doit éxécuter un script dédié
            try {
                // Après transformation, éxécution
                IsBureauRule rule = IsBureauRule.class.newInstance();
                rule.setProperties(properties);
                rule.setBureauString(bureauToTest);
                String realBureauName = rule.run();

                String result = parapheurService.getParapheursFromName(realBureauName) == null || parapheurService.getParapheursFromName(realBureauName).isEmpty() ? "false": "true";

                script = script.replace(code, result);
                matcher = pattern.matcher(script);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException("Illegal class", ex);
            }
        }
        return script;
    }

    protected Map<String, Object> executeScript(Class<? extends RuleManager> ruleClass,
            String script,
            List<CustomProperty<? extends Object>> properties) {

        try {
            // Il faut transformer le script, car la récupération de bean dans le script groovy devient très lourd et compliqué !
            script = transformFunctionsIsBureau(script, properties);

            // Après transformation, éxécution
            RuleManager rule = ruleClass.newInstance();
            rule.setProperties(properties);
            rule.setScript(script);
            rule.run();
            return rule.getResults();
        } catch(MissingPropertyException e) {
            logger.error("Selection de circuit : métadonnée " + e.getProperty() + " manquante.");
            throw new RuntimeException("Illegal class", e);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw new RuntimeException("Illegal class", ex);
        }
        
    }

}
