/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo.job;

import com.atolcd.parapheur.repo.JobService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.LockSupport;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;

/**
 * @author Emmanuel Peralta - ADULLACT Projet
 */
public abstract class AbstractJob implements JobInterface, AuthenticationUtil.RunAsWork<Void> {
    protected Logger logger = Logger.getLogger(JobService.class);

    protected String username;

    protected List<NodeRef> lockedNodes;
    protected Map<NodeRef, String> dossierToName;
    protected Map<NodeRef, Exception> exceptionMap;

    protected transient JobService jobService;
    protected transient TransactionService transactionService;

    protected boolean running;
    protected boolean shallUnlock;

    /**
     * Constructor without parameters that initialize inner fields.
     * Except for services that are binded by JobService.
     *
     * @see JobService#postJob(JobInterface)
     */
    public AbstractJob() {
        this.username = AuthenticationUtil.getRunAsUser();
        exceptionMap = new HashMap<NodeRef, Exception>();
        dossierToName = new HashMap<NodeRef, String>();
        shallUnlock = true;
    }

    /**
     * Needs to be implemented by subclasses this is where the work is done.
     *
     * @return null
     */
    @Override
    public abstract Void doWork();


    /**
     * Gets the exception map built with putExceptionForDossier
     */
    @Override
    public Map<NodeRef, Exception> getExceptionsMap() {
        return exceptionMap;
    }

    /**
     * Gets the list of nodes locked by the job
     */
    @Override
    public List<NodeRef> getLockedNodeRefs() {
        return lockedNodes;
    }

    /**
     * Gets the username of the user that posted the job.
     */
    @Override
    public String getUsername() {
        return username;
    }

    /**
     * Tests if the job is currently running
     */
    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public void setRunning(boolean running) {
        this.running = running;
    }

    public boolean isShallUnlock() {
        return shallUnlock;
    }

    /**
     * Sets the job running state to the boolean passed as parameter.
     */
    public void setShallUnlock(boolean shallUnlock) {
        this.shallUnlock = shallUnlock;
    }

    /**
     * Saves the exception that occured for dossier
     *
     * @param e
     * @param dossier
     */
    @Override
    public void putExceptionForDossier(Exception e, NodeRef dossier) {
        logger.debug(String.format("Saving exception for Nodref %s", dossier));
        exceptionMap.put(dossier, e);
    }


    /**
     * Main method of the job, that is runned by the JobRunner
     */
    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        AuthenticationUtil.runAs(this, username);

        /*
        Park Thread only when backgroundWork is enabled.
         */

        if (jobService.isBackgroundWorkEnabled()) {
            long endTime = System.currentTimeMillis();
            long duration = endTime - startTime;

            // keep load down !
            LockSupport.parkNanos(Math.max(0, duration) * 1000000);
        }
    }

    /**
     * Sets the locked NodeRef list
     */
    @Override
    public void setLockedNodeRefs(List<NodeRef> nodeRefList) {
        this.lockedNodes = nodeRefList;

    }

    /**
     * Setter for JobService for Spring Use
     */
    @Override
    public void setJobService(JobService jobService) {
        this.jobService = jobService;
    }

    /**
     * Setter for TransactionService for Spring Use
     */
    @Override
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }


}
