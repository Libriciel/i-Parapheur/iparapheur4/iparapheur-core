package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;

public class ValidatorPatch  extends XPathBasedPatch {


    private ParapheurService parapheurService;

    @Override
    protected String getXPathQuery() {
        //return "//*[subtypeOf('ph:etape-circuit')]/*";
        return "//*[subtypeOf('ph:dossier')]";
    }

    @Override
    protected void patch(NodeRef nodeToPatch) throws Exception {

        // patch de l'étape courante en y sauvegardant l'acteur courant
        // (notion qui disparait avec le patch SharedParapheurPatch
        // on sait qu'a ce moment là l'acteur courant est forcément unique.

        // en réalité ce n'est utile que pour les dossiers envoyés à s2low qui ont été validés
        // mais en attente active d'un retour du serveur.

        // pour executer cette attente active il est necessaire d'avoir le login de l'utilisateur
        // qui a initié cet envoi afin d'effectuer un approve sous la bonne identité.

        // Idée aller chercher ça dans l'audit ! : mauvaise idée ...

        //String tenantDomain = tenantAdminService.getCurrentUserDomain();

        EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(nodeToPatch);

        String acteurCourant = parapheurService.getActeurCourant(nodeToPatch);

        if (parapheurService.getCurrentValidator(nodeToPatch) == null && acteurCourant != null && acteurCourant.length() != 0) {
            parapheurService.setCurrentValidator(nodeToPatch, acteurCourant);
        }
        else {
            if (EtapeCircuit.ETAPE_MAILSEC.equals(etapeCourante.getActionDemandee())) {
                // issue a warning not
                // not possible
            }
            else if (EtapeCircuit.ETAPE_TDT.equals(etapeCourante.getActionDemandee())) {
                // issue another warning
                System.out.println(String.format("TDT sans validator : %s", nodeToPatch));
            }
        }
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }
}
