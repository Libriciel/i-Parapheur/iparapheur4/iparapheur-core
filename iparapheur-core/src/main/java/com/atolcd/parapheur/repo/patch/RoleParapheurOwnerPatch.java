/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.repo.security.authority.UnknownAuthorityException;

import java.util.List;

/**
 * Creates a role for each parapheur found in the parapheurs space.
 *
 * These roles are used for permissions.
 *
 * @author Vivien Barousse
 */
public class RoleParapheurOwnerPatch extends XPathBasedPatch {

    public static final String ALL_PARAPHEURS_XPATH = "/app:company_home" +
            "/ph:parapheurs" +
            "/*";
    
    private AuthorityService authorityService;

    @Override
    protected String getXPathQuery() {
        return ALL_PARAPHEURS_XPATH;
    }

    protected void patch(NodeRef parapheur) {
        NodeService nodeService = getNodeService();

        String parapheurName = (String) nodeService.getProperty(parapheur, ContentModel.PROP_NAME);
        String roleName = "PHOWNER_" + parapheur.getId();
        String authorityName = authorityService.getName(AuthorityType.ROLE, roleName);

        String oldAuthorityName = authorityService.getName(AuthorityType.ROLE, "PHOWNER_" + parapheurName);
        String delegateAuthorityName = authorityService.getName(AuthorityType.ROLE, "PHDELEGATES_" + parapheur.getId());
        String oldDelegateAuthorityName = authorityService.getName(AuthorityType.ROLE, "PHDELEGATES_" + parapheurName);

        if (authorityService.authorityExists(oldDelegateAuthorityName)) {
            authorityService.deleteAuthority(oldDelegateAuthorityName, false);
        }
        if (authorityService.authorityExists(delegateAuthorityName)) {
            authorityService.deleteAuthority(delegateAuthorityName, false);
        }
        if (authorityService.authorityExists(oldAuthorityName)) {
            authorityService.deleteAuthority(oldAuthorityName, false);
        }
        if (authorityService.authorityExists(authorityName)) {
            authorityService.deleteAuthority(authorityName, false);
        }

        authorityService.createAuthority(AuthorityType.ROLE, roleName);

        List<String> phOwners = (List<String>) nodeService.getProperty(parapheur, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);

        if (phOwners != null) {
            for (String phOwner : phOwners) {
                if ((phOwner.trim().length() > 0) && (authorityService.authorityExists(phOwner))) {
                    authorityService.addAuthority(authorityName, authorityService.getName(AuthorityType.USER, phOwner));
                }
            }
        }

    }

    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

}
