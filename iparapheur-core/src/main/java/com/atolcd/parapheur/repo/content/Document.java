package com.atolcd.parapheur.repo.content;

import net.iharder.Base64;
import java.io.IOException;

public class Document {
    private byte[] content;
    private String fileName;

    public Document() {

    }

    public Document(byte[] _content, String _fileName) {
        content = _content;
        fileName = _fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getContent() {
        return content;
    }

    public void setBase64Content(String contentString) {
        try {
            content = Base64.decode(contentString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}