/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.repo.content.cleanup;

import java.util.List;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class ParapheurCleanerJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap jobData = context.getJobDetail().getJobDataMap();
        // extract the content cleaner to use
        Object parapheurCleanerObj = jobData.get("parapheurCleaner");
        if (parapheurCleanerObj == null || !(parapheurCleanerObj instanceof ParapheurCleaner)) {
            throw new AlfrescoRuntimeException(
                    "ParapheurCleanerJob data must contain valid 'parapheurCleaner' reference");
        }

        final ParapheurCleaner parapheurCleaner = (ParapheurCleaner) parapheurCleanerObj;
        final String storeRef = jobData.getString("spaces.store");

        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {

            @Override
                public Object doWork() throws Exception {
                    parapheurCleaner.setArchiveStoreRef(StoreRef.STORE_REF_ARCHIVE_SPACESSTORE);
                    parapheurCleaner.execute();
                    return null;
                }
            }, "admin");

        final TenantService tenantService = (TenantService) jobData.get("tenantService");
        final TenantAdminService tenantAdminService = (TenantAdminService) jobData.get("tenantAdminService");

        List<Tenant> tenants = tenantAdminService.getAllTenants();

        for (final Tenant tenant : tenants) {
            if (tenant.isEnabled()) {
                final StoreRef tenantStoreRef = tenantService.getName(tenant.getTenantDomain(), StoreRef.STORE_REF_ARCHIVE_SPACESSTORE);
                AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {

                    @Override
                    public Object doWork() throws Exception {
                        parapheurCleaner.setArchiveStoreRef(tenantStoreRef);
                        parapheurCleaner.execute();
                        return null;
                    }
                }, tenantService.getDomainUser("admin", tenant.getTenantDomain()));
            }
        }
    }
}
