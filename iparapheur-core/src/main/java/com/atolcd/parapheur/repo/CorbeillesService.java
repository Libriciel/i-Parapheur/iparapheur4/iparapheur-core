/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import java.util.List;

/**
 *
 * @author Vivien Barousse
 */
public interface CorbeillesService {

    static final String EN_PREPARATION = "en-preparation";
    static final String A_TRAITER = "a-traiter";
    static final String A_ARCHIVER = "a-archiver";
    static final String RETOURNES = "retournes";
    static final String EN_COURS = "en-cours";
    static final String TRAITES = "traites";
    static final String A_VENIR = "a-venir";
    static final String DELEGUES = "dossiers-delegues";
    static final String RECUPERABLES = "recuperables";
    static final String SECRETARIAT = "secretariat";
    static final String EN_RETARD = "en-retard";
    static final String A_IMPRIMER = "a-imprimer";

    static final String AUCUNE = "aucune";

    public List<NodeRef> getDossiersFromCorbeilleVirtuelle(NodeRef parapheur, QName name);

    public List<NodeRef> getDossiersAImprimer(NodeRef parapheur);

    public List<NodeRef> getDossiersEnRetard(NodeRef parapheur);

    public List<NodeRef> getDossiersAVenir(NodeRef parapheur);

    public List<NodeRef> getDossiersEnCours(NodeRef parapheur);

    public List<NodeRef> getDossiersTraites(NodeRef parapheur);
    
    public List<NodeRef> getDossiersDelegues(NodeRef parapheur);

    public void insertIntoCorbeilleAImprimer(NodeRef dossier);

    /**
     * Regénère les relations entre un dossier donné et les corbeilles
     * virtuelles auxquelles il appartient.
     *
     * Cette méthode est principalement utilisée dans le cas d'un patch, afin
     * de générer les relations lors d'une migration.
     * 
     * @param dossier le dossier pour lequel les associations sont à recalculer
     */
    public void regenerateDossierRelationships(NodeRef dossier);

    public void reduceCorbeilleChildCount(NodeRef corbeille);

    public void raiseCorbeilleChildCount(NodeRef corbeille);

    void regenerateAllCorbeilleCount();

    public void updateCorbeilleChildCount(NodeRef corbeille);
    
    /**
     * Renvoie la liste des dossiers contenus dans une corbeille virtuelle.
     * <p/>
     * Les dossiers contenus dans une corbeille virtuelle sont identifiées par
     * une relation de type ph:virtuallyContains. Si une référence à une autre
     * corbeille existe (relation ph:VirtuallyRefers), les dossiers de la
     * corbeille référée sont aussi renvoyés.
     *
     * @param corbeille La corbeille a requeter
     * @return La liste des dossiers contenus dans la corbeille.
     */
    public List<NodeRef> getDossiersFromCorbeilleVirtuelle(NodeRef corbeille);

    List<NodeRef> findLate();

    /**
     * Insert le dossier dans la bannette "en-retard" si le dossier est en retard et absent de cette bannette.
     * @param dossier
     */
    public void updateLate(NodeRef dossier);

    void moveDossier(NodeRef node, NodeRef oldParent, NodeRef newParent);

    public void insertIntoCorbeillesVirtuelles(NodeRef dossier);

    public void deleteVirtuallyContainsAssociations(NodeRef dossier);

    public void removeDossierFromRecuperables(NodeRef dossier);
}
