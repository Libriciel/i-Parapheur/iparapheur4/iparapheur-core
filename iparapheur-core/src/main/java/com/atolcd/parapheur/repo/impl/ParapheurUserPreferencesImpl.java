package com.atolcd.parapheur.repo.impl;
/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * contact@atolcd.com
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

import com.atolcd.parapheur.repo.ParapheurUserPreferences;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.service.cmr.preference.PreferenceService;
import org.alfresco.service.cmr.security.PersonService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.Serializable;
import java.util.Map;

/**
 * ParapheurUserPreferencesImpl
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 13/04/12
 *         Time: 10:11
 */
public class ParapheurUserPreferencesImpl implements ParapheurUserPreferences {

    private final Logger logger = Logger.getLogger(ParapheurUserPreferences.class);

    @Qualifier("PreferenceService")
    @Autowired
    private PreferenceService preferenceService;

    @Autowired
    private PersonService personService;

    private void setPreference(String username, String key, Serializable value) {
        if(personService.personExists(username)) {
            Map<String, Serializable> preferenceMap = preferenceService.getPreferences(username);
            preferenceMap.put(key, value);
            preferenceService.setPreferences(username, preferenceMap);
        } else {
            logger.error(String.format("Impossible de définir les préférences de l'utilisateur %s : l'utilisateur n'existe pas", username));
        }
    }

    private Serializable getPreference(String username, String key, Serializable defaultValue) {
        Serializable value = null;
        try {
            if(personService.personExists(username)) {
                Map<String, Serializable> preferenceMap = preferenceService.getPreferences(username);
                value = preferenceMap.get(key);
            } else {
                logger.warn(String.format("Récupération de préférences d'un utilisateur non-existant : %s", username));
            }
        } catch (AlfrescoRuntimeException e) {
            logger.error(String.format("Erreur à la récupération des préférences utilisateur de %s", username), e);
        }
        return (value == null)? defaultValue : value;
    }

    @Override
    public void enableNotificationsForUsername(String username) {
        setPreference(username, NOTIFICATION_ENABLED_PREFERENCE, Boolean.TRUE);
    }

    @Override
    public void disableNotificationsForUsername(String username) {
        setPreference(username, NOTIFICATION_ENABLED_PREFERENCE, Boolean.FALSE);
    }

    @Override
    public Boolean isNotificationsEnabledForUsername(String username) {
        return (Boolean)getPreference(username, NOTIFICATION_ENABLED_PREFERENCE, Boolean.FALSE);
    }

    @Override
    public void enableMobileNotificationsForUsername(String username) {
        setPreference(username, MOBILE_NOTIFICATIONS_ENABLED_PREFERENCE, Boolean.TRUE);
    }

    @Override
    public void disableMobileNotificationsForUsername(String username) {
        setPreference(username, MOBILE_NOTIFICATIONS_ENABLED_PREFERENCE, Boolean.FALSE);
    }

    @Override
    public Boolean isMobileNotificationsEnabledForUsername(String username) {
        return (Boolean)getPreference(username, MOBILE_NOTIFICATIONS_ENABLED_PREFERENCE, Boolean.FALSE);
    }

    @Override
    public void enableDailyDigestForUsername(String username) {
        setPreference(username, DAILY_DIGEST_ENABLED_PREFERENCE, Boolean.TRUE);
    }

    @Override
    public void disableDailyDigestForUsername(String username) {
        setPreference(username, DAILY_DIGEST_ENABLED_PREFERENCE, Boolean.FALSE);
    }

    @Override
    public Boolean isDailyDigestEnabledForUsername(String username) {
        return (Boolean)getPreference(username, DAILY_DIGEST_ENABLED_PREFERENCE, Boolean.FALSE);
    }

    @Override
    public void setMobileUUIDDeviceForUsername(String username, String mobileUUID) {
        setPreference(username, MOBILE_DEVICE_TOKEN_PREFERENCE, mobileUUID);
    }

    @Override
    public String getMobileUUIDDeviceForUsername(String username) {
        return (String)getPreference(username, MOBILE_DEVICE_TOKEN_PREFERENCE, null);
    }

    @Override
    public String getDigestCronForUsername(String username) {
        return (String)getPreference(username, DIGEST_CRON_PREFERENCE, null);
    }

    @Override
    public void setDigestCronForUsername(String username, String cronexp) {
        setPreference(username, DIGEST_CRON_PREFERENCE, cronexp);
    }

    @Override
    public String[] getNotificationMailForUsername(String username) {
        String preference =  (String) getPreference(username, NOTIFICATION_MAIL_PREFERENCE, "");

        return preference.split(";");
    }
    
    @Override
    public void setNotificationMailForUsername(String username, String mail) {
        if(personService.personExists(username)) {
            if ((mail == null) || mail.trim().isEmpty()) {
                preferenceService.clearPreferences(username, NOTIFICATION_MAIL_PREFERENCE);
            }
            else {
                setPreference(username, NOTIFICATION_MAIL_PREFERENCE, mail);
            }
        } else {
            logger.error(String.format("Impossible de définir les préférences de l'utilisateur %s : l'utilisateur n'existe pas", username));
        }
    }
}
