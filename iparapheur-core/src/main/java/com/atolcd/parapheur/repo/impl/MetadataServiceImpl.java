/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2013, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CustomMetadataDef;
import com.atolcd.parapheur.repo.CustomMetadataType;
import com.atolcd.parapheur.repo.MetadataService;
import com.atolcd.parapheur.repo.TypesService;
import com.atolcd.parapheur.web.bean.wizard.batch.DossierFilter;
import org.adullact.iparapheur.util.CollectionsUtils;
import org.adullact.utils.StringUtils;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.service.cmr.admin.RepoAdminService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.dom4j.tree.DefaultDocument;
import org.dom4j.tree.DefaultElement;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.UserTransaction;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Vivien Barousse - ADULLACT Projet
 */
public class MetadataServiceImpl implements MetadataService {

    private static final Logger logger = Logger.getLogger(MetadataServiceImpl.class);

    public static final String MODELS_DEF_NODE_REF_XPATH =
            "/app:company_home/app:dictionary/app:models";

    public static final String METADATA_DEF_NODE_REF_XPATH =
            MODELS_DEF_NODE_REF_XPATH
            + "/cm:customMetadata";

    public static final String SUBTYPE_PATH = "/app:company_home/app:dictionary/cm:metiersoustype";

    public static final String METADATA_DEF_CUSTOM_URI = "http://www.adullact.org/parapheur/metadata/1.0";
    public static final String METADATA_DEF_CUSTOM_PREFIX = "cu";
    private static final String DEFAULT_DEFS_FILENAME = "metadataDefs.xml";

    private NodeService nodeService;

    private ContentService contentService;

    private SearchService searchService;

    private NamespaceService namespaceService;

    private RepoAdminService repoAdminService;

    private TransactionService transactionService;

    @Autowired
    private TenantAdminService tenantAdminService;
    
    private TypesService typesService;

    private String storeRef;
                       /*
    private DictionaryDAO dictionaryDAO;
    private TenantService tenantService;
    */
    private List<String> storeUrls; // used for

    private final ConcurrentHashMap<String, List<CustomMetadataDef>> customMetadataDefsCache = new ConcurrentHashMap<>();

    private boolean validateIndexedProperty(QName propName) {
        boolean retVal = true;
        /*
        if (!propDef.isIndexed()) {
            // TODO ... implement DB-level referential integrity
            throw new AlfrescoRuntimeException("Failed to validate property delete" + tenantDomain + " - cannot delete unindexed property definition '" + propName);
        }
        */

        for (String storeUrl : this.storeUrls) {
            StoreRef store = new StoreRef(storeUrl);

            // search for indexed PROPERTY
            String escapePropName = propName.toPrefixString().replace(":", "\\:");
            ResultSet rs = searchService.query(store, SearchService.LANGUAGE_LUCENE, "@" + escapePropName + ":*");
            try {
                if (rs.length() > 0) {
                    retVal = false;
                    //throw new AlfrescoRuntimeException("Failed to validate property delete" + tenantDomain + " - found " + rs.length() + " nodes in store " + store + " with PROPERTY '" + propName + "'");
                }
            } finally {
                rs.close();
            }
        }

        return retVal;
    }

    /**
     * Checks the usage of metadata in subtypes.
     * @param propName
     * @return the list of referencing subtypes.
     */
    @Override
    public List<String> getSubtypesReferencingMetadata(QName propName) {
        List<String> retVal = new ArrayList<String>();
// Remplacement de "StoreRef.STORE_REF_WORKSPACE_SPACESSTORE" par "new StoreRef(storeRef)"
        ResultSet rs = searchService.query(new StoreRef(storeRef) ,
        SearchService.LANGUAGE_LUCENE, String.format("PATH:\"%s\" TEXT:\"{%s}%s\"", SUBTYPE_PATH, propName.getNamespaceURI(), propName.getLocalName()));

        try {
            List<NodeRef> results = rs.getNodeRefs();
            for (NodeRef result : results) {
                String name = (String)nodeService.getProperty(result, ContentModel.PROP_NAME);
                if (name.endsWith(".xml")) {
                    name = name.substring(0, name.length() - 4);
                    retVal.add(name);
                }
            }
        }
        finally {
            rs.close();
        }

        return retVal;
    }

    @Override
    public Map<QName, Map<String, Serializable>> getMetaDonneesDossierWithTypo(NodeRef dossier, String type, String sstype) {
        //Liste des définitions de metadonnées
        List<CustomMetadataDef> list = getMetadataDefs();
        Map<String, Map<String, String>> mds = null;
        Map<QName, Serializable> properties = nodeService.getProperties(dossier);

        Map<QName, Map<String, Serializable>> toReturn = new HashMap<QName, Map<String, Serializable>>();
        Map<String, Serializable> child;

        String realName = null;
        String keyName = null;
        String keyValue = null;

        if (type != null && sstype != null && !type.isEmpty() && !sstype.isEmpty()) {
            try {
                //Récupération des metadonnées pour les type/sous-type donnés
                mds = typesService.getMetadatasMap(type, sstype);
            } catch(Exception e) {
                logger.error(e.getLocalizedMessage());
            }
            if (mds != null) {
                for (CustomMetadataDef customMetadataDef : list) {
                    //Récupération de la valeur de la clé complète
                    keyValue = customMetadataDef.getName().toString();
                    keyName = customMetadataDef.getName().toPrefixString(namespaceService);
                    if(keyValue != null && mds.containsKey(keyValue)) {
                        child = new HashMap<String, Serializable>();
                        if(customMetadataDef.getType() == CustomMetadataType.DATE) {
                            if (mds.get(keyValue).containsKey("default") && !mds.get(keyValue).get("default").isEmpty()) {
                                try {
                                    mds.get(keyValue).put("default", new SimpleDateFormat("yyyy-MM-dd").parse(mds.get(keyValue).get("default")).toString());
                                } catch(Exception e) {
                                    logger.error(e.getLocalizedMessage());
                                }
                            }
                        }
                        child.putAll(mds.get(keyValue));
                        child.put("realName", customMetadataDef.getTitle());
                        child.put("type", customMetadataDef.getType().toString());
                        if(properties.containsKey(customMetadataDef.getName())) {
                            child.put("value", properties.get(customMetadataDef.getName()));
                        }
                        else
                            child.put("value", "");
                        if(customMetadataDef.getEnumValues() != null) {
                            child.put("values", (Serializable) customMetadataDef.getEnumValues());
                        }
                        toReturn.put(customMetadataDef.getName(), child);
                    }
                }
            }
        }

        return toReturn;
    }

    @Override
    public Map<QName, Map<String, Serializable>> getMetaDonneesDossier(NodeRef dossier) {

        String type = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER);
        String sstype = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER);

        return getMetaDonneesDossierWithTypo(dossier, type, sstype);
    }

    @Override
    public List<CustomMetadataDef> getMetadataDefs() {
        String tenantName = tenantAdminService.getCurrentUserDomain();
        if(!customMetadataDefsCache.containsKey(tenantName)) {
            logger.info("Building Cache for metadatas");
            NodeRef metadataNode = getMetadataDefNodeRef(false);

            if (metadataNode == null) {
                logger.warn("Metadata node not found.");
                return Collections.emptyList();
            }

            ContentReader contentReader = contentService.getReader(metadataNode,
                    ContentModel.PROP_CONTENT);
            customMetadataDefsCache.put(tenantName, getMetadataDefsFromStream(contentReader.getContentInputStream()));
        } else {
            logger.info("Getting metadatas from Cache");
        }
        return customMetadataDefsCache.getOrDefault(tenantName, new ArrayList<>());
    }

    private List<CustomMetadataDef> getMetadataDefsFromStream(InputStream in) {
        List<CustomMetadataDef> defs = new ArrayList<CustomMetadataDef>();
        try {
            SAXReader reader = new SAXReader();
            Document doc = reader.read(in);
            Element aspects = doc.getRootElement().element("aspects");

            if (aspects.elements("aspect").size() != 1) {
                throw new IllegalStateException("No aspect (or more than one) found in metadata def");
            }

            Element aspect = aspects.element("aspect");
            Element properties = aspect.element("properties");
            List<Element> propElements = properties.elements("property");

            for (Element propertyElem : propElements) {

                QName propName = QName.createQName(propertyElem.attributeValue("name"), namespaceService);
                String title = propertyElem.elementText("title");
                CustomMetadataType type = CustomMetadataType.fromPropertyXml(propertyElem.attributeValue("customMetadataType"), propertyElem.elementText("type"));
                boolean isDeletable = validateIndexedProperty(propName);
                List<String> referencedBy = getSubtypesReferencingMetadata(propName);

                if (!referencedBy.isEmpty()) {
                    isDeletable = false;
                }

                CustomMetadataDef newDef;

                Element constraints = propertyElem.element("constraints");
                if (constraints != null) {
                    Element list = constraints.element("constraint").element("parameter").element("list");

                    List <Element> listelt = list.elements("value");
                    ArrayList<String> enumvalues = new ArrayList<String>();

                    for (Element elt : listelt) {
                        enumvalues.add(elt.getText());
                    }

                    boolean isAlphaOrdered = true;
                    String alphaOrdered = propertyElem.attributeValue("alphaOrdered");
                    if(alphaOrdered != null && !Boolean.valueOf(alphaOrdered)) {
                        isAlphaOrdered = false;
                    }

                    newDef = new CustomMetadataDef(propName, title, enumvalues, type, isDeletable, isAlphaOrdered);

                } else {
                    newDef = new CustomMetadataDef(propName, title, type, isDeletable);
                }

                newDef.setReferencedBy(referencedBy);
                defs.add(newDef);
            }

            return Collections.unmodifiableList(defs);
        } catch (DocumentException ex) {
            throw new RuntimeException("Document exception during XML parsing", ex);
        }
    }

    @Override
    public void deleteMetadataDef(QName metadata) {
        List<CustomMetadataDef> defs = new ArrayList<CustomMetadataDef>(getMetadataDefs());

        CustomMetadataDef toRemove = null;

        for (CustomMetadataDef d : defs) {
            if (d.getName().isMatch(metadata)) {
                toRemove = d;
            }
        }

        if (toRemove != null) {
            defs.remove(toRemove);
        }

        saveMetadataDefs(defs);
    }

    @Override
    public void addMetadataDef(CustomMetadataDef def) {
        List<CustomMetadataDef> defs = new ArrayList<CustomMetadataDef>(getMetadataDefs());

        Iterator<CustomMetadataDef> it = defs.iterator();
        CustomMetadataDef toRemove = null;

        while (it.hasNext()) {
            CustomMetadataDef  d = it.next();
            if (d.getName().isMatch(def.getName())) {
                toRemove = d;
            }
        }

        if (toRemove != null) {
            defs.remove(toRemove);
        }
        defs.add(def);
        saveMetadataDefs(defs);
    }
    
    @Override
    public void addMetadatasDefs(List<CustomMetadataDef> defsToAdd) {
        List<CustomMetadataDef> defs = new ArrayList<CustomMetadataDef>(getMetadataDefs());

        Iterator<CustomMetadataDef> it = defs.iterator();
        List<CustomMetadataDef> toRemove = new ArrayList<CustomMetadataDef>();

        while (it.hasNext()) {
            CustomMetadataDef  d = it.next();
            for (CustomMetadataDef def : defsToAdd) {
                if (d.getName().isMatch(def.getName())) {
                    toRemove.add(d);
                }
            }
        }

        defs.removeAll(toRemove);
        defs.addAll(defsToAdd);
        saveMetadataDefs(defs);
    }

    @Override
    public void saveMetadataDefs(List<CustomMetadataDef> defs) {
        Document doc = new DefaultDocument();

        Element root = new DefaultElement("model");
        root.addAttribute("name", "cu:customMetadataModel");
        root.addNamespace("", "http://www.alfresco.org/model/dictionary/1.0");

        root.addElement("description", "http://www.alfresco.org/model/dictionary/1.0").setText("Custom metadata");
        root.addElement("author", "http://www.alfresco.org/model/dictionary/1.0").setText("iParapheur");
        root.addElement("version", "http://www.alfresco.org/model/dictionary/1.0").setText("1.0");

        Element imports = root.addElement("imports", "http://www.alfresco.org/model/dictionary/1.0");
        imports.addElement("import", "http://www.alfresco.org/model/dictionary/1.0").addAttribute("uri", "http://www.alfresco.org/model/dictionary/1.0").addAttribute("prefix", "d");
        imports.addElement("import", "http://www.alfresco.org/model/dictionary/1.0").addAttribute("uri", "http://www.alfresco.org/model/content/1.0").addAttribute("prefix", "cm");
        imports.addElement("import", "http://www.alfresco.org/model/dictionary/1.0").addAttribute("uri", "http://www.alfresco.org/model/system/1.0").addAttribute("prefix", "sys");

        Element namespaces = root.addElement("namespaces", "http://www.alfresco.org/model/dictionary/1.0");
        namespaces.addElement("namespace", "http://www.alfresco.org/model/dictionary/1.0").addAttribute("uri", METADATA_DEF_CUSTOM_URI).addAttribute("prefix", METADATA_DEF_CUSTOM_PREFIX);

        Element aspects = root.addElement("aspects", "http://www.alfresco.org/model/dictionary/1.0");
        Element aspect = aspects.addElement("aspect", "http://www.alfresco.org/model/dictionary/1.0").addAttribute("name", METADATA_DEF_CUSTOM_PREFIX + ":customMetadata");
        Element properties = aspect.addElement("properties", "http://www.alfresco.org/model/dictionary/1.0");

        for (CustomMetadataDef def : defs) {
            Element property = properties.addElement("property", "http://www.alfresco.org/model/dictionary/1.0")
                    .addAttribute("name", METADATA_DEF_CUSTOM_PREFIX + ":" + def.getName().getLocalName())
                    .addAttribute("customMetadataType", def.getType().toString())
                    .addAttribute("alphaOrdered", Boolean.toString(def.isAlphaOrdered()));
            property.addElement("title", "http://www.alfresco.org/model/dictionary/1.0").setText(def.getTitle());
            property.addElement("type", "http://www.alfresco.org/model/dictionary/1.0").setText(def.getType().getAlfrescoType());

            Element index = property.addElement("index", "http://www.alfresco.org/model/dictionary/1.0")
                    .addAttribute("enabled", "true");
            index.addElement("atomic", "http://www.alfresco.org/model/dictionary/1.0").setText("true");
            index.addElement("stored", "http://www.alfresco.org/model/dictionary/1.0").setText("false");
            index.addElement("tokenised", "http://www.alfresco.org/model/dictionary/1.0").setText("false");

            List<String> enumValues = def.getEnumValues();
            if (enumValues != null) {
                Element constraints = property.addElement("constraints", "http://www.alfresco.org/model/dictionary/1.0");
                Element constraint = constraints.addElement("constraint", "http://www.alfresco.org/model/dictionary/1.0");
                constraint.addAttribute("type","LIST");
                
                Element parameter = constraint.addElement("parameter"); // <parameter name="allowedValues">
                parameter.addAttribute("name", "allowedValues");
                Element list = parameter.addElement("list") ;//<list>

                for (String value : enumValues) {
                    list.addElement("value").setText(value);
                }
            }
        }

        doc.setRootElement(root);

        UserTransaction tx;

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            XMLWriter writer = new XMLWriter(out);

            writer.write(doc);
            writer.flush();

            byte[] file = out.toByteArray();
            InputStream in = new ByteArrayInputStream(file);
            /*
            tx = transactionService.getNonPropagatingUserTransaction();
            try {
                tx.begin();
                repoAdminService.deactivateModel("customMetadata");
                tx.commit();
            } catch (Exception e) {
                try {
                    tx.rollback();
                } catch (Throwable t) {
                    // do nothing but cry
                }
            }  */
            try {
                repoAdminService.deactivateModel("customMetadata");
            }
            catch (AlfrescoRuntimeException e) {
                // tampis
            }
            finally {
                //  activateCustomMetadata();
                try {

                    repoAdminService.deployModel(in, "customMetadata");
                    repoAdminService.activateModel("customMetadata");
                } catch (AlfrescoRuntimeException e) {
                    // okay
                    // e.printStackTrace();
                    logger.warn("Exception lors de l'activation du modèle customMetadata", e);
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("I/O while writing XML document", ex);
        }

        // invalidate cache
        customMetadataDefsCache.remove(tenantAdminService.getCurrentUserDomain());
    }
       /*
    public void activateCustomMetadata() {
        try {
            repoAdminService.activateModel("customMetadata");
        } catch (AlfrescoRuntimeException e) {
            // okay
        }
    }
        */
    @Override
    public Object getValueFromString(CustomMetadataType type, String value) {
        Object val = null;
        try {
            switch (type) {
                case STRING:
                case URL:
                    val = value;
                    break;
                case INTEGER:
                    val = Integer.valueOf(value);
                    break;
                case DOUBLE:
                    val = Double.valueOf(value);
                    break;
                case DATE:
                    Date d = null;
                    try {
                        d = new SimpleDateFormat("yyyy-MM-dd").parse(value);
                    } catch (ParseException ex) {
                        try {
                            // exemple : Sun Sep 28 00:00:00 CEST 2014
                            d = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy", Locale.ENGLISH).parse(value);
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    }
                    val = d;
                    break;
                case BOOLEAN:
                    /**
                     * si null ou vide, renvoie False.
                     */
                    val = Boolean.valueOf(value);
                    break;
                default:
                    throw new RuntimeException("Unknown type: " + type);
            }

        } catch (Exception e) {
            //Do nothing... return simply null
        }
        return val;
    }

    @Override
    public void validateValue(CustomMetadataType type, String value) {
        try {
            getValueFromString(type, value);
        } catch (Exception ex) {
            throw new RuntimeException("Value not valid: " + value, ex);
        }
    }

    @Override
    public List<String> getUsedValuesForDef(CustomMetadataDef def) {
        List<String> values = new ArrayList<String>();

        DossierFilter filter = new DossierFilter();

        filter.setSearchPath(null);
        filter.addObjectType("ph:archive");
        filter.addObjectType("ph:parapheur");
        filter.addFilterElement(def.getName(), "*");

        SearchParameters searchParameters = new SearchParameters();

        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.setQuery(filter.buildFilterQuery());
        searchParameters.addStore(new StoreRef(storeRef));

        ResultSet resultSet = searchService.query(searchParameters);

        List<NodeRef> dossiers = resultSet.getNodeRefs();

        resultSet.close();
        Set<String> valuesSet = new HashSet<String>();

        for (NodeRef dossier : dossiers) {
            if(nodeService.exists(dossier)) {
                Serializable val = nodeService.getProperty(dossier, def.getName());
                if(val != null) valuesSet.add(val.toString());
            }
        }

        values.addAll(valuesSet);
        Collections.sort(values);
        return values;
    }



    private NodeRef getMetadataDefNodeRef(boolean create) {
        List<NodeRef> candidates = searchService.selectNodes(
                nodeService.getRootNode(new StoreRef(storeRef)),
                METADATA_DEF_NODE_REF_XPATH,
                null,
                namespaceService,
                false);

        if (candidates.size() >= 2) {
            throw new IllegalStateException("Multiple candidates found for custom metadata model node");
        }

        if (candidates.size() == 1) {
            return candidates.get(0);
        }

        if (!create) {
            return null;
        }

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "customMetadata.xml");
        properties.put(ContentModel.PROP_TITLE, "customMetadata.xml");

        // If the node ref doesn't exists, we create it
        NodeRef parent = getModelsDefNodeRef();
        NodeRef child = nodeService.createNode(parent,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:customMetadata.xml", namespaceService),
                ContentModel.TYPE_CONTENT,
                properties).getChildRef();

        return child;
    }
    
    @Override
    public List<CustomMetadataDef> getMetadatas(String type, String sstype) {

        // filter out the Mds not defined in the subtype
        List<CustomMetadataDef> defs = getMetadataDefs();
        List<CustomMetadataDef>  metadatas  = new ArrayList<CustomMetadataDef>();

        Map<QName, Map<String, String>> maMap = getMetadatasMap(type, sstype);
        for (CustomMetadataDef def : defs) {
            if (maMap.get(def.getName()) != null) {
                metadatas.add(def);
            }
        }

        return metadatas;
    }
    
    @Override
    public Map<QName, Map<String, String>> getMetadatasMap(String type, String sstype) {
        Map<QName, Map<String, String>> retVal = new HashMap<QName, Map<String, String>>();

        if (type != null && sstype != null && !type.isEmpty() && !sstype.isEmpty()) {
            Map<String, Map<String, String>> mds = typesService.getMetadatasMap(type, sstype);
            if (mds != null) {
                for (String key : mds.keySet()) {
                    retVal.put(QName.createQName(key), mds.get(key));
                }
            }
        }
        return retVal;
    }
    
    @Override
    public Map<String, Serializable> getMetadataValues(Map<QName, Map<String, Serializable>> map) {

        Map<String, Serializable> metadataValues = new HashMap<String, Serializable>();

        for (QName qName : map.keySet()) {
            metadataValues.put(qName.getLocalName(), map.get(qName).get("value"));
        }

        return metadataValues;
    }

    private NodeRef getModelsDefNodeRef() {
        List<NodeRef> candidates = searchService.selectNodes(
                nodeService.getRootNode(new StoreRef(storeRef)),
                MODELS_DEF_NODE_REF_XPATH,
                null,
                namespaceService,
                false);

        if (candidates.size() >= 2) {
            throw new IllegalStateException("Multiple candidates found for custom models node");
        }

        if (candidates.isEmpty()) {
            throw new IllegalStateException("No candidates found for custom models node");
        }

        return candidates.get(0);
    }

    @Override
    public void reloadMetadataDefs() {
        addMetadatasDefs(getDefaultMetadataDefs());
    }

    @Override
    public boolean areMetadataValid(@Nullable Map<QName, Serializable> metadataValues) throws IllegalArgumentException {

        // Default cases

        if ((metadataValues == null) || (metadataValues.isEmpty())) {
            return true;
        }

        // Filtering known metadata keys from entries

        List<CustomMetadataDef> knownMetadataList = getMetadataDefs();
        HashMap<QName, CustomMetadataDef> knownMetadataMap = CollectionsUtils.metadataMapFromList(knownMetadataList);
        Set<QName> newMetadataKeySet = new HashSet<QName>(knownMetadataMap.keySet());
        newMetadataKeySet.retainAll(metadataValues.keySet());

        // Checking key/value validity

        for (QName currentMetadataKey : newMetadataKeySet) {
            if (knownMetadataMap.get(currentMetadataKey).getType() == CustomMetadataType.URL && metadataValues.get(currentMetadataKey) != null) {
                String currentMetadataValue = metadataValues.get(currentMetadataKey).toString();
                // Check if value contains some protocol, if not, modify it
                if(!currentMetadataValue.isEmpty() && !currentMetadataValue.contains("://")) {
                    currentMetadataValue = "http://" + currentMetadataValue;
                    metadataValues.put(currentMetadataKey, currentMetadataValue);
                }
                if ((!StringUtils.isEmpty(currentMetadataValue)) && (!StringUtils.isUrlRFC3986Valid(currentMetadataValue))) {
                    throw new IllegalArgumentException("URL invalide : " + currentMetadataValue);
                }
            }
        }

        return true;
    }

    private List<CustomMetadataDef> getDefaultMetadataDefs() {
        InputStream is = getClass().getClassLoader().getResourceAsStream("alfresco/module/parapheur/bootstrap/" + DEFAULT_DEFS_FILENAME);
        if (is == null) {
            logger.error("Problem when trying to OPEN resource stream for " + DEFAULT_DEFS_FILENAME);
            return null;
        }
        return getMetadataDefsFromStream(is);
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setRepoAdminService(RepoAdminService repoAdminService) {
        this.repoAdminService = repoAdminService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setStoreRef(String storeRef) {
        this.storeRef = storeRef;
    }

    public void setStoreUrls(List<String> storeUrls) {
        this.storeUrls = storeUrls;
    }

    public void setTypesService(TypesService typesService) {
        this.typesService = typesService;
    }
}
