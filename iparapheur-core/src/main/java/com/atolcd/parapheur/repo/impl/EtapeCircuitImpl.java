/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2012, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD & ADULLACT-projet S.A
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CustomMetadataDef;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.annotations.Annotation;
import lombok.Getter;
import lombok.Setter;
import org.adullact.libersign.util.signature.PKCS7VerUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.MLText;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONException;
import org.springframework.extensions.surf.util.Base64;

import java.io.Serializable;
import java.security.Security;
import java.util.*;

public class EtapeCircuitImpl implements EtapeCircuit {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(EtapeCircuitImpl.class);

    private NodeRef nodeRef;

    private NodeRef parapheur;

    private NodeRef delegateur;
    
    private NodeRef delegue;

    private String parapheurName;

    private String delegateurName;
    
    private String delegueName;

    private boolean approved;

    private String transition; /* PARAPHEUR, CHEF_DE */

    private String actionDemandee; /* 1-VISA, 2-SIGNATURE, 3-TDT, 4-DIFF_EMAIL, 5-ARCHIVAGE */

    private Set<NodeRef> listeNotification;

    private Set<String> listeMetadatas;

    private Set<String> listeMetadatasRefus;

    private String annotation;

    private String annotationPrivee;

    private String signataire;

    private Map<String, Object> signataireCertInfos;

    private Date dateValidation;

    private String signature;

    private String signatureEtape;

    private String validator;

    private HashMap<Integer, ArrayList<Annotation>> annotations;

    private HashMap<DocumentPage, ArrayList<Annotation>> annotationsMultidoc;

    @Getter
    @Setter
    private boolean signed = false;

    //private String notificationsExternes;

    public EtapeCircuitImpl() {
        this.nodeRef = null;
        this.parapheur = null;
        this.delegateur = null;
        this.delegue = null;
        this.parapheurName = null;
        this.delegateurName = null;
        this.delegueName = null;
        this.approved = false;
        this.transition = null;
        this.actionDemandee = null;
        this.listeNotification = null;
        this.listeMetadatas = null;
        this.listeMetadatasRefus = null;
        this.annotation = null;
        this.annotationPrivee = null;
        this.signataire = null;
        this.signataireCertInfos = null;
        this.dateValidation = null;
        this.signature = null;
        this.signatureEtape = null;
        this.validator = null;
    }

    public EtapeCircuitImpl(NodeService nodeService, NodeRef nodeRef) {
        this.nodeRef = nodeRef;

        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
        this.parapheur = (NodeRef) properties.get(ParapheurModel.PROP_PASSE_PAR);
        if (this.parapheur != null) {
            /**
             * this.parapheurName est utile pour Freemarker: afficher le nom
             * du bureau (parapheur) de l'acteur. (cf bordereau)
             */

            /* Cas limite PROP_TITLE peut être de type */
            Object obj = nodeService.getProperty(this.parapheur, ContentModel.PROP_TITLE);

            if (obj instanceof MLText) {
                MLText mlText = (MLText) obj;
                this.parapheurName = mlText.getDefaultValue();
            } else if (obj instanceof String) {
                this.parapheurName = (String) obj;
            } else if (obj != null) {
                this.parapheurName = obj.toString();
            } else {
                this.parapheurName = null;
            }
        }
        //this.delegateur = (NodeRef) properties.get(ParapheurModel.PROP_DELEGATEUR);
        //if (this.delegateur != null) {
            /**
             * this.delegateur est le noderef du parapheur delegateur.
             * Obtenir le Nom du parapheur du delegateur
             * (corrige ticket helpdesk 914 - nom du délégataire mal affiché)
             */
            //this.delegateurName = (String) nodeService.getProperty(this.delegateur, ContentModel.PROP_TITLE).toString();
        //}
        this.delegue = (NodeRef) properties.get(ParapheurModel.PROP_DELEGUE);
        if (this.delegue != null) {
            /**
             * this.delegue est le noderef du parapheur delegué.
             * Obtenir le Nom du parapheur delegué
             * (corrige ticket helpdesk 914 - nom du délégataire mal affiché)
             */
            this.delegueName = (String) nodeService.getProperty(this.delegue, ContentModel.PROP_TITLE).toString();
        }
        Boolean bApproved = (Boolean) properties.get(ParapheurModel.PROP_EFFECTUEE);
        if (bApproved != null) {
            this.approved = bApproved;
        }
        this.actionDemandee = (String) properties.get(ParapheurModel.PROP_ACTION_DEMANDEE);
        List<NodeRef> tListeNotifs = (List<NodeRef>) properties.get(ParapheurModel.PROP_LISTE_NOTIFICATION);
        if (tListeNotifs != null) {
            this.listeNotification = new HashSet<NodeRef>(tListeNotifs);
        }
        List<String> tListeMetas = (List<String>) properties.get(ParapheurModel.PROP_LISTE_METADONNEES);
        if (tListeMetas != null) {
            this.listeMetadatas = new HashSet<String>(tListeMetas);
        }
        List<String> tListeMetasRefus = (List<String>) properties.get(ParapheurModel.PROP_LISTE_METADONNEES_REFUS);
        if (tListeMetasRefus != null) {
            this.listeMetadatasRefus = new HashSet<String>(tListeMetasRefus);
        }
        this.annotation = (String) properties.get(ParapheurModel.PROP_ANNOTATION);
        this.annotationPrivee = (String) properties.get(ParapheurModel.PROP_ANNOTATION_PRIVEE);
        this.signataire = (String) properties.get(ParapheurModel.PROP_SIGNATAIRE);
        this.signataireCertInfos = null;
        this.dateValidation = (Date) properties.get(ParapheurModel.PROP_DATE_VALIDATION);
        this.signature = (String) properties.get(ParapheurModel.PROP_SIGNATURE);
        this.signatureEtape = (String) properties.get(ParapheurModel.PROP_SIGNATURE_ETAPE);
        this.validator = (String) properties.get(ParapheurModel.PROP_VALIDATOR);
        this.signed = properties.containsKey(ParapheurModel.PROP_SIG);
        this.annotations = (HashMap<Integer, ArrayList<Annotation>>) properties.get(ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES);
        this.annotationsMultidoc = (HashMap<DocumentPage, ArrayList<Annotation>>) properties.get(ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES_MULTIDOC);
    }

    /**
     * Constructeur particulier pour avoir les infos du certificat ayant servi
     * pour produire la signature, utile pour la production du bordereau
     *
     * @param nodeService
     * @param nodeRef
     * @param wannaSignataireCertInfo
     */
    public EtapeCircuitImpl(NodeService nodeService, NodeRef nodeRef, boolean wannaSignataireCertInfo) {
        this.nodeRef = nodeRef;

        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
        this.parapheur = (NodeRef) properties.get(ParapheurModel.PROP_PASSE_PAR);
        if (this.parapheur != null) {
            /**
             * this.parapheurName est utile pour Freemarker: afficher le nom
             * du bureau (parapheur) de l'acteur. (cf bordereau)
             */
            this.parapheurName = (String) nodeService.getProperty(this.parapheur, ContentModel.PROP_TITLE);
        }
        //this.delegateur = (NodeRef) properties.get(ParapheurModel.PROP_DELEGATEUR);
        //if (this.delegateur != null) {
            /**
             * this.delegateur est le noderef du parapheur delegateur.
             * Obtenir le Nom du parapheur du delegateur
             * (corrige ticket helpdesk 914 - nom du délégataire mal affiché)
             */
            //this.delegateurName = (String) nodeService.getProperty(this.delegateur, ContentModel.PROP_TITLE).toString();
        //}
        this.delegue = (NodeRef) properties.get(ParapheurModel.PROP_DELEGUE);
        if (this.delegue != null) {
            /**
             * this.delegue est le noderef du parapheur delegué.
             * Obtenir le Nom du parapheur delegué
             * (corrige ticket helpdesk 914 - nom du délégataire mal affiché)
             */
            this.delegueName = (String) nodeService.getProperty(this.delegue, ContentModel.PROP_TITLE).toString();
        }
        Boolean bApproved = (Boolean) properties.get(ParapheurModel.PROP_EFFECTUEE);
        if (bApproved != null) {
            this.approved = bApproved;
        }
        this.actionDemandee = (String) properties.get(ParapheurModel.PROP_ACTION_DEMANDEE);
        List<NodeRef> tListeNotifs = (List<NodeRef>) properties.get(ParapheurModel.PROP_LISTE_NOTIFICATION);
        if (tListeNotifs != null) {
            this.listeNotification = new HashSet<NodeRef>(tListeNotifs);
        }
        List<String> tListeMetas = (List<String>) properties.get(ParapheurModel.PROP_LISTE_METADONNEES);
        if (tListeMetas != null) {
            this.listeMetadatas = new HashSet<String>(tListeMetas);
        }
        List<String> tListeMetasRefus = (List<String>) properties.get(ParapheurModel.PROP_LISTE_METADONNEES_REFUS);
        if (tListeMetasRefus != null) {
            this.listeMetadatasRefus = new HashSet<String>(tListeMetasRefus);
        }
        this.annotation = (String) properties.get(ParapheurModel.PROP_ANNOTATION);
        this.annotationPrivee = (String) properties.get(ParapheurModel.PROP_ANNOTATION_PRIVEE);
        this.dateValidation = (Date) properties.get(ParapheurModel.PROP_DATE_VALIDATION);
        // Prenom Nom du signataire (affichable)
        this.signataire = (String) properties.get(ParapheurModel.PROP_SIGNATAIRE);
        this.validator = (String) properties.get(ParapheurModel.PROP_VALIDATOR);
        /**
         * En cas d'etape de signature
         */
        if (ETAPE_SIGNATURE.equals(actionDemandee)) {
            // objet de signature electronique
            this.signatureEtape = (String) properties.get(ParapheurModel.PROP_SIGNATURE_ETAPE);

            // Libelle d'info de signature: "Certificat émis...." ou du JSON
            this.signature = (String) properties.get(ParapheurModel.PROP_SIGNATURE);
            /**
             * controle en cas de signature CMS: on tente l'ouverture.
             * Si probleme (enregistrement moisi): on purge le libelle d'info.
             */
            if (signatureEtape != null && signatureEtape.startsWith("-----BEGIN")) {
                byte[] sigBs = Base64.decode(signatureEtape);
                Security.addProvider(new BouncyCastleProvider());
                try {
                    CMSSignedData signedData = new CMSSignedData(PKCS7VerUtil.pem2der(sigBs, "-----BEGIN".getBytes(), "-----END".getBytes()));
                    if (signedData.getSignerInfos() == null) {
                        this.signature = null;
                    }
                } catch (CMSException ex) {
                    this.signature = null;
                }
            }

            if (wannaSignataireCertInfo && this.signature != null && !this.signature.isEmpty() && this.signature.startsWith("{\"")) {
                try {
                    /**
                     * Alors la propriété "signature" contient du JSON.
                     * On decode et on renseigne la map pour FreeMarker
                     */
                    this.signataireCertInfos = org.alfresco.util.JSONtoFmModel.convertJSONObjectToMap(signature);
                } catch (JSONException ex) {
                    log.error("could not parse JSON info in 'signature' field.", ex);
                    this.signataireCertInfos = null;
                }
            } else {
                this.signataireCertInfos = null;
            }
        } else {
            // tout un tas de proprietes à null
            this.signature = null;
            this.signatureEtape = null;
            this.signataireCertInfos = null;
        }
    }

    @Override
    public NodeRef getNodeRef() {
        return nodeRef;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getParapheur()
     */
    @Override
    public NodeRef getParapheur() {
        return this.parapheur;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getParapheurName()
     */
    @Override
    public String getParapheurName() {
        return this.parapheurName;
    }

    @Override
    public NodeRef getDelegateur() {
        return this.delegateur;
    }

    @Override
    public String getDelegateurName() {
        return this.delegateurName;
    }
    
    @Override
    public NodeRef getDelegue() {
        return this.delegue;
    }

    @Override
    public String getDelegueName() {
        return this.delegueName;
    }

    public void setParapheur(NodeRef noderef) {
        this.parapheur = noderef;
    }

    public void setParapheurName(String name) {
        this.parapheurName = name;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#isApproved()
     */
    @Override
    public boolean isApproved() {
        return this.approved;
    }

    @Override
    public String getTransition() {
        return this.transition;
    }

    public void setTransition(String str) {
        this.transition = str;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getActionDemandee()
     */
    @Override
    public String getActionDemandee() {
        return this.actionDemandee;
    }

    public void setActionDemandee(String str) {
        this.actionDemandee = str;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getListeDiffusion()
     */
    @Override
    public Set<NodeRef> getListeNotification() {
        /*
        if (this.listeNotification.trim().isEmpty())
        {
        return null;
        }
        Set<NodeRef> res = new HashSet<NodeRef>();
        if (this.listeNotification.endsWith(","))   // nettoyage du ',' final
        this.listeNotification.substring(0, this.listeNotification.length());
        String[] tab = this.listeNotification.split(",");
        for (String str : tab)
        res.add(new NodeRef(str));*/
        return this.listeNotification;
    }

    /**
     * @param notifiables
     */
    public void setListeDiffusion(Set<NodeRef> notifiables) {
        this.listeNotification = notifiables;
    }

    public Set<String> getListeMetadatas() {
        return listeMetadatas;
    }

    public void setListeMetadatas(Set<String> listeMetadatas) {
        this.listeMetadatas = listeMetadatas;
    }

    public Set<String> getListeMetadatasRefus() {
        return listeMetadatasRefus;
    }

    public void setListeMetadatasRefus(Set<String> listeMetadatasRefus) {
        this.listeMetadatasRefus = listeMetadatasRefus;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#setAndRecordListeDiffusion(Set<NodeRef> liste)
     */
    @Override
    public void setAndRecordListeDiffusion(NodeService nodeService, Set<NodeRef> liste) {
        this.listeNotification = liste;
        if (this.nodeRef != null) {
            nodeService.setProperty(nodeRef, ParapheurModel.PROP_LISTE_DIFFUSION, (Serializable) liste);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getAnnotation()
     */
    @Override
    public String getAnnotation() {
        return this.annotation;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getAnnotationPrivee()
     */
    @Override
    public String getAnnotationPrivee() {
        return this.annotationPrivee;
    }

    @Override
    public HashMap<String, ArrayList<Annotation>> getGraphicalAnnotations() {
        /* FIXME: find a less hacky way (alfresco wraps the hashmap as SimpleHash.
  SimpleHash works only with string keys. */
        HashMap<String, ArrayList<Annotation>> retVal = null;
        if (annotations != null) {
            retVal = new HashMap<String, ArrayList<Annotation>>();

            for (Integer key : annotations.keySet()) {
                retVal.put(key.toString(), annotations.get(key));
            }
        }

        return retVal;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getSignataire()
     */
    @Override
    public String getSignataire() {
        return this.signataire;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getDateValidation()
     */
    @Override
    public Date getDateValidation() {
        return this.dateValidation;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getSignature()
     */
    @Override
    public String getSignature() {
        return signature;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getSignature()
     */
    @Override
    public Map<String, Object> getSignataireCertInfos() {
        return signataireCertInfos;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getSignatureElectronique()
     */
    @Override
    public String getSignatureEtape() {
        return signatureEtape;
    }

    /**
     * @see com.atolcd.parapheur.repo.EtapeCircuit#getNotificationsExternes()
     */
    @Override
    public String getNotificationsExternes() {
        // Pour l'instant, ca sert a rien
        return null;
    }

    @Override
    public String getValidator() {
        return this.validator;
    }

    @Override
    public void setValidator(String username) {
        this.validator = username;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof EtapeCircuitImpl)) {
            return false;
        }

        EtapeCircuitImpl etape = (EtapeCircuitImpl) o;
        return etape.nodeRef.equals(this.nodeRef);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.nodeRef != null ? this.nodeRef.hashCode() : 0);
        return hash;
    }

}
