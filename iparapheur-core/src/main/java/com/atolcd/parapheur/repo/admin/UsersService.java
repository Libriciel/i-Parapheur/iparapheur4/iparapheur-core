/*
 * Version 3.3
 * CeCILL Copyright (c) 2006-2013, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo.admin;

import org.alfresco.service.NotAuditable;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.json.JSONException;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Berger Levrault - Mathieu Passenaud
 * @author ADULLACT Projet - Jason Maire
 */
public interface UsersService {

    String getUserTicketWithOpenId(String bearerToken) throws IOException, JSONException;

    /**
     * Créé un nouvel utilisateur
     * @param userName
     * @param email
     * @param firstName
     * @param lastName
     * @param password
     * @return l'id de l'utilisateur nouvellement créé.
     */
    @NotAuditable
    public String createUser(String userName, String email, String firstName, String lastName, String password);

    /**
     * Renvoie la liste des utilisateurs
     * @param search
     * @return
     */
    @NotAuditable
    public List<NodeRef> searchUser(String search);

    /**
     * Suppression d'un utilisateur. Si l'utilisateur est associé à un bureau, on le dissocie
     * avant de le supprimer.
     * @param user le nodeRef de l'utilisateur
     */
    @NotAuditable
    public void deleteUser(NodeRef user);

    
    /**
     * Hack pour exporter les données
     */
    @NotAuditable
    public void export();

    Pair<String, String> getTicketForUser(String username) throws Exception;

    /**
     * Creation d'un ticket pour login via certificats par l'API
     *
     * @param certs chaine de certificats
     * @return Le ticket
     */
    @NotAuditable
    public Pair<String, String> getTicketWithCertificate(X509Certificate[] certs);

    /**
     * Renvoie les bureaux dont l'utilisateur passé en paramètre (username) est propriétaire ou secrétaire.
     * @param username le username de l'utilisateur.
     * @return les bureaux dont l'utilisateur passé en paramètre (username) est propriétaire  ou secrétaire.
     */
    @NotAuditable
    List<NodeRef> getBureaux(String username);

    /**
     * Renvoie les bureaux dont l'utilisateur passé en paramètre (username) est propriétaire.
     * @param username le username de l'utilisateur.
     * @return les bureaux dont l'utilisateur passé en paramètre (username) est propriétaire.
     */
    @NotAuditable
    List<NodeRef> getBureauxProprietaire(String username);

    /**
     * Renvoie les bureaux dont l'utilisateur passé en paramètre (username) est secrétaire.
     * @param username le username de l'utilisateur.
     * @return les bureaux dont l'utilisateur passé en paramètre (username) est secrétaire.
     */
    @NotAuditable
    List<NodeRef> getBureauxSecretaire(String username);

    /**
     * Supprime l'utilisateur de la liste des propriétaires du bureau si isProprietaire est vrai,
     * sinon supprime l'utilisateur de la liste des secrétaires du bureau.
     *
     * @param user
     * @param bureau
     * @param isProprietaire
     */
    void removeUserFromBureau(NodeRef user, NodeRef bureau, boolean isProprietaire);

    /**
     * Ajoute l'utilisateur à la liste des propriétaires du bureau si isProprietaire est vrai,
     * sinon ajoute l'utilisateur à la liste des secrétaires du bureau.
     *
     * @param user
     * @param bureau
     * @param isProprietaire
     */
    void addUserToBureau(NodeRef user, NodeRef bureau, boolean isProprietaire);

    /**
     * Renvoie les bureaux administrés par l'utilisateur passé en paramètre.
     * @param username le username de l'utilisateur
     * @return les bureaux dont l'utilisateur passé en paramètre est propriétaire.
     */
    @NotAuditable
    List<NodeRef> getBureauxAdministres(String username);

    /**
     * Indique si l'utilisateur est administrateur de l'application.
     * @param username le username de l'utilisateur
     * @return vrai si l'utilisateur est administrateur, faux sinon.
     */
    @NotAuditable
    boolean isAdministrateur(String username);

    /**
     * Indique si l'utilisateur est administrateur fonctionnel.
     * @param username le username de l'utilisateur
     * @return vrai si l'utilisateur est administrateur fonctionnel, faux sinon.
     */
    @NotAuditable
    boolean isAdministrateurFonctionnel(String username);

    /**
     * Indique si l'utilisateur est gestionnaire de circuit.
     * @param username le username de l'utilisateur
     * @return vrai si l'utilisateur est gestionnaire de circuit, faux sinon.
     */
    @NotAuditable
    boolean isGestionnaireCircuit(String username);

    /**
     * Modifie l'utilisateur avec les propriétés passées en paramètre.
     * @param userRef le nodeRef de l'utilisateur à modifier.
     * @param propertiesMap les propriétés à mettre à jour.
     */
    @NotAuditable
    void updateUser(NodeRef userRef, HashMap<QName, Serializable> propertiesMap);

    /**
     * Rend l'utilisateur passé en paramètre administrateur de l'application.
     * @param username le username de l'utilisateur.
     */
    @NotAuditable
    void addToAdminGroup(String username);

    /**
     * Supprime les droits d'administration d'un utilisateur.
     * @param username le username de l'utilisateur.
     */
    @NotAuditable
    void removeFromAdminGroup(String username);

    /**
     * Indique si l'utilisateur passé en paramètre est propriétaire d'au moins un bureau.
     * @param username le username de l'utilisateur.
     * @return Vrai si l'utilisateur est propriétaire d'au moins un bureau, faux sinon.
     */
    @NotAuditable
    boolean isProprietaire(String username);

    /**
     * Indique si l'utilisateur passé en paramètre est secrétaire d'au moins un bureau.
     * @param username le username de l'utilisateur.
     * @return Vrai si l'utilisateur est secrétaire d'au moins un bureau, faux sinon.
     */
    @NotAuditable
    boolean isSecretaire(String username);

    /**
     * Renvoie l'id du noeud contenant le scan de signature de l'utilisateur
     * ou null si l'utilisateur n'a pas de signature.
     * @param user
     * @return l'id du noeud contenant le scan de signature de l'utilisateur.
     */
    String getSignature(NodeRef user);

    /**
     * Créé ou remplace le scan de signature de l'utilisateur.
     * @param user le nodeRef de l'utilisateur
     * @param signatureB64 l'image encodée en base 64
     * @return l'id du noeud contenant le scan de signature.
     */
    String setSignature(NodeRef user, String signatureB64);

    /**
     * Supprime l'image de signature associée à l'utilisateur.
     * @param user le nodeRef de l'utilisateur
     */
    void deleteSignature(NodeRef user);

    /**
     * Renvoie le certificat de connexion de l'utilisateur (base 64)
     * ou null si l'utilisateur n'a pas de certificat.
     * @param user
     * @return le contenu du certificat de connexion de l'utilisateur.
     */
    String getCertificat(NodeRef user);

    /**
     * Renvoie VRAI si l'utilisateur passé en paramètre est associé à
     * un certificat de connexion
     * @param user
     * @return true si l'utilisateur possède un certificat, false sinon
     */
    boolean hasCertificat(NodeRef user);

    /**
     * Créé ou remplace le certificat de connexion de l'utilisateur.
     * @param user le nodeRef de l'utilisateur
     * @param certificatB64 le certificat en base 64
     */
    java.util.Map<String, String> setCertificat(NodeRef user, String certificatB64);

    /**
     * Supprime le certificat de connexion associée à l'utilisateur.
     * @param user le nodeRef de l'utilisateur
     */
    void deleteCertificat(NodeRef user);

    /**
     * Récupère le ticket de l'utilisateur actuellement connecté
     * @return le ticket
     */
    String getCurrentUserTicket();

    /**
     * Envoie un lien de reset de mot de passe
     * @param username nom de l'utilisateur
     * @param email le mail de l'utilisateur
     * @return l'état de l'envoi : 0 = OK, 1 = KO, 2 = User ldap
     */
    int resetPasswordRequest(String username, String email);


    /**
     * Reset de mot de passe
     * @param user username
     * @param uid uuid unique et temporaire de la demande de reset de password l'utilisateur
     * @param password nouveau mot de passe
     * @return l'état du changement de mdp : 0 = OK, 1 = KO
     */
    int resetPassword(String user, String uid, String password);
}
