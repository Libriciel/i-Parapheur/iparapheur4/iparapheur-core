/*
 * Version 2.1
 * CeCILL Copyright (c) 2008-2012, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 * 
 */
package com.atolcd.parapheur.repo.patch;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author jmaire
 */
public class RepriseMailTemplatePatch extends XPathBasedPatch {

    private Properties configuration;
    private ContentService contentService;

    private static Log logger = LogFactory.getLog(RepriseMailTemplatePatch.class);

    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    @Override
    protected String getXPathQuery() {

        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/"
                + this.configuration.getProperty("spaces.dictionary.childname") + "/"
                + this.configuration.getProperty("spaces.templates.email.childname");
        return xpath;
    }

    @Override
    protected void patch(NodeRef nodeToPatch) throws Exception {
        NodeService nodeService = getNodeService();
        SearchService searchService = getSearchService();
        NamespaceService namespaceService = getNamespaceService();


        String xpath = "app:company_home/app:dictionary/app:email_templates/cm:parapheur-next-reprise.ftl";
        List<NodeRef> results = searchService.selectNodes(getRootNode(),
                xpath,
                null,
                namespaceService,
                false);

        logger.warn("Nodes Found matching cm:parapheur-next-reprise.ftl == " + results);

        if (results == null || results.isEmpty()) {

            InputStream viewStream = getClass().getClassLoader().getResourceAsStream("alfresco/module/parapheur/bootstrap/parapheur-next-reprise.ftl");

            HashMap<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, "parapheur-next-reprise.ftl");
            properties.put(ContentModel.PROP_TITLE, "parapheur-next-reprise.ftl");

            NodeRef child = nodeService.createNode(nodeToPatch, ContentModel.ASSOC_CONTAINS, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "parapheur-next-reprise.ftl"), ContentModel.TYPE_CONTENT, properties).getChildRef();

            ContentWriter writer = contentService.getWriter(child, ContentModel.PROP_CONTENT, true);

            writer.setEncoding("UTF-8");
            writer.setMimetype("text/plain");
            writer.putContent(viewStream);

            logger.warn("The new email template node was succesfully created");
        }
    }
}
