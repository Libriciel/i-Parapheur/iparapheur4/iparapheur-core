package com.atolcd.parapheur.repo.patch;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class SecureMailTemplatePatch extends XPathBasedPatch {
    private Properties configuration;
    private ContentService contentService;

    private static Log logger = LogFactory.getLog(SecureMailTemplatePatch.class);

    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    @Override
    protected String getXPathQuery() {

        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/"
                + this.configuration.getProperty("spaces.dictionary.childname") + "/"
                + this.configuration.getProperty("spaces.templates.email.childname");
        return xpath;
    }

    @Override
    protected void patch(NodeRef nodeToPatch) throws Exception {
        NodeService nodeService = getNodeService();
        SearchService searchService = getSearchService();
        NamespaceService namespaceService = getNamespaceService();


        String xpath = "app:company_home/app:dictionary/app:email_templates/cm:parapheur-mailsec-template.ftl";
        List<NodeRef> results = searchService.selectNodes(getRootNode(),
                xpath,
                null,
                namespaceService,
                false);

        logger.warn("Nodes Found matching cm:parapheur-mailsec-template.ftl == " + results);

        if (results == null || results.isEmpty()) {

            InputStream viewStream = getClass().getClassLoader().getResourceAsStream("alfresco/module/parapheur/bootstrap/parapheur-mailsec-template.ftl");

            HashMap<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, "parapheur-mailsec-template.ftl");
            properties.put(ContentModel.PROP_TITLE, "parapheur-mailsec-template.ftl");

            NodeRef child = nodeService.createNode(nodeToPatch, ContentModel.ASSOC_CONTAINS, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "parapheur-mailsec-template.ftl"), ContentModel.TYPE_CONTENT, properties).getChildRef();

            ContentWriter writer = contentService.getWriter(child, ContentModel.PROP_CONTENT, true);

            writer.setEncoding("UTF-8");
            writer.setMimetype("text/plain");
            writer.putContent(viewStream);

            logger.warn("The new email template node was succesfully created");
        }

    }
}
