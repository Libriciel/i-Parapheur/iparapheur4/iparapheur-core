package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.repo.DossierService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;

public class ResetPermsAllFolders extends XPathBasedPatch {

    public static final String FOLDERS_XPATH_QUERY = "//*[subtypeOf('ph:dossier')]";

    private static final Logger logger = Logger.getLogger(ResetPermsAllFolders.class);

    private DossierService dossierService;

    private void patchNode(NodeRef nodeRef) {
        logger.info("Setting permission on folder ID=" + nodeRef.getId());
        dossierService.setPermissionsDossier(nodeRef);
    }

    public void setDossierService(DossierService dossierService) {
        this.dossierService = dossierService;
    }

    @Override
    protected String getXPathQuery() {
        return FOLDERS_XPATH_QUERY;
    }

    @Override
    protected void patch(NodeRef nodeToPatch) {
        patchNode(nodeToPatch);
    }

}
