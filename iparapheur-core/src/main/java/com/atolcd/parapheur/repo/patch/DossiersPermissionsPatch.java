/*
 * Version 2.1
 * CeCILL Copyright (c) 2008-2013, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.DossierService;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * Created with IntelliJ IDEA.
 * User: jmaire
 * Date: 14/03/13
 * Time: 14:09
 * To change this template use File | Settings | File Templates.
 */
public class DossiersPermissionsPatch extends XPathBasedPatch {

    private DossierService dossierService;
    private int nbDossiers = 0;

    @Override
    protected String getXPathQuery() {
        return "/app:company_home/ph:parapheurs//*[subtypeOf('ph:dossier') and @ph:public='false']";
    }

    @Override
    protected void patch(NodeRef nodeToPatch) throws Exception {
        dossierService.setConfidentialPermissionsForDossier(nodeToPatch);
        nbDossiers ++;
        if (nbDossiers % 100 == 0) {
            System.out.println("\t" + nbDossiers + " dossiers traités");
        }
    }

    @Override
    protected void afterCommit() {
        super.afterCommit();
        System.out.println("Nombre de dossiers traités : " + nbDossiers);
    }

    public void setDossierService(DossierService dossierService) {
        this.dossierService = dossierService;
    }
}