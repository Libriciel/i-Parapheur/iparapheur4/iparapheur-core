/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;
import fr.starxpert.iparapheur.audit.repo.AuditParapheurServiceCallback;
import fr.starxpert.iparapheur.audit.repo.extractor.DureeDataExtractor;
import org.alfresco.repo.admin.patch.AbstractPatch;
import org.alfresco.repo.domain.audit.AuditDAO;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;

import java.io.Serializable;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import javax.transaction.UserTransaction;
import org.adullact.iparapheur.status.StatusMetier;

import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.audit.AuditService;


/**
 * @author eperalta
 */
public class AuditMigrationPatch extends AbstractPatch {

    private static Logger logger = Logger.getLogger(AuditMigrationPatch.class);
    private SessionFactory sessionFactory;

    private AuditDAO auditDAO;
    private String username;
    private String databaseURL;
    private String password;

    private AuditService auditService;
    private ParapheurService parapheurService;
    private TenantService tenantService;

    private AuthenticationComponent authenticationComponent;
    //public boolean breakPatch = true;

    public AuthenticationComponent getAuthenticationComponent() {
        return authenticationComponent;
    }

    public void setAuthenticationComponent(AuthenticationComponent authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }

    public AuditService getAuditService() {
        return auditService;
    }

    public void setAuditService(AuditService auditService) {
        this.auditService = auditService;
    }

    public ParapheurService getParapheurService() {
        return parapheurService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public String getDatabaseURL() {
        return databaseURL;
    }

    public void setDatabaseURL(String databaseURL) {
        this.databaseURL = databaseURL;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAuditDAO(AuditDAO auditDAO) {
        this.auditDAO = auditDAO;
    }

    public AuditDAO getAuditDAO() {
        return this.auditDAO;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    public TenantService getTenantService() {
        return tenantService;
    }

    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }


    /**
     * Tries to get a connection to the database using the parameters defined in alfresco-global.properties
     *
     * @return a Connection to the database
     * @throws Throwable if the connection to the database fails.
     */
    public Connection getDatabaseConnection() throws SQLException {
        assert (this.databaseURL != null);
        assert (this.username != null);
        assert (this.password != null);

        // Connection à la base de donnée
        return DriverManager.getConnection(this.databaseURL, this.username, this.password);
    }

    /**
     * Converts the timestamp stored as a String (yyyy-MM-dd HH:mm:ss) in the database
     *
     * @param timestamp the timestamp string pulled from the database
     * @return the timestamp in milliseconds (unix time ?)
     * @throws ParseException if the date parser fails to interpret the timestamp string.
     */
    private long timestampToMillis(String timestamp) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Calendar cal = new GregorianCalendar();

        Date date = sdf.parse(timestamp);

        cal.setTime(date);

        return cal.getTimeInMillis();
    }

    /**
     * Builds an SQL query string in function of the database type
     *
     * @return the SQL query string
     * @throws UnsupportedOperationException if the database engine isn't supported
     */
    private String getQueryForAuditTrail() throws UnsupportedOperationException {
        String query = null;

        if (this.databaseURL.contains("mysql")) {
            query = "SELECT aaf.* FROM alf_audit_fact aaf, alf_audit_source aas "
                    + "WHERE aas.id=aaf.audit_source_id AND aas.application='ParapheurService' "
                    + "AND aas.service IS NULL AND aaf.fail=false ORDER BY aaf.timestamp ";
        } else if (this.databaseURL.contains("postgresql")) {
            throw new UnsupportedOperationException("Migration audit trail (< 3.4.c) on postgresql database currently unsupported");
        }

        return query;
    }

    private String getQueryForAuditStats() {
        String query = null;

        if (this.databaseURL.contains("mysql")) {
            query = "SELECT aas.method, aaf.* FROM alf_audit_fact aaf, alf_audit_source aas "
                    + "WHERE aas.id=aaf.audit_source_id AND aas.application='ParapheurService' "
                    + "AND aas.service='ParapheurService' AND aaf.fail=false ORDER BY aaf.timestamp";
        } else if (this.databaseURL.contains("postgresql")) {
            throw new UnsupportedOperationException("Migration audit stats (< 3.4.c) on postgresql database currently unsupported");
        }

        return query;
    }

    /**
     * Handles the data got from the audit trail query
     *
     * @param applicationName the audit application where we insert the audit entry.
     * @param rs              the resultset returned by the query
     * @return the count of rows handled
     * @throws ParseException if the timestamp string can't be parsed
     * @throws SQLException   if the columns aren't defined
     */
    private int handleAuditTrailResultSet(String applicationName, ResultSet rs) throws SQLException, ParseException {
        AuditDAO.AuditApplicationInfo application = auditDAO.getAuditApplication(applicationName);
        Calendar cal = new GregorianCalendar();

        int count = 0;
        while (rs.next()) {
            /*
             * Récupération des différents éléments dans la table alf_audit_fact
             * pour insertion dans le nouvel audit via auditDAO.createAuditEntry.
             */
            String timestamp = rs.getString("timestamp");
            String user_id = rs.getString("user_id");
            String store_protocol = rs.getString("store_protocol");
            String store_id = rs.getString("store_id");
            String node_uuid = rs.getString("node_uuid");
            String message = rs.getString("message_text");
            String node = store_protocol + "://" + store_id + "/" + node_uuid;
            String status = rs.getString("arg_1");

            NodeRef dossier = new NodeRef(node);
            List<Serializable> list = new ArrayList<Serializable>();

            long millitime = timestampToMillis(timestamp);

            /**
             * extraction du statut (le statut étant dans une liste lorsqu'il est sérialisé via toString
             * @see java.util.List#toString()
             */
            if (status != null && status.startsWith("[")) {
                status = status.substring(1, status.length() - 1);
            }

            list.add(status);

            Map<String, Serializable> values = new HashMap<String, Serializable>();

            values.put("message", message);
            values.put("dossier", dossier);
            values.put("list", (Serializable) list);

            try {

                if (user_id.contains("@")) {
                    String domain = tenantService.getDomain(user_id);
                    this.authenticationComponent.setSystemUserAsCurrentUser(domain);
                } else {
                    this.authenticationComponent.setSystemUserAsCurrentUser();
                }


                try {
                    this.authenticationComponent.setCurrentUser(user_id);
                } catch (Exception e) {
                    logger.warn("Unable to set user : " + user_id);
                    logger.warn(String.format("Falling back on %s tenant admin", tenantService.getDomain(user_id)));
                }

                this.auditDAO.createAuditEntry(application.getId(), millitime, this.authenticationComponent.getCurrentUserName(), values);
                /*
                if (user_id.contains("@")) {
                String domain = tenantService.getDomain(user_id);
                this.authenticationComponent.setSystemUserAsCurrentUser(domain);
                }
                else {
                this.authenticationComponent.setSystemUserAsCurrentUser();
                }
                 */
                this.authenticationComponent.setSystemUserAsCurrentUser();
                count++;
            } catch (Exception e) {
                logger.warn("Unable to set user " + user_id);
            }

        }
        return count;
    }

    /**
     * Handles the results got from the audit stat query
     *
     * @param rs the results set returned by the query
     * @return
     * @throws Throwable
     */
    public int handleAuditStatsResultSet(ResultSet rs) throws Exception {
        int count = 0;
        while (rs.next()) {
            try {
                String method;
                method = rs.getString("method");
                System.out.println(method);
                if (method.equals("createDossier")) {
                    insertAuditFactInAuditCreation(rs);
                } else if (method.equals("archive")) {
                    try {
                        insertAuditFactInAuditInstruit(rs);
                    } catch (Throwable t) {
                        t.printStackTrace();
                        throw new Exception("An error occurred while inserting audit facts into AuditInstruit");
                    }
                } else if (method.equals("reject")) {
                    try {
                        insertAuditFactInAuditRejet(rs);
                    } catch (Throwable t) {
                        throw new Exception("An error occurred while inserting audit facts into AuditRejet");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
        }

        return count;
    }

    /**
     * Inserts the data pulled from the database into the AuditCreation application.
     *
     * @param rs
     * @throws Throwable
     */
    private void insertAuditFactInAuditCreation(ResultSet rs) throws Exception {
        AuditDAO.AuditApplicationInfo application = auditDAO.getAuditApplication(AuditParapheurService.AUDIT_CREATION_APPLICATION);
        String user_id = null;
        try {
            String timestamp = rs.getString("timestamp");
            user_id = rs.getString("user_id");
            if (user_id.contains("@")) {
                String domain = tenantService.getDomain(user_id);
                this.authenticationComponent.setSystemUserAsCurrentUser(domain);
            } else {
                this.authenticationComponent.setSystemUserAsCurrentUser();
            }
            String store_protocol = rs.getString("store_protocol");
            String store_id = rs.getString("store_id");
            String node_uuid = rs.getString("node_uuid");
            //String message = rs.getString("message_text");
            String node = store_protocol + "://" + store_id + "/" + node_uuid;
            String type = rs.getString("arg_2");
            String sousType = rs.getString("arg_3");

            String parapheur = rs.getString("arg_1");
            String parapheurNode = String.format("%s://%s/%s", store_protocol, store_id, parapheur);


            long millitime = timestampToMillis(timestamp);
            NodeRef parapheurRef = new NodeRef(parapheurNode);


            NodeRef nodeRef = new NodeRef(node);

            HashMap<QName, Serializable> auditedProperties = new HashMap<QName, Serializable>();
            Map<String, Serializable> values = new HashMap<String, Serializable>();

            auditedProperties.put(ParapheurModel.PROP_TYPE_METIER, type);
            auditedProperties.put(ParapheurModel.PROP_SOUSTYPE_METIER, sousType);

            values.put(AuditParapheurService.AUDIT_CREATION_PARAPHEUR, parapheurRef);
            values.put(AuditParapheurService.AUDIT_CREATION_PROPERTIES, auditedProperties);
            values.put(AuditParapheurService.AUDIT_CREATION_NODEREF, nodeRef);

            this.authenticationComponent.setCurrentUser(user_id);

            this.auditDAO.createAuditEntry(application.getId(), millitime, this.authenticationComponent.getCurrentUserName(), values);


        } catch (Exception e) {
            logger.warn("Unable to set user " + user_id);
        }
        this.authenticationComponent.setSystemUserAsCurrentUser();
    }

    /**
     * Inserts the data pulled from the database into the AuditRejet application.
     *
     * @param rs
     * @throws Throwable
     */
    private void insertAuditFactInAuditRejet(ResultSet rs) throws Throwable {
        AuditDAO.AuditApplicationInfo application = auditDAO.getAuditApplication(AuditParapheurService.AUDIT_REJET_APPLICATION);

        String user_id = null;
        try {
            /* getting interesting values from the tuple */
            user_id = rs.getString("user_id");
            String timestamp = rs.getString("timestamp");
            if (user_id.contains("@")) {
                String domain = tenantService.getDomain(user_id);
                this.authenticationComponent.setSystemUserAsCurrentUser(domain);
            } else {
                this.authenticationComponent.setSystemUserAsCurrentUser();
            }
            String store_protocol = rs.getString("store_protocol");
            String store_id = rs.getString("store_id");

            String dossier_uuid = rs.getString("node_uuid");

            String nodeURI = String.format("%s://%s/%s", store_protocol, store_id, dossier_uuid);
            //String parapheurURI = String.format("%s://%s/%s", store_protocol, store_id, rs.getString("node_uuid"));
            String parapheur = rs.getString("arg_1");
            String parapheurNode = String.format("%s://%s/%s", store_protocol, store_id, parapheur);

            NodeRef nodeRef = new NodeRef(nodeURI);
            NodeRef parapheurRef = new NodeRef(parapheurNode);
/*
            if (breakPatch == true) {
                parapheurRef = null;
            }
*/
            String type = rs.getString("arg_2");
            String subType = rs.getString("arg_3");

            long millitime = timestampToMillis(timestamp);

            DureeDataExtractor dureeDataExtractor = new DureeDataExtractor();
            dureeDataExtractor.setNodeService(nodeService);
            dureeDataExtractor.setCurrentTime(millitime);
            dureeDataExtractor.setAuditService(auditService);
            Long duree = (Long) dureeDataExtractor.extractData(nodeRef);

            /* Building a value map that mimics the value map generated by the Audit System*/
            Map<String, Serializable> values = new HashMap<String, Serializable>();

            values.put(AuditParapheurService.AUDIT_REJET_NODEREF, nodeRef);
            values.put(AuditParapheurService.AUDIT_REJET_PARAPHEUR, parapheurRef);
            values.put(AuditParapheurService.AUDIT_REJET_TYPEMETIER, type);
            values.put(AuditParapheurService.AUDIT_REJET_SOUSTYPEMETIER, subType);
            values.put(AuditParapheurService.AUDIT_REJET_DUREE, duree);

            try {
                this.authenticationComponent.setCurrentUser(user_id);
            } catch (Exception e) {
                logger.warn("Unable to set user : " + user_id);
                logger.warn(String.format("Falling back on %s tenant admin", tenantService.getDomain(user_id)));
            }

            this.auditDAO.createAuditEntry(application.getId(), millitime, this.authenticationComponent.getCurrentUserName(), values);


        } catch (Exception e) {
            logger.warn("Unable to set user " + user_id);
        }
        this.authenticationComponent.setSystemUserAsCurrentUser();
    }

    /**
     * Inserts the data pulled from the database into the AuditInstruit application.
     *
     * @param rs
     * @throws Throwable
     */
    private void insertAuditFactInAuditInstruit(ResultSet rs) throws Throwable {
        AuditDAO.AuditApplicationInfo application = auditDAO.getAuditApplication(AuditParapheurService.AUDIT_INSTRUIT_APPLICATION);
        String user_id = null;
        try {
            user_id = rs.getString("user_id");
            /* getting interesting values from the tuple */
            String timestamp = rs.getString("timestamp");
            if (user_id.contains(TenantService.SEPARATOR)) {
                String domain = tenantService.getDomain(user_id);
                this.authenticationComponent.setSystemUserAsCurrentUser(domain);
            } else {
                this.authenticationComponent.setSystemUserAsCurrentUser();
            }

            String store_protocol = rs.getString("store_protocol");
            String store_id = rs.getString("store_id");

            String dossier_uuid = rs.getString("node_uuid");

            String nodeURI = String.format("%s://%s/%s", store_protocol, store_id, dossier_uuid);
            String parapheur = rs.getString("arg_1");
            String parapheurNode = String.format("%s://%s/%s", store_protocol, store_id, parapheur);

            NodeRef nodeRef = new NodeRef(nodeURI);
            NodeRef parapheurRef = new NodeRef(parapheurNode);
            /*
            if (breakPatch == true) {
                parapheurRef = null;
            }
             */
            String type = rs.getString("arg_2");
            String subType = rs.getString("arg_3");

            long millitime = timestampToMillis(timestamp);

            try {
                this.authenticationComponent.setCurrentUser(user_id);
            } catch (Exception e) {
                logger.warn("Unable to set user : " + user_id);
                logger.warn(String.format("Falling back on %s tenant admin", tenantService.getDomain(user_id)));
            }

            DureeDataExtractor dureeDataExtractor = new DureeDataExtractor();
            dureeDataExtractor.setAuditService(auditService);
            dureeDataExtractor.setNodeService(nodeService);
            dureeDataExtractor.setCurrentTime(millitime);
            long duree = (Long) dureeDataExtractor.extractData(nodeRef);

            /* Building a value map that mimics the value map generated by the Audit System*/
            Map<String, Serializable> values = new HashMap<String, Serializable>();
            // Kinda brutal but how can we do when the file was archived ... ?
            String statut = StatusMetier.STATUS_ARCHIVE;

            values.put(AuditParapheurService.AUDIT_INSTRUIT_NODEREF, nodeRef);
            values.put(AuditParapheurService.AUDIT_INSTRUIT_PARAPHEUR, parapheurRef);
            values.put(AuditParapheurService.AUDIT_INSTRUIT_TYPEMETIER, type);
            values.put(AuditParapheurService.AUDIT_INSTRUIT_STATUSMETIER, statut);
            values.put(AuditParapheurService.AUDIT_INSTRUIT_SOUSTYPEMETIER, subType);
            values.put(AuditParapheurService.AUDIT_INSTRUIT_DUREE, duree);


            this.auditDAO.createAuditEntry(application.getId(), millitime, this.authenticationComponent.getCurrentUserName(), values);
        } catch (Exception e) {

        }
        this.authenticationComponent.setSystemUserAsCurrentUser();
    }

    /**
     * Converts the audit facts from the old audit system into the newly defined audit application.
     *
     * @throws Throwable
     */
    private void importAuditTrail() throws Exception {
        Connection connection = getDatabaseConnection();
        Statement st = connection.createStatement();

        String query = this.getQueryForAuditTrail();
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read alf_audit_fact ...");
        }
        ResultSet rs;
        try {
            rs = st.executeQuery(query);
        } catch (SQLException e) {
            logger.info("Previous audit (< 3.4.c) doesn't seem to be present.");
            return;
        }

        String applicationName = "ParapheurServiceCompat";

        if (logger.isDebugEnabled()) {
            logger.debug("Inserting audit entries with database data ...");
        }

        int count = handleAuditTrailResultSet(applicationName, rs);

        st.close();

        logger.info("" + count + " audit entries processed.");

    }

    /**
     * Converts the audit facts from the old audit system into the newly defined audit application.
     *
     * @throws Throwable
     */
    private void importAuditStats() throws Exception {
        Connection connection = getDatabaseConnection();
        Statement st = connection.createStatement();

        String query = this.getQueryForAuditStats();
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read alf_audit_fact ...");
        }
        ResultSet rs;
        try {
            rs = st.executeQuery(query);
        } catch (SQLException e) {
            logger.info("Previous audit (< 3.4.c) doesn't seem to be present.");
            return;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Inserting audit entries with database data ...");
        }

        System.out.println("rs size = " + rs);
        int count = handleAuditStatsResultSet(rs);

        st.close();

        logger.info("" + count + " audit entries processed.");

    }

    private void doTheWork() throws Exception {
        UserTransaction utx = transactionService.getUserTransaction();
        try {
            utx.begin();
            System.out.println("Working...");
            importAuditStats();
            importAuditTrail();
            //throw new RuntimeException("DebugMode");
            utx.commit();
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
                logger.error("Unexpected error during rollback", ex);
            }
            throw new RuntimeException("Patch \"AuditMigrationPatch\" failed", e);
        }


    }

    /*
    protected void executeInternal() throws Throwable {
    doTheWork();

    }
     */
    @Override
    protected String applyInternal() throws Exception {
        doTheWork();

        return "Patch successfully applied.";
    }
}
