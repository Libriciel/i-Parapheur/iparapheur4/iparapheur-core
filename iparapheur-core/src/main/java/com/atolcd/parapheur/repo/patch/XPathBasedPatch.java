/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Initial Developpment by AtolCD, maintained by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.patch;

import java.util.List;
import javax.transaction.UserTransaction;
import org.alfresco.repo.module.AbstractModuleComponent;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;

/**
 *
 * @author Vivien Barousse
 */
public abstract class XPathBasedPatch extends AbstractModuleComponent {

    private NodeService nodeService;

    private SearchService searchService;

    private NamespaceService namespaceService;

    private TransactionService transactionService;

    private String store;

    @Override
    protected final void executeInternal() throws Throwable {
        System.out.println("XPathBasedPatch - Initialisation");
        UserTransaction tx = transactionService.getUserTransaction();
        try {
            tx.begin();

            List<NodeRef> nodesRef = searchService.selectNodes(getRootNode(),
                    getXPathQuery(),
                    null,
                    namespaceService,
                    false);

            for (NodeRef nodeRef : nodesRef) {
                patch(nodeRef);
            }

            tx.commit();
            afterCommit();
        } catch (Exception ex) {
            tx.rollback();
            throw new Exception("Unknown exception during patch", ex);
        }
    }

    protected abstract String getXPathQuery();

    protected abstract void patch(NodeRef nodeToPatch) throws Exception;

    protected void afterCommit() {
        System.out.println("XPathBasedPatch - Patch " + getName() + " appliqué avec succès");
    }

    protected NodeRef getRootNode() {
        return nodeService.getRootNode(new StoreRef(store));
    }

    protected NamespaceService getNamespaceService() {
        return namespaceService;
    }

    protected NodeService getNodeService() {
        return nodeService;
    }

    protected SearchService getSearchService() {
        return searchService;
    }

    protected TransactionService getTransactionService() {
        return transactionService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

}
