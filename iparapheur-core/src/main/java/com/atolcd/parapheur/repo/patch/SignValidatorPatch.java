package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import org.junit.Assert;
import org.adullact.spring_ws.iparapheur._1.GetHistoDossierResponse;
import org.adullact.spring_ws.iparapheur._1.LogDossier;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.audit.AuditQueryParameters;
import org.alfresco.service.cmr.audit.AuditService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.PersonService;
import org.apache.log4j.Logger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.Serializable;
import java.util.*;
import org.adullact.iparapheur.status.StatusMetier;

/**
 * SignValidatorPatch
 *
 * @author manz
 *         Date: 01/03/12
 *         Time: 14:28
 */
public class SignValidatorPatch extends XPathBasedPatch {

    private ParapheurService parapheurService;
    private PersonService personService;
    private TenantService tenantService;
    private AuditService auditService;
    private static Logger logger = Logger.getLogger(SignValidatorPatch.class);

    private String PERSON_FROMNAME_REQUEST = "+TYPE:\"cm:person\" AND @cm\\:firstName:\"{0}\" AND @cm\\:lastName:\"{1}\"";

    class SignatureDossierAuditEntry {
        public String username;
        public long timestamp;
        public NodeRef dossierRef;

        public SignatureDossierAuditEntry(String username, long timestamp, NodeRef dossierRef) {
            this.username = username;
            this.timestamp = timestamp;
            this.dossierRef = dossierRef;
        }
    }

    class ParapheurServiceCompatCallback implements AuditService.AuditQueryCallback {

        private List<SignatureDossierAuditEntry> signatures;
        private int recordsRead;

        public ParapheurServiceCompatCallback() {
            signatures = null;
            recordsRead = 0;
        }

        public int getRecordsReadCount() {
            return this.recordsRead;
        }

        public List<SignatureDossierAuditEntry> getSignatures() {
            return signatures;
        }

        public void setSignatures(List<SignatureDossierAuditEntry> signatures) {
            this.signatures = signatures;
        }

        @Override
        public boolean valuesRequired() {
            return true;
        }

        @Override
        public boolean handleAuditEntry(Long entryId, String applicationName, String user, long time, Map<String, Serializable> values) {
            if (applicationName.equals("ParapheurServiceCompat")) {
                // LogDossier log = new LogDossier();
                /*
                if (logger.isDebugEnabled()) {
                    logger.debug("HandlingAuditEntry(" + entryId + ", " + applicationName + ", " + user + ", " + time + ", " + values);
                    logger.debug(time + " # " + user + " # " + values.get("message"));
                }
                */

                this.recordsRead++;

                String message = (String) (values.get("message"));
                String status = "";
                List<Object> list = (List<Object>) values.get("list");

                status = (String) list.get(0);

                /* FIXME: A priori n'est plus necessaire car list.get(0) renvoie bien le statut sans corchets.*/
                if (status.startsWith("[")) {  // virer les crochets de début et fin (bug sur audit alfresco?)
                    //   logger.debug("nettoyage sur '[' et ']'");
                    status = status.substring(1, status.length() - 1);
                }

                /* que ce soit une signature ou un rejetSignature */
                if (StatusMetier.STATUS_SIGNE.equals(status) || StatusMetier.STATUS_REJET_SIGNATAIRE.equals(status)) {
                    signatures.add(new SignatureDossierAuditEntry(user, time, (NodeRef) (values.get("dossier"))));
                }

                // Sur l'element emission du dossier on reset la liste des signatures
                if (message.equals("Reprise du dossier")) {
                    this.signatures.removeAll(this.signatures);
                }
            }

            return true;
        }

        @Override
        public boolean handleAuditEntryError(Long entryId, String errorMsg, Throwable error) {
            //FIXME: Gérer les erreurs
            return true;
        }
    }

    private List<SignatureDossierAuditEntry> getSignatairesFromAuditTrail(NodeRef dossierRef) {
        final List<SignatureDossierAuditEntry> fSignatures = new ArrayList<SignatureDossierAuditEntry>();

        // nous ne spécifions pas de limite nous voulons tout les enregistrements concernant le dossier
        // audit accessible que par un administrateur.
        final AuditService fAuditService = auditService;
        final NodeRef fDossierRef = dossierRef;
        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Void>() {
            @Override
            public Void doWork() throws Exception {
                AuditQueryParameters queryParameters = new AuditQueryParameters();
                queryParameters.setApplicationName("ParapheurServiceCompat");
                queryParameters.addSearchKey("dossier", fDossierRef);

                ParapheurServiceCompatCallback callback = new ParapheurServiceCompatCallback();
                callback.setSignatures(fSignatures);
                try {
                    fAuditService.auditQuery(callback, queryParameters, 0);
                } catch (Exception e) {
                    //logger.error("Une erreur s'est produite lors de la recuperation de la trace d'audit.");
                    logger.error("Une erreur s'est produite lors de la recuperation de la trace d'audit.");
                    //e.printStackTrace();
                }
                if (callback.getRecordsReadCount() == 0) {
                    logger.error("Error: This node has no audit trail" + fDossierRef);
                }
                return null;
            }
        }, AuthenticationUtil.getAdminUserName());

        return fSignatures;
    }


    private String getUsernameFromFirstAndLastName(String firstName, String lastName) {
        SearchParameters parameters = new SearchParameters();
        parameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        parameters.setQuery(String.format("+TYPE:\"cm:person\" AND @cm\\:firstName:\"%s\" AND @cm\\:lastName:\"%s\"",
                firstName,
                lastName));
        parameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);

        ResultSet results = getSearchService().query(parameters);
        List<NodeRef> refs = results.getNodeRefs();
        results.close();
        if (refs.size() == 1) {
            NodeRef person = refs.get(0);
            String username = (String) getNodeService().getProperty(person, ContentModel.PROP_USERNAME);
            return username;
        }
        return null;
    }

    private String join(String[] tab) {
        String retval = "";
        for (int i = 0; i < tab.length; i++) {
            if (i > 0) {
                retval += " ";
            }

            retval += tab[i];
        }

        return retval;
    }

    private String[] slice(String[] tab, int from, int to) {
        int length = to - from;

        String[] mySlice = new String[length];
        int j = 0;
        for (int i = from; i < to; i++) {
            mySlice[j] = tab[i];
            j++;
        }

        return mySlice;
    }

    private String getUsernameFromFullName(String fullname) {
        String[] items = fullname.split(" ");
        String retval = null;

        for (int i = 1; i < items.length; i++) {
            String[] s1 = slice(items, 0, i);
            String[] s2 = slice(items, i, items.length);

            String firstName = join(s1);
            String lastName = join(s2);

            String validator = getUsernameFromFirstAndLastName(firstName, lastName);

            if (validator != null) {
                retval = validator;
                break;
            }
        }

        return retval;

    }

    private String getUserFullname(String username) {
        NodeRef proprietaireRef = personService.getPerson(username);
        String firstName = (String) getNodeService().getProperty(proprietaireRef, ContentModel.PROP_FIRSTNAME);
        String lastName = (String) getNodeService().getProperty(proprietaireRef, ContentModel.PROP_LASTNAME);
        return firstName + (lastName != null ? (" " + lastName) : "");
    }


    @Override
    protected String getXPathQuery() {
        //return "//*[subtypeOf('ph:etape-circuit')]/*";
        return "//*[subtypeOf('ph:dossier')]";
    }

    @Override
    protected void patch(NodeRef nodeToPatch) throws Exception {
        List<EtapeCircuit> circuit = parapheurService.getCircuit(nodeToPatch);
        int signataire = 0;
        /*
        if (AuthenticationUtil.getAdminUserName().endsWith("cdg59")) {
            System.out.println("CDG59");
        }
        */
        List<SignatureDossierAuditEntry> signatures = getSignatairesFromAuditTrail(nodeToPatch);

        for (EtapeCircuit etape : circuit) {
            if (etape.isApproved()) {
                if (etape.getValidator() == null) {
                    if (etape.getActionDemandee().equals(EtapeCircuitImpl.ETAPE_SIGNATURE)) {
                        String statut = (String) getNodeService().getProperty(nodeToPatch, ParapheurModel.PROP_STATUS_METIER);

                        if (!statut.startsWith("Rejet")) {
                            if (signatures.size() > signataire) {
                                String signataireStr = etape.getSignataire();
                                SignatureDossierAuditEntry signatureEntry = signatures.get(signataire);
                                assert (nodeToPatch.equals(signatureEntry.dossierRef));
                                /*
                                String fetchedSig = getUserFullname(signatureEntry.username);
                                System.out.println(fetchedSig + "==" + signataireStr);
                                */
                                getNodeService().setProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_VALIDATOR, signatureEntry.username);
                            } else {
                                logger.error(String.format("Erreur: signatures ne contient pas la signature (%s)", nodeToPatch));
                            }
                            signataire++;
                        } else {
                            if (signatures.size() > signataire) {
                                SignatureDossierAuditEntry signatureEntry = signatures.get(signataire);
                                getNodeService().setProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_VALIDATOR, signatureEntry.username);
                                logger.info("Dossier Rejeté par " + etape.getSignataire() + " " + signatureEntry.username);
                                //FIXME: par souci de complétude mais si il y a une suite a cette trace d'audit c'est *mal*
                                signataire++;
                            } else {
                                logger.warn(String.format("Signatures ne contient pas le rejet", nodeToPatch));
                            }


                        }


                        /*
                        List<String> owners = parapheurService.getParapheurOwners(nodeToPatch);
                        String signataireStr = etape.getSignataire();

                        String validator = null;

                        for (String owner : owners) {
                            if (signataireStr.equals(getUserFullname(owner))) {
                                validator = owner;
                            }
                        }

                        if (validator == null) {
                            validator = getUsernameFromFullName(signataireStr);
                            if (validator == null) {
                                // admettre la défaite l'utilisateur n'existe plus

                                System.out.println("L'utilisateur n'existe probablement plus ...");
                                continue;
                            }
                        }
                        NodeRef etapeRef = ((EtapeCircuitImpl) etape).getNodeRef();
                        System.out.println("Signataire " + signataireStr + " validator:" + validator);
                        if (!signataireStr.equals(getUserFullname(validator))) {
                            System.out.println("ça ne correspond pas !");
                        }
                        */
                        //TODO: let the patch do da Job
                        //nodeService.setProperty(etapeRef, ParapheurModel.PROP_VALIDATOR, validator);
                    }
                }
            } else {
                // do not explore circuit when we hit a non approved step
                break;
            }
        }


    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    public void setAuditService(AuditService auditService) {
        this.auditService = auditService;
    }
}

