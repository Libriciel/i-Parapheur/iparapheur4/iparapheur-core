package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CustomMetadataDef;
import com.atolcd.parapheur.repo.CustomMetadataType;
import com.atolcd.parapheur.repo.MetadataService;
import org.alfresco.repo.module.AbstractModuleComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.AlfrescoTransactionSupport;
import org.alfresco.repo.policy.TransactionBehaviourQueue;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.transaction.TransactionService;

import javax.transaction.Transaction;
import javax.transaction.UserTransaction;

public class PriorityMetadataPatch extends AbstractModuleComponent {
    private MetadataService metadataService;
    private TransactionService transactionService;


    @Override
    protected void executeInternal() throws Throwable {
        AuthenticationUtil.RunAsWork<Void> work = new AuthenticationUtil.RunAsWork<Void>() {
            @Override
            public Void doWork() throws Exception {
                UserTransaction tx = transactionService.getNonPropagatingUserTransaction();
                try {

                    tx.begin();

                    CustomMetadataDef priority = new CustomMetadataDef(ParapheurModel.METADATA_PRIORITY, "Priorité d'un dossier", CustomMetadataType.STRING, false);
                    metadataService.addMetadataDef(priority);

                    tx.commit();
                } catch (Exception e) {
                    tx.rollback();
                    e.printStackTrace();
                }

                return null;
            }
        };


        AuthenticationUtil.runAs(work, AuthenticationUtil.getAdminUserName());
    }

    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }
}
