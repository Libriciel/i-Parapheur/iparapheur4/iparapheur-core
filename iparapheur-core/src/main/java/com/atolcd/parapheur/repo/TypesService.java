/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.repo;

import org.adullact.iparapheur.rules.bean.CustomProperty;
import org.alfresco.service.cmr.repository.NodeRef;
import org.dom4j.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

/**
 * Interface d'interrogation de la typologie de dossiers
 *
 * @author Vivien Barousse
 */
public interface TypesService {

    String TDT_PROTOCOLE_HELIOS = "HELIOS";
    String TDT_PROTOCOLE_ACTES = "ACTES";

    String SOUS_TYPE_KEY_TDT_AUTO = "tdtAuto";
    String SOUS_TYPE_KEY_CACHET_AUTO = "cachetAuto";
    String SOUS_TYPE_KEY_ATTEST = "attest";
    String SOUS_TYPE_KEY_CIRCUIT = "Circuit";
    String SOUS_TYPE_KEY_ACTEURS_VARIABLES = "parapheursVariables";
    String SOUS_TYPE_KEY_INCLUDE_ATTACHMENTS = "includeAttachments";
    String SOUS_TYPE_KEY_DIGITAL_SIGNATURE_MANDATORY = "digitalSignatureMandatory";
    String SOUS_TYPE_KEY_CIRCUIT_HIERARCHIQUE_VISIBLE = "circuitHierarchiqueVisible";
    String SOUS_TYPE_KEY_READING_MANDATORY = "readingMandatory";
    String SOUS_TYPE_KEY_FORBID_SAME_SIG = "forbidSameSig";
    String SOUS_TYPE_KEY_MULTI_DOCUMENT = "multiDocument";
    String SOUS_TYPE_KEY_CACHET_ID = "cachetId";
    String SOUS_TYPE_KEY_PASTELL_MAILSEC_ID = "pastellMailsecId";

    String TYPE_KEY_PROTOCOLE = "Protocole";
    String TYPE_KEY_TDT = "TdT";
    String TYPE_KEY_SIGNATURE_FORMAT = "sigFormat";
    String TYPE_KEY_SIGNATURE_LOCATION = "sigLocation";
    String TYPE_KEY_SIGNATURE_POSTALCODE = "sigPostalCode";

    /**
     * Indique si la signature électronique est obligatoire pour le couple 
     * (typeName, subTypeName) passé en paramètre, ou si l'utilisateur a la 
     * possibilité de signer papier
     * 
     * @param typeName
     * @param subtypeName
     * @return Vrai si la signature electronique est obligatoire, faux sinon
     */
    public abstract boolean isDigitalSignatureMandatory(String typeName, String subtypeName);

    boolean isMultiDocument(String typeName, String subtypeName);

    NodeRef getCertificate(String typeName, String subtypeName);

    String getPastellServerId(String typeName, String subtypeName);

    /**
     * Indique si la lecture du dossier est obligatoire avant signature, pour le
     * couple typeName / subTypeName passé en parametre
     * 
     * @param typeName
     * @param subtypeName
     * @return Vrai si lecture obligatoire avant signature, faux sinon
     */
    public abstract boolean isReadingMandatory(String typeName, String subtypeName);

    boolean isForbidSameSig(String typeName, String subtypeName);

    public abstract boolean isCircuitHierarchiqueVisible(String typeName, String subtypeName);
    
    /**
     * Indique si les pièces jointes sont incluses dans l'impression/archivage par défaut.
     * 
     * @param typeName
     * @param subtypeName
     * @return Vrai si les pièces jointes sont incluses, faux sinon.
     */
    public abstract boolean areAttachmentsIncluded(String typeName, String subtypeName);

    /**
     * Récupère la liste des calques d'impression associés à un couple type
     * et sous-type passés en paramètres.
     */
    public abstract List<NodeRef> getListeCalques(String type, String subtype);

    /**
     * Récupère la liste des calques d'impression d'annexes associés à un couple type
     * et sous-type passés en paramètres.
     */
    public abstract List<NodeRef> getListeCalquesAnnexes(String type, String subtype);

    /**
     * Récupère la liste des métadonnées obligatoires pour le couple type, soustype
     */
    
    public abstract List<String> getMandatoryMetadatas(String type, String subtype);

    /**
     * Récupère la liste des métadonnées modifiables pour le couple type, soustype
     */
    public abstract List<String> getEditableMetadatas(String type, String subtype);

    /**
     * Récupère les différents attributs pour chaque metadonnée positionnée dans le sous type
     */
    public Map<String, Map<String, String>> getMetadatasMap(String typeName, String subtypeName);
    /**
     * Récupère un circuit en fonction du type et du sous-type associé.
     *
     * Si le sous-type défini un script de choix de circuit, ce script est
     * executé afin de déterminer le circuit a utiliser.
     *
     * ATTENTION ! Aucune liste de propriétés personnalisées n'étant fournie,
     * aucun paramètre n'est fourni au script pour son execution.
     *
     * @param type type
     * @param subtype sous-type
     * @return le circuit associé au type/sous-type
     */
    public abstract NodeRef getWorkflow(String type, String subtype);

    boolean hasSelectionScript(String type, String subtype);

    NodeRef getWorkflowHandledError(NodeRef dossier, String type, String subtype, List<CustomProperty<? extends Object>> customProperties, boolean withError, boolean fromWs);

    NodeRef getWorkflowFromWs(NodeRef dossier, String type, String subtype, List<CustomProperty<? extends Object>> customProperties);

    /**
     * Retourne le circuit associé au type et aau sous-type passé en paramètre.
     *
     * Si le sous-type défini un script de choix de circuit, ce script est
     * executé afin de déterminer le circuit à utiliser.
     *
     * La liste des propriétés personnalisées fournie en paramètre est transmise
     * au script lors de son execution.
     * 
     * @param type type
     * @param subtype sous-type
     * @param customProperties propriétés personnalisées à fournir au script
     * @return le circuit associé au sous type
     */
    public abstract NodeRef getWorkflow(NodeRef dossier, String type, String subtype, List<CustomProperty<? extends Object>> customProperties);

    public List<Element> getTypesElements();

    Map<String, List<Element>> optimizedGetSavedTypeAsAdmin();

    /**
     * Indique si l'étape de tdt doit se faire automatiquement.
     * C'est le cas si et seulement si il y a une étape d'envoi au tdt sur le circuit, si le protocole est Helios
     * et si l'option d'envoi auto est à vrai dans la définition du sous-type.
     * @param type
     * @param sousType
     * @return vrai si une étape de tdt vers Helios est prévue sur le sous-type passé en paramètre.
     */
    boolean isTdtAuto(String type, String sousType);

    Map<String, Map<String, List<String>>> getUsedWorkflows();

    void setUsedWorkflows(NodeRef workflow, Map<String, List<String>> usedBy);

    /**
     * Permet la récupération de tous les éléments sousTypes de tous les types
     * @return la liste d'éléments
     */
    public Map<String, List<Element>> getSubtypesElements();

    /**
     * Récupération de la typologie en lecture / écriture
     * @param read true pour lecture / false pour écriture
     * @return Map de types avec liste de sous-types
     */
    Map<String,List<String>> optimizedGetSavedType(boolean read, NodeRef bureau);

    List<NodeRef> getListeCalquesMultiDoc(String typeDoss, String sstypeDoss, int doccount);

    boolean isCompatibleWithMultidoc(@NotNull String type);

    @Nullable String getProtocol(@NotNull String typeId);

    @Nullable String getSignatureFormat(@NotNull String typeId);

    boolean isCachetAuto(String type, String sousType);

    boolean hasToAttestSignature(String type, String sousType);

    void setValueForSousType(@NotNull String type, @NotNull String subtype, @NotNull String key, @Nullable String value);
}
