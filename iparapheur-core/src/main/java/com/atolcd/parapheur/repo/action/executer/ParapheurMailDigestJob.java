package com.atolcd.parapheur.repo.action.executer;
/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 * contact@atolcd.com
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

import com.atolcd.parapheur.repo.NotificationCenter;
import com.atolcd.parapheur.repo.ParapheurUserPreferences;
import com.atolcd.parapheur.repo.impl.DigestQuartzJobServiceImpl;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.transaction.TransactionService;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.transaction.UserTransaction;
import java.util.List;

/**
 * ParapheurMailDigestJob
 * This job is run daily (around 6am) and collects unread notifications for the previous day.
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 26/03/12
 *         Time: 10:19
 * @author jmaire
 */
public class ParapheurMailDigestJob implements Job {

    private PersonService personService;

    private ParapheurUserPreferences parapheurUserPreferences;

    private TransactionService transactionService;

    private DigestQuartzJobServiceImpl digestQuartzJobService;

    private NotificationCenter notificationCenter;

    private String runAsUser;

    private List<String> users;


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap map = context.getJobDetail().getJobDataMap();

        personService = (PersonService) map.get("personService");
        parapheurUserPreferences = (ParapheurUserPreferences) map.get("parapheurUserPreferences");
        transactionService = (TransactionService) map.get("transactionService");
        notificationCenter = (NotificationCenter) map.get("notificationCenter");

        runAsUser = (String) map.get("runAsUser");
        users = (List<String>) map.get("users");

        digestQuartzJobService = (DigestQuartzJobServiceImpl) map.get("digestQuartzJobService");


        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {

            @Override
            public Object doWork() throws Exception {
                UserTransaction utx = transactionService.getNonPropagatingUserTransaction();
                try {
                    utx.begin();

                    for (String username : users) {
                        if (parapheurUserPreferences.isDailyDigestEnabledForUsername(username)) {
                            // Les notifications sont marquées comme lues à la construction du mail.
                            digestQuartzJobService.sendDigestMailToUser(username);
                        }
                    }

                    utx.commit();
                } catch (Exception e) {

                    e.printStackTrace();
                    try {
                        utx.rollback();
                    } catch (Exception ex) {
                    }
                }
                return null;
            }

        }, runAsUser);

    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }
}