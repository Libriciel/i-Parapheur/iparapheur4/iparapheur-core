/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2014, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.repo;

import com.atolcd.parapheur.repo.impl.ParapheurServiceImpl;
import org.alfresco.service.Auditable;
import org.alfresco.service.NotAuditable;
import org.alfresco.service.PublicService;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.NoSuchPersonException;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.repository.MapNode;
import org.bouncycastle.cert.X509CertificateHolder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.cert.X509Certificate;
import java.util.*;

@PublicService
public interface ParapheurService {
    /**
     * Indique si un noeud est un ph:parapheur.
     *
     * @param nodeRef le noeud à tester
     * @return <code>true</code> si nodeRef est un ph:parapheur
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     */
    @NotAuditable
    public abstract boolean isParapheur(NodeRef nodeRef);

    /**
     * Indique si un noeud est une ph:corbeille.
     *
     * @param nodeRef le noeud à tester
     * @return <code>true</code> si nodeRef est une ph:corbeille
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     */
    @NotAuditable
    public abstract boolean isCorbeille(NodeRef nodeRef);

    /**
     * Indique si un noeud est une ph:corbeilleVirtuelle.
     *
     * @param nodeRef le noeud à tester
     * @return <code>true</code> si nodeRef est une ph:corbeilleVirtuelle
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     */
    @NotAuditable
    public abstract boolean isCorbeilleVirtuelle(NodeRef nodeRef);

    /**
     * Indique si un noeud est un ph:dossier.
     *
     * @param nodeRef le noeud à tester
     * @return <code>true</code> si nodeRef est un ph:dossier
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     */
    @NotAuditable
    public abstract boolean isDossier(NodeRef nodeRef);

    /**
     * Renvoie la liste de tous les parapheurs de l'application.
     *
     * @return la liste des parapheurs
     */
    @NotAuditable
    public abstract List<NodeRef> getParapheurs();
    
    /**
     * Renvoie la liste de tous les parapheurs de l'application, triés par
     * ordre alphabétique.
     *
     * @return la liste des parapheurs triés par ordre alphabétique
     */
    @NotAuditable
    public abstract List<NodeRef> getParapheursSorted();

    /**
     * trouve le dossier identifié par son nom dans le bureau identifié par son
     * noderef.
     * 
     * @param bureauNodeRef
     * @param dossierName
     * @return le nodeRef du dossier ou null
     */
    public abstract NodeRef getDossierFromBureauAndName(NodeRef bureauNodeRef, String dossierName);

    /**
     * Renvoie la liste de tous les parapheurs de l'application correspondant
     * au nom passe en parametre. Utilise une recherche FTS.
     *
     * @param nomParapheur
     * @return la liste des parapheurs
     */
    @NotAuditable
    public abstract List<NodeRef> getParapheursFromName(String nomParapheur);
    
    /**
     * Ajoute un propriétaire à un bureau
     * @param bureau
     * @param user 
     */
    @NotAuditable
    public void addOwnerToBureau(NodeRef bureau, NodeRef user);
    
    /**
     * Retire un utilisateur de la liste des propriétaires d'un bureau
     * @param bureau
     * @param user 
     */
    @NotAuditable
    public void removeOwnerFromBureau(NodeRef bureau, NodeRef user);

    /**
     * Renvoie la liste des parapheurs contenant la chaine de caractères passés
     * en paramètre. Utilise une recherche Lucène.
     * @param search
     * @return 
     */
    @NotAuditable
    public List<NodeRef> searchParapheur(String search);
    
    /**
     * Renvoie la liste des bureaux contenant la chaine de caractères passés
     * en paramètre parmi les bureaux associés. Utilise une recherche Lucène.
     * @param bureauCourant le bureau concerné pour la liste des associés
     * @param search
     * @return 
     */
    @NotAuditable
    public List<NodeRef> searchParapheurInAssociateList(NodeRef bureauCourant, String search);
    
    /**
     * Renvoie la liste de tous les parapheurs de l'application gérés par 
     * l'utilisateur passé en paramètre (y compris parapheurs subordonnés)
     *
     * @param userName  nom d'utilisateur "Admin fonctionnel"
     * @return la liste des parapheurs
     */
    @NotAuditable
    public abstract List<NodeRef> getAllManagedParapheursByOpAdmin(String userName);

    /**
     * Renvoie la liste des parapheurs de l'application déclarés comme étant
     * gérés par l'utilisateur passé en paramètre, hors parapheurs subordonnés
     *
     * @param userName  nom d'utilisateur "Admin fonctionnel"
     * @return la liste des parapheurs
     */
    @NotAuditable
    public abstract List<NodeRef> getRootManagedParapheursByOpAdmin(String userName);

    /**
     * Renvoie la liste de corbeilles d'un parapheur.
     *
     * @param parapheur le parapheur
     * @return les NodeRef des corbeilles du parapheur
     * @throws IllegalArgumentException si <code>parapheur</code> n'est pas un parapheur
     */
    @NotAuditable
    public abstract List<NodeRef> getCorbeilles(NodeRef parapheur);

    /**
     * Renvoie la liste des dossiers d'une corbeille.
     *
     * @param corbeille la corbeille
     * @return les NodeRef des dossiers de la corbeille
     * @throws IllegalArgumentException si <code>corbeille</code> n'est pas une corbeille
     */
    @NotAuditable
    public abstract List<NodeRef> getDossiers(NodeRef corbeille);

    /**
     * Methode historique pour obtenir la liste des documents d'un dossier.
     * Obsolete depuis le decoupage en doc principal + annexes.
     * Utiliser getMainDocuments et getAttachments
     * 
     * @param dossier
     * @return la liste de tous les documents d'un dossier.
     */
    @NotAuditable
    @Deprecated
    public abstract @Nullable List<NodeRef> getDocuments(NodeRef dossier);

    /**
     * Renvoie la liste des documents principaux du dossier passé en paramètre.
     * @param dossier
     * @return la liste des documents principaux du dossier.
     */
    @NotAuditable
    public abstract @NotNull List<NodeRef> getMainDocuments(NodeRef dossier);
    
    /**
     * Renvoie la liste des annexes d'un dossier (donc hors documents
     * principaux) du dossier passé en paramètre.
     * @param dossier
     * @return la liste des annexes (pièces jointes) d'un dossier.
     */
    @NotAuditable
    public abstract @Nullable List<NodeRef> getAttachments(NodeRef dossier);

    /**
     * Recherche une corbeille spécifique dans un parapheur.
     * <p/>
     * Le nom de la corbeille est en fait le {@link QName}
     * de la {@link org.alfresco.service.cmr.repository.ChildAssociationRef}.
     *
     * @param parapheur le parapheur contenant la corbeille.
     * @param name      le nom de la corbeille.
     * @return le NodeRef de la corbeille, ou <code>null</code> si elle n'a pas été trouvée.
     * @throws IllegalArgumentException si parapheur n'est pas un ph:parapheur.
     * @throws IllegalArgumentException si name est null
     */
    @NotAuditable
    public abstract NodeRef getCorbeille(NodeRef parapheur, QName name);

    /**
     * Parcourt les parents d'un noeud et renvoie le premier ph:dossier trouvé.
     *
     * @param nodeRef le noeud à partir duquel chercher
     * @return le NodeRef du ph:dossier ou <code>null</code> s'il n'a pas été trouvé.
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     */
    @NotAuditable
    public abstract NodeRef getParentDossier(NodeRef nodeRef);

    /**
     * Parcourt les parents d'un noeud et renvoie la première corbeille trouvée.
     *
     * @param nodeRef le noeud à partir duquel chercher
     * @return le NodeRef de la corbeille ou <code>null</code> si elle n'a pas été trouvée.
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     */
    @NotAuditable
    public abstract NodeRef getParentCorbeille(NodeRef nodeRef);

    /**
     * Parcourt les parents d'un noeud et renvoie le premier parapheur trouvé.
     *
     * @param nodeRef le noeud à partir duquel chercher
     * @return le NodeRef du parapheur ou <code>null</code> s'il n'a pas été trouvé.
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     */
    @NotAuditable
    public abstract NodeRef getParentParapheur(NodeRef nodeRef);

    /**
     * Renvoie l'autorité propriétaire d'un parapheur, c'est-à-dire un
     * nom d'utilisateur (userName) ou de groupe.
     *
     * @param parapheur le parapheur
     * @return le nom d'autorité du propriétaire, ou <code>null</code> si le noeud n'est pas <em>dans</em> un parapheur.
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     * @deprecated on le garde pour la compatiblité approeBeforev3
     */
    @NotAuditable
    @Deprecated
    public abstract String getParapheurOwner(NodeRef parapheur);

    /**
     * Renvoie la ou les autorité(s) proprietaire(s) d'une parapheur,
     * c'est à dire une liste de nom(s) d'utilisateur(s)
     * @param parapheur le parapheur
     * @return la liste des noms des autorités propriétaires
     * @throws IllegalArgumentException si le nodeRef est null ou n'existe pas dans le repository
     */
    @NotAuditable
    public abstract List<String> getParapheurOwners(NodeRef parapheur);

    /**
     * Indique si un utilisateur est propriétaire d'un parapheur, y compris
     * s'il fait partie du groupe propriétaire d'un parapheur partagé.
     *
     * @param nodeRef  un parapheur ou un noeud se trouvant dans un parapheur
     * @param userName le userName de l'utilisateur
     * @return <code>true</code> si l'utilisateur est propriétaire du parapheur.
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     * @throws IllegalArgumentException si userName est null ou vide
     */
    @NotAuditable
    public abstract boolean isParapheurOwner(NodeRef nodeRef, String userName);
    
    /**
     * Indique si un utilisateur est propriétaire ou suppléant (dans le cadre
     * d'une délégation active) d'un parapheur, y compris s'il fait partie
     * du groupe propriétaire d'un parapheur partagé.
     *
     * @param nodeRef  un parapheur ou un noeud se trouvant dans un parapheur
     * @param userName le userName de l'utilisateur
     * @return <code>true</code> si l'utilisateur est propriétaire ou délégué du parapheur.
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     * @throws IllegalArgumentException si userName est null ou vide
     */
    @NotAuditable
    public abstract boolean isParapheurOwnerOrDelegate(NodeRef nodeRef, String userName);
    
    /**
     * Indique si un utilisateur est suppléant (dans le cadre
     * d'une délégation active) d'un parapheur.
     *
     * @param nodeRef  un parapheur ou un noeud se trouvant dans un parapheur
     * @param userName le userName de l'utilisateur
     * @return <code>true</code> si l'utilisateur est délégué du parapheur.
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     * @throws IllegalArgumentException si userName est null ou vide
     */
    @NotAuditable
    public abstract boolean isParapheurDelegate(NodeRef nodeRef, String userName);
    
    /**
     * Indique si le NodeRef courant est suppléant (dans le cadre
     * d'une délégation active) d'un parapheur.
     *
     * @param titulaire  le parapheur titulaire
     * @param suppleant le parapheur suppléant
     * @return <code>true</code> si le parapheur courant est délégué du parapheur.
     * @throws IllegalArgumentException si parapheur ou courant sont null ou n'existent pas dans le repository
     */
    @NotAuditable
    public abstract boolean isParapheurDelegate(NodeRef titulaire, NodeRef suppleant);

    /**
     * Indique si le NodeRef parapheur est titulaire ou suppléant du dossier.
     *
     * @param parapheur  le parapheur
     * @param dossier le dossier
     * @return <code>true</code> si le parapheur courant est titulaire ou suppléant du dossier.
     * @throws IllegalArgumentException si parapheur ou dossier sont null ou n'existent pas dans le repository
     */
    @NotAuditable
    public abstract boolean isOwnerOrDelegateOfDossier(NodeRef parapheur, NodeRef dossier);

    List<NodeRef> getOwnedBySecretariatParapheurs(String userName);

    /**
     * Renvoie le parapheur dont l'utilisateur est propriétaire.
     *
     * @param userName le nom d'utilisateur
     * @return le NodeRef du parapheur, ou <code>null</code> s'il n'a pas été trouvé.
     * @throws IllegalArgumentException si userName est null ou vide
     */

   @NotAuditable
   @Deprecated
   public abstract NodeRef getOwnedParapheur(String userName);

    /**
     * Gets the first Parapheur owned by the user bearing userName.
     * @param userName the user name
     * @return the Noderef of the Parapheur
     */
    @NotAuditable
    public abstract NodeRef getUniqueParapheurForUser(String userName);


    /**
     * Renvoie les parapheurs dont l'utilisateur est propriétaire.
     *
     * @param userName le nom d'utilisateur
     * @return liste de NodeRef des parapheur, ou <code>null</code> s'il n'a pas été trouvé.
     * @throws IllegalArgumentException si userName est null ou vide
     */
    @NotAuditable
    public List<NodeRef> getOwnedParapheurs(String userName);

    /**
     * EN OPTION POUR PLUS TARD
     * <p/>
     * Renvoie les parapheurs dont le propriétaire est un groupe
     * duquel l'utilisateur fait partie.
     *
     * @param userName le nom de l'utilisateur
     * @return la liste des NodeRef des parapheurs
     * @throws IllegalArgumentException si userName est null ou vide
     */
    @NotAuditable
    public abstract List<NodeRef> getSharedParapheurs(String userName);

    /**
     * Indique si un utilisateur est secrétaire d'un parapheur, y compris
     * s'il fait partie du groupe secrétaire d'un parapheur à secrétariat partagé.
     *
     * @param nodeRef  un parapheur ou un noeud se trouvant dans un parapheur
     * @param userName le userName de l'utilisateur
     * @return <code>true</code> si l'utilisateur est secrétaire du parapheur.
     * @throws IllegalArgumentException si nodeRef est null ou n'existe pas dans le repository
     * @throws IllegalArgumentException si userName est null ou vide
     */
    @NotAuditable
    public abstract boolean isParapheurSecretaire(NodeRef nodeRef, String userName);

    /**
     * Indique si l'utilisateur est déclaré comme administrateur
     * 
     * @param userName le userName de l'utilisateur (login)
     * @return <code>true</code> si l'utilisateur est administrateur.
     * @deprecated use {@link com.atolcd.parapheur.repo.admin.UsersService#isAdministrateur}
     */
    @NotAuditable
    @Deprecated
    public abstract boolean isAdministrateur(String userName);

    /**
     * Indique si l'utilisateur est déclaré comme administrateur fonctionnel
     * 
     * @param userName le userName de l'utilisateur (login)
     * @return <code>true</code> si l'utilisateur est admin fonctionnel.
     * @deprecated use {@link com.atolcd.parapheur.repo.admin.UsersService#isAdministrateurFonctionnel(String)}
     */
    @NotAuditable
    @Deprecated
    public abstract boolean isAdministrateurFonctionnel(String userName);
    
    /**
     * Indique si l'utilisateur a la charge du parapheur passé en paramètre
     * en tant qu'administrateur fonctionnel
     * 
     * @param userName le userName de l'utilisateur (login)
     * @param parapheur le parapheur à tester
     * @return <code>true</code> si l'utilisateur a la charge du parapheur en
     * tant qu'administrateur fonctionnel.
     */
    @NotAuditable
    public abstract boolean isAdministrateurFonctionnelOf(String userName, NodeRef parapheur);

    /**
     * Renvoie les parapheurs pour lequel l'utilisateur est secrétaire.
     *
     * @param userName le nom de l'utilisateur
     * @return la liste des NodeRef des parapheurs
     * @throws IllegalArgumentException si userName est null ou vide
     */
    @NotAuditable
    public abstract List<NodeRef> getSecretariatParapheurs(String userName);

    /**
     * Renvoie la taille max autorisée par fichier, en kilo-octet
     * 
     * @return Taille max en kilo-octect, formaté texte
     */
    @NotAuditable
    public abstract String getUploadFileSizeLimit();

    /**
     * Renvoie le parapheur (rôle) de l'émetteur d'un dossier.
     *
     * @param dossier le dossier
     * @return le NodeRef du parapheur émetteur
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract NodeRef getEmetteur(NodeRef dossier);

    /**
     * Renvoie la date limite d'un dossier.
     *
     * @param dossier le dossier
     * @return la date limite, si positionnée, null sinon
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract Date getDateLimite(NodeRef dossier);

    /**
     * Renvoie une mapNode d'un noeud.
     *
     * @param nodeRef le noeud (dossier, document)
     * @return la mapNode
     */
    @NotAuditable
    public abstract MapNode getMapNode(NodeRef nodeRef);

    /**
     * Indique si le dossier a été émis.
     *
     * @param dossier le dossier
     * @return <code>true</code> si le dossier a été émis
     * @deprecated use {@link com.atolcd.parapheur.repo.DossierService#isEmis(org.alfresco.service.cmr.repository.NodeRef)}
     */
    @Deprecated
    @NotAuditable
    public abstract boolean isEmis(NodeRef dossier);

    /**
     * Indique si le dossier a fini son circuit (prêt à archiver, ou retourné).
     *
     * @param dossier le dossier
     * @return <code>true</code> si le dossier est terminé.
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract boolean isTermine(NodeRef dossier);

    /**
     * Indique si le dossier a été retourné par un des acteurs du circuit.
     *
     * @param dossier le dossier
     * @return <code>true</code> si le dossier est rejeté.
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract boolean isRejete(NodeRef dossier);

    /**
     * Renvoie la liste des étapes du circuit de validation d'un dossier.
     *
     * @param dossier le dossier
     * @return la liste des NodeRef des étapes du circuit
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract List<EtapeCircuit> getCircuit(NodeRef dossier);

    /**
     * Renvoie la premiere étape du circuit de validation d'un dossier.
     *
     * @param dossier le dossier
     * @return le NodeRef de la 1ere étape du circuit
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract EtapeCircuit getFirstEtape(NodeRef dossier);

    /**
     * Renvoie la liste des "parapheurs" membres de la liste de diffusion
     * d'un dossier, toutes étapes confondues.
     *
     * @param dossier le dossier
     * @return la liste des NodeRef des parapheurs membres de la liste
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    @Deprecated
    public abstract Set<NodeRef> getDiffusionToutesEtapes(NodeRef dossier);

    /**
     * Enlève le parapheurRef des listes de Diffusion du circuit associé au dossier,
     * toutes étapes confondues.
     *
     * @param dossier
     * @param parapheurRef
     */
    @NotAuditable
    public abstract void removeFromDiffusionToutesEtapes(NodeRef dossier, NodeRef parapheurRef);

    /**
     * Renvoie l'autorité (utilisateur ou groupe) acteur courant d'un dossier.
     *
     * @param dossier le dossier
     * @return le nom de l'autorité acteur courant
     * @throws IllegalArgumentException si le dossier n'est pas un dossier
     */
    @Deprecated
    @NotAuditable
    public abstract String getActeurCourant(NodeRef dossier);

    
    /**
     * BL Bugfix
     * La méthode getActeurCourant est dépréciée et utilise d'anciennes propriétés du parapheur qui peuvent ne plus être à jour (propriétaire).
     * 
     * Cette méthode est utilisée uniquement dans l'appel du TDTPROXY BL pour assurer la compatibilité.
     * @param dossier
     * @return 
     */
    public String getActeurCourantBLSRCI(NodeRef dossier);
    
    /**
     * Renvoie les autorités (utilisateur(s) ou groupe) acteur courant d'un dossier.
     *
     * @param dossier le dossier
     * @return la liste des autorités
     * @throws IllegalArgumentException si le dossier n'est pas un dossier
     */
    @NotAuditable
    public List<String> getActeursCourant(NodeRef dossier);

    /**
     * Indique si l'utilisateur est acteur courant du dossier. Cad s'il est propriétaire ou délégué du
     * bureau de l'étape courante.
     *
     * @param dossier  le dossier à tester
     * @param userName le nom d'utilisateur
     * @return <code>true</code> si l'utilisateur est acteur courant du dossier
     * @throws IllegalArgumentException si <code>dossier</code> n'est pas un dossier
     * @throws IllegalArgumentException si <code>userName</code> est <code>null</code> ou vide
     */
    @NotAuditable
    public abstract boolean isActeurCourant(NodeRef dossier, String userName);

    /**
     * Renvoie l'annotation publique d'un dossier pour l'étape en cours.
     *
     * @param dossier le dossier
     * @return l'annotation publique pour l'étape en cours
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract String getAnnotationPublique(NodeRef dossier);

    /**
     * Remplace l'annotation publique d'un dossier pour l'étape en cours.
     *
     * @param dossier    le dossier
     * @param annotation la nouvelle annotation publique
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract void setAnnotationPublique(NodeRef dossier, String annotation);

    /**
     * Renvoie l'annotation privée courante d'un dossier.
     *
     * @param dossier le dossier
     * @return l'annotation privée
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier
     */
    @NotAuditable
    public abstract String getAnnotationPrivee(NodeRef dossier);

    /**
     * Renvoie l'annotation privée précédente d'un dossier.
     *
     * @param dossier le dossier
     * @return l'annotation privée de l'étape précédente
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier
     */
    @NotAuditable
    public abstract String getAnnotationPriveePrecedente(NodeRef dossier);

    /**
     * Remplace l'annotation courante privée d'un dossier
     *
     * @param dossier    le dossier
     * @param annotation la nouvelle annotation privée
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract void setAnnotationPrivee(NodeRef dossier, String annotation);

    /**
     * Approuve l'étape en cours d'un dossier et le fait passer à l'étape suivante.
     *
     * @param dossier le dossier
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     * @throws RuntimeException         si l'étape suivante du circuit de validation n'existe pas
     * @throws RuntimeException         si une boucle de déléation a été détectée
     */
    @NotAuditable
    public abstract void approve(NodeRef dossier);
    
    /**
     * Approuve l'étape en cours d'un dossier et le fait passer à l'étape suivante.
     *
     * @param dossier le dossier
     * @param parapheurCourant le parapheur courant de l'utilisateur
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     * @throws RuntimeException         si l'étape suivante du circuit de validation n'existe pas
     * @throws RuntimeException         si une boucle de déléation a été détectée
     */
    @Auditable(parameters = {"dossier", "parapheurCourant"})
    public abstract void approveV4(NodeRef dossier, NodeRef parapheurCourant);

    @NotAuditable
    public abstract NodeRef getCurrentParapheur();

    /**
     * Provoque un enregistrement dans l'histogramme du dossier
     * sur le changement d'état de transmission.
     *
     * @param dossier le dossier
     * @param status  le statut: EnCoursTransmission | TransmissionKO (| TransmissionOK)
     * @param message le message de précision
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    public abstract void auditTransmissionTDT(NodeRef dossier, String status, String message);

    @NotAuditable
    public abstract void auditWithNewBackend(String applicationName, String message, NodeRef dossier, List<Object> list);

    /**
     * Provoque un enregistrement dans l'histogramme du dossier et l'envoi d'un
     * e-mail au createur sur une action réalisée par un administrateur
     *
     * @param dossier le dossier
     * @param action  l'action réalisée (TRANSFERER | EFFACER)
     * @param message le message de précision
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    public abstract void auditAndMailAboutDossier(NodeRef dossier, String action, String message) throws IllegalArgumentException;

    /**
     * Rejette le dossier (le fait passer à l'émetteur comme dossier retourné)
     *
     * @param dossier le dossier
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @Auditable(parameters = {"dossier"})
    public abstract void reject(NodeRef dossier);

    /**
     * Reprend le dossier (copie le dossier vers les dossiers à transmettre).
     * Cette operation se fait sur un dossier qui a ete rejete au prealable.
     *
     * @param dossier le dossier
     * @return la référence du dossier copié
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    public abstract NodeRef reprendreDossier(NodeRef dossier);

    void handleDeleteDossier(NodeRef dossier, NodeRef parapheur, boolean notify, String username);

    File produceZipSignatures(String docFileName, NodeRef dossier) throws IOException;

    /**
     * Transfère un dossier au secrétariat ou le ramène dans sa corbeille d'origine selon son état
     *
     * @param dossier le dossier
     * @return la référence du dossier copié
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    public abstract void secretariat(NodeRef dossier);

    /**
     * Récupère le dossier (le circuit repasse à l'étape précédente et le dossier est déplacé vers le parapheur précédent)
     *
     * @param dossier le dossier
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    public abstract void recupererDossier(NodeRef dossier);

    ParapheurServiceImpl.TdtState getTdtState(NodeRef dossier, boolean updateDossier);

    /**
     * Archive le dossier
     *
     * @param dossier le dossier
     * @param nom     le nom que l'on souhaite donner à l'archive
     * @return l'URL d'archive
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    public abstract String archiver(NodeRef dossier, String nom) throws Exception;
    
    /**
     * Archive le dossier avec les pièces jointes passées en paramètre. (si
     * documents est <code>null</code>, toutes les pièces jointes sont archivées.
     *
     * @param dossier   le dossier
     * @param nom       le nom que l'on souhaite donner à l'archive
     * @param documents les pièces jointes à archiver.
     * @return l'URL d'archive
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    public abstract String archiver(NodeRef dossier, String nom, List<NodeRef> documents) throws Exception;

    /**
     * Génère le bordereau de signature (première page du PDF d'impression) du dossier passé en paramètre
     * @param dossier le dossier à imprimer
     * @param statutTdt retour du Tdt s'il y en a un
     * @param ackDate date de retour du Tdt s'il y en a eu un
     * @return Un fichier PDF représentant le bordereau de signature
     */
    File genererPageSignatairesPDF(NodeRef dossier, String statutTdt, String ackDate) throws Exception;

    /**
     * Génère un PDF (d'impression) du dossier en parametre.
     * <p/>
     * Le fichier PDF contient une conversion PDF de tous les documents, ainsi
     * qu'une page de garde (en preface) resumant toutes les etapes de 
     * validation subies par le dossier si includeFirstPage est vrai.
     *
     * @param dossier le nodeRef du dossier
     * @param updateDossier indique s'il faut mettre a jour son statut
     * @param includeAnnexes indique s'il faut inclure les annexes dans le fichier
     * @param includeFirstPage indique si la page de garde doit ajoutée.
     * 
     * @return le fichier format PDF
     */
    @NotAuditable
    public abstract File genererDossierPDF(NodeRef dossier, boolean updateDossier, boolean includeAnnexes, boolean includeFirstPage) throws Exception;
    
    /**
     * Génère un PDF (d'impression) du dossier en parametre.
     * <p/>
     * Le fichier PDF contient une conversion PDF de tous les documents
     * principaux et des annexes passées en paramètres, ainsi qu'une page de
     * garde (en preface) resumant toutes les etapes de validation subies par le
     * dossier si includeFirstPage est vrai.
     *
     * @param dossier le nodeRef du dossier
     * @param updateDossier indique s'il faut mettre a jour son statut
     * @param attachments les annexes à inclure dans le pdf
     * @param includeFirstPage indique si la page de garde doit être ajoutée.
     * 
     * @return le fichier format PDF
     */
    @NotAuditable
    public abstract File genererDossierPDF(NodeRef dossier, boolean updateDossier, List<NodeRef> attachments, boolean includeFirstPage) throws Exception;

//    /**
//    * Retourne une map contenant les circuits enregistrés dans le système
//    *
//    * @return les couples (nom, référence) des circuits enregistrés, jamais <code>null</code>
//    * @deprecated  utiliser workflowService
//    */
//    @Deprecated
//    @NotAuditable
//    public abstract Map<String, NodeRef> getSavedWorkflows();

    /**
     * Retourne une map contenant les circuits "privés" enregistrés dans le système
     *
     * @return les couples (nom, référence) des circuits enregistrés, jamais <code>null</code>
     */
    @NotAuditable
    public abstract Map<String, NodeRef> getOwnedWorkflows();

    /**
     * Retourne une map contenant les circuits publics enregistrés dans le système
     *
     * @return les couples (nom, référence) des circuits enregistrés, jamais <code>null</code>
     */
    @NotAuditable
    public abstract Map<String, NodeRef> getPublicWorkflows();

    /**
     * Retourne une map contenant tous les circuits enregistrés dans le système
     *
     * @return les couples (nom, référence) des circuits enregistrés, jamais <code>null</code>
     */
    @NotAuditable
    public abstract Map<String, NodeRef> getAllWorkflows();

    /**
     * Retourne le circuit hiérarchique sous forme de liste d'étapes
     *
     * @param emetteur (le parapheur émetteur)
     * @return la liste d'étapes du circuit
     */
    @NotAuditable
    public abstract List<EtapeCircuit> getCircuitHierarchique(NodeRef emetteur);

    /**
     * Retourne la liste de validation et l'ensemble de diffusion d'un circuit prédéfini passé en paramètre
     * <p/>
     * This method is used to read saved workflows up to version 3.0.
     * <p/>
     * To read more recent workflows, please see WorkflowService
     *
     * @param workflow la référence du circuit prédéfini
     * @return la définition du circuit prédéfini
     * @see com.atolcd.parapheur.repo.ParapheurService#loadSavedWorkflow(org.alfresco.service.cmr.repository.NodeRef)
     * @see com.atolcd.parapheur.repo.WorkflowService#getSavedWorkflow(org.alfresco.service.cmr.repository.NodeRef)
     * @deprecated Since 3.1
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    @NotAuditable
    public abstract SavedWorkflow loadSavedWorkflow(NodeRef workflow);

    /**
     * Crée un noeud dans Data dictionary/Circuits et enregistre le circuit passé en paramètre
     * Si le circuit existait déjà, il est remplacé
     *
     * @param name           le nom du circuit à enregistrer
     * @param circuit        la liste des parapheurs composant le circuit de validation
     * @param diffusion      l'ensemble des parapheurs de la liste de notification
     * @param publicWorkflow indique si le workflow doit être accessible par les autres utilisateurs
     * @return la référence du noeud créé
     * @throws IllegalArgumentException si <code>name</code> ne contient pas une chaîne de caractères valide
     */
    @NotAuditable
    public abstract NodeRef saveWorkflow(String name, List<NodeRef> circuit, Set<NodeRef> diffusion, boolean publicWorkflow);

    /**
     * Crée un noeud dans Data dictionary/Circuits et enregistre le circuit passé en paramètre
     * Si le circuit existait déjà, il est remplacé
     * Cette nouvelle signature pour gérer les évolutions iParapheur V3
     *
     * @param name          le nom du circuit à enregistrer
     * @param circuit       la liste des étapes composant le circuit de validation
     * @param acl           l'ensemble des parapheurs individuels ayant accès au circuit
     * @param groupes       l'ensemble des groupes ayant accès au circuit
     * @param publicCircuit indique si le circuit est accessible à TOUS
     * @return la référence du noeud créé
     * @throws IllegalArgumentException si <code>name</code> ne contient pas une chaîne de caractères valide
     */
    @NotAuditable
    public abstract NodeRef saveWorkflow(String name, List<EtapeCircuit> circuit, Set<NodeRef> acl, Set<String> groupes, boolean publicCircuit);

    /**
     * Indique si le userName a accès à la gestion des circuits de validation
     * (  = membre du groupe GESTIONNAIRE_CIRCUITS_IPARAPHEUR )
     *
     * @param userName
     * @return <code>true</code> si userName est membre du groupe.
     * @deprecated use {@link com.atolcd.parapheur.repo.admin.UsersService#isGestionnaireCircuit(String)}
     */
    @NotAuditable
    @Deprecated
    public abstract boolean isGestionnaireCircuits(String userName);

    /**
     * Indique si le dossier peut être récupéré (droit de remords).
     *
     * @param dossier le dossier
     * @return <code>true</code> si le dossier est récupérable.
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract boolean isRecuperable(NodeRef dossier);

    /**
     * Retourne le parapheur responsable dans la hiérarchie
     *
     * @param parapheur le parapheur
     * @return la référence du parapheur responsable, ou null si le parapheur passé en paramètre n'a pas de responsable
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un parapheur.
     */
    @NotAuditable
    public abstract NodeRef getParapheurResponsable(NodeRef parapheur);

    @NotAuditable
    public abstract void changeSignatureToSignaturePapier(NodeRef dossier);

    /**
     * Définit le signataire d'un dossier pour l'étape en cours.
     *
     * @param dossier    le dossier
     * @param signataire le nom complet du signataire
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    @NotAuditable
    public abstract void setSignataire(NodeRef dossier, String signataire);

    /**
     * Définit le signataire d'un dossier pour l'étape en cours.
     *
     * @param dossier    le dossier
     * @param signataire le nom complet du signataire
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    public abstract void setSignataire(NodeRef dossier, String signataire, X509Certificate[] cert);

    /**
     * Définit le signataire d'un dossier pour l'étape en cours.
     *
     * @param dossier    le dossier
     * @param signataire le nom complet du signataire
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier.
     */
    public abstract void setSignataire(NodeRef dossier, String signataire, X509CertificateHolder[] cert);

    /**
     * Retourne le titre d'un parapheur.
     *
     * @param parapheur le parapheur
     * @return le titre du parapheur (propriété ContentModel.PROP_TITLE)
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur.
     */
    @NotAuditable
    public abstract String getNomParapheur(NodeRef parapheur);

    /**
     * Retourne l'id d'un parapheur.
     *
     * @param parapheur le parapheur
     * @return l'id du parapheur (propriété ContentModel.PROP_NAME)
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur.
     */
    @NotAuditable
    public abstract String getParapheurName(NodeRef parapheur);

    /**
     * Retourne le nom du propriétaire d'un parapheur.
     *
     * @param parapheur le parapheur
     * @return nom le nom de son propriétaire
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur.
     * @deprecated n'a plus de sens lorsque le parapheur possède plusieurs proprietaires
     */
    @NotAuditable
    @Deprecated
    public abstract String getNomProprietaire(NodeRef parapheur);

    /**
     * Retourne les noms des differents proprietaires d'un parapheur.
     * @param parapheur le parapheur
     * @return la liste des noms des proprietaires
     */
    @NotAuditable
    public List<String> getNomProprietaires(NodeRef parapheur);

    /**
     * Retourne le libellé prenom + nom à partir du nom de login.
     *
     * @param userName l'identifiant de connexion
     * @return nom le nom correspondant
     * @throws IllegalArgumentException si <code>username</code> ne représente pas un login.
     */
    @NotAuditable
    public abstract String getPrenomNomFromLogin(String userName);

    /**
     * Crée un nouveau parapheur
     * Le nom du parapheur est récupéré de la propriété <code>cm:name</code> passée en argument.
     *
     * @param properties les propriétés du parapheur
     * @return le NodeRef du parapheur qui vient d'être créé
     * @throws IllegalArgumentException si <code>properties</code> ne contient pas de propriété <code>cm:name</code>.
     */
    @NotAuditable
    public abstract NodeRef createParapheur(Map<QName, Serializable> properties) throws FileExistsException;
    
    
    /**
     * Permet de programmer une délégation, si le créneau de programmation est
     * en cours, cette fonction créera la délégation et ne la programmera pas
     * (mis à part la date de fin si il y en a une).
     * Un parapheur est programmé si la propriété PROP_DELEGATION_PROGRAMMEE
     * contient un parapheur et si la date de début se situe plus tard dans le
     * temps. La date de fin doit être supérieure à la date de début ou égale
     * à 0 (cas d'une délégation infinie où il faut enlever la délégation à la
     * main).
     * Si deleguerPresents est vrai, la délégation prendra en compte les
     * dossiers déjà présents dans la corbeille "A traiter".
     *
     * @param parapheur
     * @param parapheurDelegue
     * @param debut
     * @param fin
     * @param deleguerPresents
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur.
     * @throws IllegalArgumentException si <code>parapheurDelegue</code> ne représente pas un parapheur et n'est pas null.
     * @throws RuntimeException         si l'opération conduit à une boucle de délégation.
     */
    public abstract void programmerDelegation(NodeRef parapheur, NodeRef parapheurDelegue, Date debut, Date fin, boolean deleguerPresents);
    
    /**
     * Permet de supprimer une délégation active ou programmée.
     * @param parapheur 
     */
    public abstract void supprimerDelegation(NodeRef parapheur);

    /**
     * Vérifie si une délégation programmée (resp active) est devenue active
     * (resp inactive), si tel est le cas, la délégation programmée (resp
     * active) devient la delegation active (resp inactive).
     * Une délégation est active (resp inactive) lorsque la date à laquelle
     * cette fonction est appelée est comprise (resp pas comprise) dans le
     * créneau défini par les propriétés PROP_DATE_DEBUT_DELEGATION et
     * PROP_DATE_FIN_DELEGATION.
     * @param parapheur 
     */
    public abstract void updateDelegation(NodeRef parapheur);
            
    /**
     * Retourne le parapheur de délégation d'un parapheur donné, ou null si il n'en a pas.
     *
     * @param parapheur
     * @return le NodeRef du parapheur délégué.
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur.
     */
    @NotAuditable
    public abstract NodeRef getDelegation(NodeRef parapheur);

    /**
     * Retourne le parapheur de délégation programmée d'un parapheur donné,
     * ou null si il n'en a pas.
     *
     * @param parapheur
     * @return le NodeRef du parapheur délégué programmé.
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur.
     */
    @NotAuditable
    public abstract NodeRef getDelegationProgrammee(NodeRef parapheur);
    
    /**
     * Retourne la dernière delegation ayant été mise en place pour le parapheur donné.
     * <p/>
     * Si aucune délégation n'a jamais été mise en place pour ce parapheur, <code>null</code> est renvoyé.
     *
     * @param parapheur le parapheur dont on souhaite récupérer
     * @return la dernière délégation pour ce parapheur
     */
    @NotAuditable
    public abstract NodeRef getOldDelegation(NodeRef parapheur);

    /**
     * Renvoie la liste des bureaux se trouvant sur le chemin de délégation du
     * parapheur passé en paramètre (ce dernier étant exclus).
     *
     * @param parapheur
     * @return la liste des bureaux se trouvant sur le chemin des délégations
     */
    @NotAuditable
    public abstract List<NodeRef> getParapheursOnDelegationPath(NodeRef parapheur);
    
    /**
     * Renvoie la liste des propriétaires des bureaux se trouvant sur le chemin
     * de délégation du parapheur passé en paramètre (ce dernier étant exclus).
     *
     * @param parapheur
     * @return la liste des propriétaires des bureaux se trouvant sur le chemin
     * de délégation.
     */
    @NotAuditable
    public abstract List<String> getUsersOnDelegationPath(NodeRef parapheur);
    
    /**
     * Renvoie vrai si l'utilisateur passé en paramètre est délégué du dossier,
     * faux sinon. C'est-à-dire si c'est le propriétaire du parapheur délégué
     * du parapheur contenant le dossier passé en paramètre ET si le parapheur
     * délégué contient ce dossier dans sa corbeille virtuelle
     * "dossier délégués".
     *
     * @param dossier
     * @param userName
     * @return vrai si l'utilisateur est délégué du dossier, faux sinon
     */
    @NotAuditable
    public abstract boolean isDelegate(NodeRef dossier, String userName);
    
    /**
     * Renvoie vrai si le parapheur passé en paramètre délègue ses dossiers à
     * traiter, avant la mise en place de la délégation, faux sinon.
     *
     * @param parapheur
     * @return vrai si le parapheur délègue les dossiers à traiter avant la mise
     * en place de la délégation.
     */
    @NotAuditable
    public abstract boolean arePresentsDelegues(NodeRef parapheur);
    
    /**
     * Renvoi la liste des bureaux pour lesquels le parapheur passé en paramètre
     * est un suppléant direct (la chaîne de délégation n'est pas prise en compte).
     * @param parapheur
     * @return la liste des bureaux titulaires directs du parapaheur.
     */
    @NotAuditable
    public abstract List<NodeRef> getTitulaires(NodeRef parapheur);
    
    
    /**
     * Renvoi la liste des bureaux pour lesquels le parapheur passé en paramètre
     * est un suppléant (la chaîne de délégation est prise en compte).
     * @param parapheur
     * @return la liste des bureaux titulaires du parapaheur.
     */
    @NotAuditable
    public abstract List<NodeRef> getTitulairesOnDelegationPath(NodeRef parapheur);

    /**
     * Associer l'utilisateur aux Autorités définies par
     *                      ROLE_PHADMIN_<paraph-uuid>
     * @param username le nom d'utilisateur concerné
     * @param prfList liste des UUIDs de noderefs des parapheurs gérés par l'utilisateur
     * @return 
     */
    @NotAuditable
    public String setPHAdminAuthoritiesForUser(String username, List<String> prfList);

    /**
     * Remplace le circuit de validation d'un dossier donné. L'émetteur peut éventuellement
     * être inclus comme premier élément de la liste.
     *
     * @param dossier le NodeRef du dossier concerné
     * @param circuit la liste des étapes composant le circuit de validation
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier non-émis
     * @throws IllegalArgumentException si le circuit contient des doublons
     */
    public abstract void setCircuit(NodeRef dossier, List<EtapeCircuit> circuit);

    @NotAuditable
    void appendCircuit(NodeRef dossier, List<EtapeCircuit> circuit);

    @NotAuditable
    void restartCircuit(NodeRef dossier, String type, String subtype, String workflow);

    /**
     * Remplace la liste de diffusion d'un dossier donné
     *
     * @param dossier le NodeRef du dossier concerné
     * @param diffusion l'ensemble des NodeRef des parapheurs composant la liste de diffusion
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier ou est null
     @Auditable(key = Auditable.Key.ARG_0 ,parameters = {"dossier", "diffusion"})
     public abstract void setDiffusion(NodeRef dossier, Set<NodeRef> diffusion);
     */

    /**
     * Retourne la liste des usernames composant le secrétariat du parapheur passé en paramètre
     *
     * @param parapheur le NodeRef du parapheur à évaluer
     * @return la liste des secrétaires de ce parapheur
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur ou est null
     */
    @NotAuditable
    public abstract List<String> getSecretariatParapheur(NodeRef parapheur);
    
    /**
     * Retourne la liste des parapheurs vers lesquels le parapheur passé en paramètre peut déléguer ses dossiers
     *
     * @param parapheur le NodeRef du parapheur à évaluer
     * @return la liste des délégations possibles de ce parapheur
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur ou est null
     */
    @NotAuditable
    public abstract List<NodeRef> getDelegationsPossibles(NodeRef parapheur);
    
    /**
     * Modifie la liste des parapheurs vers lesquels le parapheur passé en paramètre peut déléguer ses dossiers
     *
     * @param parapheur le NodeRef du parapheur à évaluer
     * @param delegationsPossibles la liste des délégations possibles
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur ou est null
     */
    @NotAuditable
    public abstract void setDelegationsPossibles(NodeRef parapheur, List<NodeRef> delegationsPossibles);

    /**
     * Ajoute une délégation à un bureau pour un autre bureau
     * 
     * @param bureauCourant
     * @param bureauCible 
     */
    @NotAuditable
    public void setDelegation(NodeRef bureauCourant, NodeRef bureauCible);
    
    /**
     * Renvoie la date à partir de laquelle les délégations seront prises en
     * compte, si il n'y a pas de délégation (en cours ou programmée), cette
     * fonction renvoie la date de la dernière programmation ou null si aucune
     * délégation n'a jamais été programmée.
     *
     * @param parapheur le NodeRef du parapheur à évaluer
     * @return la date de début de délégation si une délégation est programmée
     * ou active.
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur ou est null
     */
    @NotAuditable
    public abstract Date getDateDebutDelegation(NodeRef parapheur);
    
    /**
     * Renvoie la date à partir de laquelle la délégation sera supprimée, si il
     * n'y a pas de délégation (en cours ou programmée) ou si la délégation est
     * sans fin (suppression manuelle), cette fonction renvoie null.
     *
     * @param parapheur le NodeRef du parapheur à évaluer
     * @return la date de fin de délégation ou null si il n'y en a pas.
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur ou est null
     */
    @NotAuditable
    public abstract Date getDateFinDelegation(NodeRef parapheur);
    
    /**
     * Renvoie la valeur de thème graphique 'adullact'/'blex'
     *
     * @return la valeur en fichier de configuration (parapheur.habillage)
     */
    @NotAuditable
    public String getHabillage();

    /**
     * Renvoie la valeur de propriete parapheur.ws.getdossier.pdf.enabled
     * 
     * @return true s'il faut le PDF d'impression dans le WS getDossier (annexe)
     */
    @NotAuditable
    public abstract boolean isParapheurWsGetdossierPdfEnabled();

    /**
     * Renvoie la valeur de propriete parapheur.ws.getdossier.pdf.docName
     * 
     * @return true s'il faut le PDF d'impression dans le WS getDossier (annexe)
     */
    @NotAuditable
    public abstract String getParapheurWsGetdossierPdfDocName();

    @NotAuditable
    public boolean isPreviewEnabled();

    @NotAuditable
    public void generatePreviewForNodeRef(NodeRef document, boolean duringReload);

    @NotAuditable
    public void updateProperties(NodeRef dossier, Map<QName, Serializable> metadataValues);

    /**
     * Modifie l'étape courante du dossier passé en paramètre et le déplace vers un nouveau parapheur
     *
     * @param dossier   le dossier à modifier
     * @param parapheur le parpaheur de destination
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier non-terminé
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur
     */
    public abstract void moveDossier(NodeRef dossier, NodeRef parapheur);

    @NotAuditable
    public abstract Properties getPadesSignatureProperties(NodeRef dossier);

    /**
     * Retourne le NodeRef du parapheur dont le nom est passé en paramètre ou null si il n'existe pas
     *
     * @param parapheurName le nom du parapheur à chercher
     * @return la référence du parapheur ou null
     * @throws IllegalArgumentException si <code>parapheurName</code> n'est pas une chaîne de caractères valide
     */
    @NotAuditable
    public abstract NodeRef parapheurFromName(String parapheurName);

    @NotAuditable
    public abstract String[] getSignatures(NodeRef dossier);

    @NotAuditable
    public abstract String[] getSignatures(EtapeCircuit etape);

    /**
     * Stocke la signature de la pièce principale dans une propriété d:text d'Alfresco
     *
     * @param dossier     le dossier à modifier
     * @param etape       l'etape concernee par le stockage...
     * @param signedBytes la valeur de la signature sous forme d'un tableau d'octets
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier
     */
    @NotAuditable
    public abstract void setSignature(NodeRef dossier, EtapeCircuit etape, byte[] signedBytes, String sigFormat);

    /**
     * Stocke la signature de la pièce principale dans une propriété d:text d'Alfresco
     *
     * @param dossier     le dossier à modifier
     * @param signedBytes la valeur de la signature sous forme d'un tableau d'octets
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier
     */
    public abstract void setSignature(NodeRef dossier, byte[] signedBytes, String sigFormat);

    /**
     * Retourne la signature de la pièce principale d'un dossier si elle existe, null sinon
     *
     * @param dossier le dossier concerné
     * @return la signature sous forme d'un tableau d'octets
     * @throws IllegalArgumentException si <code>dossier</code> ne représente pas un dossier
     */
    @NotAuditable
    public abstract byte[] getSignature(NodeRef dossier);

    /**
     * Retourne la signature déposée lors d'une étape d'un dossier si elle existe, null sinon
     *
     * @param etape l'étape
     * @return la signature sous forme d'un tableau d'octets
     */
    @NotAuditable
    public abstract byte[] getSignature(EtapeCircuit etape);

    /**
     * Retourne l'URL de téléchargement de l'applet utilisée pour la signature électronique
     *
     * @return l'url sous forme de chaîne de caractères
     */
    @NotAuditable
    public abstract String getSignAppletURL();

    /**
     * Retourne le chemin XPATH du bloc à signer
     *
     * @param dossier
     * @return le chemin XPATH
     */
    @NotAuditable
    public abstract String getXPathSignature(NodeRef dossier);

    /**
     * Retourne les propriétés nécessaires pour signature électronique XAdES
     *
     * @return un objet Properties
     */
    @NotAuditable
    public abstract Properties getXadesSignatureProperties(NodeRef dossier);

    /**
     * Retourne la configuration du service (principalement des childnames)
     *
     * @return un objet Properties contenant la configuration
     */
    @NotAuditable
    public abstract Properties getConfiguration();

    /**
     * crée un dossier PES_Retour (HELIOS) depuis une requete de TdT
     *
     * @param id
     * @param xmlIS
     * @return true si Ok, false sinon
     */
    @NotAuditable
    public abstract NodeRef createDossierPesRetour(String id, String parapheurName, InputStream xmlIS, Date date);

    @NotAuditable
    public abstract EtapeCircuit getCurrentEtapeCircuit(NodeRef dossier);

    @NotAuditable
    public abstract EtapeCircuit getCurrentEtapeCircuit(List<EtapeCircuit> circuit);

    @NotAuditable
    public abstract EtapeCircuit getPreviousEtapeCircuit(NodeRef dossier);
    @NotAuditable
    public abstract EtapeCircuit getPreviousEtapeCircuit(List<EtapeCircuit> circuit);

    /**
     * Interface de gestion des meta-donnees sur les dossiers,
     * en particulier le typage métier: type technique + sous-type
     *
     * Les Meta-Données sont portées par le dossier. Il peut y en avoir plusieurs:
     *  - Types: stocké au format XML dans le dictionnaire des données
     *  - Sous-types: Un fichier XML par TypeTechnique référent.
     *
     * les autres MetaDonnées pourraient être des tags (catégories) Alfresco
     */

    /**
     * Interroge iParapheur pour obtenir la liste des Types techniques
     *
     * @return flux XML des types techniques
     */
    @NotAuditable
    public abstract InputStream getSavedXMLTypes();

    NodeRef getSavedXLMTypesNode();

    /**
     * Renvoie la liste des types techniques existant dans le parapheur.
     * NB : ne retourne que les types utiles (i.e: qui ont des sous-types)
     * @return la liste des types techniques.
     */
    @NotAuditable
    public List<String> getSavedTypes(NodeRef parapheur);

    @NotAuditable
    public List<String> getSavedTypes(String username, NodeRef parapheur);
    
    @NotAuditable
    public List<String> getSavedTypes();

    /**
     * BLEX
     * Acces a la liste de TOUS les types metiers , que ceux-ci aient des
     * sous-types ou non
     *
     * @return les noms des types metier
     */
    @NotAuditable
    public List<String> getSavedTypesFull();


    /**
     * Interroge iParapheur pour obtenir la liste des sous-Types techniques
     *
     * @return flux XML des sous-types techniques
     */
    @NotAuditable
    public abstract InputStream getSavedXMLSousTypes(String typeID);

    @NotAuditable
    public List<NodeRef> getAllSavedXMLSousTypes();

    /**
     * Renvoie la liste des sous types techniques existant dans le parapheur.
     *
     * @return la liste des sous types techniques.
     */
    @NotAuditable
    public List<String> getSavedSousTypes(String type, NodeRef parapheur);


    @NotAuditable
    public List<String> getSavedSousTypes(String type, String username, NodeRef parapheur);
    
    @NotAuditable
    public List<String> getSavedSousTypes(String type);

    /**
     * Fournit les caractéristiques du Type technique donné en paramètre
     *
     * @param typeID : le Type technique iParapheur
     * @return Description, infos TdT
     */
    @NotAuditable
    public abstract Map<QName, Serializable> getTypeMetierProperties(String typeID);

    /**
     * à un couple (type, sous-type) correspond un circuit de validation.
     * Cette méthode retourne le nodeRef de ce circuit sauvegardé.
     *
     * @param typeID
     * @param sousTypeID
     * @return le nodeRef du circuit de validation correspondant
     * @deprecated pour v3.1. Utiliser TypesService.getWorkflow
     */
    @NotAuditable
    @Deprecated
    public abstract NodeRef getCircuitRef(String typeID, String sousTypeID);

    @NotAuditable
    public abstract NodeRef findUserByEmail(String from) throws NoSuchPersonException;

    @NotAuditable
    public abstract String findEmailForUsername(String username);

    @NotAuditable
    public abstract String getValidAddressForUser(String userName);

    @NotAuditable
    public abstract NodeRef rechercheDossier(String nomDossier);

    /**
     * Détection de doublon dans l'attribution de nom de dossier.
     *
     * @param nomDossier
     * @return
     */
    @NotAuditable
    public abstract boolean isNomDossierAlreadyExists(String nomDossier);

    @NotAuditable
    public abstract List<NodeRef> rechercheDossiers(String type, String soustype, String status);

    /**
     * Propriétés de signature XAdES selon le TDT et le type métier donné.
     * 
     * @param cert le certificat (ne sert pas a priori)
     * @param tdtName le nom de TDT (SRCI vs S2LOW vs FAST)
     * @param typeDossier le type de Dossier concerné
     * @return 
     */
    @NotAuditable
    public abstract Map<String, String> getSignaturePolicy(X509Certificate cert, String tdtName, String typeDossier);

    @NotAuditable
    public abstract void updateSignaturePolicyHash(NodeRef policy);

    @NotAuditable
    boolean checkSignature(EtapeCircuit etapeCircuit);

    @NotAuditable
    void markAsRead(NodeRef dossier);

    @NotAuditable
    boolean isRead(NodeRef dossier);

    @NotAuditable
    boolean isSigned(NodeRef dossier);
    /**
     * Indique si le dossier est à signature électronique obligatoire
     *
     * @param dossier
     * @return Vrai si c'est le cas, faux si la signature papier est possible
     */
    @NotAuditable
    public abstract boolean isDigitalSignatureMandatory(NodeRef dossier);

    @NotAuditable
    public boolean isReadingMandatory(NodeRef dossier);

    boolean isForbidSameSig(NodeRef dossier);

    @NotAuditable
    public boolean areAttachmentsIncluded(NodeRef dossier);

    @NotAuditable
    public NodeRef getParapheursHome();

    @NotAuditable
    public abstract List<NodeRef> getChildParapheurs(NodeRef ref);

    /**
     * Extrait informations de signature(s) utiles à l'affichage
     *
     * @param etape
     * @return liste des informations affichables
     */
    @NotAuditable
    public abstract List<Map<String, String>> getSignatureProperties(EtapeCircuit etape);

    /**
     * Gets a list of folders that are locked in batch mode for further recovery.
     *
     * @return a list of nodeRef wearing ParapheurModel.PROP_IS_IN_BATCH.
     */
    @NotAuditable
    public List<NodeRef> getBatchLockedNodes();

    @NotAuditable
    public String getCurrentValidator(NodeRef dossier);

    @NotAuditable
    public void setCurrentValidator(NodeRef dossier, String validator);

    @NotAuditable
    public void changeSignataire(NodeRef dossier, NodeRef bureauRef, String annotPub, String annotPrivee);

    @NotAuditable
    public void avisComplementaire(NodeRef dossier, NodeRef bureauRef, String annotPub, String annotPrivee);
    
    @NotAuditable
    public void ajouterActeursExternes(NodeRef dossier, List<NodeRef> acteurs);
    
    /**
     * Renvoie la liste des bureaux présents dans la liste de notification du
     * dossier passé en paramètre à l'étape courante.
     * @param dossier
     * @return la liste des bureaux présents dans la liste de notification du
     * dossier à l'étape courante
     */
    @NotAuditable
    public List<NodeRef> getListeNotification(NodeRef dossier);
    
    /**
     * Renvoie la liste des bureaux présents dans la liste de notification
     * originale du dossier passé en paramètre à l'étape courante.
     * C'est à dire la liste de notification du circuit défini par le type, le
     * sous-type et les méta-données du circuit du dossier (ne contient pas les
     * bureaux rajoutés dans la liste de notification en cours de circuit).
     * 
     * @param dossier
     * @return la liste des bureaux présents dans la liste de notification du
     * dossier à l'étape courante
     */
    @NotAuditable
    public Set<NodeRef> getListeNotificationOriginale(NodeRef dossier);
            
    /**
     * Retourne vrai si la corbeille virtuelle "A venir" est visible sur 
     * {@param parapheur}.
     * Nb : par défaut, la corbeille est visible.
     * @param parapheur
     * @return vrai si la corbeille virtuelle "A venir" est visible
     */
    @NotAuditable
    public boolean showAVenir(NodeRef parapheur);
    
    /**
     * Recherche les utilisateurs contenant search dans leur nom ou prénom.
     * 
     * @param search
     * @return les utilisateurs corresondant à search.
     */
    @NotAuditable
    public List<NodeRef> searchUser(String search);
    
    /**
     * Renvoi vrai si l'utilisateur passé en paramètre possède au moins un
     * bureau.
     * 
     * @param userName le nom de l'utilisateur à tester
     * @return vrai si l'utilisateur passé en paramètre possède au moins un
     * bureau.
     */
    @NotAuditable
    public boolean isOwner(String userName);
    
    /**
     * Renvoie la propriété targetVersion définie dans les fichiers de
     * configuration utilisée pour savoir vers quelle version pointer lors
     * d'un accès au parapheur depuis l'extérieur. Utile pour la redirection de
     * mail et la création de dossier depuis une application métier.
     * @return la version du parapheur ciblée.
     */
    @NotAuditable
    public String getTargetVersion();

    @NotAuditable
    public NodeRef getFirstChild(NodeRef nodeRef, QName childAssocType);

    @NotAuditable
    void updateOwnerPermission(NodeRef bureau, List<String> newOwners);

    @NotAuditable
    void updateSecretariatPermission(NodeRef bureau, List<String> newSecretariat);

    /**
     * Mise à jour du bureau passé en paramètre avec les propriétés présentes dans les HashMap
     * @param bureau NodeRef du bureau
     * @param propertiesMap Map de propriétés à mettre à jour
     * @param assocsMap Map d'associations à mettre à jour
     * @return true si réussite, false sinon
     */
    @NotAuditable
    public boolean updateBureau(NodeRef bureau, HashMap<QName, Serializable> propertiesMap, HashMap<QName, Serializable> assocsMap);

    /**
     * Retourne vrai si une boucle est créée en déléguant {@param parapheur}
     * à {@param parapheurDelegue}.
     * Cette fonction prend en compte la programmation des délégations.
     *
     * @param parapheur le parapheur dont on veut suivre l'arbre de délégation
     * @param parapheurDelegue
     * @param debut
     * @param fin
     * @return vrai si aucune boucle n'est détectée.
     */
    @NotAuditable
    public boolean willDelegationLoop(NodeRef parapheur, NodeRef parapheurDelegue, Date debut, Date fin);

    @NotAuditable
    public NodeRef insertEtapeAfter(NodeRef currentEtapeRef, EtapeCircuit newStep);

    @NotAuditable
    EtapeCircuit getCurrentOrRejectedEtapeCircuit(NodeRef dossierRef);

    @NotAuditable
    List<EtapeCircuit> getCircuit(NodeRef dossier, String type, String soustype);
}
