/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2012, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.repo;

import com.atolcd.parapheur.repo.annotations.Annotation;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;

import java.util.*;

/**
 * Classe définissant une étape de circuit de validation.
 * 
 * @author Vivien Barousse, Stephane Vast - Adullact Projet
 */
public interface EtapeCircuit
{
    /**
     * Designe une action demandee de VISA.
     */
    public static final String ETAPE_VISA = "VISA";
    /**
     * Designe une action demandee de Signature (electronique ou papier).
     */
    public static final String ETAPE_SIGNATURE = "SIGNATURE";
    /**
     * Designe une action demandee de Teletransmission (cf connecteurs TDT).
     */
    public static final String ETAPE_TDT = "TDT";
    /**
     * Designe une action demandee de Cachet Serveur
     */
    public static final String ETAPE_CACHET = "CACHET";
    /**
     * Designe une action demandee d'envoi du dossier via S2LOW-mailSecurise.
     */
    public static final String ETAPE_MAILSEC = "MAILSEC";
    /**
     * Designe une action demandee d'envoi du dossier via Pastell-mailSecurise.
     */
    public static final String ETAPE_MAILSEC_PASTELL = "MAILSECPASTELL";
    /**
     * Designe une action demandee de ...(je ne sais pas, c'est quoi??).
     */
    public static final String ETAPE_DIFF_EMAIL = "DIFF_EMAIL";
    /**
     * Designe une action demandee d'archivage, en fin de circuit.
     */
    public static final String ETAPE_ARCHIVAGE = "ARCHIVAGE";

    public static final String TRANSITION_PARAPHEUR = "PARAPHEUR";
    public static final String TRANSITION_CHEF_DE = "CHEF_DE";
    public static final String TRANSITION_EMETTEUR = "EMETTEUR";
    public static final String TRANSITION_VARIABLE = "VARIABLE";

    /**
     * Renvoie le parapheur associé à l'étape.
     * 
     * @return le NodeRef du parapheur associé à l'étape.
     */
    public abstract NodeRef getParapheur();
    
    /**
     * Renvoie le nom du parapheur (string) associé à l'étape.
     * 
     * @return chaine de caractère, ou null.
     */
    public abstract String getParapheurName();
    
    /**
     * Indique si l'étape a été approuvée ou non.
     * 
     * @return <code>true</code> si l'étape a été approuvée.
     */
    public abstract boolean isApproved();

    /**
     * Renvoie le type de transition d'une étape vers une autre.
     * 
     * Cela permet de choisir comme prochaine étape :
     * <ul>
     *     <li>Un parapheur particulier (PARAPHEUR)</li>
     *     <li>Le supperieur hiérarchique de l'étape précédente (CHEF_DE)</li>
     *     <li>L'emetteur du dossier (EMETTEUR)</li>
     * </ul>
     * 
     * @return transition la transition selectionnée
     */
    public abstract String getTransition();

    /**
     * Renvoie le type d'action prévue à cette étape
     *
     * @return l'action demandée: VISA, SIGNATURE, TDT, MAILSEC, DIFF_EMAIL, ARCHIVAGE
     */
    public abstract String getActionDemandee();

    /**
     * Renvoie le délégateur (noderef), en cas d'étape par délégation
     * 
     * @return le délégateur, null sinon.
     * @deprecated à partir de la v3.4, le délégateur est le parapheur où se
     * situe le dossier, il faut aller chercher le délégué.
     */
    @Deprecated
    public abstract NodeRef getDelegateur();

    /**
     * Renvoie le délégateur (string), en cas d'étape par délégation
     *
     * @return le délégateur, null sinon.
     * @deprecated à partir de la v3.4, le délégateur est le parapheur où se
     * situe le dossier, il faut aller chercher le délégué.
     */
    @Deprecated
    public abstract String getDelegateurName();
    
    /**
     * Renvoie le délégué (noderef), en cas d'étape par délégation
     * 
     * @return le délégué, null sinon.
     */
    public abstract NodeRef getDelegue();

    /**
     * Renvoie le délégué (string), en cas d'étape par délégation
     *
     * @return le délégué, null sinon.
     */
    public abstract String getDelegueName();

    /**
     * Retourne l'annotation publique déposée à cette étape du circuit.
     * 
     * @return l'annotation publique associée à l'étape.
     */
    public abstract String getAnnotation();
    
    /**
     * Retourne l'annotation privée déposée à cette étape du circuit.
     * 
     * @return l'annotation privée associée à l'étape.
     */
    public abstract String getAnnotationPrivee();
    
    /**
     * Retourne le signataire (Prenom Nom) de l'étape.
     * 
     * @return le signataire associé à l'étape.
     */
    public abstract String getSignataire();
    
    /**
     * Retourne une map d'informations sur le certificat utilisé pour la 
     * signature, ou <code>null</code> si aucun certificat n'a été utilisé.
     * Voir X509Util.getUsefulCertProps pour le contenu de la map:
     * <ul>
     *  <li>nom du porteur</li>
     *  <li>nom de l'organisation</li>
     *  <li>email</li>
     *  <li>nom de l'AC</li>
     *  <li>dates de validite du certificat</li>
     * </ul>
     * 
     * @return informations sur le signataire, via le certificat utilisé
     */
    public abstract Map<String, Object> getSignataireCertInfos();

    /**
     * Retourn la date à laquelle l'étape a été validée
     * 
     * @return la date de validation de l'étape
     */
    public abstract Date getDateValidation();

    /**
     * Retourne une indication sur le certificat utilisé pour la signature,
     * ou <code>null</code> si aucun certificat n'a été utilisé.
     * 
     * @return une indication du certificat utilisé
     */
    public abstract String getSignature();

    /**
     * Retourne la signature électronique, ou <code>null</code> si absent.
     *
     * @return signature électronique si présente
     */
    public abstract String getSignatureEtape();

    /**
     * Retourne la liste de diffusion concernant cette étape,
     * ou <code>null</code> si aucun parapheur à notifier.
     * 
     * @return une liste de parapheurs?
     */
    public abstract Set<NodeRef> getListeNotification();

    /**
     * Renseigne la liste de diffusion de l'étape.
     *
     * @return
     */
    public abstract void setAndRecordListeDiffusion(NodeService nodeService, Set<NodeRef> liste);

    /**
     * Retourne le nom de l'utilisateur qui à validé l'étape.
     * @return le nom de l'utilisateur qui à effectué l'action.
     */
    public abstract String getValidator();

    /**
     * Positionne le nom de l'utilisateur qui à validé l'étape.
     * @param username le nom de l'utilisateur.
     */
    public abstract void setValidator(String username);

    /**
     * Retourne une suite d'emails (séparés par virgule) de notification,
     * ou <code>null</code> si pas de notification externe.
     * 
     * @return une liste CSV d'adresses email
     */
    public abstract String getNotificationsExternes();

    public abstract HashMap<String, ArrayList<Annotation>> getGraphicalAnnotations();

    public abstract NodeRef getNodeRef();

    public abstract Set<String> getListeMetadatas();

    public abstract Set<String> getListeMetadatasRefus();

    public abstract boolean isSigned();
}
