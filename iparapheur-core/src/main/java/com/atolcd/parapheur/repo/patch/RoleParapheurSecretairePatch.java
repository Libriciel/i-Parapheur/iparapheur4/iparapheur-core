package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jmaire
 * Date: 24/03/2014
 * Time: 18:19
 */
public class RoleParapheurSecretairePatch extends XPathBasedPatch {

    public static final String ALL_PARAPHEURS_XPATH = "/app:company_home" +
            "/ph:parapheurs" +
            "/*";

    private AuthorityService authorityService;

    @Override
    protected String getXPathQuery() {
        return ALL_PARAPHEURS_XPATH;
    }

    protected void patch(NodeRef parapheur) {
        NodeService nodeService = getNodeService();

        String roleName = "PHSECRETARIAT_" + parapheur.getId();
        String authorityName = authorityService.getName(AuthorityType.ROLE, roleName);

        if (!authorityService.authorityExists(authorityName)) {

            List<String> secretariat = (List<String>) nodeService.getProperty(parapheur, ParapheurModel.PROP_SECRETAIRES);

            if ((secretariat != null) && (secretariat.size() > 0)) {
                // On ne créé pas le rôle s'il n'y a pas de secrétaire.
                authorityService.createAuthority(AuthorityType.ROLE, roleName);

                for (String secretaire : secretariat) {
                    if ((secretaire.trim().length() > 0) && (authorityService.authorityExists(secretaire))) {
                        authorityService.addAuthority(authorityName, authorityService.getName(AuthorityType.USER, secretaire));
                    }
                }
            }
        }

    }

    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

}