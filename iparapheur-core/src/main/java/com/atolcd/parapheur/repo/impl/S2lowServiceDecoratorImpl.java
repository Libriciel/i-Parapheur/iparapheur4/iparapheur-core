/*
 * Version 3.2
 * CeCILL Copyright (c) 2006-2012, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurException;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.S2lowService;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.atolcd.parapheur.repo.impl.exceptions.UnknownMailsecTransactionException;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.multipart.FilePart;

/**
 * S2lowServiceDecoratorImpl
 *
 * @author Emmanuel Peralta - Adullact Projet
 *         Date: 27/12/11
 *         Time: 14:50
 */
public class S2lowServiceDecoratorImpl implements S2lowService {
    private S2lowService s2lowService;
    private FastServiceImpl fastService;

    private TenantAdminService tenantAdminService;
    private TenantService tenantService;

    private NodeService nodeService;

    public void setS2lowService(S2lowService s2lowService) {
        this.s2lowService = s2lowService;
    }

    public void setFastService(FastServiceImpl fastService) {
        this.fastService = fastService;
    }

    public void setTenantAdminService(TenantAdminService tenantAdminService) {
        this.tenantAdminService = tenantAdminService;
    }

    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    private S2lowService getTdtService() {
        if (s2lowService.isActesEnabled() || s2lowService.isHeliosEnabled()) {
            return s2lowService;
        } else if (fastService.isHeliosEnabled()) {
            return fastService;
        }

        return s2lowService;
    }

    private S2lowService getActesService() {
        return s2lowService;
    }

    private S2lowService getHeliosService() {
        return getTdtService();
    }

    private S2lowService getSecureMailService() {
        return s2lowService;
    }

    @Override
    public boolean isEnabled() {
        return s2lowService.isEnabled() || fastService.isEnabled();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Properties getXadesSignatureProperties(String typeDossier) {
        return getHeliosService().getXadesSignatureProperties(typeDossier);
    }

    @Override
    public Properties getXadesSignaturePropertiesWithDossier(NodeRef dossier) {
        String typeDossier = this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER).toString();
        return getTdtServiceForDossier(dossier).getXadesSignatureProperties(typeDossier);
    }

    @Override
    public Properties getPadesSignaturePropertiesWithDossier(NodeRef dossier) {
        String typeDossier = this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER).toString();
        return getTdtServiceForDossier(dossier).getPadesSignatureProperties(typeDossier);
    }

    @Override
    public Properties getCustomXadesSignaturePropertiesWithDossier(NodeRef dossier) {
        String typeDossier = this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER).toString();
        return getTdtServiceForDossier(dossier).getCustomXadesSignatureProperties(typeDossier);
    }

    @Override
    public HttpClient createConnexionHTTPS(Map<String, String> properties, InputStream is) {
        return getActesService().createConnexionHTTPS(properties, is);
    }

    @Override
    public List<String> isCertificateOk(Map<String, String> properties, InputStream is) {
        return getActesService().isCertificateOk(properties, is);
    }

    @Override
    public boolean isConnexionOK(Map<String, String> properties, InputStream is) {
        return getActesService().isConnexionOK(properties, is);
    }

    @Override
    public Map<Integer, String> getS2lowActesNatures() {
        return getActesService().getS2lowActesNatures();
    }

    @Override
    public Map<String, String> getS2lowActesClassifications() {
        return getActesService().getS2lowActesClassifications();
    }

    @Override
    public int updateS2lowActesClassifications() throws IOException {
        return getActesService().updateS2lowActesClassifications();
    }

    @Override
    public boolean isCertificateAbleToConnect(String protocole, String typeDossier) throws IOException {
        return getActesService().isCertificateAbleToConnect(protocole, typeDossier);
    }

    @Override
    public List<String> getListLoginForType(String protocole, String typeDossier) throws IOException {
        return getActesService().getListLoginForType(protocole, typeDossier);
    }

    @Override
    public boolean isConnectionOK(String protocole, String typeDossier) throws IOException {
        return getActesService().isConnectionOK(protocole, typeDossier);
    }

    @Override
    public void envoiS2lowActes(NodeRef dossier, String nature, String classification, String numero, String objet, String date)
            throws IOException, ParapheurException {
        getActesService().envoiS2lowActes(dossier, nature, classification, numero, objet, date);
    }

    @Override
    public String setS2lowActesArchiveURL(NodeRef dossier) throws IOException {
        return getActesService().setS2lowActesArchiveURL(dossier);
    }

    @Override
    public int getInfosS2low(NodeRef dossier) throws IOException {
        return getTdtServiceForDossier(dossier).getInfosS2low(dossier);
    }

    @Override
    public String statutS2lowToString(int code) {
        return getTdtService().statutS2lowToString(code);
    }

    @Override
    public NodeRef getS2lowActesClassificationNodeRef() {
        return getActesService().getS2lowActesClassificationNodeRef();
    }

    @Override
    public void envoiS2lowHelios(NodeRef dossier) throws IOException {
        getTdtServiceForDossier(dossier).envoiS2lowHelios(dossier);
    }

    /**
     * Pour le moment on accepte que si ce n'est pas FAST alors c'est S2LOW...
     * Très utile pour la signature XAdES !
     * 
     * @param dossier
     * @return le tdtService qui correspond.
     */
    private S2lowService getTdtServiceForDossier(NodeRef dossier) {
        String nomTdt = (String)nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_NOM);

        if (FastServiceImpl.PROP_TDT_NOM_FAST.equals(nomTdt)) {
            return fastService;
        } else {
            return s2lowService;
        }
        //        if (nomTdt.equals(S2lowService.PROP_TDT_NOM_S2LOW)) {
        //            return s2lowService;
        //        }
        //        else if (nomTdt.equals(FastServiceImpl.PROP_TDT_NOM_FAST)) {
        //            return fastService;
        //        }
        //        else {
        //            throw new RuntimeException(String.format("Le tdt %s n'existe pas.", nomTdt));
        //        }
    }

    /* Unused by FAST */
    @Override
    public void getS2lowHeliosListePES_Retour() throws IOException {
        s2lowService.getS2lowHeliosListePES_Retour();
    }

    @Override
    public void restartGetS2lowStatusJob() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void restartGetS2lowStatusJobs() {
        final S2lowService fFastService = fastService;
        final S2lowService fS2lowService = s2lowService;

        List<Tenant> tenants = tenantAdminService.getAllTenants();
        List<String> admins = new ArrayList<String>();
        admins.add("admin"); // Execution in root tenant
        for (Tenant tenant : tenants) {
            if (tenant.isEnabled()) {
                admins.add(tenantService.getDomainUser("admin", tenant.getTenantDomain()));
            }
        }

        for (String admin : admins) {
            AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork() {

                @Override
                public Object doWork() throws Exception {
                    fastService.restartGetS2lowStatusJob();
                    s2lowService.restartGetS2lowStatusJob();
                    return null;
                }

            }, admin);
        }

    }

    @Override
    public boolean isMailServiceEnabled() {
        return getSecureMailService().isMailServiceEnabled();
    }

    @Override
    public boolean isActesEnabled() {
        return getTdtService().isActesEnabled();
    }

    @Override
    public boolean isHeliosEnabled() {
        return getTdtService().isHeliosEnabled();
    }

    @Override
    public void getSecureMailInfos(NodeRef dossier) {
        getSecureMailService().getSecureMailInfos(dossier);
    }

    @Override
    public String cancelMailSecJob(NodeRef dossier) throws IOException {
        return getSecureMailService().cancelMailSecJob(dossier);
    }
    
    @Override
    public boolean isMailSecJobCancelable(NodeRef dossier) {
        return getSecureMailService().isMailSecJobCancelable(dossier);
    }

    @Override
    public void restartGetMailsecS2lowStatusJobs() {
        getSecureMailService().restartGetMailsecS2lowStatusJobs();
    }

    @Override
    public String getSecureMailVersion() throws IOException {
        return getSecureMailService().getSecureMailVersion();
    }

    @Override
    public int sendSecureMail(List<String> mailto, List<String> mailcc, List<String> mailcci, String objet, String message, String password, boolean send_password, NodeRef dossierRef, List<NodeRef> attachments, boolean includeFirstPage) throws IOException, Exception {
        return getSecureMailService().sendSecureMail(mailto, mailcc, mailcci, objet, message, password, send_password, dossierRef, attachments, includeFirstPage);
    }

    @Override
    public int sendSecureMail(List<String> mailto, List<String> mailcc, List<String> mailcci, String objet, String message, String password, boolean send_password, List<FilePart> files) throws IOException {
        return getSecureMailService().sendSecureMail(mailto, mailcc, mailcci, objet, message, password, send_password, files);
    }

    @Override
    public int sendSecureMail(List<String> mailto, String objet, String message) throws IOException {
        return getSecureMailService().sendSecureMail(mailto, objet, message);
    }

    @Override
    public int getSecureMailCount() throws IOException {
        return getSecureMailService().getSecureMailCount();
    }

    @Override
    public Map<Integer, S2lowServiceImpl.SecureMailDetail> getSecureMailList(int limit, int offset) throws IOException {
        return getSecureMailService().getSecureMailList(limit, offset);
    }

    @Override
    public S2lowServiceImpl.SecureMailDetail getSecureMailDetail(int id) throws IOException, UnknownMailsecTransactionException {
        return getSecureMailService().getSecureMailDetail(id);
    }

    @Override
    public boolean deleteSecureMail(Integer id) throws IOException {
        return getSecureMailService().deleteSecureMail(id);
    }

    @Override
    public String getSecureMailTemplate(NodeRef dossier) {
        return getSecureMailService().getSecureMailTemplate(dossier);
    }

    @Override
    public String getSecureMailMessageWithTemplate(NodeRef dossier, String template) {
        return getSecureMailService().getSecureMailMessageWithTemplate(dossier, template);
    }

    @Override
    public Properties getPadesSignatureProperties(String type) {
        return getHeliosService().getPadesSignatureProperties(type);
    }

    @Override
    public Properties getCustomXadesSignatureProperties(String type) {
        return getHeliosService().getXadesSignatureProperties(type);
    }
}
