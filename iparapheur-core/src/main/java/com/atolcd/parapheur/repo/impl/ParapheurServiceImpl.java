/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2014, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Initially Developped by AtolCD, maintained by ADULLACT-projet team
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.repo.admin.UsersService;
import com.atolcd.parapheur.web.bean.wizard.batch.CakeFilter;
import com.atolcd.parapheur.web.bean.wizard.batch.DossierFilter;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.*;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import coop.libriciel.signature.exception.SignatureNotFoundException;
import coop.libriciel.signature.model.Signature;
import coop.libriciel.signature.service.SignatureService;
import coop.libriciel.utils.poppler.PopplerUtils;
import fr.bl.iparapheur.srci.SrciService;
import fr.starxpert.iparapheur.calque.repo.CalqueService;
import fr.starxpert.iparapheur.calque.repo.ImpressionCalqueService;
import org.adullact.iparapheur.comparator.NameNodeRefComparator;
import org.adullact.iparapheur.repo.amq.MessagesSender;
import org.adullact.iparapheur.repo.jscript.pastell.mailsec.MailsecPastellService;
import org.adullact.iparapheur.repo.notification.NotificationService;
import org.adullact.iparapheur.repo.worker.WorkerService;
import org.adullact.iparapheur.status.StatusMetier;
import org.adullact.iparapheur.util.CollectionsUtils;
import org.adullact.iparapheur.util.X509Util;
import org.adullact.libersign.util.signature.*;
import org.adullact.utils.StringUtils;
import org.alfresco.model.ApplicationModel;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.filestore.FileContentWriter;
import org.alfresco.repo.domain.audit.AuditDAO;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.repo.search.impl.lucene.LuceneQueryParser;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.security.authentication.AuthenticationException;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.repo.transaction.TransactionListener;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.audit.AuditService;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.rule.RuleService;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.*;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.ISO9075;
import org.alfresco.util.TempFileProvider;
import org.alfresco.web.bean.repository.MapNode;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.ui.common.Utils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.apache.xpath.XPathAPI;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERUTCTime;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.DigestCalculatorProvider;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.tsp.TSPUtil;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.Store;
import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.extensions.surf.util.Base64;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.util.FileCopyUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.faces.context.FacesContext;
import javax.transaction.UserTransaction;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.*;
import java.util.*;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class ParapheurServiceImpl implements ParapheurService, InitializingBean {

    public static final String ARCHIVES_CONFIGURATION_PATH = "/app:company_home/app:dictionary/ph:archives_configuration";
    public static final String TARGET_V3 = "3";
    public static final String TARGET_V4 = "4";
    private static final String KEY_PENDING_READ_NODES = "parapheurService.pendingReadNodes";
    private static final String KEY_PENDING_UPDATE_NODES = "parapheurService.pendingUpdateNodes";
    private static final String KEY_PENDING_PREVIEW_GENERATION = "parapheurService.pendingPreviewGeneration";
    private static Logger logger = Logger.getLogger(ParapheurService.class);
    private static Logger loggerTRACE = Logger.getLogger("TRACE." + ParapheurService.class.getName()); // BLEX
    private static Runtime rt = Runtime.getRuntime();
    @Autowired
    private NodeService nodeService;
    @Autowired
    private CopyService copyService;
    @Autowired
    private SearchService searchService;
    @Autowired
    private DictionaryService dictionaryService;
    @Autowired
    private FileFolderService fileFolderService;
    @Autowired
    private NamespaceService namespaceService;
    @Autowired
    private ContentService contentService;
    @Autowired
    private PolicyComponent policyComponent;
    @Autowired
    private TemplateService templateService;
    @Autowired
    private PersonService personService;
    @Autowired
    private RuleService ruleService;
    @Autowired
    private ActionService actionService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private AuthenticationComponent authenticationComponent;
    @Autowired
    @Qualifier("s2lowServiceDecorator")
    private S2lowService s2lowService;
    // BLEX
    @Autowired
    private SrciService srciService;
    @Autowired
    private AuthorityService authorityService;
    @Autowired
    private AuditService auditService;
    @Autowired
    private AuditDAO auditDAO;
    @Autowired
    @Qualifier("indexThreadPoolExecutor")
    private ThreadPoolExecutor threadExecuter;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    @Qualifier("policyBehaviourFilter")
    private BehaviourFilter policyFilter;
    @Autowired
    private TypesService typesService;
    @Autowired
    private WorkflowService workflowService;
    @Autowired
    private TimestampService timestampService;
    @Autowired
    private CalqueService calqueService;
    @Autowired
    private ImpressionCalqueService impressionCalqueService;
    @Autowired
    private MetadataService metadataService;
    @Autowired
    private JobService jobService;
    @Autowired
    private CorbeillesService corbeillesService;
    @Autowired
    private UsersService usersService;
    @Autowired
    private WorkerService workerService;
    @Autowired
    private ParapheurUserPreferences parapheurUserPreferences;
    @Autowired
    private TenantService tenantService;
    @Autowired
    @Qualifier("_dossierService")
    private DossierService dossierService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private MailsecPastellService mailsecPastellService;
    @Autowired
    private SignatureService signatureService;
    @Autowired
    private MessagesSender messagesSender;
    private TransactionListener transactionListener;
    private String habillage;
    private String uploadFileSizeLimit;
    private String parapheurWsGetdossierPdfEnabled;
    private String parapheurWsGetdossierPdfDocName;
    private List<String> proprietesAuditees;
    private Properties configuration;

    private static byte[] getBytesDigest(final byte[] bytesToDigest, final String digestAlgorithm) {
        Security.addProvider(new BouncyCastleProvider());    // CryptoUtil.addBouncyCastle();

        MessageDigest digest;
        byte[] hash = null;
        try {
            digest = MessageDigest.getInstance(digestAlgorithm, "BC");
            digest.update(bytesToDigest);
            hash = digest.digest();
        } catch (java.security.NoSuchAlgorithmException e) {
            logger.error(e.getLocalizedMessage(), e);
        } catch (java.security.NoSuchProviderException e) {
            logger.error(e.getLocalizedMessage(), e);
        }
        return hash;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setCopyService(CopyService copyService) {
        this.copyService = copyService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setDictionaryService(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setTypesService(TypesService typesService) {
        this.typesService = typesService;
    }

    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    public void setTimestampService(TimestampService timestampService) {
        this.timestampService = timestampService;
    }

    public void setCalqueService(CalqueService calqueService) {
        this.calqueService = calqueService;
    }

    /**
     * @param namespaceService The namespaceService to set.
     */
    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    public void setJobService(JobService jobService) {
        this.jobService = jobService;
    }

    public void setParapheurUserPreferences(ParapheurUserPreferences parapheurUserPreferences) {
        this.parapheurUserPreferences = parapheurUserPreferences;
    }

    /**
     * @param contentService The contentService to set.
     */
    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setPolicyComponent(PolicyComponent policyComponent) {
        this.policyComponent = policyComponent;
    }

    /**
     * @param templateService The templateService to set.
     */
    public void setTemplateService(TemplateService templateService) {
        this.templateService = templateService;
    }

    /**
     * @param personService The personService to set.
     */
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    /**
     * @param ruleService The ruleService to set.
     */
    public void setRuleService(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    /**
     * @param actionService The actionService to set.
     */
    public void setActionService(ActionService actionService) {
        this.actionService = actionService;
    }

    public ImpressionCalqueService getImpressionCalqueService() {
        return impressionCalqueService;
    }

    public void setImpressionCalqueService(ImpressionCalqueService impressionCalqueService) {
        this.impressionCalqueService = impressionCalqueService;
    }

    /**
     * @param permissionService The permissionService to set.
     */
    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    /**
     * @param authenticationService The authenticationService to set.
     */
    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public void setAuthenticationComponent(AuthenticationComponent authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }

    /**
     * @param service the s2lowService to set
     */
    public void setS2lowService(S2lowService service) {
        s2lowService = service;
    }

    /**
     * @param authorityService the authorityService to set
     */
    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    /**
     * @param auditService the auditService to set
     */
    public void setAuditService(AuditService auditService) {
        this.auditService = auditService;
    }

    public void setThreadExecuter(ThreadPoolExecutor threadExecuter) {
        this.threadExecuter = threadExecuter;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setPolicyFilter(BehaviourFilter policyFilter) {
        this.policyFilter = policyFilter;
    }

    public void setCorbeillesService(CorbeillesService corbeillesService) {
        this.corbeillesService = corbeillesService;
    }

    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    public MessagesSender getMessagesSender() {
        return messagesSender;
    }

    public void setMessagesSender(MessagesSender messagesSender) {
        this.messagesSender = messagesSender;
    }

    @Override
    public Properties getConfiguration() {
        return configuration;
    }

    /**
     * @param properties The configuration to set.
     */
    public void setConfiguration(Properties properties) {
        configuration = properties;
    }

    /**
     * @return the proprietesAuditees
     */
    public List<String> getProprietesAuditees() {
        return proprietesAuditees;
    }

    /**
     * @param proprietesAuditees the proprietesAuditees to set
     */
    public void setProprietesAuditees(List<String> proprietesAuditees) {
        this.proprietesAuditees = proprietesAuditees;
    }

    /**
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() {
        Assert.notNull(nodeService, "There must be a node service");
        Assert.notNull(searchService, "There must be a search service");
        Assert.notNull(dictionaryService, "There must be a dictionary service");
        Assert.notNull(fileFolderService, "There must be a fileFolder service");
        Assert.notNull(templateService, "There must be a template service");
        Assert.notNull(permissionService, "There must be a permission service");
        Assert.notNull(actionService, "There must be a action service");
        Assert.notNull(personService, "There must be a person service");
        Assert.notNull(ruleService, "There must be a rule service");
        Assert.notNull(contentService, "There must be a content service");
        Assert.notNull(namespaceService, "There must be a namespace service");
        Assert.notNull(authenticationService, "There must be an authentication service");
        Assert.notNull(s2lowService, "There must be a S2low service");
        Assert.notNull(authorityService, "There must be an authority service");
        Assert.notNull(policyComponent, "There must be a policy component");
        Assert.notNull(configuration, "There must be a configuration element");
        Assert.notNull(threadExecuter, "There must be a thread executer");
        Assert.notNull(transactionService, "There must be a transaction service");
        Assert.notNull(typesService, "There must be a typesService service");
        Assert.notNull(workflowService, "There must be a workflow service");
        Assert.notNull(timestampService, "There must be a timestamp service");
        Assert.notNull(calqueService, "There must be a calque service");
        Assert.notNull(auditService, "There must be a audit service");
        Assert.notNull(policyFilter, "There must be a behaviour filter");
        Assert.notNull(corbeillesService, "There must be a corbeilles service");
    }

    public void init() {
        this.getHabillage();
    }

    @SuppressWarnings("unchecked")
    private void queueNode(String mode, NodeRef nodeRef) {
    }

    @Override
    public void updateProperties(NodeRef dossier, Map<QName, Serializable> metadataValues) {
        nodeService.setProperties(dossier, metadataValues);
    }

    @Override
    public void generatePreviewForNodeRef(NodeRef document, boolean duringReload) {
        if (isPreviewEnabled()) {
            queueNode(KEY_PENDING_PREVIEW_GENERATION, document);
        }
        if (duringReload) {
            dossierService.deleteImagesPreviews(document, getParentDossier(document));
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isParapheur(NodeRef nodeRef) {
        return isOfType(nodeRef, ParapheurModel.TYPE_PARAPHEUR);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isCorbeille(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isCorbeille(NodeRef nodeRef) {
        return isOfType(nodeRef, ParapheurModel.TYPE_CORBEILLE);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isCorbeilleVirtuelle(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isCorbeilleVirtuelle(NodeRef nodeRef) {
        return isOfType(nodeRef, ParapheurModel.TYPE_CORBEILLE_VIRTUELLE);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isDossier(NodeRef nodeRef) {
        return isOfType(nodeRef, ParapheurModel.TYPE_DOSSIER);
    }

    @Override
    public boolean isPreviewEnabled() {
        boolean retVal = true;
        String flag = this.configuration.getProperty("parapheur.preview.enabled");
        if (flag != null && flag.equals("false")) {
            retVal = false;
        }
        return retVal;
    }

    private boolean isMobilePreviewEnabled() {
        boolean retVal = true;
        String flag = this.configuration.getProperty("parapheur.mobilepreview.enabled");
        if (flag != null && flag.equals("false")) {
            retVal = false;
        }
        return retVal;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getHabillage()
     */
    @Override
    public String getHabillage() {
        if (habillage == null) {
            habillage = this.configuration.getProperty(ParapheurModel.PARAPHEUR_HABILLAGE_KEY);
            if (habillage == null) {
                logger.warn("Propriete " + ParapheurModel.PARAPHEUR_HABILLAGE_KEY + " non trouvee, valeur utilisee par defaut = " + ParapheurModel.PARAPHEUR_HABILLAGE_VALEUR_DEFAUT);
                habillage = ParapheurModel.PARAPHEUR_HABILLAGE_VALEUR_DEFAUT;
            } else {
                logger.info("Propriete " + ParapheurModel.PARAPHEUR_HABILLAGE_KEY + " trouvee = " + habillage);
            }
        }
        return habillage;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getUploadFileSizeLimit()
     */
    @Override
    public String getUploadFileSizeLimit() {
        if (uploadFileSizeLimit == null) {
            uploadFileSizeLimit = this.configuration.getProperty(ParapheurModel.PARAPHEUR_UPLOAD_FILE_SIZE_LIMIT_KEY);
            if (uploadFileSizeLimit == null) {
                logger.warn("Propriete " + ParapheurModel.PARAPHEUR_UPLOAD_FILE_SIZE_LIMIT_KEY + " non trouvee, valeur utilisee par defaut = " + ParapheurModel.PARAPHEUR_UPLOAD_FILE_SIZE_LIMIT_DEFAUT);
                uploadFileSizeLimit = ParapheurModel.PARAPHEUR_UPLOAD_FILE_SIZE_LIMIT_DEFAUT;
            } else {
                logger.info("Propriete " + ParapheurModel.PARAPHEUR_UPLOAD_FILE_SIZE_LIMIT_KEY + " trouvee = " + uploadFileSizeLimit);
            }
        }
        return uploadFileSizeLimit;
    }

    @Override
    public boolean isParapheurWsGetdossierPdfEnabled() {
        if (parapheurWsGetdossierPdfEnabled == null) {
            parapheurWsGetdossierPdfEnabled = this.configuration.getProperty("parapheur.ws.getdossier.pdf.enabled");
            if (parapheurWsGetdossierPdfEnabled == null) {
                logger.warn("Propriété parapheur.ws.getdossier.pdf.enabled non trouvee, valeur 'false' utilisee par defaut.");
                parapheurWsGetdossierPdfEnabled = "false";
            } else {
                logger.info("Propriete parapheur.ws.getdossier.pdf.enabled trouvee = " + parapheurWsGetdossierPdfEnabled);
            }
        }
        return ("true".equalsIgnoreCase(parapheurWsGetdossierPdfEnabled) ? true : false);
    }

    public void setParapheurWsGetdossierPdfEnabled(String value) {
        this.parapheurWsGetdossierPdfEnabled = value;
    }

    @Override
    public String getParapheurWsGetdossierPdfDocName() {
        if (parapheurWsGetdossierPdfDocName == null) {
            parapheurWsGetdossierPdfDocName = this.configuration.getProperty(ParapheurModel.PARAPHEUR_WS_GETDOSSIER_PDF_KEY);
            if (parapheurWsGetdossierPdfDocName == null) {
                logger.warn("Propriété " + ParapheurModel.PARAPHEUR_WS_GETDOSSIER_PDF_KEY + " non trouvée, valeur par defaut.");
                parapheurWsGetdossierPdfDocName = ParapheurModel.PARAPHEUR_WS_GETDOSSIER_PDF_VALEUR_DEFAUT;
            } else {
                logger.info("Propriete " + ParapheurModel.PARAPHEUR_WS_GETDOSSIER_PDF_KEY + "trouvee = " + parapheurWsGetdossierPdfDocName);
            }
        }
        return parapheurWsGetdossierPdfDocName;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getParapheurs()
     */
    @Override
    public List<NodeRef> getParapheurs() {
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.parapheurs.childname");

        List<NodeRef> results;
        try {
            results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
        } catch (AccessDeniedException e) {
            logger.error("Impossible d'accéder au répertoire de stockage des parapheurs.");
            return null;
        }

        if (results != null && results.size() == 1) {
            List<NodeRef> nodes = new ArrayList<NodeRef>();
            List<ChildAssociationRef> lstEnfants = nodeService.getChildAssocs(results.get(0),
                    ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
            for (ChildAssociationRef tmpChildAssoc : lstEnfants) {
                nodes.add(tmpChildAssoc.getChildRef());
            }

            return nodes;
        } else {
            logger.error("getParapheurs - Erreur lors de la récupération des parapheurs.");
            return null;
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getParapheursSorted()
     */
    @Override
    public List<NodeRef> getParapheursSorted() {
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.parapheurs.childname");

        List<NodeRef> results;
        try {
            results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
        } catch (AccessDeniedException e) {
            logger.error("Impossible d'accéder au répertoire de stockage des parapheurs.");
            return null;
        }

        if (results != null && results.size() == 1) {
            List<NodeRef> nodes = new ArrayList<NodeRef>();
            List<ChildAssociationRef> lstEnfants = nodeService.getChildAssocs(results.get(0),
                    ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
            for (ChildAssociationRef tmpChildAssoc : lstEnfants) {
                nodes.add(tmpChildAssoc.getChildRef());
            }

            // Sort node ref alphabeticaly
            NameNodeRefComparator comparator = new NameNodeRefComparator(nodeService);
            Collections.sort(nodes, comparator);

            return nodes;
        } else {
            logger.error("getParapheursSorted - Erreur lors de la récupération des parapheurs.");
            return null;
        }
    }

    /**
     * @deprecated ne gère pas les permissions, utiliser {@link com.atolcd.parapheur.repo.admin.impl.UsersServiceImpl#addUserToBureau(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef, boolean)} à la place.
     */
    @Deprecated
    @Override
    public void addOwnerToBureau(NodeRef bureau, NodeRef user) {
        ArrayList<String> proprietaires = (ArrayList<String>) (nodeService.getProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR));
        String userName = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
        proprietaires.add(userName);
        HashMap<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, proprietaires);
        nodeService.addProperties(bureau, properties);
    }

    /**
     * @deprecated ne gère pas les permissions, utiliser {@link com.atolcd.parapheur.repo.admin.impl.UsersServiceImpl#removeUserFromBureau(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef, boolean)} à la place.
     */
    @Deprecated
    @Override
    public void removeOwnerFromBureau(NodeRef bureau, NodeRef user) {
        ArrayList<String> proprietaires = (ArrayList<String>) (nodeService.getProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR));
        String userName = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);

        proprietaires.remove(userName);
        HashMap<QName, Serializable> properties = new HashMap<QName, Serializable>();
        nodeService.removeProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);
        properties.put(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, proprietaires);
        nodeService.addProperties(bureau, properties);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getDossierFromBureauAndName(NodeRef, String)
     */
    @Override
    public NodeRef getDossierFromBureauAndName(NodeRef bureauNodeRef, String dossierName) {
        List<NodeRef> results;
        Path lePrimaryPath = nodeService.getPath(bureauNodeRef);
        String xpath = lePrimaryPath.toPrefixString(namespaceService) + "/*/*";
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("le xpath ISO9075decode: " + ISO9075.decode(xpath) + "\nle xpath:\t" + xpath);
            }
            String query = "PATH:\"" + xpath + "\"";
            query += " AND cm:name:\"" + CakeFilter.luceneEscapeString(dossierName) + "\"";

            SearchParameters searchParameters = new SearchParameters();
            searchParameters.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
            searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
            searchParameters.setQuery(query);

            results = searchService.query(searchParameters).getNodeRefs();
        } catch (AccessDeniedException e) {
            logger.error("Impossible d'accéder au répertoire de stockage du dossier.");
            return null;
        }
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Erreur lors de la récupération 'directe' du dossier par bureau et son nom.");
            }
            return null;
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getParapheursFromName(String)
     */
    @Override
    public List<NodeRef> getParapheursFromName(String nomParapheur) {
        String query = "PATH:\"/" + this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.parapheurs.childname") + "/*\"";
        query += "AND ={http://www.alfresco.org/model/content/1.0}name:" + CakeFilter.luceneEscapeString(nomParapheur);

        return searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_FTS_ALFRESCO, query).getNodeRefs();
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#searchParapheur(String)
     */
    @Override
    public List<NodeRef> searchParapheur(String search) {
        StringBuilder query = new StringBuilder(128);
        for (StringTokenizer t = new StringTokenizer(search.trim(), " "); t.hasMoreTokens(); ) {
            String token = LuceneQueryParser.escape(t.nextToken());
            query.append("+TYPE:\"").append(ParapheurModel.TYPE_PARAPHEUR).append("\" ");
            query.append("+(@").append(NamespaceService.CONTENT_MODEL_PREFIX).append("\\:name:\"*");
            query.append(token);
            query.append("*\") ");
        }
        FacesContext context = FacesContext.getCurrentInstance();
        UserTransaction tx = null;
        List<NodeRef> parapheurs = new ArrayList<NodeRef>();
        try {
            tx = Repository.getUserTransaction(context, true);
            tx.begin();

            if (logger.isDebugEnabled()) {
                logger.debug("Query " + query.toString());
            }

            // define the search parameters
            SearchParameters params = new SearchParameters();
            params.setLanguage(SearchService.LANGUAGE_LUCENE);
            params.addStore(Repository.getStoreRef());
            params.setQuery(query.toString());

            ResultSet results = searchService.query(params);

            try {
                parapheurs = results.getNodeRefs();
            } finally {
                results.close();
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Found " + parapheurs.size() + " bureau(x)");
            }
            tx.commit();
        } catch (Exception err) {
            logger.error("Erreur lors de la recherche du bureau : ", err);
            try {
                if (tx != null) {
                    tx.rollback();
                }
            } catch (Exception tex) {
            }
        }
        return parapheurs;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#searchParapheurInAssociateList(org.alfresco.service.cmr.repository.NodeRef, String)
     */
    @Override
    public List<NodeRef> searchParapheurInAssociateList(NodeRef bureauCourant, String search) {
        Assert.isTrue(isParapheur(bureauCourant), "bureauCourant doit être un ph:parapheur");
        StringBuilder query = new StringBuilder(128);
        for (StringTokenizer t = new StringTokenizer(search.trim(), " "); t.hasMoreTokens(); ) {
            String token = LuceneQueryParser.escape(t.nextToken());
            query.append("+TYPE:\"").append(ParapheurModel.TYPE_PARAPHEUR).append("\" ");
            query.append("+(@").append(NamespaceService.CONTENT_MODEL_PREFIX).append("\\:name:\"*");
            query.append(token);
            query.append("*\") ");
        }
        FacesContext context = FacesContext.getCurrentInstance();
        UserTransaction tx = null;
        List<NodeRef> parapheurs = new ArrayList<NodeRef>();
        try {
            tx = Repository.getUserTransaction(context, true);
            tx.begin();

            if (logger.isDebugEnabled()) {
                logger.debug("Query " + query.toString());
            }

            // define the search parameters
            SearchParameters params = new SearchParameters();
            params.setLanguage(SearchService.LANGUAGE_LUCENE);
            params.addStore(Repository.getStoreRef());
            params.setQuery(query.toString());

            ResultSet results = searchService.query(params);

            try {
                parapheurs = results.getNodeRefs();
                parapheurs.retainAll(getDelegationsPossibles(bureauCourant));
            } finally {
                results.close();
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Found " + parapheurs.size() + " bureau(x)");
            }
            tx.commit();
        } catch (Exception err) {
            logger.error("Erreur lors de la recherche du bureau : ", err);
            try {
                if (tx != null) {
                    tx.rollback();
                }
            } catch (Exception tex) {
            }
        }
        return parapheurs;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getAllManagedParapheursByOpAdmin(java.lang.String)
     */
    @Override
    public List<NodeRef> getAllManagedParapheursByOpAdmin(String userName) {
        List<NodeRef> nodes = new ArrayList<>();
        List<NodeRef> rootsList = getRootManagedParapheursByOpAdmin(userName);
        if (!rootsList.isEmpty()) {
            nodes.addAll(rootsList);
            rootsList.forEach(nodeRef -> {
                List<NodeRef> children = recursiveGetChildParapheurs(nodeRef);
                if (children != null && !children.isEmpty()) {
                    children.forEach(n -> {
                        if(!nodes.contains(n)) nodes.add(n);
                    });
                }
            });
        }
        return nodes;
        //        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.parapheurs.childname");
        //
        //        List<NodeRef> results = null;
        //        try {
        //            results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
        //        } catch (AccessDeniedException e) {
        //            logger.error("Impossible d'accéder au répertoire de stockage des parapheurs.");
        //            return null;
        //        }
        //
        //        if (results != null && results.size() == 1) {
        //            Set<String> authorities = this.authorityService.getAuthoritiesForUser(userName);
        //            List<NodeRef> nodes = new ArrayList<NodeRef>();
        //            List<ChildAssociationRef> lstEnfants = nodeService.getChildAssocs(results.get(0), ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
        //            for (ChildAssociationRef tmpChildAssoc : lstEnfants) {
        //                NodeRef candidat = tmpChildAssoc.getChildRef();
        //                if (authorities.contains("ROLE_PHADMIN_" + candidat.getId())) {
        //                    nodes.add(tmpChildAssoc.getChildRef());
        //                }
        //            }
        //
        //            return nodes;
        //        } else {
        //            logger.error("Erreur lors de la récupération des parapheurs.");
        //            return null;
        //        }
    }

    private List<NodeRef> recursiveGetChildParapheurs(NodeRef rootRef) {
        List<NodeRef> result = null;
        if (isParapheur(rootRef)) {
            List<NodeRef> children = getChildParapheurs(rootRef);
            if (children != null && !children.isEmpty()) {
                result = new ArrayList<NodeRef>();
                result.addAll(children);
                for (NodeRef child : children) {
                    List<NodeRef> grandChildren = recursiveGetChildParapheurs(child);
                    if (grandChildren != null && !grandChildren.isEmpty()) {
                        result.addAll(grandChildren);
                    }
                }
            }
        }
        return result;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getAllManagedParapheursByOpAdmin(java.lang.String)
     */
    @Override
    public List<NodeRef> getRootManagedParapheursByOpAdmin(String userName) {
        List<NodeRef> nodes = new ArrayList<NodeRef>();
        StoreRef storeRef = new StoreRef(getConfiguration().getProperty("spaces.store"));
        String prefix = "ROLE_PHADMIN_";
        int indexPrefix = prefix.length();
        Set<String> authorities = this.authorityService.getAuthoritiesForUser(userName);
        for (String authString : authorities) {
            if (authString.startsWith(prefix)) {
                NodeRef prfRef = new NodeRef(storeRef, authString.substring(indexPrefix));
                //System.out.println("\t Trouvé root-parapheur " + authString.substring(indexPrefix) + " = '" + getNomParapheur(prfRef) + "'.");
                nodes.add(prfRef);
            }
        }
        return nodes;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getCorbeilles(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public List<NodeRef> getCorbeilles(NodeRef parapheur) {
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");

        List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(parapheur, ContentModel.ASSOC_CONTAINS,
                RegexQNamePattern.MATCH_ALL);
        List<NodeRef> res = null;

        if (childAssocs != null && childAssocs.size() > 0) {
            res = new ArrayList<NodeRef>(childAssocs.size());
            for (ChildAssociationRef ref : childAssocs) {
                if (isCorbeille(ref.getChildRef())) {
                    res.add(ref.getChildRef());
                }
            }
        }

        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getDossiers(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public List<NodeRef> getDossiers(NodeRef corbeille) {
        Assert.isTrue(isCorbeille(corbeille), "NodeRef doit être un ph:corbeille");

        List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(corbeille, ContentModel.ASSOC_CONTAINS,
                RegexQNamePattern.MATCH_ALL);
        List<NodeRef> res = null;

        if (childAssocs != null && childAssocs.size() > 0) {
            res = new ArrayList<NodeRef>(childAssocs.size());
            for (ChildAssociationRef ref : childAssocs) {
                if (isDossier(ref.getChildRef())) {
                    res.add(ref.getChildRef());
                }
            }
        }

        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getDocuments(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    @Deprecated
    public @Nullable List<NodeRef> getDocuments(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier [" + dossier.getId() + "]");

        final List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(dossier, ContentModel.ASSOC_CONTAINS,
                RegexQNamePattern.MATCH_ALL);
        final RetryingTransactionHelper tx;
        tx = transactionService.getRetryingTransactionHelper();

        tx.setMaxRetries(5);

        return tx.doInTransaction(new RetryingTransactionCallback<List<NodeRef>>() {

            @Override
            public List<NodeRef> execute() throws Throwable {
                try {
                    List<NodeRef> result = null;
                    if (childAssocs != null && childAssocs.size() > 0) {
                        result = new ArrayList<NodeRef>(childAssocs.size());
                        for (ChildAssociationRef ref : childAssocs) {

				            /* Afficher seulement les documents pas le dossier de preview mobile */
                            NodeRef childRef = ref.getChildRef();
                            QName childType = nodeService.getType(childRef);

                            if (childType.equals(ContentModel.TYPE_CONTENT)) {
                                result.add(ref.getChildRef());
                            }
                        }
                    }

                    return result;
                } catch(InvalidNodeRefException e) {
                    throw new ConcurrencyFailureException(e.getMessage());
                }
            }
        });
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getMainDocuments(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public @NotNull List<NodeRef> getMainDocuments(NodeRef dossier) {

        List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(dossier, ContentModel.ASSOC_CONTAINS,
                RegexQNamePattern.MATCH_ALL);
        List<NodeRef> res = new ArrayList<NodeRef>();
        NodeRef first = null;
        if (childAssocs != null) {
            for (ChildAssociationRef child : childAssocs) {
                NodeRef childRef = child.getChildRef();
                QName childType = nodeService.getType(childRef);
                // Backward compatibility : Before V4 the main document was the first one.
                if ((first == null) && childType.equals(ContentModel.TYPE_CONTENT)) {
                    first = childRef;
                }

                if (nodeService.hasAspect(childRef, ParapheurModel.ASPECT_MAIN_DOCUMENT)) {
                    res.add(childRef);
                }
            }
            if (res.isEmpty() && (first != null)) {
                // We set the first document as the main document
                nodeService.addAspect(first, ParapheurModel.ASPECT_MAIN_DOCUMENT, null);
                res.add(first); // Backward compatibility...
            }
        }

        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getAttachments(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public @Nullable List<NodeRef> getAttachments(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier [" + dossier.getId() + "]");

        List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(dossier, ContentModel.ASSOC_CONTAINS,
                RegexQNamePattern.MATCH_ALL);
        List<NodeRef> res = null;
        if (childAssocs != null && childAssocs.size() > 0) {
            res = new ArrayList<NodeRef>(childAssocs.size());
            boolean found = false;
            for (ChildAssociationRef ref : childAssocs) {
                /* Afficher seulement les documents pas le dossier de preview mobile */
                NodeRef childRef = ref.getChildRef();
                QName childType = nodeService.getType(childRef);
                if (childType.equals(ContentModel.TYPE_CONTENT)) {
                    if (!nodeService.hasAspect(childRef, ParapheurModel.ASPECT_MAIN_DOCUMENT)) {
                        res.add(ref.getChildRef());
                    } else { // Backward compatibility : the aspect "MAIN_DOCUMENT" was introduced in V4
                        found = true;
                    }
                }
            }
            if (!found && !res.isEmpty()) { // Backward compatibility : Befor V4 the main document is the first one
                res = res.subList(1, res.size());
            }
        }

        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getCorbeille(org.alfresco.service.cmr.repository.NodeRef,
     * org.alfresco.service.namespace.QName)
     */
    @Override
    public NodeRef getCorbeille(NodeRef parapheur, QName name) {
        Assert.isTrue(isParapheur(parapheur), "Parapheur doit être un ph:parapheur");

        List<ChildAssociationRef> childAssocs = nodeService.getChildAssocs(parapheur, ContentModel.ASSOC_CONTAINS, name);
        if (childAssocs.size() != 1) {
            // Par design: Une seule bannette d'un nom donné par parapheur
            return null;
        }

        return childAssocs.get(0).getChildRef();
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getParentDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public NodeRef getParentDossier(NodeRef nodeRef) {
        return getFirstParentOfType(nodeRef, ParapheurModel.TYPE_DOSSIER);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getParentCorbeille(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public NodeRef getParentCorbeille(NodeRef nodeRef) {
        return getFirstParentOfType(nodeRef, ParapheurModel.TYPE_CORBEILLE);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getParentParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public NodeRef getParentParapheur(NodeRef nodeRef) {
        return getFirstParentOfType(nodeRef, ParapheurModel.TYPE_PARAPHEUR);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getParapheurOwner(org.alfresco.service.cmr.repository.NodeRef)
     * @param nodeRef
     */
    @Override
    public String getParapheurOwner(NodeRef nodeRef) {
        NodeRef parapheur = getParentParapheur(nodeRef);
        if (parapheur == null) {
            return null;
        }
        return (String) this.nodeService.getProperty(parapheur, ParapheurModel.PROP_PROPRIETAIRE_PARAPHEUR);
    }

    @Override
    public List<String> getParapheurOwners(NodeRef nodeRef) {
        NodeRef parapheur = getParentParapheur(nodeRef);
        if (parapheur == null) {
            return null;
        }
        return (ArrayList<String>) this.nodeService.getProperty(parapheur, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);
    }

    /**
     * Renvoie la liste des propriétaires du bureau passé en paramètre ayant
     * activé les notifications (mails groupés).
     *
     * @param bureau le bureau concerné.
     * @return la liste des propriétaires du bureau passé en paramètre ayant
     * activé les notifications (mails groupés).
     */
    private List<String> getParapheurOwnersToNotify(NodeRef bureau) {
        List<String> retVal = new ArrayList<String>();
        List<String> owners = getParapheurOwners(bureau);
        for (String owner : owners) {
            if (parapheurUserPreferences.isNotificationsEnabledForUsername(owner)) {
                retVal.add(owner);
            }
        }
        return retVal;
    }

    private List<String> getParapheurDelegatesToNotify(NodeRef nodeRef) {
        List<String> retVal = new ArrayList<String>();
        List<String> delegates = getUsersOnDelegationPath(getParentParapheur(nodeRef));
        for (String delegate : delegates) {
            if (parapheurUserPreferences.isNotificationsEnabledForUsername(delegate)) {
                retVal.add(delegate);
            }
        }
        return retVal;
    }

    private List<String> getParapheurSecretariesToNotify(NodeRef nodeRef) {
        List<String> retVal = new ArrayList<String>();
        List<String> secretaries = getSecretariatParapheur(nodeRef);
        for (String secretary : secretaries) {
            if (parapheurUserPreferences.isNotificationsEnabledForUsername(secretary)) {
                retVal.add(secretary);
            }
        }
        return retVal;
    }

    @Override
    public NodeRef getUniqueParapheurForUser(String userName) {
        List<NodeRef> parapheurs = getOwnedParapheurs(userName);
        if (parapheurs != null && !parapheurs.isEmpty()) {
            return parapheurs.get(0);
        }
        return null;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isParapheurOwner(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public boolean isParapheurOwner(NodeRef nodeRef, String userName) {
        List<String> proprietaires = (ArrayList<String>) nodeService.getProperty(nodeRef, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);

        return proprietaires != null && proprietaires.contains(userName);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isParapheurOwnerOrDelegate(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public boolean isParapheurOwnerOrDelegate(NodeRef parapheur, String userName) {
        return (isParapheurOwner(parapheur, userName) || isParapheurDelegate(parapheur, userName));
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isParapheurDelegate(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public boolean isParapheurDelegate(NodeRef parapheur, String userName) {

        List<NodeRef> parapheursDelegues = getParapheursOnDelegationPath(parapheur);
        boolean trouve = false;
        int i = 0;
        while ((i < parapheursDelegues.size()) && !trouve) {
            List<String> delegues = (ArrayList<String>) nodeService.getProperty(parapheursDelegues.get(i), ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);
            if (!delegues.isEmpty() && delegues.contains(userName)) {
                trouve = true;
            }
            i++;
        }
        return trouve;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isParapheurDelegate(org.alfresco.service.cmr.repository.NodeRef,
     * org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isParapheurDelegate(NodeRef parapheur, NodeRef courant) {


        return getParapheursOnDelegationPath(parapheur).contains(courant);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isOwnerOrDelegateOfDossier(org.alfresco.service.cmr.repository.NodeRef,
     * org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isOwnerOrDelegateOfDossier(NodeRef parapheur, NodeRef dossier) {

        NodeRef owner = getParentParapheur(dossier);
        return (owner.equals(parapheur)) || isParapheurDelegate(owner, parapheur);
    }

    @Override
    public List<NodeRef> getOwnedParapheurs(String userName) {
        //FIXME: how long does it takes ? compare with the lucene version !!
        //long start = System.currentTimeMillis();

        List<NodeRef> retVal = new ArrayList<NodeRef>();

        for (NodeRef parapheur : getParapheurs()) {
            List<String> proprietaires = (ArrayList<String>) nodeService.getProperty(parapheur, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);
            if (proprietaires != null && proprietaires.contains(userName)) {
                retVal.add(parapheur);
            }
        }
        /*
         String luceneQuery = String.format("+TYPE:\"ph:parapheur\" @ph\\:proprietaires:\"%s\"", userName);

		 SearchParameters searchParameters = new SearchParameters();
		 searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
		 searchParameters.setQuery(luceneQuery);
		 searchParameters.addStore(Repository.getStoreRef());

		 ResultSet resultSet = searchService.query(searchParameters);

		 retVal = resultSet.getNodeRefs();
		 resultSet.close();
		 */
        //long duration = System.currentTimeMillis() - start;

        // System.out.println(String.format("GetOwnedParapheurs fetching time = %d",duration));

        return retVal;
    }

    @Override
    public List<NodeRef> getOwnedBySecretariatParapheurs(String userName) {

        List<NodeRef> retVal = new ArrayList<NodeRef>();

        for (NodeRef parapheur : getParapheurs()) {
            List<String> secretaires = (ArrayList<String>) nodeService.getProperty(parapheur, ParapheurModel.PROP_SECRETAIRES);
            if (secretaires != null && secretaires.contains(userName)) {
                retVal.add(parapheur);
            }
        }

        return retVal;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getOwnedParapheur(java.lang.String)
     */
    @Deprecated
    @Override
    public NodeRef getOwnedParapheur(String userName) {
        throw new RuntimeException("Soon to be removed !!");
        //        if (userName.contains("@")) {
        //            for (NodeRef parapheur : getParapheurs()) {
        //                if (isParapheurOwner(parapheur, userName)) {
        //                    // Par design: un seul parapheur par utilisateur.
        //                    return parapheur;
        //                }
        //            }
        //            return null;
        //        } else {
        //            /*
        //             * STV: ce code optimisé ne fonctionne qu'en l'absence de '@'
        //             *  c'est à dire les installations mono-collectivité.
        //             * C'est un BUG Alfresco, le moteur JAXEN Xpath ne pose pas de souci
        //             */
        //            NodeRef nodeRef = getParapheursHome();
        //            List<NodeRef> candidates = searchService.selectNodes(nodeRef,
        //                    "./*[@ph:proprietaire='" + userName + "']",
        //                    null,
        //                    namespaceService,
        //                    false);
        //
        //            if (candidates.isEmpty()) {
        //                logger.warn("pas de parapheur pour: " + userName);
        //                return null;
        //            }
        //
        //            return candidates.get(0);
        //        }
    }

    /**
     * EN OPTION POUR PLUS TARD
     *
     * @see com.atolcd.parapheur.repo.ParapheurService#getSharedParapheurs(java.lang.String)
     */
    @Override
    public List<NodeRef> getSharedParapheurs(String userName) {
        throw new UnsupportedOperationException();
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isParapheurSecretaire(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean isParapheurSecretaire(NodeRef nodeRef, String userName) {
        List<String> lstSecretaires = (List<String>) nodeService.getProperty(nodeRef, ParapheurModel.PROP_SECRETAIRES);
        return lstSecretaires != null && lstSecretaires.contains(userName);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getSecretariatParapheurs(java.lang.String)
     */
    @Override
    public List<NodeRef> getSecretariatParapheurs(String userName) {
        List<NodeRef> parapheurs = new ArrayList<NodeRef>();

        for (NodeRef parapheur : getParapheurs()) {
            if (isParapheurSecretaire(parapheur, userName)) {
                parapheurs.add(parapheur);
            }
        }

        return parapheurs;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isAdministrateur(java.lang.String)
     */
    @Override
    public boolean isAdministrateur(String userName) {
        /**
         * Est-il administrateur technique ? (full access)
         */
        Set<String> authorities = authorityService.getAuthoritiesForUser(userName);
        return authorities.contains("GROUP_ALFRESCO_ADMINISTRATORS");
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isAdministrateurFonctionnel(java.lang.String)
     */
    @Override
    public boolean isAdministrateurFonctionnel(String userName) {
        /**
         * Est-il admin fonctionnel ?
         */
        boolean isOpAdmin = false;
        Set<String> authorities = authorityService.getAuthoritiesForUser(userName);
        for (String zeAuthString : authorities) {
            if (zeAuthString.startsWith("ROLE_PHADMIN_")) {
                isOpAdmin = true;
                break;
            }
        }

        return isOpAdmin;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isAdministrateurFonctionnelOf(java.lang.String,
     * org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isAdministrateurFonctionnelOf(String userName, NodeRef parapheur) {
        return (isAdministrateurFonctionnel(userName) &&
                getAllManagedParapheursByOpAdmin(userName).contains(parapheur));
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getNomParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public String getNomParapheur(NodeRef parapheur) {
        Assert.isTrue(isParapheur(parapheur), "Node Ref doit être un ph:parapheur");
        return this.nodeService.getProperty(parapheur, ContentModel.PROP_TITLE).toString();
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getParapheurName(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public String getParapheurName(NodeRef parapheur) {
        Assert.isTrue(isParapheur(parapheur), "Node Ref doit être un ph:parapheur");
        return this.nodeService.getProperty(parapheur, ContentModel.PROP_NAME).toString();
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getNomProprietaire(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Deprecated
    @Override
    public String getNomProprietaire(NodeRef parapheur) {
        Assert.isTrue(isParapheur(parapheur), "Node Ref doit être un ph:parapheur");
        String parapheurOwner = getParapheurOwner(parapheur);
        if (parapheurOwner != null && !parapheurOwner.trim().equals("")) {
            NodeRef proprietaireRef = personService.getPerson(parapheurOwner);
            String firstName = (String) nodeService.getProperty(proprietaireRef, ContentModel.PROP_FIRSTNAME);
            String lastName = (String) nodeService.getProperty(proprietaireRef, ContentModel.PROP_LASTNAME);
            return firstName + (lastName != null ? (" " + lastName) : "");
        } else {
            return "Non-attribué";
        }
    }

    @Override
    public List<String> getNomProprietaires(NodeRef parapheur) {
        Assert.isTrue(isParapheur(parapheur), "Node Ref doit être un ph:parapheur");
        List<String> parapheurOwners = getParapheurOwners(parapheur);
        List<String> retVal = new ArrayList<String>();

        if (parapheurOwners != null && !parapheurOwners.isEmpty()) {
            for (String parapheurOwner : parapheurOwners) {
                /*
                 NodeRef proprietaireRef = personService.getPerson(parapheurOwner);
				 String firstName = (String) nodeService.getProperty(proprietaireRef, ContentModel.PROP_FIRSTNAME);
				 String lastName = (String) nodeService.getProperty(proprietaireRef, ContentModel.PROP_LASTNAME);
				 */
                if (parapheurOwner == null) {
                    logger.warn("{" + getNomParapheur(parapheur) + "} a parapheurOwner == null !!??");
                } else if (parapheurOwner.isEmpty()) {
                    logger.warn("{" + getNomParapheur(parapheur) + "} a parapheurOwner VIDE !!??");
                } else {
                    retVal.add(this.getPrenomNomFromLogin(parapheurOwner));
                }
            }
            if (retVal.isEmpty()) {
                retVal.add("Non-attribué");
            }
            //return firstName + (lastName != null ? (" " + lastName) : "");
        } else {
            retVal.add("Non-attribué");
        }

        return retVal;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getPrenomNomFromLogin(String)
     */
    @Override
    public String getPrenomNomFromLogin(String userName) {
        Assert.hasText(userName, "userName ne doit pas être vide");
        NodeRef personneRef;
        try {
            personneRef = personService.getPerson(userName, false);
        } catch(NoSuchPersonException e) {
            return userName;
        }
        if(personneRef == null) {
            return userName;
        }
        String firstName = (String) nodeService.getProperty(personneRef, ContentModel.PROP_FIRSTNAME);
        String lastName = (String) nodeService.getProperty(personneRef, ContentModel.PROP_LASTNAME);
        return firstName + (lastName != null ? (" " + lastName) : "");
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#auditWithNewBackend(String, String, org.alfresco.service.cmr.repository.NodeRef, java.util.List)
     */
    @Override
    public void auditWithNewBackend(String applicationName, String message, NodeRef dossier, List<Object> list) {
        if (logger.isDebugEnabled()) {
            logger.debug("auditWithNewBackend(" + applicationName + ", " + message + ", " + dossier + ", " + list + ")");
        }
        AuditDAO.AuditApplicationInfo application = auditDAO.getAuditApplication(applicationName);
        Calendar cal = Calendar.getInstance();

        Map<String, Serializable> values = new HashMap<String, Serializable>();

        values.put("message", message);
        values.put("dossier", dossier);
        values.put("list", (Serializable) list);
        auditDAO.createAuditEntry(application.getId(), cal.getTimeInMillis(), AuthenticationUtil.getRunAsUser(), values);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#setCircuit(org.alfresco.service.cmr.repository.NodeRef,
     * java.util.List)
     */
    @Override
    public void setCircuit(NodeRef dossier, List<EtapeCircuit> circuit) {
        Assert.isTrue(!isEmis(dossier), "Node Ref doit être un ph:dossier non-émis");
        NodeRef emetteur = getEmetteur(dossier);
        if (circuit != null && !circuit.isEmpty()) {
            // On retire l'émetteur s'il est présent comme premier élément de la liste
            // ET que l'étape en question est un VISA !!!
            if (emetteur.equals(circuit.get(0).getParapheur())
                    && circuit.get(0).getActionDemandee().trim().equalsIgnoreCase("VISA")) {   // On ne modifie pas directement la liste...
                circuit = circuit.subList(1, circuit.size());
            }
            //	    // On vérifie qu'il n'y a pas de doublons
            //	    HashSet<NodeRef> hashTest = new HashSet<NodeRef>(circuit);
            //	    hashTest.add(emetteur);
            //	    Assert.isTrue(hashTest.size() == circuit.size() + 1, "Il y a des doublons dans le circuit");
        }

        NodeRef premiereEtape = getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        // On vide le circuit précédent au cas où, à part la première étape
        NodeRef etapeSupprim = getFirstChild(premiereEtape, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
        if (etapeSupprim != null) {
            this.nodeService.deleteNode(etapeSupprim);
        }

        if (circuit != null && !circuit.isEmpty()) {
            /*
             * Ajout des étapes correspondant aux parapheurs de la liste
			 */
            Map<QName, Serializable> proprietes = new HashMap<QName, Serializable>();
            // PROP_EFFECTUEE à false pour toutes étapes.
            proprietes.put(ParapheurModel.PROP_EFFECTUEE, false);
            ChildAssociationRef assocPrecedent;
            NodeRef etapePrecedenteRef = premiereEtape;
            String actionArchivage = "";
            int variableStep = 0;
            for (EtapeCircuit etape : circuit) {
                // PROP_PASSE_PAR
                if (EtapeCircuit.TRANSITION_PARAPHEUR.equals(etape.getTransition())) {
                    proprietes.put(ParapheurModel.PROP_PASSE_PAR, etape.getParapheur());
                } else if (EtapeCircuit.TRANSITION_CHEF_DE.equals(etape.getTransition())) {
                    // Si N+1 n'a pas de responsable, N est assigné à l'étape.
                    NodeRef acteurPrecedent = (NodeRef) nodeService.getProperty(etapePrecedenteRef, ParapheurModel.PROP_PASSE_PAR);
                    NodeRef responsable = getParapheurResponsable(acteurPrecedent);
                    NodeRef acteur = (responsable != null ? responsable : acteurPrecedent);
                    proprietes.put(ParapheurModel.PROP_PASSE_PAR, acteur);
                } else if (EtapeCircuit.TRANSITION_EMETTEUR.equals(etape.getTransition())) {
                    proprietes.put(ParapheurModel.PROP_PASSE_PAR, emetteur); // getEmetteur(dossier));
                }  else if (EtapeCircuit.TRANSITION_VARIABLE.equals(etape.getTransition())) {
                    List<NodeRef> acteursVariables = (List<NodeRef>) nodeService.getProperty(dossier, ParapheurModel.PROP_ACTEURS_VARIABLES);
                    NodeRef acteur = null;
                    if(acteursVariables != null && acteursVariables.size() > variableStep) {
                        acteur = acteursVariables.get(variableStep);
                        variableStep++;
                    }
                    if(acteur == null) {
                        throw new RuntimeException("Impossible de résoudre le circuit variable");
                    }
                    proprietes.put(ParapheurModel.PROP_PASSE_PAR, acteur); // getEmetteur(dossier));
                } else {
                    throw new IllegalArgumentException("Unknown workflow transition: " + etape.getTransition());
                }
                // PROP_ACTION_DEMANDEE
                actionArchivage = etape.getActionDemandee().trim();
                proprietes.put(ParapheurModel.PROP_ACTION_DEMANDEE, actionArchivage);
                assocPrecedent = this.nodeService.createNode(etapePrecedenteRef,
                        ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE, ParapheurModel.ASSOC_DOSSIER_ETAPE,
                        ParapheurModel.TYPE_ETAPE_CIRCUIT, proprietes);
                etapePrecedenteRef = assocPrecedent.getChildRef();
                // PROP_LISTE_NOTIFICATION  Liste de notifications
                Set<NodeRef> notifiesRefs = etape.getListeNotification();
                NodeRef fakeEmetteur = new NodeRef("workspace://SpacesStore/_emetteur_");
                if (notifiesRefs.contains(fakeEmetteur)) {
                    //System.out.println("#### AAAAAAAAAAAAAARH workspace://SpacesStore/_emetteur_ dans setCircuit!!, parmi " + notifiesRefs.size());
                    // on remplace!
                    notifiesRefs.remove(fakeEmetteur);
                    notifiesRefs.add(emetteur);
                }
                /**
                 * else { //System.out.println("#### Notifiables? y en a : " +
                 * notifiesRefs.size());
                 }*
                 */
                //this.nodeService.setProperty(etapePrecedenteRef, ParapheurModel.PROP_LISTE_NOTIFICATION, (Serializable) etape.getListeNotification());
                this.nodeService.setProperty(etapePrecedenteRef, ParapheurModel.PROP_LISTE_NOTIFICATION, (Serializable) notifiesRefs);
                this.nodeService.setProperty(etapePrecedenteRef, ParapheurModel.PROP_LISTE_METADONNEES, (Serializable) etape.getListeMetadatas());
                this.nodeService.setProperty(etapePrecedenteRef, ParapheurModel.PROP_LISTE_METADONNEES_REFUS, (Serializable) etape.getListeMetadatasRefus());
            }
            if (!actionArchivage.equalsIgnoreCase(EtapeCircuit.ETAPE_ARCHIVAGE)) {   // si pas d'étape ARCHIVAGE explicite , on en ajoute une pour l'emetteur
                proprietes.put(ParapheurModel.PROP_PASSE_PAR, emetteur); // getEmetteur(dossier));
                proprietes.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_ARCHIVAGE);
                this.nodeService.createNode(etapePrecedenteRef,
                        ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE, ParapheurModel.ASSOC_DOSSIER_ETAPE,
                        ParapheurModel.TYPE_ETAPE_CIRCUIT, proprietes);
            }
            dossierService.setPermissionsDossier(dossier);
        }
    }

    @Override
    public void appendCircuit(NodeRef dossier, List<EtapeCircuit> circuit) {
        Assert.notNull(dossier, "'dossier' is a mandatory parameter");

        NodeRef lastEtape = getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        while (getFirstChild(lastEtape, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE) != null) {
            lastEtape = getFirstChild(lastEtape, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
        }

        if ("ARCHIVAGE".equals(nodeService.getProperty(lastEtape, ParapheurModel.PROP_ACTION_DEMANDEE))) {
            nodeService.setProperty(lastEtape, ParapheurModel.PROP_ACTION_DEMANDEE, "VISA");
        }
        int variableStep = 0;
        for (EtapeCircuit etape : circuit) {
            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ParapheurModel.PROP_EFFECTUEE, false);
            if (EtapeCircuit.TRANSITION_PARAPHEUR.equals(etape.getTransition())) {
                properties.put(ParapheurModel.PROP_PASSE_PAR, etape.getParapheur());
            } else if (EtapeCircuit.TRANSITION_CHEF_DE.equals(etape.getTransition())) {
                // Si N+1 n'a pas de responsable, N est assigné à l'étape.
                NodeRef acteurPrecedent = (NodeRef) nodeService.getProperty(lastEtape, ParapheurModel.PROP_PASSE_PAR);
                NodeRef responsable = getParapheurResponsable(acteurPrecedent);
                NodeRef acteur = (responsable != null ? responsable : acteurPrecedent);
                properties.put(ParapheurModel.PROP_PASSE_PAR, acteur);
            } else if (EtapeCircuit.TRANSITION_EMETTEUR.equals(etape.getTransition())) {
                properties.put(ParapheurModel.PROP_PASSE_PAR, getEmetteur(dossier));
            } else if (EtapeCircuit.TRANSITION_VARIABLE.equals(etape.getTransition())) {
                List<NodeRef> acteursVariables = (List<NodeRef>) nodeService.getProperty(dossier, ParapheurModel.PROP_ACTEURS_VARIABLES);
                NodeRef acteur = null;
                if(acteursVariables != null && acteursVariables.size() > variableStep) {
                    acteur = acteursVariables.get(variableStep);
                    variableStep++;
                }
                if(acteur == null) {
                    throw new RuntimeException("Impossible de résoudre le circuit variable");
                }
                properties.put(ParapheurModel.PROP_PASSE_PAR, acteur); // getEmetteur(dossier));
            } else {
                throw new IllegalArgumentException("Unknown workflow transition: " + etape.getTransition());
            }
            properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, etape.getActionDemandee().trim());
            properties.put(ParapheurModel.PROP_LISTE_NOTIFICATION, (Serializable) etape.getListeNotification());
            properties.put(ParapheurModel.PROP_LISTE_METADONNEES, (Serializable) etape.getListeMetadatas());
            properties.put(ParapheurModel.PROP_LISTE_METADONNEES_REFUS, (Serializable) etape.getListeMetadatasRefus());
            lastEtape = this.nodeService.createNode(lastEtape,
                    ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE, ParapheurModel.ASSOC_DOSSIER_ETAPE,
                    ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();
        }
        if (!EtapeCircuit.ETAPE_ARCHIVAGE.equalsIgnoreCase((String) nodeService.getProperty(lastEtape, ParapheurModel.PROP_ACTION_DEMANDEE))) {
            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ParapheurModel.PROP_PASSE_PAR, getEmetteur(dossier));
            properties.put(ParapheurModel.PROP_EFFECTUEE, false);
            properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_ARCHIVAGE);
            this.nodeService.createNode(lastEtape,
                    ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE, ParapheurModel.ASSOC_DOSSIER_ETAPE,
                    ParapheurModel.TYPE_ETAPE_CIRCUIT, properties);
        }
        dossierService.setPermissionsDossier(dossier);
        // AUDIT
        List<Object> list = new ArrayList<Object>();
        list.add(StatusMetier.STATUS_NON_LU);
        auditWithNewBackend("ParapheurServiceCompat", "Enchainement d'un nouveau circuit", dossier, list);
    }

//    public void setDiffusion(NodeRef dossier, Set<NodeRef> diffusion) {
//        Assert.isTrue(isDossier(dossier));
//        this.nodeService.setProperty(dossier, ParapheurModel.PROP_LISTE_DIFFUSION, (Serializable) diffusion);
//    }

    @Override
    public void restartCircuit(NodeRef dossier, String type, String subtype, String workflow) {
        nodeService.setProperty(dossier, ParapheurModel.PROP_TYPE_METIER, type);
        nodeService.setProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER, subtype);
        nodeService.setProperty(dossier, ParapheurModel.PROP_WORKFLOW, workflow);
        nodeService.setProperty(dossier, ParapheurModel.PROP_TERMINE, false);

        corbeillesService.moveDossier(dossier,
                nodeService.getPrimaryParent(dossier).getParentRef(),
                getCorbeille(getParentParapheur(dossier), ParapheurModel.NAME_A_TRAITER));

        nodeService.moveNode(dossier,
                getCorbeille(getParentParapheur(dossier), ParapheurModel.NAME_A_TRAITER),
                ContentModel.ASSOC_CONTAINS,
                nodeService.getPrimaryParent(dossier).getQName());

        notificationService.notifierPourEnchainementCircuit(dossier);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#setDiffusion(org.alfresco.service.cmr.repository.NodeRef, java.util.Set)
     * @see com.atolcd.parapheur.repo.ParapheurService#createParapheur(java.util.Map)
     */
    @Override
    public NodeRef createParapheur(final Map<QName, Serializable> properties) {

        RetryingTransactionHelper trx_A = transactionService.getRetryingTransactionHelper();
        return trx_A.doInTransaction(new RetryingTransactionCallback<NodeRef>() {
            @Override
            public NodeRef execute() throws Throwable {
                Assert.isTrue(properties.containsKey(ContentModel.PROP_NAME),
                        "La propriété \"cm:name\" n'a pas été passée en paramètre.");

                // On récupère le nom du parpaheur dans la Map
                String nom = (String) properties.get(ContentModel.PROP_NAME);

                // On récupère le répertoire de stockage des parapheurs
                String xpath = configuration.getProperty("spaces.company_home.childname") + "/" + configuration.getProperty("spaces.parapheurs.childname");

                List<NodeRef> results;
                try {
                    results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
                } catch (AccessDeniedException e) {
                    if (logger.isEnabledFor(Level.WARN)) {
                        logger.warn("Erreur lors de l'accès au répertoire de stockage des parapheurs");
                    }
                    return null;
                }

                if (results == null || results.size() != 1) {
                    if (logger.isEnabledFor(Level.WARN)) {
                        logger.warn("Erreur lors de la récupération du répertoire de stockage des parapheurs");
                    }
                    return null;
                }

                NodeRef parapheursRep = results.get(0);

                // Création du parapheur
                FileInfo fileInfo = fileFolderService.create(parapheursRep, nom, ParapheurModel.TYPE_PARAPHEUR);
                NodeRef parapheur = fileInfo.getNodeRef();

                // Ajout des propriétés
                Map<QName, Serializable> props = new HashMap<QName, Serializable>();
                // propriétés par défaut
                props.put(ContentModel.PROP_CREATOR, authenticationService.getCurrentUserName());
                props.put(ContentModel.PROP_CREATED, new Date());
                props.putAll(properties);
                nodeService.setProperties(parapheur, props);

                // Création des actions de mail, utilisées par les règles
                /*
                 * Action mailDiffusionEmission = actionService.createAction("parapheur-mail");
				 * mailDiffusionEmission.setParameterValue("dest","diff");
				 * mailDiffusionEmission.setParameterValue("template","diffusion-emission.ftl");
				 * mailDiffusionEmission.setExecuteAsynchronously(false); Action mailCurrentReception =
				 * actionService.createAction("parapheur-mail"); mailCurrentReception.setParameterValue("dest","current");
				 * mailCurrentReception.setParameterValue("template","current-reception.ftl");
				 * mailCurrentReception.setExecuteAsynchronously(false); Action mailOwnerReception =
				 * actionService.createAction("parapheur-mail"); mailOwnerReception.setParameterValue("dest","owner");
				 * mailOwnerReception.setParameterValue("template","owner-reception.ftl");
				 * mailOwnerReception.setExecuteAsynchronously(false); Action mailDiffusionVisa =
				 * actionService.createAction("parapheur-mail"); mailDiffusionVisa.setParameterValue("dest","diff");
				 * mailDiffusionVisa.setParameterValue("template","diffusion-visa.ftl");
				 * mailDiffusionVisa.setExecuteAsynchronously(false); Action mailOwnerArchivage =
				 * actionService.createAction("parapheur-mail"); mailOwnerArchivage.setParameterValue("dest","owner");
				 * mailOwnerArchivage.setParameterValue("template","owner-archivage.ftl");
				 * mailOwnerArchivage.setExecuteAsynchronously(false); Action mailOwnerRetour =
				 * actionService.createAction("parapheur-mail"); mailOwnerRetour.setParameterValue("dest","owner");
				 * mailOwnerRetour.setParameterValue("template","owner-retour.ftl");
				 * mailOwnerRetour.setExecuteAsynchronously(false);
				 */

                // Création des corbeilles & corbeilles virtuelles
                Map<QName, Serializable> proprietesC = new HashMap<QName, Serializable>();

                proprietesC.put(ParapheurModel.PROP_CHILD_COUNT, 0);

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers à transmettre");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers à transmettre");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers en cours de préparation ou prêts à être émis.");
                nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_EN_PREPARATION,
                        ParapheurModel.TYPE_CORBEILLE, proprietesC);
                /*
                 * Rule rule = ruleService.createRule("outbound"); rule.setTitle("ParapheurMail");
				 * rule.addAction(mailDiffusionEmission); ruleService.saveRule(childRefCorbeille.getChildRef(),rule);
				 */

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers à traiter");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers à traiter");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers à viser ou à signer.");
                nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_TRAITER,
                        ParapheurModel.TYPE_CORBEILLE, proprietesC);
                /*
                 * rule = ruleService.createRule("inbound");
				 * rule.setTitle("ParapheurMail"); rule.addAction(mailOwnerReception);
				 * rule.addAction(mailCurrentReception);
				 * ruleService.saveRule(childRefCorbeille.getChildRef(),rule); rule =
				 * ruleService.createRule("outbound"); rule.setTitle("ParapheurMail2");
				 * rule.addAction(mailDiffusionVisa);
				 * ruleService.saveRule(childRefCorbeille.getChildRef(),rule);
				 */

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers en fin de circuit");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers en fin de circuit");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers ayant terminé leur circuit de validation.");
                proprietesC.put(ApplicationModel.PROP_ICON, "space-icon-default");
                nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_ARCHIVER,
                        ParapheurModel.TYPE_CORBEILLE, proprietesC);
                /*
                 * rule = ruleService.createRule("inbound");
				 * rule.setTitle("ParapheurMail"); rule.addAction(mailOwnerArchivage);
				 * ruleService.saveRule(childRefCorbeille.getChildRef(),rule);
				 */

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers retournés");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers retournés");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers rejetés lors du circuit de signature/visa.");
                nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_RETOURNES,
                        ParapheurModel.TYPE_CORBEILLE, proprietesC);
                /*
                 * rule = ruleService.createRule("inbound");
				 * rule.setTitle("ParapheurMail"); rule.addAction(mailOwnerRetour);
				 * ruleService.saveRule(childRefCorbeille.getChildRef(),rule);
				 */

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers en cours");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers en cours");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers qui ont été émis.");
                nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_EN_COURS,
                        ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, proprietesC);

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers traités");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers traités");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers qui ont été traités.");
                nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_TRAITES,
                        ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, proprietesC);

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers à venir");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers à venir");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers à venir");
                nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_VENIR,
                        ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, proprietesC);

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers récupérables");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers récupérables");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers sur lesquels on peut exercer un droit de remords.");
                nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_RECUPERABLES,
                        ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, proprietesC);

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers à relire - annoter");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers à relire - annoter");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers envoyés au secrétariat pour relecture - annotation.");
                nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_SECRETARIAT,
                        ParapheurModel.TYPE_CORBEILLE, proprietesC);

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers en retard");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers en retard");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers en retard");

                nodeService.createNode(parapheur,
                        ContentModel.ASSOC_CONTAINS,
                        ParapheurModel.NAME_EN_RETARD,
                        ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, proprietesC);

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers à imprimer");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers à imprimer");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers à imprimer");

                nodeService.createNode(parapheur,
                        ContentModel.ASSOC_CONTAINS,
                        ParapheurModel.NAME_A_IMPRIMER,
                        ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, proprietesC);

                proprietesC.put(ContentModel.PROP_NAME, "Dossiers en délégation");
                proprietesC.put(ContentModel.PROP_TITLE, "Dossiers en délégation");
                proprietesC.put(ContentModel.PROP_DESCRIPTION, "Dossiers ayant été délégués par d'autres bureaux.");
                nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_DOSSIERS_DELEGUES,
                        ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, proprietesC);

                authorityService.createAuthority(AuthorityType.ROLE, "PHOWNER_" + parapheur.getId());
                authorityService.createAuthority(AuthorityType.ROLE, "PHDELEGATES_" + parapheur.getId());
                return parapheur;
            }
        }, false, true);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getEmetteur(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public NodeRef getEmetteur(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        NodeRef emetteur = getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        /*if (logger.isDebugEnabled()) {
         logger.debug(" GETFIRSTCHILD: getEmetteur=" + emetteur);
		 }*/
        if (emetteur != null) {
            return (NodeRef) this.nodeService.getProperty(emetteur, ParapheurModel.PROP_PASSE_PAR);
        } else {
            return null;
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getDateLimite(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public Date getDateLimite(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");
        return (Date) nodeService.getProperty(dossier, ParapheurModel.PROP_DATE_LIMITE);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getMapNode(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public MapNode getMapNode(NodeRef nodeRef) {
        MapNode node = null;
        try {
            //permissionService.hasPermission(nodeRef, permissionService.getOwnerAuthority());
            //System.out.print(", ownerAuth=" + permissionService.getOwnerAuthority() + ", allAuths=" + permissionService.getAllAuthorities() + ", allPerms=" + permissionService.getAllPermission());
            node = new MapNode(nodeRef, this.nodeService, true);
            //System.out.println("...OK");
        } catch (org.alfresco.repo.security.permissions.AccessDeniedException ade) {
            System.out.println("getMapNode (" + nodeRef.getId() + ")...KO: " + ade.getMessage());
        } catch (Exception e) {
            System.out.println("getMapNode (" + nodeRef.getId() + ")...KO (generic exception): " + e.getMessage());
        }
        return node;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isEmis(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isEmis(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");

        NodeRef emetteur = getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        /*if (logger.isDebugEnabled()) {
         logger.debug(" GETFIRSTCHILD: ISEMIS =" + emetteur);
		 }*/
        if (emetteur != null) {
            return (Boolean) this.nodeService.getProperty(emetteur, ParapheurModel.PROP_EFFECTUEE);
        } else {
            return false;
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isTermine(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isTermine(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        return (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_TERMINE);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isRejete(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isRejete(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        String statusMetier = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS_METIER);
        /*
         * System.out.println("\tStatus métier : '" + statusMetier + "',
		 * matches('^Rejet')=" + (statusMetier.matches("^Rejet")?"oui":"non") +
		 * " startsWith('Rejet')=" +
		 * (statusMetier.startsWith("Rejet")?"oui":"non"));
		 */
        return statusMetier.startsWith("Rejet");
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getCircuit(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public List<EtapeCircuit> getCircuit(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        List<EtapeCircuit> circuit = new ArrayList<EtapeCircuit>();

        NodeRef emetteur = getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        /*if (logger.isDebugEnabled()) {
         logger.debug(" GETFIRSTCHILD: getCircuit=" + emetteur);
		 }*/
        if (emetteur == null || !this.nodeService.exists(emetteur)) {
            return null;
        }
        circuit.add(new EtapeCircuitImpl(nodeService, emetteur));

        for (NodeRef etape = getFirstChild(emetteur, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
             etape != null && this.nodeService.exists(etape);
             etape = getFirstChild(etape, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE)) {
            circuit.add(new EtapeCircuitImpl(nodeService, etape));
        }

        return circuit;
    }

    public List<EtapeCircuit> getCircuitAvecSignataireInfos(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        List<EtapeCircuit> circuit = new ArrayList<EtapeCircuit>();

        NodeRef emetteur = getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        /*if (logger.isDebugEnabled()) {
         logger.debug(" GETFIRSTCHILD: getCircuit=" + emetteur);
		 }*/
        if (emetteur == null || !this.nodeService.exists(emetteur)) {
            return null;
        }
        circuit.add(new EtapeCircuitImpl(nodeService, emetteur, true));

        for (NodeRef etape = getFirstChild(emetteur, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
             etape != null && this.nodeService.exists(etape);
             etape = getFirstChild(etape, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE)) {
            circuit.add(new EtapeCircuitImpl(nodeService, etape, true));
        }

        return circuit;
    }

    @Override
    public EtapeCircuit getFirstEtape(NodeRef dossier) {
        NodeRef emetteur = getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        if (emetteur == null || !this.nodeService.exists(emetteur)) {
            return null;
        }
        return new EtapeCircuitImpl(nodeService, emetteur);
    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public Set<NodeRef> getDiffusion(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        return new HashSet<NodeRef>((List<NodeRef>) this.nodeService.getProperty(dossier,
                ParapheurModel.PROP_LISTE_DIFFUSION));
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getDiffusionToutesEtapes(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Deprecated
    @SuppressWarnings("unchecked")
    @Override
    public Set<NodeRef> getDiffusionToutesEtapes(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        Set<NodeRef> res = new HashSet<NodeRef>();
        List<EtapeCircuit> circuit = this.getCircuit(dossier);
        if (circuit.get(0).getActionDemandee() == null) {
            return new HashSet<NodeRef>((List<NodeRef>) this.nodeService.getProperty(dossier, ParapheurModel.PROP_LISTE_DIFFUSION));
        } else {
            for (EtapeCircuit etape : circuit) {
                Set<NodeRef> liste = etape.getListeNotification();
                if (liste != null) {
                    res.addAll(liste);
                }
            }
            return res;
        }
    }

    @Deprecated
    @Override
    public void removeFromDiffusionToutesEtapes(NodeRef dossier, NodeRef parapheurRef) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");
        Assert.isTrue(isParapheur(parapheurRef), "Node Ref doit être un ph:parapheur");
        List<EtapeCircuit> circuit = this.getCircuit(dossier);
        if (circuit.get(0).getActionDemandee() == null) {   // compatibilité ascendante
            Set<NodeRef> setDiffusion = this.getDiffusion(dossier);
            if (setDiffusion.contains(parapheurRef)) {
                setDiffusion.remove(parapheurRef);
                this.nodeService.setProperty(dossier, ParapheurModel.PROP_LISTE_DIFFUSION, (Serializable) setDiffusion);
            }
        } else {
            for (EtapeCircuit etape : circuit) {
                Set<NodeRef> setDiffusion = etape.getListeNotification();
                if (setDiffusion.contains(parapheurRef)) {
                    setDiffusion.remove(parapheurRef);
                    etape.setAndRecordListeDiffusion(nodeService, setDiffusion);
                }
            }
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getActeurCourant(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    @Deprecated
    public String getActeurCourant(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        EtapeCircuitImpl etapeCourante = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
        if (etapeCourante == null) {
            if (logger.isEnabledFor(Level.DEBUG)) {
                logger.debug("Le dossier n'a pas d'étape courante : id = " + dossier);
            }

            return null;
        }
        NodeRef parapheur = etapeCourante.getParapheur();

        return getParapheurOwner(parapheur);
    }

    /**
     * BL Bugfix
     * La méthode getActeurCourant est dépréciée et utilise d'anciennes propriétés du parapheur qui peuvent ne plus être à jour (propriétaire).
     * <p/>
     * Cette méthode est utilisée uniquement dans l'appel du TDTPROXY BL pour assurer la compatibilité.
     *
     * @param dossier
     * @return
     */
    @Override
    public String getActeurCourantBLSRCI(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        EtapeCircuitImpl etapeCourante = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
        if (etapeCourante == null) {
            if (logger.isEnabledFor(Level.DEBUG)) {
                logger.debug("Le dossier n'a pas d'étape courante : id = " + dossier);
            }

            return null;
        }
        NodeRef parapheur = etapeCourante.getParapheur();

        return getParapheurOwners(parapheur).get(0);
    }

    @Override
    public List<String> getActeursCourant(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        // On utilise le bureau sur lequel se trouve le dossier pour gérer le rejet au lieu de bureau de l'étape courante.
        NodeRef parapheur = getParentParapheur(dossier);

        return getParapheurOwners(parapheur);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isActeurCourant(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public boolean isActeurCourant(NodeRef dossier, String userName) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        // On utilise le bureau sur lequel se trouve le dossier pour gérer le rejet au lieu de bureau de l'étape courante.
        NodeRef parapheur = getParentParapheur(dossier);
        // TODO : lorsque le choix des dossier est en place, utiliser la fonction
        // isDelegate(dossier, userName)
        return isParapheurOwnerOrDelegate(parapheur, userName);
    }

    private boolean isActeurCourantWithoutDelegues(NodeRef dossier, String userName) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        // On utilise le bureau sur lequel se trouve le dossier pour gérer le rejet au lieu de bureau de l'étape courante.
        NodeRef parapheur = getParentParapheur(dossier);
        return isParapheurOwner(parapheur, userName);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getAnnotationPublique(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public String getAnnotationPublique(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        EtapeCircuitImpl etapeCourante = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
        Assert.notNull(etapeCourante, "Le dossier n'a pas d'étape courante (il est terminé ou dans un état invalide)");

        return etapeCourante.getAnnotation();
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#setAnnotationPublique(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public void setAnnotationPublique(NodeRef dossier, String annotation) {
        if(annotation != null && annotation.length() < 65535) {
            Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

            EtapeCircuitImpl etapeCourante = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
            Assert.notNull(etapeCourante, "Le dossier n'a pas d'étape courante (il est terminé ou dans un état invalide)");

            this.nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_ANNOTATION, annotation);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getAnnotationPrivee(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public String getAnnotationPrivee(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        EtapeCircuitImpl etapeCourante = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
        Assert.notNull(etapeCourante, "Le dossier n'a pas d'étape courante (il est terminé ou dans un état invalide)");

        return etapeCourante.getAnnotationPrivee();
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#setAnnotationPrivee(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public void setAnnotationPrivee(NodeRef dossier, String annotation) {
        if(annotation != null && annotation.length() < 65535) {
            Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

            EtapeCircuitImpl etapeCourante = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
            Assert.notNull(etapeCourante, "Le dossier n'a pas d'étape courante (il est terminé ou dans un état invalide)");

            this.nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_ANNOTATION_PRIVEE, annotation);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getAnnotationPriveePrecedente(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public String getAnnotationPriveePrecedente(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        List<EtapeCircuit> circuit = this.getCircuit(dossier);
        EtapeCircuit etapePrecedente = null;
        if (circuit != null) {
            for (EtapeCircuit etape : circuit) {
                if (etape.isApproved()) {
                    etapePrecedente = etape;
                } else {
                    break;
                }
            }
        }

        if (etapePrecedente != null) {
            return etapePrecedente.getAnnotationPrivee();
        } else {
            return null;
        }
    }

    @Override
    public void changeSignatureToSignaturePapier(NodeRef dossier) {
        nodeService.setProperty(dossier, ParapheurModel.PROP_SIGNATURE_PAPIER, true);
        notificationService.notifierPourPassageSignaturePapier(dossier);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#setSignataire(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String)
     */
    @Override
    public void setSignataire(NodeRef dossier, String signataire) {
        setSignataire(dossier, signataire, (X509Certificate[]) null);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#setSignataire(org.alfresco.service.cmr.repository.NodeRef, String, java.security.cert.X509Certificate[])
     */
    @Override
    public void setSignataire(NodeRef dossier, String signataire, X509Certificate[] cert) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        EtapeCircuitImpl etapeCourante = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
        Assert.notNull(etapeCourante, "Le dossier n'a pas d'étape courante (il est terminé ou dans un état invalide)");

        NodeRef etapeRef = etapeCourante.getNodeRef();
        this.nodeService.setProperty(etapeRef, ParapheurModel.PROP_SIGNATAIRE, signataire);
        Boolean signaturePapier = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_PAPIER);
        if (!signaturePapier && cert != null && cert.length > 0) {
            /**
             * On recupere les informations du certificat du signataire, à
             * déposer dans une structure JSON (à exploiter par le bordereau)
             */

            String signature = new StringBuilder("Certificat émis par \"").append(X509Util.extractCN(cert[0].getIssuerX500Principal().getName()))
                    .append("\" pour le compte de ").append(X509Util.extractCN(cert[0].getSubjectX500Principal().getName())).append(".").toString();

            this.nodeService.setProperty(etapeRef, ParapheurModel.PROP_SIGNATURE, signature);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#setSignataire(org.alfresco.service.cmr.repository.NodeRef, String, org.bouncycastle.cert.X509CertificateHolder[])
     */
    @Override
    public void setSignataire(NodeRef dossier, String signataire, X509CertificateHolder[] cert) {
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        EtapeCircuitImpl etapeCourante = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
        Assert.notNull(etapeCourante, "Le dossier n'a pas d'étape courante (il est terminé ou dans un état invalide)");

        NodeRef etapeRef = etapeCourante.getNodeRef();
        this.nodeService.setProperty(etapeRef, ParapheurModel.PROP_SIGNATAIRE, signataire);
        Boolean signaturePapier = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_PAPIER);
        if (!signaturePapier && cert != null && cert.length > 0) {
            // String signature = new StringBuilder("Certificat émis par \"").append(X509Util.extractCN(cert[0].getIssuerX500Principal().getName())).append("\" pour le compte de ").append(X509Util.extractCN(cert[0].getSubjectX500Principal().getName())).append(".").toString();
            /**
             * On recupere les informations du certificat du signataire, à
             * déposer dans une structure JSON (à exploiter par le bordereau)
             */
            String signature = new JSONObject(X509Util.getUsefulCertProps(cert[0])).toString();
            System.out.println("PROP_SIGNATAIRE: " + signataire);
            System.out.println("PROP_SIGNATURE : " + signature);
            this.nodeService.setProperty(etapeRef, ParapheurModel.PROP_SIGNATURE, signature);
        }
    }

    @Override
    public void approveV4(NodeRef dossier, NodeRef parapheurCourant) {
        logger.debug("APPROVE - Entree dans la methode");
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        String username = AuthenticationUtil.getRunAsUser();
        NodeRef parapheurRef = getParentParapheur(dossier);
        Boolean signaturePapier = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_PAPIER);
        List<EtapeCircuit> circuit = getCircuit(dossier);

        Assert.notEmpty(circuit, "Le dossier n'a pas de circuit de validation: id = " + dossier);
        Assert.isTrue(circuit.size() > 1, "Impossible d'émettre le dossier, aucun circuit n'a été défini.");
        Assert.isTrue(!this.isRejete(dossier), "Impossible de valider le dossier celui ci à été rejeté");
        // Récupération de la prochaine étape
        EtapeCircuitImpl etapeCourante = null;
        EtapeCircuit etapeSuivante = null;
        for (EtapeCircuit etape : circuit) {
            if (!etape.isApproved()) {
                if (etapeCourante == null) {
                    etapeCourante = (EtapeCircuitImpl) etape;
                } else if (etapeSuivante == null) {
                    etapeSuivante = etape;
                    break;
                }
            }
        }
        if (loggerTRACE.isDebugEnabled()) {// BLEX
            loggerTRACE.debug("EtapeCircuit etapeCourante : [" + (etapeCourante != null ? etapeCourante.getActionDemandee() : "nulle") + "]"); // BLEX
        }
        Assert.notNull(etapeCourante, "(APPROVE) Le dossier n'a pas d'étape courante: id = " + dossier);
        String actionDemandee = (etapeCourante != null ? etapeCourante.getActionDemandee() : null);
        if (actionDemandee == null) {
            logger.debug("Approve sur un vieux circuit... COMPATIBILITE ASCENDANTE !");
            approveBeforeV3(dossier);
        } else {
            // on positionne systématiquement le validator sur l'étape courante.
            setCurrentValidator(dossier, AuthenticationUtil.getRunAsUser());

            actionDemandee = actionDemandee.trim();
            loggerTRACE.debug("actionDemandee=" + actionDemandee); // BLEX
            // Vérification du "droit" à approuver
            if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_VISA)) {   // VISA: Durant le circuit, seul l'acteur courant peut viser; pas d'obligation de lecture
                Assert.isTrue(isActeurCourant(dossier, username), "Vous (" + username + ") n'êtes pas acteur courant de ce dossier!");
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {   // SIGNATURE:  vérification de la lecture des documents
                // Si la signature est papier, le secrétariat a la possibilité de confirmer l'approbation
                if (signaturePapier) {
                    Assert.isTrue(isActeurCourant(dossier, username) || isParapheurSecretaire(parapheurRef, username),
                            "Vous (" + username + ") n'êtes pas acteur courant de ce dossier, ni membre du secrétariat!");
                } else {
                    Assert.isTrue(isActeurCourant(dossier, username), "Vous (" + username + ") n'êtes pas acteur courant de ce dossier!");
                }
                // La lecture du premier document est nécessaire et suffisante
                if (isReadingMandatory(dossier)) {
                    Assert.isTrue(dossierService.hasReadDossier(dossier, username), "Le document à signer n'a pas été lu.");
                }
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_TDT)) {
                Assert.isTrue(isActeurCourant(dossier, username) || isParapheurSecretaire(parapheurRef, username),
                        "Vous (" + username + ") n'êtes pas acteur courant de ce dossier, ni membre du secrétariat!");
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC)) {
                Assert.isTrue(isActeurCourant(dossier, username));
                if (!nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_STATUS).equals(S2lowServiceImpl.STATUS_MAILSEC_FULLY_CONFIRMED)) {
                    try {
                        if (s2lowService.isMailSecJobCancelable(dossier)) {
                            s2lowService.cancelMailSecJob(dossier);
                        }
                    } catch (IOException exception) {
                        throw new RuntimeException("Impossible de récupérer le dossier envoyé par mail sécurisé", exception);
                    }
                }
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC_PASTELL)) {
                Assert.isTrue(isActeurCourant(dossier, username));
                if (!nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_STATUS).equals(S2lowServiceImpl.STATUS_MAILSEC_FULLY_CONFIRMED)) {
                    try {
                        mailsecPastellService.cancel(dossier);
                    } catch (IOException e) {
                        throw new RuntimeException("Impossible de récupérer le dossier envoyé par mail sécurisé", e);
                    }
                }

            } else if(actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_CACHET)) {
                Assert.isTrue(isActeurCourant(dossier, username) || isParapheurSecretaire(parapheurRef, username),
                        "Vous (" + username + ") n'êtes pas acteur courant de ce dossier, ni membre du secrétariat!");
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_DIFF_EMAIL)) {
                // TODO
                throw new RuntimeException("L'action demandée " + actionDemandee
                        + " est impossible dans ce contexte. Veuillez informer votre administrateur de ce problème, ou retourner ce dossier à son émetteur afin que celui-ci modifie le circuit de validation.");
            } else { //  (actionDemandee=ARCHIVAGE ou autre n'a pas sa place ici)
                throw new RuntimeException("L'action demandée " + actionDemandee
                        + " est inconnue. Veuillez informer votre administrateur de ce problème, ou retourner ce dossier à son émetteur afin que celui-ci modifie le circuit de validation.");
            }
            // Délégation
            boolean traiteParDelegation = false;
            if ((parapheurCourant != null) && !parapheurCourant.equals(parapheurRef) &&
                    !isParapheurOwner(parapheurRef, username) &&
                    isParapheurDelegate(parapheurRef, username)) {// Traiter la trace sur delegation: ASPECT "DELEGATION" sur étape courante

                logger.debug("APPROVE - Aspect DELEGATION set");
                Map<QName, Serializable> delegProps = new HashMap<QName, Serializable>();
                delegProps.put(ParapheurModel.PROP_DELEGUE, parapheurCourant);
                this.nodeService.addAspect(etapeCourante.getNodeRef(), ParapheurModel.ASPECT_ETAPE_DELEGATION, delegProps);
                traiteParDelegation = true;
            } else {
                logger.debug("APPROVE -   pas de délégation");
            }

            // DETERMINER LA _BANNETTE_ DE DESTINATION
            NodeRef corbeille;
            if (loggerTRACE.isDebugEnabled()) {// BLEX
                loggerTRACE.debug("EtapeCircuit etapeSuivante : [" + (etapeSuivante != null ? etapeSuivante.getActionDemandee() : "nulle!") + "]");  // BLEX
            }
            String annotation;
            if (etapeSuivante != null) {
                NodeRef parapheur = etapeSuivante.getParapheur();
                if (!this.nodeService.exists(parapheur)) {
                    throw new RuntimeException("Le bureau suivant n'a pu être déterminé. Veuillez retourner ce dossier à son émetteur afin que celui-ci modifie le circuit de validation.");
                }

                if (this.getParapheurOwners(parapheur) == null || this.getParapheurOwners(parapheur).isEmpty()) {
                    throw new RuntimeException("Le bureau suivant est actuellement sans propriétaire. Veuillez informer votre administrateur de ce problème, ou retourner ce dossier à son émetteur afin que celui-ci modifie le circuit de validation.");
                }

                logger.debug("APPROVE - Propriete PASSE_PAR set");
                nodeService.setProperty(((EtapeCircuitImpl) etapeSuivante).getNodeRef(), ParapheurModel.PROP_PASSE_PAR, parapheur);

                if (etapeSuivante.getActionDemandee().trim().equalsIgnoreCase(EtapeCircuit.ETAPE_ARCHIVAGE)) {
                    corbeille = getCorbeille(parapheur, ParapheurModel.NAME_A_ARCHIVER);
                } else {
                    corbeille = getCorbeille(parapheur, ParapheurModel.NAME_A_TRAITER);
                }
                if (loggerTRACE.isDebugEnabled()) {// BLEX
                    loggerTRACE.debug("corbeille de destination : [" + nodeService.getProperty(corbeille, ContentModel.PROP_NAME) + "]");
                }
                // AUDIT SUR ETAPE COURANTE
                annotation = (etapeCourante != null ? etapeCourante.getAnnotation() : null);
                if (annotation != null) {
                    annotation = annotation.trim();
                }
                String strStatus = "";
                String strMessage = "";
                if (!isEmis(dossier)) {
                    strStatus = StatusMetier.STATUS_NON_LU;
                    strMessage = "Emission du dossier";
                    // TODO: FIXME HOULALA: Attention ceci parait tres dangereux
                    srciService.onEmission(dossier); // BLEX
                } // VISA, SIGNATURE, TDT, DIFF_EMAIL, (pas ARCHIVAGE)
                else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_VISA)) {
                    strStatus = StatusMetier.STATUS_VISE;
                    strMessage = "Visa sur dossier";
                    if (traiteParDelegation) {
                        strMessage += " (" + String.format(configuration.getProperty("parapheur.suppleance.text"), "", getNomParapheur(parapheurRef)) + ")";
                    }
                } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {
                    strStatus = StatusMetier.STATUS_SIGNE;
                    strMessage = "Signature sur dossier";
                    if (traiteParDelegation) {
                        strMessage += " (" + String.format(configuration.getProperty("parapheur.suppleance.text"), "", getNomParapheur(parapheurRef)) + ")";
                    }
                } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_TDT)
                        || actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_DIFF_EMAIL)) {
                    strStatus = StatusMetier.STATUS_TRANSMISSION_OK;
                    strMessage = "Dossier télétransmis";
                } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC)) {
                    strStatus = StatusMetier.STATUS_MAILSEC_OK;
                    strMessage = "Accusés de réception reçus";
                } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC_PASTELL)) {
                    strStatus = StatusMetier.STATUS_OK_MAILSEC_PASTELL;
                    strMessage = "Accusés de réception reçus";
                } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_CACHET)) {
                    strStatus = StatusMetier.STATUS_CACHET_OK;
                    strMessage = "Cachet serveur";
                }
                if (loggerTRACE.isDebugEnabled()) {// BLEX
                    loggerTRACE.debug("nouveau statut : [" + strStatus + "]");  // BLEX
                }

                List<Object> list = new ArrayList<Object>();
                list.add(strStatus);
                if (annotation != null && !annotation.trim().isEmpty()) {
                    auditWithNewBackend("ParapheurServiceCompat", annotation, dossier, list);
                } else {
                    auditWithNewBackend("ParapheurServiceCompat", strMessage, dossier, list);
                }


            } else {   // ERREUR !!!!! : on ne doit pas arriver ici!!
                logger.error("APPROVE - Etape suivante=null !?! Renvoi à Archiver chez l'émetteur...");
                NodeRef emetteur = getEmetteur(dossier);
                if (!this.nodeService.exists(emetteur)) {
                    throw new RuntimeException("Le dossier n'a pas d'émetteur: id = " + dossier);
                }
                if (this.getParapheurOwners(emetteur) == null || this.getParapheurOwners(emetteur).isEmpty()) {
                    throw new RuntimeException("Le bureau émetteur est actuellement sans propriétaire. Veuillez informer votre administrateur de ce problème.");
                }
                corbeille = getCorbeille(emetteur, ParapheurModel.NAME_A_ARCHIVER);

                nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_SIGNE);

                annotation = (etapeCourante != null ? etapeCourante.getAnnotation() : null);
                List<Object> list = new ArrayList<Object>();
                list.add(StatusMetier.STATUS_SIGNE);
                if (annotation == null || annotation.trim().isEmpty()) {
                    auditWithNewBackend("ParapheurServiceCompat", "Signature du dossier", dossier, list);
                } else {
                    auditWithNewBackend("ParapheurServiceCompat", annotation, dossier, list);
                }


            }
            NodeRef parapheurDest = getParentParapheur(corbeille);
            // On a besoin de savoir si le dossier vient d'être émis pour l'envoi de mail
            boolean emis = isEmis(dossier);
            if (loggerTRACE.isDebugEnabled()) {// BLEX
                loggerTRACE.debug("etat du dossier : isEmis=" + emis); // BLEX
            }

            // Mise à jour des informations d'étape
            logger.debug("APPROVE - Propriete EFFECTUEE set");
            nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_EFFECTUEE, Boolean.TRUE);
            logger.debug("APPROVE - Propriete DATE_VALIDATION set");
            nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_DATE_VALIDATION, new Date());
            // Tant que le document principal n'a pas été lu, le dossier est "récupérable", hors signature!
            if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("APPROVE - Propriete RECUPERABLE set = FALSE car signature.");
                }
                nodeService.setProperty(dossier, ParapheurModel.PROP_RECUPERABLE, Boolean.FALSE);
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_CACHET)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("APPROVE - Propriete RECUPERABLE set = FALSE car cachet serveur.");
                }
                nodeService.setProperty(dossier, ParapheurModel.PROP_RECUPERABLE, Boolean.FALSE);
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("APPROVE parapheur- Propriete RECUPERABLE set = TRUE");
                }
                nodeService.setProperty(dossier, ParapheurModel.PROP_RECUPERABLE, Boolean.TRUE);
            }
            String strStatus = "";

            if (etapeSuivante == null) {
                logger.error("etapeSuivante NULL?!! Malgré tout: APPROVE - Propriete TERMINE set");
                nodeService.setProperty(dossier, ParapheurModel.PROP_TERMINE, Boolean.TRUE);
                if (signaturePapier) {
                    nodeService.setProperty(dossier, ParapheurModel.PROP_SIGNATURE, "Signature papier");
                }
            } else {
                logger.debug("APPROVE - Propriete STATUS_METIER set");
                String actDmdee = etapeSuivante.getActionDemandee().trim();
                String strMessage = "";
                if (actDmdee.equalsIgnoreCase(EtapeCircuit.ETAPE_VISA)) {
                    strStatus = StatusMetier.STATUS_PRET_VISA;
                    strMessage = "Dossier déposé sur le bureau " + this.getNomParapheur(parapheurDest) + " pour Visa";
                } else if (actDmdee.equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {
                    strStatus = StatusMetier.STATUS_NON_LU;
                    strMessage = "Dossier déposé sur le bureau " + this.getNomParapheur(parapheurDest) + " pour signature";
                    if (signaturePapier) {        // La signature n'est pas électronique
                        logger.debug("APPROVE - Propriete SIGNATURE set");
                        nodeService.setProperty(dossier, ParapheurModel.PROP_SIGNATURE, "Signature papier");
                    }
                } else if (actDmdee.equalsIgnoreCase(EtapeCircuit.ETAPE_TDT)
                        || actDmdee.equalsIgnoreCase(EtapeCircuit.ETAPE_DIFF_EMAIL)) {
                    strStatus = StatusMetier.STATUS_PRET_TDT;
                    strMessage = "Dossier diffusable";
                } else if (actDmdee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC)) {
                    strStatus = StatusMetier.STATUS_PRET_MAILSEC;
                    strMessage = "Dossier déposé sur le bureau " + this.getNomParapheur(parapheurDest) + " pour envoi par mail sécurisé.";

                } else if (actDmdee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC_PASTELL)) {
                    strStatus = StatusMetier.STATUS_PRET_MAILSEC_PASTELL;
                    strMessage = "Dossier déposé sur le bureau " + this.getNomParapheur(parapheurDest) + " pour envoi par mail sécurisé via Pastell.";

                } else if (actDmdee.equalsIgnoreCase(EtapeCircuit.ETAPE_CACHET)) {
                    strStatus = StatusMetier.STATUS_PRET_CACHET;
                    strMessage = "Dossier déposé sur le bureau " + this.getNomParapheur(parapheurDest) + " pour cachet serveur.";
                } else if (actDmdee.equalsIgnoreCase(EtapeCircuit.ETAPE_ARCHIVAGE)) {
                    logger.debug("APPROVE - Propriete TERMINE set");
                    nodeService.setProperty(dossier, ParapheurModel.PROP_TERMINE, Boolean.TRUE);
                    strStatus = StatusMetier.STATUS_ARCHIVE;
                    strMessage = "Circuit terminé, dossier archivable";

					/*
                     * AUDIT A REFAIRE !! // Création de l'objet auditFact
					 * AuditState auditInfo = createAuditFact("archive",
					 * getParentParapheur(dossier), dossier,
					 * nodeService.getProperties(dossier)); // Lancement de
					 * l'audit auditDAO.audit(auditInfo);
					 *
					 */
                }
                // Si il y a une attestation de signature && que le status est en archive, on n'audit pas
                if(!(typesService.hasToAttestSignature(
                        (String) nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER),
                        (String) nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER))
                        && strStatus.equals(StatusMetier.STATUS_ARCHIVE))) {

                    nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, strStatus);

                    List<Object> list = new ArrayList<Object>();
                    list.add(strStatus);
                    auditWithNewBackend("ParapheurServiceCompat", strMessage, dossier, list);
                }

            }
            logger.debug("APPROVE - Suppression de l'aspect LU pour tous les documents du dossier");
            dossierService.setDossierNonLu(dossier);
            // Remove secretariat aspect
            this.nodeService.removeAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT);

            // DEPLACEMENT du dossier vers la corbeille de destination
            ChildAssociationRef oldParentAssoc = nodeService.getPrimaryParent(dossier);
            Assert.notNull(oldParentAssoc, "Le dossier n'a pas de parent: id = " + dossier);

            if (logger.isDebugEnabled()) {
                logger.debug("APPROVE - Deplacement du noeud");
            }

            // unlock Dossier on approve.
            if (jobService.isBackgroundWorkEnabled()) {
                jobService.unlockNode(dossier);
            }


            try {
                nodeService.moveNode(dossier, corbeille, oldParentAssoc.getTypeQName(), oldParentAssoc.getQName());
                if (logger.isInfoEnabled()) {
                    logger.info("APPROVE : Dossier confié au bureau ["
                            + this.getNomParapheur(parapheurDest) + "]");
                }
            } catch (DuplicateChildNodeNameException ex) {
                if (logger.isEnabledFor(Level.WARN)) {
                    logger.warn("Impossible de transmettre le dossier : un dossier de même nom existe dans le bureau de destination");
                }
                throw new RuntimeException("Un dossier de même nom existe dans le bureau de destination", ex);
            } catch (org.hibernate.exception.ConstraintViolationException ex2) {
                if (logger.isEnabledFor(Level.WARN)) {
                    logger.warn("Impossible de transmettre le dossier : un dossier de même nom existe dans le bureau de destination");
                }
                throw new RuntimeException("Un dossier de même nom existe dans le bureau de destination", ex2);
            }

            // Notifications (Mails digest et unitaire)
            notificationService.notifierPourValidation(dossier, parapheurCourant);

        } // fin de vérif initiale sur la version d'implémentation de circuits


        logger.debug("APPROVE - Sortie de la methode");
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#approve(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void approve(NodeRef dossier) {
        NodeRef parapheurCourant = null;
        FacesContext fc = FacesContext.getCurrentInstance();
        if (fc != null) {
            Map session = fc.getExternalContext().getSessionMap();
            parapheurCourant = (NodeRef) session.get("CURRENT_PARAPHEUR");
        }
        this.approveV4(dossier, parapheurCourant);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getCurrentParapheur
     */
    @Override
    public NodeRef getCurrentParapheur() {
        NodeRef parapheurCourant = null;
        FacesContext fc = FacesContext.getCurrentInstance();
        if (fc != null) {
            Map session = fc.getExternalContext().getSessionMap();
            parapheurCourant = (NodeRef) session.get("CURRENT_PARAPHEUR");
        }
        return parapheurCourant;
    }

    /**
     * Cette méthode ne sert que pour la traiter de vieux dossiers initiés avant
     * le passage en v3, donc avec le vieux système de circuits.
     *
     * @param dossier
     */
    @Deprecated
    private void approveBeforeV3(NodeRef dossier) {
        logger.debug("approveBeforeV3 - Entree dans la methode");
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");
        String username = this.authenticationService.getCurrentUserName();
        NodeRef parapheurRef = getParentParapheur(dossier);
        Boolean signaturePapier = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_PAPIER);
        List<EtapeCircuit> circuit = getCircuit(dossier);
        Assert.notEmpty(circuit, "Le dossier n'a pas de circuit de validation: id = " + dossier);
        Assert.isTrue(circuit.size() > 1, "Impossible d'émettre le dossier, aucun circuit n'a été défini.");

        // Récupération de la prochaine étape
        EtapeCircuitImpl etapeCourante = null;
        EtapeCircuit etapeSuivante = null;
        for (EtapeCircuit etape : circuit) {
            if (!etape.isApproved()) {
                if (etapeCourante == null) {
                    etapeCourante = (EtapeCircuitImpl) etape;
                } else if (etapeSuivante == null) {
                    etapeSuivante = etape;
                    break;
                }
            }
        }
        Assert.notNull(etapeCourante, "(approveBeforeV3) Le dossier n'a pas d'étape courante: id = " + dossier);

        if (getCurrentValidator(dossier) == null) {
            /*
             * Positionnement du nom utilisateur dans l'étape courante pour
			 * recuperation par le trigger.
			 */
            setCurrentValidator(dossier, AuthenticationUtil.getRunAsUser());
        }

        // Vérification du "droit" à approuver (visa - signature)
        if (etapeSuivante == null) {   // SIGNATURE:  vérification de la lecture des documents
            // Si la signature est papier, le secrétariat a la possibilité de confirmer l'approbation
            if (signaturePapier) {
                Assert.isTrue(isActeurCourant(dossier, username) || isParapheurSecretaire(parapheurRef, username),
                        "Vous n'êtes pas acteur courant de ce dossier, ni membre du secrétariat!");
            } else {
                Assert.isTrue(isActeurCourant(dossier, username), "Vous n'êtes pas acteur courant de ce dossier!");
            }

            // Deprecated...
            List<NodeRef> lstDocs = getDocuments(dossier);
            // La lecture du premier document est suffisante
            /*
             * for (NodeRef doc : lstDocs)
			 * Assert.isTrue(nodeService.hasAspect(doc,
			 * ParapheurModel.ASPECT_LU), "Un ou plusieurs documents n'ont pas
			 * été lus");
			 */
            Assert.isTrue(nodeService.hasAspect(lstDocs.get(0), ParapheurModel.ASPECT_LU), "Le document à signer n'a pas été lu.");
        } else {   // VISA: Durant le circuit, seul l'acteur courant peut viser
            Assert.isTrue(isActeurCourant(dossier, username), "Vous n'êtes pas acteur courant de ce dossier!");
        }

        // DETERMINER LA CORBEILLE DE DESTINATION
        NodeRef corbeille;
        if (etapeSuivante != null) {
            NodeRef parapheur = etapeSuivante.getParapheur();
            if (!this.nodeService.exists(parapheur)) {
                throw new RuntimeException("Le bureau suivant n'a pu être déterminé. Veuillez retourner ce dossier à son émetteur afin que celui-ci modifie le circuit de validation.");
            }

			/* Le dossier ne bouge plus lors des délégations, on n'a donc plus
             * besoin de suivre les délégations.
			 */
            // parapheur = followDelegation(parapheur);
            if (this.getParapheurOwner(parapheur) == null || this.getParapheurOwner(parapheur).equals("")) {
                throw new RuntimeException("Le bureau suivant est actuellement sans propriétaire. Veuillez informer votre administrateur de ce problème, ou retourner ce dossier à son émetteur afin que celui-ci modifie le circuit de validation.");
            }

            logger.debug("approveBeforeV3 - Propriete PASSE_PAR set");
            nodeService.setProperty(((EtapeCircuitImpl) etapeSuivante).getNodeRef(), ParapheurModel.PROP_PASSE_PAR, parapheur);
            corbeille = getCorbeille(parapheur, ParapheurModel.NAME_A_TRAITER);

            List<Object> list = new ArrayList<Object>();
            list.add(StatusMetier.STATUS_NON_LU);
            nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_NON_LU);

            String annotation = etapeCourante.getAnnotation();
            if (annotation != null && !annotation.trim().isEmpty()) {
                auditWithNewBackend("ParapheurServiceCompat", annotation, dossier, list);
                //auditService.audit("ParapheurService", annotation, dossier, list);
            } else {
                if (isEmis(dossier)) {
                    auditWithNewBackend("ParapheurServiceCompat", "Visa sur dossier", dossier, list);
                } else {
                    auditWithNewBackend("ParapheurServiceCompat", "Emission du dossier", dossier, list);
                }
            }
        } else {
            NodeRef emetteur = getEmetteur(dossier);
            if (!this.nodeService.exists(emetteur)) {
                throw new RuntimeException("Le dossier n'a pas d'émetteur: id = " + dossier);
            }
            if (this.getParapheurOwner(emetteur) == null || this.getParapheurOwner(emetteur).equals("")) {
                throw new RuntimeException("Le parapheur émetteur est actuellement sans propriétaire. Veuillez informer votre administrateur de ce problème.");
            }
            corbeille = getCorbeille(emetteur, ParapheurModel.NAME_A_ARCHIVER);

            List<Object> list = new ArrayList<Object>();
            list.add(StatusMetier.STATUS_SIGNE);
            nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_SIGNE);

            String annotation = etapeCourante.getAnnotation();
            if (annotation == null || annotation.trim().isEmpty()) {
                auditWithNewBackend("ParapheurServiceCompat", "Signature du dossier", dossier, list);
            } else {
                auditWithNewBackend("ParapheurServiceCompat", annotation, dossier, list);
            }

        }

        // DEPLACEMENT du dossier vers la corbeille de destination
        ChildAssociationRef oldParentAssoc = nodeService.getPrimaryParent(dossier);
        Assert.notNull(oldParentAssoc, "Le dossier n'a pas de parent: id = " + dossier);
        logger.debug("approveBeforeV3 - Deplacement du noeud");
        try {
            nodeService.moveNode(dossier, corbeille, oldParentAssoc.getTypeQName(), oldParentAssoc.getQName());
        } catch (DuplicateChildNodeNameException ex) {
            if (logger.isEnabledFor(Level.WARN)) {
                logger.warn("Impossible de transmettre le dossier : un dossier de même nom existe dans le bureau de destination");
            }
            throw new RuntimeException("Un dossier de même nom existe dans le bureau de destination", ex);
        }

        // On a besoin de savoir si le dossier vient d'être émis pour l'envoi de mail
        boolean emis = isEmis(dossier);

        // Mise à jour des informations d'étape
        logger.debug("approveBeforeV3 - Propriete EFFECTUEE set");
        nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_EFFECTUEE, Boolean.TRUE);
        logger.debug("approveBeforeV3 - Propriete DATE_VALIDATION set");
        nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_DATE_VALIDATION, new Date());

        // Tant que le document principal n'a pas été lu, le dossier est "récupérable"
        logger.debug("approveBeforeV3 - Propriete RECUPERABLE set");
        nodeService.setProperty(dossier, ParapheurModel.PROP_RECUPERABLE, Boolean.TRUE);

        if (etapeSuivante == null) {   // Il s'agissait d'une étape de signature
            logger.debug("approveBeforeV3 - Propriete TERMINE set");
            nodeService.setProperty(dossier, ParapheurModel.PROP_TERMINE, Boolean.TRUE);
            if (signaturePapier) {        // La signature n'est pas électronique
                logger.debug("approveBeforeV3 - Propriete SIGNATURE set");
                nodeService.setProperty(dossier, ParapheurModel.PROP_SIGNATURE, "Signature papier");
            }
        }

        // NOTIFICATIONS: Envoi de mails
        try {
            if (emis) {   // Progression d'un dossier connu
                mail("diff", "parapheur-diffusion-visa.ftl", dossier);
                if (etapeSuivante == null) {   // Le dossier vient de finir son circuit
                    mail("owner", "parapheur-owner-archivage.ftl", dossier);
                    mail("tiers", "parapheur-tiers.ftl", dossier);
                    // } else {
                    // Suppression du mail à l'émetteur pour chaque étape
                    // mail("owner", "parapheur-owner-reception.ftl", dossier);
                }
            } else {   // C'est un nouveau dossier
                mail("diff", "parapheur-diffusion-emission.ftl", dossier);
            }

            // Dans tous les cas, on prévient l'acteur courant
            mail("current", "parapheur-current-reception.ftl", dossier);

            if (signaturePapier && etapeSuivante != null) {   // le dossier est à signer physiquement et le prochain acteur est signataire: on prévient son secrétariat
                NodeRef etapeRef = ((EtapeCircuitImpl) etapeSuivante).getNodeRef();
                List<ChildAssociationRef> childEtapes = nodeService.getChildAssocs(etapeRef);
                if (childEtapes.isEmpty()) {
                    mail("secretariat", "parapheur-secretariat-signature.ftl", dossier);
                }
            }
        } catch (Exception e) {
            logger.warn("Une erreur est survenue lors d'un envoi de mail : " + e.getMessage());
        }
        logger.debug("approveBeforeV3 - Sortie de la methode");
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#auditTransmissionTDT(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String, java.lang.String)
     */
    @Override
    public void auditTransmissionTDT(NodeRef dossier, String status, String message) {
        if (logger.isDebugEnabled()) {
            logger.debug("auditTransmissionTDT: " + status + ", " + message);
        }
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        // TODO  auditer le fait qu'il y a une transmission en cours!
        EtapeCircuit etape = this.getCurrentEtapeCircuit(dossier);
        if (isTermine(dossier)) {
            // ça se complique... on n'est pas censé arriver là
            logger.warn("TRANSMISSION EN COURS sur Dossier Terminé???");
        } else if (!isEmis(dossier)) {
            // ça se complique... on n'est pas censé arriver là
            logger.warn("TRANSMISSION EN COURS sur Dossier pas émis ????");
        } else {
            // AUDIT SUR ETAPE COURANTE: TDT, DIFF_EMAIL, (voire ARCHIVAGE)
            String annotation = etape.getAnnotation();
            String actionDemandee = etape.getActionDemandee();
            if (annotation != null) {
                annotation = annotation.trim();
            }
            String strStatus = "";
            String strMessage = "";
            if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_TDT)
                    || actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_DIFF_EMAIL)) {
                strStatus = status;
                strMessage = message;
            }
            if (!strStatus.isEmpty()) {
                nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, strStatus);
            }

            List<Object> list = new ArrayList<Object>();
            list.add(strStatus);
            if (annotation != null && !annotation.trim().isEmpty()) {
                auditWithNewBackend("ParapheurServiceCompat", annotation, dossier, list);
            } else {
                auditWithNewBackend("ParapheurServiceCompat", strMessage, dossier, list);
            }
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#auditAndMailAboutDossier(org.alfresco.service.cmr.repository.NodeRef,
     * java.lang.String, java.lang.String)
     */
    @Override
    public void auditAndMailAboutDossier(NodeRef dossier, String action, String message) throws IllegalArgumentException {
        logger.debug("Entree dans la methode, action=[" + action + "], message=" + message);
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");
        String nomDossier = (String) nodeService.getProperty(dossier, ContentModel.PROP_NAME);

        if ("EFFACER".equals(action)) {

            TdtState state = getTdtState(dossier, false);
            notificationService.notifierPourSuppressionAdmin(dossier, state.getStatus(), state.getAckDate());
            /*
             * Trace d'audit, comme promis
			 */
            List<Object> list = new ArrayList<Object>();
            list.add("SuppressionDossier");
            list.add(nomDossier);
            auditWithNewBackend("ParapheurServiceCompat", "Suppression du dossier", dossier, list);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#reject(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void reject(NodeRef dossier) {
        logger.debug("REJECT - Entree dans la methode");
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        Assert.hasText(this.getAnnotationPublique(dossier), "Une annotation publique est obligatoire lors d'un rejet");

        ChildAssociationRef oldParentAssoc = nodeService.getPrimaryParent(dossier);
        Assert.notNull(oldParentAssoc, "Le dossier n'a pas de parent: id = " + dossier);

        String username = AuthenticationUtil.getRunAsUser();
        Assert.isTrue(isActeurCourant(dossier, username), "Vous n'êtes pas acteur courant de ce dossier!");

        // On récupère la corbeille "dossiers retournés" de la première étape
        NodeRef emetteur = getEmetteur(dossier);
        Assert.notNull(emetteur, "Le dossier n'a pas d'émetteur: id = " + dossier);
        Assert.notNull(this.getParapheurOwners(emetteur),
                "Le bureau émetteur est actuellement sans propriétaire. Veuillez informer votre administrateur de ce problème.");
        Assert.notEmpty(this.getParapheurOwners(emetteur),
                "Le bureau émetteur est actuellement sans propriétaire. Veuillez informer votre administrateur de ce problème.");

        EtapeCircuitImpl etapeCourante = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
        EtapeCircuit premiereEtape = getCircuit(dossier).get(0);
        Assert.isTrue(premiereEtape.isApproved(), "Impossible de rejeter le dossier, il n'a pas été émis.");
        Assert.isTrue(!nodeService.hasAspect(etapeCourante.getNodeRef(), ParapheurModel.ASPECT_ETAPE_COMPLEMENTAIRE), "Impossible de rejeter un dossier lors d'une étape complémentaire.");

        setCurrentValidator(dossier, AuthenticationUtil.getRunAsUser());
        nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_EFFECTUEE, Boolean.TRUE);
        nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_DATE_VALIDATION, new Date());
        NodeRef corbeille = getCorbeille(emetteur, ParapheurModel.NAME_RETOURNES);

        // AUDIT
        String actionDemandee = etapeCourante.getActionDemandee();
        String annotation;
        if (actionDemandee == null) {   // compatibilité ascendante
            nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_REJET_SIGNATAIRE);

            List<Object> list = new ArrayList<Object>();
            list.add(StatusMetier.STATUS_REJET_SIGNATAIRE);
            annotation = etapeCourante.getAnnotation();
            if (annotation == null || annotation.trim().isEmpty()) {
                auditWithNewBackend("ParapheurServiceCompat", "Rejet du dossier", dossier, list);
            } else {
                auditWithNewBackend("ParapheurServiceCompat", annotation, dossier, list);
            }
        } else {
            actionDemandee = actionDemandee.trim();
            annotation = etapeCourante.getAnnotation();
            if (annotation != null) {
                annotation = annotation.trim();
            }
            String strStatus = "";
            String strMessage = "";
            if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_VISA)) {
                strStatus = StatusMetier.STATUS_REJET_VISA;
                strMessage = "Dossier rejeté";
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {
                strStatus = StatusMetier.STATUS_REJET_SIGNATAIRE;
                strMessage = "Dossier rejeté";
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_CACHET)) {
                strStatus = StatusMetier.STATUS_REJET_CACHET;
                strMessage = "Dossier rejeté";
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC)) {
                strStatus = StatusMetier.STATUS_REJET_MAILSEC;
                strMessage = "Dossier rejeté";
                try {
                    annotation += "\n" + s2lowService.cancelMailSecJob(dossier);
                    Integer mailId = (Integer) nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID);
                    s2lowService.deleteSecureMail(mailId);
                } catch (IOException exception) {
                    throw new RuntimeException("Impossible de rejeter le dossier envoyé par mail sécurisé", exception);
                }
                nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_ANNOTATION, annotation);
                nodeService.removeProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID);
                nodeService.removeProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_STATUS);
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC_PASTELL)) {
                strStatus = StatusMetier.STATUS_REJET_MAILSEC_PASTELL;
                strMessage = "Dossier rejeté";

                // TODO-PASTELL Annuler le dossier sur le connecteur PASTELL
                nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_ANNOTATION, annotation);
                nodeService.removeProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID);
                nodeService.removeProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_STATUS);
            } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_TDT)
                    || actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_DIFF_EMAIL)) {
                strStatus = StatusMetier.STATUS_REJET_TRANSMISSION;
                strMessage = "Dossier rejeté";
                String nackReason = getNackMotif(dossier);
                if (nackReason != null && !nackReason.trim().isEmpty()) {
                    strMessage = nackReason;
                }
            }
            nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, strStatus);

            List<Object> list = new ArrayList<Object>();
            list.add(strStatus);
            if (annotation != null && !annotation.trim().isEmpty()) {
                auditWithNewBackend("ParapheurServiceCompat", annotation, dossier, list);
            } else {
                auditWithNewBackend("ParapheurServiceCompat", strMessage, dossier, list);
            }
        }

        // On déplace le dossier vers la corbeille
        logger.debug("REJECT - Deplacement du noeud");
        try {
            nodeService.moveNode(dossier, corbeille, oldParentAssoc.getTypeQName(), oldParentAssoc.getQName());
        } catch (DuplicateChildNodeNameException ex) {
            if (logger.isEnabledFor(Level.WARN)) {
                logger.warn("Impossible de transmettre le dossier : un dossier de même nom existe dans le bureau de destination");
            }
            throw new RuntimeException("Un dossier de même nom existe dans le bureau de destination", ex);
        }
        logger.debug("REJECT - Propriete TERMINE set");
        nodeService.setProperty(dossier, ParapheurModel.PROP_TERMINE, Boolean.TRUE);

        // On retire l'aspect "lu" de tous les documents du dossier
        logger.debug("REJECT - Suppression de l'aspect LU pour tous les documents");
        dossierService.setDossierNonLu(dossier);

        logger.debug("REJECT - Notification");

        notificationService.notifierPourRejet(dossier);

        logger.debug("REJECT - Sortie de la methode");
    }

    private String getNackMotif(NodeRef dossier) {
        try {
            // BLEX
            QName property;
            if (this.nodeService.hasAspect(dossier, SrciService.K.aspect_srciTransaction)) {
                property = SrciService.K.property_srciTransaction_nackHeliosXml;
            } else {
                property = ParapheurModel.PROP_NACKHELIOS_XML;
            }
            ContentReader reader = contentService.getReader(dossier, property);

            if (reader == null) {
                return null;
            }

            InputSource nack = new InputSource(reader.getContentInputStream());

            DOMParser parser = new DOMParser();
            parser.parse(nack);
            org.w3c.dom.Document doc = parser.getDocument();
            Node node = XPathAPI.selectSingleNode(doc, "/n:PES_NonAcquit/NonAcquit/Motif/@V", doc);

            return node.getTextContent();
        } catch (Exception ex) {
            logger.warn(ex);
            return null;
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#reprendreDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public NodeRef reprendreDossier(NodeRef dossier) {
        logger.debug("REPRENDRE - Entree dans la methode");
        Assert.isTrue(isDossier(dossier), "Node Ref doit être un ph:dossier");

        ChildAssociationRef oldParentAssoc = nodeService.getPrimaryParent(dossier);
        Assert.notNull(oldParentAssoc, "Le dossier n'a pas de parent: id = " + dossier);

        // On récupère la corbeille "dossiers à transmettre" dans le parapheur de la première étape
        NodeRef emetteur = getEmetteur(dossier);
        Assert.notNull(emetteur, "Le dossier n'a pas d'émetteur: id = " + dossier);

        // On déplace le dossier vers la corbeille
        NodeRef corbeille = getCorbeille(emetteur, ParapheurModel.NAME_EN_PREPARATION);
        Assert.state(corbeille != null, "Le bureau d'origine du dossier ne contient pas de corbeille \"à transmettre\" : id = " + dossier);
        logger.debug("REPRENDRE - Deplacement du noeud");
        try {
            nodeService.moveNode(dossier, corbeille, oldParentAssoc.getTypeQName(), oldParentAssoc.getQName());
            nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_NON_LU);

            List<Object> list = new ArrayList<Object>();
            list.add(StatusMetier.STATUS_NON_LU);
            auditWithNewBackend("ParapheurServiceCompat", "Reprise du dossier", dossier, list);
        } catch (DuplicateChildNodeNameException ex) {
            if (logger.isEnabledFor(Level.WARN)) {
                logger.warn("Impossible de reprendre le dossier : un dossier de même nom existe dans la corbeille de destination");
            }
            throw new RuntimeException("Un dossier de même nom existe parmi les dossiers en préparation", ex);
        }
        logger.debug("REPRENDRE - Propriete TERMINE set");
        nodeService.setProperty(dossier, ParapheurModel.PROP_TERMINE, Boolean.FALSE);
        /**
         * RàZ de toutes les étapes...
         */
        String annotations = "";
        for (EtapeCircuit etape : getCircuit(dossier)) {
            // récupération des annotations
            if ((Boolean) nodeService.getProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_EFFECTUEE)) {
                String annotation = (String) nodeService.getProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_ANNOTATION);
                if ((annotation != null) && (!annotation.isEmpty())) {
                    annotations += etape.getParapheurName();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy 'à' HH:mm", Locale.FRENCH);
                    annotations += " (" + sdf.format(nodeService.getProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_DATE_VALIDATION)) + ") :\n";
                    annotations += annotation;
                    annotations += "\n";
                }
            }
            logger.debug("REPRENDRE - Modification des proprietes de l'etape " + etape.getParapheurName());
            logger.debug("REPRENDRE - Propriete EFFECTUEE set");
            nodeService.setProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_EFFECTUEE, Boolean.FALSE);
            logger.debug("REPRENDRE - Propriete ANNOTATION set");
            nodeService.setProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_ANNOTATION, "");
            logger.debug("REPRENDRE - Propriete ANNOTATION_PRIVEE set");
            nodeService.setProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_ANNOTATION_PRIVEE, "");
            logger.debug("REPRENDRE - Propriete DATE_VALIDATION set");
            nodeService.setProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_DATE_VALIDATION, null);
            logger.debug("REPRENDRE - Propriete SIGNATURE_ETAPE set");
            nodeService.setProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_SIGNATURE_ETAPE, null);
            logger.debug("REPRENDRE - Propriete SIGNATURE set");
            nodeService.setProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_SIGNATURE, null);
            logger.debug("REPRENDRE - Aspect DELEGATION remove");
            if (nodeService.hasAspect(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.ASPECT_ETAPE_DELEGATION)) {
                //nodeService.setProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_PASSE_PAR, ((EtapeCircuitImpl) etape).getParapheur()); // FIXME
                nodeService.removeAspect(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.ASPECT_ETAPE_DELEGATION);
            }

            nodeService.setProperty(etape.getNodeRef(), ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES, null);
            nodeService.setProperty(etape.getNodeRef(), ParapheurModel.PROP_ANNOTATIONS_GRAPHIQUES_MULTIDOC, null);
            nodeService.setProperty(etape.getNodeRef(), ParapheurModel.PROP_VALIDATOR, null);
        }
        /**
         * RàZ des proprietes superflues...
         */
        if (nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID) != null) {
            nodeService.removeProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID);
        }
        if (nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_STATUS) != null) {
            nodeService.removeProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_STATUS);
        }

        /**
         * RàZ des propriétés des documents
         */
        logger.debug("REPRENDRE - Ajout de l'aspect LU a tous les documents");
        List<NodeRef> docs = getMainDocuments(dossier);
        docs.addAll(getAttachments(dossier));
        for (NodeRef doc : docs) {
            nodeService.setProperty(doc, ParapheurModel.PROP_DOCUMENT_DELETABLE, getCurrentEtapeCircuit(dossier).getNodeRef());
            dossierService.setDocumentLu(dossier, AuthenticationUtil.getRunAsUser());
        }

        logger.debug("REPRENDRE - Ajout des annotations des étapes avant la reprise à la première étape");
        nodeService.setProperty(getFirstEtape(dossier).getNodeRef(), ParapheurModel.PROP_ANNOTATION, annotations);

        logger.debug("REPRENDRE - Suppression des étapes non natives");
        /* Si le circuit du dossier contenait des étapes non natives (demande d'avis,
         * change signataire, ...), on supprime ces étapes. Ainsi celles-ci ne
		 * réapparaîtront pas dans le circuit.
		 */
        removeNonNativesStepsOf(dossier);

        notificationService.notifierPourRAZ(dossier);

        logger.debug("REPRENDRE - Sortie la methode");
        return dossier;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#recupererDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void recupererDossier(NodeRef dossier) {
        logger.debug("RECUPERER - Entree dans la methode");
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");
        //Assert.isTrue(!isTermine(dossier), "Le dossier est \"terminé\": id = " + dossier);
        Assert.isTrue(isEmis(dossier), "Le dossier n'a pas été émis: id = " + dossier);
        Assert.isTrue(isRecuperable(dossier), "Le dossier n'est pas récupérable: id = " + dossier);

        List<EtapeCircuit> circuit = getCircuit(dossier);
        EtapeCircuitImpl etapeCourante = null;
        EtapeCircuitImpl etapePrecedente = null;
        for (EtapeCircuit etape : circuit) {
            EtapeCircuitImpl etapeI = (EtapeCircuitImpl) etape;
            if (etapeI.isApproved()) {
                etapePrecedente = etapeI;
            } else {
                etapeCourante = etapeI;
                break;
            }
        }
        Assert.notNull(etapeCourante, "Le dossier n'a pas d'étape courante: id = " + dossier);
        // Cas particulier : remord après envoi mailsec (le dossier ne bouge pas)
        if ((etapeCourante.getActionDemandee() != null) && etapeCourante.getActionDemandee().trim().equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC)) {
            try {
                s2lowService.cancelMailSecJob(dossier);
                Integer mailId = (Integer) nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID);
                s2lowService.deleteSecureMail(mailId);
            } catch (IOException exception) {
                throw new RuntimeException("Impossible de récupérer le dossier envoyé par mail sécurisé", exception);
            }
            nodeService.removeProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID);
            nodeService.removeProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_STATUS);
            nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_PRET_MAILSEC);
            setSignataire(dossier, null);

            List<Object> list = new ArrayList<Object>();
            list.add(StatusMetier.STATUS_PRET_MAILSEC);
            auditWithNewBackend("ParapheurServiceCompat", "Envoi par mail sécurisé annulé", dossier, list);
        } else if ((etapeCourante.getActionDemandee() != null) && etapeCourante.getActionDemandee().trim().equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC_PASTELL)) {
            try {
                mailsecPastellService.revert(dossier);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Assert.notNull(etapePrecedente, "Le dossier n'a pas d'étape précédente: id = " + dossier);
            Assert.isTrue(isParapheurOwnerOrDelegate(etapePrecedente.getParapheur(), AuthenticationUtil.getRunAsUser()),
                    "Vous n'êtes pas propriétaire ou délégué du bureau de l'étape précédente");

            // On renvoie le dossier vers la corbeille de l'étape précédente
            ChildAssociationRef oldParentAssoc = nodeService.getPrimaryParent(dossier);
            Assert.notNull(oldParentAssoc, "Le dossier n'a pas de parent: id = " + dossier);

            NodeRef corbeille;
            // Si il s'agissait d'une émission, on renvoie dans la corbeille "à transmettre", sinon "à traiter"
            if (etapePrecedente.getNodeRef().equals(((EtapeCircuitImpl) circuit.get(0)).getNodeRef())) {
                corbeille = getCorbeille(etapePrecedente.getParapheur(), ParapheurModel.NAME_EN_PREPARATION);
            } else {
                corbeille = getCorbeille(etapePrecedente.getParapheur(), ParapheurModel.NAME_A_TRAITER);
            }

            // On met à jour les informations d'étape précédente
            logger.debug("RECUPERER - Propriete EFFECTUEE set");
            nodeService.setProperty(etapePrecedente.getNodeRef(), ParapheurModel.PROP_EFFECTUEE, Boolean.FALSE);

            logger.debug("RECUPERER - Deplacement du noeud");
            try {
                nodeService.moveNode(dossier, corbeille, oldParentAssoc.getTypeQName(), oldParentAssoc.getQName());
            } catch (DuplicateChildNodeNameException ex) {
                if (logger.isEnabledFor(Level.WARN)) {
                    logger.warn("Impossible de transmettre le dossier : un dossier de même nom existe dans la corbeille de destination");
                }
                throw new RuntimeException("Un dossier de même nom existe dans la corbeille de destination", ex);
            }

            logger.debug("RECUPERER - Aspect DELEGATION remove");
            if (nodeService.hasAspect(((EtapeCircuitImpl) etapePrecedente).getNodeRef(), ParapheurModel.ASPECT_ETAPE_DELEGATION)) {
                //nodeService.setProperty(((EtapeCircuitImpl) etapeCourante).getNodeRef(), ParapheurModel.PROP_PASSE_PAR, ((EtapeCircuitImpl) etapeCourante).getParapheur());
                nodeService.removeAspect(((EtapeCircuitImpl) etapePrecedente).getNodeRef(), ParapheurModel.ASPECT_ETAPE_DELEGATION);
            }

            // AUDIT
            String actionDemandee = null;

            if (nodeService.hasAspect(etapeCourante.getNodeRef(), ParapheurModel.ASPECT_ETAPE_NON_NATIVE)) {
                if (nodeService.hasAspect(etapeCourante.getNodeRef(), ParapheurModel.ASPECT_ETAPE_COMPLEMENTAIRE)) {
                    EtapeCircuit etapeSuivante = new EtapeCircuitImpl(nodeService, getFirstChild(etapeCourante.getNodeRef(), ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE));
                    actionDemandee = etapeSuivante.getActionDemandee();
                } else if (EtapeCircuit.ETAPE_SIGNATURE.equals(etapeCourante.getActionDemandee())) { // étape après change signataire
                    actionDemandee = etapeCourante.getActionDemandee();
                }
            } else {
                actionDemandee = etapePrecedente.getActionDemandee();
            }

            if (actionDemandee == null) {   // compatibilité ascendante
                nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_NON_LU);

                List<Object> list = new ArrayList<Object>();
                list.add(StatusMetier.STATUS_NON_LU);
                auditWithNewBackend("ParapheurServiceCompat", "Récupération du dossier", dossier, list);

            } else {
                actionDemandee = actionDemandee.trim();
                String annotation = etapeCourante.getAnnotation();
                if (annotation != null) {
                    annotation = annotation.trim();
                }
                String strStatus = "";
                String strMessage = "";
                if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_VISA)) {
                    strStatus = StatusMetier.STATUS_PRET_VISA;
                    strMessage = "Récupération du dossier";
                } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {
                    strStatus = StatusMetier.STATUS_NON_LU;
                    strMessage = "Récupération du dossier";
                } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC)) {
                    strStatus = StatusMetier.STATUS_PRET_MAILSEC;
                    strMessage = "Récupération du dossier";
                } else if (actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_TDT)
                        || actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_DIFF_EMAIL)) {
                    strStatus = StatusMetier.STATUS_PRET_TDT;
                    strMessage = "Récupération du dossier";
                }
                nodeService.setProperty(dossier, ParapheurModel.PROP_TERMINE, false);
                nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, strStatus);

                List<Object> list = new ArrayList<Object>();
                list.add(strStatus);
                if (annotation != null && !annotation.trim().isEmpty()) {
                    auditWithNewBackend("ParapheurServiceCompat", annotation, dossier, list);
                } else {
                    // BLEX auditWithNewBackend("ParapheurServiceCompat", "Signature du dossier", dossier, list);
                    // BLEX : autant mettre le message qu'on s'est fatigue a calculer ...
                    auditWithNewBackend("ParapheurServiceCompat", strMessage, dossier, list);
                }
            }

            // En revanche, le dossier n'est plus récupérable
            logger.debug("RECUPERER - Propriete RECUPERABLE set");
            nodeService.setProperty(dossier, ParapheurModel.PROP_RECUPERABLE, Boolean.FALSE);

            // Le dossier ayant déjà été lu une fois, on ajoute l'aspect aux documents
            logger.debug("RECUPERER - Ajout de l'aspect LU a tous les documents");
            dossierService.setDossierLu(dossier, AuthenticationUtil.getRunAsUser());

            // Notifications à faire avant la suppression d'étapes non natives

            notificationService.notifierPourRemord(dossier);

			/* Si l'étape courante était une étape non native (demande d'avis,
             * change signataire, ...), on supprime cette étape. Utile si
			 * l'utilisateur qui a rajouté l'étape a fait une erreur.
			 */
            if (nodeService.hasAspect(etapeCourante.getNodeRef(), ParapheurModel.ASPECT_ETAPE_NON_NATIVE)) {
                logger.debug("RECUPERER - suppression des étapes non natives");

                // Si c'est une étape complémentaire, il faut aussi effacer l'étape précédente
                if (nodeService.hasAspect(etapeCourante.getNodeRef(), ParapheurModel.ASPECT_ETAPE_COMPLEMENTAIRE)) {
                    removeEtape(etapePrecedente.getNodeRef());
                }
                removeEtape(etapeCourante.getNodeRef());
            }

        }
        logger.debug("RECUPERER - Sortie de la methode");
    }
    /*
     private void addElementIntoPDF(NodeRef nodeRef, PdfContentByte contentPDF, BaseFont font,
	 Map<Integer, NodeRef> userSignature, boolean preview) {
	 // Teste du type de donnees a ajouter
	 QName typeElement = nodeService.getType(nodeRef);
	 int coordonneeX = Integer.parseInt(nodeService.getProperty(nodeRef, CalqueModel.PROP_COORDONNEE_X)
	 .toString());
	 int coordonneeY = Integer.parseInt(nodeService.getProperty(nodeRef, CalqueModel.PROP_COORDONNEE_Y)
	 .toString());
	 if (typeElement.equals(CalqueModel.TYPE_COMMENTAIRE) || typeElement.equals(CalqueModel.TYPE_METADATA)) {
	 // Ajout d'elements de type texte
	 contentPDF.beginText();
	 contentPDF.setFontAndSize(font, 26);
	 contentPDF.setTextMatrix(coordonneeX, coordonneeY);
	 
	 String textToAdd = "";
	 if (typeElement.equals(CalqueModel.TYPE_COMMENTAIRE)) {
	 // Ajout du commentaire
	 textToAdd = nodeService.getProperty(nodeRef, CalqueModel.PROP_TEXTE).toString();
	 } else if (typeElement.equals(CalqueModel.TYPE_METADATA)) {
	 // Ajout de la meta-donnee
	 if (preview) {
	 textToAdd = "Ajout de la valeur de la méta-donnée ici.";
	 } else {
	 // TODO : recuperer la valeur de propriete
	 textToAdd = "Valeur de la méta-donnée";
	 // NodeRef targetNode = ? //faut savoir de quelle meata-donnees on parle, du dossier ? fichier ?
	 // autre chose ?
	 // QName metadata = QName.createQName(getNodeService().getProperty(nodeRef,
	 // CalqueModel.PROP_QNAME_MD).toString());
	 // textToAdd = getNodeService().getProperty(targetNode, metadata).toString();
	 }
	 
	 }
	 // Ajout du texte dans le PDF
	 contentPDF.showText(textToAdd);
	 contentPDF.endText();
	 } else if (typeElement.equals(CalqueModel.TYPE_IMAGE) || (typeElement.equals(CalqueModel.TYPE_SIGNATURE))) {
	 // Ajout d'elements de type image
	 ContentReader imageReader = null;
	 String nomImage = "";
	 
	 if (typeElement.equals(CalqueModel.TYPE_IMAGE)) {
	 // Ajout d'une image
	 imageReader = contentService.getReader(nodeRef, CalqueModel.PROP_CONTENT_IMAGE);
	 nomImage = nodeService.getProperty(nodeRef, CalqueModel.PROP_NOM_IMAGE).toString();
	 } else if (typeElement.equals(CalqueModel.TYPE_SIGNATURE)) {
	 // Ajout d'une signature
	 
	 
	 // Parcourir la liste des userSignatures pour prendre la signature correspondant au rang de l'element
	 NodeRef nodeUser = userSignature.get((Integer)nodeService.getProperty(nodeRef, CalqueModel.PROP_RANG));
	 if (nodeUser != null) {
	 imageReader = contentService.getReader(nodeUser, ParapheurModel.PROP_USER_SIGNATURE);
	 nomImage = nodeService.getProperty(nodeUser, ContentModel.PROP_USERNAME).toString();
	 }
	 else {
	 logger.warn("Pas de signature scannée associée avec l'utilisateur");
	 }
	 
	 
	 }
	 
	 try {
	 if (!nomImage.equals("") && imageReader != null) {
	 // Creer un fichier temporaire de l'image
	 File imageFile = new File(nomImage);
	 imageReader.getContent(imageFile);
	 // Recuperer l'image
	 Image image = Image.getInstance(nomImage);
	 image.setAbsolutePosition(coordonneeX, coordonneeY);
	 // Ajout de l'image dans le PDF
	 contentPDF.addImage(image);
	 // Supprimer le fichier temporaire
	 imageFile.delete();
	 }
	 
	 } catch (BadElementException elementException) {
	 logger.error("Erreur lors de la lecture de l'image.", elementException);
	 } catch (MalformedURLException urlException) {
	 logger.error("Erreur lors de la lecture de l'image.", urlException);
	 } catch (com.itextpdf.text.DocumentException documentException) {
	 logger.error("Erreur lors de l'ajout de l'image dans le PDF.", documentException);
	 } catch (IOException exception) {
	 logger.error("Erreur lors de la lecture de l'image.", exception);
	 }
	 }
	 }
	 */

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#genererPageSignatairesPDF(org.alfresco.service.cmr.repository.NodeRef, String, String)
     */
    @Override
    public File genererPageSignatairesPDF(NodeRef dossier, String statutTdt, String ackDate) throws Exception {
        // Récupération du template de la page de garde
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.dictionary.childname") + "/" + this.configuration.getProperty("spaces.templates.content.childname") + "/" + this.configuration.getProperty("templates.signataires.childname");
        List<NodeRef> results;
        results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
        if (results != null && results.size() == 1) {  // Génération de la page de garde
            NodeRef templateRef = results.get(0);
            Map<String, Object> model = new HashMap<String, Object>();
            List<EtapeCircuit> circuit = this.getCircuitAvecSignataireInfos(dossier);

            model.put("etapes", circuit);
            model.put("dossier", dossier);
            model.put("space", dossier);
            model.put("statut", statutTdt);

            model.put("dateGeneration", new Date());

            String statutMetier = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS_METIER);
            /**
             * Si le dossier est rejeté, relever le numéro de l'etape
             */
            boolean rejected = statutMetier.startsWith("Rejet");

            int etapeRejet = -1;

            if (rejected) {
                for (int i = 0; i < circuit.size(); i++) {
                    EtapeCircuit etape = circuit.get(i);
                    if (etape.isApproved()) {
                        etapeRejet = i;
                    }
                }
            }

            model.put("etape_rejet", etapeRejet);

            if (ackDate != null) {
                model.put("ackMIAT", ackDate);
            }

            // Méta-données
            List<CustomMetadataDef> mdliste = metadataService.getMetadataDefs();
            if (!mdliste.isEmpty()) {
                QName metadataQName = null;
                try {
                    // metadataQName = QName.createQName(MetadataServiceImpl.METADATA_DEF_CUSTOM_URI, "customMetadata");
                    metadataQName = QName.createQName("cu:customMetadata", namespaceService);
                } catch (org.alfresco.service.namespace.InvalidQNameException qe) {
                    logger.error("Unable to set up metadata QName : " + qe.getLocalizedMessage());
                } catch (org.alfresco.service.namespace.NamespaceException ne) {
                    logger.error("Unable to....: " + ne.getLocalizedMessage());
                }
                Map<String, Object> metadonnees = new HashMap<String, Object>();
                if (metadataQName != null && this.nodeService.hasAspect(dossier, metadataQName)) {
                    for (CustomMetadataDef customMetadataDef : mdliste) {
                        if (nodeService.getProperty(dossier, customMetadataDef.getName()) != null) {
                            String nomMD = customMetadataDef.getTitle(); // customMetadataDef.getName().getLocalName();
                            switch (customMetadataDef.getType()) {
                                case DATE:
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    Date date = (Date) nodeService.getProperty(dossier, customMetadataDef.getName());
                                    metadonnees.put("DATE_" + nomMD, dateFormat.format(date));
                                    metadonnees.put("DATE_B_" + nomMD, date);
                                    break;
                                case DOUBLE:
                                    Double penetration = (Double) nodeService.getProperty(dossier, customMetadataDef.getName());
                                    metadonnees.put("DOUBLE_" + nomMD, penetration);
                                    break;
                                case INTEGER:
                                    Integer vinteger = (Integer) nodeService.getProperty(dossier, customMetadataDef.getName());
                                    metadonnees.put("INTEGER_" + nomMD, vinteger);
                                    break;
                                case BOOLEAN:
                                    Boolean vbool = (Boolean) nodeService.getProperty(dossier, customMetadataDef.getName());
                                    metadonnees.put("STRING_" + nomMD, vbool ? "Oui" : "Non");
                                    break;
                                case STRING:
                                case URL:
                                default:
                                    metadonnees.put("STRING_" + nomMD, (String) nodeService.getProperty(dossier, customMetadataDef.getName()).toString());
                            }
                        }
                    }
                    if (!metadonnees.isEmpty()) {
                        model.put("metadonnees", metadonnees);
                    }
                }
            }

            // Création de la page de garde
            File tmpHtml = TempFileProvider.createTempFile("tmpconv", ".html");
            ContentWriter tmpWriterHtml = new FileContentWriter(tmpHtml);

            tmpWriterHtml.setMimetype(MimetypeMap.MIMETYPE_HTML);
            tmpWriterHtml.setEncoding("utf-8");
            BufferedWriter writer;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(tmpWriterHtml.getContentOutputStream(), tmpWriterHtml.getEncoding()));
            } catch (UnsupportedEncodingException uee) {  // fallback to default encoding
                if (logger.isEnabledFor(Level.WARN)) {
                    logger.warn("Unsupported encoding: " + tmpWriterHtml.getEncoding() + ", falling back to default encoding.", uee);
                }
                tmpWriterHtml.setEncoding(null);
                writer = new BufferedWriter(new OutputStreamWriter(tmpWriterHtml.getContentOutputStream()));
            }

            templateService.processTemplate("freemarker", templateRef.toString(), model, writer);
            try {
                writer.close();
            } catch (IOException ioe) {
                if (logger.isEnabledFor(Level.WARN)) {
                    logger.warn("Error closing the writer on " + tmpHtml, ioe);
                }
            }

            // Conversion au format PDF
            ContentReader tmpReaderHtml = tmpWriterHtml.getReader();
            if (tmpReaderHtml != null) {
                File tmpPdf = TempFileProvider.createTempFile("tmpconv", ".pdf");

                try {
                    // Tentative de génération du bordereau avec itext pour prise en charge du CSS
                    com.itextpdf.text.Document document = new com.itextpdf.text.Document(PageSize.A4);
                    PdfWriter pdfwriter = PdfWriter.getInstance(document, new FileOutputStream(tmpPdf));
                    document.open();
                    XMLWorkerHelper.getInstance().parseXHtml(pdfwriter, document, tmpReaderHtml.getContentInputStream());
                    document.close();
                } catch(Exception e) {
                    logger.warn("Transformation PDF impossible avec itext", e);

                    tmpReaderHtml = tmpWriterHtml.getReader();
                    FileContentWriter tmpWriterPdf = new FileContentWriter(tmpPdf);
                    tmpWriterPdf.setMimetype(MimetypeMap.MIMETYPE_PDF);
                    tmpWriterPdf.setEncoding(tmpReaderHtml.getEncoding());
                    try {
                        contentService.transform(tmpReaderHtml, tmpWriterPdf);
                    } catch (NoTransformerException ex) {
                        throw new RuntimeException(ex);
                    }
                }

                return tmpPdf;
            }
        } // Fin generation page de garde
        return null;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#genererDossierPDF(NodeRef, boolean, boolean, boolean)
     */
    @Override
    public File genererDossierPDF(NodeRef dossier, boolean updateDossier, boolean includeAnnexes, boolean includeFirstPage) throws Exception {

        TdtState state = getTdtState(dossier, updateDossier);
        List<NodeRef> documents = getMainDocuments(dossier);
        if (includeAnnexes) {
            documents.addAll(getAttachments(dossier));
        }

        File f = genererDossierPDFWithTdtState(dossier, state, documents, includeFirstPage);

        return f;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#genererDossierPDF(org.alfresco.service.cmr.repository.NodeRef, boolean, java.util.List, boolean)
     */
    @Override
    public File genererDossierPDF(NodeRef dossier, boolean updateDossier, List<NodeRef> attachments, boolean includeFirstPage) throws Exception {

        TdtState state = getTdtState(dossier, updateDossier);
        List<NodeRef> documents = getMainDocuments(dossier);
        if (attachments != null) {
            documents.addAll(attachments);
        }

        File f = genererDossierPDFWithTdtState(dossier, state, documents, includeFirstPage);

        return f;
    }

    private @Nullable File genererDossierPDFWithTdtState(NodeRef dossier, TdtState state, List<NodeRef> documents, boolean includeFirstPage) throws Exception {
        //TdtState state = getTdtState(dossier, updateDossier);
        String statutTdt = state.getStatus();
        String ackDate = state.getAckDate();
        String typeDoss = (String) this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER);
        String sstypeDoss = (String) this.nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER);
        PopplerUtils utils = new PopplerUtils();

        ArrayList<File> tmpFiles = new ArrayList<File>(documents.size() + (includeFirstPage ? 1 : 0));
        // Bordereau de circulation
        if (includeFirstPage) {
            File pageSignataires = genererPageSignatairesPDF(dossier, statutTdt, ackDate);
            if (null != pageSignataires) {
                tmpFiles.add(pageSignataires);
            }
        }

        // Conversion des documents
        int doccount = 1;
        boolean isMultiDoc = typesService.isMultiDocument(typeDoss, sstypeDoss);
        String sigFormat = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT);

        for (NodeRef doc : documents) {
            String name = (String) nodeService.getProperty(doc, ContentModel.PROP_NAME);
            String extension = "";
            int i = name.lastIndexOf('.');
            if(i > 0) extension = name.substring(i + 1);
            if(extension.equalsIgnoreCase("zip")) {
                // ignore this annex file
                continue;
            }

            ContentReader tmpReader = null;
            boolean isVisuelPdf = this.nodeService.getProperty(doc, ParapheurModel.PROP_VISUEL_PDF) != null && sigFormat != null && !sigFormat.contains("PAdES");
            if (isVisuelPdf) {
                tmpReader = this.contentService.getReader(doc, ParapheurModel.PROP_VISUEL_PDF);
            } else {
                tmpReader = this.contentService.getReader(doc, ContentModel.PROP_CONTENT);
            }
            if (tmpReader == null) {
                logger.warn("Erreur lors de la récupération du contenu du document: id = " + doc);
                continue;
            }
            File tmp = TempFileProvider.createTempFile("tmpconv", ".pdf");
            FileContentWriter tmpWriter = new FileContentWriter(tmp);
            tmpWriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
            tmpWriter.setEncoding(tmpReader.getEncoding());
            try {
                // Dans certains cas (ex : dossier PES ne contenant que des PJ), le "visuel" fourni
                // n'est pas conforme au format pdf (ex : chaîne "null"). On utilise alors un pdf vide
                // en remplacement pour assurer l'archivage.
                // Note : en toute rigueur, il aurait fallu contrôler le format des visuels dès l'entrée
                // dans les circuits du parapheur. L'opération effectuée ici permet de prendre en charge les dossiers
                // déjà émis dans cet état.
                if (isVisuelPdf) {
                    try {
                        contentService.transform(tmpReader, tmpWriter);
                        if(!utils.isValid(tmp.getAbsolutePath())) {
                            String nomDossier = this.nodeService.getProperty(dossier, ContentModel.PROP_TITLE).toString();
                            throw new Exception("Le document PDF généré pour " + nomDossier + " est invalide");
                        }
                    } catch (Exception ex) {
                        String nomDossier = this.nodeService.getProperty(dossier, ContentModel.PROP_TITLE).toString();
                        logger.warn("Un pdf vide remplace le visuel du dossier " + nomDossier + ", qui n'est pas interpretable comme un pdf.", ex);
                        // Créer le pdf vide
                        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
                        PdfWriter.getInstance(document, new FileOutputStream(tmp));
                        document.open();
                        try {
                            document.add(new com.itextpdf.text.Paragraph("Dossier " + nomDossier + " : visuel PDF absent."));
                        } finally {
                            document.close();
                        }
                    }
                } else {
                    contentService.transform(tmpReader, tmpWriter);
                }

                // kawarimi no jutsu
                try {
                    if (typeDoss != null && sstypeDoss != null) {
                        if (nodeService.hasAspect(doc, ParapheurModel.ASPECT_MAIN_DOCUMENT)) {
                            List<NodeRef> listeCalques;
                            if (isMultiDoc) {
                                listeCalques = typesService.getListeCalquesMultiDoc(typeDoss, sstypeDoss, doccount);
                            } else {
                                listeCalques = typesService.getListeCalques(typeDoss, sstypeDoss);
                            }
                            if (!listeCalques.isEmpty()) {
                                tmp = impressionCalqueService.applyClaques(dossier, tmp, listeCalques);
                            }
                        } else {
                            List<NodeRef> listeCalques = typesService.getListeCalquesAnnexes(typeDoss, sstypeDoss);
                            if (!listeCalques.isEmpty()) {
                                tmp = impressionCalqueService.applyClaques(dossier, tmp, listeCalques);
                            }
                        }
                    }

                } catch (Exception e) {
                    logger.warn(String.format("Impossible d'appliquer les calques. [%s]", e.getMessage()));
                    e.printStackTrace();
                }

                tmpFiles.add(tmp);
                doccount++;
            } catch (NoTransformerException nte) {
                logger.warn(nte);
                throw new RuntimeException("Aucune conversion trouvée, veuillez contacter votre administrateur.", nte);
            } catch (Exception e) {
                logger.warn(e.getLocalizedMessage(), e);
                throw new RuntimeException("Erreur lors de la conversion, veuillez contacter votre administrateur.", e);
            }
        } // Fin conversion des documents

        /**
         * Assemblage des PDFs en un seul, apposition des calques d'impression
         */
        if (!tmpFiles.isEmpty()) {
            File archive = TempFileProvider.createTempFile("tmpconvfinal", ".pdf");

            String firstPagePath = tmpFiles.get(0).getAbsolutePath();
            List<String> allpaths = new ArrayList<>();

            for(int i = 1; i < tmpFiles.size(); i++) {
                allpaths.add(tmpFiles.get(i).getAbsolutePath());
            }
            boolean result = utils.merge(firstPagePath, archive.getAbsolutePath(), allpaths);
            if(!result) {
                throw new RuntimeException("Erreur lors de la fusion des documents. Fonction Merge");
            }

            return archive;
        }
        return null;
    }

    @NotNull
    private PdfReader fixIncorrectFormPage(File tmp) throws IOException {
        PdfReader docPrincReader = new PdfReader(new FileInputStream(tmp));

        try {
            File tmpFile = TempFileProvider.createTempFile("tmpconvfix", ".pdf");
            FileOutputStream _os = new FileOutputStream(tmpFile.getAbsoluteFile());
            PdfStamper _stamper = new PdfStamper(docPrincReader, _os,'\0');
            AcroFields form = _stamper.getAcroFields();

            Map<String, AcroFields.Item> fields = form.getFields().entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            fields.forEach((o, o2) -> { if(o2.getPage(0) < 1) form.removeField(o); });

            _stamper.close();

            docPrincReader.close();

            docPrincReader = new PdfReader(new FileInputStream(tmpFile));
        } catch(Exception e) {
            logger.error("Cannot remove invalid field :");
            e.printStackTrace();
        }

        return docPrincReader;
    }

    private String getDateEtapeTdt(NodeRef dossier) {
        List<EtapeCircuit> circuit = getCircuit(dossier);
        String date = null;
        if (circuit != null) {
            for (EtapeCircuit etape : circuit) {
                if (!etape.isApproved()) {
                    break;
                }
                if (EtapeCircuit.ETAPE_TDT.equals(etape.getActionDemandee())) {
                    if (etape.getDateValidation() != null) {
                        // L'attribut est au format AAAA-MM-JJ
                        date = new SimpleDateFormat("yyyy-MM-dd").format(etape.getDateValidation());
                    }
                    break; // Une seule étape de Tdt par circuit.
                }
            }
        }
        return date;
    }

    @Override
    public TdtState getTdtState(NodeRef dossier, boolean updateDossier) {
        String statut = null;
        String ackDate = null;
        if (s2lowService != null && s2lowService.isEnabled() && nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW)) {
            if (updateDossier) {
                int codeInfoS2low = -1;
                try {
                    codeInfoS2low = s2lowService.getInfosS2low(dossier);
                } catch (IOException ioe) {
                    if (logger.isEnabledFor(Level.WARN)) {
                        logger.warn("Impossible de récupérer le statut s2low ref = " + dossier, ioe);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // FIXME : Spécifique S2LOW ACTES + HELIOS !!!
                // Check statut!={1:posté, 2:en attente de trs, 3:transmis} pour ne pas archiver un état transitoire
                Assert.isTrue((codeInfoS2low < 1 || codeInfoS2low > 3), "Dossier non archivable actuellement: traitement S2LOW en cours");

                statut = this.nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS).toString();
            } else {
                if (this.nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS) != null) {
                    statut = this.nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS).toString();
                }
            }
            // Récup date acquittement MIAT depuis ParapheurModel.PROP_ARACTE_XML
            if (this.nodeService.getProperty(dossier, ParapheurModel.PROP_ARACTE_XML) != null) {
                ContentReader contentreader = contentService.getReader(dossier, ParapheurModel.PROP_ARACTE_XML);
                SAXReader saxreader = new SAXReader();
                try {
                    Document document = saxreader.read(contentreader.getContentInputStream());
                    Element rootElement = document.getRootElement();
                    // L'attribut est au format AAAA-MM-JJ
                    ackDate = rootElement.attributeValue("DateReception");
                } catch (DocumentException e) {
                    if (logger.isEnabledFor(Level.WARN)) {
                        logger.warn("Erreur sur récup Date MIAT dossier ref = " + dossier, e);
                    }
                }

            }
            // Récup date acquittement MIAT depuis ParapheurModel.PROP_NACKHELIOS_XML
            else if (this.nodeService.getProperty(dossier, ParapheurModel.PROP_NACKHELIOS_XML) != null) {
                ContentReader contentreader = contentService.getReader(dossier, ParapheurModel.PROP_NACKHELIOS_XML);
                SAXReader saxreader = new SAXReader();
                try {
                    Document document = saxreader.read(contentreader.getContentInputStream());
                    Element rootElement = document.getRootElement();
                    // L'attribut est au format AAAA-MM-JJ
                    Element current = rootElement.element("EnTetePES");
                    if (current != null) {
                        current = current.element("DteStr");
                        if (current != null) {
                            ackDate = current.attributeValue("V");
                        }
                    }
                    if (ackDate == null) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("Date ACK non trouvée dans le xml, on prend la date de validation de l'étape de Tdt pour : " + dossier);
                        }
                        ackDate = getDateEtapeTdt(dossier);
                    }
                    if (ackDate == null) {
                        if (logger.isEnabledFor(Level.WARN)) {
                            logger.warn("Aucune Date MIAT trouvée pour le dossier : " + dossier);
                        }
                    }
                } catch (DocumentException e) {
                    if (logger.isEnabledFor(Level.WARN)) {
                        logger.warn("Erreur sur récup Date MIAT dossier ref = " + dossier, e);
                    }
                }

            } else {
                if (logger.isEnabledFor(Level.WARN)) {
                    logger.warn("Statut [" + statut + "], mais pas de date MIAT, XML absent!");
                }
            }
        } // BLEX SRCI {
        else if (nodeService.hasAspect(dossier, SrciService.K.aspect_srciTransaction)) {
            if (updateDossier) {
                try {
                    statut = srciService.getTdtStatusText(dossier);
                } catch (Exception e) {
                    logger.warn("impossible de recuperer le statut SRCI de [" + dossier + "]", e);
                }
            } else {
                if (this.nodeService.getProperty(dossier, SrciService.K.property_srciTransaction_status) != null) {
                    statut = SrciService.tdtStatusCodeToText(
                            this.nodeService.getProperty(dossier, SrciService.K.property_srciTransaction_status).toString());
                }
            }
        } // } BLEX SRCI

        return new TdtState(statut, ackDate);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#archiver(org.alfresco.service.cmr.repository.NodeRef,
     * String, java.util.List)
     */
    @Override
    public String archiver(NodeRef dossier, String titreArchive) throws Exception {
        return archiver(dossier, titreArchive, getAttachments(dossier));
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#archiver(org.alfresco.service.cmr.repository.NodeRef,
     * String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public String archiver(NodeRef dossier, String titreArchive, List<NodeRef> annexes) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("ARCHIVER - Entree dans la methode");
        }
        String urlArchive = null;
        String currentUser = AuthenticationUtil.getRunAsUser();
        NodeRef bureauParent = getParentParapheur(dossier);
        /**
         * On peut archiver un dossier que si:
         *     dossier terminé
         *   ET  currentUser pas null
         *   ET  currentUser propriétaire/secrétaire/délégué du parapheur courant détenant le dossier
         */
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");
        Assert.isTrue(isTermine(dossier), "Le dossier n'est pas \"terminé\": id = " + dossier);
        // Assert.isTrue(getActeurCourant(dossier) == null, "Toutes les étapes du dossier n'ont pas été validées: id = " + dossier);
        Assert.notNull(currentUser, "Aucun utilisateur courant !");
        Assert.isTrue(isParapheurOwnerOrDelegate(bureauParent, currentUser) || isParapheurSecretaire(bureauParent, currentUser), "Le dossier ne peut être archivé que par un bureau autorisé.");
        EtapeCircuit etapeCourante = this.getCurrentEtapeCircuit(dossier);
        if (etapeCourante == null) {   // compatibilité ascendante
            Assert.isTrue(getParapheurOwners(getEmetteur(dossier)).contains(currentUser), "Le dossier ne peut être archivé que par son émetteur.");
        } else {
            setCurrentValidator(dossier, AuthenticationUtil.getRunAsUser());
            if (isRejete(dossier)) {
                /**
                 * v3.3 : On doit pouvoir archiver AUSSI les dossiers rejetés.
                 *   - L'info d'étape courante n'est plus pertinente...
                 * Il faut que:
                 *    ET  dossier "rejeté" (cf statut métier commence par "Rejet")
                 *    ET  dans la corbeille "Dossiers rejetés"
                 */
                // pas de contrôle particulier à effectuer, avec 'isRejete' ce devrait être ok implicitement non?
                logger.info("Archivage d'un dossier rejeté: " + dossier.getId());
            } else {
                /**
                 * Depuis les nouveaux circuits (avec action demandée) on archive si
                 *   - en étape d'archivage
                 *    (ET  dans la corbeille "Dossiers en fin de circuit", implicite?)
                 */
                Assert.isTrue(etapeCourante.getActionDemandee().trim().equalsIgnoreCase(EtapeCircuit.ETAPE_ARCHIVAGE), "Le dossier n'est pas à une étape d'archivage.");
                /* On positionne la date de validation et la propriété Effectuée avant de génerer le pdf d'archive */
                nodeService.setProperty(((EtapeCircuitImpl) etapeCourante).getNodeRef(),
                        ParapheurModel.PROP_EFFECTUEE,
                        Boolean.TRUE);
                nodeService.setProperty(((EtapeCircuitImpl) etapeCourante).getNodeRef(),
                        ParapheurModel.PROP_DATE_VALIDATION,
                        new Date());
                String zeActor = getPrenomNomFromLogin(currentUser);
                if (!isParapheurOwner(bureauParent, currentUser)) {
                    if (isParapheurSecretaire(bureauParent, currentUser)) {
                        zeActor += " (secrétaire)";
                    } else if (isParapheurDelegate(bureauParent, currentUser)) {
                        zeActor = String.format(configuration.getProperty("parapheur.suppleance.text"), getPrenomNomFromLogin(currentUser), getNomParapheur(bureauParent));
                    }
                }
                nodeService.setProperty(((EtapeCircuitImpl) etapeCourante).getNodeRef(),
                        ParapheurModel.PROP_SIGNATAIRE,
                        zeActor);

            }
        }

        // Récupération du répertoire des archives
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.archives.childname");
        List<NodeRef> results;
        results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
        if (results == null || results.size() != 1) {
            throw new RuntimeException("Il n'y a pas de dossier \"Archives\"");
        }
        NodeRef archivesRef = results.get(0);


        String nomArchive = (String) nodeService.getProperty(dossier, ContentModel.PROP_NAME);
        String description = (String) nodeService.getProperty(dossier, ContentModel.PROP_DESCRIPTION);
        if (titreArchive == null || titreArchive.length() == 0) {
            titreArchive = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE) + ".pdf";
        } else if (!titreArchive.endsWith(".pdf")) {
            titreArchive += ".pdf";
        }

        // generer PDF archive ici
        //        File archive = genererDossierPDF(dossier, true, documents);
        File archive = genererDossierPDF(dossier, false, annexes, true);
        if (null != archive) {
            try {
                // Création du noeud
                if (logger.isDebugEnabled()) {
                    logger.debug("ARCHIVER - Creation du noeud archive");
                }
                FileInfo fileInfo = fileFolderService.create(archivesRef, nomArchive, ParapheurModel.TYPE_ARCHIVE);
                NodeRef archiveRef = fileInfo.getNodeRef();
                ContentWriter writer = contentService.getWriter(archiveRef, ContentModel.PROP_CONTENT, true);
                writer.setMimetype(MimetypeMap.MIMETYPE_PDF);
                writer.setEncoding("UTF-8");
                if (logger.isDebugEnabled()) {
                    logger.debug("ARCHIVER - Ecriture du contenu PDF");
                }
                writer.putContent(archive);

                // Ajout de l'aspect "titled" (title et description) à l'archive
                Map<QName, Serializable> titled_props = new HashMap<QName, Serializable>();
                titled_props.put(ContentModel.PROP_TITLE, titreArchive);
                titled_props.put(ContentModel.PROP_DESCRIPTION, description);
                nodeService.addAspect(archiveRef, ContentModel.ASPECT_TITLED, titled_props);

                this.nodeService.setProperty(archiveRef, ContentModel.PROP_CREATOR, currentUser);
                this.nodeService.setProperty(archiveRef, ContentModel.PROP_MODIFIER, currentUser);

                List<NodeRef> docsRefs = this.getMainDocuments(dossier);

                String docFileName = (String) this.nodeService.getProperty(docsRefs.get(0), ContentModel.PROP_NAME);

                if(docsRefs.size() == 1) {
                    // Mode d'archivage unidocument principal
                    logger.debug("ARCHIVER - Sauvegarde du document original");
                    ContentReader docReader = this.contentService.getReader(docsRefs.get(0), ContentModel.PROP_CONTENT);
                    ContentWriter docWriter = this.contentService.getWriter(archiveRef, ParapheurModel.PROP_ORIGINAL, true);
                    docWriter.setEncoding(docReader.getEncoding());
                    docWriter.setMimetype(docReader.getMimetype());
                    docWriter.putContent(docReader);

                    this.nodeService.setProperty(archiveRef, ParapheurModel.PROP_ORIGINAL_NAME, docFileName);

                    //Maintenant, on associe l'attestation de signature... si présente !
                    if(nodeService.getProperty(docsRefs.get(0), ParapheurModel.PROP_ATTEST_CONTENT) != null) {
                        logger.debug("ARCHIVER - Sauvegarde de l'attestation de signature");
                        ContentReader attestReader = this.contentService.getReader(docsRefs.get(0), ParapheurModel.PROP_ATTEST_CONTENT);
                        ContentWriter attestWriter = this.contentService.getWriter(archiveRef, ParapheurModel.PROP_ATTEST_CONTENT, true);
                        attestWriter.setEncoding(attestReader.getEncoding());
                        attestWriter.setMimetype(attestReader.getMimetype());
                        attestWriter.putContent(attestReader);
                    }
                } else {
                    // Gestion d'archivage multi-document
                    File tmpZipFile = TempFileProvider.createTempFile(titreArchive + "_DOCs", "zip");
                    ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(tmpZipFile));
                    for (NodeRef doc : docsRefs) {
                        ContentReader docReader = this.contentService.getReader(doc, ContentModel.PROP_CONTENT);
                        String docname = (String) nodeService.getProperty(doc, ContentModel.PROP_NAME);
                        String nfdNormalizedString = Normalizer.normalize(docname, Normalizer.Form.NFD);
                        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
                        nfdNormalizedString = pattern.matcher(nfdNormalizedString).replaceAll("");
                        zout.putNextEntry(new ZipEntry(nfdNormalizedString));
                        // Transfer bytes from the file to the ZIP file
                        zout.write(IOUtils.toByteArray(docReader.getContentInputStream()));
                        zout.closeEntry();
                    }
                    zout.close();
                    ContentWriter sigWriter = this.contentService.getWriter(archiveRef, ParapheurModel.PROP_ORIGINAL, true);
                    sigWriter.setMimetype(MimetypeMap.MIMETYPE_ZIP);
                    sigWriter.setEncoding("UTF-8");
                    sigWriter.putContent(tmpZipFile);

                    this.nodeService.setProperty(archiveRef, ParapheurModel.PROP_ORIGINAL_NAME, titreArchive + ".zip");


                    // Gestion de la multi-attestation
                    boolean hasEntry = false;
                    File tmpAttFile = TempFileProvider.createTempFile(titreArchive + "_Attests", "zip");
                    ZipOutputStream zoutAttest = new ZipOutputStream(new FileOutputStream(tmpAttFile));
                    for (NodeRef doc : docsRefs) {
                        if(nodeService.getProperty(doc, ParapheurModel.PROP_ATTEST_CONTENT) != null) {
                            ContentReader docReader = this.contentService.getReader(doc, ParapheurModel.PROP_ATTEST_CONTENT);
                            String docname = "Attestation - " + nodeService.getProperty(doc, ContentModel.PROP_NAME);
                            String nfdNormalizedString = Normalizer.normalize(docname, Normalizer.Form.NFD);
                            Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
                            nfdNormalizedString = pattern.matcher(nfdNormalizedString).replaceAll("");
                            zoutAttest.putNextEntry(new ZipEntry(nfdNormalizedString));
                            // Transfer bytes from the file to the ZIP file
                            zoutAttest.write(IOUtils.toByteArray(docReader.getContentInputStream()));
                            zoutAttest.closeEntry();
                            hasEntry = true;
                        }
                    }
                    if(hasEntry) {
                        zoutAttest.close();
                        ContentWriter attWriter = this.contentService.getWriter(archiveRef, ParapheurModel.PROP_ATTEST_CONTENT, true);
                        attWriter.setMimetype(MimetypeMap.MIMETYPE_ZIP);
                        attWriter.setEncoding("UTF-8");
                        attWriter.putContent(tmpAttFile);
                    }
                }

				/*
                 * List<NodeRef> archiveRefs = new ArrayList<NodeRef>();
				 * archiveRefs.add(archiveRef);
				 */
                String sigFormat = nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT).toString();
                // Sauvegarde du document principal et de sa signature
                byte[] signature = this.getSignature(dossier);
                if ((signature != null && signature.length > 0)) {
                    logger.debug("ARCHIVER - Signature recuperee");
                    logger.debug("ARCHIVER - Ajout de l'aspect SIGNED");
                    this.nodeService.addAspect(archiveRef, ParapheurModel.ASPECT_SIGNED, null);
                    logger.debug("ARCHIVER - Sauvegarde du nom original");

                    List<EtapeCircuit> etapes = this.getCircuit(dossier);
                    if (etapes.get(0).getActionDemandee() == null) {   // compatibilité ascendante
                        logger.debug("ARCHIVER - Sauvegarde de la signature");
                        ContentWriter sigWriter = this.contentService.getWriter(archiveRef, ParapheurModel.PROP_SIG, true);
                        if (sigFormat.startsWith("XAdES")) {
                            sigWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
                        } else if (sigFormat.startsWith("PKCS#7")) {
                            sigWriter.setMimetype("application/pkcs7-signature");
                        } else if (sigFormat.startsWith("PKCS#1")) {
                            sigWriter.setMimetype("text/plain");
                        } else {
                            throw new UnsupportedOperationException("Unknown signature format: " + sigFormat);
                        }
                        sigWriter.setEncoding("UTF-8");
                        sigWriter.putContent(new ByteArrayInputStream(signature));
                    }
                } else {   //  Traiter l'archivage de signatures multiples
                    File tmpZipFile = this.produceZipSignatures(docFileName, dossier);
                    if (tmpZipFile != null) {
                        ContentWriter sigWriter = this.contentService.getWriter(archiveRef, ParapheurModel.PROP_SIG, true);
                        sigWriter.setMimetype(MimetypeMap.MIMETYPE_ZIP);
                        sigWriter.setEncoding("UTF-8");
                        sigWriter.putContent(tmpZipFile);
                    }
                }

                // Copie les étapes du circuit sur l'archive pour conservation
                // Cette copie doit etre faite avant de modifier les permissions
                // sous peine d'access denied
                // Envoi du mail de notification avant la modification du dossier
                notificationService.notifierPourArchivage(dossier, nomArchive);
                List<ChildAssociationRef> children = nodeService.getChildAssocs(dossier,
                        ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                        RegexQNamePattern.MATCH_ALL);

                copyService.copy(children.get(0).getChildRef(),
                        archiveRef,
                        ParapheurModel.CHILD_ASSOC_OLD_PREMIERE_ETAPE,
                        children.get(0).getQName(),
                        true);
                // Copie des metadonnées définie sur l'archive.
                for (CustomMetadataDef def : metadataService.getMetadataDefs()) {
                    Serializable propValue = nodeService.getProperty(dossier, def.getName());
                    if(propValue != null) {
                        nodeService.setProperty(archiveRef, def.getName(), propValue);
                    }
                }
                // Résolution des permissions
                dossierService.setPermissionsArchive(archiveRef, dossier);
                // Si envoyé à S2LOW: sauvegarde ID transaction, renseignement de l'URL d'archive dans S2LOW
                if (s2lowService != null && s2lowService.isEnabled() && nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW)) {
                    logger.debug("ARCHIVER - Sauvegarde du numero de Transaction S2LOW");
                    Map<QName, Serializable> pptes = new HashMap<QName, Serializable>();
                    pptes.put(ParapheurModel.PROP_TRANSACTION_ID, this.nodeService.getProperty(dossier,
                            ParapheurModel.PROP_TRANSACTION_ID));

                    this.nodeService.addAspect(archiveRef, ParapheurModel.ASPECT_S2LOW, pptes);
                    // this.nodeService.setProperty(archiveRef, ParapheurModel.PROP_TRANSACTION_ID,
                    // this.nodeService.getProperty(dossier, ParapheurModel.PROP_TRANSACTION_ID));

                    if (this.contentService.getReader(dossier, ParapheurModel.PROP_ARACTE_XML) != null) {
                        logger.debug("ARCHIVER - Sauvegarde du XML retour MIAT");
                        ContentReader sigReader = this.contentService.getReader(dossier, ParapheurModel.PROP_ARACTE_XML);
                        ContentWriter sigWriter = this.contentService.getWriter(archiveRef,
                                ParapheurModel.PROP_ARACTE_XML, true);
                        sigWriter.setEncoding(sigReader.getEncoding());
                        sigWriter.setMimetype(sigReader.getMimetype());
                        sigWriter.putContent(sigReader);

                        if (S2lowService.PROP_TDT_NOM_S2LOW.equals(nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_NOM))) {
                            // Tentative de renseignement de l'URL d'archive
                            logger.debug("ARCHIVER - Renseignement URL archivage dans S2LOW ");
                            try {
                                urlArchive = s2lowService.setS2lowActesArchiveURL(archiveRef);
                            } catch (IOException ioe) {
                                logger.info("ARCHIVER - " + ioe.getLocalizedMessage());
                            }
                        }

                    } else {
                        logger.warn("ARCHIVER - XML retour MIAT Absent !");
                    }
                }
                // Si dossier "typé metier" on récupère les meta donnees
                if (nodeService.hasAspect(dossier, ParapheurModel.ASPECT_TYPAGE_METIER)) {
                    logger.debug("ARCHIVER - Sauvegarde meta données Type Metier");
                    Map<QName, Serializable> pptes = new HashMap<QName, Serializable>();
                    pptes.put(ParapheurModel.PROP_TYPE_METIER, this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER));
                    pptes.put(ParapheurModel.PROP_SOUSTYPE_METIER, this.nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER));
                    if (this.nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_NOM) != null) {
                        pptes.put(ParapheurModel.PROP_TDT_NOM, this.nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_NOM));
                    }
                    if (this.nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_PROTOCOLE) != null) {
                        pptes.put(ParapheurModel.PROP_TDT_PROTOCOLE, this.nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_PROTOCOLE));
                    }
                    if (this.nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_FICHIER_CONFIG) != null) {
                        pptes.put(ParapheurModel.PROP_TDT_FICHIER_CONFIG, this.nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_FICHIER_CONFIG));
                    }
                    if (this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_SIGNATURE) != null) {
                        pptes.put(ParapheurModel.PROP_TYPE_SIGNATURE, this.nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_SIGNATURE));
                    }
                    if (this.nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT) != null) {
                        pptes.put(ParapheurModel.PROP_SIGNATURE_FORMAT, this.nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT));
                    }
                    if (this.nodeService.getProperty(dossier, ParapheurModel.PROP_XPATH_SIGNATURE) != null) {
                        pptes.put(ParapheurModel.PROP_XPATH_SIGNATURE, this.nodeService.getProperty(dossier, ParapheurModel.PROP_XPATH_SIGNATURE));
                    }
                    this.nodeService.addAspect(archiveRef, ParapheurModel.ASPECT_TYPAGE_METIER, pptes);
                }

                List<Object> list = new ArrayList<Object>();
                list.add(StatusMetier.STATUS_ARCHIVE);
                if (urlArchive != null) {
                    list.add(urlArchive);
                }
                auditWithNewBackend("ParapheurServiceCompat", "Archivage du dossier", dossier, list);


                // TODO: avant de supprimer le dossier: relevé des indicateurs de gestion, audit au format XML
                // TODO:   META Donnees d'archivage ?? QUELLES DONNEES ??


                // On supprime le dossier archivé
//                logger.debug("ARCHIVER - Suppression du dossier");
//                dossierService.deleteDossier(dossier, false);
            } catch (org.alfresco.service.cmr.model.FileExistsException ex1) {
                throw new RuntimeException("Une archive nommée '" + nomArchive + "' existe déjà.", ex1);
            }
        }
        return urlArchive;
    }

    @Override
    public void handleDeleteDossier(NodeRef dossier, NodeRef parapheur, boolean notify, String username) {
        try {
            JSONObject sendDeleteDossier = new JSONObject();

            sendDeleteDossier.put(WorkerService.ID, dossier.getId());
            sendDeleteDossier.put(WorkerService.USERNAME, username);
            if(notify) {
                sendDeleteDossier.put(WorkerService.BUREAUCOURANT, parapheur.getId());
            }
            sendDeleteDossier.put(WorkerService.ACTION, DossierService.ACTION_DOSSIER.SUPPRESSION);
            sendDeleteDossier.put(WorkerService.TYPE, WorkerService.TYPE_DOSSIER);
            sendDeleteDossier.put(WorkerService.NOTIFME, notify);

            messagesSender.sendWorker(sendDeleteDossier.toString(), dossier.getId());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private String getEntryZipName(String docName, int ordre, String signataire, List<String> existingNames) {
        String sigdetacheeFormat = this.configuration.getProperty("parapheur.filename.signature.detachee");
        if(sigdetacheeFormat == null) {
            sigdetacheeFormat = "%d-%o-%S";
        }
        String sigExterneName = this.configuration.getProperty("parapheur.filename.signature.externe");
        if(sigExterneName == null) {
            sigExterneName = "signature_externe";
        }

        String entryName = sigdetacheeFormat.replace("%d", docName).replace("%o", Integer.toString(ordre)).replace("%S", ordre == 0 ? sigExterneName : signataire);
        // Find %{i}d
        Pattern p = Pattern.compile("%(\\d+)s");
        Matcher m = p.matcher(entryName);
        if(m.find() && m.groupCount() == 1) {
            int group = Integer.parseInt(m.group(1));
            entryName = entryName.replace("%" + group + "s", ordre == 0 ? sigExterneName : signataire.substring(0, Math.min(signataire.length(), group)));
        }

        if(existingNames.contains(entryName)) {
            int duplicates = 1;
            while(existingNames.contains(entryName + " (" + duplicates + ")")) {
                duplicates++;
            }
            entryName = entryName + " (" + duplicates + ")";
        }
        existingNames.add(entryName);
        return entryName;
    }

    @Override
    public File produceZipSignatures(String docFileName, NodeRef dossier) throws IOException {
        File tmpZipFile = TempFileProvider.createTempFile(docFileName + "_SIGs", "zip");
        ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(tmpZipFile));
        boolean isEmpty = true;
        List<com.atolcd.parapheur.repo.EtapeCircuit> etapes = this.getCircuit(dossier);
        if(etapes == null) {
            return null;
        }
        int ordre = 0;
        List<NodeRef> docsRefs = this.getMainDocuments(dossier);
        List<String> existingNames = new ArrayList<>();
        for (EtapeCircuit etape : etapes) {
            if (etape.getActionDemandee().trim().equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {
                ordre++;
            }
            for(NodeRef document: docsRefs) {
                try {
                    Signature sig = this.signatureService.getDetachedSignature(document.getId(), etape.getNodeRef().getId());
                    if(sig != null) {
                        String docName = (String) this.nodeService.getProperty(document, ContentModel.PROP_NAME);

                        // Handle detached signature names, avoiding duplicates
                        String entryName = getEntryZipName(docName, ordre, etape.getSignataire(), existingNames);
                        zout.putNextEntry(new ZipEntry(entryName + "." + sig.getExtension()));

                        byte[] sigbytes = IOUtils.toByteArray(sig.getStream());
                        boolean isB64 = false;
                        try {
                            isB64 = org.apache.commons.net.util.Base64.isArrayByteBase64(sigbytes) && !(new String(sigbytes)).startsWith("-----"); // This is for PKCS7, which is by default base64 compliant
                            if(isB64) {
                                sigbytes = Base64.decode(sigbytes);
                            }
                        } catch(Exception e) {
                            // Do nothing
                        }

                        if(sig.getMediaType().equals(new MediaType("application", "pkcs1"))) {
                            // In case of pkcs1, we need to separate signature and certificate
                            zout.write(sigbytes, 0, sigbytes.length);
                            zout.closeEntry();

                            String clePubliqueEtape = (String) nodeService.getProperty(etape.getNodeRef(), ParapheurModel.PROP_CLE_PUBLIQUE_ETAPE);
                            if (clePubliqueEtape != null) {
                                // On signe en PKCS#1, on doit conserver la clé publique du signataire !
                                zout.putNextEntry(new ZipEntry(ordre + "- " + etape.getSignataire() + ".pem"));
                                if(!clePubliqueEtape.contains("BEGIN CERTIFICATE")) {
                                    boolean jumpLine = clePubliqueEtape.length() % 64 != 0;
                                    clePubliqueEtape = "-----BEGIN CERTIFICATE-----\n" + clePubliqueEtape.replaceAll("(.{64})", "$1\n") + (jumpLine ? "\n" : "") + "-----END CERTIFICATE-----";
                                }
                                // Transfer bytes from the file to the ZIP file
                                zout.write(clePubliqueEtape.getBytes(), 0, clePubliqueEtape.getBytes().length);
                                zout.closeEntry();
                            }
                        } else {
                            zout.write(sigbytes, 0, sigbytes.length);
                            zout.closeEntry();
                        }

                        isEmpty = false;
                    }
                } catch(SignatureNotFoundException e) {
                }
            }
        }
        if (isEmpty) {
            return null;
        }
        zout.close();
        return tmpZipFile;
    }

    protected Properties getArchivesConfiguration() {
        List<NodeRef> nodes = searchService.selectNodes(
                nodeService.getRootNode(new StoreRef(configuration.getProperty("spaces.store"))),
                ARCHIVES_CONFIGURATION_PATH,
                null,
                namespaceService, false);

        if (nodes.isEmpty()) {
            throw new IllegalStateException("Unable to find archives configuration file");
        }


        NodeRef archivesConfNode = nodes.get(0);

        ContentReader reader = contentService.getReader(archivesConfNode, ContentModel.PROP_CONTENT);
        Properties archivesConf = new Properties();
        try {
            archivesConf.load(reader.getContentInputStream());
        } catch (IOException ex) {
            throw new RuntimeException("Unable to read archives configuration", ex);
        }

        return archivesConf;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#secretariat(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void secretariat(NodeRef dossier) {
        logger.debug("SECRETARIAT - Entree dans la methode");

        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");
        Assert.isTrue(!isTermine(dossier), "Le dossier est \"terminé\": id = " + dossier);

        ChildAssociationRef oldParentAssoc = nodeService.getPrimaryParent(dossier);
        Assert.notNull(oldParentAssoc, "Le dossier n'a pas de parent: id = " + dossier);

        NodeRef parapheur = this.getParentParapheur(dossier);
        Assert.notNull(parapheur, "Impossible de retrouver le bureau du dossier : id = " + dossier);

        Assert.isTrue(isActeurCourant(dossier, AuthenticationUtil.getRunAsUser()) || isParapheurSecretaire(parapheur, AuthenticationUtil.getRunAsUser()),
                "Vous n'êtes pas acteur courant de ce dossier, ni membre du secrétariat!");

        NodeRef corbeille = null;
        if (this.nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT)) {
            if (this.isEmis(dossier)) {
                corbeille = this.getCorbeille(parapheur, ParapheurModel.NAME_A_TRAITER);
            } else {
                corbeille = this.getCorbeille(parapheur, ParapheurModel.NAME_EN_PREPARATION);
            }

            if (corbeille != null) {
                logger.debug("SECRETARIAT - Suppression de l'aspect SECRETARIAT");
                this.nodeService.removeAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT);
            }
        } else {
            corbeille = this.getCorbeille(parapheur, ParapheurModel.NAME_SECRETARIAT);
            if (corbeille != null) {
                logger.debug("SECRETARIAT - Ajout de l'aspect SECRETARIAT");
                this.nodeService.addAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT, null);
            }
        }

        if (corbeille == null) {
            throw new RuntimeException("Impossible de trouver la corbeille destination du dossier : id = " + dossier);
        }

        logger.debug("SECRETARIAT - Deplacement du noeud");
        try {
            nodeService.moveNode(dossier, corbeille, oldParentAssoc.getTypeQName(), oldParentAssoc.getQName());

            List<Object> list = new ArrayList<Object>();
            list.add((String) nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS_METIER));
            auditWithNewBackend("ParapheurServiceCompat", "Opération de secrétariat sur le dossier", dossier, list);

        } catch (DuplicateChildNodeNameException ex) {
            if (logger.isEnabledFor(Level.WARN)) {
                logger.warn("Impossible de transmettre le dossier : un dossier de même nom existe dans la corbeille de destination");
            }
            throw new RuntimeException("Un dossier de même nom existe dans la corbeille de destination", ex);
        }

        notificationService.notifierPourSecretariat(dossier);
    }

    @Override
    public Map<String, NodeRef> getOwnedWorkflows() {
        Map<String, NodeRef> res = new HashMap<String, NodeRef>();
        String xpath = this.configuration.getProperty("spaces.company_home.childname")
                + "/" + this.configuration.getProperty("spaces.dictionary.childname")
                + "/" + this.configuration.getProperty("spaces.savedworkflows.childname");
        try {
            List<NodeRef> results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
            if (results != null && results.size() == 1) {
                List<ChildAssociationRef> children = nodeService.getChildAssocs(results.get(0), ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
                for (ChildAssociationRef childAssoRef : children) {
                    NodeRef ref = childAssoRef.getChildRef();
                    String owner = (String) nodeService.getProperty(ref, ContentModel.PROP_CREATOR);
                    boolean isPrivate = nodeService.hasAspect(ref, ParapheurModel.ASPECT_PRIVATE_WORKFLOW);
                    if (isOfType(ref, ParapheurModel.TYPE_SAVED_WORKFLOW)
                            && isPrivate
                            && AuthenticationUtil.getRunAsUser().equals(owner)) {
                        String name = (String) nodeService.getProperty(ref, ContentModel.PROP_NAME);
                        res.put(name, ref);
                    }
                }
            }
        } catch (AccessDeniedException err) {   // ignore
            logger.warn("Access denied to workflow directory for user " + AuthenticationUtil.getRunAsUser(), err);
        }
        return res;
    }

    @Override
    public Map<String, NodeRef> getPublicWorkflows() {
        Map<String, NodeRef> res = new HashMap<String, NodeRef>();
        String xpath = this.configuration.getProperty("spaces.company_home.childname")
                + "/" + this.configuration.getProperty("spaces.dictionary.childname")
                + "/" + this.configuration.getProperty("spaces.savedworkflows.childname");
        try {
            List<NodeRef> results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
            if (results != null && results.size() == 1) {
                List<ChildAssociationRef> children = nodeService.getChildAssocs(results.get(0), ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
                for (ChildAssociationRef childAssoRef : children) {
                    NodeRef ref = childAssoRef.getChildRef();
                    boolean isPrivate = nodeService.hasAspect(ref, ParapheurModel.ASPECT_PRIVATE_WORKFLOW);
                    if (isOfType(ref, ParapheurModel.TYPE_SAVED_WORKFLOW)
                            && !isPrivate) {
                        String name = (String) nodeService.getProperty(ref, ContentModel.PROP_NAME);
                        res.put(name, ref);
                    }
                }
            }
        } catch (AccessDeniedException err) {   // ignore
            logger.warn("Access denied to workflow directory for user " + AuthenticationUtil.getRunAsUser(), err);
        }
        return res;
    }

    @Override
    public Map<String, NodeRef> getAllWorkflows() {
        Map<String, NodeRef> res = new HashMap<String, NodeRef>();
        String xpath = this.configuration.getProperty("spaces.company_home.childname")
                + "/" + this.configuration.getProperty("spaces.dictionary.childname")
                + "/" + this.configuration.getProperty("spaces.savedworkflows.childname");
        try {
            List<NodeRef> results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
            if (results != null && results.size() == 1) {
                List<ChildAssociationRef> children = nodeService.getChildAssocs(results.get(0), ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
                for (ChildAssociationRef childAssoRef : children) {
                    NodeRef ref = childAssoRef.getChildRef();
                    if (isOfType(ref, ParapheurModel.TYPE_SAVED_WORKFLOW)) {
                        String name = (String) nodeService.getProperty(ref, ContentModel.PROP_NAME);
                        res.put(name, ref);
                    }
                }
            }
        } catch (AccessDeniedException err) {   // ignore
            logger.warn("Access denied to workflow directory for user " + AuthenticationUtil.getRunAsUser(), err);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("getAllWorkflows: returned " + res);
        }
        return res;
    }

    // STV:  Backup de la précédente méthode
    @Deprecated
    public Map<String, NodeRef> getSavedWorkflows(String type, String sstype) {
        Map<String, NodeRef> res = new HashMap<String, NodeRef>();
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.dictionary.childname") + "/" + this.configuration.getProperty("spaces.savedworkflows.childname");

        try {   // Récupération du répertoire de circuits
            List<NodeRef> results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
            if (results != null && results.size() == 1) {
                NodeRef circuitsRep = null;
                List<ChildAssociationRef> circuitsPrives = nodeService.getChildAssocs(results.get(0),
                        ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
                for (ChildAssociationRef prive : circuitsPrives) {
                    NodeRef rep = prive.getChildRef();
                    if (isOfType(rep, ContentModel.TYPE_FOLDER) && this.authenticationService.getCurrentUserName().equals(nodeService.getProperty(rep, ContentModel.PROP_NAME))) {   // On a trouvé un dossier contenant les circuits privés de l'utilisateur courant
                        circuitsRep = rep;
                        break;
                    }
                }

                if (circuitsRep != null) {   // On alimente la liste avec les circuits privés
                    List<ChildAssociationRef> circuit = nodeService.getChildAssocs(circuitsRep, ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
                    for (ChildAssociationRef child : circuit) {
                        NodeRef circuitRef = child.getChildRef();
                        String name = (String) nodeService.getProperty(circuitRef, ContentModel.PROP_NAME);
                        res.put(name, circuitRef);
                    }
                }

                // On alimente la liste avec les circuits publics
                circuitsRep = results.get(0);
                List<ChildAssociationRef> circuit = nodeService.getChildAssocs(circuitsRep, ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
                for (ChildAssociationRef child : circuit) {
                    NodeRef circuitRef = child.getChildRef();
                    if (isOfType(circuitRef, ContentModel.TYPE_CONTENT)) {
                        String name = (String) nodeService.getProperty(circuitRef, ContentModel.PROP_NAME);
                        res.put(name + " (public)", circuitRef);
                    }
                }
            }
        } catch (AccessDeniedException err) {   // ignore
        }
        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getCircuitHierarchique(NodeRef)
     */
    @Override
    public List<EtapeCircuit> getCircuitHierarchique(NodeRef emetteur) {
        List<EtapeCircuit> circuit = new ArrayList<EtapeCircuit>();
        NodeRef parapheur = emetteur;
        while (parapheur != null) {
            EtapeCircuitImpl etape = new EtapeCircuitImpl();
            etape.setParapheur(parapheur);
            etape.setTransition("PARAPHEUR");
            parapheur = this.getParapheurResponsable(parapheur);
            if (parapheur == null) {
                etape.setActionDemandee(EtapeCircuit.ETAPE_SIGNATURE);
            } else {
                etape.setActionDemandee(EtapeCircuit.ETAPE_VISA);
            }
            circuit.add(etape);//this.addParapheurVisa(parapheur);
        }
        // ajout d'une dernière étape archivage par l'émetteur
        EtapeCircuitImpl finale = new EtapeCircuitImpl();
        finale.setParapheur(emetteur);
        finale.setTransition("PARAPHEUR");
        finale.setActionDemandee(EtapeCircuit.ETAPE_ARCHIVAGE);
        circuit.add(finale);
        return circuit;
    }

    /**
     * This method is used to read saved workflows up to version 3.0.
     * <p/>
     * To read more recent workflows, please see WorkflowService
     *
     * @see com.atolcd.parapheur.repo.ParapheurService#loadSavedWorkflow(org.alfresco.service.cmr.repository.NodeRef)
     * @see com.atolcd.parapheur.repo.WorkflowService#getSavedWorkflow(org.alfresco.service.cmr.repository.NodeRef)
     * @deprecated Since 3.1
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    @Override
    public SavedWorkflow loadSavedWorkflow(NodeRef workflow) {
        Assert.isTrue(nodeService.exists(workflow),
                "Le Node Ref passé en paramètre ne correspond pas à un noeud existant");

        SavedWorkflowImpl res = new SavedWorkflowImpl();
        res.setNodeRef(workflow);
        res.setName((String) nodeService.getProperty(workflow, ContentModel.PROP_NAME));
        logger.debug("ParapheurServiceImpl::loadSavedWorkflow, circuit : " + res.getName());
        ContentReader contentreader = contentService.getReader(workflow, ContentModel.PROP_CONTENT);
        SAXReader saxreader = new SAXReader();

        try {
            if (contentreader == null) {
                logger.error("contentReader is NULL.");
            }
            String myString = contentreader.getContentString();
            StringReader stringReader = new StringReader(myString);
            Document document = saxreader.read(stringReader);
            Element rootElement = document.getRootElement();
            // Check l'attribut version: si présent=3.0 ou absent (vieille implémentation!)
            if (rootElement.attributeValue("version") == null) {
                logger.debug("Circuit OLD SCHOOL !!!");
                /**
                 * Attention ici: vieille implémentation avec juste des éléments
                 * "validation" et "notification" Donc: on notifie à TOUTES les
                 * étapes
                 */
                Element validation = rootElement.element("validation");
                if (validation != null) {
                    for (Iterator i = validation.elementIterator("etape"); i.hasNext(); ) {
                        Element etape = (Element) i.next();
                        EtapeCircuit eci = new EtapeCircuitImpl();
                        ((EtapeCircuitImpl) eci).setTransition("PARAPHEUR"); // valeur par défaut
                        ((EtapeCircuitImpl) eci).setParapheur(new NodeRef(etape.getText()));
                        if (i.hasNext()) {
                            ((EtapeCircuitImpl) eci).setActionDemandee(EtapeCircuit.ETAPE_VISA);
                        } else {
                            ((EtapeCircuitImpl) eci).setActionDemandee(EtapeCircuit.ETAPE_SIGNATURE);
                        }
                        Element notification = rootElement.element("notification");
                        if (notification != null) {
                            Set<NodeRef> liste = new HashSet<NodeRef>();
                            for (Iterator j = notification.elementIterator("etape"); j.hasNext(); ) {
                                Element etapeElt = (Element) j.next();
                                liste.add(new NodeRef(etapeElt.getText().trim()));
                            }
                            ((EtapeCircuitImpl) eci).setListeDiffusion(liste);
                        }
                        res.addToCircuit(((EtapeCircuitImpl) eci));
                    }
                }
            } else if (rootElement.attributeValue("version").equalsIgnoreCase("3.0")) {
                logger.debug("Circuit v3");
                // Droits d'usage: acl + groupes
                Element aclElt = rootElement.element("acl");
                if (aclElt != null) {
                    for (Iterator i = aclElt.elementIterator("parapheur"); i.hasNext(); ) {
                        Element parapheurElt = (Element) i.next();
                        res.addToAclParapheurs(new NodeRef(parapheurElt.getText().trim()));
                    }
                }
                Element groupesElt = rootElement.element("groupes");
                if (groupesElt != null) {
                    for (Iterator i = groupesElt.elementIterator("groupe"); i.hasNext(); ) {
                        Element groupeElt = (Element) i.next();
                        res.addToAclGroupes(groupeElt.getText().trim());
                    }
                }
                // Etapes du circuit de validation
                Element etapesElt = rootElement.element("etapes");
                if (etapesElt != null) {
                    for (Iterator i = etapesElt.elementIterator("etape"); i.hasNext(); ) {
                        Element etapeElt = (Element) i.next();
                        EtapeCircuitImpl eci = new EtapeCircuitImpl();
                        eci.setTransition(etapeElt.element("transition").getText().trim());
                        if (EtapeCircuit.TRANSITION_PARAPHEUR.equals(eci.getTransition())) {
                            eci.setParapheur(new NodeRef(etapeElt.element("parapheur").getText().trim()));
                        }
                        eci.setActionDemandee(etapeElt.element("action-demandee").getText().trim());
                        // Liste de diffusion
                        Element diffElt = etapeElt.element("diffusion");
                        if (diffElt != null) {
                            Set<NodeRef> liste = new HashSet<NodeRef>();
                            for (Iterator j = diffElt.elementIterator("noderef"); j.hasNext(); ) {
                                Element nodeElt = (Element) j.next();
                                liste.add(new NodeRef(nodeElt.getText().trim()));
                            }
                            eci.setListeDiffusion(liste);
                        }
                        res.addToCircuit(eci);
                    }
                }
            } else if (rootElement.attributeValue("version").equalsIgnoreCase("3.1")) {
                /*
                 * Quick and dirty patch: If workflow version is 3.1, then
				 * workflowService is called to load the workflow.
				 */
                return workflowService.getSavedWorkflow(workflow);
            }
        } catch (DocumentException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#saveWorkflow(java.lang.String,
     * java.util.List, java.util.Set, boolean)
     */
    @Deprecated
    @Override
    public NodeRef saveWorkflow(String name, List<NodeRef> circuit, Set<NodeRef> diffusion, boolean publicWorkflow) {
        Assert.hasText(name, "Le circuit doit être nommé");

        NodeRef res = null;
        NodeRef circuitsRef = null;
        List<NodeRef> results = null;

        // On prépare le dossier de sauvegarde
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.dictionary.childname") + "/" + this.configuration.getProperty("spaces.savedworkflows.childname");
        try {
            results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
        } catch (AccessDeniedException err) {
            if (logger.isDebugEnabled()) {
                logger.debug("Access denied");
            }
            return null;    // ignore and return null
        }

        if (results != null && results.size() == 1) {
            if (publicWorkflow) {
                circuitsRef = results.get(0);
            } else {   // Circuit privé: recherche du sous-répertoire
                List<ChildAssociationRef> circuitsPrives = nodeService.getChildAssocs(results.get(0),
                        ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
                for (ChildAssociationRef prive : circuitsPrives) {
                    NodeRef rep = prive.getChildRef();
                    // On a trouvé un dossier contenant les circuits privés de l'utilisateur courant
                    if (isOfType(rep, ContentModel.TYPE_FOLDER) && this.authenticationService.getCurrentUserName().equals(
                            nodeService.getProperty(rep, ContentModel.PROP_NAME))) {
                        circuitsRef = rep;
                        break;
                    }
                }

                if (circuitsRef == null) {
                    try {
                        FileInfo info = this.fileFolderService.create(results.get(0), this.authenticationService.getCurrentUserName(), ContentModel.TYPE_FOLDER);
                        circuitsRef = info.getNodeRef();
                        // Mise en place des permissions du répertoire
                        permissionService.setInheritParentPermissions(circuitsRef, false);
                        permissionService.setPermission(circuitsRef, authenticationService.getCurrentUserName(),
                                PermissionService.COORDINATOR, true);
                    } catch (FileExistsException e) {
                        logger.warn("Impossible de créer le répertoire de circuits privés pour l'utilisateur : " + this.authenticationService.getCurrentUserName());
                        return null;
                    }
                }
            }

            try {
                // On vérifie si le circuit existe déjà
                res = nodeService.getChildByName(circuitsRef, ContentModel.ASSOC_CONTAINS, name);
                if (res == null) {
                    // Création du noeud
                    FileInfo fileInfo = fileFolderService.create(circuitsRef, name, ContentModel.TYPE_CONTENT);
                    res = fileInfo.getNodeRef();
                    permissionService.setInheritParentPermissions(res, false);
                    permissionService.setPermission(res, authenticationService.getCurrentUserName(),
                            PermissionService.COORDINATOR, true);
                    permissionService.setPermission(res, "GROUP_EVERYONE", PermissionService.CONSUMER, true);
                }
            } catch (FileExistsException e) {
                logger.warn("Erreur lors de la modification du circuit");
                return null;
            }

            // Préparation des données XML
            String xmlData = null;
            Document doc = DocumentHelper.createDocument();
            Element root = doc.addElement("circuit");

            Element validation = root.addElement("validation");
            for (NodeRef etape : circuit) {
                validation.addElement("etape").addText(etape.toString());
            }

            Element notification = root.addElement("notification");
            for (NodeRef notifie : diffusion) {
                notification.addElement("etape").addText(notifie.toString());
            }

            try {
                StringWriter out = new StringWriter(1024);
                XMLWriter writerTmp = new XMLWriter(OutputFormat.createPrettyPrint());
                writerTmp.setWriter(out);
                writerTmp.write(doc);
                xmlData = out.toString();

                // Ecriture des données XML dans le nouveau noeud
                ContentWriter writer = this.contentService.getWriter(res, ContentModel.PROP_CONTENT, true);
                writer.setMimetype(MimetypeMap.MIMETYPE_XML);
                writer.setEncoding("UTF-8");
                writer.putContent(xmlData);
            } catch (IOException e) {
                if (nodeService.exists(res)) {
                    fileFolderService.delete(res);
                }
                return null;
            }
        }

        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#saveWorkflow(java.lang.String,
     * java.util.List, java.util.Set, java.util.Set, boolean)
     */
    @Deprecated
    @Override
    public NodeRef saveWorkflow(String name, List<EtapeCircuit> circuit, Set<NodeRef> acl, Set<String> groupes, boolean publicCircuit) {
        Assert.hasText(name, "Le circuit doit être nommé");

        NodeRef res = null;
        NodeRef circuitsRef = null;
        List<NodeRef> results = null;

        // Dossier de sauvegarde
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.dictionary.childname") + "/" + this.configuration.getProperty("spaces.savedworkflows.childname");
        try {
            results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
        } catch (AccessDeniedException err) {
            if (logger.isDebugEnabled()) {
                logger.debug(err.getLocalizedMessage());
            }
            return null;    // on ignore
        }

        if (results != null && results.size() == 1) {
            circuitsRef = results.get(0);

            try {
                // On vérifie si le circuit existe déjà
                res = nodeService.getChildByName(circuitsRef, ContentModel.ASSOC_CONTAINS, name);
                if (res == null) {
                    // Création du noeud
                    FileInfo fileInfo = fileFolderService.create(circuitsRef, name, ParapheurModel.TYPE_SAVED_WORKFLOW);
                    res = fileInfo.getNodeRef();
                    permissionService.setInheritParentPermissions(res, false);
                    permissionService.setPermission(res, authenticationService.getCurrentUserName(),
                            PermissionService.COORDINATOR, true);
                    permissionService.setPermission(res, "GROUP_EVERYONE", PermissionService.CONSUMER, true);
                }
            } catch (FileExistsException e) {
                logger.warn("Erreur lors de la modification du circuit");
                return null;
            }

            if (!publicCircuit) {
                Map<QName, Serializable> privProps = new HashMap<QName, Serializable>();
                privProps.put(ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS, (Serializable) acl);
                privProps.put(ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_GROUPS, (Serializable) groupes);
                nodeService.addAspect(res, ParapheurModel.ASPECT_PRIVATE_WORKFLOW, privProps);
            } else if (nodeService.hasAspect(res, ParapheurModel.ASPECT_PRIVATE_WORKFLOW)) {
                nodeService.removeAspect(res, ParapheurModel.ASPECT_PRIVATE_WORKFLOW);
            }

            // Construction arbre XML
            String xmlData = null;
            Document doc = DocumentHelper.createDocument();
            Element root = doc.addElement("circuit").addAttribute("version", "3.0");

            //  droits d'usage si !=public
            if (!publicCircuit) {
                if (acl != null) {
                    Element aclElt = root.addElement("acl");
                    for (NodeRef elt : acl) {
                        aclElt.addElement("parapheur").addText(elt.toString());
                    }
                }
                if (groupes != null) {
                    Element grpElt = root.addElement("groupes");
                    for (String elt : groupes) {
                        if (!elt.equalsIgnoreCase("GESTIONNAIRE_CIRCUITS_IPARAPHEUR")) {
                            grpElt.addElement("groupe").addText(elt);
                        }
                    }
                }
            }
            // Etapes du circuit
            Element etapes = root.addElement("etapes");
            for (EtapeCircuit etape : circuit) {
                Element etapeElt = etapes.addElement("etape");
                // Objet: parapheur, 'chef de'
                etapeElt.addElement("transition").addText(etape.getTransition());
                if (EtapeCircuit.TRANSITION_PARAPHEUR.equals(etape.getTransition())) {
                    etapeElt.addElement("parapheur").addText(etape.getParapheur().toString());
                }
                // Action demandée
                etapeElt.addElement("action-demandee").addText(etape.getActionDemandee());
                // liste de notification !
                Element diffusionElt = etapeElt.addElement("diffusion");
                if (etape.getListeNotification() != null) {
                    for (NodeRef notifie : etape.getListeNotification()) {
                        diffusionElt.addElement("noderef").addText(notifie.toString());
                    }
                }
            }

            try {   // transformation XML==>String
                StringWriter out = new StringWriter(1024);
                XMLWriter writerTmp = new XMLWriter(OutputFormat.createPrettyPrint());
                writerTmp.setWriter(out);
                writerTmp.write(doc);
                xmlData = out.toString();

                // Ecriture des données XML dans le nouveau noeud
                ContentWriter writer = this.contentService.getWriter(res, ContentModel.PROP_CONTENT, true);
                writer.setMimetype(MimetypeMap.MIMETYPE_XML);
                writer.setEncoding("UTF-8");
                writer.putContent(xmlData);
                if (!publicCircuit) {   // Aspect pour ACL
                    Map<QName, Serializable> ppties = new HashMap<QName, Serializable>();
                    if (acl != null) {
                        StringBuilder str = new StringBuilder("");
                        for (NodeRef elt : acl) {
                            str.append(elt.toString()).append(",");
                        }
                        ppties.put(ParapheurModel.PROP_CIRCUIT_ACL_PARAPHEURS, str.toString());
                    }
                    if (groupes != null) {
                        StringBuilder str = new StringBuilder("");
                        for (String elt : groupes) {
                            if (!elt.equalsIgnoreCase("GESTIONNAIRE_CIRCUITS_IPARAPHEUR")) {
                                str.append(elt).append(",");
                            }
                        }
                        ppties.put(ParapheurModel.PROP_CIRCUIT_ACL_GROUPES, str.toString());
                    }
                    nodeService.addAspect(res, ParapheurModel.ASPECT_CIRCUIT_ACCESS_LISTE, ppties);
                }
            } catch (IOException e) {
                if (nodeService.exists(res)) {
                    fileFolderService.delete(res);
                }
                return null;
            }
        }
        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isGestionnaireCircuits(String)
     */
    @Override
    public boolean isGestionnaireCircuits(String userName) {
        Set<String> authorities = this.authorityService.getAuthoritiesForUser(userName);
        // Dans Alfresco les groupes sont préfixés par 'GROUP_', d'où la chaine ci-dessous
        return authorities.contains("GROUP_GESTIONNAIRE_CIRCUITS_IPARAPHEUR");
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isRecuperable(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isRecuperable(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");
        EtapeCircuit etapeCourante = getCurrentEtapeCircuit(dossier);
        if (etapeCourante == null) {
            // Allo Houston , on a un problème je crois
            if (logger.isEnabledFor(Level.WARN)) {
                logger.warn("isRecuperable: impossible d'avoir l'etape courante!??");
            }
            return false;
        }
        if (etapeCourante.getActionDemandee().equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC)) {
            return s2lowService.isMailSecJobCancelable(dossier);
        } else if (etapeCourante.getActionDemandee().equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC_PASTELL) && nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID) != null) {
            return true;
        } else {
            return (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_RECUPERABLE);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getParapheurResponsable(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public NodeRef getParapheurResponsable(NodeRef parapheur) {
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");

        List<AssociationRef> hierarchie = nodeService.getTargetAssocs(parapheur, ParapheurModel.ASSOC_HIERARCHIE);

        if (hierarchie.size() == 1) {
            return hierarchie.get(0).getTargetRef();
        } else {
            return null;
        }
    }

    /**
     * Permet de d'ajouter ou de supprimer une délégation active; lors d'un
     * ajout, les dossiers de la corbeille "A traiter" sont référencés chez le
     * délégué si la propriété PROP_DELEGUER_PRESENTS est vraie.
     */
    public void setDelegation(NodeRef parapheur, NodeRef parapheurDelegue) {
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");

        if (parapheurDelegue != null) {
            Assert.isTrue(isParapheur(parapheurDelegue), "NodeRef doit être un ph:parapheur");
        }

        List<NodeRef> anciensDelegues = getParapheursOnDelegationPath(parapheur);


        NodeRef ancienDelegue = getDelegation(parapheur);
        // On supprime la délégation passée si elle existe
        if (ancienDelegue != null) {
            nodeService.removeAssociation(parapheur, ancienDelegue, ParapheurModel.ASSOC_DELEGATION);
            Set<String> containedAuthorities = authorityService.getContainedAuthorities(AuthorityType.ROLE, "ROLE_PHDELEGATES_" + parapheur.getId(), true);
            // Si l'admin a changé le nom du parapheur, les roles associés au parapheurName seront vides..
            // TODO : utiliser les noderef plutot que le name du parapheur dans les roles.
            if (containedAuthorities.contains("ROLE_PHDELEGATES_" + ancienDelegue.getId())) {
                authorityService.removeAuthority("ROLE_PHDELEGATES_" + parapheur.getId(), "ROLE_PHDELEGATES_" + ancienDelegue.getId());
            }
            if (containedAuthorities.contains("ROLE_PHOWNER_" + ancienDelegue.getId())) {
                authorityService.removeAuthority("ROLE_PHDELEGATES_" + parapheur.getId(), "ROLE_PHOWNER_" + ancienDelegue.getId());
            }
        }

        // On supprime la délégation programmée si elle existe
        List<AssociationRef> delegationsProgrammees = nodeService.getTargetAssocs(parapheur, ParapheurModel.ASSOC_DELEGATION_PROGRAMMEE);
        for (AssociationRef delegationProgramme : delegationsProgrammees) {
            nodeService.removeAssociation(parapheur, delegationProgramme.getTargetRef(), ParapheurModel.ASSOC_DELEGATION_PROGRAMMEE);
        }

        NodeRef aTraiter = getCorbeille(parapheur, ParapheurModel.NAME_A_TRAITER);
        NodeRef CorbeilleDossiersDelegues = getCorbeille(parapheur, ParapheurModel.NAME_DOSSIERS_DELEGUES);
        List<NodeRef> dossiersATraiter = getDossiers(aTraiter);

        // Suppression des anciennes associations
        if (ancienDelegue != null) {
            NodeRef corbeilleAncienDelegue = getCorbeille(ancienDelegue, ParapheurModel.NAME_DOSSIERS_DELEGUES);
            if (corbeilleAncienDelegue != null) {
                // On supprime les références vers la corbeille "Dossiers en délégation"
                List<AssociationRef> references = nodeService.getSourceAssocs(CorbeilleDossiersDelegues, ParapheurModel.ASSOC_VIRTUALLY_REFERS);
                for (AssociationRef reference : references) {
                    if (reference.getSourceRef().equals(corbeilleAncienDelegue)) {
                        nodeService.removeAssociation(reference.getSourceRef(), CorbeilleDossiersDelegues, ParapheurModel.ASSOC_VIRTUALLY_REFERS);
                    }
                }
                /* On supprime tous les liens de la corbeille "dossiers délégués" de
                 * l'ancien délégué qui référencent des dossiers de la corbeille
				 * "A traiter" de parapheur.
				 */
                List<AssociationRef> assocs = nodeService.getTargetAssocs(corbeilleAncienDelegue, ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
                if ((dossiersATraiter != null) && !dossiersATraiter.isEmpty()) {
                    for (AssociationRef assoc : assocs) {
                        NodeRef dossierTarget = assoc.getTargetRef();
                        if (dossiersATraiter.contains(dossierTarget)) {
                            nodeService.removeAssociation(assoc.getSourceRef(),
                                    assoc.getTargetRef(),
                                    assoc.getTypeQName());
                        }
                    }
                }
                // Mise à jour du nombre de dossiers délégué sur l'ancien chemin de délégation
                for (NodeRef delegue : anciensDelegues) {
                    NodeRef corbeilleDelegue = getCorbeille(delegue, ParapheurModel.NAME_DOSSIERS_DELEGUES);
                    corbeillesService.updateCorbeilleChildCount(corbeilleDelegue);
                }
                if ((parapheurDelegue != null) && (!ancienDelegue.equals(parapheurDelegue))) {
                    try {
                        notificationService.notifierPourDelegation(parapheur, ancienDelegue, false);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (AuthenticationException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        // Creation du nouveau délégué
        if (parapheurDelegue != null) {
            NodeRef corbeilleNouveauDelegue = getCorbeille(parapheurDelegue, ParapheurModel.NAME_DOSSIERS_DELEGUES);

            nodeService.createAssociation(parapheur, parapheurDelegue, ParapheurModel.ASSOC_DELEGATION);
            List<AssociationRef> listAssoc = nodeService.getTargetAssocs(parapheur, ParapheurModel.ASSOC_OLD_DELEGATION);
            for (AssociationRef assoRef : listAssoc) {
                nodeService.removeAssociation(parapheur, assoRef.getTargetRef(), ParapheurModel.ASSOC_OLD_DELEGATION);
            }
            nodeService.createAssociation(parapheur, parapheurDelegue, ParapheurModel.ASSOC_OLD_DELEGATION);
            /* Création de la référence vers la corbeille "Dossiers en délégation"
             * du délégateur pour le chainage des délégations.
			 */
            nodeService.createAssociation(corbeilleNouveauDelegue, CorbeilleDossiersDelegues, ParapheurModel.ASSOC_VIRTUALLY_REFERS);
            /* Creation des nouvelles associations si les dossiers déjà présents
             * dans la corbeille "A traiter" doivent être pris en compte (propriété
			 * PROP_DELEGUER_PRESENTS à vrai)
			 */
            if (arePresentsDelegues(parapheur)) {
                if (dossiersATraiter != null) {
                    for (NodeRef dossier : dossiersATraiter) {
                        nodeService.createAssociation(corbeilleNouveauDelegue,
                                dossier,
                                ParapheurModel.ASSOC_VIRTUALLY_CONTAINS);
                    }
                }
            }
            // Mise à jour du nombre de dossiers délégué sur le nouveau chemin de délégation
            for (NodeRef delegue : getParapheursOnDelegationPath(parapheur)) {
                NodeRef corbeilleDelegue = getCorbeille(delegue, ParapheurModel.NAME_DOSSIERS_DELEGUES);
                corbeillesService.updateCorbeilleChildCount(corbeilleDelegue);
            }
            authorityService.addAuthority("ROLE_PHDELEGATES_" + parapheur.getId(), "ROLE_PHDELEGATES_" + parapheurDelegue.getId());
            authorityService.addAuthority("ROLE_PHDELEGATES_" + parapheur.getId(), "ROLE_PHOWNER_" + parapheurDelegue.getId());

            if (!parapheurDelegue.equals(ancienDelegue)) {
                try {
                    notificationService.notifierPourDelegation(parapheur, parapheurDelegue, true);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (AuthenticationException e) {
                    e.printStackTrace();
                }
            }
        }

        if (logger.isInfoEnabled() && parapheurDelegue != null) {
            logger.info("Parapheur '" + getNomParapheur(parapheur) + "' est delegue aupres de '" + getNomParapheur(parapheurDelegue) + "'.");
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#programmerDelegation(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef, java.util.Date, java.util.Date, boolean)
     */
    @Override
    public void programmerDelegation(NodeRef parapheur, NodeRef parapheurDelegue, Date debut, Date fin, boolean deleguerPresents) {

        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");
        Assert.isTrue(isParapheur(parapheurDelegue), "NodeRef doit être un ph:parapheur");
        Date now = new Date();
        Assert.isTrue((fin == null) || (now.before(fin) && ((debut == null) || debut.before(fin))),
                "Le créneau de programmation est invalide");
        Assert.isTrue(!willDelegationLoop(parapheur, parapheurDelegue, debut, fin),
                "Impossible choisir cette délégation, cela créerait une boucle");

        nodeService.setProperty(parapheur, ParapheurModel.PROP_DELEGUER_PRESENTS, deleguerPresents);

        if ((debut != null) && now.before(debut)) { // délégation programmée
            /* suppression de la délégation active (si il y en a une)
             * On n'utilise pas supprimerDelegation() car la propriété
			 * PROP_DELEGUER_PRESENTS ne doit pas être enlevée.
			 */
            setDelegation(parapheur, null);
            // Mise en place de la délégation programmée
            nodeService.createAssociation(parapheur, parapheurDelegue, ParapheurModel.ASSOC_DELEGATION_PROGRAMMEE);
            nodeService.setProperty(parapheur, ParapheurModel.PROP_DATE_DEBUT_DELEGATION, debut);
            nodeService.setProperty(parapheur, ParapheurModel.PROP_DATE_FIN_DELEGATION, fin);
        } else { // délégation active
            // Mise en place de la délégation active
            setDelegation(parapheur, parapheurDelegue);
            nodeService.setProperty(parapheur, ParapheurModel.PROP_DATE_DEBUT_DELEGATION, debut);
            nodeService.setProperty(parapheur, ParapheurModel.PROP_DATE_FIN_DELEGATION, fin);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#supprimerDelegation(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void supprimerDelegation(NodeRef parapheur) {
        nodeService.setProperty(parapheur, ParapheurModel.PROP_DELEGUER_PRESENTS, false);
        setDelegation(parapheur, null);
        nodeService.setProperty(parapheur, ParapheurModel.PROP_DATE_DEBUT_DELEGATION, null);
        nodeService.setProperty(parapheur, ParapheurModel.PROP_DATE_FIN_DELEGATION, null);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#updateDelegation(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void updateDelegation(NodeRef parapheur) {

        String nomParapheur = getNomParapheur(parapheur);
        logger.info("[updateDelegation] Traitement du parapheur : " + nomParapheur + " (id = " + parapheur.getId() + ")");

        Date date = new Date();
        Date debut = (Date) nodeService.getProperty(parapheur, ParapheurModel.PROP_DATE_DEBUT_DELEGATION);
        Date fin = (Date) nodeService.getProperty(parapheur, ParapheurModel.PROP_DATE_FIN_DELEGATION);
        if (debut == null) { // pas de délégation
            logger.info("[updateDelegation] Pas de délégation trouvée");
            return;
        }
        NodeRef delegue = null;
        List<AssociationRef> listAssoc = nodeService.getTargetAssocs(parapheur, ParapheurModel.ASSOC_DELEGATION);
        if (listAssoc != null && listAssoc.size() == 1) {
            delegue = listAssoc.get(0).getTargetRef();
        }

        NodeRef delegueProgramme = getDelegationProgrammee(parapheur);

        if ((fin != null) && date.after(fin)) {
            // la delegation active est arrivée à terme.
            logger.info("[updateDelegation] Fin de la délégation programmée de " + parapheur.getId());
            supprimerDelegation(parapheur);
        } else if ((delegueProgramme != null) && date.after(debut)) {
            if ((fin == null) || date.before(fin)) {
                // la délégation programmée est arrivée.
                logger.info("[updateDelegation] Début de la délégation programmée de "+ parapheur.getId());
                setDelegation(parapheur, delegueProgramme);
            } else {
                // la délégation programmée a été loupée..
                logger.info("[updateDelegation] Délégation loupée pour " + parapheur.getId());
                supprimerDelegation(parapheur);
            }
        }

        logger.info("[updateDelegation] Traitement terminé");
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getDelegationProgrammee(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public NodeRef getDelegationProgrammee(NodeRef parapheur) {
        if (parapheur == null) {
            return null;
        }
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");
        NodeRef delegueProgramme = null;
        List<AssociationRef> listAssoc = nodeService.getTargetAssocs(parapheur, ParapheurModel.ASSOC_DELEGATION_PROGRAMMEE);
        if (listAssoc != null && listAssoc.size() == 1) {
            delegueProgramme = listAssoc.get(0).getTargetRef();
        }
        return delegueProgramme;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getDelegation(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public NodeRef getDelegation(NodeRef parapheur) {
        if (parapheur == null) {
            return null;
        }
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");

        List<AssociationRef> listAssoc = nodeService.getTargetAssocs(parapheur, ParapheurModel.ASSOC_DELEGATION);
        if (listAssoc != null && listAssoc.size() == 1) {
            return listAssoc.get(0).getTargetRef();
        }

        return null;
    }

    /**
     * @see ParapheurService#getOldDelegation(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public NodeRef getOldDelegation(NodeRef parapheur) {
        if (parapheur == null) {
            return null;
        }

        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");

        List<AssociationRef> listAssoc = nodeService.getTargetAssocs(parapheur, ParapheurModel.ASSOC_OLD_DELEGATION);
        if (listAssoc != null && listAssoc.size() == 1) {
            return listAssoc.get(0).getTargetRef();
        }

        return null;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isDelegate(org.alfresco.service.cmr.repository.NodeRef,
     * String)
     */
    @Override
    public boolean isDelegate(NodeRef dossier, String userName) { // FIXME : revoir la recherche
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");
        NodeRef parapheur = getParentParapheur(dossier);
        boolean trouve = isParapheurDelegate(parapheur, userName);

		/* TODO : récupérer les dossiers de la corbeille "Dossiers en
         * délégation" de l'utilisateur et vérifier si dossier est dedans
		 * (seulement si la fonctionnalité du choix des dossiers à déléguer
		 * est en place).
		 */
        return trouve;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#arePresentsDelegues(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean arePresentsDelegues(NodeRef parapheur) {
        Boolean delegues = (Boolean) nodeService.getProperty(parapheur, ParapheurModel.PROP_DELEGUER_PRESENTS);
        return ((delegues != null) ? delegues : false);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getTitulaires(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public List<NodeRef> getTitulaires(NodeRef parapheur) {
        List<NodeRef> titulaires = new ArrayList<NodeRef>();
        List<AssociationRef> delegations = nodeService.getSourceAssocs(parapheur, ParapheurModel.ASSOC_DELEGATION);
        for (AssociationRef delegation : delegations) {
            titulaires.add(delegation.getSourceRef());
        }
        return titulaires;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getTitulairesOnDelegationPath(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public List<NodeRef> getTitulairesOnDelegationPath(NodeRef parapheur) {
        List<NodeRef> titulaires = new ArrayList<NodeRef>();
        List<AssociationRef> delegations = nodeService.getSourceAssocs(parapheur, ParapheurModel.ASSOC_DELEGATION);
        for (AssociationRef delegation : delegations) {
            titulaires.addAll(getTitulairesOnDelegationPath(delegation.getSourceRef()));
        }
        return titulaires;
    }

    /**
     * @see ParapheurService#setPHAdminAuthoritiesForUser(java.lang.String,
     * java.util.List)
     */
    @Override
    public String setPHAdminAuthoritiesForUser(String username, List<String> prfList) {
        Set<String> authSet = null;
        /**
         * Controle ULTRA-qualite sanitaire: virer tous les orphelins de type ROLE
         */
        System.out.println("All USER authorities: " + authorityService.getAllAuthorities(AuthorityType.USER));
        authSet = authorityService.getAllAuthorities(AuthorityType.ROLE);
        System.out.println("All ROLE authorities: " + authSet);
        if (!authSet.isEmpty()) {
            for (String auth : authSet) {
                if (auth.startsWith("ROLE_PHADMIN_")) {
                    System.out.println("######>>> Auth '" + auth + "' ##########");
                    if (authorityService.authorityExists(auth)) {
                        System.out.println("\t  ROLEs, true : " + authorityService.getContainedAuthorities(AuthorityType.ROLE, auth, true)
                                + ",  false: " + authorityService.getContainedAuthorities(AuthorityType.ROLE, auth, false));
                        System.out.println("\t  USERs, true : " + authorityService.getContainedAuthorities(AuthorityType.USER, auth, true)
                                + ",  false: " + authorityService.getContainedAuthorities(AuthorityType.USER, auth, false));
                        Set<String> lesCandidats = authorityService.getContainedAuthorities(AuthorityType.USER, auth, false);
                        if (!lesCandidats.isEmpty()) {
                            System.out.println("\t" + auth + ": il y a encore " + lesCandidats.size() + " USER: " + lesCandidats);
                        } else {
                            System.out.println("\t" + auth + ": est vide. Purge??");
                            /*
                             * purge
							 */
                            //                            while (this.authorityService.authorityExists(auth)) {
                            //                                System.out.println("\tDeleting '" + auth + "'. ");
                            //                                this.authorityService.deleteAuthority(auth, true);
                            //                            }
                        }
                    } else {
                        System.out.println("SHOULD NEVER EVER HAPPEN:  Auth " + auth + " does not exist: ignoring");
                    }
                } else {
                    logger.debug(" Auth '" + auth + "' did not start with 'ROLE_PHADMIN_': ignoring");
                }
            }
        }
        /**
         * Controle qualite sanitaire sur les ROLE affectés à 'username'
         */
        authSet = authorityService.getAuthoritiesForUser(username);
        System.out.println("Begin with username : " + username + ", Authorities=" + authSet);

        if (prfList == null || prfList.isEmpty()) {
            System.out.println("#### Empty List for " + username);
            /**
             * purger les autorités détenues par cet utilisateur
             */
            for (String auth : authSet) {
                if (auth.startsWith("ROLE_PHADMIN_")) {
                    authorityService.removeAuthority(auth, username);
                    System.out.println("removeAuthority for tuple " + auth + ", " + username);
                    Set<String> otherAdmins = authorityService.getContainedAuthorities(AuthorityType.USER, auth, false);
                    // SI il ne reste plus personne, on supprime le role.
                    if (otherAdmins.isEmpty()) {
                        authorityService.deleteAuthority(auth);
                    }
                }
            }
        } else {
            /**
             * Bestial: purger _toutes_ les autorités avant de les repositionner
             */
            System.out.println("##### REMEMBER Auth REAL-ID for '" + username + "' is : " + authorityService.getName(AuthorityType.USER, username));
            Set<String> lesCandidats;
            for (String auth : authSet) {
                if (auth.startsWith("ROLE_PHADMIN_")) {
                    System.out.println("\n######>>> Auth '" + auth + "' ##########");
                    while (authorityService.authorityExists(auth)
                            && authorityService.getContainedAuthorities(AuthorityType.USER, auth, true).contains(username)) {
                        /**
                         * Traiter les eventuelles authorities de type USER
                         */
                        lesCandidats = authorityService.getContainedAuthorities(AuthorityType.USER, auth, false);
                        if (lesCandidats.contains(username)) {
                            authorityService.removeAuthority(auth, username);
                            System.out.println("USER] removeAuthority for tuple '" + auth + "', " + username);

                            Set<String> ceuxQuiRestent = authorityService.getContainedAuthorities(AuthorityType.USER, auth, false);
                            System.out.println("USER] Size of " + auth + " auth = " + ceuxQuiRestent.size());
                            if (ceuxQuiRestent.isEmpty()) {
                                authorityService.deleteAuthority(auth);
                                System.out.println("USER] deleteAuthority " + auth);
                            }
                        } else if (lesCandidats.isEmpty()) {
                            System.out.println("USER] WTF???? Auth " + auth + " does not contain any USER authorities? PURGE ???!");
                            System.out.println("USER] REMEMBER that " + username + " is known to be:\n\tin these ROLE types: "
                                    + this.authorityService.getContainingAuthorities(AuthorityType.ROLE, username, true) + "\n\tin these USER types: "
                                    + this.authorityService.getContainingAuthorities(AuthorityType.USER, username, true));
                            authorityService.deleteAuthority(auth);
                            System.out.println("USER] deleteAuthority " + auth);
                        } else {
                            System.out.println("USER] Attention: Auth " + auth + " does not contain '" + username + "', but instead has\n\t" + lesCandidats);
                        }
                    }
                } else {
                    logger.debug(" Auth '" + auth + "' did not start with 'ROLE_PHADMIN_': ignoring");
                }
            }
            /**
             * Repeuplement...
             */
            System.out.println("\t On veut ajouter: " + prfList);
            for (String prfString : prfList) {
                if (prfString != null && !prfString.trim().isEmpty()) {
                    /**
                     * 1: verif existence (NB: PHADMIN ou ROLE_PHADMIN ???)
                     */
                    if (!authorityService.authorityExists("ROLE_PHADMIN_" + prfString)) {
                        String newAuth = authorityService.createAuthority(AuthorityType.ROLE, "PHADMIN_" + prfString);
                        System.out.println("\t\tCreate missing Authority => " + newAuth);
                        /**
                         * 2: ajouter l'utilisateur si absent (NB: forcement absent, on a tout pété à la boucle precedente)
                         */
                        authorityService.addAuthority("ROLE_PHADMIN_" + prfString, authorityService.getName(AuthorityType.USER, username));
                        System.out.println("\t\tPut " + authorityService.getName(AuthorityType.USER, username) + " authority in ROLE_PHADMIN_" + prfString);
                    } else {
                        authorityService.addAuthority("ROLE_PHADMIN_" + prfString, authorityService.getName(AuthorityType.USER, username));
                        System.out.println("\t\tAuthority ROLE_PHADMIN_" + prfString + " already exists");
                    }

                } else {
                    logger.debug(" Bizarre: entree noderef vide????");
                }
            }
        }
        return "y";
        //                /**
        //                 * Update the ROLE_PHOWNER corresponding to this parapheur
        //                 */
        //                String oldName = "", nom="";
        //                if (authorityService.authorityExists("ROLE_PHOWNER_" + oldName)) {
        //                    Set<String> oldUsers = authorityService.getContainedAuthorities(AuthorityType.USER,  "ROLE_PHOWNER_" + oldName, false);       //was:   "ROLE_PHOWNER_" + this.nom, false);
        //                    for (String oldUser : oldUsers) {
        //                        authorityService.removeAuthority("ROLE_PHOWNER_" + oldName, oldUser);         //was:  authorityService.removeAuthority("ROLE_PHOWNER_" + this.nom, oldUser);
        //                        System.out.println("removeAuthority for tuple ROLE_PHOWNER_" + oldName + ", " + oldUser);
        //                    }
        //                    oldUsers = authorityService.getContainedAuthorities(AuthorityType.USER, "ROLE_PHOWNER_" + oldName, false);
        //
        //                    System.out.println("Size of ROLE_PHOWNER_" + oldName + " auth = " + oldUsers.size());
        //                    if (!oldName.equals(nom) && oldUsers.isEmpty()) {
        //                        authorityService.deleteAuthority("ROLE_PHOWNER_" + oldName);
        //                        System.out.println("deleteAuthority of ROLE_PHOWNER_" + oldName);
        //                    }
        //                }
        //                //  if (!oldName.equals(this.nom)) {
        //                if (!authorityService.authorityExists("PHOWNER_" + nom)) {
        //                    authorityService.createAuthority(AuthorityType.ROLE, "PHOWNER_" + nom);
        //                }
        //
        //                if (this.proprietaires != null && !this.proprietaires.isEmpty()) {
        //                    for (String currentProprietaire : this.proprietaires) {
        //                        authorityService.addAuthority("ROLE_PHOWNER_" + this.nom, authorityService.getName(AuthorityType.USER, currentProprietaire));
        //                    }
        //                }

    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getSecretariatParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> getSecretariatParapheur(NodeRef parapheur) {
        List<String> lstSecretaires;
        if (nodeService.getProperty(parapheur, ParapheurModel.PROP_SECRETAIRES) != null) {
            lstSecretaires = (List<String>) nodeService.getProperty(parapheur, ParapheurModel.PROP_SECRETAIRES);
        } else {
            lstSecretaires = Collections.<String>emptyList();
        }

        return lstSecretaires;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getDelegationsPossibles(org.alfresco.service.cmr.repository.NodeRef)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<NodeRef> getDelegationsPossibles(NodeRef parapheur) {
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");
        List<NodeRef> delegationsPossibles = new ArrayList<NodeRef>();
        if (nodeService.getProperty(parapheur, ParapheurModel.PROP_DELEGATIONS_POSSIBLES) != null) {
            delegationsPossibles = (ArrayList<NodeRef>) nodeService.getProperty(parapheur, ParapheurModel.PROP_DELEGATIONS_POSSIBLES);
        } else {
            delegationsPossibles = (ArrayList<NodeRef>) getParapheurs();
            delegationsPossibles.remove(parapheur);
        }
        return delegationsPossibles;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#setDelegationsPossibles(org.alfresco.service.cmr.repository.NodeRef,
     * java.util.List)
     */
    @Override
    public void setDelegationsPossibles(NodeRef parapheur, List<NodeRef> ListdelegationsPossibles) {
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");
        ArrayList<NodeRef> delegationsPossibles = new ArrayList<NodeRef>(ListdelegationsPossibles);
        Assert.isTrue(!delegationsPossibles.contains(parapheur), "La délégation d'un bureau vers lui-même est impossible");
        NodeRef delegation = getDelegation(parapheur);
        /* Si il y a une délégation en cours, on ne peut pas
         * enlever le bureau concerné des délégations possibles.
		 */
        if (delegation != null) {
            Assert.isTrue(delegationsPossibles.contains(delegation), "Un bureau vers qui vous deleguez ne peut être enlevé des délégations possibles");
        }

        nodeService.setProperty(parapheur, ParapheurModel.PROP_DELEGATIONS_POSSIBLES, delegationsPossibles);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getDateDebutDelegation(org.alfresco.service.cmr.repository.NodeRef)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Date getDateDebutDelegation(NodeRef parapheur) {
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");
        return (Date) nodeService.getProperty(parapheur, ParapheurModel.PROP_DATE_DEBUT_DELEGATION);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getDateFinDelegation(org.alfresco.service.cmr.repository.NodeRef)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Date getDateFinDelegation(NodeRef parapheur) {
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");
        return (Date) nodeService.getProperty(parapheur, ParapheurModel.PROP_DATE_FIN_DELEGATION);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getParapheursOnDelegationPath(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public List<NodeRef> getParapheursOnDelegationPath(NodeRef parapheur) {

        // On parcourt l'arbre de délégation et on ajoute chaque parapheur rencontré
        // à un Set. Si on a déjà rencontré le parapheur, c'est qu'il y a une boucle
        // et on lève donc une exception.
        List<NodeRef> delegues = new ArrayList<NodeRef>();
        HashSet<NodeRef> arbreDelegations = new HashSet<NodeRef>();
        NodeRef parapheurCourant = parapheur;
        NodeRef delegue = getDelegation(parapheurCourant);
        boolean boucle = false;
        while (!boucle && (delegue != null)) {
            if (arbreDelegations.add(parapheurCourant)) {
                delegues.add(delegue);
            } else {
                boucle = true;
                delegues = new ArrayList<NodeRef>();
            }
            parapheurCourant = delegue;
            delegue = getDelegation(parapheurCourant);
        }
        return delegues;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getUsersOnDelegationPath(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public List<String> getUsersOnDelegationPath(NodeRef parapheur) {
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");

        // On parcourt l'arbre de délégation et on ajoute chaque parapheur rencontré
        // à un Set. Si on a déjà rencontré le parapheur, c'est qu'il y a une boucle
        // et on lève donc une exception.
        Set<NodeRef> arbreDelegations = new HashSet<NodeRef>();
        Set<String> delegues = new HashSet<String>();
        NodeRef parapheurCourant = parapheur;
        NodeRef delegue = getDelegation(parapheurCourant);
        boolean boucle = false;
        while (!boucle && (delegue != null)) {
            if (arbreDelegations.add(parapheurCourant)) {
                delegues.addAll(getParapheurOwners(delegue));
            } else {
                boucle = true;
                delegues = new HashSet<String>();
            }
            parapheurCourant = delegue;
            delegue = getDelegation(parapheurCourant);
        }
        return new ArrayList<String>(delegues);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#moveDossier(org.alfresco.service.cmr.repository.NodeRef,
     * org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void moveDossier(NodeRef dossier, NodeRef parapheur) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");
        Assert.isTrue(!isTermine(dossier), "Le dossier est \"terminé\" : id=" + dossier);
        Assert.isTrue(isParapheur(parapheur), "NodeRef doit être un ph:parapheur");

        // Modification de l'étape courante
        EtapeCircuitImpl etapeCourante = (EtapeCircuitImpl) this.getCurrentEtapeCircuit(dossier);
        NodeRef bureauPrecedent = etapeCourante.getParapheur();
        nodeService.setProperty(etapeCourante.getNodeRef(), ParapheurModel.PROP_PASSE_PAR, parapheur);

        // Déplacement du dossier
        NodeRef corbeille = null;
        ChildAssociationRef oldParentAssoc = nodeService.getPrimaryParent(dossier);
        QName corbeilleQName = nodeService.getPrimaryParent(oldParentAssoc.getParentRef()).getQName();
        corbeille = getCorbeille(parapheur, corbeilleQName);
        Assert.notNull(oldParentAssoc, "Le dossier n'a pas de parent: id = " + dossier);
        try {
            nodeService.moveNode(dossier, corbeille, oldParentAssoc.getTypeQName(), oldParentAssoc.getQName());
        } catch (DuplicateChildNodeNameException ex) {
            if (logger.isEnabledFor(Level.WARN)) {
                logger.warn("Impossible de transmettre le dossier : un dossier de même nom existe dans le bureau de destination");
            }
            throw new RuntimeException("Un dossier de même nom existe dans le bureau de destination", ex);
        }
        // Résolution des nouvelles permissions
        dossierService.setPermissionsDossier(dossier);

        // Envoi d'un mail à l'utilisateur concerné
        try {
            notificationService.notifierPourDeplacement(dossier, corbeilleQName.getLocalName(), bureauPrecedent);
        } catch(Exception e) {
            logger.warn("Cannot sent mail for move folder action", e);
        }

        // Mise à jour des indicateurs de lecture
        nodeService.setProperty(dossier, ParapheurModel.PROP_RECUPERABLE, Boolean.TRUE);
        dossierService.setDossierNonLu(dossier);
        corbeillesService.moveDossier(dossier, oldParentAssoc.getParentRef(), nodeService.getPrimaryParent(dossier).getParentRef());

        // BLEX : enregistrement d'un evenement historique pour ce dossier
        // TODO: FIXME Attention ceci peut etre dangereux, pourquoi un autre systeme d'AUDIT ????
        new EnregistreurEvenementsDossier(nodeService, authenticationService, auditDAO).onDeplacementDossier(dossier, parapheur);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getSignature(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public byte[] getSignature(NodeRef dossier) {
        // Gestion du multidoc -
        // Pour compatibilité ascendante -
        // On récupère seulement la première signature, qui correspond au premier document
        if(getSignatures(dossier) != null) {
            return getSignatures(dossier)[0].getBytes();
        }
        return null;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getSignature(com.atolcd.parapheur.repo.EtapeCircuit)
     */
    @Override
    public byte[] getSignature(EtapeCircuit etape) {
        // Gestion du multidoc -
        // Pour compatibilité ascendante -
        // On récupère seulement la première signature, qui correspond au premier document
        String[] signatures = getSignatures(etape);
        if(signatures != null && signatures.length > 0) {
            return signatures[0].getBytes();
        }
        return null;
    }

    @Override
    public String[] getSignatures(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");
        String signatureString = (String) this.nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_ELECTRONIQUE);
        String[] sigs = null;
        if (signatureString != null) {
            byte[] signature = Base64.decode(signatureString);
            // Gestion du multidoc -
            // Pour compatibilité ascendante -
            // On récupère seulement la première signature, qui correspond au premier document
            String sigStr = new String(signature);
            if(sigStr.contains("<")) {
                sigs = new String[] {sigStr};
            } else {
                sigs = sigStr.split(",");
            }
        }
        return sigs;
    }

    @Override
    public String[] getSignatures(EtapeCircuit etape) {
        if (etape == null) {
            return null;
        }
        String signatureString = (String) this.nodeService.getProperty(etape.getNodeRef(), ParapheurModel.PROP_SIGNATURE_ETAPE);
        String[] sigs = null;
        if (signatureString != null) {
            byte[] signature = Base64.decode(signatureString);
            // Gestion du multidoc -
            // Pour compatibilité ascendante -
            // On récupère seulement la première signature, qui correspond au premier document
            String sigStr = new String(signature);
            if(sigStr.contains("<")) {
                sigs = new String[] {sigStr};
            } else {
                sigs = sigStr.split(",");
            }
        }
        return sigs;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#setSignature(org.alfresco.service.cmr.repository.NodeRef,
     * EtapeCircuit, byte[])
     */
    @Override
    public void setSignature(NodeRef dossier, EtapeCircuit etapeCircuit, byte[] signedBytes, String sigFormat) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");
        byte[] pemSign = signedBytes;

        if (StringUtils.startsWithIgnoreCase(sigFormat, "PKCS#7")) {
            int stI = (signedBytes.length > 10 ? 10 : signedBytes.length);
            String sigStart = new String(signedBytes, 0, stI);
            if (!sigStart.equals("-----BEGIN")) {
                /**
                 * On stocke la signature format PEM, pas DER
                 */
                pemSign = PKCS7VerUtil.der2pem(signedBytes);
            }
        }
        NodeRef etape = etapeCircuit.getNodeRef();
        // FIXME : STV pourquoi ici le parametre Base64.DONT_BREAK_LINES ?? et pas ligne 3631??
        String signatureString = Base64.encodeBytes(pemSign, Base64.DONT_BREAK_LINES);
        // Save signatureString only for detached signature
        if(!StringUtils.startsWithIgnoreCase(sigFormat, "PAdES") && !sigFormat.equalsIgnoreCase("xades/enveloped")) {
            this.nodeService.setProperty(etape, ParapheurModel.PROP_SIGNATURE_ETAPE, signatureString);
        }

        this.nodeService.setProperty(etape, ParapheurModel.PROP_SIGNATURE_FORMAT, sigFormat);

        // On stocke les infos de signature
        X509CertificateHolder[] signatureCerts = null;

        if (StringUtils.startsWithIgnoreCase(sigFormat, "PKCS#7") || StringUtils.startsWithIgnoreCase(sigFormat, "PAdES")) {

            byte[] derSign = PKCS7VerUtil.pem2der(pemSign, "-----BEGIN".getBytes(), "-----END".getBytes());
            signatureCerts = new X509CertificateHolder[]{
                    PKCS7VerUtil.getSignatureCertificateHolder(null, derSign)
            };
            String signatureInfos = new JSONObject(X509Util.getUsefulCertProps(signatureCerts[0])).toString();
            this.nodeService.setProperty(etape, ParapheurModel.PROP_SIGNATURE, signatureInfos);
        } else if(sigFormat.toLowerCase().startsWith("xades") && !sigFormat.equalsIgnoreCase("xades/enveloped")) {
            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                dbf.setNamespaceAware(true);
                org.w3c.dom.Document doc = dbf.newDocumentBuilder().parse(new ByteArrayInputStream(signedBytes));

                // Find Signature element.
                NodeList nl = doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");

                Node sigNode = nl.item(0);

                NodeList x509nl = doc.getElementsByTagNameNS(XMLSignature.XMLNS, "X509Certificate");

                CertificateFactory fact = CertificateFactory.getInstance("X.509");
                ByteArrayInputStream in = new ByteArrayInputStream(org.bouncycastle.util.encoders.Base64.decode(x509nl.item(0).getTextContent()));
                X509Certificate cer = (X509Certificate) fact.generateCertificate(in);

                X509CertificateHolder holder = new X509CertificateHolder(cer.getEncoded());

                String signatureInfos = new JSONObject(X509Util.getUsefulCertProps(holder)).toString();
                this.nodeService.setProperty(etape, ParapheurModel.PROP_SIGNATURE, signatureInfos);
            } catch(Exception e) {
                e.printStackTrace();
            }

        }
    }

    private byte[] getFirstDocumentBytes(NodeRef dossierRef) {
        ContentReader reader = contentService.getReader(this.getMainDocuments(dossierRef).get(0), ContentModel.PROP_CONTENT);
        byte[] bytesToSign = new byte[(int) reader.getSize()];
        try {
            reader.getContentInputStream().read(bytesToSign);
        } catch (IOException e) {
            bytesToSign = null;
        }
        return bytesToSign;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#setSignature(org.alfresco.service.cmr.repository.NodeRef,
     * byte[])
     */
    @Override
    public void setSignature(NodeRef dossier, byte[] signedBytes, String sigFormat) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");
        String signatureString = null;

        if (logger.isDebugEnabled()) {
            logger.debug("Enregistrement d'une signature FORMAT = '" + sigFormat + "'");
        }

        EtapeCircuit currentEtapeCircuit = getCurrentEtapeCircuit(dossier);

        /**
         * Cas de signature CMS/PKCS#7
         */
        if (sigFormat.toLowerCase().startsWith("pkcs#7")) {
            String sig = new String(signedBytes);
            String baseSignatureStr = null;
            for(String sigUnit : sig.split(",")) {
                byte[] signedUnit = sigUnit.getBytes();
                int stI = (signedUnit.length > 10 ? 10 : signedUnit.length);
                String sigStart = new String(signedUnit, 0, stI);
                if (!sigStart.equals("-----BEGIN")) {
                    if(baseSignatureStr == null) {
                        baseSignatureStr = "";
                    } else {
                        baseSignatureStr += ",";
                    }
                    /**
                     * On stocke la signature format PEM, pas DER
                     */
                    signedUnit = PKCS7VerUtil.der2pem(signedUnit);
                    baseSignatureStr += new String(signedUnit);
                }

                /**
                 * Horodatage de la signature, si service actif et configuré
                 */
            }
            if(baseSignatureStr != null) {
                signedBytes = baseSignatureStr.getBytes();
                if(signatureString == null && baseSignatureStr.contains(",")) {
                    signatureString = Base64.encodeBytes(signedBytes, Base64.DONT_BREAK_LINES);
                }
            }
            if (signatureString == null) {
                signatureString = Base64.encodeBytes(signedBytes, Base64.DONT_BREAK_LINES);
            }
        } else if(sigFormat.toLowerCase().startsWith("pkcs#1")) {
            String signature = new String(signedBytes);
            if(signature.contains(";")) {
                String[] sigs = signature.split(";");
                if(sigs.length < 2) {
                    throw new RuntimeException("L'operation de signature n'a pas ete effectuee. Signature PKCS#1 incomplète");
                }

                signatureString = Base64.encodeBytes(sigs[0].getBytes());

                if(currentEtapeCircuit != null) {
                    boolean jumpLine = sigs[1].length() % 64 != 0;
                    nodeService.setProperty(
                            currentEtapeCircuit.getNodeRef(),
                            ParapheurModel.PROP_CLE_PUBLIQUE_ETAPE,
                            "-----BEGIN CERTIFICATE-----\n" + sigs[1].replaceAll("(.{64})", "$1\n") + (jumpLine ? "\n" : "") + "-----END CERTIFICATE-----");
                }
            } else {
                throw new RuntimeException("L'operation de signature n'a pas ete effectuee. Signature PKCS#1 incomplète");
            }
        }

        if (signatureString == null && !(new String(signedBytes).contains(","))) {
            signatureString = Base64.encodeBytes(signedBytes, Base64.DONT_BREAK_LINES);
        }
        nodeService.setProperty(dossier, ParapheurModel.PROP_SIGNATURE_ELECTRONIQUE, signatureString);

        // MODIF STV: la signature est portée par l'étape, pas par le dossier
        // Cela permet la co-signature
        if (currentEtapeCircuit != null) {
            if(!StringUtils.startsWithIgnoreCase(sigFormat, "PAdES") && !sigFormat.equalsIgnoreCase("xades/enveloped")) {
                this.nodeService.setProperty(currentEtapeCircuit.getNodeRef(), ParapheurModel.PROP_SIGNATURE_ETAPE, signatureString);
            }
        }

        /**
         * Cas signature HELIOS-XADES on tente d'encapsuler la signature dans le
         * document principal au chemin XPath fourni
         *
         * TODO: désynchroniser l'opération d'injection qui est très couteuse en
         * ressources. Pourquoi pas: construire une file d'attente de jobs?
         * TODO: Vérifier que ça marche
         */
        if (sigFormat.equalsIgnoreCase("xades/enveloped")
                && signedBytes != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("Encapsulage signature XAdES dans doc PES");
            }
            NodeRef nodeDoc = getMainDocuments(dossier).get(0);
            ContentReader reader = contentService.getReader(nodeDoc, ContentModel.PROP_CONTENT);
            String docEncoding = reader.getEncoding().toUpperCase();
            //  on se limite à Integer.MAX_VALUE = 2147483647, quid si le fichier est plus gros (>2Go) ?
            if (reader.getSize() > Integer.MAX_VALUE) {
                logger.fatal("Document XML trop gros pour pouvoir etre traité.");
                return;
            }
            InputStream bytesToSign = reader.getContentInputStream();

            String strSig;
            InputStream pesSigne = null;
            try {
                strSig = new String(signedBytes, docEncoding);
                String strXPath = (String) this.nodeService.getProperty(dossier, ParapheurModel.PROP_XPATH_SIGNATURE);

                rt.gc();
                if (logger.isDebugEnabled()) {
                    logger.debug("Injection '" + docEncoding + "' dans '" + strXPath + "' de Signature:" + strSig.substring(0, (strSig.length() > 40) ? 40 : strSig.length()));
                }
                // BLEX Traces pour statistiques d'utilisation de la mémoire pendant l'injection de signature
                // L'injection de la signature dans le flux est une opération critique en ce qui concerne la consommation mémoire. Il s'agit d'ajouter des logs informant :
                // - du dossier traité
                // - de la taille du flux à signer
                // - d'une estimation de la mémoire consommée lors de l'opération
                // - du résultat de l'opération
                long freememBefore = Runtime.getRuntime().freeMemory();
                try {
                    pesSigne = Xades.injectXadesSigIntoXml(bytesToSign, strSig, strXPath, docEncoding);
                } finally {
                    if (logger.isInfoEnabled()) {
                        long freememAfter = Runtime.getRuntime().freeMemory();
                        String nomDossier = (String) nodeService.getProperty(dossier, ContentModel.PROP_NAME);
                        logger.info("injection signature : ok=" + (pesSigne != null) + "; " +
                                    "user=" + AuthenticationUtil.getRunAsUser() + "; " +
                                    "dossier=" + nomDossier + "; " +
                                    "freeMemory.before=" + freememBefore + "; " +
                                    "freeMemory.after=" + freememAfter + "; " +
                                    "usedMemory=" + (freememBefore - freememAfter));
                    }
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("  Tentative d'injection finie");
                }
                // logger.debug("MON PES Signé:\n" + PES_Signe);
                rt.gc();
            } catch (UnsupportedEncodingException e) {
                logger.error("Encoding not supported", e);
            }
            if (pesSigne != null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("  MaJ en entrepot...");
                }
                ContentWriter writer = this.contentService.getWriter(getMainDocuments(dossier).get(0), ContentModel.PROP_CONTENT, true);
                writer.setEncoding(reader.getEncoding());
                writer.setMimetype(reader.getMimetype());
                writer.putContent(pesSigne);
                if (logger.isDebugEnabled()) {
                    logger.debug("    ...MaJ entrepot OK, Fin setSignature.");
                }
            } else {
                logger.warn("  PES_Signe est NULL, Pas de MaJ en Entrepot !!! ############");
            }
        } else if (StringUtils.startsWithIgnoreCase(sigFormat, "PAdES")) {
            /**
             * Cas signature PAdES...
             * On met à jour le document principal.
             */
            logger.info("  Prise en compte signature PAdES ISO 32000-1...");

            try {
                //On split en cas de signature µdoc
                List<NodeRef> mainDocs = getMainDocuments(dossier);
                String signature = "";
                if (signedBytes != null) {
                    signature = new String(signedBytes);
                }
                String[] sigs = signature.split(",");
                for (int i = 0; i < sigs.length; i++) {
                    byte[] sigBytes = sigs[i].getBytes();
                    int stI = (sigBytes.length > 10 ? 10 : sigBytes.length);
                    String sigStart = new String(sigBytes, 0, stI);
                    if (!sigStart.equals("-----BEGIN")) {
                        sigBytes = PKCS7VerUtil.der2pem(sigBytes);
                    } else {
                    }
                    if (logger.isDebugEnabled()) {
                        logger.debug("   Contenu de signature : \n" + (new String(sigBytes)));
                    }

                    //                /**
                    //                 * Horodatage de la signature, si service actif et configuré
                    //                 */
                    //                if (timestampService.isAvailable()
                    //                        && timestampService.isEnabled()) {
                    //                    byte[] timestampedSignature = pkcs7TimeStamper(sigBytes);
                    //                    if (timestampedSignature != null) {
                    //                        signatureString = Base64.encodeBytes(PKCS7VerUtil.der2pem(timestampedSignature), Base64.DONT_BREAK_LINES);
                    //                    }
                    //                }

                    // On vérifie qu'il existe bien un document principal correspondant à la signature...
                    if(mainDocs.size() > i) {
                        NodeRef docPrincipalRef = mainDocs.get(i);
                        /**
                         * on se base sur le PDF préparé (stocké dans VISUEL-PDF)
                         * (voir DossierServiceImpl)
                         */
                        ContentReader tmpReader = contentService.getReader(docPrincipalRef, ParapheurModel.PROP_SIGNATURE_PDF);
                        if (null == tmpReader) {
                            // FIXME !!!!! je crois que ça peut arriver si enchainement de circuit...
                            logger.warn("Attention la signature PAdES ne va pas pouvoir conclure sur "+ nodeService.getProperty(docPrincipalRef, ContentModel.PROP_NAME));
                        }
                        File tmp = TempFileProvider.createTempFile("tmpconv-PDFsign", ".pdf");
                        FileCopyUtils.copy(tmpReader.getContentInputStream(), new FileOutputStream(tmp));

                        // mise à jour fichier PDF temporaire...
                        byte[] derSignature = PKCS7VerUtil.pem2der(sigBytes, "-----BEGIN".getBytes(), "-----END".getBytes());
                        new PadesServerHelper.SignatureInjector(derSignature, tmp).injectSignature();


                        // ...et recopie du résultat à la place du PDF original !
                        ContentWriter pdfwriter = contentService.getWriter(docPrincipalRef, ContentModel.PROP_CONTENT, Boolean.TRUE);
                        pdfwriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
                        pdfwriter.setEncoding("UTF-8");
                        FileCopyUtils.copy(new FileInputStream(tmp), pdfwriter.getContentOutputStream());

                        // ...et suppression du visuel PDF !
                        nodeService.removeProperty(docPrincipalRef, ParapheurModel.PROP_SIGNATURE_PDF);
                    } else {
                        logger.warn("J'ai recu plus de signature que je n'ai de documents !");
                    }
                }
            } catch (IOException ex) {
                logger.error(ex.getLocalizedMessage());
            }

        } else {
            /**
             * Cas de signature DIA: la signature qui remonte de l'applet EST le
             * flux signé en entier : donc on met à jour le document principal
             */
            if ("XAdES/DIA".equalsIgnoreCase(sigFormat)) {
                NodeRef diaRef = getMainDocuments(dossier).get(0);
                ContentReader reader = this.contentService.getReader(diaRef, ContentModel.PROP_CONTENT);
                ContentWriter writer = this.contentService.getWriter(diaRef, ContentModel.PROP_CONTENT, true);
                writer.setEncoding(reader.getEncoding());
                writer.setMimetype(reader.getMimetype());
                try {
                    String signedDIA = new String(signedBytes, reader.getEncoding().toUpperCase());

                    /**
                     * Horodatage de la signature, si service actif et configuré
                     */
                    String diaTimestamped = null;
                    String xPathString = null;
                    String sigIdString = null;
                    if (timestampService.isAvailable()
                            && timestampService.isEnabled()) {

                        /**
                         * 1- Identifier la signature
                         *
                         * La signature est localisee grace au 'claimedRole'
                         * passé à l'applet Cette propriété est liée au TITRE du
                         * parapheur signataire
                         */
                        String titre = getNomParapheur(getParentParapheur(dossier)).toLowerCase();
                        if (titre.contains("departement")) {
                            xPathString = Xades.DIA_DP_PATH;
                            sigIdString = Xades.DIA_DP_SIG;
                        } else if (titre.contains("conservatoire") && titre.contains("littoral")) {
                            xPathString = Xades.DIA_CL_PATH;
                            sigIdString = Xades.DIA_CL_SIG;
                        } else if (titre.contains("commune")) {
                            xPathString = Xades.DIA_CM_PATH;
                            sigIdString = Xades.DIA_CM_SIG;
                        }
                        try {
                            /**
                             * 2- Extraire la signature, calcul du hash
                             */
                            byte[] digest = XadesTimeStamper.computeSignatureDigestFromXML(signedBytes, sigIdString);

                            /**
                             * 3- Attraper un jeton pour cette signature
                             */
                            StringBuilder builder = new StringBuilder();
                            if (digest != null) {
                                for (byte dataByte : digest) {  // Un octet est représenté par 2 caractères hexadécimaux
                                    builder.append(Integer.toHexString((dataByte & 0xF0) >> 4));
                                    builder.append(Integer.toHexString(dataByte & 0x0F));
                                }
                            }
                            String digestString = builder.toString();
                            JetonHorodatage myJeton = timestampService.getTimeStampToken(digestString);

                            /**
                             * 4- injecter le jeton au bon endroit
                             */
                            if (myJeton != null) {
                                byte[] token = Base64.decode(myJeton.getToken());
                                byte[] timestampedDIA = XadesTimeStamper.injectTokenForSignature(signedBytes, sigIdString, token);
                                if (timestampedDIA != null) {
                                    diaTimestamped = new String(timestampedDIA, reader.getEncoding().toUpperCase());
                                }
                            }
                        } catch (XMLSignatureException ex) {
                            logger.error(ex.getLocalizedMessage(), ex);
                        }
                    }

                    if (diaTimestamped != null && !diaTimestamped.trim().isEmpty()) {
                        writer.putContent(diaTimestamped);
                        if (logger.isInfoEnabled()) {
                            logger.info(" DIA " + sigIdString + " signee et horodatee");
                        }
                    } else {
                        writer.putContent(signedDIA);
                        logger.warn(" DIA " + sigIdString + " signee et NON HORODATEE.");
                    }

                } catch (UnsupportedEncodingException ex) {
                    logger.error("Encoding not supported", ex);
                }
            }
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getSignAppletURL()
     */
    @Override
    public String getSignAppletURL() {
        String appletUrl = this.configuration.getProperty("signature.applet.url");
        if (logger.isDebugEnabled()) {
            logger.debug("getSignAppletURL : [" + appletUrl + "]");
        }
        return (appletUrl != null ? appletUrl : null);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getXPathSignature
     */
    @Override
    public String getXPathSignature(NodeRef dossier) {
        if (dossier == null) {
            return null;
        }
        Serializable xps = this.nodeService.getProperty(dossier, ParapheurModel.PROP_XPATH_SIGNATURE);
        if (xps == null) {
            return null;
        }
        return xps.toString();
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getXadesSignatureProperties
     */
    @Override
    public Properties getXadesSignatureProperties(NodeRef dossier) {
        Properties props;
        java.io.Serializable tdtNom = this.nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_NOM);
        String tdtProtocol = (String) this.nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_PROTOCOLE);
        /**
         * QUESTION DEBILE de Stephane: si tout type de flux (cf DIA, ANTS-ECD)
         * pourrait etre signe XAdES, pourquoi faire des "if" ??? attention, la
         * reponse n'est pas si evidente que cela....
         */
        if (tdtNom != null
                && (tdtNom.toString().equals("S²LOW")
                || FastServiceImpl.PROP_TDT_NOM_FAST.equals((String) tdtNom))) {
            props = s2lowService.getXadesSignaturePropertiesWithDossier(dossier);
            logger.debug("getXadesSignatureProperties" + props.toString());
            return props;
            // BLEX
        } else if (tdtNom != null && tdtNom.toString().equals(SrciService.K.tdtName)) {
            props = srciService.getXadesSignatureProperties();
            logger.debug("proprietes XadesSignature SRCI : " + props.toString());
            return props;
        } else if (tdtNom != null && tdtProtocol.equals("HELIOS")) {
            // Dorénavant tout type de flux XML (ou non!) pourrait etre signé XAdES (cf DIA), donc on autorise quand même.
            logger.warn("Le dossier n'a pas la propriété TDT_NOM=S²LOW, FAST ni SRCI.");
            // props = s2lowService.getXadesSignatureProperties();
            props = s2lowService.getXadesSignaturePropertiesWithDossier(dossier);
            if (logger.isDebugEnabled()) {
                logger.debug("proprietes XAdES pour signature : " + props.toString());
            }
            return props;
        } else {
            props = s2lowService.getCustomXadesSignaturePropertiesWithDossier(dossier);
            if (logger.isDebugEnabled()) {
                logger.debug("proprietes XAdES pour signature : " + props.toString());
            }
            return props;
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getXadesSignatureProperties
     */
    @Override
    public Properties getPadesSignatureProperties(NodeRef dossier) {
        return s2lowService.getPadesSignaturePropertiesWithDossier(dossier);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#parapheurFromName(java.lang.String)
     */
    @Override
    public NodeRef parapheurFromName(String parapheurName) {
        Assert.hasText(parapheurName, "Le nom passé en paramètre n'est pas une chaîne de caractères valide.");

        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.parapheurs.childname");

        List<NodeRef> results;
        try {
            results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
        } catch (AccessDeniedException e) {
            logger.error("Impossible d'accéder au répertoire de stockage des parapheurs.");
            return null;
        }

        if (results != null && results.size() == 1) {
            return nodeService.getChildByName(results.get(0), ContentModel.ASSOC_CONTAINS, parapheurName);
        } else {
            logger.error("parapheurFromName - Erreur lors de la récupération des parapheurs.");
            return null;
        }
    }

    @Override
    public NodeRef createDossierPesRetour(String id, String parapheurName, InputStream xmlIS, Date datePes) {
        NodeRef dossierRef = null;
        UserTransaction tx = null;
        tx = this.transactionService.getUserTransaction();
        try {
            tx.begin();

            NodeRef parapheur = this.parapheurFromName(parapheurName);
            if (parapheur != null) {
                Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
                Map<QName, Serializable> typageProps = new HashMap<QName, Serializable>();
                // nom Dossier
                properties.put(ContentModel.PROP_NAME, id);
                properties.put(ContentModel.PROP_CREATED, datePes);
                properties.put(ContentModel.PROP_MODIFIED, datePes);
                dossierRef = dossierService.createDossier(parapheur, properties);
                dossierService.setDossierPropertiesWithAuditTrail(dossierRef, parapheur, properties, true);
                // doc principal
                FileInfo fileInfo = fileFolderService.create(dossierRef, id, ContentModel.TYPE_CONTENT);
                NodeRef docNodeRef = fileInfo.getNodeRef();
                ContentWriter cw = contentService.getWriter(docNodeRef, ContentModel.PROP_CONTENT, Boolean.TRUE);
                cw.setMimetype("text/xml");
                cw.setEncoding("ISO-8859-1");
                cw.putContent(xmlIS);
                typageProps = this.getTypeMetierProperties("PES");
                typageProps.put(ParapheurModel.PROP_TYPE_METIER, "PES");
                this.nodeService.addAspect(dossierRef, ParapheurModel.ASPECT_TYPAGE_METIER, typageProps);
                // Dans ce cas: pas d'annotations, ni de circuit ni de signataire
                // a priori le dossier est cree dans 'a transmettre'.
                // TODO Vérifier la méthode, peut-etre pas finie
            } else {
                logger.error("Parapheur '" + parapheurName + "' inconnu.");
            }
            tx.commit();
        } catch (Exception e) {
            try {
                tx.rollback();
            } catch (Exception e1) {
                logger.error(e1.getMessage(), e1);
            }
            logger.error(e.getMessage(), e);
        }

        return dossierRef;
    }

    /**
     * Interroge iParapheur pour obtenir la liste des Types techniques
     *
     * @return flux XML des types techniques
     */
    @Override
    public InputStream getSavedXMLTypes() {
        try {
            NodeRef xmlNode = getSavedXLMTypesNode();
            if (xmlNode != null) { // found what we were looking for
                ContentReader contentreader = contentService.getReader(xmlNode, ContentModel.PROP_CONTENT);
                return contentreader.getContentInputStream(); // return contentreader.getContentString();
            } else {
                // System.out.println("getSavedXMLTypes: (xpath=" + xpath + "), Found " + results.size() + " node(s).");
                logger.warn("getSavedXMLTypes bad node(s).");
                return null;
            }
        } catch (AccessDeniedException err) {
            // ignore and return null
            logger.error(err.getLocalizedMessage());
            System.out.println("### getSavedXMLTypes: Access denied Exception");
            return null;
        }
    }

    @Override
    public NodeRef getSavedXLMTypesNode() {
        if (logger.isInfoEnabled()) {
            logger.info("getSavedXMLTypes: BEGIN");
        }
        // Construction de: app:company_home/app:dictionary/cm:metiertype/TypesMetier.xml
        String xpath = "/" + this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.dictionary.childname") + "/cm:metiertype/cm:TypesMetier.xml";
        List<NodeRef> results;
        try {
            results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);
            if (results != null && results.size() == 1) { // found what we were looking for
                return results.get(0);
            }
        } catch (AccessDeniedException err) {
            // ignore and return null
            logger.error(err.getLocalizedMessage());
            System.out.println("### getSavedXMLTypes: Access denied Exception");
        }
        return null;
    }

    @Override
    public List<String> getSavedTypes(NodeRef parapheur) {
        List<String> savedTypes = new ArrayList<String>();
        try {
            SAXReader saxreader = new SAXReader();
            InputStream istream = getSavedXMLTypes();
            if (null == istream) {
                throw new DocumentException("cannot access list of types");
            }
            Document docXml = saxreader.read(istream);
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierTypes")) {
                Iterator<Element> itMetierType = rootElement.elementIterator("MetierType");
                String tID;
                for (Iterator<Element> ie = itMetierType; ie.hasNext(); ) {
                    Element mtype = ie.next();
                    tID = mtype.element("ID").getTextTrim();
                    if (getSavedSousTypes(tID, parapheur).size() > 0) {
                        savedTypes.add(tID);
                    }
                }
            }
        } catch (DocumentException ex) {
            logger.error("Exception parsing saved types XML document", ex);
        }
        return savedTypes;
    }

    @Override
    public List<String> getSavedTypes() {
        List<String> savedTypes = new ArrayList<String>();
        try {
            SAXReader saxreader = new SAXReader();
            InputStream istream = getSavedXMLTypes();
            if (null == istream) {
                throw new DocumentException("cannot access list of types");
            }
            Document docXml = saxreader.read(istream);
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierTypes")) {
                Iterator<Element> itMetierType = rootElement.elementIterator("MetierType");
                String tID;
                for (Iterator<Element> ie = itMetierType; ie.hasNext(); ) {
                    Element mtype = ie.next();
                    tID = mtype.element("ID").getTextTrim();
                    if (getSavedSousTypes(tID).size() > 0) {
                        savedTypes.add(tID);
                    }
                }
            }
        } catch (DocumentException ex) {
            logger.error("Exception parsing saved types XML document", ex);
        }
        return savedTypes;
    }

    /**
     * (BLEX) Acces a la liste de TOUS les types metiers , que ceux-ci aient des
     * sous-types ou non
     * (Adullact) question: a quoi cela peut-il servir ?
     *
     * @return les noms de tous les types metier connus
     */
    @Override
    public List<String> getSavedTypesFull() {
        List<String> savedTypes = new ArrayList<String>();
        try {
            SAXReader saxreader = new SAXReader();
            InputStream istream = getSavedXMLTypes();
            if (null == istream) {
                throw new DocumentException("impossible d'acceder a la liste des types");
            }
            Document docXml = saxreader.read(istream);
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierTypes")) {
                Iterator<Element> itMetierType = rootElement.elementIterator("MetierType");
                String tID;
                for (Iterator<Element> ie = itMetierType; ie.hasNext(); ) {
                    Element mtype = ie.next();
                    tID = mtype.element("ID").getTextTrim();
                    savedTypes.add(tID);
                }
            }
        } catch (DocumentException ex) {
            logger.error("erreur de lecture du document XML contenant les types", ex);
        }
        return savedTypes;
    }

    @Override
    public List<String> getSavedTypes(String username, NodeRef parapheur) {
        final NodeRef fParapheur = parapheur;
        return AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<List<String>>() {

            @Override
            public List<String> doWork() throws Exception {
                return getSavedTypes(fParapheur);
            }
        }, username);
    }

    /**
     * Interroge iParapheur pour obtenir la liste des sous-Types techniques
     *
     * @return flux XML des sous-types techniques
     */
    @Override
    public InputStream getSavedXMLSousTypes(String typeID) {
        logger.debug("getSavedXMLSousTypes: BEGIN");

        //StoreRef spacesStoreRef = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");
        StoreRef spacesStoreRef = new StoreRef(this.configuration.getProperty("spaces.store"));
        String xpath = "/" + this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.dictionary.childname")
                + "/cm:metiersoustype/*[@cm:name='" + typeID + ".xml']";
        try {
            List<NodeRef> results = searchService.selectNodes(nodeService.getRootNode(spacesStoreRef), xpath, null, namespaceService, false);
            // results = searchService.query(spacesStoreRef, SearchService.LANGUAGE_LUCENE, queryStr);
            if (results.size() == 1) { // found what we were looking for
                logger.debug("getSavedXMLSousTypes: Found 1 node");
                NodeRef xmlSousTypesNoderef = results.get(0);
                ContentReader contentreader = contentService.getReader(xmlSousTypesNoderef, ContentModel.PROP_CONTENT);
                if (contentreader != null) {
                    logger.debug("getSavedXMLSousTypes:  contentreader Ok !");
                    return contentreader.getContentInputStream(); // return contentreader.getContentString();
                } else {
                    logger.warn("getSavedXMLSousTypes:  contentreader est NULL !");
                    return null;
                }
            } else {
                logger.warn("getSavedXMLSousTypes: (" + typeID + "), Found " + results.size() + " node(s).");
                return null;
            }
        } catch (AccessDeniedException err) {
            // ignore and return null
            logger.error("getSavedXMLSousTypes: Access denied Exception");
            return null;
        }
    }

    /**
     * Interroge iParapheur pour obtenir la liste des sous-Types techniques
     *
     * @return flux XML des sous-types techniques
     */
    @Override
    public List<NodeRef> getAllSavedXMLSousTypes() {
        logger.debug("getSavedXMLSousTypes: BEGIN");

        //StoreRef spacesStoreRef = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");
        StoreRef spacesStoreRef = new StoreRef(this.configuration.getProperty("spaces.store"));
        String xpath = "/" + this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.dictionary.childname")
                + "/cm:metiersoustype/*";
        try {
            List<NodeRef> results = searchService.selectNodes(nodeService.getRootNode(spacesStoreRef), xpath, null, namespaceService, false);
            return results;
        } catch (AccessDeniedException err) {
            // ignore and return null
            logger.error("getSavedXMLSousTypes: Access denied Exception");
            return null;
        }
    }

    @Override
    public List<String> getSavedSousTypes(String type, NodeRef currentParapheur) {

        List<String> savedSousTypes = new ArrayList<String>();
        try {
            SAXReader saxreader = new SAXReader();
            InputStream istream = getSavedXMLSousTypes(type);
            if (null == istream) {
                throw new DocumentException("cannot get access to " + type);
            }
            Document docXml = saxreader.read(istream);
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                Iterator<Element> itMetierSousType = rootElement.elementIterator("MetierSousType");
                String tID;
                for (Iterator<Element> ie = itMetierSousType; ie.hasNext(); ) {
                    Element msoustype = ie.next();
                    tID = msoustype.element("ID").getTextTrim();
                    Attribute attrVisibility = msoustype.attribute("visibility");

                    boolean isPublic = false;
                    List<String> aclParapheurs = new ArrayList<String>();
                    List<String> aclGroups = new ArrayList<String>();

                    if (attrVisibility != null
                            && "public".equals(attrVisibility.getText())) {
                        isPublic = true;
                    }

                    Element mAclParaph = msoustype.element("parapheurs");
                    if (mAclParaph != null) {
                        Iterator<Element> parapheurs = mAclParaph.elementIterator("parapheur");
                        while (parapheurs.hasNext()) {
                            Element parapheur = parapheurs.next();
                            aclParapheurs.add(parapheur.getTextTrim());
                        }
                    }

                    Element mAclGroups = msoustype.element("groups");
                    if (mAclGroups != null) {
                        Iterator<Element> groups = mAclGroups.elementIterator("group");
                        while (groups.hasNext()) {
                            Element group = groups.next();
                            NodeRef groupRef = new NodeRef(group.getTextTrim());
                            if (groupRef != null && nodeService.exists(groupRef)) {
                                String groupName = (String) nodeService.getProperty(groupRef, ContentModel.PROP_AUTHORITY_NAME);
                                aclGroups.add(groupName);
                            }
                        }
                    }

                    String currentUser = AuthenticationUtil.getRunAsUser();
                    Collection<String> groups = authorityService.getAuthoritiesForUser(currentUser);
                    //NodeRef currentParapheur = parapheur;
                    assert (getParapheurOwners(currentParapheur).contains(currentUser));

                    if (isPublic
                            || (currentParapheur != null
                            && aclParapheurs.contains(currentParapheur.toString()))
                            || CollectionsUtils.containsOneOf(aclGroups, groups)) {
                        savedSousTypes.add(tID);
                    }
                }
            }
        } catch (DocumentException ex) {
            logger.error(ex.getLocalizedMessage());
            //logger.error("Exception parsing saved types XML document", ex);
        }

        return savedSousTypes;
    }

    @Override
    public List<String> getSavedSousTypes(final String type, String username, NodeRef parapheur) {
        final NodeRef fParapheur = parapheur;
        return AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<List<String>>() {

            @Override
            public List<String> doWork() throws Exception {
                return getSavedSousTypes(type, fParapheur);
            }
        }, username);
    }

    @SuppressWarnings("unchecked")
    @Override
    public NodeRef getCircuitRef(String typeID, String sousTypeID) {
        logger.debug("getCircuitRef: BEGIN");
        //StoreRef spacesStoreRef = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");
        StoreRef spacesStoreRef = new StoreRef(this.configuration.getProperty("spaces.store"));
        //String queryStr = "+PATH:\"/app:company_home/app:dictionary/cm:metiersoustype/*\" " + "+@cm\\:name:\"" + typeID + ".xml\"";
        String xpath = "/" + this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.dictionary.childname")
                + "/cm:metiersoustype/*[@cm:name='" + typeID + ".xml']";
        try {
            List<NodeRef> results = searchService.selectNodes(nodeService.getRootNode(spacesStoreRef), xpath, null, namespaceService, false);
            // results = searchService.query(spacesStoreRef, SearchService.LANGUAGE_LUCENE, queryStr);
            if (results.size() == 1) { // found what we were looking for
                logger.debug("getCircuitRef: Found 1 xmlSousTypeNoderef node");
                NodeRef xmlSousTypeNoderef = results.get(0);
                ContentReader contentreader = contentService.getReader(xmlSousTypeNoderef, ContentModel.PROP_CONTENT);
                if (contentreader != null) {
                    logger.debug("getCircuitRef:  contentreader Ok !");
                    SAXReader saxreader = new SAXReader();
                    Document docXml = saxreader.read(contentreader.getContentInputStream());
                    Element rootElement = docXml.getRootElement();
                    if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                        Iterator<Element> itMetierSousType = rootElement.elementIterator("MetierSousType");
                        for (Iterator<Element> ie = itMetierSousType; ie.hasNext(); ) {
                            Element msoustype = ie.next();
                            if (sousTypeID.equalsIgnoreCase(msoustype.element("ID").getTextTrim())) {
                                // zou c'est trouvé
                                NodeRef circuitRef = findCircuitRefFromName(getCircuitsHome(), msoustype.element("Circuit").getTextTrim());
                                if (circuitRef == null) {
                                    throw new DocumentException("getCircuitRef: aucun Circuit correspondant à (" + typeID + "," + sousTypeID + ") dans iParapheur");
                                }
                                return circuitRef;
                            }
                        }
                    } else {
                        logger.debug("   getCircuitRef: pas de MetierSousTypes dans XML");
                    }
                    logger.debug("   getCircuitRef: Rien trouvé...");
                    return null;
                } else {
                    logger.debug("getCircuitRef:  contentreader est NULL !?!");
                    return null;
                }
            } else {
                logger.debug("getSavedXMLSousTypes: (" + typeID + "), Found " + results.size() + " xmlSousTypeNoderef node(s).");
                return null;
            }
        } catch (DocumentException de) {
            // ignore and return null
            logger.debug("getCircuitRef: Erreur sur parsing fichier XML");
            return null;
        } catch (AccessDeniedException err) {
            // ignore and return null
            logger.debug("getCircuitRef: Access denied Exception");
            return null;
        }
    }

    @Override
    public List<String> getSavedSousTypes(String type) {

        List<String> savedSousTypes = new ArrayList<String>();
        String currentUser = AuthenticationUtil.getRunAsUser();
        Collection<String> groupsList = authorityService.getAuthoritiesForUser(currentUser);
        ArrayList<String> parapheursList = new ArrayList<String>();
        for (NodeRef p : getOwnedParapheurs(currentUser)) {
            parapheursList.add(p.toString());
        }
        try {
            SAXReader saxreader = new SAXReader();
            InputStream istream = getSavedXMLSousTypes(type);
            if (null == istream) {
                throw new DocumentException("cannot get access to " + type);
            }
            Document docXml = saxreader.read(istream);
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                Iterator<Element> itMetierSousType = rootElement.elementIterator("MetierSousType");
                String tID;
                for (Iterator<Element> ie = itMetierSousType; ie.hasNext(); ) {
                    Element msoustype = ie.next();
                    tID = msoustype.element("ID").getTextTrim();
                    Attribute attrVisibility = msoustype.attribute("visibilityFilter");

                    boolean isPublic = false;
                    boolean isVisible = false;
                    List<String> aclParapheurs = new ArrayList<String>();
                    List<String> aclGroups = new ArrayList<String>();

                    //Si l'attribut n'existe pas, le sousType est public par défaut
                    if (attrVisibility == null || "public".equals(attrVisibility.getText())) {
                        isPublic = true;
                    }

                    if (!isPublic) {
                        Element mAclParaph = msoustype.element("parapheursFilters");
                        if (mAclParaph != null) {
                            Iterator<Element> parapheurs = mAclParaph.elementIterator("parapheur");
                            while (parapheurs.hasNext()) {
                                Element parapheur = parapheurs.next();
                                aclParapheurs.add(parapheur.getTextTrim());
                            }
                        }

                        Element mAclGroups = msoustype.element("groupsFilters");
                        if (mAclGroups != null) {
                            Iterator<Element> groups = mAclGroups.elementIterator("group");
                            while (groups.hasNext()) {
                                Element group = groups.next();
                                NodeRef groupRef = new NodeRef(group.getTextTrim());
                                if (groupRef != null && nodeService.exists(groupRef)) {
                                    String groupName = (String) nodeService.getProperty(groupRef, ContentModel.PROP_AUTHORITY_NAME);
                                    aclGroups.add(groupName);
                                }
                            }
                        }

                        isVisible = CollectionsUtils.containsOneOf(aclParapheurs, parapheursList) || CollectionsUtils.containsOneOf(aclGroups, groupsList);
                    }

                    if (isPublic || isVisible) {
                        savedSousTypes.add(tID);
                    }
                }
            }
        } catch (DocumentException ex) {
            logger.error("Exception parsing saved types XML document", ex);
        }

        return savedSousTypes;
    }

    /**
     * Fournit les caractéristiques du Type technique donné en paramètre
     *
     * @param typeID : le Type technique iParapheur
     * @return infos TdT
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<QName, Serializable> getTypeMetierProperties(String typeID) {
        SAXReader saxreader = new SAXReader();
        Map<QName, Serializable> props = new HashMap<QName, Serializable>();
        try {
            Document docXml = saxreader.read(getSavedXMLTypes());
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierTypes")) {
                Iterator<Element> itMetierType = rootElement.elementIterator("MetierType");
                for (Iterator<Element> ie = itMetierType; ie.hasNext(); ) {
                    Element mtype = ie.next();
                    if (mtype.element("ID").getTextTrim().equalsIgnoreCase(typeID.trim())) {
                        props.put(ParapheurModel.PROP_TDT_NOM, mtype.element("TdT").element("Nom").getTextTrim());
                        props.put(ParapheurModel.PROP_TDT_PROTOCOLE, mtype.element("TdT").element("Protocole").getTextTrim());
                        props.put(ParapheurModel.PROP_TDT_FICHIER_CONFIG, mtype.element("TdT").element("UriConfiguration").getTextTrim());
                        props.put(ParapheurModel.PROP_TYPE_SIGNATURE, this.getTypeSignatureFromProtocoleTDT(mtype.element("TdT").element("Protocole").getTextTrim()));
                        props.put(ParapheurModel.PROP_SIGNATURE_FORMAT, mtype.element("sigFormat").getTextTrim());
                        props.put(ParapheurModel.PROP_SIGNATURE_LOCATION, mtype.element("sigLocation").getTextTrim());
                        props.put(ParapheurModel.PROP_SIGNATURE_POSTALCODE, mtype.element("sigPostalCode").getTextTrim());
                    }
                }
            }
        } catch (DocumentException e) {
            logger.error("getTypeMetierProperties: Erreur sur parsing fichier XML", e);
            return null;
        }
        return props;
    }

    @Override
    public NodeRef findUserByEmail(String from) {
        StoreRef containerStore = personService.getPeopleContainer().getStoreRef();

        ResultSet results = null;
        String queryStr = "TYPE:\"" + ContentModel.TYPE_PERSON + "\" AND "
                + "@cm\\:email:\"" + from + "\"";

        results = searchService.query(containerStore, SearchService.LANGUAGE_LUCENE, queryStr);
        if (results.length() < 1) {
            throw new NoSuchPersonException("Email inconnu dans le Parapheur");
        }
        // XXX Quid s'il existe plusieurs utilisateurs avec cet email ?? Ici on prend le 1er
        return results.getNodeRef(0);
    }

    @Override
    public String findEmailForUsername(String username) {
        // Si l'utilisateur existe réellement, la fonction renvoit la PROP_EMAIL
        String emailString = null;
        if (personService.personExists(username)) {
            NodeRef personRef = personService.getPerson(username);
            emailString = (String) nodeService.getProperty(personRef, ContentModel.PROP_EMAIL);
        }
        return emailString;
    }

    @Override
    public String getValidAddressForUser(String userName) {

        NodeRef userNode = personService.getPerson(userName, false);
        String preferencesMail = parapheurUserPreferences.getNotificationMailForUsername(userName).length > 0 ? parapheurUserPreferences.getNotificationMailForUsername(userName)[0] : null;
        String userMail = (String) nodeService.getProperty(userNode, ContentModel.PROP_EMAIL);

        return StringUtils.firstValidMailAddress(null, preferencesMail, userMail);
    }

    /**
     * La méthode crée une requête LUCENE à partir du nom de dossier passé en paramètre
     * puis si la liste de résultat est non-nulle, renvoit le NodeRef du premier élément
     */
    @Override
    public NodeRef rechercheDossier(String nomDossier) {
        StoreRef spacesStoreRef = new StoreRef(this.configuration.getProperty("spaces.store"));
        ResultSet results = null;
        // Requete:  PATH:"/app:company_home/*" +@cm\:name:"nouveau contrat Hot-line"
        String queryStr = "PATH:\"/app:company_home/*\" +TYPE:\"ph:dossier\" +@cm\\:name:\"" + nomDossier + "\"";
        // String queryStr = "PATH:\"/app:company_home/*\" +TYPE:\"ph:dossier\" +@cm\\:name:\"" + org.alfresco.webservice.util.ISO9075.encode(nomDossier) + "\"";
        System.out.println("rechercheDossier [" + queryStr + "]");
        results = searchService.query(spacesStoreRef, SearchService.LANGUAGE_LUCENE, queryStr);
        if (results.length() < 1) {
            return null;
        } else {
            return results.getNodeRef(0);
        }
    }

    @Override
    public boolean isNomDossierAlreadyExists(String nomDossier) {
        return (rechercheDossier(nomDossier) != null);
    }

    @Override
    public List<NodeRef> rechercheDossiers(String type, String soustype, String status) {
        //StoreRef spacesStoreRef = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");
        StoreRef spacesStoreRef = new StoreRef(this.configuration.getProperty("spaces.store"));
        ResultSet results = null;
        String queryStr = "PATH:\"/" + this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.parapheurs.childname") + "/*\" +TYPE:\"ph:dossier\"";
        // affinage de la requete
        if (type != null) {
            queryStr = queryStr.concat(" +@ph\\:typeMetier:\"" + type + "\"");    // +@ph\:typeMetier:"PES"
        }
        if (soustype != null) {
            queryStr = queryStr.concat(" +@ph\\:soustypeMetier:\"" + soustype + "\"");    // +@ph\:soustypeMetier:"Bordereaux"
        }
        if (status != null) {
            queryStr = queryStr.concat(" +@ph\\:status-metier:\"" + status + "\"");
        }
        logger.debug("rechercheDossiers Query=" + queryStr);
        results = searchService.query(spacesStoreRef, SearchService.LANGUAGE_LUCENE, queryStr);
        if (results.length() < 1) {
            return null;
        } else {
            return results.getNodeRefs();
        }
    }

    /*
     * PRIVATE
	 */
    private String getTypeSignatureFromProtocoleTDT(String protocole) {
        if (protocole.equalsIgnoreCase("HELIOS")) {
            return "XADES";
        } else {
            return "CMS";
        }
    }

    private NodeRef findCircuitRefFromName(NodeRef circuitsHome, String circuitName) {
        ResultSet results = null;
        NodeRef res = null;
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.dictionary.childname") + "/" + this.configuration.getProperty("spaces.savedworkflows.childname");
        try {
            //String query = "+PATH:\"/app:company_home/app:dictionary/ph:savedworkflows/*\" +@cm\\:name:\"" + circuitName + "\"";
            String query = "+PATH:\"/" + xpath + "/*\" +@cm\\:name:\"" + circuitName + "\"";
            logger.debug(query);
            results = searchService.query(circuitsHome.getStoreRef(), SearchService.LANGUAGE_LUCENE, query);
            int nbreResultats = results.length();
            switch (nbreResultats) {
                case 0:
                    throw new RuntimeException("findCircuitRefFromName: Circuit [" + circuitName + "] inconnu !, trouvé 0 noeud!");
                case 1:
                    res = results.getNodeRef(0);
                    break;
                default:
                    for (int i = 0; i < nbreResultats; i++) {
                        if (circuitName.equals(this.nodeService.getProperty(results.getNodeRef(i), ContentModel.PROP_NAME).toString())) {
                            res = results.getNodeRef(i);
                            break;
                        }
                    }
            }
        } catch (Exception e) {
            logger.error(e.getClass() + "---" + e.getMessage());
        } finally {
            if (results != null) {
                results.close();
            }
        }
        return res;
    }

    /**
     * @return The "circuits home" NodeRef
     */
    private NodeRef getCircuitsHome() {
        //StoreRef spacesStoreRef = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");
        StoreRef spacesStoreRef = new StoreRef(this.configuration.getProperty("spaces.store"));
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.dictionary.childname") + "/" + this.configuration.getProperty("spaces.savedworkflows.childname");

        ResultSet results = null;
        try {
            String queryStr = "PATH:\"/" + xpath + "\"";
            results = searchService.query(spacesStoreRef, SearchService.LANGUAGE_LUCENE, queryStr);
            if (results.length() != 1) {
                throw new RuntimeException("Noeud des Circuits inconnu !");
            }
            return results.getNodeRef(0);

        } catch (Exception e) {
            logger.error(e.getClass() + "---" + e.getMessage());
        } finally {
            if (results != null) {
                results.close();
            }
        }
        return null;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#willDelegationLoop(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef, java.util.Date, java.util.Date)
     */
    @Override
    public boolean willDelegationLoop(NodeRef parapheur, NodeRef parapheurCible, Date debut, Date fin) {
        boolean boucle = false;

        // on ignore la délégation actuellement en place sur "parapheur",
        // qu'on remplace par les dates données en paramètres

        //On gère les cas sans date de début ou fin
        debut = debut != null ? debut : new Date(Long.MIN_VALUE);
        fin = fin != null ? fin : new Date(Long.MAX_VALUE);

        // Délégation précédente
        Date lastDebut = debut;
        Date lastFin = fin;

        // Le parapheur "parent" de la délégation dans la boucle
        NodeRef parapheurInLoop;
        // Le parapheur cible de la délégation dans la boucle
        NodeRef delegueInLoop = parapheurCible;

        while(delegueInLoop != null) {
            // Le delegue actuel devient le parapheur dans la boucle, sur lequel vérifier la délégation
            parapheurInLoop = delegueInLoop;

            delegueInLoop = getDelegation(parapheurInLoop);
            if (delegueInLoop == null) {
                delegueInLoop = getDelegationProgrammee(parapheurInLoop);
            }

            // Si delegueInLoop != null, on récupère les dates
            if(delegueInLoop != null) {
                Date creneauDebut = getDateDebutDelegation(parapheurInLoop);
                Date creneauFin = getDateFinDelegation(parapheurInLoop);

                //On gère les cas sans date de début ou de fin
                creneauDebut = creneauDebut != null ? creneauDebut : new Date(Long.MIN_VALUE);
                creneauFin = creneauFin != null ? creneauFin : new Date(Long.MAX_VALUE);

                // Si les dates voulues et de la délégation précédente sont en collision, on vérifie le parapheur cible
                // Voir http://wiki.c2.com/?TestIfDateRangesOverlap
                if((debut.before(creneauFin) && creneauDebut.before(fin)) &&
                        (lastDebut.before(creneauFin) && creneauDebut.before(lastFin))) {
                    if(delegueInLoop.equals(parapheur)) {
                        boucle = true;
                        break;
                    } else {
                        // Si il y a collision mais que le parapheur n'est pas le même, on continu de remonter l'arbre des délégations
                        lastDebut = creneauDebut;
                        lastFin = creneauFin;
                    }
                // S'il n'y a pas de collision, la chaine est brisée et la boucle n'est pas possible
                } else {
                    break;
                }
            }
        }
        return boucle;
    }

    private void mail(String dest, String template, NodeRef ref) {
        Action mail = actionService.createAction("parapheur-mail");
        mail.setParameterValue("dest", dest);
        mail.setParameterValue("template", template);
        mail.setExecuteAsynchronously(false);
        this.actionService.executeAction(mail, ref);
    }

    @Override
    public EtapeCircuit getCurrentEtapeCircuit(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");

        List<EtapeCircuit> circuit = getCircuit(dossier);
        if (circuit == null) {
            return null;
        }
        if (isTermine(dossier) && circuit.get(0).getActionDemandee() == null) {
            return null;
        }
        return getCurrentEtapeCircuit(circuit);
    }

    public EtapeCircuit getPreviousEtapeCircuit(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");

        List<EtapeCircuit> circuit = getCircuit(dossier);
        if (circuit == null) {
            return null;
        }
        if (isTermine(dossier) && circuit.get(0).getActionDemandee() == null) {
            return null;
        }
        return getPreviousEtapeCircuit(circuit);
    }

    public EtapeCircuit getPreviousEtapeCircuit(List<EtapeCircuit> circuit) {
        if (circuit == null) {
            return null;
        }
        EtapeCircuit previous = null;

        for (EtapeCircuit etape : circuit) {
            if (!etape.isApproved()) {
                return previous;
            }
            previous = etape;
        }
        return null;
    }

    @Override
    public EtapeCircuit getCurrentEtapeCircuit(List<EtapeCircuit> circuit) {
        if (circuit == null) {
            return null;
        }

        for (EtapeCircuit etape : circuit) {
            if (!etape.isApproved()) {
                return etape;
            }
        }
        return null;
    }

    private boolean isOfType(NodeRef nodeRef, QName type) {
        Assert.notNull(nodeRef, "Node Ref is mandatory");
        Assert.notNull(this.nodeService, "NodeService is mandatory");
        Assert.notNull(type, "Type is mandatory");
        if(!this.nodeService.exists(nodeRef)) {
            logger.warn("Node Ref does not exists - " + nodeRef.getId());
            return false;
        }

        // find it's type so we can see if it's a node we are interested in
        QName type2 = this.nodeService.getType(nodeRef);

        // make sure the type is defined in the data dictionary
        TypeDefinition typeDef = this.dictionaryService.getType(type2);

        if (typeDef == null) {
            if (logger.isEnabledFor(Level.WARN)) {
                logger.warn("Found invalid object in database: id = " + nodeRef + ", type = " + type2);
            }

            return false;
        }

        return this.dictionaryService.isSubClass(type2, type);
    }

    private NodeRef getPrimaryParent(NodeRef nodeRef) {
        return nodeService.getPrimaryParent(nodeRef).getParentRef();
    }

    @Override
    public NodeRef getFirstChild(NodeRef nodeRef, QName childAssocType) {
        List<ChildAssociationRef> childAssocs = this.nodeService.getChildAssocs(nodeRef, childAssocType,
                RegexQNamePattern.MATCH_ALL);

        if (childAssocs.size() == 1) {
            return childAssocs.get(0).getChildRef();
        }

        return null;
    }

    @Override
    public void updateOwnerPermission(NodeRef bureau, List<String> newOwners) {
        ArrayList<String> previousOwners = (ArrayList<String>) nodeService.getProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);
        updateAuthorityChildren(bureau, previousOwners, newOwners, "PHOWNER_");
    }

    @Override
    public void updateSecretariatPermission(NodeRef bureau, List<String> newSecretariat) {
        ArrayList<String> previousSecretariat = (ArrayList<String>) nodeService.getProperty(bureau, ParapheurModel.PROP_SECRETAIRES);
        updateAuthorityChildren(bureau, previousSecretariat, newSecretariat, "PHSECRETARIAT_");
    }

    private void updateAuthorityChildren(NodeRef bureau, List<String> previousChildren, List<String> newChildren, String shortName) {
        String roleName = "ROLE_" + shortName;
        if (!authorityService.authorityExists(roleName + bureau.getId())) {
            authorityService.createAuthority(AuthorityType.ROLE, shortName + bureau.getId());
        }

        // Instead of deleting and recreate the whole role, we add the new secretaries and
        // delete the old ones (that are not secretary anymore)

        List<String> childrenToAdd;
        if (newChildren != null) {
            childrenToAdd = new ArrayList<String>(newChildren);
        } else {
            childrenToAdd = new ArrayList<String>();
        }
        if (previousChildren != null) {
            childrenToAdd.removeAll(previousChildren);
        }

        List<String> childrenToRemove;
        if (previousChildren != null) {
            childrenToRemove = new ArrayList<String>(previousChildren);
        } else {
            childrenToRemove = new ArrayList<String>();
        }
        if (newChildren != null) {
            childrenToRemove.removeAll(newChildren);
        }

        if (!childrenToRemove.isEmpty()) {
            for (String childToRemove : childrenToRemove) {
                if (!childToRemove.isEmpty() && authorityService.authorityExists(childToRemove)) {
                    authorityService.removeAuthority(roleName + bureau.getId(),
                            authorityService.getName(AuthorityType.USER, childToRemove));
                }
            }
        }

        if (!childrenToAdd.isEmpty()) {
            for (String childToAdd : childrenToAdd) {
                if (!childToAdd.isEmpty()) {
                    authorityService.addAuthority(roleName + bureau.getId(),
                            authorityService.getName(AuthorityType.USER, childToAdd));
                }
            }
        }
    }

    @Override
    public boolean updateBureau(final NodeRef bureau, final HashMap<QName, Serializable> propertiesMap, final HashMap<QName, Serializable> assocsMap) {

        RetryingTransactionHelper trx_A = transactionService.getRetryingTransactionHelper();
        trx_A.doInTransaction(new RetryingTransactionCallback<Object>() {
            @Override
            public Object execute() throws Throwable {
                boolean habilitationsChanged = false;
                boolean habilitationsEnabled = false;
                Map<QName, Serializable> habilitations = new HashMap<QName, Serializable>();
                boolean hasAspect = nodeService.hasAspect(bureau, ParapheurModel.ASPECT_HABILITATIONS);
                habilitations.put(ParapheurModel.PROP_HAB_ARCHIVAGE, hasAspect ? nodeService.getProperty(bureau, ParapheurModel.PROP_HAB_ARCHIVAGE) : false);
                habilitations.put(ParapheurModel.PROP_HAB_SECRETARIAT, hasAspect ? nodeService.getProperty(bureau, ParapheurModel.PROP_HAB_SECRETARIAT) : false);
                habilitations.put(ParapheurModel.PROP_HAB_TRAITER, hasAspect ? nodeService.getProperty(bureau, ParapheurModel.PROP_HAB_TRAITER) : false);
                habilitations.put(ParapheurModel.PROP_HAB_TRANSMETTRE, hasAspect ? nodeService.getProperty(bureau, ParapheurModel.PROP_HAB_TRANSMETTRE) : false);

                //Base properties
                for (Map.Entry<QName, Serializable> entry : propertiesMap.entrySet()) {
                    if (entry.getKey().equals(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR)) {
                        updateOwnerPermission(bureau, (List<String>) entry.getValue());
                    }
                    if (entry.getKey().equals(ParapheurModel.PROP_SECRETAIRES)) {
                        updateSecretariatPermission(bureau, (List<String>) entry.getValue());
                    }
                    if (entry.getKey().equals(ParapheurModel.ASPECT_HABILITATIONS)) {
                        if (nodeService.hasAspect(bureau, ParapheurModel.ASPECT_HABILITATIONS)) {
                            nodeService.removeAspect(bureau, ParapheurModel.ASPECT_HABILITATIONS);
                        }
                        habilitationsEnabled = (Boolean) entry.getValue();
                    }
                    if (entry.getKey().equals(ParapheurModel.PROP_HAB_ARCHIVAGE) ||
                            entry.getKey().equals(ParapheurModel.PROP_HAB_SECRETARIAT) ||
                            entry.getKey().equals(ParapheurModel.PROP_HAB_TRAITER) ||
                            entry.getKey().equals(ParapheurModel.PROP_HAB_TRANSMETTRE)) {
                        habilitationsChanged = true;
                        habilitations.put(entry.getKey(), entry.getValue());
                    }
                    nodeService.setProperty(bureau, entry.getKey(), entry.getValue());
                }

                if (habilitationsChanged || habilitationsEnabled) {
                    nodeService.addAspect(bureau, ParapheurModel.ASPECT_HABILITATIONS, habilitations);
                }

                //Hierarchie
                for (Map.Entry<QName, Serializable> entry : assocsMap.entrySet()) {
                    if (entry.getKey().equals(ParapheurModel.ASSOC_HIERARCHIE)) {
                        NodeRef superieur = getParapheurResponsable(bureau);
                        if (superieur != null) {
                            nodeService.removeAssociation(bureau, superieur, ParapheurModel.ASSOC_HIERARCHIE);
                        }
                        if (entry.getValue() != null && !"".equals(entry.getValue())) {
                            superieur = getParapheurResponsable(new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE.toString() + "/" + entry.getValue()));
                            while (superieur != null) {
                                if (superieur.equals(bureau)) {
                                    return false;//throw new AlfrescoRuntimeException("Le responsable sélectionné provoque une boucle dans la hiérarchie");
                                }
                                superieur = getParapheurResponsable(superieur);
                            }

                            nodeService.createAssociation(bureau, new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE.toString() + "/" + entry.getValue()), ParapheurModel.ASSOC_HIERARCHIE);
                        }
                    }
                }
                return null;
            }
        }, false, false);


        //        //Delegation
        //        if(delegationMap.get("delegation_parapheurCible") != null) {
        //            programmerDelegation(bureau, new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE.toString() + "/" + delegationMap.get("delegation_parapheurCible")),
        //                    new Date((Long) delegationMap.get("delegation_dateDebut")),
        //                    new Date((Long) delegationMap.get("delegation_dateFin")),
        //            delegationMap.get("delegation_dossiersActuels") == "true");
        //        }

        return true;
    }

    private ChildAssociationRef getFirstChildAssoc(NodeRef nodeRef, QName childAssocType) {
        List<ChildAssociationRef> childAssocs = this.nodeService.getChildAssocs(nodeRef, childAssocType,
                RegexQNamePattern.MATCH_ALL);

        if (childAssocs.size() == 1) {
            return childAssocs.get(0);
        }

        return null;
    }

    private NodeRef getFirstParentOfType(NodeRef nodeRef, QName parentType) {
        Assert.notNull(nodeRef, "Node Ref is mandatory");
        Assert.isTrue(this.nodeService.exists(nodeRef), "Node Ref must exist in the repository");
        Assert.notNull(parentType, "Type is mandatory");

        for (NodeRef parent = nodeRef; parent != null && this.nodeService.exists(parent); parent = getPrimaryParent(parent)) {
            if (isOfType(parent, parentType)) {
                return parent;
            }
        }

        return null;
    }

    @Override
    public void changeSignataire(NodeRef dossier, NodeRef bureauRef, String annotPub, String annotPrivee) {
        EtapeCircuit etape = getCurrentEtapeCircuit(dossier);
        NodeRef etapeRef = etape.getNodeRef();
        NodeRef parapheur = getParentParapheur(dossier);
        String user = AuthenticationUtil.getRunAsUser();
        Assert.isTrue(etape.getActionDemandee().equals("SIGNATURE"), "L'étape courante n'est pas une étape de signature.");
        Assert.isTrue(isParapheurOwnerOrDelegate(parapheur, user), "L'utilisateur ne fait pas parti des proprietaires ou délégués du bureau.");

        EtapeCircuitImpl nouvelleEtape = new EtapeCircuitImpl();
        nouvelleEtape.setActionDemandee("SIGNATURE");
        nouvelleEtape.setParapheur(bureauRef);

        insertEtapeAfter(etapeRef, nouvelleEtape);

        nodeService.setProperty(etapeRef, ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);

        setAnnotationPrivee(dossier, annotPrivee);
        setAnnotationPublique(dossier, annotPub);

        approve(dossier);
    }

    @Override
    public void avisComplementaire(NodeRef dossier, NodeRef bureauRef, String annotPub, String annotPrivee) {
        EtapeCircuit previous = getPreviousEtapeCircuit(dossier);
        EtapeCircuit etape = getCurrentEtapeCircuit(dossier);
        NodeRef parapheur = getParentParapheur(dossier);

        String user = AuthenticationUtil.getRunAsUser();
        Assert.notNull(previous, "Impossible de demander un avis complémentaire quand le dossier n'est pas émis.");
        Assert.isTrue(isParapheurOwnerOrDelegate(parapheur, user), "L'utilisateur ne fait pas parti des proprietaires ou délégués du bureau.");

        ((EtapeCircuitImpl) etape).setActionDemandee(EtapeCircuit.ETAPE_VISA);
        NodeRef newStepRef = insertEtapeAfter(previous.getNodeRef(), etape);


        EtapeCircuitImpl nouvelleEtape = new EtapeCircuitImpl();
        nouvelleEtape.setActionDemandee(EtapeCircuit.ETAPE_VISA);
        nouvelleEtape.setParapheur(bureauRef);

        NodeRef avisStepRef = insertEtapeAfter(newStepRef, nouvelleEtape);
        nodeService.addAspect(avisStepRef, ParapheurModel.ASPECT_ETAPE_COMPLEMENTAIRE, null);


        setAnnotationPrivee(dossier, annotPrivee);
        setAnnotationPublique(dossier, annotPub);
        approve(dossier);

    }

    @Override
    public void ajouterActeursExternes(NodeRef dossier, List<NodeRef> acteurs) {

        EtapeCircuit etape = getCurrentEtapeCircuit(dossier);
        Assert.isTrue(etape.getActionDemandee().equals(EtapeCircuit.ETAPE_SIGNATURE) ||
                etape.getActionDemandee().equals(EtapeCircuit.ETAPE_VISA), "L'étape courante n'est pas une étape de signature ou de visa.");

        NodeRef parapheur = getParentParapheur(dossier);
        String user = AuthenticationUtil.getRunAsUser();
        Assert.isTrue(isParapheurOwnerOrDelegate(parapheur, user), "L'utilisateur ne fait pas partie des proprietaires ou délégués du bureau.");

        List<NodeRef> precedentsBureaux = (ArrayList<NodeRef>) nodeService.getProperty(etape.getNodeRef(), ParapheurModel.PROP_LISTE_NOTIFICATION);

        // Ajout et suppression des permissions de lecture sur le dossier si il n'est pas public
        Boolean isPublic = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_PUBLIC);
        if ((isPublic != null) && !isPublic) {
            Set<String> precedentsNotifies = new HashSet<String>();

            for (NodeRef bureau : precedentsBureaux) {
                List<String> owners = getParapheurOwners(bureau);
                if (owners != null) {
                    precedentsNotifies.addAll(owners);
                }
            }

            Set<String> nouveauxNotifies = new HashSet<String>();
            for (NodeRef bureau : acteurs) {
                List<String> owners = getParapheurOwners(bureau);

                if (owners != null) {
                    nouveauxNotifies.addAll(owners);
                }
            }

            List<String> toRemove = new ArrayList<String>(precedentsNotifies);
            toRemove.removeAll(nouveauxNotifies);
            toRemove.removeAll(getNotifiesExceptCurrentStep(dossier));

            for (String userToRemove : toRemove) {
                permissionService.deletePermission(dossier, userToRemove, PermissionService.READ);

            }
            List<String> toAdd = new ArrayList<String>(nouveauxNotifies);
            for (String userToAdd : toAdd) {
                permissionService.setPermission(dossier, userToAdd, PermissionService.READ, true);
            }
        }

        nodeService.setProperty(etape.getNodeRef(), ParapheurModel.PROP_LISTE_NOTIFICATION, new ArrayList<NodeRef>(acteurs));
    }

    /**
     * Renvoie la liste des notifiés (propriétaires des bureaux) du dossier passé
     * en paramètre sur toutes les étapes sauf l'étape courante.
     *
     * @param dossier
     * @return la liste des notifiés du dossier passé sur toutes les étapes sauf l'étape courante.
     */
    private Set<String> getNotifiesExceptCurrentStep(NodeRef dossier) {
        Set<String> notifies = new HashSet<String>();
        List<EtapeCircuit> etapes = getCircuit(dossier);
        boolean skipped = false;
        for (EtapeCircuit etape : etapes) {
            if (!skipped && !etape.isApproved()) {
                skipped = true;
            } else {
                List<NodeRef> listeNotif = (ArrayList<NodeRef>) nodeService.getProperty(etape.getNodeRef(), ParapheurModel.PROP_LISTE_NOTIFICATION);
                if (listeNotif != null) {
                    for (NodeRef bureau : listeNotif) {
                        List<String> owners = getParapheurOwners(bureau);
                        if (owners != null) {
                            notifies.addAll(owners);
                        }
                    }
                }
            }
        }
        return notifies;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getListeNotification(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public List<NodeRef> getListeNotification(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");
        List<NodeRef> notifies = (ArrayList<NodeRef>) nodeService.getProperty(getCurrentEtapeCircuit(dossier).getNodeRef(), ParapheurModel.PROP_LISTE_NOTIFICATION);
        return (notifies != null) ? notifies : new ArrayList<NodeRef>();
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getListeNotificationOriginale(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public Set<NodeRef> getListeNotificationOriginale(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");

        String type = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER);
        String sousType = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER);
        Set<NodeRef> notifies = new HashSet<NodeRef>();
        List<EtapeCircuit> circuitOriginal;
        try {
            // Plante si le type ou le sous-type a été modifié/supprimé
            circuitOriginal = workflowService.getSavedWorkflow(typesService.getWorkflow(type, sousType)).getCircuit();
        } catch (Exception e) {
            return notifies;
        }
        List<EtapeCircuit> circuitDossier = getCircuit(dossier);
        int index = -1; // On commence à -1 pour passer l'étape d'émission non présente dans le circuit original.
        boolean etapeNative = true;
        for (EtapeCircuit etape : circuitDossier) {
            if (!etape.isApproved()) { // etape courante
                etapeNative = !nodeService.hasAspect(etape.getNodeRef(), ParapheurModel.ASPECT_ETAPE_NON_NATIVE);
                break;
            }
            if (!nodeService.hasAspect(etape.getNodeRef(), ParapheurModel.ASPECT_ETAPE_NON_NATIVE)) {
                index++;
            }
        }

        if ((etapeNative) && (index >= 0) && (index < circuitOriginal.size())) {
            notifies = circuitOriginal.get(index).getListeNotification();
        }
        return notifies;
    }

    @Override
    public NodeRef insertEtapeAfter(NodeRef currentEtapeRef, EtapeCircuit newStep) {
        // current ---> next
        NodeRef nextStepRef = getFirstChild(currentEtapeRef, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);

        // current -x-> next
        //nodeService.removeAssociation(currentEtapeRef, nextStepRef, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();


        Set listeMetas = newStep.getListeMetadatas();
        if(listeMetas == null) {
            listeMetas = new HashSet();
        }

        Set listeMetasRefus = newStep.getListeMetadatasRefus();
        if(listeMetasRefus == null) {
            listeMetasRefus = new HashSet();
        }

        properties.put(ParapheurModel.PROP_PASSE_PAR, newStep.getParapheur());
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, newStep.getActionDemandee());
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_LISTE_METADONNEES, new ArrayList<String>(listeMetas));
        properties.put(ParapheurModel.PROP_LISTE_METADONNEES_REFUS, new ArrayList<String>(listeMetasRefus));

        // current ---> new    next
        ChildAssociationRef associationRef = this.nodeService.createNode(currentEtapeRef,
                ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE, ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties);

        // current ---> new ---> next
        nodeService.moveNode(nextStepRef, associationRef.getChildRef(), ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE, ParapheurModel.ASSOC_DOSSIER_ETAPE);

        // Ajout de l'aspect ph:etape-non-native
        nodeService.addAspect(associationRef.getChildRef(), ParapheurModel.ASPECT_ETAPE_NON_NATIVE, null);

        return associationRef.getChildRef();
    }

    @Override
    public EtapeCircuit getCurrentOrRejectedEtapeCircuit(NodeRef dossier) {
        Assert.isTrue(isDossier(dossier), "NodeRef doit être un ph:dossier");

        List<EtapeCircuit> circuit = getCircuit(dossier);
        if (circuit == null) {
            return null;
        }
        if (isTermine(dossier) && circuit.get(0).getActionDemandee() == null) {
            return null;
        }
        boolean isRejete = isRejete(dossier);
        EtapeCircuit current = null;

        for (EtapeCircuit etape : circuit) {
            if (etape.isApproved()) {
                current = etape;
            } else {
                if (!isRejete) {
                    current = etape;
                    break;
                }
            }
        }
        return current;
    }

    @Override
    public List<EtapeCircuit> getCircuit(NodeRef dossier, String type, String soustype) {
        return null;
    }

    /**
     * Supprime l'étape passée en paramètre et relie le fils de l'étape à son père.
     *
     * @param currentStepRef le nodeRef de l'étape à supprimer.
     * @return le nodeRef du noeud fils de l'étape supprimmée (ou null si elle
     * n'en avait pas).
     */
    private NodeRef removeEtape(NodeRef currentStepRef) {

        NodeRef previousStepRef = getPrimaryParent(currentStepRef);

        Assert.isTrue((previousStepRef != null) && isOfType(previousStepRef, ParapheurModel.TYPE_ETAPE_CIRCUIT),
                "La première étape d'un circuit ne peut pas être supprimée");

        NodeRef nextStepRef = getFirstChild(currentStepRef, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
        if (nextStepRef != null) {
            // previous --> next
            nodeService.moveNode(nextStepRef, previousStepRef, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE, ParapheurModel.ASSOC_DOSSIER_ETAPE);
        }

        if (EtapeCircuit.ETAPE_SIGNATURE.equals((String) nodeService.getProperty(currentStepRef, ParapheurModel.PROP_ACTION_DEMANDEE))) {
            // Si étape de signature, on remet la signature sur l'étape précédente
            nodeService.setProperty(previousStepRef, ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_SIGNATURE);
        }
        // previous -x-> current -x-> next
        nodeService.deleteNode(currentStepRef);
        return nextStepRef;
    }

    private void removeNonNativesStepsOf(NodeRef dossier) {
        NodeRef stepRef = getFirstChild(getFirstEtape(dossier).getNodeRef(), ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);

        while ((stepRef != null) && this.nodeService.exists(stepRef)) {
            if (nodeService.hasAspect(stepRef, ParapheurModel.ASPECT_ETAPE_NON_NATIVE)) {
                stepRef = removeEtape(stepRef);
            } else {
                stepRef = getFirstChild(stepRef, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
            }
        }
    }


    @Override
    public Map<String, String> getSignaturePolicy(X509Certificate cert, String tdtName, String typeDossier) {
        /**
         * Attention: signature policy != certification policy.
         * La politique de signature ne se trouve pas dans le certificat.
         * C'est pourquoi:   pId = null
         */
        Map<String, String> properties = X509Util.getPolicyProperties(cert);
        String pId = null; // properties.get("pPolicyIdentifierID");
        if (pId != null) {
            String xpath = "/app:company_home/app:dictionary/ph:signaturePolicies/*[@cm:name=\"" + pId + "\"]";
            List<NodeRef> results = searchService.selectNodes(
                    nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                    xpath, null, namespaceService, false);
            NodeRef policyRef;
            if (results.size() >= 1) {
                policyRef = results.get(0);
            } else {
                NodeRef policyHome = getPolicyHomeNodeRef();
                Map<QName, Serializable> policyProps = new HashMap<QName, Serializable>();
                policyProps.put(ContentModel.PROP_NAME, pId);
                policyProps.put(ContentModel.PROP_TITLE, properties.get("pSPURI"));
                policyProps.put(ContentModel.PROP_DESCRIPTION, properties.get("pPolicyIdentifierDescription"));
                policyRef = nodeService.createNode(policyHome, ContentModel.ASSOC_CONTAINS,
                        QName.createQName("cm:" + pId, namespaceService), ContentModel.PROP_CONTENT, policyProps).getChildRef();
                updateSignaturePolicyHash(policyRef);
            }
            if (policyRef != null) {
                ContentReader reader = contentService.getReader(policyRef, ContentModel.PROP_CONTENT);
                if (reader == null) {
                    // WARN: incompatible avec override de TDT sur le type, mais on ne vient plus ici (pId=null...)
                    Properties xades = s2lowService.getXadesSignatureProperties(null);
                    properties.put("pPolicyIdentifierID", xades.getProperty("pPolicyIdentifierID"));
                    properties.put("pPolicyIdentifierDescription", xades.getProperty("pPolicyIdentifierDescription"));
                    properties.put("pPolicyDigest", xades.getProperty("pPolicyDigest"));
                    properties.put("pSPURI", xades.getProperty("pSPURI"));
                    logger.warn("Unable to get true PolicyDigest");
                } else {
                    properties.put("pPolicyDigest", reader.getContentString());
                }
            }
        } else {
            Properties xades;
            // BLEX
            if (SrciService.K.tdtName.equals(tdtName)) {
                xades = srciService.getXadesSignatureProperties();
            } else {
                xades = s2lowService.getXadesSignatureProperties(typeDossier);
            }
            properties.put("pPolicyIdentifierID", xades.getProperty("pPolicyIdentifierID"));
            properties.put("pPolicyIdentifierDescription", xades.getProperty("pPolicyIdentifierDescription"));
            properties.put("pPolicyDigest", xades.getProperty("pPolicyDigest"));
            properties.put("pSPURI", xades.getProperty("pSPURI"));
        }
        return properties;
    }

    public NodeRef getPolicyHomeNodeRef() {
        NodeRef policyHome = null;
        String policiesPath = "/app:company_home/app:dictionary/ph:signaturePolicies";
        List<NodeRef> policiesHomes = searchService.selectNodes(
                nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                policiesPath,
                null,
                namespaceService,
                false);

        if (policiesHomes.size() > 0) {
            policyHome = policiesHomes.get(0);
        } else {
            NodeRef dictionary = searchService.selectNodes(
                    nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                    "/app:company_home/app:dictionary",
                    null,
                    namespaceService,
                    false).get(0);

            policyHome = nodeService.createNode(dictionary,
                    ContentModel.ASSOC_CONTAINS,
                    QName.createQName("ph:signaturePolicies", namespaceService),
                    ContentModel.TYPE_FOLDER).getChildRef();
        }
        return policyHome;
    }

    @Override
    public void updateSignaturePolicyHash(NodeRef policy) {
        String spuri = (String) nodeService.getProperty(policy, ContentModel.PROP_TITLE);
        try {
            URL spurl = new URL(spuri);

            URLConnection spcon = spurl.openConnection();
            InputStream spin = spcon.getInputStream();
            ByteArrayOutputStream spout = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int readed;
            while ((readed = spin.read(buffer)) != -1) {
                spout.write(buffer, 0, readed);
            }

            byte[] data = spout.toByteArray();

            String content = new String(data);
            if (content.contains("<html>") || content.contains("<HTML>")) {
                Pattern pattern = Pattern.compile("(u|U)(r|R)(l|L)=([^\"]+)");
                Matcher matcher = pattern.matcher(content);

                if (matcher.find()) {
                    URL newUrl = new URL(spurl, matcher.group(4));

                    URLConnection newspcon = newUrl.openConnection();
                    InputStream newspin = newspcon.getInputStream();
                    ByteArrayOutputStream newspout = new ByteArrayOutputStream();
                    while ((readed = newspin.read(buffer)) != -1) {
                        newspout.write(buffer, 0, readed);
                    }

                    data = newspout.toByteArray();
                }
            }

            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.update(data);
            String b64hash = Base64.encodeBytes(digest.digest());

            ContentWriter writer = contentService.getWriter(policy, ContentModel.PROP_CONTENT, true);
            writer.setMimetype(MimetypeMap.MIMETYPE_BINARY);
            writer.putContent(b64hash);

            spin.close();
        } catch (MalformedURLException ex) {
            throw new RuntimeException("Invalid SPURI for policy", ex);
        } catch (IOException ex) {
            throw new RuntimeException("Can't download policy", ex);
        } catch (Exception ex) {
            throw new RuntimeException("Unknown exception", ex);
        }
    }

    @Override
    public boolean checkSignature(EtapeCircuit etapeCircuit) {
        NodeRef dossierRef = getParentDossier(((EtapeCircuitImpl) etapeCircuit).getNodeRef());
        String sigFormat = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT);
        if (!sigFormat.startsWith("PKCS#7")) {
            logger.info("checkSignature: pas de vérif car sigFormat=" + sigFormat);
            return false;
        }
        logger.debug("checkSignature avec format=" + sigFormat);
        byte[] signature = getSignature(etapeCircuit);
        NodeRef document = getMainDocuments(dossierRef).get(0);
        ContentReader docReader = contentService.getReader(document, ContentModel.PROP_CONTENT);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        docReader.getContent(out);
        byte[] data = out.toByteArray();
        return PKCS7VerUtil.verify(data, PKCS7VerUtil.pem2der(signature, "-----BEGIN".getBytes(), "-----END".getBytes()));
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getSignatureProperties(com.atolcd.parapheur.repo.EtapeCircuit)
     */
    @Override
    public List<Map<String, String>> getSignatureProperties(EtapeCircuit etape) {
        List<Map<String, String>> sigProps = new ArrayList<Map<String, String>>();
        NodeRef dossierRef = getParentDossier(((EtapeCircuitImpl) etape).getNodeRef());
        String sigFormat = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT);
        if (!sigFormat.startsWith("PKCS#7")) {
            if(sigFormat.startsWith("PKCS#1") || sigFormat.equalsIgnoreCase("xades/detached")) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy 'à' HH:mm", Locale.FRENCH);
                String strObj = (String) nodeService.getProperty(etape.getNodeRef(), ParapheurModel.PROP_SIGNATURE);

                JSONObject obj = null;
                try {
                    obj = new JSONObject(strObj);
                    Map<String, String> map = new HashMap<String, String>();
                    Iterator<String> keysItr = obj.keys();

                    while(keysItr.hasNext()) {
                        String key = keysItr.next();
                        Object value = null;
                        value = obj.get(key);
                        map.put(key, (String) value);
                    }
                    map.put("signature_date", sdf.format(etape.getDateValidation()));
                    sigProps.add(map);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return sigProps;
        }
        try {
            byte[] signature = getSignature(etape);
            Security.addProvider(new BouncyCastleProvider());

            // On gère le cas de signature multi document : On récupère la première signature !
            byte[] firstSignature = new String(signature).split(",")[0].getBytes();

            CMSSignedData signedData = new CMSSignedData(PKCS7VerUtil.pem2der(firstSignature, "-----BEGIN".getBytes(), "-----END".getBytes()));
            // deprecated: CertStore certs = signedData.getCertificatesAndCRLs("Collection", "BC");
            Store certs = signedData.getCertificates();
            SignerInformationStore signers = signedData.getSignerInfos();
            Iterator<SignerInformation> iterator = signers.getSigners().iterator();
            while (iterator.hasNext()) {
                SignerInformation signerInfo = (SignerInformation) iterator.next();
                /*DERSequence timeSeq = (DERSequence) signerInfo.getSignedAttributes()
                 .get(new DERObjectIdentifier("1.2.840.113549.1.9.5"))
				 .toASN1Object()
				 .getDERObject();*/
                if (signerInfo != null) {
                    Date signingDate = null;
                    boolean gotTimestamp = false;
                    DERUTCTime time = null;
                    if (signerInfo.getSignedAttributes() != null
                            && signerInfo.getSignedAttributes().get(new ASN1ObjectIdentifier("1.2.840.113549.1.9.5")) != null) {
                        /**
                         * 1.2.840.113549.1.9.5 is Signing date Attribute, equivalent to PKCS9Attribute.SIGNING_TIME_OID
                         */
                        org.bouncycastle.asn1.cms.Attribute timeObject = signerInfo.getSignedAttributes().get(new ASN1ObjectIdentifier("1.2.840.113549.1.9.5"));

                        if (timeObject != null) {
                            DERSet timeSet = (DERSet) timeObject.getAttrValues();
                            Object derObject = timeSet.getObjects().nextElement();

                            if(derObject instanceof ASN1GeneralizedTime) {
                                ASN1GeneralizedTime derGeneralizedTime = (ASN1GeneralizedTime) derObject;
                                time = new DERUTCTime(derGeneralizedTime.getDate());
                                signingDate = time.getAdjustedDate();
                            } else {
                                time = (DERUTCTime) timeSet.getObjects().nextElement();
                                signingDate = time.getAdjustedDate();
                            }
                        } else {
                            logger.warn("No SIGNING Time found for PKCS7 ???");
                        }
                    } else {
                        /**
                         * There is no PKCS9Attribute.SIGNING_TIME_OID signed
                         * attribute
                         *
                         * Is there any Signed Attributes at all??
                         */
                        if (signerInfo.getSignedAttributes() != null) {
                            logger.warn("No SIGNING Time attribute found for PKCS7 ???\n toString=" + signerInfo.getSignedAttributes().toString());
                        } else {
                            logger.warn("No Signed attributes found for PKCS7???, has "
                                    + signerInfo.getUnsignedAttributes().toASN1EncodableVector().size() + " unsigned attributes." /*
                                                                                                                                       * .getCounterSignatures().size() + "counterSigs."
																																	   */);
                        }
                        /**
                         * Let's check for the Unsigned attributes....
                         */
                        /**
                         * tSTInfo ?
                         */
                        //                        DERObjectIdentifier tSTInfoOID = new DERObjectIdentifier("1.2.840.113549.1.9.16.1.4");
                        //                        if (signerInfo.getUnsignedAttributes().get(tSTInfoOID) != null) {
                        //                            logger.warn("\tHAZ tSTInfo");
                        //                        } else {
                        //                            logger.warn("no tSTInfo.");
                        //                        }
                        /**
                         * possible timestamp?
                         */
                        DigestCalculatorProvider dcp = new BcDigestCalculatorProvider();
                        List<TimeStampToken> timeStamps = (List) TSPUtil.getSignatureTimestamps(signerInfo, dcp);
                        if (!timeStamps.isEmpty()) {
                            signingDate = timeStamps.get(0).getTimeStampInfo().getGenTime();
                            logger.info(" HAZ timestamp !! : " + signingDate.toString());
                            gotTimestamp = true;
                        }
                    }

                    /**
                     * Always finally check for timestamp token, if not already caught
                     */
                    if (!gotTimestamp) {
                        /**
                         * JUST FOR LOG: is there any Signed Attributes??
                         */
                        if (signerInfo.getSignedAttributes() != null) {
                            logger.warn("No SIGNING Time attribute found for PKCS7 ???\n toString=" + signerInfo.getSignedAttributes().toString());
                        } else {
                            logger.warn("No Signed attributes found for PKCS7???, has "
                                    + signerInfo.getUnsignedAttributes().toASN1EncodableVector().size() + " unsigned attributes." // .getCounterSignatures().size() + "counterSigs."
                            );
                        }
                        /**
                         * If not already found... possible timestamp?
                         */
                        DigestCalculatorProvider dcp = new BcDigestCalculatorProvider();
                        List<TimeStampToken> timeStamps = new ArrayList<TimeStampToken>();
                        try {
                            timeStamps = (List) TSPUtil.getSignatureTimestamps(signerInfo, dcp);
                        } catch (Exception e) {
                            logger.warn("Signature timestamp could not be parsed");
                        }
                        if (!timeStamps.isEmpty()) {
                            signingDate = timeStamps.get(0).getTimeStampInfo().getGenTime();
                            logger.info(" HAZ timestamp !! : " + signingDate.toString());
                        } else {
                            logger.debug("          no timestamp !! : ");
                        }
                    }


                    // deprecated X509Certificate cert = (X509Certificate) certs.getCertificates(signerInfo.getSID()).iterator().next();
                    //org.bouncycastle.util.Selector selector = org.bouncycastle.util.Selector().
                    X509CertificateHolder certHolder = (X509CertificateHolder) certs.getMatches(signerInfo.getSID()).iterator().next();
                    // certHolder.getSubject().toString();
                    // deprecated:   X509Certificate cert = (X509Certificate) certs.getMatches(signerInfo.getSID()).iterator().next(); //getCertificates(signerInfo.getSID()).iterator().next();
                    Map<String, String> props = new HashMap<String, String>();
                    props.put("subject_name", X509Util.extractCN(certHolder.getSubject().toString()));
                    String issuerDN = certHolder.getIssuer().toString();
                    //                    System.out.println("      issuer : " + issuerDN);
                    props.put("issuer_name", X509Util.extractCNIssuer(issuerDN));
                    //                    props.put("subject_name", extractCN(cert.getSubjectX500Principal().getName()));
                    //                    props.put("issuer_name", extractCN(cert.getIssuerX500Principal().getName()));
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy 'à' HH:mm", Locale.FRENCH);
                    if (signingDate != null) {
                        props.put("signature_date", sdf.format(signingDate));    // time.getDate().toString()); //etape.getDateValidation().toString());
                    } else {
                        props.put("signature_date", etape.getDateValidation().toString());
                        logger.warn("PKCS7 date signature missing ???  ");
                    }
                    if (logger.isInfoEnabled()) {
                        logger.info("  PKCS7 signature found: " + props.get("subject_name"));
                    }
                    props.put("certificate_valid_from", sdf.format(certHolder.getNotBefore()));
                    props.put("certificate_valid_to", sdf.format(certHolder.getNotAfter()));
                    //                    props.put("certificate_valid_from", sdf.format(cert.getNotBefore()));  // cert.getNotBefore().toString());
                    //                    props.put("certificate_valid_to", sdf.format(cert.getNotAfter()));  // cert.getNotAfter().toString());
                    sigProps.add(props);
                }
            }
            if (logger.isInfoEnabled()) {
                logger.info(" ## PKCS7 file has " + sigProps.size() + " signatures.");
            }
            return sigProps;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public List<NodeRef> getBatchLockedNodes() {
        List<NodeRef> retVal = new ArrayList<NodeRef>();

        StoreRef storeRef = new StoreRef(getConfiguration().getProperty("spaces.store"));

        DossierFilter batchFilter = new DossierFilter();

        batchFilter.setSearchPath(null);
        batchFilter.addFilterElement(ParapheurModel.PROP_IN_BATCH_QUEUE, "true");


        SearchParameters searchParameters = new SearchParameters();

        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.setQuery(batchFilter.buildFilterQuery());
        searchParameters.addStore(storeRef);

        ResultSet res = searchService.query(searchParameters);

        retVal = res.getNodeRefs();

        res.close();

        return retVal;
    }

    @Override
    public void markAsRead(NodeRef dossier) {
        nodeService.addAspect(dossier, ParapheurModel.ASPECT_LU, null);
    }

    @Override
    public boolean isRead(NodeRef dossier) {
        // On estime que le dossier est lu quand le premier document l'est
        return nodeService.hasAspect(getMainDocuments(dossier).get(0), ParapheurModel.ASPECT_LU);
    }

    @Override
    public boolean isSigned(@NotNull NodeRef dossier) {

        List<EtapeCircuit> circuit = getCircuit(dossier);
        boolean isRejete = isRejete(dossier);
        boolean isSigned = false;
        boolean checkNextEtape = false;

        if (circuit == null) {
            return false;
        }

        for (EtapeCircuit etape : circuit) {
            if(checkNextEtape && etape.isApproved()) {
                isSigned = true;
                checkNextEtape = false;
            }
            if (StringUtils.equalsIgnoreCase(etape.getActionDemandee(), EtapeCircuit.ETAPE_SIGNATURE)) {
                if(etape.isApproved()) {
                    if(isRejete) {
                        checkNextEtape = true;
                    } else {
                        isSigned = true;
                    }
                }
            }
        }

        return isSigned;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isDigitalSignatureMandatory(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean isDigitalSignatureMandatory(NodeRef dossier) {
        Boolean digitalSignatureMandatory = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_DIGITAL_SIGNATURE_MANDATORY);

        if (digitalSignatureMandatory == null) {
            if ("blex".equals(getHabillage())) {
                // BL prefere que les gens signent electroniquement par defaut
                return true;
            }
            // Compatibilité ascendante : on permet la signature papier
            return false;
        }

        return digitalSignatureMandatory;
    }

    @Override
    public boolean isReadingMandatory(NodeRef dossier) {
        Boolean readingMandatory = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_READING_MANDATORY);

        if (readingMandatory == null) {
            // Backward compatibility : reading was mandatory by default
            return true;
        }

        return readingMandatory;
    }

    @Override
    public boolean isForbidSameSig(NodeRef dossier) {
        if(dossier == null) return false;
        return typesService.isForbidSameSig((String) nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER), (String) nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER));
    }

    @Override
    public boolean areAttachmentsIncluded(NodeRef dossier) {
        Boolean includeAttachments = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_INCLUDE_ATTACHMENTS);
        // Backward compatibility : include attachments by default
        return (includeAttachments == null) || includeAttachments;
    }

    @Override
    public NodeRef getParapheursHome() {
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" + this.configuration.getProperty("spaces.parapheurs.childname");

        List<NodeRef> results;
        results = searchService.selectNodes(nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))), xpath, null, namespaceService, false);

        return results.get(0);
    }

    @Override
    public List<NodeRef> getChildParapheurs(NodeRef nodeRef) {
        List<AssociationRef> associations = nodeService.getSourceAssocs(nodeRef, ParapheurModel.ASSOC_HIERARCHIE);
        List<NodeRef> children = new ArrayList<NodeRef>();
        for (AssociationRef association : associations) {
            children.add(association.getSourceRef());
        }
        return children;
    }

    @Override
    public String getCurrentValidator(NodeRef dossier) {
        EtapeCircuitImpl etapeCircuit = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
        return etapeCircuit.getValidator();
    }

    @Override
    public void setCurrentValidator(NodeRef dossier, String validator) {
        EtapeCircuitImpl etapeCircuit = (EtapeCircuitImpl) getCurrentEtapeCircuit(dossier);
        nodeService.setProperty(etapeCircuit.getNodeRef(), ParapheurModel.PROP_VALIDATOR, validator);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#showAVenir(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean showAVenir(NodeRef parapheur) {
        Boolean show = (Boolean) nodeService.getProperty(parapheur, ParapheurModel.PROP_SHOW_A_VENIR);
        return ((show != null) ? show : true);
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#searchUser(String)
     */
    @Override
    public List<NodeRef> searchUser(String search) {
        search = search != null ? search : "";
        FacesContext context = FacesContext.getCurrentInstance();
        UserTransaction tx = null;
        List<NodeRef> people = new ArrayList<NodeRef>();
        try {
            tx = Repository.getUserTransaction(context, true);
            tx.begin();

            // define the query to find people by their first or last name
            StringBuilder query = new StringBuilder(128);
            Utils.generatePersonSearch(query, search);

            if (logger.isDebugEnabled()) {
                logger.debug("Query " + query.toString());
            }

            // define the search parameters
            SearchParameters params = new SearchParameters();
            params.setLanguage(SearchService.LANGUAGE_LUCENE);
            params.addStore(Repository.getStoreRef());
            params.setQuery(query.toString());

            ResultSet results = searchService.query(params);

            try {
                people = results.getNodeRefs();
            } finally {
                results.close();
            }
            // Désolé l'utilisateur 'admin' peut posséder un parapheur
            //people.remove(personService.getPerson(AuthenticationUtil.getAdminUserName()));
            people.remove(personService.getPerson(tenantService.getDomainUser(AuthenticationUtil.getGuestUserName(), tenantService.getCurrentUserDomain())));
            people.remove(personService.getPerson(tenantService.getDomainUser(AuthenticationUtil.getSystemUserName(), tenantService.getCurrentUserDomain())));

            if (logger.isDebugEnabled()) {
                logger.debug("Found " + people.size() + " users");
            }
            tx.commit();
        } catch (Exception err) {
            logger.error("Erreur lors de la recherche de l'utilisateur : ", err);
            try {
                if (tx != null) {
                    tx.rollback();
                }
            } catch (Exception tex) {
            }
        }
        return people;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#isOwner(java.lang.String)
     */
    @Override
    public boolean isOwner(String userName) {
        boolean owner = false;
        String prefix = "ROLE_PHOWNER_";
        Set<String> authorities = this.authorityService.getAuthoritiesForUser(userName);
        for (String authString : authorities) {
            if (authString.startsWith(prefix)) {
                owner = true;
                break;
            }
        }
        return owner;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#getTargetVersion()
     */
    @Override
    public String getTargetVersion() {
        String targetVersion = this.configuration.getProperty("parapheur.mail.targetVersion");
        // Default targetVersion is 3
        return (targetVersion != null) ? targetVersion : "3";
    }

    public static class TdtState {

        private String status, ackDate;

        public TdtState(String status, String ackDate) {
            this.status = status;
            this.ackDate = ackDate;
        }

        public String getAckDate() {
            return ackDate;
        }

        public String getStatus() {
            return status;
        }
    }
}
