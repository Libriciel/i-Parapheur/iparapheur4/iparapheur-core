package com.atolcd.parapheur.repo;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.util.Assert;

import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 */
public class S2lowMailSecGetInfosJob implements Job {
    private static Logger logger = Logger.getLogger(S2lowMailSecGetInfosJob.class);

    public void execute(final JobExecutionContext ctx) throws JobExecutionException {
        JobDataMap dataMap = ctx.getJobDetail().getJobDataMap();

        final S2lowService s2lowService = (S2lowService) dataMap.get("s2lowService");
        final TransactionService transactionService = (TransactionService) dataMap.get("transactionService");
        final NodeRef dossier = (NodeRef) dataMap.get("dossier");
        String runAs = (String) dataMap.get("runAs");

        Assert.notNull(s2lowService, "s2lowService is mandatory");
        Assert.notNull(transactionService, "transactionService is mandatory");
        Assert.notNull(dossier, "dossier is mandatory");
        if(runAs == null) {
            logger.warn("Error retrieving S2low status. RunAs is mandatory... Can't do nothing, cancel job...");
            UserTransaction utx = transactionService.getUserTransaction();
            try {
                utx.begin();

                ctx.getScheduler().unscheduleJob(
                        ctx.getJobDetail().getName(),
                        "S2LOW-MAILSEC");

                utx.commit();
            } catch (Exception ex) {
                logger.warn("Error cancelling mailsec job. Retrying in 5 minutes", ex);
                ex.printStackTrace();
                try {
                    utx.rollback();
                } catch (SystemException e) {
                    e.printStackTrace();
                }
            }
            return;
        }

        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {

            public Object doWork() throws Exception {
                UserTransaction utx = transactionService.getUserTransaction();
                try {
                    utx.begin();

                    s2lowService.getSecureMailInfos(dossier);

                    utx.commit();
                } catch (Exception ex) {
                    logger.warn("Error retrieving S2low status. Retrying in 5 minutes", ex);
                    ex.printStackTrace();
                    utx.rollback();
                }
                return null;
            }

        }, runAs);
    }

}
