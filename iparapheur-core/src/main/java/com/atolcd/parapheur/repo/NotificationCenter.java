package com.atolcd.parapheur.repo;
/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2012, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 * contact@atolcd.com
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

import com.atolcd.parapheur.repo.notifications.NotificationCenterObserver;
import org.adullact.iparapheur.repo.notification.Notification;
import org.alfresco.service.cmr.repository.NodeRef;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * NotificationCenter
 * Cette interface permet de gérer les notifications et des le diffuser aux utilisateurs.
 * Trois modes de diffusions sont possible :
 *     - par mail direct,
 *     - par mail groupé (enregistrement des notifications et un job qui vient relever celles à envoyer à intervalle
 *       défini dans les options utilisateurs),
 *     - par websocket pour les utilisateurs connectés.
 *
 * @author manz Date: 13/02/12 Time: 12:32
 * @author jmaire
 */
public interface NotificationCenter {

    /**
     * Différents types d'observeurs
     */
    public enum NotificationObserverType {
        MOBILE_OBSERVER
    }

    enum Reason {
        approve,
        reject,
        remord,
        reviewing,
        print,
        archive,
        deleteAdmin,
        moveAdmin,
        create,
        edit,
        delegation,
        retard,
        delete,
        raz,
        chain,
        error,
        signInfo,
        emit,
        resetpassword
    }

    /** Les constantes VALUE_TYPE déterminent sur quel objet porte une notification.
     *  Si c'est un dossier, alors on peut bloquer le noeud (ex. au passage d'un étape),
     *  pour un bureau, c'est juste informatif (en lecture), on ne bloque pas le noeud
     *  (ex. mise en place d'une délégation).
     **/
    public static final String VALUE_TYPE_DOSSIER = "dossier";
    public static final String VALUE_TYPE_BUREAU = "bureau";

    public static final String VALUE_FREQUENCY_NEVER = "never";
    public static final String VALUE_FREQUENCY_ALWAYS = "always";
    public static final String VALUE_FREQUENCY_HOURLY = "hourly";
    public static final String VALUE_FREQUENCY_DAILY = "daily";
    public static final String VALUE_FREQUENCY_WEEKLY = "weekly";

    public static final String MODEL_URL = "url";
    public static final String MODEL_NOM_ACTION_DEMANDEE = "nomActionDemandeee";
    public static final String MODEL_NOM_ACTION_EFFECTUEE = "nomActionEffectuee";
    public static final String MODEL_FOOTER = "footer";
    public static final String MODEL_NB_DOSSIERS = "nbDossiers";
    public static final String MODEL_AS_SECRETARY = "asSecretary";

    /**
     * Marque la notification comme lue Positionne également le champ
     * modifiedTimestamp de la notification.
     *
     * @param username
     * @param notification
     */
    public void markNotificationAsRead(String username, Notification notification);

    /**
     * Marque toutes les notifications comme lues.
     *
     * @param username
     */
    public void markNotificationsAsRead(String username);

    /**
     * Ajoute la notification au profil de l'utilisateur désigné par username.
     * L'UUID de la notification est automatiquement positionné
     *
     * @param username
     * @param notification
     */
    public void postNotification(String username, Notification notification);

    /**
     * Envoie un mail aux l'utilisateurs passés en paramètre. Des pièces jointes peuvent être fournies.
     * @param notification notification contenant les informations sur le dossier et l'action faite dessus.
     * @param attachments les fichiers à joindre (peut être null ou vide).
     * @param users les usernames des destinataires.
     */
    void sendMailToUsers(Notification notification, List<String> users, Map<String, File> attachments);

    /**
     * Diffuse les notifications aux utilisateurs passés en paramètre.
     *
     * @param users
     * @param notification
     */
    public void broadcastNotification(List<String> users, Notification notification);

    void broadcastNotification(List<String> users, Notification notification, boolean digest, boolean forceMail);

    /**
     * Diffuse les notifications aux utilisateurs passés en paramètre seulement sur les websockets
     * des utilisateurs connectés.
     * @param users les utilisateurs concernés par la notification
     * @param notification la notification à envoyer
     */
    void broadcastSocketNotification(List<String> users, Notification notification);

    /**
     * @param username
     */
    public void enableNotificationsForUser(String username);

    public void disableNotificationsForUser(String username);

    /**
     * Supprime les notifications pour l'utilisteur passé en paramètre
     *
     * @see com.atolcd.parapheur.model.ParapheurModel#PROP_NOTIFICATION_MAP
     * @see
     * com.atolcd.parapheur.model.ParapheurModel#PROP_NOTIFICATION_UNREAD_LIST
     * @param username
     */
    public void clearNotifications(String username);

    /**
     * Récupère la notification identifiée par l'UUID pour un utilisateur ciblé.
     *
     * @param username L'utilisateur proprietaire de la notification
     * @param uuid L'identifiant de la notification
     * @return une notification
     */
    public Notification getNotificationForUUID(String username, UUID uuid);

    /**
     *
     * @param username
     * @param uuid
     * @param notification
     */
    // public void setNotificationForUUID(String username, UUID uuid, Notification notification);
    /**
     *
     * @param username
     * @return
     */
    public List<Notification> getUnreadNotificationsForUser(String username);

    /**
     * @param username
     * @return
     */
    public List<Notification> getNotificationsForUser(String username);


    /* Observer Pattern methods */
    /**
     *
     * @param observer
     * @param type
     */
    public void registerObserver(NotificationCenterObserver observer, NotificationObserverType type);

    /**
     *
     * @param observer
     */
    public void unregisterObserver(NotificationCenterObserver observer);

    /**
     *
     * @param notification
     */
    public void notifyObservers(Notification notification);

    /**
     * Supprime la notification passée en paramètre.
     * @param notification
     */
    public void deleteNotification(Notification notification);

    /**
     * Renvoie vrai si l'utilisateur possède un certificat de connexion
     * @param user
     * @return vrai si l'utilisateur possède un certificat de connexion, faux sinon
     */
    boolean hasCertificate(NodeRef user);

    /**
     * Renvoi une adresse mail valide pour les mails groupés ou directs.
     * En priorité, on va voir le mail de notification dans les préférences utilisateurs, sinon, on prend
     * l'email du compte utilisateur.
     * @param user
     * @return une adresse email valide ou null si aucune n'est ratachée à l'utilisateur.
     */
    public String[] getValidAddressForUser(NodeRef user);


    /**
     * Envoi de mail pour reset de password
     * @param username username de l'utilisateur
     * @param usermail mail de l'utilisateur
     * @param uuid uuid unique de remise à zéro
     */
    public void sendResetPassword(String username, String usermail, String uuid);
}
