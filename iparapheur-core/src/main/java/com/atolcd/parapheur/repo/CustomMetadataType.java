/*
 * Version 3.2
 * CeCILL Copyright (c) 2010-2013, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed and maintained by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Classe définissant les types de métadonnées supportés par i-parapheur
 *
 * @author Vivien Barousse - ADULLACT Projet
 */
public enum CustomMetadataType {

    STRING("d:text"),
    INTEGER("d:int"),
    DOUBLE("d:double"),
    DATE("d:date"),
    BOOLEAN("d:boolean"),
    URL("d:text");

    private String alfType;

    CustomMetadataType(String alfType) {
        this.alfType = alfType;
    }

    public String getAlfrescoType() {
        return alfType;
    }

    public static CustomMetadataType fromAlfrescoType(String alfType) {

        for (CustomMetadataType type : values()) {
            if (type.getAlfrescoType().equals(alfType)) {
                return type;
            }
        }

        throw new IllegalArgumentException("'" + alfType + "' isn't a known metadata type");
    }

    public static @NotNull CustomMetadataType fromPropertyXml(@Nullable String aspectValue, @NotNull String alfType) {

        CustomMetadataType result = null;

        if (aspectValue != null) {
            result = valueOf(aspectValue);
        }

        if (result == null) {
            result = fromAlfrescoType(alfType);
        }

        // System.out.println(" fromPropertyXml - in:"+ aspectValue + "/" + alfType + " out:" + result);
        return result;
    }

}

