/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.repo;

import java.util.List;
import java.util.Set;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * Interface de gestion de Circuit de validation dans iParapheur
 * @author svast
 */
public interface SavedWorkflow
{

    /**
     * Returns the node ref containing this saved workflow
     * @return the workflow node ref
     */
    public NodeRef getNodeRef();

   /**
    * Renvoie la liste des étapes du circuit de validation
    * 
    * @return la liste des étapes
    */
   public abstract List<EtapeCircuit> getCircuit();

   /**
    * Renvoie la liste des parapheurs composant le circuit de validation
    * 
    * @return la liste des références aux parapheurs
    */
   public abstract List<NodeRef> getListeEtapesParapheurs();

   /**
    * Renvoie l'ensemble des parapheurs de la liste de notification
    * Cette méthode n'a pas de sens dans le cadre de la nouvelle
    * gestion de circuit de validation: il y a une liste par étape du circuit
    *
    * @return l'ensemble des références aux parapheurs pour l'étape courante
    */
   /*
   public abstract Set<NodeRef> getDiffusion();
    */

   /**
    * Renvoie l'ensemble des parapheurs notifiés pour l'étape passée en paramètre
    *
    * @return ensemble de références aux parapheurs
    */
   public abstract Set<NodeRef> getDiffusion(EtapeCircuit etape);

   /**
    * Renvoie le nom du circuit
    * @return le nom du circuit
    */
   public abstract String getName();

   public boolean isPublic();

   public abstract Set<NodeRef> getAclParapheurs();

    public abstract Set<String> getAclGroupes();
}
