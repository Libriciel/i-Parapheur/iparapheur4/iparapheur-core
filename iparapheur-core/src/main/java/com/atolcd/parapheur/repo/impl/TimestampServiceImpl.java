/*
 * Version 1.1
 * CeCILL Copyright (c) 2010-2011, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.repo.TimestampService;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import javax.transaction.UserTransaction;
import org.adullact.libersign.util.signature.JetonHorodatage;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.util.HttpURLConnection;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

/**
 * @see com.atolcd.parapheur.repo.TimestampService
 *
 * @author Stephane Vast
 */
public class TimestampServiceImpl implements TimestampService {

    private static Logger logger = Logger.getLogger(TimestampService.class);

    private static final String HORODATAGE_SERVICE_ADULLACT = "adullact";
    private static final String HORODATAGE_SERVICE_CERTEUROPE = "certeurope";

    private static final String CERTIDATE_CODE_APPLICATION = "5d7d4549725f156f533ba8aab3f04739";
    private static final String CERTIDATE_URL_QUALIF = "https://horodatage2.certeurope.fr/index.php";
    private static final String CERTIDATE_URL_PROD = "https://horodate.certeurope.fr";

    private Properties configuration;

    private NodeService nodeService;

    private NamespaceService namespaceService;

    private SearchService searchService;

    private FileFolderService fileFolderService;

    private TransactionService transactionService;

    public boolean isEnabled() {
        try {
            Map<String, String> props = this.getProperties();
            if (props.get("name")!=null &&
                    !props.get("name").trim().equals("_off_")) {
                return true;
            }
        } catch(AlfrescoRuntimeException e) {
            logger.error("Unable to get the properties: " + e.getLocalizedMessage());
        }
        return false;
    }


    /**
     * @see com.atolcd.parapheur.repo.TimestampService
     */
    public boolean isAvailable() {
        // FIXME : do the job
        return true;
        // throw new UnsupportedOperationException("Not supported yet.");
    }


    public Map<String, String> getProperties() {
        try {
            Map<String, String> properties = new HashMap<String, String>();
            SAXReader reader = new SAXReader();
            // cm:timestamper_configuration.xml
            Document document = reader.read(readFile("timestamper_configuration.xml"));
            // List<Node> nodes = document.getRootElement().element("horodate").elements();
            List<Node> nodes = document.getRootElement().elements();
            for (Node node : nodes) {
                properties.put(node.getName(), node.getText());
            }
            return properties;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new AlfrescoRuntimeException(ex.getMessage(), ex);
        }
    }

    public JetonHorodatage getTimeStampToken(String hash) {
        Map<String, String> timestamperProps = getProperties();
        String serviceName = timestamperProps.get("name");

        if (serviceName == null || serviceName.trim().isEmpty()) {
            logger.error("No timestamp service name specified, abort");
            return null;
        }
        serviceName = serviceName.trim().toLowerCase();
        if (serviceName.equals(TimestampServiceImpl.HORODATAGE_SERVICE_ADULLACT)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Go for Adullact timestamping service");
            }
            return getTimeStampOpensign(hash, timestamperProps);
        } else if (serviceName.equals(TimestampServiceImpl.HORODATAGE_SERVICE_CERTEUROPE)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Go for Certeurope timestamping service");
            }
            // Hash de test "01b15dacd69e334b3ab0f8c543826519ac7bd177"
            return getTimeStampCertidate(hash, timestamperProps);
        } else {
            logger.error("the service name specified '" + serviceName + "' is unknown");
        }
        return null;
    }

    private JetonHorodatage getTimeStampOpensign(String hash, Map<String, String> props) {
        //String response = null;
        JetonHorodatage jeton = null;

        if (logger.isDebugEnabled()) {
            logger.debug("getTimeStampOpensign: hash=" + hash);
        }
        //String urlString = "http://" + props.get("server") + ":" + props.get("port") + "/horodatage.php";

        HttpClient client = createConnectionHTTP(props);
        PostMethod post = new PostMethod("/horodatage.php");
        // HttpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        post.addParameter("action", "0");
        post.addParameter("clientId", props.get("idclient"));
        String algorithme = props.get("algorithme");
        post.addParameter("algo", algorithme);
        post.addParameter("hash", hash);
        try {
            int status = client.executeMethod(post);
            if (HttpStatus.SC_OK == status) {
                String response = post.getResponseBodyAsString();

                if (logger.isDebugEnabled()) {
                    logger.debug(response);
                }
                String[] rep = response.split("\n\n");
                if (rep.length >= 5) {
                    if (OpensignConnector.ERR_SUCCESS.equals(rep[0])) {
                        jeton = new JetonHorodatage();
                        jeton.setToken(rep[1]);
                        jeton.setNumero(rep[2]);
                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            jeton.setDate(dateFormat.parse(rep[3]));
                        } catch (java.text.ParseException ex) {
                            if (logger.isDebugEnabled()) {
                                logger.debug(ex.getLocalizedMessage());
                            }
                            jeton.setDate(null);
                        }
                        jeton.setHash(hash);
                        jeton.setAlgorithme(algorithme);
                    }
                } else {
                    /**
                     * something went wrong...
                     */
                    OpensignConnector osConnector = new OpensignConnector();
                    logger.error("No luck: " + osConnector.ErrcodeToString(rep[0]));
                }
            } else {
                throw new IOException("Echec de la récupération de la connexion à la plate-forme : statut = " + status);
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(TimestampServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            post.releaseConnection();
        }
        return jeton;
    }

    private HttpClient createConnectionHTTP(Map<String, String> props) {
        HttpClient plop = new HttpClient();
        plop.getHostConfiguration().setHost(props.get("server"), Integer.parseInt(props.get("port")));
        return plop;
    }
    
    private JetonHorodatage getTimeStampOpensign_quiMarchepas(String hash, Map<String, String> props) {
        String response = null;
        Writer writer = null;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("getTimeStampOpensign: hash=" + hash);
            }
            URL url = null;
            // HttpURLConnection HttpConn = null;

            // http://opensign.test.adullact.org/horodatage.php
            String urlString = "http://" + props.get("server") + ":" + props.get("port") + "/horodatage.php";
            System.out.print("0[" + urlString + "] ");
            url = new URL( urlString ); //props.get("server"));
            if (url == null) {
                logger.warn("WTF???? url is Null !");
            }

            System.out.println("1 ");
            HttpURLConnection HttpConn = (HttpURLConnection) url.openConnection();
            System.out.print("2 ");
            HttpConn.setRequestMethod("POST");
            System.out.print("3 ");
            HttpConn.setUseCaches(false);
            HttpConn.setDoInput(true);
            HttpConn.setDoOutput(true);
            HttpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            System.out.print("4 ");
            writer = new BufferedWriter(new OutputStreamWriter(HttpConn.getOutputStream()));
            System.out.print("5 ");
            writer.write("&action=0");
            writer.write("clientId=" + props.get("idclient"));
            String algorithme = props.get("algorithme");
            writer.write("&algo=" + algorithme);
            writer.write("&hash=" + hash);
            writer.close();
            InputStream inputStream = HttpConn.getInputStream();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] temp = new byte[1024];
            int readedBytes = -1;
            do {
                readedBytes = inputStream.read(temp, 0, 1024);
                if (readedBytes > 0) {
                    byteArrayOutputStream.write(temp, 0, readedBytes);
                }
            } while (readedBytes > 0);
            response = new String(byteArrayOutputStream.toByteArray());
            String[] rep = response.split("\n\n");
            if (rep.length >= 5) {
                if (OpensignConnector.ERR_SUCCESS.equals(rep[0])) {
                    JetonHorodatage jeton = new JetonHorodatage();
                    jeton.setToken(rep[1]);
                    jeton.setNumero(rep[2]);
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        jeton.setDate(dateFormat.parse(rep[3]));
                    } catch (java.text.ParseException ex) {
                        if (logger.isDebugEnabled()) {
                            logger.debug(ex.getLocalizedMessage());
                        }
                        jeton.setDate(null);
                    }
                    jeton.setHash(hash);
                    jeton.setAlgorithme(algorithme);
                    return jeton;
                }
            } else {
                /**
                 * something went wrong...
                 */
                OpensignConnector osConnector = new OpensignConnector();
                logger.error("No luck: " + osConnector.ErrcodeToString(rep[0]));
                if (logger.isDebugEnabled()) {
                    logger.debug(response);
                }
            }
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage(), ex);
            throw new AlfrescoRuntimeException(ex.getLocalizedMessage(), ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                logger.error("cloture writer finally", ex);
            }
        }

        return null;
    }

    private JetonHorodatage getTimeStampCertidate(String hash, Map<String, String> props) {
        String response = null;
        Writer writer = null;

        try {
            URL url = null;
            HttpURLConnection HttpConn = null;
            url = new URL( "https://" + props.get("server") );
            HttpConn = (HttpURLConnection) url.openConnection();
            HttpConn.setRequestMethod("POST");
            HttpConn.setUseCaches(false);
            HttpConn.setDoInput(true);
            HttpConn.setDoOutput(true);
            HttpConn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            writer = new BufferedWriter(new OutputStreamWriter(HttpConn.getOutputStream()));
            writer.write("appId="+ props.get("idclient") ); // CERTIDATE_CODE_APPLICATION); //
            writer.write("&action=0");
            writer.write("&hash=" + hash);
            writer.write("&algo=SHA1");
            writer.close();
            InputStream inputStream = HttpConn.getInputStream();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] temp = new byte[1024];
            int readedBytes = -1;
            do {
                readedBytes = inputStream.read(temp, 0, 1024);
                if (readedBytes > 0) {
                    byteArrayOutputStream.write(temp, 0, readedBytes);
                }
            } while (readedBytes > 0);

            response = new String(byteArrayOutputStream.toByteArray());
            String rep[]=response.split("\n\n");
            if (rep.length>=5)
            {
                if ("0".equals(rep[0]))//La réponse de l'horodateur es ok.
                {
                    //                this.jeton=rep[1];
                    //                this.serial=rep[2];
                    //                this.dateH=rep[3];
                    JetonHorodatage jeton = new JetonHorodatage();
                    jeton.setToken(rep[1]);
                    jeton.setNumero(rep[2]);
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        jeton.setDate(dateFormat.parse(rep[3]));
                    } catch (java.text.ParseException ex) {
                        if (logger.isDebugEnabled()) {
                            logger.debug(ex.getLocalizedMessage());
                        }
                        jeton.setDate(null);
                    }
                    jeton.setHash(hash);
                    jeton.setAlgorithme("SHA1");
                    return jeton;
                } else {
                    logger.error(response);
                }
            } else {
                logger.debug(response);
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
            throw new AlfrescoRuntimeException(ex.getMessage(), ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                logger.error("", ex);
            }
        }

        return null;
    }

    /**
     * Lecture d'un fichier dans 'Company Home/Dictionnaire de données/certificats'
     *
     * @param nameFile
     * @return inputstream contenu du fichier
     * @throws Exception
     */
    private InputStream readFile(String nameFile) throws Exception {
        UserTransaction trx = transactionService.getUserTransaction();
        InputStream inputStream = null;
        try {
            trx.begin();

            // Recherche du noeud Certificats dans Dictionary
            String xpath = "/app:company_home/app:dictionary/ph:certificats/cm:timestamper_configuration.xml";
            List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                "/app:company_home/app:dictionary/ph:certificats/cm:timestamper_configuration.xml",
                null, namespaceService, false);
                        // .query(new StoreRef(this.configuration.getProperty("spaces.store")), searchService.LANGUAGE_XPATH, xpath);

            if (nodes.size() == 1) {
                NodeRef file = nodes.get(0);

                if (file != null) {
                    ContentReader content = fileFolderService.getReader(file);
                    inputStream = content.getContentInputStream();
                }
            }

            trx.commit();
            return inputStream;
        } catch (Exception e) {
            try {
                trx.rollback();
            } catch (Exception ex) {
                logger.error("Error during rollback", ex);
            }
            throw e;
        }
    }

    /**
     * Liste des fonctions spécifiques Opensign:
     *  - codes erreur
     */
    private class OpensignConnector {

        /**
         * Codes retournés par le serveur d'horodatage
         */
        //    0   Requête traitée avec succès.
        public static final String ERR_SUCCESS = "0";
        //    1   Time-out
        public static final String ERR_TIMEOUT = "1";
        //    2   Paramètres manquants dans la requête
        public static final String ERR_MISSING_PARAMS = "2";
        //    3   Identifiant client inconnu
        public static final String ERR_UNKNOWN_PEER = "3";
        //    4   Service indisponible
        public static final String ERR_SERVICE_UNAVAILABLE = "4";
        //    5   Permission denied : Identifiant client connu, mais pas d'autorisation
        public static final String ERR_PERM_DENIED = "5";
        //    6   CRL serveur inaccessible
        public static final String ERR_CRL_UNREACHABLE = "6";
        //    7   Jeton introuvable, numero de serie inconnu)
        public static final String ERR_UNKNOWN_TOKEN = "7";
        //    8   Autre erreur
        public static final String ERR_OTHER = "8";

        // codes action "idaction"
        //    0 : Demande de jeton
        //    1 : Vérification de jeton
        //    2 : Récupération d'infos sur un jeton (OPTION)
        //    3 : Téléchargement d'un jeton archivé (OPTION)

        /**
         * paramètre pour algorithme de hachage
         */
        public String ErrcodeToString(String errorcode) {
            if (errorcode==null || errorcode.trim().isEmpty()) {
                return "Unknown ERROR CODE: " + errorcode;
            }
            String message = null;
            try {
                int parseInt = Integer.parseInt(errorcode);
                switch(parseInt) {
                    case 0:
                        message = "Requête traitée avec succès";
                        break;
                    case 1:
                        message = "Time-out";
                        break;
                    case 2:
                        message = "Paramètres manquants dans la requête";
                        break;
                    case 3:
                        message = "Identifiant client inconnu";
                        break;
                    case 4:
                        message = "Service indisponible";
                        break;
                    case 5:
                        message = "Permission denied : Identifiant client connu, mais pas d'autorisation";
                        break;
                    case 6:
                        message = "CRL serveur inaccessible";
                        break;
                    case 7:
                        message = "Jeton introuvable, numero de serie inconnu";
                        break;
                    case 8:
                    default:
                        message = "Autre erreur";
                        break;
                }
            } catch (java.lang.NumberFormatException ex) {
                if (logger.isDebugEnabled()) {
                    logger.debug(ex.getLocalizedMessage());
                }
                return "Unknown ERROR CODE: " + errorcode;
            }
            return message;
        }
    }

    /*
     * Auto generated setters, used by Spring for dependency injection
     *
     * <editor-fold defaultstate="collapsed" desc="Auto generated setters, used by Spring for dependency injection">
     */
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    // </editor-fold>
}
