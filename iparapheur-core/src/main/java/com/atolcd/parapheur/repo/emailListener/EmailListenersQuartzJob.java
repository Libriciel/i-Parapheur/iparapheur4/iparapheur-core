/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 * 
 */
package com.atolcd.parapheur.repo.emailListener;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.internet.MimeUtility;
import javax.transaction.UserTransaction;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.NoSuchPersonException;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.SavedWorkflow;

/**
 * This class is deprecated as it doesn't work in a multi tenant mode.
 * 
 * @author svast
 * @deprecated since 3.0
 */
@Deprecated
public class EmailListenersQuartzJob implements Job
{
    /** Used for running the initialization code or not; if true, initialization is complete */
    private static boolean initialized = false;
    
    // 1ere méthode
    // private List<NodeRef> nodeRefs = new ArrayList<NodeRef>();

    /** MailBox parameters */
    private static String mailServer;
    private static String protocol;
    private static Integer mailPort;
    private static String emailFolder;
    private static String emailUserName;
    private static String emailPassword;

    /** Alfresco Service Registry - used for finding necessary Alfresco services */
    private static ServiceRegistry serviceRegistry;
    private static SearchService searchService;
    /** Alfresco Node Service */
    private static NodeService nodeService;
    // private static NamespaceService namespaceService;
    private static ParapheurService parapheurService;
    private static DossierService dossierService;
    /** Alfresco AuthenticationComponent object - used for being able to run as the system admin */
    private static AuthenticationComponent authenticationComponent;
    
    
    private static NodeRef circuitsHome;

    /** Used for logging */
    private static Log logger = LogFactory.getLog(EmailListenersQuartzJob.class);


    
    
    /**
     * The main method. Runs at intervals defined in scheduled-jobs-context.xml (repeatInterval)
     * 
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
	//logger.debug("initialize");
	initialize(context);
	// After this, Alfresco services will think that the system admin is running this code
	// logger.debug("setSystemUserAsCurrentUser");
	authenticationComponent.setSystemUserAsCurrentUser();

	// 2nde version ADULLACT: 1 seule boite email, dont les propriétés sont dans module-context.xml
	//logger.debug("checkMailBox");
	checkMailBox();
	
	// 1ere version : la boite email est configurée en ASPECT du circuit: 1 boite/circuit, lourd.
//	logger.debug("findValidEmailListenerAspects");
//	findValidEmailListenerAspects();
//	// logger.debug("Will check for: " + emailListenerTasks.size() + " EMailListener tasks...");
//	for (NodeRef circuitRef : this.nodeRefs)
//	{
//	    logger.debug("checkMailBox");
//	    checkMailBox(circuitRef);
//	}
    }

    private void checkMailBox()
    {
	// 1 chercher les emails
	if (emailFolder == null || emailFolder.equals("") || protocol.equals("pop3"))
	{
	    emailFolder = "INBOX";
	}
	logger.debug("checkMailBox  "+protocol+"://"+emailUserName+"@"+mailServer+":"+mailPort+"/"+emailFolder);
	URLName url = new URLName(protocol, mailServer, mailPort, emailFolder, emailUserName, emailPassword);
	Properties props = System.getProperties();
	Session session = Session.getDefaultInstance(props, null);
	session.setDebug(false);
	Store store = null;
	Folder folder = null;

	int savedMessagesCount = 0;

	UserTransaction tx = null;
	NodeRef dossierRef = null;
	TransactionService transactionService = serviceRegistry.getTransactionService();

	try
	{
	    tx = transactionService.getUserTransaction();

	    // Connect to the e-mail store
	    store = session.getStore(url);
	    store.connect();

	    folder = store.getFolder(emailFolder);
	    if (folder == null)
		throw new MessagingException("Invalid folder");

	    // Open the folder in read-write mode as we will be modifying message properties for IMAP, and delete
	    // messages for POP3
	    folder.open(Folder.READ_WRITE);

	    Message[] msgs = folder.getMessages();

	    for (int i = 0; i < msgs.length; i++)
	    {
		// Try to understand if the message is new or not.
		// This only works for IMAP. If the protocol is POP3, the message is considered as new.
		boolean messageIsNew = true;
		Flags flags = msgs[i].getFlags();
		Flags.Flag[] sysFlags = flags.getSystemFlags();
		for (int j = 0; j < sysFlags.length; j++)
		{
		    if (sysFlags[j].equals(Flags.Flag.SEEN))
			messageIsNew = false;
		}
		if (messageIsNew)
		{
		    // Get message headers
		    EmailMessageHeaders headers = EmailUtils.extractHeaders(msgs[i]);

		    // Get body and attachments
		    Map<String, Part> attachments = new HashMap<String, Part>();
		    Map<String, String> body = new HashMap<String, String>();
		    body.put("body-text", "");
		    body.put("body-html", "");
		    EmailUtils.dumpPart(msgs[i], attachments, body, 0);

		    try
		    {
			tx.begin();
			String sujet = MimeUtility.decodeText(headers.getSubject()).replace("<", "(").replace(">",
				")").replace("@", " AT ").replace("\"", "'").replaceAll("[*\\\\?/:|\\xA3\\xAC%&+;]","_");
			String champFrom = MimeUtility.decodeText(headers.getFrom().replaceAll("^.*<", "").replaceAll(">$",""));
			logger.debug("checkMailBox: FROM:"+champFrom+", sujet["+sujet+"]");
			
			NodeRef emetteurNodeRef = findUserByEmail(champFrom);
			if (emetteurNodeRef == null) 
			{
			    // On sort !
			    logger.debug("emetteurNodeRef est NULL: l'émetteur email est inconnu dans le parapheur!");
			    throw new DocumentException("l'émetteur email est inconnu dans le parapheur!");
			}

			NodeRef parapheur = null;
			NodeRef circuitRef = null;
			Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
			String firstName = new String();
			String lastName = new String();
			String nomDocumentDossier = new String();
			String annotations = new String();

			// Traitement des propriétés contenues dans le fichier XML
			if (attachments.containsKey("XML_Dossier_Description.xml"))
			{
			    /*
			     * <ph:emailListener xmlns:ph="http://www.adullact.org/models/parapheur">
			     * <ph:email_emetteur>stephane.vast@adullact-projet.coop</ph:email_emetteur>
			     * <ph:nom_dossier>Demande de Matériel ou Logiciel</ph:nom_dossier>
			     * <ph:date_limite>2008-01-25</ph:date_limite> <ph:visibilite>PUBLIC</ph:visibilite>
			     * <ph:signature_papier>false</ph:signature_papier> <ph:annotations_publiques><![CDATA[
			     * Test d'annotation publique. ]]></ph:annotations_publiques>
			     * <ph:document>exemple5pages.pdf</ph:document> <!--OPTIONNEL <ph:PJ></ph:PJ> -->
			     * <ph:nom_circuit>formulaire demande materiel ou logiciel</ph:nom_circuit>
			     * </ph:emailListener>
			     */
			    // traiter le message
			    SAXReader saxreader = new SAXReader();
			    Document documentXML = saxreader.read(attachments.get("XML_Dossier_Description.xml").getInputStream());
			    // TODO Schema XML à implémenter
			    Element rootElement = documentXML.getRootElement();
			    //logger.debug("root=["+rootElement.getName()+"], nameSpace=["+ rootElement.getNamespace()+"], nameSpaceURI=["+ rootElement.getNamespaceURI()+"]");
			    if (rootElement.getName().equalsIgnoreCase("emailListener")
				    && rootElement.getNamespaceURI().equalsIgnoreCase("http://www.adullact.org/models/parapheur"))
			    {
				// emailEmetteur
				Element emailEmetteur = rootElement.element("email_emetteur");
				if (emailEmetteur != null)
				{
				    champFrom = MimeUtility.decodeText(emailEmetteur.getText().replaceAll("^.*<", "").replaceAll(">$",""));
				    emetteurNodeRef = findUserByEmail(champFrom);
				    if (emetteurNodeRef == null) 
				    {
				       logger.debug("emetteurNodeRef est NULL: l'émetteur email est inconnu dans le parapheur!");
				       throw new DocumentException("XML_Dossier_Description.xml: email_emetteur est inconnu dans i-Parapheur");
				    }
				    String userName = (String) nodeService.getProperty(emetteurNodeRef, ContentModel.PROP_USERNAME);
				    firstName = (String) nodeService.getProperty(emetteurNodeRef, ContentModel.PROP_FIRSTNAME);
				    lastName = (String) nodeService.getProperty(emetteurNodeRef, ContentModel.PROP_LASTNAME);
				    authenticationComponent.setCurrentUser(userName);
				    parapheur = parapheurService.getUniqueParapheurForUser(userName);

				} else
				    throw new DocumentException("XML_Dossier_Description.xml: email_emetteur absent");
				// nomDossier
				Element nomDossier = rootElement.element("nom_dossier");
				if (nomDossier != null && !nomDossier.getTextTrim().isEmpty())
				{
				    properties.put(ContentModel.PROP_NAME, nomDossier.getTextTrim());
				} else
				    throw new DocumentException("XML_Dossier_Description.xml: nom_dossier absent");

				// dateLimite
				Element dateLimite = rootElement.element("date_limite");
				if (dateLimite != null && !dateLimite.getTextTrim().isEmpty())
				{
				    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				    properties.put(ParapheurModel.PROP_DATE_LIMITE, dateFormat.parse(dateLimite.getText()));
				}
				// Visibilite
				Element visibilite = rootElement.element("visibilite");
				if (visibilite != null && !visibilite.getTextTrim().isEmpty())
				{
				    if (visibilite.getText().equalsIgnoreCase("Confidentiel"))
				    {
					properties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.TRUE);
				    } else if (visibilite.getText().equalsIgnoreCase("Public"))
				    {
					properties.put(ParapheurModel.PROP_PUBLIC, Boolean.TRUE);
				    }
				}
				// Signature papier
				Element signaturePapier = rootElement.element("signature_papier");
				if (signaturePapier != null && !signaturePapier.getTextTrim().isEmpty())
				{
				    properties.put(ParapheurModel.PROP_SIGNATURE_PAPIER, Boolean.parseBoolean(signaturePapier.getText()));
				}
				// Annotations publiques
				Element annotations_publiques = rootElement.element("annotations_publiques");
				if (annotations_publiques != null)
				{
				    annotations = annotations_publiques.getText();
				}
				// Récup document principal
				Element document = rootElement.element("document");
				if (document != null && !document.getTextTrim().isEmpty())
				{
				    nomDocumentDossier = document.getText();
				} else
				    throw new DocumentException("XML_Dossier_Description.xml: document non specifie");
				
				// Récup nomCircuit
				Element nomCircuit = rootElement.element("nom_circuit");
				if (nomCircuit != null && !nomCircuit.getTextTrim().isEmpty())
				{
				    circuitRef = findCircuitRefFromName(nomCircuit.getTextTrim());
				    if (circuitRef == null)
					throw new DocumentException("XML_Dossier_Description.xml: nom_circuit INCONNU dans iParapheur");
				} else
				    throw new DocumentException("XML_Dossier_Description.xml: nom_circuit absent");

				// -------------------------------------
				// On a un document, des propriétés, un circuit: on peut déjà créer le dossier
				dossierRef = dossierService.createDossier(parapheur, properties);
				// Document
				FileFolderService ffs = serviceRegistry.getFileFolderService();
				FileInfo fileInfo = ffs.create(dossierRef, nomDocumentDossier, ContentModel.TYPE_CONTENT);
				NodeRef docNodeRef = fileInfo.getNodeRef();
				ContentService cs = serviceRegistry.getContentService();
				ContentWriter cw = cs.getWriter(docNodeRef, ContentModel.PROP_CONTENT, Boolean.TRUE);
				Part documentDossier = attachments.get(nomDocumentDossier);
				if (nomDocumentDossier.endsWith(".pdf") || nomDocumentDossier.endsWith(".PDF"))
				    cw.setMimetype(MimetypeMap.MIMETYPE_PDF);
				else
				    cw.setMimetype(documentDossier.getContentType());
				logger.debug("Document "+ nomDocumentDossier +" de type " + documentDossier.getContentType() + "=>" + cw.getMimetype());
				cw.setEncoding("UTF-8");
				cw.putContent(documentDossier.getInputStream());

				//  traiter PJ:  <!--OPTIONNEL <ph:PJs></ph:PJs> -->
				Element PJs = rootElement.element("PJs");
				if (PJs != null)
				{
				    Iterator<Element> eltIterator = (Iterator<Element>) PJs.elementIterator();
				    for (Iterator<Element> ie = eltIterator; ie.hasNext(); )
				    {
					Element pjDocElt = ie.next();
					if (pjDocElt != null && !pjDocElt.getTextTrim().isEmpty())
					{
					    // Add Pièce jointe
					    String nomPJ = pjDocElt.getTextTrim();
					    FileInfo fileInfo2 = ffs.create(dossierRef, nomPJ, ContentModel.TYPE_CONTENT);
					    NodeRef docPJNodeRef = fileInfo2.getNodeRef();
					    ContentWriter cw2 = cs.getWriter(docPJNodeRef, ContentModel.PROP_CONTENT, Boolean.TRUE);
					    Part pjDoc = attachments.get(pjDocElt.getText());
					    if (nomPJ.endsWith(".pdf") || nomPJ.endsWith(".PDF"))
						cw2.setMimetype(MimetypeMap.MIMETYPE_PDF);
					    else
						cw2.setMimetype(pjDoc.getContentType());
					    logger.debug("DocumentPJ "+ nomPJ +" de type " + pjDoc.getContentType());
					    cw2.setEncoding("UTF-8");
					    cw2.putContent(pjDoc.getInputStream());
					}
				    }
				}
				
			    } else
				throw new DocumentException("Doc XML XML_Dossier_Description.xml invalide");

			} else
			    throw new RuntimeException("Message invalide: XML_Dossier_Description.xml absent!");

			// ajout Annotation publique, et le circuit.
			// Circuit
			SavedWorkflow circuit = parapheurService.loadSavedWorkflow(circuitRef);
			parapheurService.setCircuit(dossierRef, circuit.getCircuit());
			// Annotations 
			parapheurService.setAnnotationPublique(dossierRef, annotations);

			// Emission automatique du dossier=> visa de l'émetteur, et approbation
			parapheurService.setSignataire(dossierRef, firstName + ((lastName!=null && lastName.length() > 0) ? " " + lastName : ""));
			parapheurService.approve(dossierRef);

			tx.commit();
		    } catch (Exception e)
		    {
			try
			{
			    tx.rollback();
			} catch (RuntimeException e1)
			{
			    logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		    } finally
		    {
			// For POP3, delete the message from the message store
			// For IMAP, mark the message as SEEN, so that it stays in the message store, but we will not
			// process it again
			if (protocol.equals("pop3"))
			    msgs[i].setFlag(Flags.Flag.DELETED, true);
			else if (protocol.equals("imap"))
			    msgs[i].setFlag(Flags.Flag.SEEN, true);

			savedMessagesCount++;
		    }
		}
	    }

	} catch (Exception e)
	{
	    logger.error(e.getMessage());
	} finally
	{
	    logger.debug("  " + protocol + "://" + emailUserName + "@" + mailServer + ":" + mailPort + ": "
		    + savedMessagesCount + " message(s) traité(s).");
	    try
	    {
		folder.close(true);
		store.close();
	    } catch (Exception e)
	    {
		logger.error(e.getMessage());
	    }
	}

    }
    
    /*  1ere version avec AtolCD     */
//    private void checkMailBox(NodeRef circuitRef)
//    {
//	String protocol = (String) nodeService.getProperty(circuitRef, ParapheurModel.PROP_EMAILPROTOCOL);
//	String mailServer = (String) nodeService.getProperty(circuitRef, ParapheurModel.PROP_EMAILSERVER);
//	Integer mailPort = (Integer) nodeService.getProperty(circuitRef, ParapheurModel.PROP_EMAILPORT);
//	String emailFolder = (String) nodeService.getProperty(circuitRef, ParapheurModel.PROP_EMAILFOLDER);
//	String emailUserName = (String) nodeService.getProperty(circuitRef, ParapheurModel.PROP_EMAILUSERNAME);
//	String emailPassword = (String) nodeService.getProperty(circuitRef, ParapheurModel.PROP_EMAILPASSWORD);
//	logger.debug("checkMailBox with "+emailUserName+"@"+mailServer+":"+mailPort+"("+protocol+")/"+emailFolder);
//
//	// 1 chercher les emails
//	if (emailFolder == null || emailFolder.equals("") || protocol.equals("pop3"))
//	{
//	    emailFolder = "INBOX";
//	}
//	logger.debug("checkMailBox with "+emailUserName+"@"+mailServer+":"+mailPort+"("+protocol+")/"+emailFolder);
//	URLName url = new URLName(protocol, mailServer, mailPort, emailFolder, emailUserName, emailPassword);
//	Properties props = System.getProperties();
//	Session session = Session.getDefaultInstance(props, null);
//	session.setDebug(false);
//	Store store = null;
//	Folder folder = null;
//
//	int savedMessagesCount = 0;
//
//	UserTransaction tx = null;
//	TransactionService transactionService = serviceRegistry.getTransactionService();
//
//	try
//	{
//	    tx = transactionService.getUserTransaction();
//
//	    // Connect to the e-mail store
//	    store = session.getStore(url);
//	    store.connect();
//
//	    folder = store.getFolder(emailFolder);
//	    if (folder == null)
//		throw new MessagingException("Invalid folder");
//
//	    // Open the folder in read-write mode as we will be modifying message properties for IMAP, and delete
//	    // messages for POP3
//	    folder.open(Folder.READ_WRITE);
//
//	    Message[] msgs = folder.getMessages();
//
//	    for (int i = 0; i < msgs.length; i++)
//	    {
//		// Try to understand if the message is new or not.
//		// This only works for IMAP. If the protocol is POP3, the message is considered as new.
//		boolean messageIsNew = true;
//		Flags flags = msgs[i].getFlags();
//		Flags.Flag[] sysFlags = flags.getSystemFlags();
//		for (int j = 0; j < sysFlags.length; j++)
//		{
//		    if (sysFlags[j].equals(Flags.Flag.SEEN))
//			messageIsNew = false;
//		}
//		if (messageIsNew)
//		{
//		    // Get message headers
//		    EmailMessageHeaders headers = EmailUtils.extractHeaders(msgs[i]);
//
//		    // Get body and attachments
//		    Map<String, Part> attachments = new HashMap<String, Part>();
//		    Map<String, String> body = new HashMap<String, String>();
//		    body.put("body-text", "");
//		    body.put("body-html", "");
//		    EmailUtils.dumpPart(msgs[i], attachments, body, 0);
//
//		    try
//		    {
//			tx.begin();
//			String nomDossier = MimeUtility.decodeText(headers.getSubject()).replace("<", "(").replace(">",
//				")").replace("@", " AT ").replace("\"", "'").replaceAll("[*\\\\?/:|\\xA3\\xAC%&+;]",
//				"_");
//
//			logger.debug("checkMailBox: Subject"+headers.getSubject()+"->nomDossier["+nomDossier+"]");
//			//logger.debug("FROM:"+headers.getFrom());
//
//			String champFrom = MimeUtility.decodeText(headers.getFrom().replaceAll("^.*<", "").replaceAll(">$",""));
//			logger.debug("FROM:"+champFrom);
//			NodeRef emetteurNodeRef = findUserByEmail(champFrom);
//			if (emetteurNodeRef == null) 
//			{
//			    logger.debug("emtteurNodeRef est NULL!");
//			}
//			String userName = (String) nodeService.getProperty(emetteurNodeRef, ContentModel.PROP_USERNAME);
//			String firstName = (String) nodeService.getProperty(emetteurNodeRef, ContentModel.PROP_FIRSTNAME);
//			String lastName = (String) nodeService.getProperty(emetteurNodeRef, ContentModel.PROP_LASTNAME);
//
//			authenticationComponent.setCurrentUser(userName);
//			NodeRef parapheur = parapheurService.getOwnedParapheur(userName);
//			Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
//			String nomDocumentDossier = new String();
//			String annotations = new String();
//
//			properties.put(ContentModel.PROP_NAME, nomDossier);
//			// Autres propriétés
//			if (attachments.containsKey("XML_Dossier_Description.xml"))
//			{
//			    /*
//			     * <ph:emailListener xmlns:ph="http://www.adullact.org/models/parapheur">
//			     * <ph:date_limite>2008-01-25</ph:date_limite> <ph:visibilite>PUBLIC</ph:visibilite>
//			     * <ph:signature_papier>false</ph:signature_papier> <ph:annotations_publiques><![CDATA[
//			     * Test d'annotation publique. ]]></ph:annotations_publiques>
//			     * <ph:document>exemple5pages.pdf</ph:document> <!--OPTIONNEL <ph:PJs></ph:PJs> -->
//			     * </ph:emailListener>
//			     */
//			    // traiter le message
//			    SAXReader saxreader = new SAXReader();
//			    Document documentXML = saxreader.read(attachments.get("XML_Dossier_Description.xml")
//				    .getInputStream());
//			    // Il reste à Schéma XML à implémenter
//			    Element rootElement = documentXML.getRootElement();
//			    //logger.debug("root=["+rootElement.getName()+"], nameSpace=["+ rootElement.getNamespace()+"], nameSpaceURI=["+ rootElement.getNamespaceURI()+"]");
//			    if (rootElement.getName().equalsIgnoreCase("emailListener")
//				    && rootElement.getNamespaceURI().equalsIgnoreCase("http://www.adullact.org/models/parapheur"))
//			    {
//				Element dateLimite = rootElement.element("date_limite");
//				if (dateLimite != null)
//				{
//				    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//				    properties.put(ParapheurModel.PROP_DATE_LIMITE, dateFormat.parse(dateLimite.getText()));
//				}
//				Element visibilite = rootElement.element("visibilite");
//				if (visibilite != null)
//				{
//				    if (visibilite.getText().equalsIgnoreCase("Confidentiel"))
//				    {
//					properties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.TRUE);
//				    } else if (visibilite.getText().equalsIgnoreCase("Public"))
//				    {
//					properties.put(ParapheurModel.PROP_PUBLIC, Boolean.TRUE);
//				    }
//				}
//				Element signaturePapier = rootElement.element("signature_papier");
//				if (signaturePapier != null)
//				{
//				    properties.put(ParapheurModel.PROP_SIGNATURE_PAPIER, Boolean.parseBoolean(signaturePapier.getText()));
//				}
//				Element annotations_publiques = rootElement.element("annotations_publiques");
//				if (annotations_publiques != null)
//				{
//				    annotations = annotations_publiques.getText();
//				}
//				Element document = rootElement.element("document");
//				if (document != null)
//				{
//				    nomDocumentDossier = document.getText();
//				} else
//				    throw new DocumentException("XML_Dossier_Description.xml: document absent");
//				
//				// Il reste à traiter PJ				
//
//			    } else
//				throw new DocumentException("Doc XML XML_Dossier_Description.xml invalide");
//
//			} else
//			    throw new RuntimeException("Message invalide: XML_Dossier_Description.xml absent!");
//
//			NodeRef dossierRef = parapheurService.createDossier(parapheur, properties);
//			// ajout Annotation, Fichiers, et le circuit donné en paramètre.
//			// Document
//			FileFolderService ffs = serviceRegistry.getFileFolderService();
//			FileInfo fileInfo = ffs.create(dossierRef, nomDocumentDossier, ContentModel.TYPE_CONTENT);
//			NodeRef docNodeRef = fileInfo.getNodeRef();
//			ContentService cs = serviceRegistry.getContentService();
//			ContentWriter cw = cs.getWriter(docNodeRef, ContentModel.PROP_CONTENT, Boolean.TRUE);
//			Part documentDossier = attachments.get(nomDocumentDossier);
//			cw.setMimetype(documentDossier.getContentType());
//			logger.debug("Document "+ nomDocumentDossier +" de type " + documentDossier.getContentType());
//			cw.setEncoding("UTF-8");
//			cw.putContent(documentDossier.getInputStream());
//			// Circuit
//			SavedWorkflow circuit = parapheurService.loadSavedWorkflow(circuitRef);
//			parapheurService.setCircuit(dossierRef, circuit.getCircuit());
//			parapheurService.setDiffusion(dossierRef, circuit.getDiffusion());
//			// Annotation
//			parapheurService.setAnnotationPublique(dossierRef, annotations);
//
//			// Emission automatique du dossier=> visa de l'émetteur, et approbation
//			parapheurService.setSignataire(dossierRef, firstName + ((lastName!=null && lastName.length() > 0) ? " " + lastName : ""));
//			parapheurService.approve(dossierRef);
//
//			tx.commit();
//		    } catch (Exception e)
//		    {
//			try
//			{
//			    tx.rollback();
//			} catch (RuntimeException e1)
//			{
//			    logger.error(e.getMessage(), e);
//			}
//			logger.error(e.getMessage(), e);
//		    } finally
//		    {
//			// For POP3, delete the message from the message store
//			// For IMAP, mark the message as SEEN, so that it stays in the message store, but we will not
//			// process it again
//			if (protocol.equals("pop3"))
//			    msgs[i].setFlag(Flags.Flag.DELETED, true);
//			else if (protocol.equals("imap"))
//			    msgs[i].setFlag(Flags.Flag.SEEN, true);
//
//			savedMessagesCount++;
//		    }
//		}
//	    }
//
//	} catch (Exception e)
//	{
//	    logger.error(e.getMessage());
//	} finally
//	{
//	    logger.debug("Boite " + protocol + "://" + emailUserName + "@" + mailServer + ":" + mailPort + ": "
//		    + savedMessagesCount + "message(s) traité(s).");
//	    try
//	    {
//		folder.close(true);
//		store.close();
//	    } catch (Exception e)
//	    {
//		logger.error(e.getMessage());
//	    }
//	}
//
//    }

    /**
     * Finds necessary services and the company home node
     * 
     * @param context
     *                The JobExecutionContext, used for getting the necessary beans from the config file
     *                (scheduled-jobs-context.xml)
     */
    private void initialize(JobExecutionContext context) {
        if (!initialized) {
            JobDataMap jobData = context.getJobDetail().getJobDataMap();

            Object serviceRegistryObj = jobData.get("serviceRegistry");
            serviceRegistry = (ServiceRegistry) serviceRegistryObj;
            Object authenticationComponentObj = jobData.get("authenticationComponent");
            authenticationComponent = (AuthenticationComponent) authenticationComponentObj;

            searchService = serviceRegistry.getSearchService();
            nodeService = serviceRegistry.getNodeService();
            // namespaceService = serviceRegistry.getNamespaceService();
            parapheurService = (ParapheurService) jobData.get("parapheurService");
            authenticationComponent.setSystemUserAsCurrentUser();
            circuitsHome = getCircuitsHome();

            TransactionService transactionService = serviceRegistry.getTransactionService();
            UserTransaction trx = transactionService.getUserTransaction();
            try {
                trx.begin();

                // Recherche du fichier email_listener-configuration.xml
                String xpath = "/app:company_home/app:dictionary/ph:email_listener"; // -configuration";
                ResultSet result = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_XPATH, xpath);

                if (result.length() > 0) {
                    NodeRef nodeEmailListener = result.getNodeRef(0);
                    List<ChildAssociationRef> childs = nodeService.getChildAssocs(nodeEmailListener);
                    if (childs.size() > 0) {
                        NodeRef nodeConfig = childs.get(0).getChildRef();
                        FileFolderService fileFolderService = serviceRegistry.getFileFolderService();
                        SAXReader reader = new SAXReader();
                        Document document = reader.read(fileFolderService.getReader(nodeConfig).getContentInputStream());
                        mailServer = document.selectSingleNode("/email-listener/server").getText();
                        protocol = document.selectSingleNode("/email-listener/protocole").getText();
                        mailPort = Integer.parseInt(document.selectSingleNode("/email-listener/port").getText());
                        emailFolder = document.selectSingleNode("/email-listener/folder").getText();
                        emailUserName = document.selectSingleNode("/email-listener/username").getText();
                        emailPassword = document.selectSingleNode("/email-listener/password").getText();
                    }
                } else {
                    logger.error("EmailListenerQuartJob::init =========== Configuration non trouvee!!!!!!!!==============");
                }
                trx.commit();
                initialized = true;
            } catch (Exception e) {
                //TODO
            }
        }
    }

    /**
     * Searches for nodes in the repository which have the ph:emailListener Aspect applied. The found nodes are put in
     * the emailListenerTasks map, along with the mailbox connection properties.
     */
//    private void findValidEmailListenerAspects()
//    {
//	ResultSet results = null;
//	try
//	{
//	    /*
//	     * String queryStr = "ASPECT:\"" + EmailListenerModel.ASPECT_LISTENTOEMAIL.toString() +
//	     * "\"+@emailListener\\:listenToEmailAspectEnabled:true";
//	     */
//	    //String queryStr = "ASPECT:\"" + ParapheurModel.ASPECT_EMAIL_LISTENER.toString() + "\" +@"
//		//    + ParapheurModel.PROP_EMAIL_ENABLED.toPrefixString(namespaceService) + ":true";
//	    //logger.debug(queryStr);
//	    //String queryStr2 = "ASPECT:\"" + ParapheurModel.ASPECT_EMAIL_LISTENER.toPrefixString(namespaceService)
//		//    + "\" +@" + ParapheurModel.PROP_EMAIL_ENABLED.toPrefixString(namespaceService) + ":true";
//	    //logger.debug(queryStr2);
//	    String queryStr3 = "ASPECT:\"" + ParapheurModel.ASPECT_EMAIL_LISTENER.toPrefixString(namespaceService)
//		    + "\" +@ph\\:listenToEmailAspectEnabled:true";
//	    logger.debug(queryStr3);
//	    results = searchService.query(circuitsHome.getStoreRef(), SearchService.LANGUAGE_LUCENE, queryStr3);
//	    this.nodeRefs = results.getNodeRefs();
//
//	} catch (Exception e)
//	{
//	    logger.error(e.getClass() + "---" + e.getMessage());
//	} finally
//	{
//	    if (results != null)
//	    {
//		results.close();
//	    }
//	}
//
//    }

    private NodeRef findCircuitRefFromName(String circuitName)
    {
	ResultSet results = null;
	try
	{
	    String query = "@cm\\:name:\"" + circuitName + "\"";
	    logger.debug(query);
	    results = searchService.query(circuitsHome.getStoreRef(), SearchService.LANGUAGE_LUCENE, query);
	    if (results.length() != 1)
	    {
		throw new AlfrescoRuntimeException("Circuit inconnu !");
	    }
	    else
		return results.getNodeRef(0);
	}
	catch (Exception e)
	{
	    logger.error(e.getClass() + "---" + e.getMessage());
	} finally
	{
	    if (results != null)
	    {
		results.close();
	    }
	}
	return null;
    }
    
    /**
     * @return The "circuits home" NodeRef
     */
    private NodeRef getCircuitsHome()
    {
	StoreRef spacesStoreRef = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");

	ResultSet results = null;
	try
	{
	    String queryStr = "PATH:\"/app:company_home/app:dictionary/ph:savedworkflows\"";
	    results = searchService.query(spacesStoreRef, SearchService.LANGUAGE_LUCENE, queryStr);
	    if (results.length() != 1)
		throw new AlfrescoRuntimeException("Noeud des Circuits inconnu !");
	    return results.getNodeRef(0);

	} catch (Exception e)
	{
	    logger.error(e.getClass() + "---" + e.getMessage());
	} finally
	{
	    if (results != null)
	    {
		results.close();
	    }
	}

	return null;
    }

    private NodeRef findUserByEmail(String from) throws NoSuchPersonException
    {
	StoreRef spacesStoreRef = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");

	ResultSet results = null;
	String queryStr = "PATH:\"/sys:system/sys:people/*\" +@cm\\:email:" + from;
	results = searchService.query(spacesStoreRef, SearchService.LANGUAGE_LUCENE, queryStr);
	if (results.length() < 1)
	{
	    throw new NoSuchPersonException("Email inconnu dans le Parapheur");
	}
	// XXX Quid s'il existe plusieurs utilisateurs avec cet email ?? Ici on prend le 1er
	return results.getNodeRef(0);
    }

}