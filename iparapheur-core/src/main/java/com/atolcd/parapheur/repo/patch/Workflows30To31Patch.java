/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.SavedWorkflow;
import com.atolcd.parapheur.repo.WorkflowService;
import java.util.List;
import javax.transaction.UserTransaction;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.module.AbstractModuleComponent;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;

/**
 *
 * @author Vivien Barousse
 */
public class Workflows30To31Patch extends XPathBasedPatch {

    public static final String ALL_WORKFLOWS_QUERY = "/app:company_home"
            + "/app:dictionary"
            + "/ph:savedworkflows"
            + "/*";

    private ParapheurService parapheurService;

    private WorkflowService workflowService;

    private PermissionService permissionService;

    private AuthorityService authorityService;

    protected void patch(NodeRef workflow) {
        NodeService nodeService = getNodeService();
        
        @SuppressWarnings("deprecated")
        SavedWorkflow savedWorkflow = parapheurService.loadSavedWorkflow(workflow);

        // New circuits are empty when loaded with old code
        if (!savedWorkflow.getCircuit().isEmpty()) {
            String user = (String) nodeService.getProperty(workflow, ContentModel.PROP_CREATOR);

            workflowService.saveWorkflow(savedWorkflow.getName(),
                    savedWorkflow.getCircuit(),
                    savedWorkflow.getAclParapheurs(),
                    savedWorkflow.getAclGroupes(),
                    savedWorkflow.isPublic());

            String userAuthority = authorityService.getName(AuthorityType.USER, user);
            permissionService.setPermission(workflow, userAuthority, PermissionService.EDITOR, true);
        }
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @Override
    protected String getXPathQuery() {
        return ALL_WORKFLOWS_QUERY;
    }

}
