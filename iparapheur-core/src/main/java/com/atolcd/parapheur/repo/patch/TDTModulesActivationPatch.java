package com.atolcd.parapheur.repo.patch;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.module.AbstractModuleComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.transaction.TransactionService;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.DocumentFactory;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import javax.transaction.UserTransaction;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TDTModulesActivationPatch
 *
 * @author eperalta
 *         Date: 31/01/12
 *         Time: 20:34
 */
public class TDTModulesActivationPatch extends AbstractModuleComponent {

    private NodeService nodeService;
    private TransactionService transactionService;
    private SearchService searchService;
    private FileFolderService fileFolderService;
    private ContentService contentService;

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    protected Map<String, String> getPropertiesFromConfigFile(String rootElement) {
        assert (rootElement != null && !rootElement.equals(""));
        Map<String, String> properties = new HashMap<String, String>();

        try {
            SAXReader reader = new SAXReader();
            Document document = reader.read(readFile("s2low_configuration.xml"));
            List<Node> nodes = document.getRootElement().element(rootElement).elements();
            for (Node node : nodes) {
                properties.put(node.getName(), node.getText());
            }
            return properties;
        } catch (Exception ex) {
            throw new AlfrescoRuntimeException(ex.getMessage(), ex);
        }
    }

    private InputStream readFile(String nameFile) throws Exception {
        UserTransaction trx = transactionService.getUserTransaction();
        InputStream inputStream = null;
        try {
            trx.begin();

            // Recherche du noeud Certificats dans Dictionary
            String xpath = "/app:company_home/app:dictionary/ph:certificats";
            ResultSet result = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_XPATH, xpath);

            if (result.length() > 0) {
                NodeRef nodeCertificat = result.getNodeRef(0);
                List<ChildAssociationRef> childs = this.nodeService.getChildAssocs(nodeCertificat);
                NodeRef file = null;

                // Récupération des fichiers concernant le certificat
                for (ChildAssociationRef child : childs) {
                    String name = this.nodeService.getProperty(child.getChildRef(), ContentModel.PROP_NAME).toString();
                    if (name.equals(nameFile)) {
                        // Sélection du fichier demandé
                        file = child.getChildRef();
                    }
                }

                if (file != null) {
                    ContentReader content = fileFolderService.getReader(file);
                    inputStream = content.getContentInputStream();
                }
            }
            trx.commit();
            return inputStream;
        } catch (Exception e) {
            try {
                trx.rollback();
            } catch (Exception ex) {
                System.out.println("Error during rollback");
                ex.printStackTrace();
            }
            throw e;
        }
    }

    private Map<String, String> addActivatedProperty(String name) {
        Map<String, String> properties = getPropertiesFromConfigFile(name);
        String password = properties.get("password");

        Boolean isActivated = !"_off_".equals(password);
        properties.put("active", isActivated.toString());
        return properties;
    }

    private void addSectionToDoc(Document document, String section, Map<String, String> properties, Element root) {
        Element sectionElt = root.addElement(section);

        for (Map.Entry<String, String> e : properties.entrySet()) {
            sectionElt.addElement(e.getKey()).addText(e.getValue());
        }
    }

    @Override
    protected void executeInternal() throws Throwable {
        AuthenticationUtil.RunAsWork<Void> work = new AuthenticationUtil.RunAsWork<Void>() {
            @Override
            public Void doWork() throws Exception {
                UserTransaction tx = transactionService.getNonPropagatingUserTransaction();
                try {

                    tx.begin();

                    Map<String, String> actesProperties = addActivatedProperty("actes");
                    Map<String, String> heliosProperties = addActivatedProperty("helios");
                    Map<String, String> mailsecProperties = addActivatedProperty("mailsec");

                    Document document = DocumentFactory.getInstance().createDocument("UTF-8");
                    Element s2lowElt = document.addElement("s2low");
                    s2lowElt.addElement("configs2low").addText("ph:s2low");

                    addSectionToDoc(document, "actes", actesProperties, s2lowElt);
                    addSectionToDoc(document, "helios", heliosProperties, s2lowElt);
                    addSectionToDoc(document, "mailsec", mailsecProperties, s2lowElt);

                    ResultSet results = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE,
                            SearchService.LANGUAGE_XPATH,
                            "/app:company_home/app:dictionary/ph:certificats/cm:s2low_properties.xml");

                    if (results.length() == 1) {
                        NodeRef s2lowConfig = results.getNodeRef(0);
                        ContentWriter writer = contentService.getWriter(s2lowConfig, ContentModel.PROP_CONTENT, true);
                        writer.setEncoding("UTF-8");
                        writer.putContent(new ByteArrayInputStream(document.asXML().getBytes()));
                    }
                    tx.commit();
                } catch (Exception e) {
                    tx.rollback();
                    e.printStackTrace();
                }

                return null;
            }
        };


        AuthenticationUtil.runAs(work, AuthenticationUtil.getAdminUserName());
    }

}
