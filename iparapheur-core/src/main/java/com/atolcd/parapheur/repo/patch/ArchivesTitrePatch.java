/*
 * Version 2.1
 * CeCILL Copyright (c) 2008-2012, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 * 
 */
package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import java.io.Serializable;
import java.util.List;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.admin.patch.AbstractPatch;
import org.alfresco.repo.module.AbstractModuleComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.admin.PatchException;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.transaction.UserTransaction;

/**
 *
 * @author Jason Maire - ADULLACT Projet
 */
public class ArchivesTitrePatch extends AbstractModuleComponent {

    private static Log logger = LogFactory.getLog(ArchivesTitrePatch.class);
    private String store;
    private TransactionService transactionService;
    private NodeService nodeService;
    private SearchService searchService;
    private NamespaceService namespaceService;

    public NamespaceService getNamespaceService() {
        return namespaceService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public SearchService getSearchService() {
        return searchService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public TransactionService getTransactionService() {
        return transactionService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public NodeService getNodeService() {
        return nodeService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setStore(String store) {
        this.store = store;
    }

    @Override
    protected void checkProperties() {
        try {
            super.checkProperties();
        } catch (Exception e) {
            logger.error("Erreur de pptes dans le patch ArchivesTitrePatch", e);
        }
    }

    @Override
    protected void executeInternal() throws Throwable {
        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<String>()

        {

            public String doWork() throws Exception

            {

                UserTransaction tx = transactionService.getNonPropagatingUserTransaction();
                try {

                    tx.begin();

                    // Récupération du répertoire des archives
                    String archivesFolderPath = "/app:company_home/ph:archives";
                    NodeRef rootNodeRef = nodeService.getRootNode(new StoreRef(store));
                    List<NodeRef> results = searchService.selectNodes(rootNodeRef, archivesFolderPath, null, namespaceService, false);

                    if (results == null || results.size() != 1) {
                        // Apparemment, pour les nouveaux parapheurs, le répertoire d'archives n'existe pas..
                        //throw new PatchException(msg);
                        logger.warn("Impossible de trouver le répertoire d'archives");
                    } else {
                        System.out.println("ArchivesTitrePatch: répertoire d'archives trouvé.");
                        NodeRef archivesRef = results.get(0);

                        List<ChildAssociationRef> children = nodeService.getChildAssocs(archivesRef);
                        System.out.println("ArchivesTitrePatch: il y a " + children.size() + " archive nodes.");
                        int nbTraites = 0;
                        for (ChildAssociationRef child : children) {

                            NodeRef archive = child.getChildRef();
                            if (nodeService.exists(archive)
                                    && ParapheurModel.TYPE_ARCHIVE.equals(nodeService.getType(archive))
                                    && (nodeService.getProperty(archive, ContentModel.PROP_TITLE) == null)) {

                                Serializable prop = nodeService.getProperty(archive, ContentModel.PROP_NAME);
                                //nodeService.setProperty(archive, ContentModel.PROP_TITLE, prop);
                                java.util.HashMap<QName, Serializable> props = new java.util.HashMap<QName, Serializable>();
                                props.put(ContentModel.PROP_TITLE, prop);
                                props.put(ContentModel.PROP_DESCRIPTION, "Pre-archive i-Parapheur");
                                nodeService.addAspect(archive, ContentModel.ASPECT_TITLED, props);
                                nbTraites ++;
                            }
                        }
                        System.out.println("ArchivesTitrePatch : nombre d'archives traitées : " + nbTraites);
                    }

                    tx.commit();
                } catch (Exception e) {
                    tx.rollback();
                    System.out.println(e);
                }
                return null;
            }

        }, AuthenticationUtil.getAdminUserName());
    }
}