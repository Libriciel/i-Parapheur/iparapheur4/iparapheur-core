/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Initial Developpment by AtolCD, maintained by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Service de base pour la gestion des méta-données.
 *
 * @author Vivien Barousse - ADULLACT Projet
 */
public interface MetadataService {

    List<CustomMetadataDef> getMetadataDefs();

    void deleteMetadataDef(QName metadata);

    void addMetadataDef(CustomMetadataDef def);
    
    void addMetadatasDefs(List<CustomMetadataDef> defsToAdd);

    void saveMetadataDefs(List<CustomMetadataDef> defs);

    Object getValueFromString(CustomMetadataType customMetadataType, String type);

    void validateValue(CustomMetadataType type, String value);

    List<String> getUsedValuesForDef(CustomMetadataDef def);
    
    List<String> getSubtypesReferencingMetadata(QName propName);
    
    List<CustomMetadataDef> getMetadatas(String type, String sstype);
    
    Map<QName, Map<String, String>> getMetadatasMap(String type, String sstype);
    
    Map<String, Serializable> getMetadataValues(Map<QName, Map<String, Serializable>> map);

    Map<QName, Map<String, Serializable>> getMetaDonneesDossierWithTypo(NodeRef dossier, String type, String sstype);

    Map<QName, Map<String, Serializable>> getMetaDonneesDossier(NodeRef dossier);

    boolean areMetadataValid(@Nullable Map<QName, Serializable> metadataValues) throws IllegalArgumentException;

    /**
     * Reload metadata's definitions. 
     * 
     * If the model doesn't exist, it is created with the default model.
     * If the model already exists, we test if the default definitions are 
     * defined and add them if not.
     */
    void reloadMetadataDefs();
}
