package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CustomMetadataDef;
import com.atolcd.parapheur.repo.CustomMetadataType;
import org.junit.Assert;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.module.AbstractModuleComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import javax.transaction.UserTransaction;
import java.io.InputStream;
import java.util.List;

public class SecureMailConfigPatch extends AbstractModuleComponent {
    private ContentService contentService;
    private NodeService nodeService;
    private TransactionService transactionService;
    private NamespaceService namespaceService;
    private SearchService searchService;

    private static Log logger = LogFactory.getLog(ArchilandConfigPatch.class);


    public ContentService getContentService() {
        return contentService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public NodeService getNodeService() {
        return nodeService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public TransactionService getTransactionService() {
        return transactionService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public NamespaceService getNamespaceService() {
        return namespaceService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public SearchService getSearchService() {
        return searchService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    protected Document getConfigDocument() {
        NodeRef configNodeRef = getConfigNode();

        ContentReader reader = contentService.getReader(configNodeRef, ContentModel.PROP_CONTENT);
        SAXReader saxReader = new SAXReader();
        try {
            Document doc = saxReader.read(reader.getContentInputStream());

            return doc;
        } catch (DocumentException ex) {
            // log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    protected void setConfigDocument(Document doc) {
        NodeRef configNode = getConfigNode();

        ContentWriter writer = contentService.getWriter(configNode, ContentModel.PROP_CONTENT, true);

        writer.putContent(doc.asXML());
    }

    protected NodeRef getConfigNode() {
        List<NodeRef> nodes = getSearchService().selectNodes(getNodeService().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                "/app:company_home/app:dictionary/ph:certificats/cm:s2low_properties.xml",
                null,
                getNamespaceService(),
                false);

        if (nodes.size() != 1) {
            throw new RuntimeException("Can't find S2low configuration file");
        }

        NodeRef configNodeRef = nodes.get(0);

        return configNodeRef;
    }


    protected Document getConfigPatchDocument() {
        InputStream viewStream = getClass().getClassLoader().getResourceAsStream("alfresco/module/parapheur/s2low/mailsec-snippet.xml");
        SAXReader saxReader = new SAXReader();
        try {
            Document doc = saxReader.read(viewStream);
            return doc;
        } catch (DocumentException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }


    @Override
    protected void executeInternal() throws Throwable {
        AuthenticationUtil.RunAsWork<Void> work = new AuthenticationUtil.RunAsWork<Void>() {
            @Override
            public Void doWork() throws Exception {
                UserTransaction tx = getTransactionService().getNonPropagatingUserTransaction();
                try {

                    tx.begin();
                    Document doc = getConfigDocument();

                    List<Node> configNodes = doc.selectNodes("/s2low/mailsec");


                    if (configNodes.isEmpty()) {
                        Document snippet = getConfigPatchDocument();
                        doc.getRootElement().add(snippet.getRootElement());
                        setConfigDocument(doc);
                    }

                    tx.commit();
                } catch (Exception e) {
                    tx.rollback();
                }

                return null;
            }
        };


        AuthenticationUtil.runAs(work, AuthenticationUtil.getAdminUserName());
    }
}