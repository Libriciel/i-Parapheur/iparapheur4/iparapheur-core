/*
 * Version 3.3
 * CeCILL Copyright (c) 2006-2013, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo.admin.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.NotificationCenter;
import com.atolcd.parapheur.repo.ParapheurUserPreferences;
import com.atolcd.parapheur.repo.WorkflowService;
import com.atolcd.parapheur.repo.admin.UsersService;
import com.atolcd.parapheur.web.app.ParapheurApplication;
import org.adullact.iparapheur.domain.CertificatesDAO;
import org.adullact.iparapheur.domain.CertificatesEntity;
import org.adullact.iparapheur.domain.hibernate.CertificatesEntityImpl;
import org.adullact.iparapheur.repo.jscript.JsKeyMaterial;
import org.adullact.iparapheur.util.X509Util;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.exporter.ACPExportPackageHandler;
import org.alfresco.repo.exporter.ExporterComponent;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.*;
import org.alfresco.service.cmr.view.ExportPackageHandler;
import org.alfresco.service.cmr.view.ExporterCrawlerParameters;
import org.alfresco.service.cmr.view.Location;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.Pair;
import org.alfresco.util.TempFileProvider;
import org.alfresco.web.app.Application;
import org.alfresco.web.bean.repository.Repository;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.extensions.surf.util.Base64;
import org.springframework.util.Assert;

import javax.faces.context.FacesContext;
import javax.transaction.UserTransaction;
import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 *
 * @author Berger Levrault - Mathieu Passenaud
 */
public class UsersServiceImpl implements UsersService {

    private static Logger logger = Logger.getLogger(UsersServiceImpl.class);

    private String userInfoEndpoint = null;
    
    @Autowired
    @Qualifier("PersonService")
    private PersonService personService;
    
    @Autowired
    @Qualifier("NodeService")
    private NodeService nodeService;
     
    @Autowired
    private MimetypeService mimetypeService;

    @Autowired
    @Qualifier("authenticationComponent")
    AuthenticationComponent authComponent;

    /*@Autowired
    @Qualifier("authenticationService")
    AuthenticationService authService;*/

    @Autowired
    private MutableAuthenticationService authenticationService;

    @Autowired
    private ParapheurUserPreferences parapheurUserPreferences;

    @Autowired
    @Qualifier("certificatesDAOWithTransactionInterceptor")
    CertificatesDAO certificatesDAO;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private SearchService searchService;

    @Autowired
    private NamespaceService namespaceService;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private ContentService contentService;
    
    @Autowired
    private TransactionService transactionService;

    @Autowired
    private NotificationCenter notificationCenter;

    @Value("${parapheur.auth.keycloak.json.path}")
    private String jsonPath;


    public boolean isAuthorizeHeaderLogin() {
        return authorizeHeaderLogin;
    }

    public void setAuthorizeHeaderLogin(boolean authorizeHeaderLogin) {
        this.authorizeHeaderLogin = authorizeHeaderLogin;
    }

    private boolean authorizeHeaderLogin;

    @Override
    public String getUserTicketWithOpenId(String bearerToken) throws IOException, JSONException {
        String jsonContent = FileUtils.readFileToString(new File(jsonPath));
        JSONObject configuration = new JSONObject(jsonContent);
        // If we got here, we have to get a keycloak.json from specific URL
        if(jsonPath != null && userInfoEndpoint == null) {
            String url = configuration.getString("auth-server-url");
            String realm = configuration.getString("realm");

            // We have built the url, now we get the configuration JSON
            URL openIdConfigUrl = new URL(url + "/realms/" + realm + "/.well-known/openid-configuration");
            HttpURLConnection con = null;
            try {
                con = (HttpURLConnection) openIdConfigUrl.toURI().normalize().toURL().openConnection();
                con.setRequestMethod("GET");

                JSONObject response = getJsonObjectFromUrl(con);

                userInfoEndpoint = response.getString("userinfo_endpoint");
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        if(userInfoEndpoint != null) {
            URL url = new URL(userInfoEndpoint);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", "Bearer " + bearerToken);

            JSONObject response = getJsonObjectFromUrl(con);

            String username;
            String userkey = "preferred_username";
            if(configuration.has("principal-attribute")) {
                userkey = configuration.getString("principal-attribute");
            }
            if(response.has(userkey)) {
                username = response.getString(userkey);
            } else {
                throw new JSONException("Cannot fetch username from JSON response : " + response.toString());
            }


            authComponent.setCurrentUser(username);
            return authenticationService.getCurrentTicket();
        } else {
            return null;
        }
    }

    @NotNull
    private JSONObject getJsonObjectFromUrl(HttpURLConnection con) throws IOException, JSONException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();

        return new JSONObject(content.toString());
    }

    @Override
    public String createUser(String username, String email, String firstName, String lastName, String password) {

        Assert.isTrue(username != null, "Username is null");
        Assert.isTrue(email != null, "email is null");
        Assert.isTrue(firstName != null, "firstName is null");
        Assert.isTrue(lastName != null, "lastName is null");
        Assert.isTrue(password != null, "password is null");

        String userId;

        UserTransaction tx = null;
        try
        {
            FacesContext context = FacesContext.getCurrentInstance();
            tx = Repository.getUserTransaction(context, false);


            // Si on est sur du multi-tenant, on rajoute au besoin le @domaine
            if (tenantService.isEnabled()) {
                String currentDomain = tenantService.getCurrentUserDomain();
                if (!currentDomain.equals(TenantService.DEFAULT_DOMAIN))
                {
                    if (!tenantService.isTenantUser(username)) {
                        // force domain onto the end of the username
                        username = tenantService.getDomainUser(username, currentDomain);
                    }
                    else {
                        try {
                            tenantService.checkDomainUser(username);
                        }
                        catch (RuntimeException re) {
                            return "L'utilisateur doit appartenir au même domain que l'admin : " + currentDomain;
                        }
                    }
                }
            }

            if(!personService.personExists(username)) {
                tx.begin();

                // Recherche du noeud des parapheurs
                String XPath =  Application.getRootPath(context) + "/" + ParapheurApplication.getParapheursFolderName(context);
                NodeRef rootNodeRef = nodeService.getRootNode(Repository.getStoreRef());
                List<NodeRef> nodes = searchService.selectNodes(rootNodeRef, XPath, null, namespaceService, false);
                if (nodes.size() == 0) {
                    return "Impossible de trouver l'espace de stockage des bureaux   f: " + XPath;
                }
                NodeRef parapheursHome = nodes.get(0);

                //Création des propriétés de l'utilisateur
                Map<QName, Serializable> props = new HashMap<QName, Serializable>(7, 1.0f);
                props.put(ContentModel.PROP_USERNAME, username);
                props.put(ContentModel.PROP_FIRSTNAME, firstName);
                props.put(ContentModel.PROP_LASTNAME, lastName);
                props.put(ContentModel.PROP_HOMEFOLDER, parapheursHome);
                props.put(ContentModel.PROP_EMAIL, email);

                // Création du noeud utilisateur
                NodeRef userRef = personService.createPerson(props);

                // Ajout des permissions sur l'utilisateur
                permissionService.setPermission(userRef, username, permissionService.getAllPermission(), true);

                // Création de l'authentification ACEGI pour l'utilisateur
                authenticationService.createAuthentication(username, password.toCharArray());

                userId = userRef.getId();
                // commit the transaction
                tx.commit();
            } else {
                return "already exists";
            }
        }
        catch (Throwable e)
        {
            // rollback the transaction
            try { if (tx != null) {tx.rollback();} } catch (Exception tex) {}
            if (logger.isDebugEnabled()) {
                logger.error("Erreur lors de la création de l'utilisateur", e);
            }
            return "Erreur lors de la création de l'utilisateur" + e.getMessage();
        }
        return userId;
    }

    @Override
    public List<NodeRef> searchUser(String search) {
        search = search != null ? search : "";
        //transactionService.ge
        //FacesContext context = FacesContext.getCurrentInstance();
        UserTransaction tx = null;
        List<NodeRef> people = new ArrayList<NodeRef>();
        try {
            tx = transactionService.getUserTransaction(true);
            tx.begin();

            // define the query to find people by their first or last name
            String q = "+TYPE:\"{http://www.alfresco.org/model/content/1.0}person\" AND " +
                    "(@\\{http\\://www.alfresco.org/model/content/1.0\\}firstName:\"*"+ search +"*\" OR " +
                    "@\\{http\\://www.alfresco.org/model/content/1.0\\}lastName:\"*"+ search +"*\" OR " +
                    "@\\{http\\://www.alfresco.org/model/content/1.0\\}userName:\"*"+ search +"*\")";

            if (logger.isDebugEnabled()) {
                logger.debug("Query " + q);
            }

            // define the search parameters
            SearchParameters params = new SearchParameters();
            params.setLanguage(SearchService.LANGUAGE_LUCENE);
            params.addStore(Repository.getStoreRef());
            params.setQuery(q);

            ResultSet results = searchService.query(params);

            try {
                people = results.getNodeRefs();
            } finally {
                results.close();
            }
            // Désolé l'utilisateur 'admin' peut posséder un parapheur
            //people.remove(personService.getPerson(AuthenticationUtil.getAdminUserName()));
            people.remove(personService.getPerson(tenantService.getDomainUser(AuthenticationUtil.getGuestUserName(), tenantService.getCurrentUserDomain())));
            people.remove(personService.getPerson(tenantService.getDomainUser(AuthenticationUtil.getSystemUserName(), tenantService.getCurrentUserDomain())));

            if (logger.isDebugEnabled()) {
                logger.debug("Found " + people.size() + " users");
            }
            tx.commit();
        }
        catch (Exception err) {
            logger.error("Erreur lors de la recherche de l'utilisateur : ", err);
            try {
                if (tx != null) {
                    tx.rollback();
                }
            } catch (Exception tex) {}
        }
        return people;
    }
    
    @Override
    public void export(){
        try{
            // define which part of the Repository to export
            Location location = new Location(new StoreRef("workspace", "SpacesStore"));
            location.setPath("/");
            ExporterCrawlerParameters parameters = new ExporterCrawlerParameters();
            parameters.setExportFrom(location);


            // setup an ACP Package Handler to export to an ACP file format

            File zipFile = TempFileProvider.createTempFile("alf", ACPExportPackageHandler.ACP_EXTENSION);
            File dataFile = new File("/home/apzoei/export");
            File contentDir = new File("/home/apzoei/export");
            ExportPackageHandler handler = new ACPExportPackageHandler(new FileOutputStream(zipFile), 
                    dataFile, contentDir, mimetypeService);

            
            // now export (note: we're not interested in progress in the example)
            ExporterComponent exporter = new ExporterComponent();
            exporter.exportView(handler, parameters, null);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public Pair<String, String> getTicketForUser(String username) throws Exception {
        if(!authorizeHeaderLogin) {
            throw new Exception("Property 'authorizeHeaderLogin' not set. You can't do this...");
        }
        authComponent.setCurrentUser(username);

        return new Pair<String, String>(username, authenticationService.getCurrentTicket());
    }

    @Override
    public Pair<String, String> getTicketWithCertificate(X509Certificate[] certs) {
        Pair<String, String> pair = null;

        if (certs!= null && certs.length >0) {
            String identifier = JsKeyMaterial.getUniqueId(certs);

            if (!identifier.equals(BigInteger.ZERO.toString())) {
                AuthenticationUtil.setRunAsUserSystem();
                String ticket = null;
                String userName = null;
                CertificatesEntity c = certificatesDAO.getCertificatesById(identifier);
                if (c != null) {
                    userName = c.getUsername();
                }
                if (userName == null) {
                    throw new RuntimeException("Utilisateur inconnu");
                }

                authComponent.setCurrentUser(userName);
                ticket = authenticationService.getCurrentTicket();
                pair = new Pair<String, String>(userName, ticket);
            }
        }
        return pair;
    }

    @Override
    public void deleteUser(NodeRef user) {
        String username = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
        removeUserFromBureaux(username);
        personService.deletePerson(username);
        
    }

    private void removeUserFromBureaux(String username) {
        Set<String> authorities = authorityService.getAuthoritiesForUser(username);
        for (String auth : authorities) {
            if (auth.startsWith("ROLE_PHOWNER_")) {
                String bureauId = auth.substring("ROLE_PHOWNER_".length());
                NodeRef bureau = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, bureauId);
                if (nodeService.exists(bureau)) {
                    removeOwnerFromBureau(username, bureau);
                }
            }
            else if (auth.startsWith("ROLE_PHSECRETARIAT_")) {
                String bureauId = auth.substring("ROLE_PHSECRETARIAT_".length());
                NodeRef bureau = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, bureauId);
                if (nodeService.exists(bureau)) {
                    removeSecretaireFromBureau(username, bureau);
                }
            }
        }
    }

    public void setPersonService(PersonService personService){
        this.personService = personService;
    }


    @Override
    public List<NodeRef> getBureaux(String username) {
        ArrayList<NodeRef> bureaux = new ArrayList<NodeRef>();
        Set<String> authorities = this.authorityService.getAuthoritiesForUser(username);
        for (String authority : authorities) {
            if (authority.startsWith("ROLE_PHOWNER_")) {
                String bureauId = authority.substring("ROLE_PHOWNER_".length());
                NodeRef bureau = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, bureauId);
                if (nodeService.exists(bureau)) {
                    bureaux.add(bureau);
                }
            }
        }
        return bureaux;
    }

    @Override
    public List<NodeRef> getBureauxProprietaire(String username) {
        ArrayList<NodeRef> bureaux = new ArrayList<NodeRef>();
        Set<String> authorities = this.authorityService.getAuthoritiesForUser(username);
        for (String authority : authorities) {
            if (authority.startsWith("ROLE_PHOWNER_")) {
                String bureauId = authority.substring("ROLE_PHOWNER_".length());
                NodeRef bureau = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, bureauId);
                if (nodeService.exists(bureau)) {
                    bureaux.add(bureau);
                }
            }
        }
        return bureaux;
    }

    @Override
    public List<NodeRef> getBureauxSecretaire(String username) {
        ArrayList<NodeRef> bureaux = new ArrayList<NodeRef>();
        Set<String> authorities = this.authorityService.getAuthoritiesForUser(username);
        for (String authority : authorities) {
            if (authority.startsWith("ROLE_PHSECRETARIAT_")) {
                String bureauId = authority.substring("ROLE_PHSECRETARIAT_".length());
                NodeRef bureau = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, bureauId);
                if (nodeService.exists(bureau)) {
                    bureaux.add(bureau);
                }
            }
        }
        return bureaux;
    }

    @Override
    public List<NodeRef> getBureauxAdministres(String username) {
        ArrayList<NodeRef> bureaux = new ArrayList<NodeRef>();
        Set<String> authorities = this.authorityService.getAuthoritiesForUser(username);
        for (String authority : authorities) {
            if (authority.startsWith("ROLE_PHADMIN_")) {
                String bureauId = authority.substring("ROLE_PHADMIN_".length());
                NodeRef bureau = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, bureauId);
                if (nodeService.exists(bureau)) {
                    bureaux.add(bureau);
                }
            }
        }
        return bureaux;
    }

    @Override
    public void removeUserFromBureau(NodeRef user, NodeRef bureau, boolean isProprietaire) {
        if (isProprietaire) {
            removeOwnerFromBureau(user, bureau);
        }
        else {
            removeSecretaireFromBureau(user, bureau);
        }
    }

    @Override
    public void addUserToBureau(NodeRef user, NodeRef bureau, boolean isProprietaire) {
        if (isProprietaire) {
            addOwnerToBureau(user, bureau);
        }
        else {
            addSecretaireToBureau(user, bureau);
        }
    }

    private void addOwnerToBureau(NodeRef user, NodeRef bureau){
        ArrayList<String> proprietaires = (ArrayList<String>) (nodeService.getProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR));
        String userName = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
        if (!proprietaires.contains(userName)) {
            proprietaires.add(userName);
            nodeService.setProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, proprietaires);
            if (!authorityService.authorityExists("ROLE_PHOWNER_" + bureau.getId())) {
                authorityService.createAuthority(AuthorityType.ROLE, "PHOWNER_" + bureau.getId());
            }
            authorityService.addAuthority("ROLE_PHOWNER_" + bureau.getId(), authorityService.getName(AuthorityType.USER, userName));
        }

        List<String> previousOwners = (List<String>) nodeService.getProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);
    }

    private void removeOwnerFromBureau(NodeRef user, NodeRef bureau){
        ArrayList<String> proprietaires = (ArrayList<String>) (nodeService.getProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR));
        String userName  = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
        if (proprietaires.remove(userName)) {
            nodeService.setProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, proprietaires);
            authorityService.removeAuthority("ROLE_PHOWNER_" + bureau.getId(), authorityService.getName(AuthorityType.USER, userName));
        }
    }

    private void removeOwnerFromBureau(String username, NodeRef bureau){
        ArrayList<String> proprietaires = (ArrayList<String>) (nodeService.getProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR));
        if (proprietaires.remove(username)) {
            nodeService.setProperty(bureau, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, proprietaires);
            authorityService.removeAuthority("ROLE_PHOWNER_" + bureau.getId(), authorityService.getName(AuthorityType.USER, username));
        }
    }

    private void addSecretaireToBureau(NodeRef user, NodeRef bureau){
        ArrayList<String> secretaires = (ArrayList<String>) (nodeService.getProperty(bureau, ParapheurModel.PROP_SECRETAIRES));
        String userName = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
        if (!secretaires.contains(userName)) {
            secretaires.add(userName);
            nodeService.setProperty(bureau, ParapheurModel.PROP_SECRETAIRES, secretaires);
            if (!authorityService.authorityExists("ROLE_PHSECRETARIAT_" + bureau.getId())) {
                authorityService.createAuthority(AuthorityType.ROLE, "PHSECRETARIAT_" + bureau.getId());
            }
            authorityService.addAuthority("ROLE_PHSECRETARIAT_" + bureau.getId(), authorityService.getName(AuthorityType.USER, userName));
        }
    }

    private void removeSecretaireFromBureau(NodeRef user, NodeRef bureau){
        ArrayList<String> secretaires = (ArrayList<String>) (nodeService.getProperty(bureau, ParapheurModel.PROP_SECRETAIRES));
        String userName  = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
        if (secretaires.remove(userName)) {
            nodeService.setProperty(bureau, ParapheurModel.PROP_SECRETAIRES, secretaires);
            authorityService.removeAuthority("ROLE_PHSECRETARIAT_" + bureau.getId(), authorityService.getName(AuthorityType.USER, userName));
        }
    }

    private void removeSecretaireFromBureau(String username, NodeRef bureau){
        ArrayList<String> secretaires = (ArrayList<String>) (nodeService.getProperty(bureau, ParapheurModel.PROP_SECRETAIRES));
        if (secretaires.remove(username)) {
            nodeService.setProperty(bureau, ParapheurModel.PROP_SECRETAIRES, secretaires);
            authorityService.removeAuthority("ROLE_PHSECRETARIAT_" + bureau.getId(), authorityService.getName(AuthorityType.USER, username));
        }
    }

    @Override
    public boolean isProprietaire(String username) {
        return isInAuthorityPrefixedBy(username, "ROLE_PHOWNER_");
    }

    @Override
    public boolean isSecretaire(String username) {
        return isInAuthorityPrefixedBy(username, "ROLE_PHSECRETARIAT_");
    }

    @Override
    public boolean isAdministrateurFonctionnel(String username) {
        return isInAuthorityPrefixedBy(username, "ROLE_PHADMIN_");
    }

    @Override
    public boolean isAdministrateur(String username) {
        return isInAuthority(username, "GROUP_ALFRESCO_ADMINISTRATORS");
    }

    @Override
    public boolean isGestionnaireCircuit(String username) {
        return isInAuthority(username, WorkflowService.GROUP_GESTIONNAIRE_CIRCUITS);
    }

    private boolean isInAuthority(String username, String authorithy) {
        Set<String> authorities = authorityService.getAuthoritiesForUser(username);
        return authorities.contains(authorithy);
    }

    private boolean isInAuthorityPrefixedBy(String username, String authorithyPrefix) {
        boolean contained = false;
        Set<String> authorities = authorityService.getAuthoritiesForUser(username);
        for (String auth : authorities) {
            if (auth.startsWith(authorithyPrefix)) {
                contained = true;
                break;
            }
        }
        return contained;
    }

    @Override
    public void updateUser(NodeRef userRef, HashMap<QName, Serializable> propertiesMap) {
        for(Map.Entry<QName, Serializable> entry: propertiesMap.entrySet()) {
            nodeService.setProperty(userRef, entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void addToAdminGroup(String username) {
        String adminGroup = "GROUP_ALFRESCO_ADMINISTRATORS";
        if (authorityService.authorityExists(adminGroup)
                && !authorityService.getContainedAuthorities(AuthorityType.USER, adminGroup, true).contains(username)) {
            authorityService.addAuthority("GROUP_ALFRESCO_ADMINISTRATORS", authorityService.getName(AuthorityType.USER, username));
        }
    }

    @Override
    public void removeFromAdminGroup(String username) {
        String adminGroup = "GROUP_ALFRESCO_ADMINISTRATORS";
        while (authorityService.authorityExists(adminGroup)
                && authorityService.getContainedAuthorities(AuthorityType.USER, adminGroup, true).contains(username)) {
            authorityService.removeAuthority(adminGroup, authorityService.getName(AuthorityType.USER, username));
        }
    }

    @Override
    public String getSignature(NodeRef user) {
        String signatureId = null;

        if (nodeService.hasAspect(user, ParapheurModel.ASPECT_SIGNATURESCAN)) {
            List<ChildAssociationRef> scans = nodeService.getChildAssocs(user, ParapheurModel.ASSOC_SIGNATURE_SCAN, RegexQNamePattern.MATCH_ALL);
            if (scans.size() > 0) {
                signatureId = scans.get(0).getChildRef().getId();
            }
        }
        return signatureId;
    }

    @Override
    public String setSignature(NodeRef user, String signatureB64) {

        ChildAssociationRef childAssociationRef = null;

        List<ChildAssociationRef> scans = nodeService.getChildAssocs(user, ParapheurModel.ASSOC_SIGNATURE_SCAN, RegexQNamePattern.MATCH_ALL);
        if(scans.size() > 0) {
            childAssociationRef = scans.get(0);
        }

        if(childAssociationRef == null) {
            childAssociationRef = nodeService.createNode(user,
                    ParapheurModel.ASSOC_SIGNATURE_SCAN,
                    QName.createQName("cm:signature"),
                    ContentModel.TYPE_CONTENT);
        }

        InputStream is = new ByteArrayInputStream(Base64.decode(signatureB64));

        ContentWriter imageWriter = contentService.getWriter(childAssociationRef.getChildRef(),ContentModel.PROP_CONTENT, true);
        /* Ecriture du contenu de la signature dans PROP_CONTENT du nouveau nœud */
        imageWriter.putContent(is);

        return childAssociationRef.getChildRef().getId();
    }

    @Override
    public void deleteSignature(NodeRef user) {

        if (nodeService.hasAspect(user, ParapheurModel.ASPECT_SIGNATURESCAN)) {
            for (ChildAssociationRef c : nodeService.getChildAssocs(user, ParapheurModel.ASSOC_SIGNATURE_SCAN, RegexQNamePattern.MATCH_ALL)) {
                nodeService.removeChildAssociation(c);
            }
            nodeService.removeAspect(user, ParapheurModel.ASPECT_SIGNATURESCAN);
        }
    }

    @Override
    public String getCertificat(NodeRef user) {
        ContentReader reader = contentService.getReader(user, ParapheurModel.PROP_CERTIFICAT);
        return (reader != null)? reader.getContentString() : null;
    }

    @Override
    public boolean hasCertificat(NodeRef user) {
        ContentReader reader = contentService.getReader(user, ParapheurModel.PROP_CERTIFICAT);
        return (reader != null);
    }

    @Override
    public Map<String, String> setCertificat(NodeRef user, String certificatB64) {

        String username = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
        byte[] decoded = Base64.decode(certificatB64);
        if (decoded != null) {
            String certString = new String(decoded);
            X509Certificate cert = X509Util.getX509CertificateFromString(certString);
            String idCertificat = X509Util.getUniqueId(cert);

            // Si l'utilisateur possédait un autre certificat, on le supprime
            deleteCertificat(user);

            // Si le nouveau certificat est utilisé par un autre utilisateur, on le désassocie de l'ancien utilisateur
            CertificatesEntity certFromOtherUser = certificatesDAO.getCertificatesById(idCertificat);
            if (certFromOtherUser != null) {
                NodeRef oldUser = personService.getPerson(certFromOtherUser.getUsername());
                if (oldUser != null) {
                    deleteCertificat(oldUser);
                }
                certificatesDAO.deleteCertificates(certFromOtherUser);
            }


            ContentWriter writer = contentService.getWriter(user,ParapheurModel.PROP_CERTIFICAT, true);
            writer.setMimetype(X509Util.MIMETYPE_X509_CA_CERT);
            writer.putContent(new ByteArrayInputStream(decoded));
            nodeService.setProperty(user, ParapheurModel.PROP_ID_CERTIFICAT, idCertificat);

            CertificatesEntity entity = new CertificatesEntityImpl();
            entity.setUsername(username);
            entity.setCertificateId(idCertificat);
            certificatesDAO.createCertificates(entity);

            return (cert != null)? X509Util.getUsefulCertProps(cert) : null;
        }
        return null;
    }

    @Override
    public void deleteCertificat(NodeRef user) {
        String username = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
        CertificatesEntity cert = certificatesDAO.getCertificatesByUsername(username);
        if (cert != null) {
            certificatesDAO.deleteCertificates(cert);
            nodeService.setProperty(user, ParapheurModel.PROP_CERTIFICAT, "");
            nodeService.setProperty(user, ParapheurModel.PROP_ID_CERTIFICAT, "");
        }
    }

    @Override
    public String getCurrentUserTicket() {
        return authenticationService.getCurrentTicket();
    }

    @Override
    public int resetPasswordRequest(String username, String email) {

        return AuthenticationUtil.runAs(() -> {
            // Code 0 = ok
            int result = 0;

            NodeRef user = null;
            try {
                user = personService.getPerson(username, false);
            } catch(NoSuchPersonException e) {
                // ignore exception
            }

            if(user != null) {
                boolean isFromLdap = false;
                List<ChildAssociationRef> assocs = nodeService.getParentAssocs(user);
                for(ChildAssociationRef assoc : assocs) {
                    NodeRef parent = assoc.getParentRef();
                    if(((String)nodeService.getProperty(parent, ContentModel.PROP_NAME)).contains("AUTH.EXT")) {
                        isFromLdap = true;
                        break;
                    }
                }
                if(isFromLdap) {
                    // Code 2 = Ldap user
                    logger.warn("LDAP User " + username + " requested a password reset");
                    result = 2;
                } else {
                    String definedEmail = (String) nodeService.getProperty(user, ContentModel.PROP_EMAIL);
                    List<String> userMails = Arrays.asList(parapheurUserPreferences.getNotificationMailForUsername(username));
                    if(definedEmail != null && (definedEmail.equals(email) || userMails.contains(email))) {
                        String uuid = UUID.randomUUID().toString();
                        // Add uuid to specific user properties
                        nodeService.setProperty(user, ParapheurModel.PROP_RESET_PASSWORD_UUID, uuid);
                        nodeService.setProperty(user, ParapheurModel.PROP_RESET_PASSWORD_DATE, new Date());
                        // Send mail to user with link with uuid in it
                        notificationCenter.sendResetPassword(username, email, uuid);
                        // Code 0 = OK
                        result = 0;
                    }
                }
            }
            return result;
        }, getTenantAdminUsername(username));
    }

    @Override
    public int resetPassword(String username, String uuid, String password) {

        return AuthenticationUtil.runAs(() -> {
            // Code 1 = error
            int result = 1;

            NodeRef user = null;
            try {
                user = personService.getPerson(username, false);
            } catch(NoSuchPersonException e) {
                // ignore exception
            }

            if(user != null) {
                String definedUuid = (String) nodeService.getProperty(user, ParapheurModel.PROP_RESET_PASSWORD_UUID);
                Date definedDate = (Date) nodeService.getProperty(user, ParapheurModel.PROP_RESET_PASSWORD_DATE);
                if(definedDate != null) {
                    Date limit = new Date(definedDate.getTime() + (30 * 60 * 1000));

                    if(limit.before(new Date())) {
                        logger.warn("Reset token is expired");
                        nodeService.removeProperty(user, ParapheurModel.PROP_RESET_PASSWORD_UUID);
                        nodeService.removeProperty(user, ParapheurModel.PROP_RESET_PASSWORD_DATE);
                        // code 2 = expired
                        result = 2;
                    } else if(uuid != null && uuid.equals(definedUuid)) {
                        authenticationService.setAuthentication(username, password.toCharArray());
                        nodeService.removeProperty(user, ParapheurModel.PROP_RESET_PASSWORD_UUID);
                        result = 0;
                    }
                }
            }

            return result;
        }, getTenantAdminUsername(username));
    }

    private String getTenantAdminUsername(String username) {
        String adminUsername = "admin";

        try {
            if(tenantService.isEnabled()
                    && tenantService.isTenantUser(username)
                    && tenantService.getTenant(tenantService.getUserDomain(username)).isEnabled()) {
                adminUsername += "@" + tenantService.getUserDomain(username);
            }
        } catch(java.lang.RuntimeException ex) {
            // ignore exception
        }

        return adminUsername;
    }


}
