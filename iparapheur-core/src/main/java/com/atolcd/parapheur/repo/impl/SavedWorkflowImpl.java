package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.SavedWorkflow;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.alfresco.service.cmr.repository.NodeRef;

public class SavedWorkflowImpl implements SavedWorkflow, Serializable {

    private NodeRef nodeRef;

    private Set<NodeRef> aclParapheurs;

    private Set<String> aclGroupes;

    private List<EtapeCircuit> etapes;

    private String name;

    private boolean _public;

    public SavedWorkflowImpl() {
        this.aclParapheurs = new HashSet<NodeRef>();
        this.aclGroupes = new HashSet<String>();
        this.etapes = new ArrayList<EtapeCircuit>();
    }

    public NodeRef getNodeRef() {
        return nodeRef;
    }

    public void setNodeRef(NodeRef nodeRef) {
        this.nodeRef = nodeRef;
    }

    public boolean isPublic() {
        return _public;
    }

    public void setPublic(boolean _public) {
        this._public = _public;
    }

    public List<EtapeCircuit> getCircuit() {
        return this.etapes;
    }

    public List<NodeRef> getListeEtapesParapheurs() {
        List<NodeRef> circuit = new ArrayList<NodeRef>();
        for (EtapeCircuit elt : this.etapes) {
            circuit.add(elt.getParapheur());
        }
        return circuit;
    }

    /**
     * @see com.atolcd.parapheur.repo.SavedWorkflow#getDiffusion(EtapeCircuit)
     */
    public Set<NodeRef> getDiffusion(EtapeCircuit etape) {
        return etape.getListeNotification();
    }

    public void addToCircuit(EtapeCircuitImpl etape) {
        this.etapes.add(etape);
    }

    public void addToAclParapheurs(NodeRef noderef) {
        this.aclParapheurs.add(noderef);
    }

    public void addToAclGroupes(String groupe) {
        this.aclGroupes.add(groupe);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<NodeRef> getAclParapheurs() {
        return aclParapheurs;
    }

    public Set<String> getAclGroupes() {
        return aclGroupes;
    }
}
