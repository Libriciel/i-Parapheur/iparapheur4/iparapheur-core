/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.patch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.alfresco.repo.admin.patch.AbstractPatch;
import org.alfresco.repo.domain.audit.AuditDAO;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.audit.AuditQueryParameters;

import org.alfresco.service.cmr.audit.AuditService;

/**
 *
 * @author eperalta
 */
public class AuditWrongOwnerPatch extends AbstractPatch {
//public class AuditNullParapheurPatch extends AbstractPatch {

    private AuditService auditService;
    private AuditDAO auditDAO;
    private TenantService tenantService;
    

    @Override
    protected String applyInternal() throws Exception {
        int count;

        AuditQueryParameters aqp = new AuditQueryParameters();
        aqp.setApplicationName("ParapheurServiceCompat");
        AuditDAO.AuditApplicationInfo application = auditDAO.getAuditApplication("ParapheurServiceCompat");

        UpdateUserCallback callback = new UpdateUserCallback();
        callback.setAuditDAO(auditDAO);
        callback.setApplicationId(application.getId());

        auditService.auditQuery(callback, aqp, 0);

        auditDAO.deleteAuditEntries(callback.getAuditEntriesToDelete());
        count = callback.getAuditEntriesToDelete().size();

        return "Fixed " + count + " Audit items with wrong ownership";
    }

    public AuditDAO getAuditDAO() {
        return auditDAO;
    }

    public void setAuditDAO(AuditDAO auditDAO) {
        this.auditDAO = auditDAO;
    }

    public AuditService getAuditService() {
        return auditService;
    }

    public void setAuditService(AuditService auditService) {
        this.auditService = auditService;
    }

    public class UpdateUserCallback implements AuditService.AuditQueryCallback {
        List<Long> auditEntriesToDelete;
        long applicationId;

        
        AuditDAO auditDAO;

        public UpdateUserCallback() {
            this.auditEntriesToDelete = new ArrayList<Long>();
        }

        @Override
        public boolean valuesRequired() {
            return true;
        }

        @Override
        public boolean handleAuditEntry(Long entryId, String applicationName, String user, long time, Map<String, Serializable> map) {
            String message = map.get("message").toString();

            if (message.endsWith(": Dossier lu et prêt pour la signature")) {
                // extract username

                String goodUserName = message.substring(0, message.indexOf(':'));

                if (!user.equals(goodUserName)) {

                    final long fTime = time;
                    final String fGoodUserName = goodUserName;
                    final Map fMap = map;

                    AuthenticationUtil.RunAsWork<Boolean> work = new AuthenticationUtil.RunAsWork() {

                        @Override
                        public Boolean doWork() throws Exception {
                            auditDAO.createAuditEntry(applicationId, fTime, fGoodUserName, fMap);
                            return true;
                        }
                    };

                    AuthenticationUtil.runAs(work, AuthenticationUtil.getSystemUserName());


                    auditEntriesToDelete.add(entryId);
                }
            }

            return true;
        }

        public boolean handleAuditEntryError(Long l, String string, Throwable thrwbl) {
            return true;
        }

        public List<Long> getAuditEntriesToDelete() {
            return auditEntriesToDelete;
        }

        public void setAuditEntriesToDelete(List<Long> auditEntriesToDelete) {
            this.auditEntriesToDelete = auditEntriesToDelete;
        }

        public AuditDAO getAuditDAO() {
            return auditDAO;
        }

        public void setAuditDAO(AuditDAO auditDAO) {
            this.auditDAO = auditDAO;
        }

        public long getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(long applicationId) {
            this.applicationId = applicationId;
        }
    }
}
