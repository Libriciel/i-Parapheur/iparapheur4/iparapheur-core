/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.repo.impl.exceptions.SubTypeNotFoundRuntimeException;
import org.adullact.iparapheur.repo.jscript.pastell.mailsec.MailsecPastellModel;
import org.adullact.iparapheur.rules.bean.CustomProperty;
import org.adullact.iparapheur.util.CollectionsUtils;
import org.adullact.utils.StringUtils;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.namespace.NamespaceService;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.dom4j.tree.DefaultAttribute;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 * Gestion de la typologie de dossiers
 *
 * @author Vivien Barousse - ADULLACT Projet
 */
public final class TypesServiceImpl implements TypesService {

    private static Logger logger = Logger.getLogger(TypesService.class);
    private StoreRef storeRef;
    private NodeService nodeService;
    private ContentService contentService;
    private SearchService searchService;
    private NamespaceService namespaceService;
    private WorkflowService workflowService;
    private ParapheurService parapheurService;
    private RulesService rulesService;

    @Autowired
    private AuthorityService authorityService;

    @Override
    public boolean isDigitalSignatureMandatory(String typeName, String subtypeName) {

        String mandatory = getValueForSousType(typeName, subtypeName, SOUS_TYPE_KEY_DIGITAL_SIGNATURE_MANDATORY);
        if ("blex".equals(parapheurService.getHabillage())) {
            // BL prefere que les gens signent pour de vrai par defaut
            return (mandatory == null) || Boolean.parseBoolean(mandatory);
        }
        // par defaut, on autorise la signature papier.
        return Boolean.parseBoolean(mandatory);
    }

    @Override
    public boolean isMultiDocument(String typeName, String subtypeName) {
        String mandatory = getValueForSousType(typeName, subtypeName, SOUS_TYPE_KEY_MULTI_DOCUMENT);
        return Boolean.parseBoolean(mandatory);
    }

    @Override
    public NodeRef getCertificate(String typeName, String subtypeName) {
        String id = getValueForSousType(typeName, subtypeName, SOUS_TYPE_KEY_CACHET_ID);
        return new NodeRef("workspace://SpacesStore/" + id);
    }

    @Override
    public String getPastellServerId(String typeName, String subtypeName) {
        String id = getValueForSousType(typeName, subtypeName, SOUS_TYPE_KEY_PASTELL_MAILSEC_ID);
        NodeRef node = new NodeRef("workspace://SpacesStore/" + id);

        if(!nodeService.exists(node)) {
            throw new AlfrescoRuntimeException("Connecteur Pastell introuvable ou supprimé");
        }

        return (String) nodeService.getProperty(node, MailsecPastellModel.PROP_PASTELL_CONNECTOR_SERVER_ID);
    }

    @Override
    public boolean isReadingMandatory(String typeName, String subtypeName) {
        String mandatory = getValueForSousType(typeName, subtypeName, SOUS_TYPE_KEY_READING_MANDATORY);
        if (mandatory != null) {
            return Boolean.parseBoolean(mandatory);
        } else {
            throw new IllegalStateException("Missing element: "
                    + "'readingMandatory' "
                    + "for subtype: '" + subtypeName
                    + "' (type: '" + typeName
                    + "')");
        }
    }

    @Override
    public boolean isForbidSameSig(String typeName, String subtypeName) {
        if(typeName == null || subtypeName == null) return false;
        String isForbidden = getValueForSousType(typeName, subtypeName, SOUS_TYPE_KEY_FORBID_SAME_SIG);
        if (isForbidden != null) {
            return Boolean.parseBoolean(isForbidden);
        } else {
            return false;
        }
    }

    @Override
    public boolean isCircuitHierarchiqueVisible(String typeName, String subtypeName) {
        String visible = getValueForSousType(typeName, subtypeName, SOUS_TYPE_KEY_CIRCUIT_HIERARCHIQUE_VISIBLE);
        /* Backwards compatibility: If the property doesn't exists on subtype, the old behaviour is
         * conserved: circuit hierarchique is visible */
        return (visible == null) || Boolean.parseBoolean(visible);
    }
    
    @Override
    public boolean areAttachmentsIncluded(String typeName, String subtypeName) {
        String attachmentIncluded = getValueForSousType(typeName, subtypeName, SOUS_TYPE_KEY_INCLUDE_ATTACHMENTS);
        // Backwards compatibility: the attachments are included by default (if null).
        return (attachmentIncluded == null) || Boolean.parseBoolean(attachmentIncluded);
    }

    public List<NodeRef> getListeCalques(String typeName, String subtypeName, boolean forAnnexes) {
        List<NodeRef> results = new ArrayList<NodeRef>();
        ContentReader subtypesReader = contentService.getReader(
                getSubtypesNodeRef(typeName),
                ContentModel.PROP_CONTENT);
        try {
            SAXReader saxreader = new SAXReader();
            Document docXml = saxreader.read(subtypesReader.getContentInputStream());
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                Iterator<Element> itMetierSousType = rootElement.elementIterator("MetierSousType");
                for (Iterator<Element> ie = itMetierSousType; ie.hasNext();) {
                    Element msoustype = ie.next();
                    String tID = msoustype.element("ID").getText().trim();
                    if (tID.equals(subtypeName)) {
                        Element mCalques = msoustype.element(forAnnexes ? "calquesAnnexes" : "calques");
                        if (mCalques != null) {
                            Iterator<Element> calques = mCalques.elementIterator("calque");
                            while (calques.hasNext()) {
                                Element calque = calques.next();
                                NodeRef calqueRef;
                                if(!forAnnexes && calque.element("id") != null) {
                                    calqueRef = new NodeRef(calque.element("id").getText().trim());
                                } else {
                                    calqueRef = new NodeRef(calque.getText().trim());
                                }
                                results.add(calqueRef);
                            }
                        }
                        /**
                         * else { // no 'calques' defined
                         }*
                         */
                        return results;
                    }
                }
                throw new IllegalArgumentException("Subtype " + subtypeName
                        + " doesn't exists for type '" + typeName + "'");
            }
            throw new IllegalStateException("Invalid XML file: missing 'MetierTypes' element");
        } catch (DocumentException ex) {
            throw new RuntimeException("Unexpected exception parsing XML document", ex);
        }
    }

    @Override
    public List<NodeRef> getListeCalques(String typeName, String subtypeName) {
        return getListeCalques(typeName, subtypeName, false);
    }

    @Override
    public List<NodeRef> getListeCalquesAnnexes(String typeName, String subtypeName) {
        return getListeCalques(typeName, subtypeName, true);
    }

    /**
     * Transforms the xml stored metadata defs into a more usable structure
     * results.get
     *
     * @param typeName
     * @param subtypeName
     * @return Map de map de map...
     */
    @Override
    public Map<String, Map<String, String>> getMetadatasMap(String typeName, String subtypeName) {
        Map<String, Map<String, String>> mds;

        ParseSubtypes<Map<String, Map<String, String>>> parser = new ParseSubtypes<Map<String, Map<String, String>>>("metadatas", "metadata") {

            @Override
            public Map<String, Map<String, String>> callback(Iterator<Element> elements) {

                if (results == null) {
                    results = new HashMap<String, Map<String, String>>();
                }

                String id;
                while (elements.hasNext()) {
                    Map<String, String> props = new HashMap<String, String>();
                    Element element = elements.next();

                    id = element.getText().trim();
                    List<DefaultAttribute> attributes = element.attributes();

                    for (DefaultAttribute attribute : attributes) {

                        props.put(attribute.getName(), attribute.getValue());
                    }

                    results.put(id, props);

                }
                return results;
            }
        };

        mds = parser.parse(typeName, subtypeName);

        return mds;
    }

    //TODO: use it in calquesService.
    public List<NodeRef> getCalquesMZ(String typeName, String subtypeName) {
        List<NodeRef> retval;

        ParseSubtypes<List<NodeRef>> parser = new ParseSubtypes<List<NodeRef>>("calques", "calque") {

            @Override
            public List<NodeRef> callback(Iterator<Element> calques) {

                while (calques.hasNext()) {
                    Element calque = calques.next();
                    NodeRef calqueRef = new NodeRef(calque.getText().trim());
                    results.add(calqueRef);
                }
                return results;

            }
        };

        retval = parser.parse(typeName, subtypeName);

        return retval;
    }

    private abstract class ParseSubtypes<T> {

        protected T results;
        private String collectionName;
        private String elementName;

        public abstract T callback(Iterator<Element> elements);

        public ParseSubtypes(String collectionName, String elementName) {
            this.collectionName = collectionName;
            this.elementName = elementName;

        }

        public T parse(String typeName, String subtypeName) {


            ContentReader subtypesReader = contentService.getReader(
                    getSubtypesNodeRef(typeName),
                    ContentModel.PROP_CONTENT);
            try {
                SAXReader saxreader = new SAXReader();
                Document docXml = saxreader.read(subtypesReader.getContentInputStream());
                Element rootElement = docXml.getRootElement();
                if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                    Iterator<Element> itMetierSousType = rootElement.elementIterator("MetierSousType");
                    for (Iterator<Element> ie = itMetierSousType; ie.hasNext();) {
                        Element msoustype = ie.next();
                        String tID = msoustype.element("ID").getText().trim();
                        if (tID.equals(subtypeName)) {
                            Element collectionElt = msoustype.element(this.collectionName);
                            if (collectionElt != null) {
                                Iterator<Element> elements = collectionElt.elementIterator(this.elementName);
                                this.callback(elements);
                            }
                            /**
                             * else { // no collection named this.colletionName
                             * defined
                            }*
                             */
                            return results;
                        }
                    }
                    throw new IllegalArgumentException("Subtype " + subtypeName
                            + " doesn't exists for type '" + typeName + "'");
                }
                throw new IllegalStateException("Invalid XML file: missing 'MetierTypes' element");
            } catch (DocumentException ex) {
                throw new RuntimeException("Unexpected exception parsing XML document", ex);
            }
        }
    }

    @Override
    public List<String> getEditableMetadatas(String typeName, String subtypeName) {
        List<String> results = new ArrayList<String>();
        ContentReader subtypesReader = contentService.getReader(
                getSubtypesNodeRef(typeName),
                ContentModel.PROP_CONTENT);
        try {
            SAXReader saxreader = new SAXReader();
            Document docXml = saxreader.read(subtypesReader.getContentInputStream());
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                Iterator<Element> itMetierSousType = rootElement.elementIterator("MetierSousType");
                for (Iterator<Element> ie = itMetierSousType; ie.hasNext();) {
                    Element msoustype = ie.next();
                    String tID = msoustype.element("ID").getText().trim();
                    if (tID.equals(subtypeName)) {
                        Element mCalques = msoustype.element("metadatas");
                        if (mCalques != null) {
                            Iterator<Element> calques = mCalques.elementIterator("metadata");
                            while (calques.hasNext()) {
                                Element calque = calques.next();
                                if (calque.attribute("editable").getValue().equals("true")) {
                                    results.add(calque.getText().trim());
                                }
                            }
                        }
                        /**
                         * else { // no 'calques' defined
                        }*
                         */
                        return results;
                    }
                }
                throw new IllegalArgumentException("Subtype " + subtypeName
                        + " doesn't exists for type '" + typeName + "'");
            }
            throw new IllegalStateException("Invalid XML file: missing 'MetierTypes' element");
        } catch (DocumentException ex) {
            throw new RuntimeException("Unexpected exception parsing XML document", ex);
        }
    }

    @Override
    public List<String> getMandatoryMetadatas(String typeName, String subtypeName) {
        List<String> mandatory;

        ParseSubtypes<List<String>> parser = new ParseSubtypes<List<String>>("metadatas", "metadata") {

            @Override
            public List<String> callback(Iterator<Element> elements) {

                if (results == null) {
                    results = new ArrayList<String>();
                }

                String id = "";
                while (elements.hasNext()) {
                    Element element = elements.next();
                    if (element.attribute("mandatory").getValue().equals("true")) {
                        results.add(element.getText().trim());
                    }

                }
                return results;
            }
        };

        mandatory = parser.parse(typeName, subtypeName);

        return mandatory;
    }


    /*
     * public List<String> getMandatoryMetadatas(String typeName, String
     * subtypeName) { List<String> results = new ArrayList<String>();
     * ContentReader subtypesReader = contentService.getReader(
     * getSubtypesNodeRef(typeName), ContentModel.PROP_CONTENT); try { SAXReader
     * saxreader = new SAXReader(); Document docXml =
     * saxreader.read(subtypesReader.getContentInputStream()); Element
     * rootElement = docXml.getRootElement(); if
     * (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
     * Iterator<Element> itMetierSousType =
     * rootElement.elementIterator("MetierSousType"); for (Iterator<Element> ie
     * = itMetierSousType; ie.hasNext();) { Element msoustype = ie.next();
     * String tID = msoustype.element("ID").getTextTrim(); if
     * (tID.equals(subtypeName)) { Element mCalques =
     * msoustype.element("metadatas"); if (mCalques != null) { Iterator<Element>
     * calques = mCalques.elementIterator("metadata"); while (calques.hasNext())
     * { Element calque = calques.next(); if
     * (calque.attribute("mandatory").equals("true")) {
     * results.add(calque.getTextTrim()); } } } else { // no 'calques' defined }
     * return results; } } throw new IllegalArgumentException("Subtype " +
     * subtypeName + " doesn't exists for type '" + typeName + "'"); } throw new
     * IllegalStateException("Invalid XML file: missing 'MetierTypes' element");
     * } catch (DocumentException ex) { throw new RuntimeException("Unexpected
     * exception parsing XML document", ex); } }
     */
    @Override
    public NodeRef getWorkflow(String type, String subtype) {
        return getWorkflow(null, type, subtype, Collections.<CustomProperty<? extends Object>>emptyList());
    }

    @Override
    public boolean hasSelectionScript(String type, String subtype) {
        String circuitValue = getValueForSousType(type, subtype, SOUS_TYPE_KEY_CIRCUIT);
        return circuitValue != null && circuitValue.isEmpty();
    }

    @Override
    public NodeRef getWorkflowHandledError(NodeRef dossier, String type, String subtype, List<CustomProperty<? extends Object>> customProperties, boolean withError, boolean fromWs) {
        String xpath = "/app:company_home/app:dictionary/cm:metiersoustype/*[@cm:name='" + type + ".xml']";
        List<NodeRef> results;
        try {
            results = searchService.selectNodes(nodeService.getRootNode(storeRef), xpath, null, namespaceService, false);
            if (results != null &&  results.size() == 1) {
                NodeRef xmlSousTypeNoderef = results.get(0);
                ContentReader contentreader = contentService.getReader(xmlSousTypeNoderef, ContentModel.PROP_CONTENT);
                if (contentreader != null) {
                    SAXReader saxreader = new SAXReader();
                    Document docXml = saxreader.read(contentreader.getContentInputStream());
                    Element rootElement = docXml.getRootElement();
                    if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                        Iterator<Element> itMetierSousType = rootElement.elementIterator("MetierSousType");
                        for (Iterator<Element> ie = itMetierSousType; ie.hasNext();) {
                            Element msoustype = ie.next();
                            if (subtype.equalsIgnoreCase(msoustype.element("ID").getText().trim())) {
                                String circuit = null;

                                int variables = 0;

                                if (msoustype.element("Circuit") != null
                                        && !msoustype.element("Circuit").getText().trim().equals("")) {
                                    circuit = msoustype.element("Circuit").getText().trim();
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("circuit (" + type + " // " + subtype + "): " + circuit);
                                    }
                                } else if (msoustype.element("Script") != null
                                		&& !msoustype.element("Script").getText().trim().equals("")) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("(debug)SCRIPT:\n\t" + msoustype.element("Script").getText().trim());
                                    }
                                    Map<String, Object> resultForScript = null;
                                    try {
	                                    if(withError) {
	                                        resultForScript = rulesService.selectWorkflow(msoustype.element("Script").getText().trim(), customProperties);
	                                    } else {
	                                        resultForScript = rulesService.selectWorkflowWithoutPropertiesError(msoustype.element("Script").getText().trim(), customProperties);
	                                    }
                                    } catch(Throwable ex) {
                                    	// L'exécution du script peut provoquer une "Error" exception qui si elle n'est pas correctement
                                        // traitée ne provoquera pas la fermeture des transactions de l'appelant.
                                        // On transforme donc une Throwable en RuntimeException
                                        throw new SubTypeNotFoundRuntimeException("Erreur lors de l'exécution du script permettant de retrouver le circuit. Métadonnées obligatoires non définies ou script invalide");
                                    }
                                    circuit = (String) resultForScript.get("workflow");
                                    // Next, for each bureau in parameter... Find it and put it in dossier !
                                    List<NodeRef> listeVariables = new ArrayList<NodeRef>();
                                    for(String bureauName : (List<String>) resultForScript.get("variables")) {
                                        // Script can return null desk as a String if it's not found
                                        if(!bureauName.equals("null")) {
                                            List<NodeRef> listBureaux = parapheurService.getParapheursFromName(bureauName);
                                            if(listBureaux != null && listBureaux.size() > 0) {
                                                listeVariables.add(listBureaux.get(0));
                                                variables++;
                                            } else {
                                                if (logger.isEnabledFor(Level.ERROR)) {
                                                    logger.error("Acteur variable introuvable : " + bureauName);
                                                }
//                                                throw new SubTypeNotFoundRuntimeException("No workflow defined for " + type + " / " + subtype);
                                            }
                                        }
                                    }
                                    if(dossier != null) {
                                        nodeService.setProperty(dossier, ParapheurModel.PROP_ACTEURS_VARIABLES, (Serializable) listeVariables);
                                    }
                                }

                                NodeRef circuitRef = null;

                                if (circuit != null) {
                                    circuitRef = workflowService.getWorkflowByName(circuit);

                                    if(fromWs) {
                                        for(EtapeCircuit ec: workflowService.getSavedWorkflow(circuitRef).getCircuit()) {
                                            if(EtapeCircuit.TRANSITION_VARIABLE.equals(ec.getTransition())) {
                                                variables--;
                                            }
                                        }

                                        if(variables < 0) {
                                            throw new SubTypeNotFoundRuntimeException("Acteur(s) variable(s) non défini(s)");
                                        }
                                    }

                                } else if (logger.isDebugEnabled()) {
                                    logger.debug("circuit is null? did not found the name");
                                }

                                if (circuitRef == null) {
                                    if (logger.isEnabledFor(Level.ERROR)) {
                                        logger.error("No workflow defined for " + type + " / " + subtype);
                                    }
                                    throw new SubTypeNotFoundRuntimeException("No workflow defined for " + type + " / " + subtype);
                                    //throw new RuntimeException("No workflow defined for " + type + " / " + subtype);
                                }

                                return circuitRef;
                            }
                        }
                    }
                }
            }
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("Type/SubType not found.");
            }
            throw new RuntimeException("Type/Subtype not found.");
        } catch (DocumentException de) {
            throw new RuntimeException("Error parsing XML document.", de);
        } catch (AccessDeniedException err) {
            throw new RuntimeException("Access denied to type node.", err);
        }
    }

    @Override
    public NodeRef getWorkflowFromWs(NodeRef dossier, String type, String subtype, List<CustomProperty<? extends Object>> customProperties) {
        return this.getWorkflowHandledError(dossier, type, subtype, customProperties, true, true);
    }

    @Override
    public NodeRef getWorkflow(NodeRef dossier, String type, String subtype, List<CustomProperty<? extends Object>> customProperties) {
        return this.getWorkflowHandledError(dossier, type, subtype, customProperties, true, false);
    }

    @Override
    public Map<String, Map<String, List<String>>> getUsedWorkflows() {
        //Utilisation de la classe MultiMap Guava, pour lier plusieurs sous-type à un seul circuit
        Map<String, Map<String, List<String>>> map = new HashMap<String, Map<String, List<String>>>();

        Map<String, List<Element>> subtypes = getSubtypesElements();

        for(String type : subtypes.keySet()) {
            for(Element subtype : subtypes.get(type)) {
                if(subtype.element("Circuit") != null
                        && !subtype.element("Circuit").getText().trim().equals("")) {
                    String circuit = subtype.element("Circuit").getText().trim();
                    //On a un circuit défini
                    if(!map.containsKey(circuit)) {
                        map.put(circuit, new HashMap<String, List<String>>());
                    }
                    if(!map.get(circuit).containsKey(type)) {
                        map.get(circuit).put(type, new ArrayList<String>());
                    }
                    map.get(circuit).get(type).add(subtype.element("ID").getText().trim());
                } else if (subtype.element("Script") != null) {
                    //TODO : Handle Script !
                }
            }
        }

        return map;
    }

    @Override
    public void setUsedWorkflows(NodeRef workflow, Map<String, List<String>> usedBy) {
        nodeService.setProperty(workflow, ParapheurModel.PROP_USED_BY, (Serializable) usedBy);
    }

    @Override
    public Map<String, List<Element>> getSubtypesElements() {
        String xpath = "/app:company_home/app:dictionary/cm:metiersoustype/*";
        List<NodeRef> results;
        Map<String, List<Element>> toReturn = new HashMap<String, List<Element>>();
        try {
            results = searchService.selectNodes(nodeService.getRootNode(storeRef), xpath, null, namespaceService, false);
            if (results != null) {
                for(NodeRef xmlSousTypeNoderef : results) {
                    String name = ((String) nodeService.getProperty(xmlSousTypeNoderef, ContentModel.PROP_NAME)).replace(".xml", "");
                    List<Element> subtypes = new ArrayList<Element>();
                    ContentReader contentreader = contentService.getReader(xmlSousTypeNoderef, ContentModel.PROP_CONTENT);
                    if (contentreader != null) {
                        SAXReader saxreader = new SAXReader();
                        Document docXml = saxreader.read(contentreader.getContentInputStream());
                        Element rootElement = docXml.getRootElement();
                        if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                            Iterator<Element> itMetierSousType = rootElement.elementIterator("MetierSousType");
                            for (; itMetierSousType.hasNext(); ) {
                                subtypes.add(itMetierSousType.next());
                            }
                        }
                    }
                    toReturn.put(name, subtypes);
                }

            } else {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("Type/SubType not found.");
                }
                throw new RuntimeException("Type/Subtype not found.");
            }
        } catch (DocumentException de) {
            throw new RuntimeException("Error parsing XML document.", de);
        } catch (AccessDeniedException err) {
            throw new RuntimeException("Access denied to type node.", err);
        }
        return toReturn;
    }

    @Override
    public Map<String, List<String>> optimizedGetSavedType(boolean read, NodeRef bureau) {
        Map<String, List<String>> savedTypes = new HashMap<String, List<String>>();
        String currentUser = AuthenticationUtil.getRunAsUser();
        Collection<String> groupsList = authorityService.getAuthoritiesForUser(currentUser);
        ArrayList<String> parapheursList = new ArrayList<String>();
        if(!read && bureau != null) {
            parapheursList.add(bureau.toString());
        } else {
            for(NodeRef p : parapheurService.getOwnedParapheurs(currentUser)) {
                parapheursList.add(p.toString());
            }
        }
        SAXReader saxreader = new SAXReader();
        InputStream istreamSubtype;
        Document docXml;
        Element rootElementSubtype;

        try {

            List<NodeRef> subtypesNodes = parapheurService.getAllSavedXMLSousTypes();

            for(NodeRef subtypeNode : subtypesNodes) {

                List<String> savedSousTypes = new ArrayList<String>();

                ContentReader contentreader = contentService.getReader(subtypeNode, ContentModel.PROP_CONTENT);

                if (contentreader != null) {
                    logger.debug("getSavedXMLSousTypes:  contentreader Ok !");
                    istreamSubtype = contentreader.getContentInputStream(); // return contentreader.getContentString();

                    String typeName = (String) nodeService.getProperty(subtypeNode, ContentModel.PROP_NAME);

                    if (null == istreamSubtype) {
                        throw new DocumentException("cannot get access to " + typeName);
                    }
                    docXml = saxreader.read(istreamSubtype);
                    rootElementSubtype = docXml.getRootElement();
                    if (rootElementSubtype.getName().equalsIgnoreCase("MetierSousTypes")) {
                        Iterator<Element> itMetierSousType = rootElementSubtype.elementIterator("MetierSousType");
                        String stID;
                        for (Iterator<Element> ies = itMetierSousType; ies.hasNext(); ) {
                            Element msoustype = ies.next();
                            stID = msoustype.element("ID").getTextTrim();

                            if(read) {
                                if(isSoustypeVisible(msoustype, parapheursList, groupsList)) {
                                    savedSousTypes.add(stID);
                                }
                            } else if(isSoustypeWritable(msoustype, parapheursList, groupsList)) {
                                savedSousTypes.add(stID);
                            }
                        }
                    }
                    if(!savedSousTypes.isEmpty()) {
                        savedTypes.put(typeName.replace(".xml", ""), savedSousTypes);
                    }

                } else {
                    logger.warn("getSavedXMLSousTypes:  contentreader est NULL !");
                }
            }
        } catch (DocumentException ex) {
            logger.error("Exception parsing saved types XML document", ex);
        }

        return savedTypes;
    }

    @Override
    public List<NodeRef> getListeCalquesMultiDoc(String typeName, String subtypeName, int doccount) {
        List<NodeRef> results = new ArrayList<NodeRef>();
        ContentReader subtypesReader = contentService.getReader(
                getSubtypesNodeRef(typeName),
                ContentModel.PROP_CONTENT);
        try {
            SAXReader saxreader = new SAXReader();
            Document docXml = saxreader.read(subtypesReader.getContentInputStream());
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                Iterator<Element> itMetierSousType = rootElement.elementIterator("MetierSousType");
                for (Iterator<Element> ie = itMetierSousType; ie.hasNext();) {
                    Element msoustype = ie.next();
                    String tID = msoustype.element("ID").getText().trim();
                    if (tID.equals(subtypeName)) {
                        Element mCalques = msoustype.element("calques");
                        if (mCalques != null) {
                            Iterator<Element> calques = mCalques.elementIterator("calque");
                            while (calques.hasNext()) {
                                Element calque = calques.next();
                                if(calque.element("id") == null) {
                                    NodeRef calqueRef = new NodeRef(calque.getText().trim());
                                    results.add(calqueRef);
                                } else {
                                    NodeRef calqueRef = new NodeRef(calque.element("id").getText().trim());
                                    if(numDocIsForCalque(calque.element("numDocument").getText().trim(), doccount)) {
                                        results.add(calqueRef);
                                    }
                                }
                            }
                        }
                        /**
                         * else { // no 'calques' defined
                         }*
                         */
                        return results;
                    }
                }
                throw new IllegalArgumentException("Subtype " + subtypeName
                        + " doesn't exists for type '" + typeName + "'");
            }
            throw new IllegalStateException("Invalid XML file: missing 'MetierTypes' element");
        } catch (DocumentException ex) {
            throw new RuntimeException("Unexpected exception parsing XML document", ex);
        }
    }

    @Override
    public boolean isCompatibleWithMultidoc(@NotNull String type) {

        String protocole = getValueForType(type, TYPE_KEY_TDT, TYPE_KEY_PROTOCOLE);
        boolean isProtocoleHelios = (protocole != null) && protocole.contentEquals(TDT_PROTOCOLE_HELIOS);
        boolean isProtocoleActes = (protocole != null) && protocole.contentEquals(TDT_PROTOCOLE_ACTES);

        String signatureType = getValueForType(type, TYPE_KEY_SIGNATURE_FORMAT, null);
        boolean isSignatureCompatible = (signatureType != null)
                && (signatureType.contains("PAdES")
                || StringUtils.startsWithIgnoreCase(signatureType, "xades")
                || signatureType.contains("PKCS#7")
                || signatureType.contains("AUTO"));

        return (!isProtocoleHelios) && (!isProtocoleActes) && isSignatureCompatible;
    }

    @Override
    public @Nullable String getProtocol(@NotNull String typeId) {
        return getValueForType(typeId, TYPE_KEY_TDT, TYPE_KEY_PROTOCOLE);
    }

    @Override
    public @Nullable String getSignatureFormat(@NotNull String typeId) {
        return getValueForType(typeId, TYPE_KEY_SIGNATURE_FORMAT, null);
    }

    private boolean numDocIsForCalque(String numDocument, int doccount) {
        // On supprime les espaces...
        numDocument = numDocument.replace(" ", "");
        // Séparateur => virgule
        String[] toHandle = numDocument.split(",");
        try {
            // Pour chaque élément, séparé par une virgule
            for(String element : toHandle) {
                // On split sur le tiret... pour définir une plage
                String[] vars = element.split("-");
                // Le premier élément est forcément défini
                int firstElement = Integer.valueOf(vars[0]);
                // Le deuxième élément, non
                int lastElement = Integer.MAX_VALUE;
                if(vars.length == 2 && !vars[1].isEmpty()) {
                    lastElement = Integer.valueOf(vars[1]);
                }
                // Si le premier élément est 0 -> on accepte tous les documents
                // Si l'élément ne contient pas de tiret, il s'agit d'un numéro de page
                // Si l'élément contient un tiret, il s'agit d'une plage
                if(firstElement == 0 || (!element.contains("-") && doccount == firstElement) || (element.contains("-") && doccount >= firstElement && doccount <= lastElement)) {
                    return true;
                }
            }
        } catch(Exception e) {
            //Do nothing
        }
        return false;
    }

    @Override
    public List<Element> getTypesElements() {
        SAXReader saxreader = new SAXReader();
        List<Element> elements = new ArrayList<Element>();
        try {
            Document docXml = saxreader.read(parapheurService.getSavedXMLTypes());
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierTypes")) {
                Iterator<Element> itMetierType = rootElement.elementIterator("MetierType");
                for (Iterator<Element> ie = itMetierType; ie.hasNext(); ) {
                    elements.add(ie.next());
                }
            }
        } catch (DocumentException e) {
            logger.error("getTypeMetierProperties: Erreur sur parsing fichier XML", e);
            return null;
        }
        return elements;
    }

    @Override
    public Map<String, List<Element>> optimizedGetSavedTypeAsAdmin() {

        Map<String, List<Element>> savedTypes = new HashMap<String, List<Element>>();
        SAXReader saxreader = new SAXReader();
        InputStream istreamSubtype;
        Document docXml;
        Element rootElementSubtype;
        try {
            List<NodeRef> subtypesNodes = parapheurService.getAllSavedXMLSousTypes();
            for(NodeRef subtypeNode : subtypesNodes) {

                List<Element> savedSousTypes = new ArrayList<Element>();

                ContentReader contentreader = contentService.getReader(subtypeNode, ContentModel.PROP_CONTENT);

                if (contentreader != null) {
                    logger.debug("getSavedXMLSousTypes:  contentreader Ok !");
                    istreamSubtype = contentreader.getContentInputStream(); // return contentreader.getContentString();

                    String typeName = (String) nodeService.getProperty(subtypeNode, ContentModel.PROP_NAME);

                    if (null == istreamSubtype) {
                        throw new DocumentException("cannot get access to " + typeName);
                    }
                    docXml = saxreader.read(istreamSubtype);
                    rootElementSubtype = docXml.getRootElement();
                    if (rootElementSubtype.getName().equalsIgnoreCase("MetierSousTypes")) {
                        Iterator<Element> itMetierSousType = rootElementSubtype.elementIterator("MetierSousType");
                        for (Iterator<Element> ies = itMetierSousType; ies.hasNext(); ) {
                            savedSousTypes.add(ies.next());
                        }
                    }
                    if(!savedSousTypes.isEmpty()) {
                        savedTypes.put(typeName.replace(".xml", ""), savedSousTypes);
                    }

                } else {
                    logger.warn("getSavedXMLSousTypes:  contentreader est NULL !");
                }
            }
        } catch (DocumentException ex) {
            logger.error("Exception parsing saved types XML document", ex);
        }

        return savedTypes;
    }

    private boolean isSoustypeReadableOrWritable(Element msoustype, ArrayList<String> parapheursList, Collection<String> groupsList, boolean readable) {
        Attribute attrVisibility = msoustype.attribute("visibility" + (readable ? "Filter" : ""));

        boolean isPublic = false;
        boolean isVisible = false;
        List<String> aclParapheurs = new ArrayList<String>();
        List<String> aclGroups = new ArrayList<String>();

        //Si l'attribut n'existe pas, le sousType est public par défaut
        if (attrVisibility == null || "public".equals(attrVisibility.getText())) {
            isPublic = true;
        }

        if(!isPublic) {
            Element mAclParaph = msoustype.element("parapheurs" + (readable ? "Filters" : ""));
            if (mAclParaph != null) {
                Iterator<Element> parapheurs = mAclParaph.elementIterator("parapheur");
                while (parapheurs.hasNext()) {
                    Element parapheur = parapheurs.next();
                    aclParapheurs.add(parapheur.getTextTrim());
                }
            }

            Element mAclGroups = msoustype.element("groups" + (readable ? "Filters" : ""));
            if (mAclGroups != null) {
                Iterator<Element> groups = mAclGroups.elementIterator("group");
                while (groups.hasNext()) {
                    Element group = groups.next();
                    NodeRef groupRef = new NodeRef(group.getTextTrim());
                    if (nodeService.exists(groupRef)) {
                        String groupName = (String) nodeService.getProperty(groupRef, ContentModel.PROP_AUTHORITY_NAME);
                        aclGroups.add(groupName);
                    }
                }
            }

            isVisible = CollectionsUtils.containsOneOf(aclParapheurs, parapheursList) || CollectionsUtils.containsOneOf(aclGroups, groupsList);
        }

        return isPublic || isVisible;
    }

    private boolean isSoustypeWritable(Element msoustype, ArrayList<String> parapheursList, Collection<String> groupsList) {
        return isSoustypeReadableOrWritable(msoustype, parapheursList, groupsList, false);
    }

    private boolean isSoustypeVisible(Element msoustype, ArrayList<String> parapheursList, Collection<String> groupsList) {
        return isSoustypeReadableOrWritable(msoustype, parapheursList, groupsList, true);
    }

    @Override
    public boolean isTdtAuto(String type, String sousType) {
        String tdtAuto = getValueForSousType(type, sousType, SOUS_TYPE_KEY_TDT_AUTO);
        boolean isHelios = TDT_PROTOCOLE_HELIOS.equals(getValueForType(type, TYPE_KEY_TDT, TYPE_KEY_PROTOCOLE));
        return (isHelios && tdtAuto != null) && Boolean.parseBoolean(tdtAuto);
    }

    @Override
    public boolean isCachetAuto(String type, String sousType) {
        String cachetAuto = getValueForSousType(type, sousType, SOUS_TYPE_KEY_CACHET_AUTO);
        return (cachetAuto != null) && Boolean.parseBoolean(cachetAuto);
    }

    @Override
    public boolean hasToAttestSignature(String type, String sousType) {
        String hasToAttest = getValueForSousType(type, sousType, SOUS_TYPE_KEY_ATTEST);
        return Boolean.parseBoolean(hasToAttest);
    }


    protected NodeRef getTypesNodeRef() {
        String xpath = "/app:company_home/app:dictionary/cm:metiertype/cm:TypesMetier.xml']";

        List<NodeRef> results = searchService.selectNodes(
                nodeService.getRootNode(storeRef),
                xpath,
                null,
                namespaceService,
                false);

        if (results.isEmpty()) {
            throw new IllegalArgumentException("Impossible de trouver le noeud des types dans : " + xpath);
        }

        return results.get(0);
    }

    protected NodeRef getSubtypesNodeRef(String type) {
        String xpath = "/app:company_home/app:dictionary/cm:metiersoustype"
                + "/*[@cm:name='" + type + ".xml']";

        List<NodeRef> results = searchService.selectNodes(
                nodeService.getRootNode(storeRef),
                xpath,
                null,
                namespaceService,
                false);

        if (results.isEmpty()) {
            throw new IllegalArgumentException("Le type '" + type + "' n'existe pas ou a été supprimé");
        }

        return results.get(0);
    }

    protected @Nullable String getValueForType(String type, String key, String subKey) {
        ContentReader subtypesReader = contentService.getReader(
                getTypesNodeRef(),
                ContentModel.PROP_CONTENT);

        try {
            SAXReader saxreader = new SAXReader();
            Document docXml = saxreader.read(subtypesReader.getContentInputStream());
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierTypes")) {
                Iterator<Element> itMetierType = rootElement.elementIterator("MetierType");
                for (Iterator<Element> ie = itMetierType; ie.hasNext();) {
                    Element mtype = ie.next();
                    String tID = mtype.element("ID").getText().trim();
                    if (tID.equals(type)) {
                        Element element = mtype.element(key);

                        if (element == null) {
                            return null;
                        }
                        else if (subKey == null) {
                            return element.getTextTrim();
                        }
                        else {
                            return element.element(subKey).getTextTrim();
                        }
                    }
                }
                throw new IllegalArgumentException("Type " + type + " doesn't contains value for keys '" + key + "'/'"+ subKey +"'");
            }
            throw new IllegalStateException("Invalid XML file: missing 'MetierTypes' element");
        } catch (DocumentException ex) {
            throw new RuntimeException("Unexpected exception parsing XML document", ex);
        }
    }


    protected String getValueForSousType(String type, String sousType, String key) {

        ContentReader subtypesReader = contentService.getReader(
                getSubtypesNodeRef(type),
                ContentModel.PROP_CONTENT);

        try {
            SAXReader saxreader = new SAXReader();
            Document docXml = saxreader.read(subtypesReader.getContentInputStream());
            Element rootElement = docXml.getRootElement();
            if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                Iterator<Element> itMetierType = rootElement.elementIterator("MetierSousType");
                for (Iterator<Element> ie = itMetierType; ie.hasNext();) {
                    Element mtype = ie.next();
                    String tID = mtype.element("ID").getText().trim();
                    if (tID.equals(sousType)) {
                        Element element = mtype.element(key);
                        return (element == null)?
                                null :
                                element.getTextTrim();
                    }
                }
                throw new IllegalArgumentException("Le sous-type " + sousType + " n'existe pas pour le type '" + type + "'");
            }
            throw new IllegalStateException("Invalid XML file: missing 'MetierSousTypes' element");
        } catch (DocumentException ex) {
            throw new RuntimeException("Unexpected exception parsing XML document", ex);
        }
    }

    /**
     * Adds a subtitle value of the given local name.
     * If a value already exists for the given name it will be replaced.
     *
     * @param subtype "*" to match every subtypes.
     * @param key node will be created if Key doesn't exists yet
     * @param value if null, deletes the value
     */
    @SuppressWarnings("unchecked")
    public void setValueForSousType(@NotNull String type, @NotNull String subtype, @NotNull String key, @Nullable String value) {

        NodeRef subtypesNode = getSubtypesNodeRef(type);
        ContentReader subtypesReader = contentService.getReader(subtypesNode, ContentModel.PROP_CONTENT);

        try {
            SAXReader saxreader = new SAXReader();
            Document docXml = saxreader.read(subtypesReader.getContentInputStream());
            Element rootElement = docXml.getRootElement();

            if (rootElement.getName().equalsIgnoreCase("MetierSousTypes")) {
                Iterator<Element> subtypeIterator = rootElement.elementIterator("MetierSousType");

                while (subtypeIterator.hasNext()) {
                    Element subtypeElement = subtypeIterator.next();
                    String subtypeId = subtypeElement.element("ID").getText().trim();

                    if (subtype.contentEquals("*") || subtypeId.contentEquals(subtype)) {

                        if (subtypeElement.element(key) == null) {
                            subtypeElement.addElement(key);
                        }

                        if (value != null) {
                            subtypeElement.element(key).setText(value);
                        } else {
                            subtypeElement.element(key).detach();
                        }
                    }
                }
            }
            else {
                throw new IllegalStateException("Invalid XML file: missing 'MetierSousTypes' element");
            }

            StringWriter out = new StringWriter(1024);
            XMLWriter writerTmp = new XMLWriter(OutputFormat.createPrettyPrint());
            writerTmp.setWriter(out);
            writerTmp.write(docXml);
            String xmlData = out.toString();

            ContentWriter subtypesWriter = contentService.getWriter(subtypesNode, ContentModel.PROP_CONTENT, true);
            subtypesWriter.setMimetype("text/xml");
            subtypesWriter.setEncoding("UTF-8");
            subtypesWriter.putContent(xmlData);
        }
        catch (DocumentException ex) {
            throw new RuntimeException("Unexpected exception parsing XML document", ex);
        }
        catch (IOException ex) {
            throw new RuntimeException("Unexpected exception writing XML document", ex);
        }

    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setRulesService(RulesService rulesService) {
        this.rulesService = rulesService;
    }

    public void setStoreRef(StoreRef storeRef) {
        this.storeRef = storeRef;
    }

    public void setStore(String storeRef) {
        this.storeRef = new StoreRef(storeRef);
    }
}
