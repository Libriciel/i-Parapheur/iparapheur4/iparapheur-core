package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: lhameury
 * Date: 29/08/13
 * Time: 10:45
 */
public class CorbeilleTraitesPatch extends XPathBasedPatch {
    public static final String PARAPHEURS_XPATH_QUERY = "/app:company_home/ph:parapheurs/*";

    @Override
    protected String getXPathQuery() {
        return PARAPHEURS_XPATH_QUERY;
    }

    @Override
    protected void patch(NodeRef node) {
        NodeService nodeService = getNodeService();

        if (nodeService.getChildAssocs(node, ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, ParapheurModel.NAME_TRAITES).isEmpty()) {
            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, "Dossiers traités");
            properties.put(ContentModel.PROP_TITLE, "Dossiers traités");
            properties.put(ContentModel.PROP_DESCRIPTION, "Dossiers qui ont été traités.");
            try {
                NodeRef corbeille = nodeService.createNode(node, ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_TRAITES,
                        ParapheurModel.TYPE_CORBEILLE_VIRTUELLE, properties).getChildRef();
                nodeService.setProperty(corbeille, ParapheurModel.PROP_CHILD_COUNT, 0);
                nodeService.setProperty(corbeille, ParapheurModel.PROP_CORBEILLE_LATE, false);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }


}
