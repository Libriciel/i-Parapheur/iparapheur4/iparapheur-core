package com.atolcd.parapheur.repo.patch;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 17/07/12
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */
public class MailTemplateAddPatch extends XPathBasedPatch {
    private Properties configuration;
    private ContentService contentService;

    private String templateFilename;

    private static Log logger = LogFactory.getLog(MailTemplateAddPatch.class);

    public MailTemplateAddPatch(String templateFilename) {
        this.templateFilename = templateFilename;
    }


    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public String getTemplateFilename() {
        return templateFilename;
    }

    @Override
    protected String getXPathQuery() {

        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/"
                + this.configuration.getProperty("spaces.dictionary.childname") + "/"
                + this.configuration.getProperty("spaces.templates.email.childname");
        return xpath;
    }

    @Override
    protected void patch(NodeRef nodeToPatch) throws Exception {
        NodeService nodeService = getNodeService();
        SearchService searchService = getSearchService();
        NamespaceService namespaceService = getNamespaceService();


        String xpath = String.format("app:company_home/app:dictionary/app:email_templates/cm:%s", getTemplateFilename());
        List<NodeRef> results = searchService.selectNodes(getRootNode(),
                xpath,
                null,
                namespaceService,
                false);

        logger.warn(String.format("Nodes Found matching template %s == %s", getTemplateFilename(), results));

        if (results == null || results.isEmpty()) {

            InputStream viewStream = getClass().getClassLoader().getResourceAsStream(String.format("alfresco/module/parapheur/bootstrap/%s", getTemplateFilename()));

            HashMap<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, getTemplateFilename());
            properties.put(ContentModel.PROP_TITLE, getTemplateFilename());

            NodeRef child = nodeService.createNode(nodeToPatch, ContentModel.ASSOC_CONTAINS, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, getTemplateFilename()), ContentModel.TYPE_CONTENT, properties).getChildRef();

            ContentWriter writer = contentService.getWriter(child, ContentModel.PROP_CONTENT, true);

            writer.setEncoding("UTF-8");
            writer.setMimetype("text/plain");
            writer.putContent(viewStream);

            logger.warn("The new email template node was succesfully created");
        }

    }


}
