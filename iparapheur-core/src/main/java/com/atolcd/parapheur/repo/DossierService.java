/*
 * Version 3.4
 * CeCILL Copyright (c) 2012-2014, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 * 
 */
package com.atolcd.parapheur.repo;

import com.atolcd.parapheur.repo.content.Document;
import com.atolcd.parapheur.repo.impl.DossierServiceImpl;
import com.atolcd.parapheur.repo.impl.S2lowServiceImpl;
import com.atolcd.parapheur.web.bean.wizard.batch.Predicate;
import org.adullact.spring_ws.iparapheur._1.LogDossier;
import org.alfresco.service.Auditable;
import org.alfresco.service.NotAuditable;
import org.alfresco.service.PublicService;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 30/08/12
 * Time: 11:02
 * @author Emmeanuel Peralta - ADULLACT Projet
 */
@PublicService
public interface DossierService {

    enum ACTION_DOSSIER {
        REMORD,
        SUPPRESSION,
        RAZ,
        EDITION,
        SECRETARIAT,
        REJET,
        ARCHIVAGE,
        VISA,
        TDT_HELIOS,
        TDT_ACTES,
        MAILSEC,
        SIGNATURE,
        ENREGISTRER,
        EMAIL,
        JOURNAL,
        TRANSFERT_ACTION,
        AVIS_COMPLEMENTAIRE,
        ENCHAINER_CIRCUIT,
        TRANSFORM,
        GET_ATTEST,
        SIGN_INFO,
        CACHET,
        CORBEILLE_EVENT
    }

    // <editor-fold desc="Actions">

    //Creation de dossier
    @NotAuditable
    public NodeRef beginCreateDossier(NodeRef parapheur);

    /**
     * Crée un nouveau dossier dans un parapheur (dans la corbeille "en préparation").
     * Le nom du dossier est récupéré de la propriété <code>cm:name</code> passée en argument.
     *
     * @param parapheur  le parapheur dans lequel créer le dossier
     * @param properties les propriétés du dossier
     * @return le NodeRef du dossier qui vient d'être créé
     * @throws IllegalArgumentException si <code>parapheur</code> ne représente pas un parapheur
     *                                  ou si <code>properties</code> ne contient pas de propriété
     *                                  <code>cm:name</code>.
     */
    @Auditable(parameters = {"parapheur", "properties"})
    public abstract NodeRef createDossier(NodeRef parapheur,
                                          Map<QName, Serializable> properties) throws FileExistsException;

    @NotAuditable
    public NodeRef addSignature(NodeRef dossier, Document doc);

    @NotAuditable
    public String finalizeCreateDossier(NodeRef dossierRef);

    boolean isPesViewEnabled();

    /**
     * Vérifie si la finalization du dossier est possible
     *
     * @param ref Dossier à vérifier
     * @return Erreur sous forme de string permettant l'i18n, "success" en cas
     * de réussite
     */
    @NotAuditable
    @NotNull String checkFinalize(NodeRef ref);

    @NotAuditable
    public void finalizeDossier(NodeRef dossier);

    @NotAuditable
    public NodeRef addDocumentPrincipal(NodeRef dossierRef, Document doc, Document visudoc)
            throws Exception;

    @NotAuditable
    public NodeRef setVisuelDocument(NodeRef dossierRef, NodeRef document, Document visuel)
            throws Exception;

    @NotAuditable
    public S2lowServiceImpl.SecureMailDetail getInfosMailSec(NodeRef dossierRef);

    @NotAuditable
    public NodeRef replaceDocumentPrincipal(NodeRef dossierRef, Document doc, Document visudoc) throws Exception;

    int getMaxMainDocuments();

    @NotAuditable
    public boolean removeDocument(NodeRef document, NodeRef dossier, NodeRef bureauCourant);

    @NotAuditable
    public String setDossierProperties(NodeRef dossier, Map<String, Serializable> properties);

    @NotAuditable
    public void setDossierQNameProperties(NodeRef dossier, Map<QName, Serializable> originalProperties);

    @Auditable(parameters = {"dossierRef", "parapheur", "properties"})
    @NotNull String setDossierPropertiesWithAuditTrail(NodeRef dossierRef, NodeRef parapheur, Map<QName, Serializable> properties, boolean onlyAudit);

    @NotAuditable
    boolean hasToAcceptLockedPdf();

    @NotAuditable
    public NodeRef addAnnexe(NodeRef dossierRef, Document doc, Document realdoc) throws Exception;

    NodeRef getCircuitRef(NodeRef dossier, String type, String sousType);

    @NotAuditable
    public void setCircuit(NodeRef dossierRef, String type, String sousType);

    @NotAuditable
    public List<NodeRef> getArchives(int count, int index, List<Predicate<?, ?>> filters, String propSort, boolean asc);

    @NotAuditable
    public List<NodeRef> getDossiers(NodeRef bureauRef, String parentName, int count, int index, List<Predicate<?, ?>> filters, String propSort, boolean asc, int numPage, int pendingFile);

    @NotAuditable
    public List<NodeRef> searchFolder(List<Predicate<?, ?>> filters);

    /**
     * Permet l'envoie d'un mail avec le dossier passé en paramètre en pièce
     * jointe
     *
     * @param dossiers Le dossier à joindre
     * @param destinataires Le(s) destinataire(s)
     * @param objet L'objet du/des mail(s)
     * @param message Le message du/des mail(s)
     * @param includedAnnexes indique si les annexes doivent être incluses dans le pdf joint
     * @param includeFirstPage indique si le bordereau de signature doit être inclu dans le pdf joint
     */
    @NotAuditable
    public void sendMail(List<NodeRef> dossiers, List<String> destinataires, String objet, String message, List<NodeRef> attachments, boolean includedAnnexes, boolean includeFirstPage);

    @NotAuditable
    public void sendMailSec(NodeRef bureauCourant, List<NodeRef> dossiers, List<String> destinataires, List<String> destinatairesCC,
            List<String> destinatairesCCI, String objet, String message, String password, boolean showPass, List<NodeRef> includedAnnexes,
            boolean annexesIncluded);

    @NotAuditable
    public byte[] print(NodeRef dossier, List<NodeRef> includedAnnexes, boolean includeFirstPage) throws FileNotFoundException, IOException, Exception;

    //Actions générales
    @NotAuditable
    public Map<String, String> deleteNodes(List<NodeRef> filesToDelete);

    @NotAuditable
    public Map<String, String> rejectDossier(List<NodeRef> filesToReject, String pub, String priv, NodeRef bureauCourant);

    @NotAuditable
    public Map<String,String> signerDossier(List<NodeRef> _filesToSign, String pub, String priv, NodeRef bureauCourant, List<String> signature) throws Exception;

    @NotAuditable
    public Map<String, String> visaDossier(List<NodeRef> _filesToVisa, String pub, String priv, NodeRef bureauCourant, boolean consecutiveSteps);

    @NotAuditable
    public String archiveDossier(NodeRef fileToArchive, String titreArchive, List<NodeRef> annexes);

    @NotAuditable
    public Map<String, String> archiveDossier(List<NodeRef> filesToArchive, List<String> nomArchive, NodeRef bureauCourant, List<Boolean> includeAttachments);

    @NotAuditable
    public Map<String, String> RAZDossier(List<NodeRef> filesToRAZ, NodeRef bureauCourant);

    @NotAuditable
    public Map<String, String> remorseDossier(List<NodeRef> filesToRemorse, NodeRef bureauCourant);

    @NotAuditable
    public Map<String,String> secretariat(List<NodeRef> _filesToSec, String pub, String priv, NodeRef bureauCourant);

    // </editor-fold desc="Actions">

    // <editor-fold desc="Test actions">

    @NotAuditable
    public boolean canRemoveDoc(NodeRef document, NodeRef dossier, NodeRef bureauCourant);

    @NotAuditable
    public boolean canRemorse(NodeRef ref, NodeRef bureauCourant);

    @NotAuditable
    public boolean canDelete(NodeRef ref);

    @NotAuditable
    public boolean canReject(NodeRef ref, NodeRef bureauCourant);

    @NotAuditable
    public boolean canSign(NodeRef ref, NodeRef bureauCourant);

    @NotAuditable
    public boolean canRAZ(NodeRef ref, NodeRef bureauCourant);

    @NotAuditable
    public boolean canEdit(NodeRef ref, NodeRef bureauCourant);

    @NotAuditable
    public boolean canArchive(NodeRef ref, NodeRef bureauCourant);

    @NotAuditable
    public boolean canSecretariat(NodeRef ref, NodeRef bureauCourant);

    // </editor-fold desc="Test actions">

    boolean isNextEtapeXadesSig(NodeRef dossier);

    @NotAuditable
    public Map<String, String> getSignatureInformations(NodeRef dossier, NodeRef ref);

    @NotAuditable
    public Map<String, String> generateSignatureInformations(NodeRef dossier, NodeRef ref);

    @NotAuditable
    public List<LogDossier> getLogsDossier(NodeRef dossierRef);

    @NotAuditable
    public String getPesPreviewUrl(NodeRef nodeRef);

    String getPesPreviewUrlFromArchive(NodeRef nodeRef);

    @NotAuditable
    public boolean isPesViewEnabled(NodeRef dossier);

    @NotAuditable
    public void sendTdtActes(NodeRef bureauCourant, NodeRef dossierRef, String nature, String classification, String numero, Date date, String objet, String annotPub, String annotPriv) throws Exception;

    @NotAuditable
    void sendTdtHelios(NodeRef bureauCourant, NodeRef dossierRef, String annotPub, String annotPriv) throws Exception;

    @NotAuditable
    void doStamp(NodeRef bureauCourant, NodeRef dossierRef, String annotPub, String annotPriv, boolean automatic) throws Exception;

    /**
     * Indique si l'utilisateur passé en parapmètre a lu le dossier passé en paramètre.
     * C'est-à-dire s'il a lu les documents principaux.
     * @param dossier
     * @param username
     * @return vrai si l'utilisateur a lu le dossier.
     */
    @NotAuditable
    boolean hasReadDossier(NodeRef dossier, String username);

    /**
     * Indique si l'utilisateur passé en parapmètre a lu le document passé en paramètre
     * @param document
     * @param username
     * @return vrai si l'utilisateur a lu le document.
     */
    @NotAuditable
    boolean hasReadDocument(NodeRef document, String username);

    /**
     * Permet d'indiquer que l'utilisateur passé en paramètre à lu le document
     * passé en paramètre.
     * @param document
     * @param lecteur
     */
    @NotAuditable
    public void setDocumentLu(NodeRef document, String lecteur);

    /**
     * Permet d'indiquer l'utilisateur passé en paramètre a lu le dossier
     * (tous les documents du dossier).
     * @param dossier
     * @param lecteur
     */
    @NotAuditable
    public void setDossierLu(NodeRef dossier, String lecteur);

    /**
     * Marque le dossier passé en paramètre comme non lu (tous ces documents
     * seront non lus).
     * @param dossier
     */
    @NotAuditable
    public void setDossierNonLu(NodeRef dossier);

    /**
     * Renvoie vrai si l'utilisateur n'est pas le seul à avoir lu le document.
     * @param document
     * @param username
     * @return vrai si l'utilisateur n'est pas le seul à avoir lu le document.
     */
    @NotAuditable
    public boolean didSomeoneElseReadDocument(NodeRef document, String username);

    /**
     * Renvoie la liste des étapes de visa consécutives que
     * l'utilisateur passé en paramètre peut traiter (en partant de l'étape
     * courante) incluant les étapes en délégation.
     * Nb : les étapes de signatures ne sont pas considérées.
     * Renvoie une liste vide si l'utilisateur n'a pas le droit de traiter
     * l'étape courante ou si le dossier n'est pas émis.
     * @param dossier
     * @param user
     * @return la liste des étapes consécutive que user peut traiter en
     * visa depuis l'étape courante.
     */
    @NotAuditable
    public List<EtapeCircuit> getConsecutiveSteps(NodeRef dossier, String user);

    /**
     * Indique si le document passé en paramètre est un document principal.
     * @param document
     * @return true si document est un document principal.
     */
    @NotAuditable
    public boolean isDocumentPrincipal(NodeRef document);

    /**
     * Supprime les images générées pour ce document. Les images sont utilisées
     * pour la visualisation du document principal en V4.
     * @param document  le document concerné
     * @param dossier  le dossier concerné
     */
    @NotAuditable
    public void deleteImagesPreviews(NodeRef document, NodeRef dossier);

    /**
     * Supprime les fichiers flash créés pour ce document. Les swf sont utilisés
     * pour la visualisation du document principal en V3.
     * @param document le document principal du dossier concerné
     */
    @NotAuditable
    public void deleteSwfPreviews(NodeRef document);

    /**
     * Supprime les fichiers flash créés pour ce dossier. Les swf sont utilisés
     * pour la visualisation du document principal en V3.
     * @param dossier le dossier concerné
     */
    @NotAuditable
    public void deleteSwfPreviewsForDossier(NodeRef dossier);

    /**
     * Retourne le NodeRef du noeud contenant les preview flash pour le dossier
     * passé en paramètre.
     * @param dossier le dossier concerné.*
     * @param createIfNotExists Si vrai et si le noeud contenant les previews
     * n'existe pas, alors le noeud est créé, si faux ne fait rien et renvoie
     * null.
     * @return Retourne le nodeRef du noeud contenant les preview flash du dossier.
     */
    @NotAuditable
    public NodeRef getPreviewNode(NodeRef dossier, boolean createIfNotExists);

    /**
     * Retourne le NodeRef du noeud contenant les previews flash.
     * @return le NodeRef du noeud contenant les previews flash.
     */
    @NotAuditable
    public NodeRef getPreviewsNode();

    /**
     * Supprime le dossier passé en paramètre. Si des previews flash ont été
     * créées pour ce dossier, elles seront supprimées.
     * @param dossier le dossier à supprimer.
     * @param notify doit on notifier l'utilisateur propriétaire
     */
    @NotAuditable
    public void deleteDossier(NodeRef dossier, boolean notify);

    /**
     * Suppression du dossier par un administrateur.
     * Supprime le dossier passé en paramètre. Si des previews flash ont été
     * créées pour ce dossier, elles seront supprimées.
     * On notifie l'émetteur du dossier par mail direct avec le dossier en pièce-jointe.
     *
     * @param dossier le dossier à supprimer.
     */
    @NotAuditable
    void deleteDossierAdmin(NodeRef dossier);

    /**
     * Place les permissions sur le dossier (reset)
     * @param dossier
     */
    @NotAuditable
    public void setPermissionsDossier(NodeRef dossier);

    /**
     * Place les permissions sur l'archive en fonction des permission du dossier.
     * @param archive l'archive sur laquelle les permissions sont placées.
     * @param dossier le dossier à archiver.
     */
    @NotAuditable
    public void setPermissionsArchive(NodeRef archive, NodeRef dossier);

    /**
     * Positionne les permission pour les dossiers confidentiels (et groupe du coup)
     * @param dossier
     */
    @NotAuditable
    public void setConfidentialPermissionsForDossier(NodeRef dossier);

    /**
     * Renvoie le dossier (cm:Folder) contenant les aperçus png du document passé en paramètre.
     * @param dossier le ph:dossier contenant le document
     * @param document
     * @param createIfNotExists si vrai, créé le dossier (cm:Folder) s'il n'existe pas.
     * @return le dossier (cm:Folder) contenant les aperçus png du document passé en paramètre.
     */
    @NotAuditable
    public NodeRef getDocumentImagesFolder(NodeRef dossier, NodeRef document, boolean createIfNotExists);

    /**
     * Renvoie le nodeRef de la page d'aperçu numéro 'page' du document fourni en paramètre. Si la page d'aperçu n'est
     * pas encore générée, on la génère.
     * @param dossier Dossier sur lequel se trouve le document
     * @param document Document sur lequel générer la page
     * @param page Numéro de page à générer
     * @return Le NodeRef de l'image.
     */
    @NotAuditable
    public NodeRef getImageForDocument(NodeRef dossier, NodeRef document, int page) throws Exception;

    /**
     * Récupération du nombre de pages dans le document PDF passé en paramètre.
     * @param document Le document sur lequel récupérer le nombre de pages.
     * @return le nombre de page du document.
     */
    @NotAuditable
    public int getPageCountForDocument(NodeRef document);

    /**
     * Indique si le dossier a été lu par un acteur de l'étape courante
     * @param dossier le dossier à tester
     * @return true si lu, false sinon
     */
    @NotAuditable
    boolean isDossierLu(NodeRef dossier);

    void readDossier(NodeRef dossier, String username);

    /**
     * Retourne la liste des types MIME autorises
     */
    @NotAuditable
    List<String> getAuthorizedMimeTypeList();

    @NotAuditable
    List<String> getCorbeillesParent(NodeRef dossier, NodeRef bureauCourant);

    List<NodeRef> getCircuit(NodeRef dossier);

    String getActionDemandee(NodeRef dossier);

    /**
     * Renvoie la liste des actions faisable sur le dossier en fonction du bureau utilisé pour atteindre ce dossier.
     * @param dossier le dossier sur lequel les actions renvoyées sont possibles
     * @param bureau le bureau avec lequel on peut faire ces actions sur le dossier
     * @return la liste des actions faisable sur le dossier en fonction du bureau utilisé pour atteindre ce dossier.
     */
    @NotAuditable
    List<ACTION_DOSSIER> getActions(NodeRef dossier, NodeRef bureau);

    boolean isCurrentCachetEtapeAuto(NodeRef dossier);

    @NotAuditable
    DossierServiceImpl.SignatureInformations getSignatureInformations(NodeRef dossierRef, String annotPub, String annotPriv, NodeRef bureauCourant);

    boolean isPdfProtected(Document doc) throws Exception;

    /**
     * Indique si l'étape courante du dossier est automatique.
     * Pour l'instant, seule une étape de tdt vers helios peut être automatique.
     * @param dossier
     */
    boolean isCurrentTDTEtapeAuto(NodeRef dossier);

    /**
     * Indique si l'étape précédente du dossier est la dernière étape de signature.
     * @param dossier
     */
    boolean isPrevEtapeLastSignature(NodeRef dossier);

    /**
     * Renvoi vrai si le dossier a été émis (première étape du circuit validée).
     * @param dossier
     * @return vrai si le dossier est émis.
     */
    boolean isEmis(NodeRef dossier);

    /**
     * Réorganisation des documents.
     * Les documents principaux sont replacés en premier dans la liste, suivi des annexes
     * @param dossier Le NodeRef du dossier concerné
     * @param documents La liste des documents à ordonner
     */
    void setOrderDocuments(NodeRef dossier, List<Map<String, Serializable>> documents);

    /**
     * Réorganisation des documents.
     * Les documents principaux sont replacés en premier dans la liste, suivi des annexes
     * @param dossier Le NodeRef du dossier concerné
     */
    void reorderDocuments(NodeRef dossier);

    /**
     * Adds a PAdES signature position, overriding the #signature# and the PAdES properties ones.
     */
    void addCustomSignatureToDocument(NodeRef dossier, NodeRef document, int x, int y, int width, int height, int page);

    /**
     * Deletes the document's metadata, leaving the #signature# and the PAdES properties positions by default.
     */
    void deleteCustomSignatureFromDocument(NodeRef dossier, NodeRef document);

    /**
     * Returns the current signature position (custom if exists, or #signature# if exists, or PAdES properties position).
     * @return A stringed JSON (x,y,width,height...), or an error message.
     */
    String getCustomSignaturesJsonPosition(NodeRef dossier, boolean cachet);
}
