package com.atolcd.parapheur.repo.annotations;


import java.util.Date;
import java.util.UUID;

/**
 * Annotation
 * @author Emmanuel Peralta
 */
public interface Annotation {

    public UUID getUUID();
    public void setUUID(UUID uuid);

    public String getType();
    public String getAuthor();
    public Date getDate();

    public void updateDate();
    public int getPage();


    public String getPenColor();
    public void setPenColor(String penColor);

    public String getFillColor();
    public void setFillColor(String fillColor);

    public Rect getRect();
    public void setRect(Rect rect);

    public String getText();
    public void setText(String text);

    public String getPicto();
    public void setPicto(String picto);
    
    public boolean isSecretaire();
    public void setSecretaire(boolean secretaire);
}
