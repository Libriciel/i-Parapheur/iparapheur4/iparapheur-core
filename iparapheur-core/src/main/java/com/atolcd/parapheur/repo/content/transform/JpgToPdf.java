/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.repo.content.transform;

import java.io.InputStream;

import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.transform.AbstractContentTransformer2;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.TransformationOptions;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

public class JpgToPdf extends AbstractContentTransformer2
{
   
   @Override
   protected void transformInternal(ContentReader reader, ContentWriter writer, TransformationOptions to) throws Exception {
      PDDocument doc = null;
      InputStream is = null;
      PDXObjectImage image = null;
      
      try
      {
         // Nouveau document PDF
         doc = new PDDocument();
         
         // On ajoute une page au format A4 21x29.7 
         PDRectangle format = new PDRectangle(595.0f, 842.0f);
         PDPage page = new PDPage();
         page.setMediaBox(format);
         doc.addPage(page);
         
         // Ouverture du contenu Jpg
         is = reader.getContentInputStream();
         image = new PDJpeg(doc,is);
         
         // Ecriture dans le document PDF
         PDPageContentStream contentStream = new PDPageContentStream(doc, page);
         int height = image.getHeight();
         int width = image.getWidth();
         
         // Redimensionnement
         double imageRatio = (double)width/(double)height;
         
         int deltaW = (int) ((width - 595) * imageRatio);
         int deltaH = (int) ((height - 842) * imageRatio);
         
         if (deltaW > 0 && deltaW > deltaH)
         {
            width = 595;
            height = (int)(595/imageRatio);
         }
         else if (deltaH > 0 && deltaH > deltaW)
         {
            height = 842;
            width = (int)(842*imageRatio);
         }
         
         contentStream.drawXObject(image, (595-width)/2, (842-height)/2, width, height);
         contentStream.close();
         
         // Sauvegarde dans le flux de sortie
         doc.save(writer.getContentOutputStream());
         
      }
      finally
      {
         if (doc != null)
         {
            try
            {
               doc.close();
            }
            catch (Throwable e)
            {
               e.printStackTrace();
            }
         }
         if (is != null)
         {
            try
            {
               is.close();
            }
            catch (Throwable e)
            {
               e.printStackTrace();
            }
         }
      }
   }
   
   public double getReliability(String sourceMimetype, String targetMimetype)
   {
      if (!MimetypeMap.MIMETYPE_IMAGE_JPEG.equals(sourceMimetype)
            || !MimetypeMap.MIMETYPE_PDF.equals(targetMimetype))
      {
         // supporte seulement JPG --> PDF
         return 0.0;
      }
      
      else
      {
         return 1.0;
      }
   }


    public boolean isTransformable(String string, String string1, TransformationOptions to) {
        if (!MimetypeMap.MIMETYPE_IMAGE_JPEG.equals(string)
                || !MimetypeMap.MIMETYPE_PDF.equals(string1)) {
            return false;
        }
        else {
            return true;
        }
    }
   
}
