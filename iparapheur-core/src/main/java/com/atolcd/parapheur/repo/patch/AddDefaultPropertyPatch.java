/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import java.io.Serializable;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;

/**
 * Utilisé pour définir une propriété à tous les dossiers
 * Définir d'autre constructeurs afin de gérer d'autres types de propriétés
 * @author Lukas Hameury <lukas.hameury@adullact-projet.coop>
 */
public class AddDefaultPropertyPatch extends XPathBasedPatch {
    public static final String SUBTYPES_XPATH_QUERY = "//*[subtypeOf('ph:dossier')]";
    
    private int nbDossier;
    private QName property;
    private Serializable defaultValue;
    
    /**
     * Constructeur pour propriété de type Boolean
     * @param property le nom de la propriété (sans le préfix 'ph:')
     * @param defaultValue la valeur à positionner
     */
    public AddDefaultPropertyPatch(String property, Boolean defaultValue) {
        this.nbDossier = 1;
        this.property = QName.createQName(ParapheurModel.PARAPHEUR_MODEL_URI, property);
        this.defaultValue = defaultValue;
    }
    
    private void patchNode(NodeRef node) {
        if(nbDossier%100 == 0) {
            System.out.println("AddDefaultPropertyPatch - Dossiers traités : " + nbDossier);
        }
        NodeService nodeService = getNodeService();
        if (nodeService.getProperty(node, property) == null) {
            nodeService.setProperty(node, property, defaultValue);
        }
        nbDossier++;
    }

    @Override
    protected String getXPathQuery() {
        return SUBTYPES_XPATH_QUERY;
    }

    @Override
    protected void patch(NodeRef nodeToPatch) {
        patchNode(nodeToPatch);
    }

    public Serializable getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Serializable defaultValue) {
        this.defaultValue = defaultValue;
    }

    public QName getProperty() {
        return property;
    }

    public void setProperty(QName property) {
        this.property = property;
    }
}
