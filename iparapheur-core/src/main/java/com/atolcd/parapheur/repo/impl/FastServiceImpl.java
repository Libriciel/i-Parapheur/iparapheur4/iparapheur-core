/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2012, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurException;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.FastHeliosQuartzJob;
import com.atolcd.parapheur.repo.MetadataService;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Calendar;
import javax.transaction.Status;
import javax.transaction.UserTransaction;
import javax.xml.transform.TransformerException;
import org.adullact.iparapheur.tdt.s2low.TransactionStatus;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.filestore.FileContentWriter;
import org.alfresco.repo.domain.node.ContentDataWithId;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.ssl.KeyMaterial;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.client.methods.DeleteMethod;
import org.apache.jackrabbit.webdav.client.methods.PropFindMethod;
import org.apache.jackrabbit.webdav.client.methods.PutMethod;
import org.apache.jackrabbit.webdav.property.DavProperty;
import org.apache.jackrabbit.webdav.property.DavPropertyName;
import org.apache.jackrabbit.webdav.property.DavPropertySet;
import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.apache.xpath.XPathAPI;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.quartz.*;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Implementation connecteur vers TDT CDC-FAST protocole HELIOS
 * 
 * @author Sabrina Toulouse - ADULLACT Projet
 * @author Emmanuel Peralta - ADULLACT Projet
 * @author Stephane Vast - ADULLACT Projet
 * @author Jason Maire - ADULLACT Projet
 */
public class FastServiceImpl implements S2lowService, InitializingBean {

    private static final Logger logger = Logger.getLogger(S2lowService.class);

    public static String PROP_TDT_NOM_FAST = "FAST";

    /**
     * Cette location de base sert à créer l'URL web-dav pour FAST, selon
     * template :
     * https://<server><URL_HELIOS_IMPORTER_FICHIER><collectivite>/in/
     * pour obtenir une URL du genre:
     * https://demo-parapheur.cdcfast.fr/partage/webdav/clients/221500010/in/
     */
    // Sabrina Toulouse: private static final String URL_HELIOS_IMPORTER_FICHIER = "/partage/webdav/clients/221500010/in/";
    private static final String URL_HELIOS_IMPORTER_FICHIER = "/partage/webdav/clients/";
//    private static final String URL_HELIOS_IMPORTER_FICHIER = "/alfresco/webdav/Dictionnaire%20des%20Donn%C3%A9es/Certificats";

    private Properties configuration;

    private NodeService nodeService;

    private ContentService contentService;

    private ParapheurService parapheurService;

    private SearchService searchService;

    private NamespaceService namespaceService;

    private FileFolderService fileFolderService;

    private TransactionService transactionService;

    private AuthenticationComponent authenticationComponent;

    private TenantService tenantService;

    private TenantAdminService tenantAdminService;
    
    private MetadataService metadataService;

    private Scheduler scheduler;

    private boolean enabled;

    private HttpClient httpClient;

    public Properties getConfiguration() {
        return configuration;
    }

    /**
     * @param configuration The configuration to set.
     */
    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    /**
     * @param contentService The contentService to set.
     */
    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    /**
     * @param namespaceService The namespaceService to set.
     */
    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    /**
     * @param fileFolderService The fileFolderService to set.
     */
    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    /**
     * @param transactionService The transactionService to set.
     */
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * @param nodeService The nodeService to set.
     */
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    /**
     * @param searchService The searchService to set.
     */
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setAuthenticationComponent(AuthenticationComponent authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }

    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    public void setTenantAdminService(TenantAdminService tenantAdminService) {
        this.tenantAdminService = tenantAdminService;
    }
    
    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    /**
     * @param scheduler the Scheduler to set
     */
    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    /**
     * @return the enabled
     */
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isActesEnabled() {
        return false;
    }

    @Override
    public boolean isHeliosEnabled() {
        return "true".equals(getPropertiesHelios().get("active"));
    }

    /* (non-Javadoc)
    * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
    */
    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(nodeService, "There must be a node service");
        Assert.notNull(searchService, "There must be a search service");
        Assert.notNull(contentService, "There must be a content service");
        Assert.notNull(namespaceService, "There must be a namespace service");
        Assert.notNull(parapheurService, "There must be a parapheur service");
        Assert.notNull(configuration, "There must be a configuration element");
        //purgeFolderIn();
    }

    @Override
    public void restartGetS2lowStatusJobs() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Map<String, String> getPropertiesHelios() {
        try {
            Map<String, String> propertiesHelios = new HashMap<String, String>();
            SAXReader reader = new SAXReader();
            Document document = reader.read(readFile("fast_configuration.xml"));
            List<Node> nodes = document.getRootElement().element("helios").elements();
            for (Node node : nodes) {
                propertiesHelios.put(node.getName(), node.getText());
            }
            return propertiesHelios;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new AlfrescoRuntimeException(ex.getMessage(), ex);
        }
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#getXadesSignatureProperties(java.lang.String)
     */
    @Override
    public Properties getXadesSignatureProperties(String typeDossier) {
        Map<String, String> propertiesHelios = getPropertiesHelios();
        Properties props = new Properties();
        props.setProperty("pPolicyIdentifierID", propertiesHelios.get("pPolicyIdentifierID"));
        props.setProperty("pPolicyIdentifierDescription", propertiesHelios.get("pPolicyIdentifierDescription"));
        props.setProperty("pPolicyDigest", propertiesHelios.get("pPolicyDigest"));
        props.setProperty("pSPURI", propertiesHelios.get("pSPURI"));
        props.setProperty("pCity", propertiesHelios.get("pCity"));
        props.setProperty("pPostalCode", propertiesHelios.get("pPostalCode"));
        props.setProperty("pCountryName", propertiesHelios.get("pCountryName"));
        props.setProperty("pClaimedRole", propertiesHelios.get("pClaimedRole"));

        return props;
    }

    @Override
    public Properties getXadesSignaturePropertiesWithDossier(NodeRef dossier) {
        // pas d'appel direct à cette methode.
        return null;
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#getS2lowActesNatures()
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<Integer, String> getS2lowActesNatures() {
        Map<Integer, String> mapRes = new HashMap<Integer, String>();

        if (logger.isDebugEnabled()) {
            logger.debug("getS2lowActesNatures: BEGIN");
        }
        NodeRef configRef = getS2lowActesClassificationNodeRef();
        if (configRef == null) {
            logger.error("getS2lowActesNatures: configRef is null");
            return null;
        }
        ContentReader contentreader = contentService.getReader(configRef, ContentModel.PROP_CONTENT);
        if (contentreader == null) {
            logger.error("getS2lowActesNatures: contentreader is null");
            return null;
        }
        SAXReader saxreader = new SAXReader();

        try {
            InputSource is = new InputSource(contentreader.getContentInputStream());
            is.setEncoding("ISO-8859-1");
            Document document = saxreader.read(is);
            Element rootElement = document.getRootElement();
            Element natures = rootElement.element("NaturesActes");
            if (natures != null) {
                Iterator<Element> elementIterator = (Iterator<Element>) natures.elementIterator("NatureActe");
                for (Iterator<Element> i = elementIterator; i.hasNext(); ) {
                    Element nature = i.next();
                    mapRes.put(Integer.parseInt(nature.attribute("CodeNatureActe").getValue()),
                            nature.attribute("Libelle").getValue());
                }
            }
        } catch (DocumentException e) {
            if (logger.isDebugEnabled()) {
                logger.debug(e.getMessage(), e);
            }
            return null;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getS2lowActesNatures: number of nature Elts=" + mapRes.size());
        }
        return mapRes;
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#getXadesSignatureProperties()
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, String> getS2lowActesClassifications() {
        /*
        Map<String, String> mapRes = new HashMap<String, String>();

        NodeRef configRef = getS2lowActesClassificationNodeRef();
        if (configRef == null) {
            logger.error("getS2lowActesClassifications: configRef is null");
            return null;
        }
        ContentReader contentreader = contentService.getReader(configRef, ContentModel.PROP_CONTENT);
        if (contentreader == null) {
            logger.error("getS2lowActesClassifications: contentreader is null");
            return null;
        }
        SAXReader saxreader = new SAXReader();

        try {
            InputSource is = new InputSource(contentreader.getContentInputStream());
            is.setEncoding("ISO-8859-1");
            Document document = saxreader.read(is);
            Element rootElement = document.getRootElement();
            Element matieres1 = rootElement.element("Matieres");
            if (matieres1 != null) {
                for (Iterator<Element> i1 = matieres1.elementIterator("Matiere1"); i1.hasNext();) {
                    Element matiere1 = i1.next();
                    String id1 = matiere1.attribute("CodeMatiere").getValue().toString();
                    mapRes.put(id1, matiere1.attribute("Libelle").getValue().toString());

                    for (Iterator<Element> i2 = matiere1.elementIterator("Matiere2"); i2.hasNext();) {
                        Element matiere2 = i2.next();
                        String id2 = id1 + "-" + matiere2.attribute("CodeMatiere").getValue().toString();
                        mapRes.put(id2, matiere2.attribute("Libelle").getValue().toString());

                        for (Iterator<Element> i3 = matiere2.elementIterator("Matiere3"); i3.hasNext();) {
                            Element matiere3 = i3.next();
                            String id3 = id2 + "-" + matiere3.attribute("CodeMatiere").getValue().toString();
                            mapRes.put(id3, matiere3.attribute("Libelle").getValue().toString());

                            for (Iterator<Element> i4 = matiere3.elementIterator("Matiere4"); i4.hasNext();) {
                                Element matiere4 = i4.next();
                                String id4 = id3 + "-" + matiere4.attribute("CodeMatiere").getValue().toString();
                                mapRes.put(id4, matiere4.attribute("Libelle").getValue().toString());

                                for (Iterator<Element> i5 = matiere4.elementIterator("Matiere5"); i5.hasNext();) {
                                    Element matiere5 = i5.next();
                                    String id5 = id4 + "-" + matiere5.attribute("CodeMatiere").getValue().toString();
                                    mapRes.put(id5, matiere5.attribute("Libelle").getValue().toString());
                                }
                            }
                        }
                    }
                }
            }
        } catch (DocumentException e) {
            return null;
        }

        return mapRes;
        */
        throw new RuntimeException("Not supported yet.");
    }

    @Override
    public int updateS2lowActesClassifications() throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * @param archive
     * @throws java.io.IOException
     * @see com.atolcd.parapheur.repo.S2lowService#setS2lowActesArchiveURL(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public String setS2lowActesArchiveURL(NodeRef archive) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * @throws java.io.IOException
     * @see com.atolcd.parapheur.repo.S2lowService#getInfosS2low(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public int getInfosS2low(NodeRef dossier) throws IOException {
        Assert.isTrue(this.parapheurService.isDossier(dossier), "Node Ref doit être de type ph:dossier");

        int result;
        if ("HELIOS".equals(this.nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_PROTOCOLE))) {
            result = getInfosS2lowHelios(dossier);
            /*
            if (result == TransactionStatus.STATUS_ACK) {
                parapheurService.approve(dossier);
            }
            */
        } else {
            throw new UnsupportedOperationException("Not supported yet.");
            // result = getInfosS2lowActes(dossier);
        }

        return result;
    }

    /**
     * Compute the name of the xml on as sent to fast
     * by getting ParapheurModel.PROP_TRANSACTION_ID from the nodeRef
     */
    private String computeNameForDossier(NodeRef dossierRef, NodeRef fichierRef) {
        Integer transactionId = (Integer) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TRANSACTION_ID);
        Map<String, String> propertiesHelios = getPropertiesHelios();
        String collectivite = propertiesHelios.get("collectivite");
        String nomFichier;
        if (transactionId != null) { // Backward compatibility
             nomFichier = "" + transactionId + ".xml";
        }
        else {
            /* On suppose que le nom du fichier est égal au nomFic et qu'il est
             * unique dans toute la collectivité (donc on préfixe le nom avec le
             * SIREN de la collectivité
             */
            nomFichier = (String)nodeService.getProperty(fichierRef, ContentModel.PROP_NAME);
        }
        return collectivite + "_" + nomFichier;
    }

    private boolean isFileExists(HttpClient client, String path) {
        boolean retval = false;
        PropFindMethod method = null;
        try {
            method = new PropFindMethod(path);
            client.executeMethod(method);

            if (method.succeeded()) {
                retval = true;
            }

        } catch (HttpException e) {
            logger.error(e.getLocalizedMessage());
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
        return retval;
    }

    private String getARacteXml(HttpClient client, String path) {
        String retVal = "";
        GetMethod getMethod = new GetMethod(path);
        try {
            client.executeMethod(getMethod);

            retVal = getMethod.getResponseBodyAsString();
        } catch (HttpException e) {
            logger.error(e.getLocalizedMessage());
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
        }
        return retVal;
    }
    
    //Quand FAST aura mis à disposition son WS...
    /*private boolean isFileDifferent(HttpClient client, String path, File file) {
        boolean retval = true;
        GetMethod getMethod = new GetMethod(path);
        try {
            client.executeMethod(getMethod);
            if (getMethod.getStatusCode() == HttpStatus.SC_OK) {
                InputStream fileFromFAST = getMethod.getResponseBodyAsStream();
                retval = !getSHA1CheckSum(fileFromFAST).equals(getSHA1CheckSum(new FileInputStream(file)));
            }
        } catch (Exception e) {
            throw new RuntimeException("Erreur lors de la comparaison des deux fichiers : " + e.getMessage());
        }
        return retval;
    }
    
    private String getSHA1CheckSum(InputStream file) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA1");
        InputStream fis = file;
        byte[] dataBytes = new byte[1024];
        int nread = 0; 

        while ((nread = fis.read(dataBytes)) != -1) {
            md.update(dataBytes, 0, nread);
        }

        byte[] mdbytes = md.digest();

        //convert the byte to hex format
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mdbytes.length; i++) {
            sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }*/

    /*
    enum Domaine {
        "Technique",
        "Signature Technique",
        "Signature Globale",
        "Signature bordereau",
        "PJ",
        "Dépense",

    } */

    /* is this code DEAD ????
    class ElementACQUIT {
        Integer DomaineAck;
        Boolean EtatAck;
        String ExerciceBord;
        Integer NumBord;
        Integer NumPiece;
        Integer NumLigne;
        String IdUnique;
        String NumAnoAck;
        String LibelleAnoAck;
    }

    private ElementACQUIT buildElementACQUIT(Element elementACQUIT) {
        List<Element> elements = elementACQUIT.elements();
        ElementACQUIT elementACK = new ElementACQUIT();
        for (Element e : elements) {
            String v = e.attribute("V").getValue();
            String t = e.getName();

            if (t.equals("DomaineAck")) {
                elementACK.DomaineAck = Integer.parseInt(v);
            } else if (t.equals("EtatAck")) {
                // With FAST Hacks
                if (v.equals("1") || v.equals("true")) {
                    elementACK.EtatAck = true;
                } else if (v.equals("0") || v.equals("false")) {
                    elementACK.EtatAck = false;
                }
            } else if (t.equals("ExerciceBord")) {
                elementACK.ExerciceBord = v;
            } else if (t.equals("NumBord")) {
                elementACK.NumBord = Integer.parseInt(v);
            } else if (t.equals("NumPiece")) {
                elementACK.NumPiece = Integer.parseInt(v);
            } else if (t.equals("IdUnique")) {
                elementACK.IdUnique = v;
            } else if (t.equals("NumAnoAck")) {
                elementACK.NumAnoAck = v;
            } else if (t.equals("LibelleAnoAck")) {
                elementACK.LibelleAnoAck = v;
            }
        }
        return elementACK;
    }

    private List<ElementACQUIT> parseACQUIT(String arHeliosXML) throws DocumentException {
        List<ElementACQUIT> retVal = new ArrayList<ElementACQUIT>();

        String PESV2_ACQUIT_TAG = "ACQUIT";
        String PESV2_ElementACQUIT_TAG = "ElementACQUIT";
        SAXReader saxreader = new SAXReader();
        InputSource inputSource = new InputSource(arHeliosXML);

        Document document;

        document = saxreader.read(inputSource);

        Element rootElement = document.getRootElement();
        Element ACQUIT = rootElement.element(PESV2_ACQUIT_TAG);

        for (Iterator<Element> i1 = ACQUIT.elementIterator(PESV2_ACQUIT_TAG); i1.hasNext(); ) {
            Element elementACQUIT = i1.next();
            ElementACQUIT elementACK = buildElementACQUIT(elementACQUIT);
            retVal.add(elementACK);
        }
        return retVal;
    }
*/


    private boolean isFileOldEnoughToBeInError(HttpClient client, String path) {
        boolean retval = false;
        try {
            Map<String, String> properties = getPropertiesHelios();
            PropFindMethod method = new PropFindMethod(path, DavConstants.PROPFIND_ALL_PROP, DavConstants.DEPTH_1);
            client.executeMethod(method);


            MultiStatus multiStatus = method.getResponseBodyAsMultiStatus();
            DavPropertySet props = multiStatus.getResponses()[0].getProperties(HttpStatus.SC_OK);
            DavProperty<String> property = (DavProperty<String>) props.get(DavPropertyName.CREATIONDATE);

            WebdavDateParser dateParser = new WebdavDateParser();

            Date fileCreation = dateParser.parseDate(property);

            GregorianCalendar calendar = new GregorianCalendar();

            String file_ttl_str;
            int file_ttl = -1;

            file_ttl_str = properties.get("time_to_consider_file_not_pickable");

            if (file_ttl_str == null) {
                file_ttl_str = "24"; // defaulting to 24 hours.
            }

            try {
                file_ttl = -1 * Integer.parseInt(file_ttl_str);
            } catch (NumberFormatException e) {

            }

            // 3 hours to be in error time_to_consider_file_in_error
            calendar.add(Calendar.HOUR, file_ttl);

            retval = calendar.getTime().getTime() > fileCreation.getTime();
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
        } catch (DavException e) {
            logger.error(e.getLocalizedMessage());
        }
        return retval;
    }

    @Override
    public Properties getPadesSignatureProperties(String type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Properties getPadesSignaturePropertiesWithDossier(NodeRef dossier) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    public class WebdavDateParser {
        Vector<SimpleDateFormat> dateFormats;

        public WebdavDateParser() {

            dateFormats = new Vector<SimpleDateFormat>();
            dateFormats.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US));
            dateFormats.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.US));
            dateFormats.add(new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US));
            dateFormats.add(new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US));
            dateFormats.add(new SimpleDateFormat("EEEEEE, dd-MMM-yy HH:mm:ss zzz", Locale.US));
            dateFormats.add(new SimpleDateFormat("EEE MMMM d HH:mm:ss yyyy", Locale.US));

            TimeZone gmtZone = TimeZone.getTimeZone("GMT");
            for (SimpleDateFormat dateFormat : dateFormats)
                dateFormat.setTimeZone(gmtZone);
        }

        /**
         * Converts a WebDAV property specifying a date into a Date object
         *
         * @param propDate The property
         * @return The Date object
         */
        public Date parseDate(DavProperty propDate) {
            Date date = null;

            if (propDate != null) {
                String dateValue = propDate.getValue().toString();
                for (SimpleDateFormat dateFormat : dateFormats) {
                    try {
                        date = dateFormat.parse(dateValue);
                        break;
                    } catch (ParseException e) {
                    }
                }
            }

            return date;
        }

    }


    private void putARacteXml(NodeRef dossier, String ARActeXml) {
        if (logger.isDebugEnabled()) {
            logger.debug("Saving ACK as a property");
        }
        ContentWriter arWriter = this.contentService.getWriter(dossier, ParapheurModel.PROP_ARACTE_XML, true);
        arWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        arWriter.setEncoding("UTF-8");  // FIXME: c'est peut-etre ISO-8859-1
        arWriter.putContent(ARActeXml);
    }

    private String extractNomFicFromDossier(NodeRef dossier) {
        // Cas du PES ? Seulement le premier doc principal est utilise
        NodeRef nodeDoc = parapheurService.getMainDocuments(dossier).get(0);
        ContentReader reader = contentService.getReader(nodeDoc, ContentModel.PROP_CONTENT);
        String docEncoding = reader.getEncoding().toUpperCase();
        String nomFic = null;

        //  on se limite à Integer.MAX_VALUE = 2147483647, quid si le fichier est plus gros (>2Go) ?
        if (reader.getSize() > Integer.MAX_VALUE) {
            logger.fatal("Document XML trop gros pour pouvoir etre traité.");
            return null;
        }
        String l_str = null;
        DOMParser domParser = new DOMParser();

        org.w3c.dom.Document l_xmlDocument;

        byte[] docContent = new byte[(int) reader.getSize()];
        try {
            reader.getContentInputStream().read(docContent);
        } catch (IOException e) {
            logger.warn("Lecture du NomFic Impossible");
            docContent = null;
            if (docContent != null) {
                // juste pour éviter un warning dans l'IDE
                return null;
            }
            return null;
        }

        try {
            String strXml = new String(docContent, docEncoding);

            docContent = null;
            if (docContent != null) { return null; }

            InputSource docSource = new InputSource(new StringReader(strXml));
            domParser.parse(docSource);

            l_xmlDocument = domParser.getDocument();

            NodeList results = XPathAPI.selectNodeList(l_xmlDocument, "/*/Enveloppe/Parametres/NomFic");
            if (results.getLength() == 1) {
                org.w3c.dom.Node nomFicNode = results.item(0);
                NamedNodeMap attributes = nomFicNode.getAttributes();
                org.w3c.dom.Node valueAttribute = attributes.getNamedItem("V");
                nomFic = valueAttribute.getTextContent();
            }

        } catch (UnsupportedEncodingException e) {
            logger.error(e.getLocalizedMessage());
        } catch (SAXException e) {
            logger.error(e.getLocalizedMessage());
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
        } catch (TransformerException e) {
            logger.error(e.getLocalizedMessage());
        }

        return nomFic;
    }

    private void deleteFile(HttpClient client, String path) throws IOException {
        DeleteMethod deleteMethod = new DeleteMethod(path);
        client.executeMethod(deleteMethod);
    }


    private HttpClient getHttpClient(Map<String, String> propertiesHelios) throws IOException {
        if (httpClient == null) {
            // HTTPS
            try {
                EasySSLProtocolSocketFactory easy = new EasySSLProtocolSocketFactory();
                KeyMaterial km = new KeyMaterial(readFile(propertiesHelios.get("name")), propertiesHelios.get("password").toCharArray());
                easy.setKeyMaterial(km);
                Protocol easyhttps = new Protocol("https", (ProtocolSocketFactory) easy, Integer.parseInt(propertiesHelios.get("port")));
                Protocol.registerProtocol("https", easyhttps);
            } catch (Exception e) {
                logger.error("Erreur lors de la modification du protocole https");
                if (logger.isDebugEnabled()) {
                    logger.debug(e.getMessage(), e);
                }
                throw new IOException("getInfoS2lowHelios: HTTPS impossible.", e);
            }
            // /HTTPS

            httpClient = new HttpClient();
        }
        return httpClient;

    }

    /**
     * Récupération de la réponse de FAST quant à l'état du PES Helios
     * 
     *   Algo:
     * 
     *   filename = ComputeFilenameFromDossierRef(dossierRef)
     *   if (isFilenameInFolder(filename, "in")) {
     *       // decide wether it's waiting or in error
     *       setStatus("En cours de Transmission");
     *       or
     *       setStatus("Refusé");
     *   }
     *   else {
     *       if (isFileNameInFolder(filename, "out")) {
     *           // get ACK
     *           setStatus("Validé");
     *           parapheurService.approve(dossierRef);
     *       }
     *       else {
     *           setStatus("En cours");
     *       }
     *   }
     * 
     *
     * @param dossier
     * @return
     * @throws IOException
     */
    private int getInfosS2lowHelios(NodeRef dossier) throws IOException {
        Map<String, String> propertiesHelios = getPropertiesHelios();
        String serverAddress = propertiesHelios.get("server");
        String port = propertiesHelios.get("port");
        String collectivite = propertiesHelios.get("collectivite");
        String cinematique = propertiesHelios.get("cinematique");

        HttpClient client = getHttpClient(propertiesHelios);


        if (client == null) {
            throw new IOException("getInfoS2lowHelios : HTTPS impossible.");
        }
        
        String baseUrl = "https://" + serverAddress + ":" + port + "/";
        // Cas du PES ? Seulement le premier doc principal est utile
        String filename = computeNameForDossier(dossier, this.parapheurService.getMainDocuments(dossier).get(0));
        String inFolder = baseUrl + URL_HELIOS_IMPORTER_FICHIER + collectivite + "/in/" + cinematique;
        String outFolder = baseUrl + URL_HELIOS_IMPORTER_FICHIER + collectivite + "/out/" + cinematique;
        Integer statut;// = TransactionStatus.STATUS_ERROR;

        ContentDataWithId acquittement = (ContentDataWithId) nodeService.getProperty(dossier, ParapheurModel.PROP_ARACTE_XML);

        /* Si on a déjà un acquittement on n'essaie pas d'en récuperer un nouveau
         * cas de l'archivage par exemple.
         */
        if (acquittement != null && acquittement.getSize() > 0) {
            return TransactionStatus.STATUS_ACK;
        }

        if (isFileExists(client, inFolder + "/" + filename)) {
            if (isFileOldEnoughToBeInError(client, inFolder + "/" + filename)) {
                statut = TransactionStatus.STATUS_ERROR;
            } else {
                statut = TransactionStatus.STATUS_POSTE;
            }
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("checking for ACK");
            }
            // lire le XML pour extraire le nomfic en effet le nom du fichier contenant l'ack est <nomfic>.xml
            String nomFic = extractNomFicFromDossier(dossier);

            if (logger.isDebugEnabled()) {
                logger.debug("Nomfic = " + nomFic);
            }

            if (isFileExists(client, outFolder + "/" + nomFic + ".xml")) {

                if (isFileExists(client, outFolder + "/" + filename)) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("file PESv2 is also in outFolder, delete it.");
                    }
                    // suppression du PES du dossier Out.
                    deleteFile(client, outFolder + "/" + filename);
                } 

                // on récupère l'acquitement et on approve
                String aRActesXml = getARacteXml(client, outFolder + "/" + nomFic + ".xml");
                putARacteXml(dossier, aRActesXml);

                if (logger.isDebugEnabled()) {
                    logger.debug("ACK saved trying to approve");
                }

                parapheurService.approveV4(dossier, parapheurService.getCurrentParapheur());

                // suppression acquittement du dossier Out.
                deleteFile(client, outFolder + "/" + nomFic + ".xml");
                statut = TransactionStatus.STATUS_ACK;
            }
            /*else if (testEnTraitement) {
                statut = TransactionStatus.STATUS_EN_TRAITEMENT;
            }*/
            else {
                statut = TransactionStatus.STATUS_EN_TRAITEMENT;
                //statut = TransactionStatus.STATUS_NON_TRANSMIS;
            }
        }

        nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS, statutS2lowToString(statut));

        if (logger.isDebugEnabled()) {
            logger.debug("Status : " + statutS2lowToString(statut));
        }

        return statut;
    }

    /**
     * @see com.atolcd.parapheur.repo.S2lowService#statutS2lowToString(int)
     */
    @Override
    public String statutS2lowToString(int code) {
        String res = null;
        switch (code) {
            case TransactionStatus.STATUS_ERROR:
                res = "Erreur";
                break;
            /*
            case TransactionStatus.STATUS_ANNULE:
                res = "Annulé";
                break;
            */
            case TransactionStatus.STATUS_POSTE:
                res = "Posté";
                break;
            /*
            case TransactionStatus.STATUS_EN_ATTENTE:
                res = "En attente de transmission";
                break;

            case TransactionStatus.STATUS_TRANSMIS:
                res = "Transmis";
                break;
            */
            case TransactionStatus.STATUS_ACK:
                res = "Acquittement reçu";
                break;
            /*
            case TransactionStatus.STATUS_VALIDE:
                res = "Validé";
                break;
            */
            case TransactionStatus.STATUS_NACK:
                res = "Refusé";
                break;
            case TransactionStatus.STATUS_EN_TRAITEMENT:
                res = "En traitement";
                break;
            case TransactionStatus.STATUS_NON_TRANSMIS:
                res = "Non transmis";
                break;
        }
        return res;
    }

    @Override
    public boolean isCertificateAbleToConnect(String protocole, String typeDossier) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override
    public List<String> getListLoginForType(String protocole, String typeDossier) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isConnectionOK(String protocole, String typeDossier) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * @throws java.io.IOException
     * @see com.atolcd.parapheur.repo.S2lowService#envoiS2lowActes(org.alfresco.service.cmr.repository.NodeRef, String, String, String, String, String)
     */
    @Override
    public void envoiS2lowActes(NodeRef dossier, String nature, String classification, String numero, String objet, String date)
            throws IOException, ParapheurException {
        throw new UnsupportedOperationException("Not supported yet.");
    }


    /**
     * @see com.atolcd.parapheur.repo.S2lowService#getS2lowActesClassificationNodeRef()
     */
    @Override
    public NodeRef getS2lowActesClassificationNodeRef() {
        NodeRef res = null;

        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/"
                + this.configuration.getProperty("spaces.dictionary.childname") + "/"
                + this.configuration.getProperty("spaces.configfast.childname");

        List<NodeRef> results;
        try {
            results = searchService.selectNodes(
                    nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))),
                    xpath,
                    null,
                    namespaceService,
                    false);
            if (results != null && results.size() == 1) {
                // found what we were looking for
                logger.debug("getS2lowClassificationNodeRef: Found 1 node");
                res = results.get(0);
            }
        } catch (AccessDeniedException err) {
            // I Still haven't found what I was looking for....
            // ...so ignore and return null
            logger.debug("getS2lowClassificationNodeRef: Access denied Exception");
            return null;
        }

        return res;
    }

    /**
     * @throws java.io.IOException
     * @see com.atolcd.parapheur.repo.S2lowService#envoiS2lowHelios(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void envoiS2lowHelios(NodeRef dossier) throws IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("envoi FAST");
        }
        Assert.isTrue(this.parapheurService.isDossier(dossier), "Node Ref doit être de type ph:dossier");
        Assert.isTrue(!this.nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW), "Le dossier a déjà été envoyé à la plate-forme CDC-FAST");
        
        Map<String, String> propertiesHelios = getPropertiesHelios();
        HttpClient client = getHttpClient(propertiesHelios);
        if (client == null) {
            throw new IOException("envoiS2lowHelios cnxFAST: HTTPS impossible.");
        }

        String serverAddress = propertiesHelios.get("server");
        String port = propertiesHelios.get("port");
        String collectivite = propertiesHelios.get("collectivite");
        String cinematique = propertiesHelios.get("cinematique");
        
        // Récupération des fichiers - premier document principal
        List<NodeRef> documents = this.parapheurService.getMainDocuments(dossier);
        NodeRef fichier = documents.get(0);
        String nomFichier = computeNameForDossier(dossier, fichier);
        
        //  Nom du fichier correct     PutMethod put = new PutMethod("https://" + serverAddress + ":" + port + URL_HELIOS_IMPORTER_FICHIER + propertiesHelios.get("collectivite") + nomDossier + "-" + this.nodeService.getProperty(documents.get(0), ContentModel.PROP_NAME));
        //https://demo-parapheur.cdcfast.fr/partage/webdav/clients/221500010/in/
        //https://demo-parapheur.cdcfast.fr/partage/webdav/clients/221500010/in/pes
        String pathFile = "https://" + serverAddress + ":" + port + "/" +
                URL_HELIOS_IMPORTER_FICHIER + collectivite +
                "/in/" + cinematique + "/" + nomFichier;
        
        // On marque l'envoi du fichier :
        nodeService.setProperty(dossier, ParapheurModel.PROP_VALIDATOR, AuthenticationUtil.getRunAsUser());
        
        
        
        //Quand FAST aura mis à disposition son WS...
        /*int statusTransaction = getInfosS2lowHelios(dossier);
        if ((statusTransaction != TransactionStatus.STATUS_POSTE) &&
                (statusTransaction != TransactionStatus.STATUS_NON_TRANSMIS)){
            if (logger.isDebugEnabled()) {
                logger.debug("Le fichier : " + nomFichier + " présent dans le dossier : " + nodeService.getProperty(dossier, ContentModel.PROP_NAME) + "a déjà été envoyé, status : " + statutS2lowToString(statusTransaction));
            }
            return;
        }*/
        
        PutMethod put = new PutMethod(pathFile);

        ArrayList<FilePart> fileParts = new ArrayList<FilePart>();
        ArrayList<Part> parts = new ArrayList<Part>();

        // Premier fichier; Cette version d'API ne permet l'envoi que d'un fichier XML (le premier doc du dossier)
        // envoi du document principal
        ContentReader mainDocReader = this.contentService.getReader(fichier, ContentModel.PROP_CONTENT);
        String mainDocMimeType = mainDocReader.getMimetype().trim();
        if (logger.isDebugEnabled()) {
            logger.debug("envoiS2lowHelios cnxFAST (" + documents.size() + " docs): ajout mainDoc type " + mainDocMimeType);
        }
        if (!mainDocMimeType.equalsIgnoreCase(MimetypeMap.MIMETYPE_XML) &&
                !mainDocMimeType.contains("text/xml") &&
                !mainDocMimeType.contains("application/xml") &&
                !mainDocMimeType.contains("application/readerpesv2") &&
                !mainDocMimeType.equalsIgnoreCase("application/vnd.xemelios-xml")) {
            throw new RuntimeException("Echec d'envoi: La plateforme HELIOS attend un fichier XML.");
        }
        File ficXml = ensureMimeType(mainDocReader, mainDocMimeType);
        
        //Quand FAST aura mis à disposition son WS...
        /*if (statusTransaction == TransactionStatus.STATUS_POSTE) {
            if (logger.isDebugEnabled()) {
                logger.debug("Un fichier portant le même nom est déjà présent dans : " + pathFile);
                logger.debug("On récupère le dossier pour voir les différences");
            }
            if (isFileDifferent(client, pathFile, ficXml)) {
                nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS, statutS2lowToString(TransactionStatus.STATUS_ERROR));
                throw new RuntimeException("Un fichier du même nom avec un contenu différent est déjà présent sur le dépôt : nom fichier = " + nomFichier);
            }
            else if (logger.isDebugEnabled()) {
                logger.debug("Les fichiers sont égaux. Mise à jour du status : " + statutS2lowToString(TransactionStatus.STATUS_POSTE));
            }
        }*/
        
        
        fileParts.add(new FilePart("enveloppe", ficXml));

        for (FilePart ficPart : fileParts) {
            parts.add(ficPart);
        }
        Part[] partsTab = new Part[parts.size()];
        for (int i = 0; i < parts.size(); i++) {
            partsTab[i] = parts.get(i);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("   Request Charset: " + put.getRequestCharSet());
        }

        RequestEntity requestEntity = new InputStreamRequestEntity(new FileInputStream(ficXml));
        put.setRequestEntity(requestEntity);
        // Exécution de la méthode PUT
        try {
            
            if (logger.isDebugEnabled()) {
                logger.debug("Envoi du fichier : " + nomFichier + " présent dans le dossier : " + nodeService.getProperty(dossier, ContentModel.PROP_TITLE));
            }
            
            int status = client.executeMethod(put);
            String sreponse = put.getResponseBodyAsString();

            if (logger.isDebugEnabled()) {
                logger.debug("FAST response status : " + status);
                logger.debug("FAST response body : " + sreponse);
            }
            if (HttpStatus.SC_CREATED == status) {
                nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS, statutS2lowToString(TransactionStatus.STATUS_POSTE));
                if (sreponse.startsWith("KO")) {
                    String error = "Erreur retournée par la plate-forme FAST : ";
                    throw new RuntimeException(error + sreponse.replace('\n', ':'));
                }
                //startGetS2lowStatusJob(dossier);
            } 
            else {
                throw new RuntimeException("Echec lors du dépôt du fichier " + nomFichier + ": statut = " + status + "réponse : " + sreponse);
            }
        } finally {
            put.releaseConnection();
        }
    }

    @Override
    public void getS2lowHeliosListePES_Retour() throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isMailServiceEnabled() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void getSecureMailInfos(NodeRef dossier) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String cancelMailSecJob(NodeRef dossier) throws IOException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
    
    @Override
    public boolean isMailSecJobCancelable(NodeRef dossier) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void restartGetMailsecS2lowStatusJobs() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getSecureMailVersion() throws IOException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int sendSecureMail(List<String> mailto, List<String> mailcc, List<String> mailcci, String objet, String message, String password, boolean send_password, NodeRef dossierRef, List<NodeRef> attachments, boolean include) throws IOException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int sendSecureMail(List<String> mailto, List<String> mailcc, List<String> mailcci, String objet, String message, String password, boolean send_password, List<FilePart> files) throws IOException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int sendSecureMail(List<String> mailto, String objet, String message) throws IOException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getSecureMailCount() throws IOException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Map<Integer, S2lowServiceImpl.SecureMailDetail> getSecureMailList(int limit, int offset) throws IOException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public S2lowServiceImpl.SecureMailDetail getSecureMailDetail(int id) throws IOException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean deleteSecureMail(Integer id) throws IOException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getSecureMailTemplate(NodeRef dossier) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getSecureMailMessageWithTemplate(NodeRef dossier, String template) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Properties getCustomXadesSignatureProperties(String type) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Properties getCustomXadesSignaturePropertiesWithDossier(NodeRef dossier) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public HttpClient createConnexionHTTPS(Map<String, String> properties, InputStream is) {
        return null;
    }

    @Override
    public List<String> isCertificateOk(Map<String, String> properties, InputStream is) {
        return null;
    }

    @Override
    public boolean isConnexionOK(Map<String, String> properties, InputStream is) {
        return false;
    }

    @Deprecated
    protected void startGetS2lowStatusJob(NodeRef dossierRef) {
    }


    public void runGetFastStatusJob() {
        SearchParameters searchParameters = new SearchParameters();

        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.setQuery("TYPE:\"ph:dossier\" AND @ph\\:tdtNom:\"FAST\" AND (@ph\\:status:\"En traitement\" OR @ph\\:status:\"Posté\")");
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);

        ResultSet resultSet = searchService.query(searchParameters);

        List<NodeRef> dossiers = resultSet.getNodeRefs();

        resultSet.close();

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Running batch job %d files", dossiers.size()));
        }

        for (NodeRef dossier : dossiers) {

            try {
            EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossier);

            if (etape != null
                    && etape.getActionDemandee().equals(EtapeCircuit.ETAPE_TDT)
                    && nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW)) {
                if (PROP_TDT_NOM_FAST.equals(nodeService.getProperty(dossier, ParapheurModel.PROP_TDT_NOM))) {
                    String fastStatusStr = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS);

                    if (logger.isDebugEnabled()) {
                        String name = (String)nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
                        logger.debug(String.format("fetching status for %s", name));
                    }

                    if (fastStatusStr != null) {
                        if (statutS2lowToString(TransactionStatus.STATUS_EN_TRAITEMENT).equals(fastStatusStr) ||
                                statutS2lowToString(TransactionStatus.STATUS_POSTE).equals(fastStatusStr)) {
                            final NodeRef fDossier = dossier;
                            String validator = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_VALIDATOR);
                            if (validator != null) {
                                AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {
                                    @Override
                                    public Object doWork() throws Exception {
                                        UserTransaction transaction = transactionService.getNonPropagatingUserTransaction();

                                        try {
                                            transaction.begin();
                                            getInfosS2lowHelios(fDossier);
                                            transaction.commit();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            try {
                                                if (transaction.getStatus() == Status.STATUS_ACTIVE) {
                                                    transaction.rollback();
                                                }
                                            } catch (Throwable t) {
                                                // do nothing but cry
                                            }

                                        }
                                        return null;
                                    }
                                }, validator);
                            } else {
                                logger.error("Le dossier ne possède pas de validateur courant");
                            }
                        }

                    }
                }
            }
            } catch (Exception e) {
               e.printStackTrace();
            }
        }

    }

    @Override
    public void restartGetS2lowStatusJob() {

        try {
            // Trigger: Execute in 2 minutes, and repeat every 5 minutes
            Map<String, String> heliosProps = getPropertiesHelios();


            int check_freq = 30;
            try {
                check_freq = Integer.parseInt(heliosProps.get("check_freq"));
            } catch (Exception e) {

            }

            String tenantName = "";

            if (tenantAdminService.isEnabled()) {
                tenantName = tenantService.getUserDomain(AuthenticationUtil.getRunAsUser());
                if (tenantName.length() > 0) {
                    tenantName = "-" + tenantName;
                }
            }


            // BLEX
            long oneMinute = 60 * 1000;

            // BLEX : relance toutes les X min
            long maxRepeatInterval = check_freq * oneMinute;

            // BLEX : delai de lancement au hasard entre dans 1 minute et X minutes
            long delay = (long)(Math.random()* maxRepeatInterval) + oneMinute ;

            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Scheduling job every %d minutes starting in %d", check_freq, delay/oneMinute));
            }

            Trigger trigger = new SimpleTrigger(
                    FASTHELIOSQUARTZTRIGGER_NAME + tenantName,
                    FASTQUARTZTRIGGER_GROUP,
                    new Date(System.currentTimeMillis() + delay),
                    null,
                    SimpleTrigger.REPEAT_INDEFINITELY,
                    check_freq * 60 * 1000);

            JobDetail details = new JobDetail(
                    "FastHeliosQuartzJob" + tenantName,
                    "FAST",
                    FastHeliosQuartzJob.class);

            Map<String, Object> jobDataMap = new HashMap<String, Object>();

            if (logger.isDebugEnabled()) {
                logger.debug("FastHeliosQuartzJob with tenant=" + tenantName);
            }
            jobDataMap.put("fastService", this);
            jobDataMap.put("tenantService", tenantService);
            jobDataMap.put("tenantAdminService", tenantAdminService);
            jobDataMap.put("adminName", AuthenticationUtil.getRunAsUser());
            details.setJobDataMap(new JobDataMap(jobDataMap));

            scheduler.scheduleJob(details, trigger);
        } catch (Exception e) {
            throw new RuntimeException("Error scheduling job", e);
        }


    }


    public static String FASTHELIOSQUARTZTRIGGER_NAME = "FastHeliosQuartzTrigger";
    public static String FASTQUARTZTRIGGER_GROUP = "FAST";
    /*
     * removes the trigger from scheduling loop
     *
     */
    public void unscheduleGetStatusJob() {
        String tenantName = "";
        if (logger.isDebugEnabled()) {
            logger.debug("!FAST: unschedule running job");
        }
        if (tenantAdminService.isEnabled()) {
            tenantName = tenantService.getUserDomain(AuthenticationUtil.getRunAsUser());
            if (tenantName.length() > 0) {
                tenantName = "-" + tenantName;
            }
        }

        String triggerName = FASTHELIOSQUARTZTRIGGER_NAME + tenantName;

        try {
            scheduler.unscheduleJob(triggerName, FASTQUARTZTRIGGER_GROUP);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }


    }

    /**
     * Initialise un client HTTPS selon les paramètres donnés en entrée.
     *
     * @param properties devant contenir: server, port (pour le host), name et
     *                   password (certificat). Peut aussi contenir proxyhost, etc.
     * @return une instance de HttpClient ou null en cas de probleme.
     */
   /* private HttpClient createConnexionHTTPS(Map<String, String> properties) {
        try {
            // Voir pour le futur:      AuthSSLProtocolSocketFactory
            //  http://svn.apache.org/viewvc/httpcomponents/oac.hc3x/trunk/src/contrib/org/apache/commons/httpclient/contrib/ssl/AuthSSLProtocolSocketFactory.java?view=markup
            EasySSLProtocolSocketFactory easy = new EasySSLProtocolSocketFactory();
            InputStream is = readFile(properties.get("name"));
            if (is == null) {
                logger.warn("No certificate found, null.");
                return null;
            }
            KeyMaterial km = new KeyMaterial(is, properties.get("password").toCharArray());
            easy.setKeyMaterial(km);

            int port = Integer.parseInt(properties.get("port"));
            Protocol myhttps = new Protocol("https", (ProtocolSocketFactory) easy, port);
            HttpClient httpclient = new HttpClient();
            httpclient.getHostConfiguration().setHost(properties.get("server"), port, myhttps);

            // Valeurs de proxy: proxyhost, etc.
            setProxyingParams(httpclient, properties);

            return httpclient;

        } catch (UnrecoverableKeyException ex) {
            logger.error("HTTPS impossible, UnrecoverableKeyException", ex);
        } catch (NoSuchAlgorithmException ex) {
            logger.error("HTTPS impossible, NoSuchAlgorithmException", ex);
        } catch (KeyStoreException ex) {
            logger.error("HTTPS impossible, KeyStoreException", ex);
        } catch (KeyManagementException ex) {
            logger.error("HTTPS impossible, KeyManagementException", ex);
        } catch (IOException ex) {
            logger.error("HTTPS impossible, IOException", ex);
        } catch (CertificateException ex) {
            logger.error("HTTPS impossible, CertificateException", ex);
        } catch (Exception ex) {
            logger.error("HTTPS impossible, autre Exception", ex);
        }
        return null;
    }

    private void setProxyingParams(HttpClient httpClient, Map<String, String> properties) {
        String proxyHost = properties.get("proxyhost");
        String proxyPort = properties.get("proxyport");
        String proxyUserName = properties.get("proxyusername");
        String proxyPassword = properties.get("proxypassword");
        String hostName = properties.get("hostname");
        String domainName = properties.get("domainname");
        boolean doIt = false;

        if (proxyHost != null && !proxyHost.trim().isEmpty() &&
                proxyPort != null && !proxyPort.trim().isEmpty()) {
            httpClient.getHostConfiguration().setProxy(proxyHost, Integer.parseInt(proxyPort));
            doIt = true;
        }

        Credentials credentials = null;

        if (proxyUserName != null && !proxyUserName.trim().isEmpty() && proxyPassword != null) {
            if (hostName != null && domainName != null) {
                credentials = new NTCredentials(proxyUserName, proxyPassword, hostName, domainName);
            } else {
                credentials = new UsernamePasswordCredentials(proxyUserName, proxyPassword);
            }
        }

        if (credentials != null && doIt) {
            AuthScope authScope = new AuthScope(proxyHost, Integer.parseInt(proxyPort), properties.get("realm"));
            // httpClient.getState().setProxyCredentials(properties.get("realm"), proxyHost, credentials);
            httpClient.getState().setProxyCredentials(authScope, credentials);
            // httpClient.getState().setAuthenticationPreemptive(true);
            httpClient.getParams().setAuthenticationPreemptive(true);
        }
    }  */

    private InputStream readFile(String nameFile) throws Exception {
        UserTransaction trx = transactionService.getUserTransaction();
        InputStream inputStream = null;
        try {
            trx.begin();

            // Recherche du noeud Certificats dans Dictionary
            String xpath = "/app:company_home/app:dictionary/ph:certificats_fast";
            ResultSet result = searchService.query(new StoreRef(this.configuration.getProperty("spaces.store")), SearchService.LANGUAGE_XPATH, xpath);

            if (result.length() > 0) {
                NodeRef nodeCertificat = result.getNodeRef(0);
                List<ChildAssociationRef> childs = nodeService.getChildAssocs(nodeCertificat);
                NodeRef file = null;

                // Récupération des fichiers concernant le certificat
                for (ChildAssociationRef child : childs) {
                    String name = nodeService.getProperty(child.getChildRef(), ContentModel.PROP_NAME).toString();
                    if (name.equals(nameFile)) {
                        // Sélection du fichier demandé
                        file = child.getChildRef();
                    }
                }

                if (file != null) {
                    ContentReader content = fileFolderService.getReader(file);
                    inputStream = content.getContentInputStream();
                }
            }
            trx.commit();
            return inputStream;
        } catch (Exception e) {
            try {
                trx.rollback();
            } catch (Exception ex) {
                logger.error("Error during rollback", ex);
            }
            throw e;
        }
    }

    private File ensureMimeType(ContentReader reader, String mimeType) {
        File fic = TempFileProvider.createTempFile("s2low", null);
        // Si le type MIME correspond, on l'envoie tel quel
        if (mimeType.equals(reader.getMimetype())) {
            reader.getContent(fic);
        } // Sinon, on commence par le transformer
        else {
            FileContentWriter tmpWriter = new FileContentWriter(fic);
            tmpWriter.setMimetype(mimeType);
            tmpWriter.setEncoding(reader.getEncoding());
            this.contentService.transform(reader, tmpWriter);
        }

        return fic;
    }


    /**
     * Tell if the module is activated.
     *
     * @param properties
     * @return true if activated, false otherwise
     */
    public boolean isActivated(Map<String, String> properties) {

        if (properties != null && properties.get("activated") != null) {
            return "true".equals(properties.get("activated"));
        } else {
            return false;
        }

    }

}
