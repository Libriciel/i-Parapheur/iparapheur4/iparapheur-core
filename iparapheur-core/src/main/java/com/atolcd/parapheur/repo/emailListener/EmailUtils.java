/*
 * Copyright (C) 2006 Vardar Software, Consultancy, Training Ltd.Sti.
 * http://vardar.biz.tr
 *
 * Licensed under the Mozilla Public License version 1.1. 
 * You may obtain a copy of the License at
 *
 *   http://www.mozilla.org/MPL/MPL-1.1.txt
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the
 * License.
 */
package com.atolcd.parapheur.repo.emailListener;

import java.io.InputStream;
import java.util.Map;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;

/**
 * Utility methods to process e-mail messages
 * 
 * @author turgayz
 */
public class EmailUtils
{

    /**
     * @param m
     *                The message from which to extract headers
     * @return {@link EmailMessageHeaders}
     * @throws Exception
     */
    public static EmailMessageHeaders extractHeaders(Message m) throws Exception
    {
	EmailMessageHeaders headers = new EmailMessageHeaders();

	Address[] from = m.getFrom();
	headers.setFrom(from[0].toString());

	Address[] toAddresses = m.getRecipients(Message.RecipientType.TO);
	String to = "";
	if (toAddresses != null)
	{
	    for (int i = 0; i < toAddresses.length; i++)
	    {
		to += toAddresses[i] + ", ";
	    }
	    to = to.substring(0, to.length() - 2);
	}
	headers.setTo(to);

	Address[] ccAddresses = m.getRecipients(Message.RecipientType.CC);
	String cc = "";
	if (ccAddresses != null)
	{
	    for (int i = 0; i < ccAddresses.length; i++)
	    {
		cc += ccAddresses[i] + ", ";
	    }
	    cc = cc.substring(0, cc.length() - 2);
	}
	headers.setCc(cc);

	Address[] bccAddresses = m.getRecipients(Message.RecipientType.BCC);
	String bcc = "";
	if (bccAddresses != null)
	{
	    for (int i = 0; i < bccAddresses.length; i++)
	    {
		bcc += bccAddresses[i] + ", ";
	    }
	    bcc = bcc.substring(0, bcc.length() - 2);
	}
	headers.setBcc(bcc);

	headers.setSubject(m.getSubject());

	headers.setSentDate(m.getSentDate());
	headers.setReceivedDate(m.getReceivedDate());

	return headers;
    }

    /**
     * Extracts message body and attachments
     * 
     * @param p
     *                The {@link Part} object to extract from
     * @param attachments
     *                This map holds the attachments, keys are filenames, values are {@link Part} objects
     * @param body
     *                This map holds the message body as a String, in key "body". A map is used, because the method is
     *                called recursively.
     * @param attachmentNumber
     *                Used to name an attachment if it has no name - increased by 1 for each case
     * @throws Exception
     */
    public static void dumpPart(Part p, Map<String, Part> attachments, Map<String, String> body, int attachmentNumber)
	    throws Exception
    {
	if (!p.isMimeType("multipart/*"))
	{
	    // if this is an attachment, put it in the attachments map
	    String disposition = p.getDisposition();
	    if (disposition != null && disposition.equalsIgnoreCase(Part.ATTACHMENT))
	    {
		String fileName = p.getFileName();
		if (fileName == null || fileName.equals(""))
		    fileName = "Attachment_" + ++attachmentNumber;

		attachments.put(fileName, p);
		return;
	    }
	}

	if (p.isMimeType("text/plain"))
	{
	    // if plain text, append to body
	    String bodyStr = body.get("body-text");
	    if (!bodyStr.equals(""))
		bodyStr += "\n\n-------------------------------\n";

	    bodyStr += (String) p.getContent();
	    body.put("body-text", bodyStr);

	} else if (p.isMimeType("text/html"))
	{
	    // if the HTML content is inline, put it into the body
	    String disposition = p.getDisposition();
	    if (disposition == null)
	    {
		String bodyStr = body.get("body-html");
		if (!bodyStr.equals(""))
		    bodyStr += "\n\n-------------------------------\n";
		bodyStr += (String) p.getContent();
		body.put("body-html", bodyStr);
	    } else
	    {
		// if HTML attachment, put it in the attachments map
		String fileName = p.getFileName();
		if (fileName == null || fileName.equals(""))
		{
		    fileName = "Attachment_" + ++attachmentNumber + ".html";
		}
		attachments.put(fileName, p);
	    }

	} else if (p.isMimeType("text/xml"))
	{
	    // if XML, put it in the attachments map
	    String fileName = p.getFileName();
	    if (fileName == null || fileName.equals(""))
		fileName = "Attachment_" + ++attachmentNumber + ".xml";

	    attachments.put(fileName, p);

	} else if (p.isMimeType("application/*"))
	{
	    // if application file, put it in the attachments map
	    String fileName = p.getFileName();
	    if (fileName == null || fileName.equals(""))
		fileName = "Attachment_" + ++attachmentNumber;

	    attachments.put(fileName, p);

	} else if (p.isMimeType("image/*"))
	{
	    // if image, put it in the attachments map
	    String fileName = p.getFileName();
	    if (fileName == null || fileName.equals(""))
		fileName = "Attachment_" + ++attachmentNumber;

	    attachments.put(fileName, p);

	} else if (p.isMimeType("multipart/*"))
	{
	    // if multipart, this method will be called recursively for each of
	    // its parts
	    Multipart mp = (Multipart) p.getContent();
	    int count = mp.getCount();
	    for (int i = 0; i < count; i++)
	    {
		dumpPart(mp.getBodyPart(i), attachments, body, attachmentNumber);
	    }

	} else if (p.isMimeType("message/rfc822"))
	{
	    // if rfc822, call this method with its content as the part
	    dumpPart((Part) p.getContent(), attachments, body, attachmentNumber);

	} else
	{
	    // if we cannot identify its MIME type, check its Java type
	    Object o = p.getContent();

	    if (o instanceof String)
	    {
		// if it is a String object, append to body
		String bodyStr = body.get("body");
		if (!bodyStr.equals(""))
		    bodyStr += "\n\n-------------------------------\n";

		bodyStr += (String) o;
		body.put("body", bodyStr);

	    } else if (o instanceof InputStream)
	    {
		// if it is an InputStream object, append its contents to body
		InputStream is = (InputStream) o;
		int c;
		StringBuffer buff = new StringBuffer();
		while ((c = is.read()) != -1)
		    buff.append(c);
		String bodyStr = body.get("body");
		if (!bodyStr.equals(""))
		    bodyStr += "\n\n-------------------------------\n";

		bodyStr += buff.toString();
		body.put("body", bodyStr);

	    } else
	    {
		// if all else fails, put this in the attachments map
		String fileName = p.getFileName();
		if (fileName == null || fileName.equals(""))
		    fileName = "Attachment_" + ++attachmentNumber;

		attachments.put(fileName, p);
	    }
	}

    }

}
