/*
 * Version 3.1
 * CeCILL Copyright (c) 2010-2013, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped and maintained by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.action.executer;

import org.adullact.utils.StringUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.MailActionExecuter;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.repo.template.DateCompareMethod;
import org.alfresco.repo.template.HasAspectMethod;
import org.alfresco.repo.template.I18NMessageMethod;
import org.alfresco.repo.template.TemplateNode;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.*;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.apache.commons.validator.EmailValidator;
import org.apache.log4j.Logger;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.Serializable;
import java.util.*;


/**
 * Ce bean gère les envois d'email + pièce jointe (fonction "envoi par email").
 *
 * @author Vivien Barousse - Adullact Projet
 * @author Stephane Vast - Adullact Projet
 */
public class MailWithAttachmentActionExecuter extends MailActionExecuter {

    private static final Logger logger = Logger.getLogger(MailWithAttachmentActionExecuter.class);

    public static final String NAME = "mail-with-attachment";

    public static final String PARAM_BASEURL = "baseurl";
    public static final String PARAM_FORCEHTTPS = "forceHttps";
    public static final String PARAM_PROTOCOL = "protocol";
    public static final String PARAM_PREFIXE = "prefixe";
    public static final String PARAM_TARGETVERSION = "targetversion";
    public static final String PARAM_FOOTER = "footer";
    public static final String PARAM_ATTACHMENTS = "attachements";
    public static final String PARAM_NOTIFICATIONS = "notifications";
    public static final String PARAM_ADULLACT_MAIL_TYPE = "adullact_mail_type";
    public static final String PARAM_ADULLACT_MAIL_TYPE_NOTIF_GROUPEES = "adullact_mail_type_notif_groupees";

    private String headerEncoding;

    private PersonService personService;

    private SearchService searchService;

    private NodeService nodeService;

    private AuthorityService authorityService;

    private TemplateService templateService;

    private AuthenticationService authService;

//    private String fromAddress;

    private JavaMailSender javaMailSender;

    private ServiceRegistry serviceRegistry;

    private NamespaceService namespaceService;

    private Properties configuration;

    /**
     * @param configuration The configuration to set.
     */
    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    @Override
    public void setHeaderEncoding(String headerEncoding) {
        super.setHeaderEncoding(headerEncoding);
        this.headerEncoding = headerEncoding;
    }

    @Override
    public void setPersonService(PersonService personService) {
        super.setPersonService(personService);
        this.personService = personService;
    }

    @Override
    public void setNodeService(NodeService nodeService) {
        super.setNodeService(nodeService);
        this.nodeService = nodeService;
    }

    // @Override
    public void setSearchService(SearchService searchService) {
        //super.setSearchService(nodeService);
        this.searchService = searchService;
    }

    @Override
    public void setAuthorityService(AuthorityService authorityService) {
        super.setAuthorityService(authorityService);
        this.authorityService = authorityService;
    }

    @Override
    public void setTemplateService(TemplateService templateService) {
        super.setTemplateService(templateService);
        this.templateService = templateService;
    }

    @Override
    public void setAuthenticationService(AuthenticationService authService) {
        super.setAuthenticationService(authService);
        this.authService = authService;
    }

//    @Override
//    public void setFromAddress(String fromAddress) {
//        super.setFromAddress(fromAddress);
//        this.fromAddress = fromAddress;
//    }

    @Override
    public void setMailService(JavaMailSender javaMailSender) {
        super.setMailService(javaMailSender);
        this.javaMailSender = javaMailSender;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    @Override
    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        super.setServiceRegistry(serviceRegistry);
        this.serviceRegistry = serviceRegistry;
    }

    /*
    protected void handleAttachments(Action ruleAction) {
        Serializable attachmentsValue = ruleAction.getParameterValue(PARAM_ATTACHMENTS);
        List<File> attachments = null;
        if (attachmentsValue != null) {
            if (attachmentsValue instanceof File) {
                attachments = new ArrayList<File>(1);
                attachments.add((File) attachmentsValue);
            } else {
                attachments = (List<File>) attachmentsValue;
            }
        }

     // For each attachments add a mime part to the mail.

    }
     *
     *
     *
    */

    protected NodeRef getTemplateRefForName(String templateName) {

        NodeRef retVal = null;

        // Récupération du répertoire de templates mail
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" +
                this.configuration.getProperty("spaces.dictionary.childname") + "/" +
                this.configuration.getProperty("spaces.templates.email.childname");

        List<NodeRef> results;
        try {
            results = searchService.selectNodes(
                    nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))),
                    xpath, null, namespaceService, false);
        } catch (AccessDeniedException e) {
            logger.error("Impossible d'accéder au répertoire de templates.");
            return null;
        }
        if (results == null || results.size() != 1) {
            logger.error("Le chemin d'accès au répertoire de templates est incorrect.");
            return null;
        }

        // Récupération du template correspondant au paramètre passé
        List<ChildAssociationRef> lstEnfants = nodeService.getChildAssocs(results.get(0), ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
        for (ChildAssociationRef tmpChildAssoc : lstEnfants) {
            NodeRef tmp = tmpChildAssoc.getChildRef();
            //System.out.println(templateName+" = "+nodeService.getProperty(tmp, ContentModel.PROP_NAME));
            if (templateName.equals(nodeService.getProperty(tmp, ContentModel.PROP_NAME))) {
                retVal = tmp;
                break;
            }
        }
        return retVal;
    }


    @Override
    protected void executeImpl(
            final Action ruleAction,
            final NodeRef actionedUponNodeRef) {

         // Create the mime mail message
         MimeMessagePreparator mailPreparer = new MimeMessagePreparator() {

            @SuppressWarnings("unchecked")
            @Override
            public void prepare(MimeMessage mimeMessage) throws MessagingException {

                if (logger.isDebugEnabled()) {
                    logger.debug(ruleAction.getParameterValues());
                }

                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
                message.setValidateAddresses(false);
                // set header encoding if one has been supplied
                if (headerEncoding != null && headerEncoding.length() != 0) {
                    mimeMessage.setHeader("Content-Transfer-Encoding", headerEncoding);
                }


                String baseUrl = configuration.getProperty("Parametre." + PARAM_BASEURL);
                if (baseUrl != null) {
                    ruleAction.setParameterValue(PARAM_BASEURL, baseUrl);
                }
                else {
                    ruleAction.setParameterValue(PARAM_BASEURL, "CONFIGURER(module-context.xml):Parametre." + PARAM_BASEURL);
                }

                boolean forceHttps = Boolean.valueOf(configuration.getProperty("Parametre." + PARAM_FORCEHTTPS, "true"));

                String objetPrefixe = configuration.getProperty("Parametre." + PARAM_PREFIXE);
                if (null != objetPrefixe) {
                    ruleAction.setParameterValue(PARAM_PREFIXE, objetPrefixe);
                }
                else {
                    ruleAction.setParameterValue(PARAM_PREFIXE, "");
                }

                String targetVersion = configuration.getProperty("Parametre." + PARAM_TARGETVERSION);
                /*if (targetVersion != null) {
                    ruleAction.setParameterValue(PARAM_TARGETVERSION, targetVersion);
                } else {
                    ruleAction.setParameterValue(PARAM_TARGETVERSION, "3");
                }*/

                // set recipient
                String to = (String) ruleAction.getParameterValue(PARAM_TO);
                if (to != null && to.length() != 0) {
                    message.setTo(to);
                }
                else {
                    // see if multiple recipients have been supplied - as a list of authorities
                    Serializable authoritiesValue = ruleAction.getParameterValue(PARAM_TO_MANY);
                    List<String> authorities = null;
                    if (authoritiesValue != null) {
                        if (authoritiesValue instanceof String) {
                            authorities = new ArrayList<String>(1);
                            authorities.add((String) authoritiesValue);
                        }
                        else {
                            authorities = (List<String>) authoritiesValue;
                        }
                    }

                    if (authorities != null && !authorities.isEmpty()) {
                        List<String> recipients = new ArrayList<String>(authorities.size());
                        for (String authority : authorities) {
                            AuthorityType authType = AuthorityType.getAuthorityType(authority);
                            if (logger.isDebugEnabled()) {
                                logger.debug("authType=" + authType);
                                logger.debug("attendu=" + AuthorityType.USER);
                            }
                            if (authType.equals(AuthorityType.USER)) {
                                if (personService.personExists(authority)) {
                                    NodeRef person = personService.getPerson(authority);
                                    String address = (String) nodeService.getProperty(person, ContentModel.PROP_EMAIL);
                                    if (address != null && address.trim().length() != 0 && validateAddress(address)) {
                                        recipients.add(address);
                                    }
                                }
                                else {
                                    recipients.add(authority);
                                }
                            }
                            else if (authType.equals(AuthorityType.GROUP)) {
                                // else notify all members of the group
                                Set<String> users = authorityService.getContainedAuthorities(AuthorityType.USER, authority, false);
                                for (String userAuth : users) {
                                    if (personService.personExists(userAuth)) {
                                        NodeRef person = personService.getPerson(userAuth);
                                        String address = (String) nodeService.getProperty(person, ContentModel.PROP_EMAIL);
                                        if (address != null && address.trim().length() != 0) {
                                            recipients.add(address);
                                        }
                                    }
                                }
                            }
                        }
                        message.setTo(recipients.toArray(new String[recipients.size()]));
                    }
                    else {
                        // No recipients have been specified
                        logger.error("No recipient has been specified for the mail action");
                    }
                }

                // set subject line
                message.setSubject((String) ruleAction.getParameterValue(PARAM_PREFIXE) +
                        (String) ruleAction.getParameterValue(PARAM_SUBJECT));

                // See if an email template has been specified
                String text = null;
                String templateName = (String) ruleAction.getParameterValue(PARAM_TEMPLATE);

                NodeRef templateRef = getTemplateRefForName(templateName);

                if (templateRef != null) {
                    // build the email template model
                    Map<String, Object> model = createEmailTemplateModel(actionedUponNodeRef);

                    // Custom field iParapheur
                    String fullURL =  "http" + (forceHttps ? "s" : "") +"://" + ruleAction.getParameterValue(PARAM_BASEURL);
                    if ("4".equals(targetVersion)) {
                        fullURL += "/#/apercu/";
                    }
                    else {
                        fullURL += "/navigate/browse/" + StoreRef.STORE_REF_WORKSPACE_SPACESSTORE + "/";
                    }
                    if(ruleAction.getParameterValue(PARAM_NOTIFICATIONS) == null) {
                        fullURL += actionedUponNodeRef.getId();
                    }

                    model.put("annotation", ruleAction.getParameterValue(PARAM_TEXT)); // MailActionExecuter.PARAM_TEXT;
                    model.put("url", fullURL);
                    //model.put("targetversion", targetVersion);
                    model.put("notificationsMap", ruleAction.getParameterValue(PARAM_NOTIFICATIONS));
                    if (ruleAction.getParameterValue(PARAM_FOOTER) != null) {
                        model.put("footer", ruleAction.getParameterValue(PARAM_FOOTER));
                    }

                    // process the template against the model
                    text = templateService.processTemplate("freemarker", templateRef.toString(), model);
                }

                // set the text body of the message
                if (text == null) {
                    text = (String) ruleAction.getParameterValue(PARAM_TEXT);
                }
                message.setText(text, true);


                HashMap<String, File> attachments = (HashMap<String, File>) ruleAction.getParameterValue(PARAM_ATTACHMENTS);

                if (attachments != null) {
                    for (Map.Entry<String, File> entry : attachments.entrySet()) {
                        message.addAttachment(entry.getKey(), entry.getValue());
                    }
                }

                // Getting possibles from-addresses
                NodeRef person = null;
                try {
                    person = personService.getPerson(authService.getCurrentUserName(), false);
                // Sometimes, user "System" cannot be found and an Exception is thrown, we have to handle it
                } catch(NoSuchPersonException e) {
                }

                String mailType = (String) ruleAction.getParameterValue(MailWithAttachmentActionExecuter.PARAM_ADULLACT_MAIL_TYPE);
                boolean isNotificationGroupees = ((mailType != null) && mailType.contentEquals(MailWithAttachmentActionExecuter.PARAM_ADULLACT_MAIL_TYPE_NOTIF_GROUPEES));

                String actionFrom = (String) ruleAction.getParameterValue(MailWithAttachmentActionExecuter.PARAM_FROM);
                String fromActualUser = (person != null) ? (String) nodeService.getProperty(person, ContentModel.PROP_EMAIL) : null;
                String alfrescoGlobalPropertiesFrom = configuration.getProperty("Parametre.From");
                String defaultFrom = "ne-pas-repondre@iparapheur-template-address.org";

                // Setting appropriate "from" address

                String finalAddress;

                if (isNotificationGroupees) {
                    finalAddress = StringUtils.firstValidMailAddress(logger, alfrescoGlobalPropertiesFrom, defaultFrom);
                } else {
                    finalAddress = StringUtils.firstValidMailAddress(logger, actionFrom, fromActualUser, alfrescoGlobalPropertiesFrom, defaultFrom);
                }

                message.setFrom(finalAddress);
                ruleAction.setParameterValue(MailActionExecuter.PARAM_FROM, finalAddress);

                logger.debug("isNotificationGroupees       : " + isNotificationGroupees);
                logger.debug("mailFrom                     : " + actionFrom);
                logger.debug("alfrescoGlobalPropertiesFrom : " + alfrescoGlobalPropertiesFrom);
                logger.debug("defaultFrom                  : " + defaultFrom);
                logger.debug("fromActualUser               : " + fromActualUser);
                logger.debug("                            >> " + finalAddress);
            }
        };

        try {
            // Send the message
            javaMailSender.send(mailPreparer);
        } catch (Throwable e) {
            // don't stop the action but let admins know email is not getting sent
            String to = (String) ruleAction.getParameterValue(PARAM_TO);
            if (to == null) {
                Object obj = ruleAction.getParameterValue(PARAM_TO_MANY);
                if (obj != null) {
                    to = obj.toString();
                }
            }

            logger.error("Failed to send email to " + to, e);
        }
    }

    private boolean validateAddress(String address) {
        boolean result = false;

        EmailValidator emailValidator = EmailValidator.getInstance();
        if (emailValidator.isValid(address)) {
            result = true;
        }
        else {
            logger.error("Failed to send email to '" + address + "' as the address is incorrectly formatted");
        }

        return result;
    }

    private Map<String, Object> createEmailTemplateModel(NodeRef ref) {

        Map<String, Object> model = new HashMap<String, Object>(8, 1.0f);

        NodeRef person = personService.getPerson(AuthenticationUtil.getRunAsUser());
        model.put("person", new TemplateNode(person, serviceRegistry, null));
        model.put("document", new TemplateNode(ref, serviceRegistry, null));
        NodeRef parent = serviceRegistry.getNodeService().getPrimaryParent(ref).getParentRef();
        model.put("space", new TemplateNode(parent, serviceRegistry, null));

        // current date/time is useful to have and isn't supplied by FreeMarker by default
        model.put("date", new Date());

        // add custom method objects
        model.put("hasAspect", new HasAspectMethod());
        model.put("message", new I18NMessageMethod());
        model.put("dateCompare", new DateCompareMethod());


        return model;
    }

}
