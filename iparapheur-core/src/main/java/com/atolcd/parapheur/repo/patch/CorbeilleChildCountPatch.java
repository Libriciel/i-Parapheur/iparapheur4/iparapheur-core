/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.repo.CorbeillesService;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 *
 * @author viv
 */
public class CorbeilleChildCountPatch extends XPathBasedPatch {

    private CorbeillesService corbeillesService;

    @Override
    protected String getXPathQuery() {
        return "//*[subtypeOf('ph:parapheur')]/*";
    }

    @Override
    protected void patch(NodeRef nodeToPatch) throws Exception {
        corbeillesService.updateCorbeilleChildCount(nodeToPatch);
    }

    public void setCorbeillesService(CorbeillesService corbeillesService) {
        this.corbeillesService = corbeillesService;
    }

}
