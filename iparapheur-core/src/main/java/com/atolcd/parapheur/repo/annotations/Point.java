package com.atolcd.parapheur.repo.annotations;

import java.io.Serializable;

/**
 * Point
 *
 * @author Emmmanuel Peralta
 */
public class Point implements Serializable {
    private static final long serialVersionUID = 1L;
    Double x;
    Double y;

    public Point(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    public Double distanceTo(Point p) {
        return Math.sqrt(Math.pow(p.x - x, 2) + Math.pow(p.y - y, 2));
    }

    @Override
    public boolean equals(Object o) {
        return ((Point) o).x.equals(x) && ((Point) o).y.equals(y);
    }
}
