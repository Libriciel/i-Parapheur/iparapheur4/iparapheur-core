/*
 * Version 3.3
 * CeCILL Copyright (c) 2006-2013, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.repo;

import com.atolcd.parapheur.repo.annotations.Annotation;
import com.atolcd.parapheur.repo.annotations.Rect;
import org.alfresco.service.cmr.repository.NodeRef;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * AnnotationService
 *
 * @author Emmanuel Peralta - Adullact Projet
 */
public interface AnnotationService {

    public final static String PEN_COLOR = "pencolor";
    public final static String FILL_COLOR = "fillcolor";
    public final static String PEN_WIDTH = "penwdith";
    public final static String RECT_VALUES = "rect";
    public final static String TEXT = "text";
    public final static String PICTO = "picto";

    public final static String TYPE_RECT = "rect";
    public final static String TYPE_HIGHLIGHT = "highlight";
    public final static String TYPE_TEXT_ANNOT = "text";


    public @Nullable UUID addAnnotation(@NotNull NodeRef dossier, @NotNull NodeRef document, @NotNull Annotation a);

    public void updateAnnotation(@NotNull NodeRef dossier, @NotNull NodeRef document, @NotNull Annotation a);

    public void removeAnnotation(@NotNull NodeRef dossier, @NotNull NodeRef document, @NotNull UUID annotationUUID);

    public @NotNull HashMap<Integer, ArrayList<Annotation>> getAllDocumentAnnotations(@NotNull NodeRef dossier, @NotNull NodeRef document);

    public @NotNull List<HashMap<Integer, ArrayList<Annotation>>> getDocumentAnnotationsByEtape(@NotNull NodeRef dossier, @NotNull final NodeRef document);

    public @NotNull List<HashMap<String, HashMap<Integer, ArrayList<Annotation>>>> getAnnotationsByEtape(@NotNull NodeRef dossier);

    public @Nullable Annotation getAnnotationForUUID(@NotNull NodeRef dossier, @NotNull UUID uuid);

    public void removeAllCurrentAnnotations(@NotNull NodeRef dossier);

    /**
     * Helper method to get an instance of Annotation with the correct structure.
     */
    public Annotation getNewTextAnnotationWith(Rect r, String picto, String text, Integer page, String fullname);

    public Annotation getNewRectAnnotationWith(Rect r, String penColor, String fillColor, String text, Integer page, String fullname);

    public Annotation getNewHighlightAnnotationWith(Rect r, String penColor, String fillColor, String text, Integer page, String fullname);

    // <editor-fold desc="Deprecated methods">

    @Deprecated
    public UUID addAnnotation(NodeRef dossier, Annotation a);

    @Deprecated
    public void removeAnnotation(NodeRef dossier, UUID annotationUUID);

    @Deprecated
    public void updateAnnotation(NodeRef dossier, Annotation a);

    @Deprecated
    public HashMap<Integer, ArrayList<Annotation>> getAllAnnotations(NodeRef dossier);

    @Deprecated
    public HashMap<Integer, ArrayList<Annotation>> getCurrentAnnotations(NodeRef dossier);

    // </editor-fold desc="Deprecated methods">
}
