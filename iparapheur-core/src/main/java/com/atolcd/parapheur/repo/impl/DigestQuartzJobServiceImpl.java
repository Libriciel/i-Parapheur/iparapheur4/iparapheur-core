/*
 * Version 2.1
 * CeCILL Copyright (c) 2012-2015, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 * contact@atolcd.com
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.NotificationCenter;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.ParapheurUserPreferences;
import com.atolcd.parapheur.repo.action.executer.MailWithAttachmentActionExecuter;
import com.atolcd.parapheur.repo.action.executer.ParapheurMailDigestJob;
import org.adullact.iparapheur.repo.notification.Notification;
import org.adullact.iparapheur.repo.notification.NotificationService;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.validator.EmailValidator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.text.ParseException;
import java.util.*;

/**
 * DigestQuartzJobServiceImpl
 *
 * @author Emmanuel Peralta
 */
public class DigestQuartzJobServiceImpl {

    public void setDefaultCronString(String defaultCronString) {
        this.defaultCronString = defaultCronString;
    }
    public String getDefaultCronString() {
        return this.defaultCronString;
    }
    private static final Logger logger = Logger.getLogger(DigestQuartzJobServiceImpl.class);
    private String defaultCronString;

    private Map<String, CronTrigger> defaultTriggerMap;


    public static String DIGEST_QUARTZ_GROUP = "digestQuartzGroup";

    public static String DEFAULT_PREFIX = "Default";

    @Qualifier("tenantService")
    @Autowired
    private TenantService tenantService;
    @Qualifier("tenantAdminService")
    @Autowired
    private TenantAdminService tenantAdminService;
    @Qualifier("personService")
    @Autowired
    private PersonService personService;
    @Qualifier("transactionService")
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private NotificationCenter notificationCenter;
    @Autowired
    private ParapheurUserPreferences preferences;
    @Qualifier("nodeService")
    @Autowired
    private NodeService nodeService;

    @Qualifier("actionService")
    @Autowired
    private ActionService actionService;
    @Autowired
    private ParapheurService parapheurService;

    @Autowired
    private ParapheurUserPreferences parapheurUserPrefrences;

    @Qualifier("schedulerFactory")
    @Autowired
    private Scheduler scheduler;

    @Autowired
    private CorbeillesService corbeillesService;

    @Autowired
    private NotificationService notificationService;


    private String dumpTriggersByGroupAndName() {
        String res = "";
        try {
            String[] triggerGroupNames = scheduler.getTriggerGroupNames();


            for (String triggerGroupName : triggerGroupNames) {
                String[] triggers = scheduler.getTriggerNames(triggerGroupName);
                res += "-----------------" + triggerGroupName + "---------------------\n";
                for (String s : triggers) {
                    res += "trigger = " + triggerGroupName + ":" + s + " ";
                    Trigger t = scheduler.getTrigger(s, triggerGroupName);
                    if (t instanceof CronTrigger) {
                        res += (((CronTrigger) t).getCronExpression());
                    }
                    res += "\n";
                }
            }

            res += "--------------------------------------\n";

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return res;
    }

    private String dumpJobsByGroupAndName() {
        String res = "";
        try {
            String[] jobGroupNames = scheduler.getJobGroupNames();


            for (String jobGroupName : jobGroupNames) {
                String[] triggers = scheduler.getJobNames(jobGroupName);
                res += "-----------------" + jobGroupName + "---------------------\n";
                for (String s : triggers) {
                    JobDetail t = scheduler.getJobDetail(s, jobGroupName);
                    res += "job = " + jobGroupName + ":" + s + " " + t + "\n";
                }
            }

            res += "--------------------------------------\n";

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return res;
    }

    protected void scheduleTriggerForUsers(List<String> users, String cronString, String jobName) throws SchedulerException {

        JobDetail jobDetail = new JobDetail(jobName, DIGEST_QUARTZ_GROUP, ParapheurMailDigestJob.class);

        Map<String, Object> jobHashMap = new HashMap<String, Object>();
        jobHashMap.put("tenantService", tenantService);
        jobHashMap.put("tenantAdminService", tenantAdminService);
        jobHashMap.put("personService", personService);
        jobHashMap.put("transactionService", transactionService);
        jobHashMap.put("notificationCenter", notificationCenter);
        jobHashMap.put("parapheurUserPreferences", preferences);
        jobHashMap.put("nodeService", nodeService);
        jobHashMap.put("users", users);
        jobHashMap.put("digestQuartzJobService", this);

        jobHashMap.put("runAsUser", AuthenticationUtil.getRunAsUser());

        jobDetail.setJobDataMap(new JobDataMap(jobHashMap));
        CronTrigger retVal = new CronTrigger();
        try {
            retVal.setCronExpression(cronString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        retVal.setName(jobName);
        retVal.setGroup(DIGEST_QUARTZ_GROUP);
        retVal.setJobName(jobName);
        retVal.setJobGroup(DIGEST_QUARTZ_GROUP);
        scheduler.scheduleJob(jobDetail, retVal);


        if (logger.isDebugEnabled()) {
            logger.debug(dumpTriggersByGroupAndName());
            logger.debug(dumpJobsByGroupAndName());
        }
    }

    public void unscheduleCronJobForUser(String user) throws SchedulerException {
        // find the correctJob
        scheduler.unscheduleJob(user, DIGEST_QUARTZ_GROUP);
    }

    public void scheduleDefaultCronJob(List<String> users) throws SchedulerException {
        String currentDomain = tenantService.getUserDomain(AuthenticationUtil.getRunAsUser());
        scheduleTriggerForUsers(users, defaultCronString, DEFAULT_PREFIX + currentDomain);
    }

    public void unscheduleDefaultCrobJob() throws SchedulerException {
        String currentDomain = tenantService.getUserDomain(AuthenticationUtil.getRunAsUser());
        scheduler.unscheduleJob(DEFAULT_PREFIX + currentDomain, DIGEST_QUARTZ_GROUP);
    }

    public void scheduleCronJobForUsers(List<String> users) throws SchedulerException {
        List<String> defaultUsers = new ArrayList<String>();
        for (String user : users) {
            if (preferences.isDailyDigestEnabledForUsername(user)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("scheduling job for " + user);
                }
                String cronString = preferences.getDigestCronForUsername(user);
                if (cronString != null) {
                    List<String> userList = new ArrayList<String>();
                    userList.add(user);
                    scheduleTriggerForUsers(userList, cronString, user);
                } else {
                    defaultUsers.add(user);
                }
            }
        }

        scheduleDefaultCronJob(defaultUsers);
    }

    public void scheduleCronJobForUser(String username, String cronString) throws SchedulerException {
        List<String> userList = new ArrayList<String>();
        userList.add(username);
        scheduleTriggerForUsers(userList, cronString, username);
    }

    public void unscheduleCronJobForUsers(List<String> users) throws SchedulerException {

        for (String user : users) {
            try {
                unscheduleCronJobForUser(user);
            } catch (Exception e) {
                logger.error("Unable to unschedule job for " + user, e);
            }
        }

        unscheduleDefaultCrobJob();

        if (logger.isDebugEnabled()) {
            logger.debug(dumpTriggersByGroupAndName());
        }
    }


    protected HashMap<String, HashMap<NodeRef, ArrayList<Notification>>> buildDigestMailForUser(String username) {
        List<Notification> notifications = notificationCenter.getUnreadNotificationsForUser(username);

        Collections.sort(notifications, new NotificationComparator());
        /* Construit une HashMap NodeRef -> notification par bureau pour affichage dans le mail. */
        HashMap<String, HashMap<NodeRef, ArrayList<Notification>>> notificationHashMap =
                new HashMap<String, HashMap<NodeRef, ArrayList<Notification>>>();
        for (Notification n : notifications) {
            String rootKey = n.getRoleNotifie();

            if (rootKey != null) {
                HashMap<NodeRef, ArrayList<Notification>> rootMap ;

                if (!notificationHashMap.containsKey(rootKey)) {
                    notificationHashMap.put(rootKey, new HashMap<NodeRef, ArrayList<Notification>>());
                }

                rootMap = notificationHashMap.get(rootKey);

                NodeRef key = n.getDossier();

                if (rootMap.containsKey(key)) {
                    rootMap.get(key).add(n);
                } else {
                    ArrayList<Notification> notificationsList = new ArrayList<Notification>();
                    notificationsList.add(n);
                    rootMap.put(key, notificationsList);
                }

            }
            // Pas besoin de conserver la notification après son envoi, ça ne fait que remplir la base de données !
            //notificationCenter.markNotificationAsRead(username, n);
            notificationCenter.deleteNotification(n);
        }

        return notificationHashMap;
    }

    public void sendDigestMailToUser(String username) {

        if (personService.personExists(username)) {

            logger.setLevel(Level.INFO);
            logger.info(String.format("Sending digest mail to user %s", username));

            NodeRef person = personService.getPerson(username);
            String[] emailList = notificationCenter.getValidAddressForUser(person);

            for(NodeRef bureau : parapheurService.getOwnedParapheurs(username)) {
                List<NodeRef> dossiers = corbeillesService.getDossiersEnRetard(bureau);
                if(dossiers != null && !dossiers.isEmpty()) {
                    for(NodeRef dossier : dossiers) {
                        notificationService.notifierPourRetardSingleUser(bureau, dossier, username);
                    }
                }
            }

            HashMap<String, HashMap<NodeRef, ArrayList<Notification>>> notifications = buildDigestMailForUser(username);

            for(String email : emailList) {
                if (EmailValidator.getInstance().isValid(email)) {

                    Action mailAction = actionService.createAction(MailWithAttachmentActionExecuter.NAME);

                    if (notifications.keySet().size() > 0) {
                        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_TEMPLATE, "parapheur-digest-mail.ftl");
                        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_SUBJECT, "Notifications Groupées");
                        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_NOTIFICATIONS, notifications);
                        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_TEXT, "");
                        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_TO, email);
                        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_ADULLACT_MAIL_TYPE, MailWithAttachmentActionExecuter.PARAM_ADULLACT_MAIL_TYPE_NOTIF_GROUPEES);

                    /* "Dirty hack" pour libellé  BLEX */
                        if ("blex".equals(parapheurService.getHabillage())) {
                            mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_FOOTER, NotificationCenterImpl.FOOTER_BLEX);
                        } else {
                            mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_FOOTER, NotificationCenterImpl.FOOTER_LIBRICIEL);
                        }
                        actionService.executeAction(mailAction, person);
                    }
                }
            }

        }
    }

    private static class NotificationComparator implements Comparator<Notification> {
        @Override
        public int compare(Notification n1, Notification n2) {
            return (int) (n1.getTimestamp() - n2.getTimestamp());
        }
    }
}
