package com.atolcd.parapheur.repo.patch;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 * @author jmaire
 */
public class MailTemplateAddAllPatch extends XPathBasedPatch {
    private Properties configuration;
    private ContentService contentService;

    private List<String> templateFilenames;

    private static Log logger = LogFactory.getLog(MailTemplateAddAllPatch.class);

    public MailTemplateAddAllPatch(List<String> templateFilenames) {
        this.templateFilenames = templateFilenames;
    }


    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    @Override
    protected String getXPathQuery() {

        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/"
                + this.configuration.getProperty("spaces.dictionary.childname") + "/"
                + this.configuration.getProperty("spaces.templates.email.childname");
        return xpath;
    }

    @Override
    protected void patch(NodeRef nodeToPatch) throws Exception {
        NodeService nodeService = getNodeService();
        SearchService searchService = getSearchService();
        NamespaceService namespaceService = getNamespaceService();

        for (String templateFileName : templateFilenames) {
            String xpath = String.format("app:company_home/app:dictionary/app:email_templates/cm:%s", templateFileName);
            List<NodeRef> results = searchService.selectNodes(getRootNode(),
                    xpath,
                    null,
                    namespaceService,
                    false);

            logger.debug(String.format("Nodes Found matching template %s == %s", templateFileName, results));

            if (results == null || results.isEmpty()) {

                InputStream viewStream = getClass().getClassLoader().getResourceAsStream(String.format("alfresco/module/parapheur/bootstrap/%s", templateFileName));

                HashMap<QName, Serializable> properties = new HashMap<QName, Serializable>();
                properties.put(ContentModel.PROP_NAME, templateFileName);
                properties.put(ContentModel.PROP_TITLE, templateFileName);

                NodeRef child = nodeService.createNode(nodeToPatch, ContentModel.ASSOC_CONTAINS, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, templateFileName), ContentModel.TYPE_CONTENT, properties).getChildRef();

                ContentWriter writer = contentService.getWriter(child, ContentModel.PROP_CONTENT, true);

                writer.setEncoding("UTF-8");
                writer.setMimetype("text/plain");
                writer.putContent(viewStream);

                logger.debug("The new email template node was succesfully created");
            }

        }
    }


}
