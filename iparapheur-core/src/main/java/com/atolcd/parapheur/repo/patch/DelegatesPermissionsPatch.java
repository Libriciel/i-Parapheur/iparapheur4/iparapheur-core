/*
 * Version 2.1
 * CeCILL Copyright (c) 2008-2013, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jmaire
 * Date: 14/03/13
 * Time: 12:26
 */
public class DelegatesPermissionsPatch extends XPathBasedPatch {

    public static final String PARAPHEURS_XPATH_QUERY = "/app:company_home/ph:parapheurs/*";
    private AuthorityService authorityService;


    @Override
    protected String getXPathQuery() {
        return PARAPHEURS_XPATH_QUERY;
    }

    @Override
    protected void patch(NodeRef nodeToPatch) throws Exception {
        //Premierement, suppression des roles avec nom de bureau
        NodeService nodeService = getNodeService();
        if (!authorityService.authorityExists("ROLE_PHDELEGATES_" + nodeToPatch.getId())) {
            authorityService.createAuthority(AuthorityType.ROLE, "PHDELEGATES_" + nodeToPatch.getId());
        }
        List<AssociationRef> listAssoc = nodeService.getTargetAssocs(nodeToPatch, ParapheurModel.ASSOC_DELEGATION);
        if (listAssoc != null && listAssoc.size() == 1) {
            NodeRef delegate = listAssoc.get(0).getTargetRef();
            if (!authorityService.authorityExists("ROLE_PHDELEGATES_" + delegate.getId())) {
                authorityService.createAuthority(AuthorityType.ROLE, "PHDELEGATES_" + delegate.getId());
            }
            if (!authorityService.authorityExists("ROLE_PHOWNER_" + delegate.getId())) {
                authorityService.createAuthority(AuthorityType.ROLE, "PHOWNER_" + delegate.getId());
            }
            authorityService.addAuthority("ROLE_PHDELEGATES_" + nodeToPatch.getId(), "ROLE_PHDELEGATES_" + delegate.getId());
            authorityService.addAuthority("ROLE_PHDELEGATES_" + nodeToPatch.getId(), "ROLE_PHOWNER_" + delegate.getId());
        }
    }

    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }
}