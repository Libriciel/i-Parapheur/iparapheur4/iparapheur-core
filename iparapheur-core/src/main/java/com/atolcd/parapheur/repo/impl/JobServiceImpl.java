/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.JobService;
import com.atolcd.parapheur.repo.job.JobInterface;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;

import javax.transaction.Status;
import javax.transaction.UserTransaction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;

/**
 * @author Emmanuel Peralta - ADULLACT Projet
 */
public class JobServiceImpl implements JobService {
    private NodeService nodeService;
    private TransactionService transactionService;

    private boolean backgroundWorkEnabled;
    private Integer threadDefaultPriority;

    private TaskExecutor taskExecutor;

    protected Logger logger = Logger.getLogger(JobService.class);

    public JobServiceImpl() {

        /* Low priority ThreadFactory */
        ThreadFactory factory = new ThreadFactory() {
            public String PH_JOB_PREFIX="PH_Background_Job";
            private int jobId = 1;
            @Override
            public Thread newThread(Runnable runnable) {
                Thread nt = new Thread(runnable);
                nt.setPriority(Thread.MIN_PRIORITY);
                nt.setName(PH_JOB_PREFIX+"-"+jobId);
                jobId++;
                nt.setDaemon(true);
                return nt;
            }
        };

        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), factory);
        ConcurrentTaskExecutor concurrentTaskExecutor = new ConcurrentTaskExecutor(executor);
        taskExecutor = concurrentTaskExecutor;
    }

    @Override
    public boolean isBackgroundWorkEnabled() {
        return backgroundWorkEnabled;
    }

    public void setBackgroundWorkEnabledString(String backgroundWorkEnabled) {
        this.backgroundWorkEnabled = false;
        if (backgroundWorkEnabled != null && "true".equals(backgroundWorkEnabled)) {
            this.backgroundWorkEnabled = true;
        }
    }

    @Override
    public void lockNode(NodeRef nodeRef) {
        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Locking node %s.", nodeRef));
        }
        if (isBackgroundWorkEnabled()) {
            if(nodeService.exists(nodeRef)) {
                nodeService.setProperty(nodeRef, ParapheurModel.PROP_IN_BATCH_QUEUE, true);
            }
        }
    }

    @Override
    public void unlockNode(NodeRef nodeRef) {
        if(nodeService.exists(nodeRef)) {
            nodeService.removeProperty(nodeRef, ParapheurModel.PROP_IN_BATCH_QUEUE);
        }
    }

    @Override
    public void unlockNodeInTransaction(NodeRef nodeRef) {
        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Unlocking node %s.", nodeRef));
        }
        if (isBackgroundWorkEnabled()) {
            UserTransaction transaction = transactionService.getNonPropagatingUserTransaction();
            try {
                transaction.begin();
                unlockNode(nodeRef);
                transaction.commit();
            } catch (Exception e) {
                try {
                    if (transaction.getStatus() == Status.STATUS_ACTIVE) {
                        transaction.rollback();

                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void unlockNodes(List<NodeRef> nodeRefs) {
        if (isBackgroundWorkEnabled()) {
            UserTransaction transaction = transactionService.getNonPropagatingUserTransaction();

            try {
                transaction.begin();
                for (NodeRef nodeToUnlock : nodeRefs) {
                    nodeService.removeProperty(nodeToUnlock, ParapheurModel.PROP_IN_BATCH_QUEUE);
                }
                transaction.commit();
            } catch (Exception e) {
                try {
                    if (transaction.getStatus() == Status.STATUS_ACTIVE) {
                        transaction.rollback();
                    }
                } catch (Throwable t) {
                    //FIXME: log it
                }
            }
        }
    }

    @Override
    public void postJobAndLockNodes(JobInterface job, List<NodeRef> nodeRefList) {

        // if backgroundWorkEnabled use the thread to do the work
        if (isBackgroundWorkEnabled()) {
            postJobAndLockNodesOnThread(job, nodeRefList);
        }
        // otherwise just run it.
        else {
            job.setLockedNodeRefs(nodeRefList);
            postJob(job);
        }
    }

    private void postJobAndLockNodesOnThread(JobInterface job, List<NodeRef> nodeRefList) {
        List<NodeRef> lockedNodes = new ArrayList<NodeRef>();
        UserTransaction transaction = transactionService.getNonPropagatingUserTransaction();

        try {
            transaction.begin();

            // Trying to lock All Nodes concerned by the batch operation
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Locking nodes (%d).", nodeRefList.size()));
            }

            for (NodeRef nodeRef : nodeRefList) {
                lockNode(nodeRef);
                lockedNodes.add(nodeRef);
            }

            // if it succeed adding them to the job object for unlocking
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Inserting lockedNodes into (%s).", job));
            }
            job.setLockedNodeRefs(lockedNodes);

            transaction.commit();
        } catch (Exception e) {
            try {
                if (transaction.getStatus() == Status.STATUS_ACTIVE) {
                    transaction.rollback();
                }
            } catch (Exception e2) {
                // log exceptions ?
            }
            throw new RuntimeException("Erreur lors du verrouillage des dossiers pour le traitement par lots", e); // re-throw the exception
        }
        // adding job to the workQueue.
        // code never run when an exception occurs.
        postJob(job);
    }

    @Override
    public void postJob(JobInterface j) {

        //FIXME: do it in a proper way (just binding JobService and fetching transactionService from there ?
        // or déporting the runMethod to JobService ? it might be more workable...
        j.setTransactionService(transactionService);
        j.setJobService(this);

        if (isBackgroundWorkEnabled()) {
            postJobOnThread(j);
        } else {
            j.run();
        }
    }

    private void postJobOnThread(JobInterface j) {
        taskExecutor.execute(j);
    }


    @Override
    public void sendReportToUser(JobInterface job) {
        //TODO: implement User reporting with mailExecuter

        AtomicReference<Map<String, Object>> model = new AtomicReference<Map<String, Object>>();
        model.set(new HashMap<String, Object>());
        model.get().put("batch_exceptions", job.getExceptionsMap());
        model.get().put("batch_nodes", job.getLockedNodeRefs());

        String report = "";
        for (NodeRef dossier : job.getLockedNodeRefs()) {

            Map<NodeRef, Exception> exceptionMap = job.getExceptionsMap();
            Exception e = exceptionMap.get(dossier);

            if (e != null) {
               // String name = (String) nodeService.getProperty(dossier, ContentModel.PROP_NAME);
                report += "=====================================\n";
                report += String.format("[%s] '%s' : ", job.getUsername(), "");

                report += String.format("'%s'\n", e.toString());
                StackTraceElement[] stackTrace = e.getStackTrace();
                for (StackTraceElement element : stackTrace) {
                    report += element.toString() + "\n";
                }
            }

        }
        if (!report.isEmpty()) {
            logger.error(report);
        } else {
            logger.error(String.format("job done for %s.", job.getUsername()));
        }
    }

    // Spring dependency injection
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }
}
