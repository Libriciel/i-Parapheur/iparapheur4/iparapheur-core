/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2015, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped and maintained by ADULLACT-projet S.A.
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.repo.action.executer;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.ParapheurUserPreferences;
import org.adullact.iparapheur.repo.notification.Notification;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.MailActionExecuter;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.*;

/**
 * @deprecated , please use notificationService / NotificationCenter instead
 */
@Deprecated
public class ParapheurMailActionExecuter extends PatchedMailActionExecuter {

    public static final String TEMPLATE_CURRENT_RECEPTION = "parapheur-current-reception.ftl";
    public static final String TEMPLATE_CURRENT_ARCHIVAGE_AFTER_TDT = "parapheur-current-tdt-ok-archivage.ftl";
    public static final String TEMPLATE_OWNER_ARCHIVAGE = "parapheur-owner-archivage.ftl";
    public static final String TEMPLATE_DIFF_EMISSION = "parapheur-diffusion-emission.ftl";
    public static final String TEMPLATE_DIFF_VISA = "parapheur-diffusion-visa.ftl";
    public static final String TEMPLATE_DIFF_TDT = "parapheur-diffusion-tdt-ok.ftl";
    public static final String TEMPLATE_TIERS_ARCHIVAGE_AFTER_TDT = "parapheur-tiers-tdt-ok-archivage.ftl";
    public static final String TEMPLATE_TIERS_ARCHIVAGE = "parapheur-tiers.ftl";
    public static final String TEMPLATE_TIERS_VISA = "parapheur-tiers-visa.ftl";
    public static final String TEMPLATE_SECRETARIAT_SIGNATURE = "parapheur-secretariat-signature.ftl";

    private static final Log logger = LogFactory.getLog(ParapheurMailActionExecuter.class);

    public static final String PARAM_DEST = "dest";

    public static final String PARAM_FOOTER_LIBRICIEL = "Ce message a été envoyé par l'application i-Parapheur de Libriciel SCOP, logiciel libre sous licences AGPLv3/CeCILLv2. "
            + "Pour plus d'informations, accédez à la page suivante : "
            + "<a  href=\"http://adullact.net/projects/paraphelec/\" style=\"color: rgb(59, 89, 152); text-decoration: none;\" target=\"_blank\">http://adullact.net/projects/paraphelec/</a>.";
    public static final String PARAM_FOOTER_BLEX = "Ce message a été envoyé par l'application i-Parapheur de Berger-Levrault";

    public static final String PARAM_ANNOTATION = "annotation";

    public static final String NAME = "parapheur-mail";

    /**
     * The several needed Alfresco Services
     */
    private NodeService nodeService;
    private ParapheurService parapheurService;
    private NamespaceService namespaceService;
    private SearchService searchService;
    private TransactionService transactionService;

    private Properties configuration;
    
    @Autowired
    private ParapheurUserPreferences parapheurUserPreferences;

    /**
     * @param namespaceService The namespaceService to set.
     */
    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    /**
     * @param searchService The searchService to set.
     */
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    @Override
    public void setPersonService(PersonService personService) {
        super.setPersonService(personService);
    }

    /**
     * @param nodeService       the NodeService to set.
     */
    @Override
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
        super.setNodeService(nodeService);
    }

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    /**
     * @param configuration The configuration to set.
     */
    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    /**
     * @param transactionService The transactionService to set.
     */
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * Execute the rule action
     */
    @Override
    protected void executeImpl(Action ruleAction, NodeRef actionedUponNodeRef) {
        Assert.isTrue(parapheurService.isDossier(actionedUponNodeRef), "Cette action ne peut être déclenchée que par un noeud de type ph:dossier");
        ruleAction.setParameterValue(MailActionExecuter.PARAM_TO, null);
        ruleAction.setParameterValue(MailActionExecuter.PARAM_TEXT, " ");
        ArrayList<String> lstDestinataires = new ArrayList<String>();
        ArrayList<String> lstDestinatairesHttps = new ArrayList<String>();
        NodeRef templateRef = null;

        String mailFrom = this.configuration.getProperty("Parametre.From");
        if (null != mailFrom) {
            ruleAction.setParameterValue(MailActionExecuter.PARAM_FROM, mailFrom);
        } else {
            ruleAction.setParameterValue(MailActionExecuter.PARAM_FROM, "ne-pas-repondre@iparapheur-template-address.coop");
        }

        String baseUrl = this.configuration.getProperty("Parametre."+PARAM_BASEURL);
        if (null != baseUrl) {
            ruleAction.setParameterValue(PARAM_BASEURL, baseUrl);
        } else {
            ruleAction.setParameterValue(PARAM_BASEURL, "CONFIGURER(module-context.xml):Parametre."+PARAM_BASEURL);
        }

        String objetPrefixe = this.configuration.getProperty("Parametre."+PARAM_PREFIXE);
        if (null != objetPrefixe) {
            ruleAction.setParameterValue(PARAM_PREFIXE, objetPrefixe);
        } else {
            ruleAction.setParameterValue(PARAM_PREFIXE, "");
        }

        String targetVersion = this.configuration.getProperty("Parametre."+PARAM_TARGETVERSION);
        if (null != targetVersion) {
            ruleAction.setParameterValue(PARAM_TARGETVERSION, targetVersion);
        } else {
            /* valeur par defaut: "3" */
            ruleAction.setParameterValue(PARAM_TARGETVERSION, "3");
        }

        // Récupération du répertoire de templates mail
        String xpath = this.configuration.getProperty("spaces.company_home.childname") + "/" +
                this.configuration.getProperty("spaces.dictionary.childname") + "/" +
                this.configuration.getProperty("spaces.templates.email.childname");

        List<NodeRef> results;
        try {
            results = searchService.selectNodes(
                    nodeService.getRootNode(new StoreRef(this.configuration.getProperty("spaces.store"))),
                    xpath, null, namespaceService, false);
        } catch (AccessDeniedException e) {
            if (logger.isWarnEnabled()) {
                logger.warn("Impossible d'accéder au répertoire de templates.");
            }
            return;
        }
        if (results == null || results.size() != 1) {
            if (logger.isWarnEnabled()) {
                logger.warn("Le chemin d'accès au répertoire de templates est incorrect.");
            }
            return;
        }

        // Récupération du template correspondant au paramètre passé
        List<ChildAssociationRef> lstEnfants = nodeService.getChildAssocs(results.get(0), ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
        for (ChildAssociationRef tmpChildAssoc : lstEnfants) {
            NodeRef tmp = tmpChildAssoc.getChildRef();
            if (ruleAction.getParameterValue(PARAM_TEMPLATE).equals(nodeService.getProperty(tmp, ContentModel.PROP_NAME))) {
                templateRef = tmp;
                break;
            }
        }

        // Mail à l'émetteur du dossier
        Notification.Target target = Notification.Target.valueOf((String) ruleAction.getParameterValue(PARAM_DEST));
        switch (target) {
            case owner :
                List<String> emetteurs = parapheurService.getParapheurOwners(parapheurService.getEmetteur(actionedUponNodeRef));
                if (emetteurs != null && !emetteurs.isEmpty()) {
                    for (String emetteur : emetteurs) {
                        if (emetteur != null && !emetteur.trim().equals("")) {
                            NodeRef emetteurRef = usernameToNoderef(emetteur);
                            if (emetteurRef != null && hasValidEmail(emetteurRef)) {
                                if (hasCertificate(emetteurRef)) {
                                    lstDestinatairesHttps.add(emetteur);
                                } else {
                                    lstDestinataires.add(emetteur);
                                }
                            }
                        }
                    }
                    ruleAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Votre dossier: \"" + nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_TITLE) + "\"");
                } else {
                    if (logger.isWarnEnabled()) {
                        logger.warn("Emetteur du dossier non trouvé: id = " + actionedUponNodeRef);
                    }
                    return;
                }
                break;
            case current : // Mail à l'acteur courant du circuit de validation
                /**
                 * Dans le cas général, on récupère l'acteur courant dans le circuit
                 * de validation.
                 * Ticket helpdesk #789: le cas général ne marche pas pour les
                 * dossiers rejetés. Donc: ne pas chercher l'acteur courant, mais
                 * attraper le détenteur du dossier en retard.
                 */
                boolean isDossierRejete = parapheurService.isRejete(actionedUponNodeRef);
                if (isDossierRejete) {
                    List<String> owners = parapheurService.getParapheurOwners(parapheurService.getParentParapheur(actionedUponNodeRef));
                    if (owners != null && !owners.isEmpty()) {
                        for (String owner : owners) {
                            if (owner != null && !owner.trim().isEmpty()) {
                                NodeRef ownerRef = usernameToNoderef(owner);
                                if (ownerRef != null && hasValidEmail(ownerRef)) {
                                    if (hasCertificate(ownerRef)) {
                                        lstDestinatairesHttps.add(owner);
                                    } else {
                                        lstDestinataires.add(owner);
                                    }
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("Detenteur du dossier rejeté trouvé '" + owner + "' pour id= " + actionedUponNodeRef);
                                    }
                                }
                            }
                        }
                        ruleAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Dossier à traiter: \"" + nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_TITLE) + "\"");
                    } else {
                        if (logger.isWarnEnabled()) {
                            logger.warn("Detenteur du dossier rejeté non trouvé: id = " + actionedUponNodeRef);
                        }
                        return;
                    }
                }
                else {
                    List<String> courants = parapheurService.getActeursCourant(actionedUponNodeRef);
                    if (courants != null && !courants.isEmpty()) {
                        for (String courant : courants) {
                            if (courant != null && !courant.trim().isEmpty()) {
                                NodeRef courantRef = usernameToNoderef(courant);
                                if (courantRef != null && hasValidEmail(courantRef)) {
                                    if (hasCertificate(courantRef)) {
                                        lstDestinatairesHttps.add(courant);
                                    } else {
                                        lstDestinataires.add(courant);
                                    }
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("Acteur courant trouvé '" + courant + "' pour id= " + actionedUponNodeRef);
                                    }
                                }
                            }
                        }
                        ruleAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Dossier à traiter: \"" + nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_TITLE) + "\"");
                    } else {
                        if (logger.isWarnEnabled()) {
                            logger.warn("Acteur courant du dossier non trouvé: id = " + actionedUponNodeRef);
                        }
                        return;
                    }
                }
                break;
            case diff : // Mail à la liste de diffusion
                HashSet<String> setDest = new HashSet<String>();
                List<EtapeCircuit> lstEtapes = parapheurService.getCircuit(actionedUponNodeRef);
                //String emetteur = parapheurService.getParapheurOwner(lstEtapes.get(0).getParapheur()); String acteurCourant = parapheurService.getActeurCourant(actionedUponNodeRef);

                /**
                 * Cet mail est envoyé alors que le dossier a DEJA été déplacé avec
                 * succès vers sa nouvelle bannette. Il faut donc agir sur la liste
                 * de notification de l'étape PRECEDENTE.
                 */
                if (lstEtapes != null) {
                    EtapeCircuit etapePrecedente=null;
                    for (EtapeCircuit etape : lstEtapes) {
                        if (!etape.isApproved()) { // on a trouvé l'etape courante
                            break;                 // donc on sort de la boucle.
                        } else {
                            etapePrecedente = etape;
                        }
                    }
                    if (etapePrecedente != null) {
                        Set<NodeRef> setParapheurs = etapePrecedente.getListeNotification();
                        if (setParapheurs != null) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("### cette etape de " + etapePrecedente.getActionDemandee() +
                                    " par '" + etapePrecedente.getSignataire() +
                                    "' interesse " + setParapheurs.size() + " bureau(x)");
                            }
                            for (NodeRef paraphNodeRef : setParapheurs) {
                                List<String> owners = parapheurService.getParapheurOwners(paraphNodeRef);
                                if (owners != null && !owners.isEmpty()) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("\t### " + parapheurService.getNomParapheur(paraphNodeRef) +
                                                " has " + owners.size() + "owners, first is " + owners.get(0));
                                    }
                                    setDest.addAll(owners);
                                }
                            }
                        }
                    }

                    /**
                     * Ticket helpdesk #436 : les notifications automatiques aux acteurs du circuit ne sont plus nécessaires
                     *
                        String acteurPrecedent = parapheurService.getParapheurOwner(etapePrecedente.getParapheur());
                        // ajoute à la liste  les acteurs du circuit sauf l'émetteur, le détenteur du dossier, et l'acteur de l'étape précédente (qui viens de valider son étape)
                        for (EtapeCircuit etape : lstEtapes) {
                            String user = parapheurService.getParapheurOwner(etape.getParapheur());
                            if (acteurCourant != null) {
                                if (!acteurCourant.equals(user) && !emetteur.equals(user) && !acteurPrecedent.equals(user)) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("Diff email avec acteurCourant="+acteurCourant+", et user="+user);
                                    }
                                    setDest.add(user);
                                }
                            }
                        }
                     */
                }

                if (!setDest.isEmpty()) {
                    for (String destinataire : setDest) {
                        if (!destinataire.trim().isEmpty()) {
                            NodeRef destinataireRef = usernameToNoderef(destinataire);
                            if (destinataireRef != null && hasValidEmail(destinataireRef)) {
                                if (hasCertificate(destinataireRef)) {
                                    lstDestinatairesHttps.add(destinataire);
                                } else {
                                    lstDestinataires.add(destinataire);
                                }
                            }
                        }
                    }
                    if(ruleAction.getParameterValue(PARAM_TEMPLATE).equals("parapheur-diffusion-archive.ftl")) {
                        ruleAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Dossier archivé: \"" + nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_TITLE) + "\"");
                    } else {
                        ruleAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Dossier en cours: \"" + nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_TITLE) + "\"");
                    }
                } else {
                    if (logger.isWarnEnabled()) {
                        logger.warn("Liste de diffusion vide pour le dossier: id = " + actionedUponNodeRef);
                    }
                    return;
                }
                break;
            case secretariat : // Mail au secrétariat de l'acteur courant (impression pour signature)
                NodeRef parapheurRef = parapheurService.getParentParapheur(actionedUponNodeRef);
                List<String> secretaires = parapheurService.getSecretariatParapheur(parapheurRef);
                if (secretaires.isEmpty()) {
                    if (logger.isWarnEnabled()) {
                        logger.warn("Secrétariat responsable du dossier non trouvé: id = " + actionedUponNodeRef);
                    }
                    return;
                }

                for (String secretaire : secretaires) {
                    if (secretaire != null && !secretaire.trim().isEmpty()) {
                        NodeRef secretaireRef = usernameToNoderef(secretaire);
                        if (secretaireRef != null && hasValidEmail(secretaireRef)) {
                            if (hasCertificate(secretaireRef)) {
                                lstDestinatairesHttps.add(secretaire);
                            } else {
                                lstDestinataires.add(secretaire);
                            }
                        }
                    }
                }
                if (ruleAction.getParameterValue(PARAM_TEMPLATE).equals("parapheur-secretariat-relecture.ftl")) {
                    ruleAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Dossier à relire: \"" + nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_TITLE) + "\"");
                } else {
                    ruleAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Dossier à imprimer: \"" + nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_TITLE) + "\"");
                }
                break;

            case tiers : // Mail externe (utilisé par les émetteurs de dossiers WebServices notamment)
                String wsEmetteur = null;
                if (nodeService.getProperty(actionedUponNodeRef, ParapheurModel.PROP_WS_EMETTEUR) != null) {
                    wsEmetteur = nodeService.getProperty(actionedUponNodeRef, ParapheurModel.PROP_WS_EMETTEUR).toString();
                }
                if (wsEmetteur != null && !wsEmetteur.trim().equals("") && hasValidEmail(usernameToNoderef(wsEmetteur))) {
                    lstDestinataires.add(wsEmetteur);
                    ruleAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Votre dossier: \"" + nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_TITLE) + "\"");
                } else {
                    if (logger.isWarnEnabled()) {
                        logger.warn("ws-Emetteur du dossier non trouvé: id = " + actionedUponNodeRef);
                    }
                    return;
                }
                break;
            case next : // Mail pour les propriétaires du parapheur de l'étape suivante (et les délégués)
                List<EtapeCircuit> etapes = parapheurService.getCircuit(actionedUponNodeRef);

                if (etapes != null) {
                    EtapeCircuit etapeSuivante=null;
                    boolean stop = false;
                    for (EtapeCircuit etape : etapes) {
                        if (stop) { // vrai une étape après l'étape courante
                            etapeSuivante = etape;
                            break;
                        }
                        if (!etape.isApproved()) {
                            stop = true;
                        }
                    }
                    if (etapeSuivante != null) {
                        NodeRef parapheur = etapeSuivante.getParapheur();
                        if (parapheur != null) {
                            List<String> owners = parapheurService.getParapheurOwners(parapheur);
                            if (owners != null) {
                                Set<String> actors = new HashSet<String>(parapheurService.getUsersOnDelegationPath(parapheur));
                                actors.addAll(owners);
                                for (String destinataire : new ArrayList<String>(actors)) {
                                    if (!destinataire.trim().isEmpty()) {
                                        NodeRef destinataireRef = usernameToNoderef(destinataire);
                                        if (destinataireRef != null && hasValidEmail(destinataireRef)) {
                                            if (hasCertificate(destinataireRef)) {
                                                lstDestinatairesHttps.add(destinataire);
                                            } else {
                                                lstDestinataires.add(destinataire);
                                            }
                                        }
                                    }
                                }
                                ruleAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Dossier repris : \"" + nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_TITLE) + "\"");
                            }
                            else {
                                if (logger.isDebugEnabled()) {
                                    logger.warn("Aucun propriétaire du bureau de la prochaine étape du dossier trouvé : id = " + actionedUponNodeRef);
                                }
                            }
                        }
                    }
                }
                break;

            case delegues : // Mail aux délégués du parapheur courant
                NodeRef parapheur = parapheurService.getParentParapheur(actionedUponNodeRef);
                List<String> delegues = (parapheur != null)? parapheurService.getUsersOnDelegationPath(parapheur) : null;

                if (delegues != null && !delegues.isEmpty()) {
                    for (String delegue : delegues) {
                        if (delegue != null && !delegue.trim().isEmpty()) {
                            NodeRef delegueRef = usernameToNoderef(delegue);
                            if (delegueRef != null && hasValidEmail(delegueRef)) {
                                if (hasCertificate(delegueRef)) {
                                    lstDestinatairesHttps.add(delegue);
                                } else {
                                    lstDestinataires.add(delegue);
                                }
                                if (logger.isDebugEnabled()) {
                                    logger.debug("delegué trouvé '" + delegue + "' pour id= " + actionedUponNodeRef);
                                }
                            }
                        }
                    }
                    ruleAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Dossier en délégation : \"" + nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_TITLE) + "\"");
                    ruleAction.setParameterValue(PatchedMailActionExecuter.PARAM_TITULAIRE, parapheurService.getNomParapheur(parapheur));
                } else {
                    if (logger.isWarnEnabled()) {
                        logger.warn("Aucun délégué trouvé pour " + actionedUponNodeRef);
                    }
                    return;
                }
                break;
        }

        // Passage de paramètres et envoi du mail
        if (templateRef != null) {
            // on se sert de PARAM_TEXT pour mettre le texte de l'annotation
            if (ruleAction.getParameterValue(PARAM_ANNOTATION)!=null) {
                ruleAction.setParameterValue(MailActionExecuter.PARAM_TEXT, ruleAction.getParameterValue(PARAM_ANNOTATION));
            }
            /* "Dirty hack" pour libellé  BLEX */
            if ("blex".equals(parapheurService.getHabillage())) {
                ruleAction.setParameterValue(PatchedMailActionExecuter.PARAM_FOOTER, PARAM_FOOTER_BLEX);
            } else {
                ruleAction.setParameterValue(PatchedMailActionExecuter.PARAM_FOOTER, PARAM_FOOTER_LIBRICIEL);
            }
            ruleAction.setParameterValue(MailActionExecuter.PARAM_TEMPLATE, templateRef);

            Serializable paramBaseUrl = ruleAction.getParameterValue(PARAM_BASEURL);
            /**
             * Liens HTTP (user sans certificat)
             */
            if (!lstDestinataires.isEmpty()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Sending mail to: " + lstDestinataires);
                }
                ruleAction.setParameterValue(PARAM_BASEURL, "http://" + paramBaseUrl);
                ruleAction.setParameterValue(MailActionExecuter.PARAM_TO_MANY, lstDestinataires);
                super.executeImpl(ruleAction, actionedUponNodeRef);
            } else {
                if (logger.isWarnEnabled()) {
                    logger.warn("Pas de mail envoyable avec lien HTTP, ou destinataire(s) sans adresse email valide");
                }
            }

            /**
             * Liens HTTPS
             */
            if (!lstDestinatairesHttps.isEmpty()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Sending mail (with https link) to: " + lstDestinataires);
                }
                ruleAction.setParameterValue(PARAM_BASEURL, "https://" + paramBaseUrl);
                ruleAction.setParameterValue(MailActionExecuter.PARAM_TO_MANY, lstDestinatairesHttps);
                super.executeImpl(ruleAction, actionedUponNodeRef);
            } else {
                if (logger.isWarnEnabled()) {
                    logger.warn("Pas de mail avec lien HTTPS envoyable, ou destinataire(s) sans adresse email valide");
                }
            }
        } else {
            if (logger.isWarnEnabled()) {
                logger.warn("Template non trouvé: " + ruleAction.getParameterValue(PARAM_TEMPLATE));
            }
        }
    }

//    private boolean hasValidEmail(String candidat) {
//        boolean res = false;
//        if (AuthorityType.getAuthorityType(candidat).equals(AuthorityType.USER)) {
//            if (personService.personExists(candidat) == true) {
//                NodeRef person = personService.getPerson(candidat);
//                String address = (String) nodeService.getProperty(person, ContentModel.PROP_EMAIL);
//                if (address != null && address.trim().length() != 0
//                        && validateAddress(address.trim())) {
//                    return true;
//                }
//            }
//        }
//        return res;
//    }
    private boolean hasValidEmail(NodeRef candidatRef) {
        String address = (String) nodeService.getProperty(candidatRef, ContentModel.PROP_EMAIL);
        if (address != null && address.trim().length() != 0
                && validateAddress(address.trim()) &&
                !parapheurUserPreferences.isNotificationsEnabledForUsername(
                (String) nodeService.getProperty(candidatRef, ContentModel.PROP_USERNAME))) {
            return true;
        } else {
            return false;
        }
    }
    private boolean hasCertificate(NodeRef candidatRef) {
        boolean res = false;
        if (candidatRef != null) {
            String certifString = (String) nodeService.getProperty(candidatRef, ParapheurModel.PROP_ID_CERTIFICAT);
            if (certifString != null && !certifString.trim().isEmpty()) {
                res = true;
            }
        }
        return res;
    }
    private NodeRef usernameToNoderef(String candidat) {
        if (AuthorityType.getAuthorityType(candidat).equals(AuthorityType.USER)) {
            if (personService.personExists(candidat) == true) {
                return personService.getPerson(candidat);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Add the parameter definitions
     */
    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
        paramList.add(new ParameterDefinitionImpl(PARAM_DEST, DataTypeDefinition.TEXT, true, getParamDisplayLabel(PARAM_DEST)));
        paramList.add(new ParameterDefinitionImpl(PARAM_TEMPLATE, DataTypeDefinition.TEXT, true, getParamDisplayLabel(PARAM_TEMPLATE)));
    }

}
