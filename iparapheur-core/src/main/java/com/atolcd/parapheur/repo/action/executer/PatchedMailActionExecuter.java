/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.action.executer;

import java.io.Serializable;
import java.util.*;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.adullact.utils.StringUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.MailActionExecuter;
import org.alfresco.repo.template.DateCompareMethod;
import org.alfresco.repo.template.HasAspectMethod;
import org.alfresco.repo.template.I18NMessageMethod;
import org.alfresco.repo.template.TemplateNode;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.TemplateService;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.service.cmr.security.PersonService;
import org.apache.commons.validator.EmailValidator;
import org.apache.log4j.Logger;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

/**
 *
 * @author Vivien Barousse
 */
public class PatchedMailActionExecuter extends MailActionExecuter {

    private static final Logger logger = Logger.getLogger(PatchedMailActionExecuter.class);
    public static final String PARAM_BASEURL = "baseurl";
    public static final String PARAM_PREFIXE = "prefixe";
    public static final String PARAM_TARGETVERSION = "targetversion";
    public static final String PARAM_FOOTER = "footer";
    public static final String PARAM_TITULAIRE = "titulaire";
    private String headerEncoding;
    protected PersonService personService;
    private NodeService nodeService;
    private AuthorityService authorityService;
    private TemplateService templateService;
    private AuthenticationService authService;
    private String fromAddress;
    private JavaMailSender javaMailSender;
    private ServiceRegistry serviceRegistry;

    @Override
    public void setHeaderEncoding(String headerEncoding) {
        super.setHeaderEncoding(headerEncoding);
        this.headerEncoding = headerEncoding;
    }

    @Override
    public void setPersonService(PersonService personService) {
        super.setPersonService(personService);
        this.personService = personService;
    }

    @Override
    public void setNodeService(NodeService nodeService) {
        super.setNodeService(nodeService);
        this.nodeService = nodeService;
    }

    @Override
    public void setAuthorityService(AuthorityService authorityService) {
        super.setAuthorityService(authorityService);
        this.authorityService = authorityService;
    }

    @Override
    public void setTemplateService(TemplateService templateService) {
        super.setTemplateService(templateService);
        this.templateService = templateService;
    }

    @Override
    public void setAuthenticationService(AuthenticationService authService) {
        super.setAuthenticationService(authService);
        this.authService = authService;
    }

    @Override
    public void setFromAddress(String fromAddress) {
        super.setFromAddress(fromAddress);
        this.fromAddress = fromAddress;
    }

    @Override
    public void setMailService(JavaMailSender javaMailSender) {
        super.setMailService(javaMailSender);
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        super.setServiceRegistry(serviceRegistry);
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    protected void executeImpl(
            final Action ruleAction,
            final NodeRef actionedUponNodeRef) {
        // Create the mime mail message
        MimeMessagePreparator mailPreparer = new MimeMessagePreparator() {
            @SuppressWarnings("unchecked")
            @Override
            public void prepare(MimeMessage mimeMessage) throws MessagingException {
                if (logger.isDebugEnabled()) {
                    logger.debug(ruleAction.getParameterValues());
                }

                MimeMessageHelper message = new MimeMessageHelper(mimeMessage);

                // set header encoding if one has been supplied
                if (headerEncoding != null && headerEncoding.length() != 0) {
                    mimeMessage.setHeader("Content-Transfer-Encoding", headerEncoding);
                }

                // set recipient
                String to = (String) ruleAction.getParameterValue(PARAM_TO);
                if (to != null && to.length() != 0) {
                    message.setTo(to);
                } else {
                    // see if multiple recipients have been supplied - as a list of authorities
                    Serializable authoritiesValue = ruleAction.getParameterValue(PARAM_TO_MANY);
                    List<String> authorities = null;
                    if (authoritiesValue != null) {
                        if (authoritiesValue instanceof String) {
                            authorities = new ArrayList<String>(1);
                            authorities.add((String) authoritiesValue);
                        } else {
                            authorities = (List<String>) authoritiesValue;
                        }
                    }

                    if (authorities != null && !authorities.isEmpty()) {
                        List<String> recipients = new ArrayList<String>(authorities.size());
                        for (String authority : authorities) {
                            AuthorityType authType = AuthorityType.getAuthorityType(authority);
                            if (authType.equals(AuthorityType.USER)) {
                                if (personService.personExists(authority) == true) {
                                    NodeRef person = personService.getPerson(authority);
                                    String address = (String) nodeService.getProperty(person, ContentModel.PROP_EMAIL);
                                    if (address != null && address.trim().length() != 0 && validateAddress(address.trim())) {
                                        recipients.add(address.trim());
                                    }
                                }
                            } else if (authType.equals(AuthorityType.GROUP)) {
                                // else notify all members of the group
                                Set<String> users = authorityService.getContainedAuthorities(AuthorityType.USER, authority, false);
                                for (String userAuth : users) {
                                    if (personService.personExists(userAuth) == true) {
                                        NodeRef person = personService.getPerson(userAuth);
                                        String address = (String) nodeService.getProperty(person, ContentModel.PROP_EMAIL);
                                        if (address != null && address.trim().length() != 0 && validateAddress(address.trim())) {
                                            recipients.add(address.trim());
                                        }
                                    }
                                }
                            }
                        }

                        if (!recipients.isEmpty()) {
                            message.setTo(recipients.toArray(new String[recipients.size()]));
                        } else {
                            logger.error("No recipiant actually have any valid email address");
                        }
                    } else {
                        // No recipiants have been specified
                        logger.error("No recipiant has been specified for the mail action");
                    }
                }

                // set subject line
                message.setSubject((String) ruleAction.getParameterValue(PARAM_PREFIXE)
                        + (String) ruleAction.getParameterValue(PARAM_SUBJECT));

                // See if an email template has been specified
                String text = null;
                NodeRef templateRef = (NodeRef) ruleAction.getParameterValue(PARAM_TEMPLATE);
                if (templateRef != null) {
                    // build the email template model
                    Map<String, Object> model = createEmailTemplateModel(actionedUponNodeRef);

                    // Custom fields i-Parapheur
                    model.put("annotation", ruleAction.getParameterValue(PARAM_TEXT)); // MailActionExecuter.PARAM_TEXT;
                    model.put("baseurl", ruleAction.getParameterValue(PARAM_BASEURL));
                    if (ruleAction.getParameterValue(PARAM_TARGETVERSION) != null) {
                        model.put("targetversion", ruleAction.getParameterValue(PARAM_TARGETVERSION));
                    }
                    if (ruleAction.getParameterValue(PARAM_FOOTER) != null) {
                        model.put("footer", ruleAction.getParameterValue(PARAM_FOOTER));
                    }
                    if (ruleAction.getParameterValue(PARAM_TITULAIRE) != null) {
                        model.put(PARAM_TITULAIRE, ruleAction.getParameterValue(PARAM_TITULAIRE));
                    }

                    // process the template against the model
                    text = templateService.processTemplate("freemarker", templateRef.toString(), model);
                }

                // set the text body of the message
                if (text == null) {
                    text = (String) ruleAction.getParameterValue(PARAM_TEXT);
                }
                message.setText(text, true);

                // Getting possibles "from" addresses

                NodeRef person = personService.getPerson(authService.getCurrentUserName(), false);
                String fromActualUser = (person != null) ? (String) nodeService.getProperty(person, ContentModel.PROP_EMAIL) : null;
                String ruleActionFrom = (String) ruleAction.getParameterValue(PARAM_FROM);
                String defaultFrom = "ne-pas-repondre@iparapheur-template-address.org";

                // Setting appropriate "from" address

                String finalAddress = StringUtils.firstValidMailAddress(logger, fromActualUser, ruleActionFrom, fromAddress, defaultFrom);
                message.setFrom(finalAddress);

                logger.debug("fromActualUser               : " + fromActualUser);
                logger.debug("ruleActionFrom               : " + ruleActionFrom);
                logger.debug("fromAddress                  : " + fromAddress);
                logger.debug("defaultFrom                  : " + defaultFrom);
                logger.debug("                             > " + finalAddress);
            }
        };

        try {
            // Send the message
            javaMailSender.send(mailPreparer);
        } catch (Throwable e) {
            // don't stop the action but let admins know email is not getting sent
            String to = (String) ruleAction.getParameterValue(PARAM_TO);
            if (to == null) {
                Object obj = ruleAction.getParameterValue(PARAM_TO_MANY);
                if (obj != null) {
                    to = obj.toString();
                }
            }

            // BLEX { : j'en peux plus de ces stack-traces dans les logs

            if (logger.isDebugEnabled()) {
                logger.debug("Failed to send email to " + to, e);
            } else {
                String message = "Erreur envoi mail a  [" + to + "]";
                if (e.getMessage() != null) {
                    message += ",\n\t [" + e.getMessage() + "]";
                } else {
                    message += ", [" + e.getClass().getName() + "]";
                }
                if (e.getCause() != null) {
                    if (e.getCause().getMessage() != null) {
                        message += "\n\tcause: [" + e.getCause().getMessage() + "]";
                    } else {
                        message += "\n\tcause: [" + e.getCause().getClass().getName() + "]";
                    }
                }
                logger.error(message);
            }
            // BLEX }
        }
    }

    protected boolean validateAddress(String address) {
        boolean result = false;

        EmailValidator emailValidator = EmailValidator.getInstance();
        if (emailValidator.isValid(address)) {
            result = true;
        } else {
            logger.error("Failed to send email from/to '" + address + "' as the address is incorrectly formatted");
        }

        return result;
    }

    private Map<String, Object> createEmailTemplateModel(NodeRef ref) {
        Map<String, Object> model = new HashMap<String, Object>(10, 1.0f);

        NodeRef person = personService.getPerson(authService.getCurrentUserName());
        model.put("person", new TemplateNode(person, serviceRegistry, null));
        model.put("document", new TemplateNode(ref, serviceRegistry, null));
        NodeRef parent = serviceRegistry.getNodeService().getPrimaryParent(ref).getParentRef();
        model.put("space", new TemplateNode(parent, serviceRegistry, null));

        // current date/time is useful to have and isn't supplied by FreeMarker by default
        model.put("date", new Date());

        // add custom method objects
        model.put("hasAspect", new HasAspectMethod());
        model.put("message", new I18NMessageMethod());
        model.put("dateCompare", new DateCompareMethod());

        return model;
    }
}
