/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atolcd.parapheur.repo.bootstrap;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.alfresco.repo.importer.ImporterBootstrap;
import org.alfresco.repo.module.AbstractModuleComponent;

import org.alfresco.repo.module.ImporterModuleComponent;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.util.PropertyCheck;

/**
 *
 * @author eperalta
 */
public class ConditionalBootstrapper  extends AbstractModuleComponent {
    private ImporterBootstrap importer;
    private Properties bootstrapView;
    private List<Properties> bootstrapViews;
    private SearchService searchService;
    private NodeService nodeService;
    private NamespaceService namespaceService;
    
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }
   

    /**
     * Set the helper that has details of the store to load the data into.
     * Alfresco has a set of predefined importers for all the common stores in use.
     *
     * @param importer the bootstrap bean that performs the store bootstrap.
     */
    public void setImporter(ImporterBootstrap importer)
    {
        this.importer = importer;
    }

    /**
     * Set a list of bootstrap views to import.<br/>
     * This is an alternative to {@link #setBootstrapViews(List)}.
     *
     * @param bootstrapView the bootstrap data location
     *
     * @see ImporterBootstrap#setBootstrapViews(List)
     */
    public void setBootstrapView(Properties bootstrapView)
    {
        this.bootstrapView = bootstrapView;
    }

    /**
     * Set a list of bootstrap views to import.<br/>
     * This is an alternative to {@link #setBootstrapView(Properties)}.
     *
     * @param bootstrapViews the bootstrap data locations
     *
     * @see ImporterBootstrap#setBootstrapViews(List)
     */
    public void setBootstrapViews(List<Properties> bootstrapViews)
    {
        this.bootstrapViews = bootstrapViews;
    }

    @Override
    protected void checkProperties()
    {
        PropertyCheck.mandatory(this, "importerBootstrap", importer);
        if (bootstrapView == null && bootstrapViews == null)
        {
            PropertyCheck.mandatory(this, null, "bootstrapViews or bootstrapView");
        }
        // fulfil contract of override
        super.checkProperties();
    }

    @Override
    protected void executeInternal() throws Throwable
    {
        // Construct the bootstrap views
        List<Properties> views = new ArrayList<Properties>(1);
        if (bootstrapViews != null)
        {
            views.addAll(bootstrapViews);
        }
        if (bootstrapView != null)
        {
            views.add(bootstrapView);
        }

        views = filterBootstrapViews(views);


        // modify the bootstrapper
        importer.setBootstrapViews(views);
        importer.setUseExistingStore(true);              // allow import into existing store

        importer.bootstrap();
        // Done
    }

    private List<Properties> filterBootstrapViews(List<Properties> views) {
        List<Properties> retVal = new ArrayList<Properties>();

        for (Properties view : views) {
            if (view.getProperty("checkPath") != null) {
                String checkPath = view.getProperty("checkPath");
                StoreRef storeRef = importer.getStoreRef();
                NodeRef rootNodeRef = nodeService.getRootNode(storeRef);

                List<NodeRef> results = searchService.selectNodes(
                        rootNodeRef,
                        checkPath,
                        null,
                        namespaceService,
                        false);
                
                if (results.size() > 1) {
                    throw new RuntimeException("Résultats multiples pour " + checkPath);
                } else if (results.size() == 0) {
                    retVal.add(view);
                }
            } else {
                retVal.add(view);
            }
        }

        return retVal;
    }
}
/*

    @Override
    protected String applyInternal() throws Exception
    {
        StoreRef storeRef = importerBootstrap.getStoreRef();
        NodeRef rootNodeRef = nodeService.getRootNode(storeRef);
        if (checkPath != null)
        {
            List<NodeRef> results = searchService.selectNodes(
                    rootNodeRef,
                    checkPath,
                    null,
                    namespaceService,
                    false);
            if (results.size() > 1)
            {
                throw new PatchException(ERR_MULTIPLE_FOUND, checkPath);
            }
            else if (results.size() == 1)
            {
                // nothing to do - it exsists
                return I18NUtil.getMessage(MSG_EXISTS, checkPath);

            }
        }
        String path = bootstrapView.getProperty("path");
        List<Properties> bootstrapViews = new ArrayList<Properties>(1);
        bootstrapViews.add(bootstrapView);
        // modify the bootstrapper
        importerBootstrap.setBootstrapViews(bootstrapViews);
        importerBootstrap.setUseExistingStore(true);              // allow import into existing store

        importerBootstrap.bootstrap();
        // done
        return I18NUtil.getMessage(MSG_CREATED, path, rootNodeRef);
    }
}
*/