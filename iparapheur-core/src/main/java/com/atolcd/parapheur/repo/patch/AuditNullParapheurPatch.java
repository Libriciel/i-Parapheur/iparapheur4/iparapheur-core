/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.repo.patch;

import fr.starxpert.iparapheur.audit.cmr.AuditParapheurService;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.alfresco.repo.admin.patch.AbstractPatch;
import org.alfresco.repo.domain.audit.AuditDAO;
import org.alfresco.service.cmr.audit.AuditQueryParameters;

import org.alfresco.service.cmr.audit.AuditService;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * @author eperalta
 */
public class AuditNullParapheurPatch extends AbstractPatch {

    private AuditService auditService;
    private AuditDAO auditDAO;
    private AuditMigrationPatch auditMigrationPatch;

    @Override
    protected String applyInternal() throws Exception {
        int count;
        //auditMigrationPatch.breakPatch = false;
        AuditQueryParameters aqp = new AuditQueryParameters();
        aqp.setApplicationName(AuditParapheurService.AUDIT_INSTRUIT_APPLICATION);
        aqp.addSearchKey(AuditParapheurService.AUDIT_INSTRUIT_PARAPHEUR, null);

        RemoveAndAddCallback callback = new RemoveAndAddCallback();

        callback.setAuditMigrationPatch(auditMigrationPatch);

        auditService.auditQuery(callback, aqp, 0);

        auditDAO.deleteAuditEntries(callback.getAuditEntriesToDelete());
        count = callback.getAuditEntriesToDelete().size();

        aqp = new AuditQueryParameters();
        aqp.setApplicationName(AuditParapheurService.AUDIT_REJET_APPLICATION);
        aqp.addSearchKey(AuditParapheurService.AUDIT_REJET_PARAPHEUR, null);

        callback = new RemoveAndAddCallback();

        callback.setAuditMigrationPatch(auditMigrationPatch);

        auditService.auditQuery(callback, aqp, 0);

        auditDAO.deleteAuditEntries(callback.getAuditEntriesToDelete());
        count += callback.getAuditEntriesToDelete().size();

        return "Fixed " + count + " Null parapheurs from audit Import";
    }

    public AuditDAO getAuditDAO() {
        return auditDAO;
    }

    public void setAuditDAO(AuditDAO auditDAO) {
        this.auditDAO = auditDAO;
    }

    public AuditMigrationPatch getAuditMigrationPatch() {
        return auditMigrationPatch;
    }

    public void setAuditMigrationPatch(AuditMigrationPatch auditMigrationPatch) {
        this.auditMigrationPatch = auditMigrationPatch;
    }

    public AuditService getAuditService() {
        return auditService;
    }

    public void setAuditService(AuditService auditService) {
        this.auditService = auditService;
    }


    class RemoveAndAddCallback implements AuditService.AuditQueryCallback {

        List<Long> auditEntriesToDelete;
        AuditMigrationPatch auditMigrationPatch;

        public RemoveAndAddCallback() {
            this.auditEntriesToDelete = new ArrayList<Long>();
        }


        public boolean valuesRequired() {
            return true;
        }

        private boolean fetchAndHandle(NodeRef nodeRef, long ts) {

            String query = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Calendar gregorian = new GregorianCalendar();
            gregorian.setTimeInMillis(ts);
            Date date = gregorian.getTime();

            String timestamp = sdf.format(date);

            query = "SELECT aas.method, aaf.* FROM alf_audit_fact aaf, alf_audit_source aas "
                    + "WHERE aas.id=aaf.audit_source_id AND aas.application='ParapheurService' "
                    + "AND aas.service='ParapheurService' AND aaf.fail=false AND aaf.node_uuid='" + nodeRef.getId() + "' AND aaf.timestamp='" + timestamp + "' ORDER BY aaf.timestamp";
            int count = 0;

            System.out.println(query);
            try {
                Connection connection = auditMigrationPatch.getDatabaseConnection();
                Statement st = connection.createStatement();
                ResultSet rs = st.executeQuery(query);
                count = auditMigrationPatch.handleAuditStatsResultSet(rs);
                System.out.println("Audit Count=" + count);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return count > 0;

        }

        public boolean handleAuditEntry(Long entryId, String applicationName, String user, long time, Map<String, Serializable> map) {

            NodeRef nodeRef = null;
            NodeRef parapheurRef = null;

            System.out.println(String.format("id:%d an:%s ur:%s ts:%d map:%s", entryId, applicationName, user, time, map));
            if (map != null) {
                if (applicationName.equals(AuditParapheurService.AUDIT_INSTRUIT_APPLICATION)) {
                    nodeRef = (NodeRef) map.get(AuditParapheurService.AUDIT_INSTRUIT_NODEREF);
                    parapheurRef = (NodeRef) map.get(AuditParapheurService.AUDIT_INSTRUIT_PARAPHEUR);
                } else if (applicationName.equals(AuditParapheurService.AUDIT_REJET_APPLICATION)) {
                    nodeRef = (NodeRef) map.get(AuditParapheurService.AUDIT_REJET_NODEREF);
                    parapheurRef = (NodeRef) map.get(AuditParapheurService.AUDIT_REJET_PARAPHEUR);
                }
            }

            if (nodeRef != null && parapheurRef == null) {
                if (fetchAndHandle(nodeRef, time)) {
                    auditEntriesToDelete.add(entryId);
                }
            }
            return true;
        }

        public boolean handleAuditEntryError(Long l, String string, Throwable thrwbl) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<Long> getAuditEntriesToDelete() {
            return auditEntriesToDelete;
        }

        public void setAuditEntriesToDelete(List<Long> auditEntriesToDelete) {
            this.auditEntriesToDelete = auditEntriesToDelete;
        }

        public AuditMigrationPatch getAuditMigrationPatch() {
            return auditMigrationPatch;
        }

        public void setAuditMigrationPatch(AuditMigrationPatch auditMigrationPatch) {
            this.auditMigrationPatch = auditMigrationPatch;
        }

    }

}
