/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.patch;

import com.atolcd.parapheur.model.ParapheurModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.UserTransaction;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.module.AbstractModuleComponent;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * 
 * @author Vivien Barousse
 * @since 3.2
 */
public class WorkflowsLocation32Patch extends AbstractModuleComponent {

    private static final Logger logger = Logger.getLogger(WorkflowsLocation32Patch.class);

    public static final String WORKFLOWS_LOCATION = "/app:company_home/app:dictionary/ph:savedworkflows";

    private String store;

    private NodeService nodeService;

    private ContentService contentService;

    private SearchService searchService;

    private TransactionService transactionService;

    private NamespaceService namespaceService;

    @Override
    protected void executeInternal() throws Throwable {
        doPatchImpl();
    }

    private void doPatchImpl() {
        StoreRef storeRef = new StoreRef(store);

        UserTransaction utx = transactionService.getUserTransaction();
        try {
            utx.begin();

            NodeRef rootNode = nodeService.getRootNode(storeRef);

            List<NodeRef> workflowsLocations = searchService.selectNodes(rootNode,
                    WORKFLOWS_LOCATION,
                    null,
                    namespaceService,
                    false);

            if (workflowsLocations.size() > 0) {
                NodeRef workflowsLocation = workflowsLocations.get(0);

                List<ChildAssociationRef> children = nodeService.getChildAssocs(workflowsLocation);
                for (ChildAssociationRef child : children) {
                    NodeRef childRef = child.getChildRef();
                    QName childType = nodeService.getType(childRef);
                    if (childType.equals(ContentModel.TYPE_FOLDER)) {
                        moveWorkflows(childRef, workflowsLocation);
                        nodeService.deleteNode(childRef);
                    } else if (childType.equals(ContentModel.TYPE_CONTENT)) {
                        nodeService.setType(childRef, ParapheurModel.TYPE_SAVED_WORKFLOW);
                    }
                }
            }

            utx.commit();
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
                logger.error("Unexpected error during rollback", ex);
            }
            throw new RuntimeException("Patch \"Workflows Location 3.0.4\" failed", e);
        }
    }

    private void moveWorkflows(NodeRef srcFolder, NodeRef dstFolder) {
        List<ChildAssociationRef> workflows = nodeService.getChildAssocs(srcFolder);
        for (ChildAssociationRef workflow : workflows) {
            NodeRef workflowRef = workflow.getChildRef();

            /* Lecture du nom du dossier qui ici est le nom d'utilisateur qui
             * servira de suffixe au nom du circuit.
             */
            String username = (String)nodeService.getProperty(srcFolder, ContentModel.PROP_NAME);
            moveWorkflow(workflowRef, dstFolder, username);
        }
    }

    private void moveWorkflow(NodeRef workflow, NodeRef dstFolder, String username) {
        QName childQName = nodeService.getPrimaryParent(workflow).getQName();
       
        /* Dans le cas de circuits personnels on suffixe avec le nom du dossier 
         * (ici c'est l'username du proprietaire)
         */
        String newLocalName = nodeService.getProperty(workflow, ContentModel.PROP_NAME)+ "_"+username;
        childQName = QName.createQName(childQName.getNamespaceURI(), newLocalName);
        nodeService.setProperty(workflow, ContentModel.PROP_NAME, newLocalName);
        
        nodeService.moveNode(workflow, dstFolder, ContentModel.ASSOC_CONTAINS, childQName);
        nodeService.setType(workflow, ParapheurModel.TYPE_SAVED_WORKFLOW);
        makePrivate(workflow);
    }

    private void makePrivate(NodeRef workflow) {
        ContentReader reader = contentService.getReader(workflow, ContentModel.PROP_CONTENT);

        try {
            SAXReader saxReader = new SAXReader();
            Document document = saxReader.read(reader.getContentInputStream());

            Element rootEl = document.getRootElement();
            Element aclEl = rootEl.element("acl");
            Element groupsEl = rootEl.element("groupes");

            ArrayList<String> parapheurs = new ArrayList<String>();
            ArrayList<String> groups = new ArrayList<String>();

            for (Object parapheurOb : aclEl.elements("parapheur")) {
                Element parapheurEl = (Element) parapheurOb;
                String parapheur = parapheurEl.getTextTrim();
                parapheurs.add(parapheur);
            }

            for (Object groupOb : groupsEl.elements("groupe")) {
                Element groupEl = (Element) groupOb;
                String group = groupEl.getTextTrim();
                groups.add(group);
            }

            Map<QName, Serializable> aspectProps = new HashMap<QName, Serializable>();
            aspectProps.put(ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS, parapheurs);
            aspectProps.put(ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_GROUPS, groups);

            nodeService.addAspect(workflow, ParapheurModel.ASPECT_PRIVATE_WORKFLOW, aspectProps);
        } catch (Exception e) {
            throw new RuntimeException("Unable to parse workflow content", e);
        }
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

}
