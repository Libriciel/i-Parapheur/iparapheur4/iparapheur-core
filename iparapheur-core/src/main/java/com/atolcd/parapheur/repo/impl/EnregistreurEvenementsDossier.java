/*
 * Version 3.2
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.domain.audit.AuditDAO;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;


/**
 * Une classe utilitaire permettant d'enregistrer des evenements historiques
 * sur un dossier, dans certaines conditions lies aux deplacement de dossiers
 * et aux operations de delegation de parapheur.
 *
 *                                                                            <br><br><u>
 * Exceptions                                                                 </u><br>
 * Les methodes publiques de cette classe ne <b>generent pas d'exception</b> en
 * cas d'anomalie. Les exceptions sont simplement tracees.
 *
 * @author BLEX
 */
public class EnregistreurEvenementsDossier {

    /* la clef utilisee pour enregistrer les evenements historiques */
    public static final String APPLICATION_NAME = "ParapheurServiceCompat";

    private static Logger logger = Logger.getLogger(EnregistreurEvenementsDossier.class);

    private NodeService nodeService;
    private AuditDAO auditDAO;
    private AuthenticationService authenticationService;

    /**
     * Creation d'un enregistreur
     * Passage en parametres des services necessaires aux traitements
     *
     */
    public EnregistreurEvenementsDossier(NodeService nodeService,
            AuthenticationService authenticationService, AuditDAO auditDAO) {
        Assert.notNull(nodeService);
        Assert.notNull(authenticationService);
        Assert.notNull(auditDAO);

        this.nodeService = nodeService;
        this.auditDAO = auditDAO;
        this.authenticationService = authenticationService;
    }

    /**
     * Enregistrement d'evenements lorsqu'un dossier vient d'etre deplace
     * depuis un parapheur vers un autre
     *
     *                                                                         <br><br><u>
     * Exception                                                               </u><br>
     * Ce traitement ne genere pas d'exception en cas d'anomalie
     * (les exceptions sont simplement tracees)
     *
     * @param dossier
     *        le dossier deplace
     *
     * @param parapheurDestination
     *        le bureau destination
     */
    public void onDeplacementDossier(NodeRef dossier, NodeRef parapheurDestination) {

        String message = "Deplacement du dossier vers le bureau "
                + nodeService.getProperty(parapheurDestination, ContentModel.PROP_TITLE);

        doEnregistreEvenementDossier(dossier, message);
    }

    /**
     * Enregistrement d'un evenement historique pour un dossier
     *
     * @param dossier le dossier concernt
     * @param message le message a inscrire
     * @param etatDossier l'etat ("status") du dossier a inscrire en regard de l'evenement
     */
    void doEnregistreEvenementDossier(NodeRef dossier, String message, String etatDossier) {
        List<Object> list = new ArrayList<Object>();
        list.add(etatDossier);

        if (logger.isDebugEnabled()) {
            logger.debug("dossier=" + dossier + ", message=" + message + ", etatDossier=" + etatDossier);
        }

        AuditDAO.AuditApplicationInfo application = auditDAO.getAuditApplication(APPLICATION_NAME);
        Calendar cal = Calendar.getInstance();

        Map<String, Serializable> values = new HashMap<String, Serializable>();

        values.put("message", message);
        values.put("dossier", dossier);
        values.put("list", (Serializable) list);

        auditDAO.createAuditEntry(application.getId(), cal.getTimeInMillis(), this.authenticationService.getCurrentUserName(), values);
    }

    /**
     * Enregistrement d'un evenement historique pour un dossier.
     * L'etat courant du dossier sera inscrit en regard de l'evenement
     *
     * @param dossier le dossier concernt
     * @param message le message a inscrire
     */
    void doEnregistreEvenementDossier(NodeRef dossier, String message) {
        doEnregistreEvenementDossier(dossier, message,
                (String) nodeService.getProperty(dossier, ParapheurModel.PROP_STATUS_METIER));
    }

}
