/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atolcd.parapheur.repo.impl.exceptions;

/**
 *
 * @author Mathieu Passenaud
 */
public class SubTypeNotFoundRuntimeException extends RuntimeException{
    
    public SubTypeNotFoundRuntimeException(String message){
        super(message);
    }
}
