package com.atolcd.parapheur.repo.impl.exceptions;

/**
 * Created by lhameury on 30/09/15.
 */
public class UnknownMailsecTransactionException extends Exception {

    public UnknownMailsecTransactionException(String message){
        super(message);
    }
}
