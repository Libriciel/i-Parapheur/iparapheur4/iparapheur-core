/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.ui.repo.component.shelf;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.faces.component.NamingContainer;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.el.MethodBinding;
import javax.faces.el.ValueBinding;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.FacesEvent;
import org.alfresco.web.bean.repository.MapNode;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.ui.common.Utils;
import org.alfresco.web.ui.repo.WebResources;
import org.alfresco.web.ui.repo.component.shelf.UIShelfItem;

/**
 * JSF Component providing UI for a list of corbeilles.
 * 
 * @author bma
 */
public class UICorbeillesShelfItem extends UIShelfItem {

    private final static int ACTION_CLICK_ITEM = 0;

    private Object value = null;

    private MethodBinding clickActionListener;

    /**
     * @see javax.faces.component.StateHolder#restoreState(javax.faces.context.FacesContext, java.lang.Object)
     */
    @Override
    public void restoreState(FacesContext context, Object state) {
        Object values[] = (Object[]) state;
        // standard component attributes are restored by the super class
        super.restoreState(context, values[0]);
        this.value = values[1];
        this.clickActionListener = (MethodBinding) values[2];
    }

    /**
     * @see javax.faces.component.StateHolder#saveState(javax.faces.context.FacesContext)
     */
    @Override
    public Object saveState(FacesContext context) {
        Object values[] = new Object[4];
        // standard component attributes are saved by the super class
        values[0] = super.saveState(context);
        values[1] = this.value;
        values[2] = this.clickActionListener;

        return (values);
    }

    /**
     * Get the value (for this component the value is used as the List of corbeilles nodes)
     *
     * @return the value
     */
    public Object getValue() {
        //if (this.value == null) {
            ValueBinding vb = getValueBinding("value");
            if (vb != null) {
                this.value = vb.getValue(getFacesContext());
            }
        //}
        return this.value;
    }

    /**
     * Set the value (for this component the value is used as the List of corbeilles nodes)
     *
     * @param value     the value
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * @param binding    The MethodBinding to call when Click is performed by the user
     */
    public void setClickActionListener(MethodBinding binding) {
        this.clickActionListener = binding;
    }

    /**
     * @return The MethodBinding to call when Click is performed by the user
     */
    public MethodBinding getClickActionListener() {
        return this.clickActionListener;
    }

    /**
     * @see javax.faces.component.UIComponentBase#decode(javax.faces.context.FacesContext)
     */
    @Override
    public void decode(FacesContext context) {
        Map requestMap = context.getExternalContext().getRequestParameterMap();
        String fieldId = getHiddenFieldName();
        String valueId = (String) requestMap.get(fieldId);

        if (valueId != null && valueId.length() != 0) {
            // decode the values - we are expecting an action identifier and an index
            int sepIndex = valueId.indexOf(NamingContainer.SEPARATOR_CHAR);
            int action = Integer.parseInt(valueId.substring(0, sepIndex));
            int index = Integer.parseInt(valueId.substring(sepIndex + 1));

            // raise an event to process the action later in the lifecycle
            CorbeilleEvent event = new CorbeilleEvent(this, action, index);
            this.queueEvent(event);
        }
    }

    /**
     * @see javax.faces.component.UIComponentBase#encodeBegin(javax.faces.context.FacesContext)
     */
    @Override
    @SuppressWarnings("unchecked")
    public void encodeBegin(FacesContext context) throws IOException {
        if (isRendered() == false) {
            return;
        }

        ResponseWriter out = context.getResponseWriter();
        List<Map> items = (List<Map>) getValue();
        int nb;
        out.write(SHELF_START);
        if (items != null) {
            for (int i = 0; i < items.size(); i++) {
                out.write("<tr><td width=100%><table>");

                out.write("<tr><td width=16>");
                MapNode item = (MapNode) items.get(i);
                Boolean virtuelle = (Boolean) item.get("virtuelle");

                out.write(Utils.buildImageTag(context,
                        virtuelle ? WebResources.IMAGE_INFO : WebResources.IMAGE_SPACE,
                        16, 16, null, null, "absmiddle"));

                out.write("</td>");
                nb = Integer.parseInt(item.get("childCount").toString());
                Boolean late = (Boolean) item.get("late");
                String style = "";
                if (late) {
                    style += "color:red; ";
                }
                if (nb > 0) {
                    style += "font-weight:bold;";
                }

                out.write("<td width=100%>");
                out.write(buildActionLink(ACTION_CLICK_ITEM, i, item.getName(), style));
                out.write("</td>");

                out.write("<td>");
                out.write("<span style='" + style + "'>");
                out.write("&nbsp;" + item.get("childCount") + "&nbsp;");
                out.write("</span>");
                out.write("</td></tr>");

                out.write("</table></td></tr>");
            }
        }

        out.write(SHELF_END);
    }

    /**
     * @see javax.faces.component.UIComponentBase#broadcast(javax.faces.event.FacesEvent)
     */
    @Override
    @SuppressWarnings("unchecked")
    public void broadcast(FacesEvent event) throws AbortProcessingException {
        if (event instanceof CorbeilleEvent) {
            // found an event we should handle
            CorbeilleEvent corbeilleEvent = (CorbeilleEvent) event;

            List<Node> items = (List<Node>) getValue();
            if (items != null && items.size() > corbeilleEvent.Index) {
                // process the action
                switch (corbeilleEvent.Action) {
                    case ACTION_CLICK_ITEM:
                        Utils.processActionMethod(getFacesContext(), getClickActionListener(), corbeilleEvent);
                        break;
                }
            }
        } else {
            super.broadcast(event);
        }
    }

    // ------------------------------------------------------------------------------
    // Private helpers
    /**
     * We use a hidden field name on the assumption that only one Shortcut Shelf item
     * instance is present on a single page.
     *
     * @return hidden field name
     */
    private String getHiddenFieldName() {
        return getClientId(getFacesContext());
    }

    /**
     * Build HTML for an link representing a Shortcut action
     *
     * @param action     action indentifier to represent
     * @param index      index of the Node item this action relates too
     * @param text       of the action to display
     *
     * @return HTML for action link
     */
    private String buildActionLink(int action, int index, String text, String style) {
        FacesContext context = getFacesContext();

        StringBuilder buf = new StringBuilder(200);

        buf.append("<a href='#' onclick=\"");
        // generate JavaScript to set a hidden form field and submit
        // a form which request attributes that we can decode
        buf.append(Utils.generateFormSubmit(context, this, getHiddenFieldName(), encodeValues(action, index)));
        buf.append("\" style=\"").append(style).append("\">");

        buf.append(Utils.cropEncode(text));

        buf.append("</a>");

        return buf.toString();
    }

    /**
     * Encode the specified values for output to a hidden field
     *
     * @param action     Action identifer
     * @param index      Index of the Node item the action is for
     *
     * @return encoded values
     */
    private static String encodeValues(int action, int index) {
        return Integer.toString(action) + NamingContainer.SEPARATOR_CHAR + Integer.toString(index);
    }

    /**
     * Class representing the an action relevant to the Shortcut element.
     */
    public static class CorbeilleEvent extends ActionEvent {

        private static final long serialVersionUID = -6342154122424413004L;

        public int Action;

        public int Index;

        public CorbeilleEvent(UIComponent component, int action, int index) {
            super(component);
            Action = action;
            Index = index;
        }

    }

}
