/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.bean;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationException;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.servlet.FacesHelper;
import org.alfresco.web.bean.LoginBean;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.bean.users.ChangePasswordDialog;
import org.alfresco.web.ui.common.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.faces.context.FacesContext;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;

/**
 * Bean de gestion des comptes utilisateurs, surcharge du bean Alfresco.
 * Ajoute la liaison avec les espaces 'parapheur' (cas de suppression),
 * la gestion des certificats, etc.
 * 
 * @author Stephane VAST - ADULLACT Projet
 */
public class UsersBean extends org.alfresco.web.bean.users.UsersDialog {
    private static Log logger = LogFactory.getLog(UsersBean.class);

    private TenantService tenantService;

    private ParapheurService parapheurService;

    /**
     * @return Returns the parapheurService.
     */
    public ParapheurService getParapheurService() {
        return parapheurService;
    }

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    /**
     * @see org.alfresco.web.bean.users.UsersDialog#deleteOK()
     */
    @SuppressWarnings("unchecked")
    @Override
    public String deleteOK() {
        UserTransaction tx = null;
        FacesContext context = FacesContext.getCurrentInstance();
        String userName = (String) properties.getPerson().getProperties().get("userName");
        List<NodeRef> ownedParapheurs = parapheurService.getOwnedParapheurs(userName);
        List<NodeRef> secretariatParapheurs = parapheurService.getSecretariatParapheurs(userName);


        // AuthenticationService authenticationService = Repository.getServiceRegistry(context).getAuthenticationService();
        PersonService personService = Repository.getServiceRegistry(context).getPersonService();

        try {
            ServiceRegistry serviceRegistry = Repository.getServiceRegistry(context);
            TransactionService transactionService = serviceRegistry.getTransactionService();
            tx = transactionService.getUserTransaction();
            tx.begin();
         
            // we only delete the user auth if Alfresco is managing the authentication 
            Map session = context.getExternalContext().getSessionMap();
            if (session.get(LoginBean.LOGIN_EXTERNAL_AUTH) == null) {
                // delete the User authentication
                try {
                    personService.deletePerson(userName);
                } catch (AuthenticationException authErr) {
                    Utils.addErrorMessage(Application.getMessage(FacesContext.getCurrentInstance(), "error_delete_user_object"));
                }
            }

            // Si la personne possédait des parapheurs, on libère ceux-ci
            if (ownedParapheurs != null && ownedParapheurs.isEmpty()) {

                for (NodeRef ownedParapheur : ownedParapheurs) {
                    ArrayList<String> owners = (ArrayList<String>) this.getNodeService().getProperty(ownedParapheur, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);
                    owners.remove(userName);
                    this.getNodeService().setProperty(ownedParapheur, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, owners);
                }
            }

            // Si la personne faisait partie du secrétariat d'un parapheur, on la supprime de celui-ci
            if (secretariatParapheurs != null && !secretariatParapheurs.isEmpty()) {
                for (NodeRef parapheur : secretariatParapheurs) {
                    List<String> secretariat = parapheurService.getSecretariatParapheur(parapheur);
                    secretariat.remove(userName);
                    this.getNodeService().setProperty(parapheur, ParapheurModel.PROP_SECRETAIRES, (Serializable) secretariat);
                }
            }
         
            // delete the associated Person
            personService.deletePerson(userName);

            // commit the transaction
            tx.commit();

            // re-do the search to refresh the list
            search();
        } catch (Throwable e) {
            // rollback the transaction
            try {
                if (tx != null) {
                    tx.rollback();
                }
            } catch (Exception tex) {
            }
            Utils.addErrorMessage(MessageFormat.format(Application.getMessage(
                    FacesContext.getCurrentInstance(), "error_delete_user"), e.getMessage()), e);
        }

        return "manageUsers";
    }

    /*
   public String getOwnedParapheur()
   {
      NodeRef parapheur = parapheurService.getOwnedParapheur((String)properties.getPerson().getProperties().get("userName"));
      if (parapheur != null)
         return parapheurService.getNomParapheur(parapheur);
      else
         return "";
   } */

    public List<String> getOwnedParapheurs() {
        List<NodeRef> parapheurs = parapheurService.getOwnedParapheurs((String)properties.getPerson().getProperties().get("userName"));
        List<String> parapheursNames = new ArrayList<String>();
        if (parapheurs != null && !parapheurs.isEmpty()) {
            for (NodeRef parapheur : parapheurs) {
                parapheursNames.add(parapheurService.getNomParapheur(parapheur));
            }
        }

        return parapheursNames;

    }
   
    public boolean getSecretaire() {
        List<NodeRef> parapheurs = parapheurService.getSecretariatParapheurs((String) properties.getPerson().getProperties().get("userName"));
        if (parapheurs != null) {
            return !parapheurs.isEmpty();
        } else {
            return false;
        }
    }

    /**
     * @see org.alfresco.web.bean.users.UsersDialog#getUsers()
     */
    @Override
    public List<Node> getUsers() {
        tenantService = (TenantService) FacesHelper.getManagedBean(FacesContext.getCurrentInstance(), "tenantService");
        AuthorityService authorityService = Repository.getServiceRegistry(FacesContext.getCurrentInstance()).getAuthorityService();

        List<Node> parapheurUsers = super.getUsers();

        // Ne pas afficher les utilisateurs "guest" et "System"
        int cpt = 0;
        int guestIndex = -1;
        int systemIndex = -1;
        for (int i = 0; (cpt < 2) && (i < parapheurUsers.size()); i++) {
            String username = (String) parapheurUsers.get(i).getProperties().get(ContentModel.PROP_USERNAME);
            String base = tenantService.getBaseNameUser(username);
            if ("guest".equals(base)) {
                guestIndex = i;
                cpt ++;
            } else if ("System".equals(base)) {
                systemIndex = i;
                cpt ++;
            }
        }
        if (guestIndex != -1) {
            parapheurUsers.remove(guestIndex);
            if (systemIndex > guestIndex) {
                systemIndex--;
            }
        }
        if (systemIndex != -1) {
            parapheurUsers.remove(systemIndex);
        }

        for (Node user : parapheurUsers) {
            Map<String,Object> propsMap = user.getProperties();
            String username = (String) propsMap.get(ContentModel.PROP_USERNAME);

            String certifString = (String) propsMap.get(ParapheurModel.PROP_ID_CERTIFICAT);
            propsMap.put("hasCertificate", (certifString!=null && !certifString.isEmpty()));

            /**
             * Identifier s'il s'agit du compte "admin"
             */
            propsMap.put("isAdminAccount", (username.startsWith("admin@") || username.equals("admin")));

            /**
             * Est-il administrateur technique ? (full access)
             */
            Set<String> authorities = authorityService.getAuthoritiesForUser(username);
            propsMap.put("isAdministrator", authorities.contains("GROUP_ALFRESCO_ADMINISTRATORS"));

            /**
             * Est-il admin fonctionnel ?
             * NB: pas le coup d'appeler parapheurService.isAdministrateurFonctionnel, qui refait appel à
             *        authorityService.getAuthoritiesForUser(username)
             */
            boolean isOpAdmin = false;
            for (String zeAuthString : authorities) {
                if (zeAuthString.startsWith("ROLE_PHADMIN_")) {
                    isOpAdmin = true;
                    break;
                }
            }
            propsMap.put("isOpAdmin", isOpAdmin);

            // logs
            if (logger.isDebugEnabled()) {
                System.out.println("=== user "+ username);
                for (Iterator<String> it = authorities.iterator(); it.hasNext();) {
                    String string = it.next();
                    System.out.println("  Authority for " + username + " : " + string);
                }
            }
        }

        return parapheurUsers;
    }

    public String changePasswordOK() {
        ChangePasswordDialog dialog = (ChangePasswordDialog) FacesHelper.getManagedBean(FacesContext.getCurrentInstance(),
                "ChangePasswordDialog");
        return dialog.changePasswordOK("cancel",
                FacesContext.getCurrentInstance());
    }

}
