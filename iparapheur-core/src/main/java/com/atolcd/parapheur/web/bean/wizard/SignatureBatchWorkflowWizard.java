/*
 * Version 3.3
 * CeCILL Copyright (c) 2011-2014, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 * contact@atolcd.com
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.web.bean.wizard;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.repo.job.AbstractJob;
import com.atolcd.parapheur.web.action.evaluator.SignEvaluator;
import com.atolcd.parapheur.web.bean.ClientCertificateBean;
import fr.bl.iparapheur.srci.SrciService;
import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.*;
import javax.faces.context.FacesContext;
import org.adullact.libersign.util.signature.DigestComputer;
import org.adullact.libersign.util.signature.PKCS7VerUtil;
import org.adullact.libersign.util.signature.PesDigest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.servlet.FacesHelper;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.bean.repository.User;
import org.apache.log4j.Logger;
import org.bouncycastle.cert.X509CertificateHolder;
import org.springframework.extensions.surf.util.Base64;

/**
 * 
 * @author Emmanuel Peralta - Adullact Projet
 * @author Stephane Vast - Adullact Projet
 */
public class SignatureBatchWorkflowWizard extends AbstractBatchWorkflowWizard {
    private Logger logger = Logger.getLogger(SignatureBatchWorkflowWizard.class);
    private PersonService personService;
    private ContentService contentService;
    private List<Map<String, String>> appletParams;


    @Override
    public void init(Map<String, String> parameters) {
        super.init(parameters);
        appletParams = null;
    }

    @Override
    protected String getSearchPath() {
        NodeRef parapheurCourant = parapheurBean.getParapheurCourant();

        String parapheurName = (String) nodeService.getProperty(parapheurCourant, ContentModel.PROP_NAME);
        Path path = nodeService.getPath(parapheurCourant);
        Path.Element e = path.last();

        String elementString = e.getElementString();


        QName corbeille = ParapheurModel.NAME_A_TRAITER.getPrefixedQName(namespaceService);
        corbeilleName = ParapheurModel.NAME_A_TRAITER.toString();
        return String.format("/app:company_home/ph:parapheurs/%s/%s/*", e.getPrefixedString(namespaceService), corbeille.getPrefixString());

    }

    @Override
    protected boolean shallSelectDossier(NodeRef dossier) {
        SignEvaluator evaluator = new SignEvaluator();
        EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossier);
        return (!EtapeCircuit.ETAPE_TDT.equalsIgnoreCase(etape.getActionDemandee().trim()) &&
                evaluator.evaluate(new Node(dossier)));
    }

    protected X509Certificate[] finishUnitImpl(SelectableDossier dossier, X509Certificate[] userCert, X509Certificate[] signCert, String fullName, int dossierId, String sig64) {
        NodeRef dossierRef = dossier.getDossier().getNodeRef();
        EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossierRef);
        if (dossier.isRead()) {
            parapheurService.markAsRead(dossierRef);
        }
        if (dossier.isSelected()) {
            if (EtapeCircuit.ETAPE_SIGNATURE.equals(etape.getActionDemandee())) {
                //String sig64 = (String) requestMap.get("wizard:wizard-body:signature_" + dossierId);
                // byte[] signature = (sig64 != null && !sig64.trim().isEmpty()) ? Base64.decode(sig64) : null;
                byte[] signature;
                if (sig64 != null && !sig64.trim().isEmpty()) {
                    /**
                     * en cas de XAdES enveloppé multi IDs, on a plusieurs 
                     * signatures (par exemple si PESv2). 
                     * Alors on ne decode pas tout de suite dans ce cas précis!!!
                     */
                    if (sig64.contains(",")) {
                        signature = sig64.getBytes();
                    } else {
                        signature = Base64.decode(sig64);
                    }
                } else {
                    signature = null;
                }

                X509CertificateHolder[] signatureCerts=null;

                if (signature != null) {
                    long start = System.currentTimeMillis();
                    parapheurService.setSignature(dossierRef, signature, null);

                    String sigFormat = ((String) getNodeService().getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT)).toLowerCase().trim();
                    if (userCert != null) {
                        if (sigFormat.startsWith("pkcs#7")) {
                            byte[] derSign = PKCS7VerUtil.pem2der(signature, "-----BEGIN".getBytes(), "-----END".getBytes());
                            signatureCerts = new X509CertificateHolder[]{
                                    PKCS7VerUtil.getSignatureCertificateHolder(null, derSign)
                            };
//                            signCert = new X509Certificate[]{
//                                    PKCS7VerUtil.getSignatureCertificate(null, derSign)
//                            };
                        }
                    }

                } else {
                    throw new RuntimeException("Impossible d'effectuer la ou les operation(s) de signature. Assurez-vous d'être en mode sécurisé pour signer.");
                }
                parapheurService.setAnnotationPublique(dossierRef, annotation);
                parapheurService.setAnnotationPrivee(dossierRef, annotationPrivee);
                this.parapheurService.setSignataire(dossierRef, fullName, signatureCerts);
                parapheurService.approveV4(dossierRef, parapheurService.getCurrentParapheur());


            } else { // C'est un VISA
                parapheurService.setAnnotationPublique(dossierRef, annotation);
                parapheurService.setAnnotationPrivee(dossierRef, annotationPrivee);
                this.parapheurService.setSignataire(dossierRef, fullName, (java.security.cert.X509Certificate[])null);
                parapheurService.approveV4(dossierRef, parapheurService.getCurrentParapheur());
            }
        }

        return signCert;
    }

    @Override
    public String getStepDescription() {
        return "Veuillez selectionner les dossiers à " + getActionName().toLowerCase() + ".";
    }

    @Override
    protected String finishImpl(FacesContext fc, String string) throws Throwable {

        long startTime = System.currentTimeMillis();

        //User currentUser = Application.getCurrentUser(context);
        String fullName = null;
        String username = Application.getCurrentUser(fc).getUserName();
        // Le signataire enregistré doit être le propriétaire du parapheur (cas de signature papier)
        NodeRef parapheurRef = this.parapheurBean.getParapheurCourant(); //this.parapheurService.getParentParapheur(dossierRef);
        if (this.parapheurService.isParapheurSecretaire(parapheurRef, username)) {
            NodeRef secretaireRef = this.personService.getPerson(username);
            fullName = String.format("%s pour le compte de \"%s\"", User.getFullName(this.getNodeService(), secretaireRef), parapheurService.getNomParapheur(parapheurRef));
        } else if (this.parapheurService.isParapheurOwner(parapheurRef, username)) {
            fullName = Application.getCurrentUser(fc).getFullName(this.getNodeService());
        }

        ClientCertificateBean cert = (ClientCertificateBean) FacesHelper.getManagedBean(fc, "ClientCertificateBean");
        X509Certificate[] userCert = (cert != null ? cert.getX509Certificate() : null);

        // FIXME: La ligne suivante est une escroquerie!
        X509Certificate[] signCert = userCert;

        int dossierId = 1;
        for (SelectableDossier dossier : getDossiers()) {
            if (dossier.isSelected()) {

                final SelectableDossier fDossier = dossier;
                final X509Certificate[] fUserCert = userCert;
                final X509Certificate[] fSignCert = signCert;
                final String fFullname = fullName;
                final int fDossierId = dossierId;

                RetryingTransactionHelper txnHelper = Repository.getRetryingTransactionHelper(fc);
                txnHelper.setMaxRetryWaitMs(0);
                txnHelper.setMinRetryWaitMs(0);
                txnHelper.setRetryWaitIncrementMs(1);
                txnHelper.setMaxRetries(5);

                Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
                final String sig64 = (String) requestMap.get("wizard:wizard-body:signature_" + fDossierId);
                /**
                 * Poser l'incrémentation AVANT le déclenchement de la callback
                 * car la callback fait avancer le dossier (qui peut passer
                 * d'un VISA à une SIGNATURE par exemple) et fausser le test
                 * ci-après !
                 */
                NodeRef dossierRef = dossier.getDossier().getNodeRef();
                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossierRef);

                if (EtapeCircuit.ETAPE_SIGNATURE.equals(etape.getActionDemandee())) {
                    dossierId++;
                }
                // AHA!
                jobService.lockNode(fDossier.getDossier().getNodeRef());

                jobService.postJob(new AbstractJob() {
                    @Override
                    public Void doWork() {
                        RetryingTransactionHelper helper = transactionService.getRetryingTransactionHelper();

                        helper.setMaxRetries(5);
                        helper.setMinRetryWaitMs(1000);
                        helper.setMaxRetryWaitMs(10000);
                        helper.setRetryWaitIncrementMs(1000);

                        try {
                            RetryingTransactionHelper.RetryingTransactionCallback<Void> callback = new RetryingTransactionHelper.RetryingTransactionCallback<Void>() {
                                @Override
                                public Void execute() throws Throwable {
                                    NodeRef dossierRef = fDossier.getDossier().getNodeRef();
                                    finishUnitImpl(fDossier, fUserCert, fSignCert, fFullname, fDossierId, sig64);

                                    return null;
                                }
                            };
                            helper.doInTransaction(callback, false, true);
                        } catch (Exception e) {
                            putExceptionForDossier(e, fDossier.getDossier().getNodeRef());
                            logger.error("la signature à échouée");
                            jobService.unlockNodeInTransaction(fDossier.getDossier().getNodeRef());
                        }

                        return null;
                    }
                });

            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Sign Time: " + (System.currentTimeMillis() - startTime));
        }

        if (jobService.isBackgroundWorkEnabled()) {
            corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurBean.getParapheurCourant(), ParapheurModel.NAME_A_TRAITER));
        }

        this.forceShelfUpdate();
        this.browseBean.updateUILocation(parapheurBean.getParapheurCourant());

        return "success";

    }

    /**
     * Returns a list of parameters used by the signature applet.
     * <br />
     * <br />
     * <p/>
     * These parameters include (for each document we need to sign):
     * <ul>
     * <li>An unique ID, incremental</li>
     * <li>The signature format to use: cms, xades or pesv2</li>
     * <li>The document hash</li>
     * <li>All PESv2 relative properties</li>
     * </ul>
     *
     * @return applet parameters list.
     */
    @Override
    public List<Map<String, String>> getAppletParams() {
        if (appletParams == null) {
            appletParams = new ArrayList<Map<String, String>>();
            int docId = 1;
            for (SelectableDossier dossier : getDossiers()) {
                NodeRef dossierRef = dossier.getDossier().getNodeRef();
                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossierRef);
                if (dossier.isSelected()
                        && EtapeCircuit.ETAPE_SIGNATURE.equals(etape.getActionDemandee())) {
                    Map<String, String> params = new HashMap<String, String>();

                    // Unique document id, incremental
                    params.put("id", "" + docId);

                    // Signature format: cms by default
                    String format = "cms";
                    String sigFormat = (String) getNodeService().getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT);

                    if ("PKCS#7/single".equals(sigFormat)) {
                        format = "CMS";
                    } else if ("PKCS#7/multiple".equals(sigFormat)) {
                        format = "CMS-Allin1";
                    } else if ("XAdES/enveloped".equals(sigFormat)) {
                        format = "XADES-env";
                    } else if ("XAdES/DIA".equals(sigFormat)) {
                        format = "XADES-env-xpath";
                    } else if ("XAdES/detached".equals(sigFormat)) {
                        format = "XADES";
                    } else if ("XAdES132/detached".equals(sigFormat)) {
                        format = "XADES132";
                    } else if ("XAdES-T/enveloped".equals(sigFormat)) {
                        format = "XADES-T-env";
                    } else {
                        throw new UnsupportedOperationException("Unknown signature format: " + sigFormat);
                    }

                    params.put("format", format);

                    // Si ce n'est pas du CMS ou dérivé, alors c'est du XAdES-like
                    if (!format.startsWith("CMS")) {
                        // PES properties, retrieved from S2low configuration files
                        Properties props = parapheurService.getXadesSignatureProperties(dossierRef);
                        if (props != null) {
                            for (Map.Entry<Object, Object> e : props.entrySet()) {
                                params.put(e.getKey().toString(), e.getValue().toString());
                            }
                        }
                    }

                    String tdtName = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_NOM);

                    if (S2lowService.PROP_TDT_NOM_S2LOW.equals(tdtName) || SrciService.K.tdtName.equals(tdtName)) { 
                        // Le ROLE du signataire dans une signature XAdES est le TITRE de son Parapheur
                        params.put("pClaimedRole", this.parapheurService.getNomParapheur(this.parapheurService.getParentParapheur(dossierRef)));
                    }

                    // Computed hash for file
                    byte[] hash = null;
                    if ("pesv2".equalsIgnoreCase(format)
                            || format.toLowerCase().startsWith("xades")) {
                        NodeRef doc = parapheurService.getDocuments(dossierRef).get(0);
                        ContentReader reader = contentService.getReader(doc, ContentModel.PROP_CONTENT);
                        java.io.InputStream is = reader.getContentInputStream();
                        if (is == null) {
                            return null;
                        }

                        if (format.equalsIgnoreCase("XADES-env-xpath")) {  // Cas particulier signature DIA
                            /**
                             * Pour ce cas TRES particulier, le flux va descendre en
                             * entier. On canonicalise, avant d'envoyer
                             */
                            try {
                                java.io.ByteArrayOutputStream baStream = new java.io.ByteArrayOutputStream();
                                nu.xom.Builder builder = new nu.xom.Builder();
                                nu.xom.canonical.Canonicalizer canonicalizer = new nu.xom.canonical.Canonicalizer(baStream, nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);
                                nu.xom.Document document = builder.build(is);
                                nu.xom.Element rootElement = document.getRootElement();
                                if (null == rootElement) {
                                    params.put("pesid", "");
                                } else {
                                    params.put("pesid", rootElement.getAttributeValue("Id"));
                                    // this.setPesV2SignParameters(document.getRootElement().getAttributeValue("Id"));
                                }
                                canonicalizer.write(document);
                                hash = baStream.toByteArray();
                            } catch (nu.xom.ParsingException ex) {
                                logger.error(ex);
                                return null;
                            } catch (java.io.IOException ex) {
                                logger.error(ex);
                                return null;
                            }

                            NodeRef parapheurRef = parapheurService.getParentParapheur(dossierRef);
                            // On chope la DESCRIPTION construite ainsi: "pCodePostal pCity"
                            String descriptionString = (String) getNodeService().getProperty(parapheurRef, ContentModel.PROP_DESCRIPTION);
                            if (descriptionString != null) {
                                descriptionString = descriptionString.trim();
                                if (descriptionString.length() > 5) {
                                    params.put("pPostalCode", descriptionString.substring(0, 5));
                                    params.put("pCity", descriptionString.substring(5).trim());
                                }
                            }

                            params.put("hash", "");
                            if (hash != null) {
                                StringBuilder builder = new StringBuilder();
                                for (byte dataByte : hash) {
                                    builder.append(Integer.toHexString((dataByte & 0xF0) >> 4));
                                    builder.append(Integer.toHexString(dataByte & 0x0F));
                                }
                                params.put("hash", builder.toString());
                            }

                        } else if (format.equalsIgnoreCase("XADES132")) {  // utilisé pour ANTS-ECD
                            /*
                             * en attendant mieux, ici on impose le format SHA-256.
                             * C'est péremptoire, mais j'assume ce couplage fort.
                             * 
                             * Amusant: on la joue comme pour PKCS7: on presume qu'il ne s'agit pas de XML...
                             */
                            // TODO : un jour, détecter le format source (XML vs non-XML) pour la canonicalisation éventuelle
                            // FIXME : pas besoin de signature policy ???
                            try {
                                MessageDigest digest = MessageDigest.getInstance("SHA256");
                                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                                reader.getContent(buffer);
                                digest.update(buffer.toByteArray());
                                hash = digest.digest(); //getBytesDigest(getBytesToSign(), "SHA-256");
                            } catch (NoSuchAlgorithmException e) {
                                throw new RuntimeException("SHA256 unsupported", e);
                            }

                            params.put("hash", "");
                            if (hash != null) {
                                StringBuilder builder = new StringBuilder();
                                for (byte dataByte : hash) {
                                    builder.append(Integer.toHexString((dataByte & 0xF0) >> 4));
                                    builder.append(Integer.toHexString(dataByte & 0x0F));
                                }
                                params.put("hash", builder.toString());
                            }

                        }
                        else {  // Cas general de XAdES (non DIA)

//                            PesDigest pesDigest = DigestComputer.computeDigest(is,
//                                    parapheurService.getXPathSignature(dossierRef));
//                            if (pesDigest == null) {
//                                logger.error("Unable to compute Hash.");
//                                throw new RuntimeException("Unable to compute Hash");
//                            }
                            //                    PesDigest pesDigest = DigestComputer.computeDigest(is,
                            //                        parapheurService.getXPathSignature(dossierRef));
                            //                    if (pesDigest == null) {
                            //                        this.setPesV2SignParameters(null);
                            //                        return null;
                            //                    }
                            //                    this.setPesV2SignParameters(pesDigest.getId());
                            //                    myhash = pesDigest.getDigest();
                            /*
                             * Changement d'algo pour support Xpath multi-resultats
                             */
                            String xPathSignature = parapheurService.getXPathSignature(dossierRef);
                            System.out.println("xPathSignature = " + xPathSignature);
                            List<PesDigest> pesDigest = DigestComputer.computeDigests(is, xPathSignature);
                            if (pesDigest == null) {
                                logger.error("Unable to compute Hash.");
                                throw new RuntimeException("Unable to compute Hash");
                            } else {
                                int nbre = pesDigest.size();
                                boolean first = true;

                                // serialisation des ID de PES
                                StringBuilder idBuilder = new StringBuilder();
                                for (int i = 0; i < nbre; i++) {
                                    if (first) {
                                        first = false;
                                    } else {
                                        idBuilder.append(",");
                                    }
                                    idBuilder.append(pesDigest.get(i).getId());
                                }
                                //this.setPesV2SignParameters(idBuilder.toString());

                                // serialisation des Digests
                                StringBuilder hashesBuilder = new StringBuilder();
                                first = true;
                                for (int i = 0; i < nbre; i++) {
                                    if (first) {
                                        first = false;
                                    } else {
                                        hashesBuilder.append(",");
                                    }

                                    hash = pesDigest.get(i).getDigest();

                                    StringBuilder builder=new StringBuilder();
                                    if (hash == null) {
                                        hashesBuilder.append(" ");
                                    } else {
                                        for (byte dataByte: hash) {
                                            builder.append(Integer.toHexString((dataByte & 0xF0)>>4));
                                            builder.append(Integer.toHexString(dataByte & 0x0F));
                                        }
                                        hashesBuilder.append( builder.toString());
                                    }
                                }
                                //return hashesBuilder.toString();
                                params.put("pesid", idBuilder.toString());
                                params.put("hash", hashesBuilder.toString());

                            }
                            //hash = pesDigest.getDigest();
                            params.put("pEncoding", reader.getEncoding());
                        }
                    } else {
                        NodeRef doc = parapheurService.getDocuments(dossierRef).get(0);
                        ContentReader reader = contentService.getReader(doc, ContentModel.PROP_CONTENT);
                        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                        reader.getContent(buffer);

                        try {
                            MessageDigest digest = MessageDigest.getInstance("SHA1");
                            digest.update(buffer.toByteArray());
                            hash = digest.digest();
                        } catch (NoSuchAlgorithmException e) {
                            throw new RuntimeException("SHA1 unsupported", e);
                        }
                        if ("CMS-Allin1".equalsIgnoreCase(format)) {
                            // Il faut donner la signature courante!
                            byte[] p7Sig = parapheurService.getSignature(dossierRef);
                            if (p7Sig != null) {
                                params.put("p7sSignatureString", new String(Base64.encodeBytes(p7Sig)));
                                if (logger.isDebugEnabled()) {
                                    logger.debug("CMS-Allin1 pre-Signature not null, is:\n" + params.get("p7sSignatureString"));
                                }
                            } else {
                                params.put("p7sSignatureString", "null");
                            }
                        }

                        params.put("hash", "");
                        if (hash != null) {
                            StringBuilder builder = new StringBuilder();
                            for (byte dataByte : hash) {
                                builder.append(Integer.toHexString((dataByte & 0xF0) >> 4));
                                builder.append(Integer.toHexString(dataByte & 0x0F));
                            }
                            params.put("hash", builder.toString());
                        }
                    }

                    appletParams.add(params);
                    docId++;
                }
            }
        }
        return appletParams;
    }

    @Override
    public String getStepTitle() {
        switch (this.currentStep) {
            case 0:
                return "Dossiers à " + getActionName().toLowerCase();
            default:
                return super.getStepTitle();

        }
    }
    
    @Override
    public String getFinishButtonLabel() {
        return getActionName();
    }
    
    protected String getActionName() {
        return "Viser/Signer";
    }

    @Override
    public String getAppletUrl() {
        return parapheurService.getSignAppletURL();
    }

    @Override
    public int getAppletParamsCount() {
        return appletParams.size();
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }
}
