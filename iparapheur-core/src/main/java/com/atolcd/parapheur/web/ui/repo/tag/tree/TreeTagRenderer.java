/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atolcd.parapheur.web.ui.repo.tag.tree;

import com.atolcd.parapheur.web.ui.repo.component.tree.TreeModel;
import com.atolcd.parapheur.web.ui.repo.component.tree.UITree;
import java.io.IOException;
import java.util.Map;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.Renderer;
import org.apache.myfaces.shared_impl.renderkit.RendererUtils;

/**
 *
 * @author viv
 */
public class TreeTagRenderer extends Renderer {

    @Override
    public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
        ResponseWriter writer = context.getResponseWriter();

        UITree tree = (UITree) component;
        TreeModel model = (TreeModel) tree.getValue();
        writer.startElement("ul", component);
        encodeNode(context, component, writer, model, model.getRoot(), tree.getVar());
        writer.endElement("ul");
    }

    private void encodeNode(FacesContext context,
            UIComponent component,
            ResponseWriter writer,
            TreeModel model,
            Object node,
            String var) throws IOException {
        writer.startElement("li", component);

        Map<String, Object> requestMap = context.getExternalContext().getRequestMap();
        if (var != null) {
            requestMap.put(var, node);
        }

        RendererUtils.renderChildren(context, component);

        if (var != null) {
            requestMap.remove(var);
        }

        writer.endElement("li");

        if (!model.isLeaf(node)) {
            writer.startElement("ul", component);
            for (int i = 0; i < model.getChildrenCount(node); i++) {
                encodeNode(context, component, writer, model, model.getChild(node, i), var);
            }
            writer.endElement("ul");
        }
    }

}
