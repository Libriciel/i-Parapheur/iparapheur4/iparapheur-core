/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2012, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet
 * 
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.repo.impl.FastServiceImpl;
import com.atolcd.parapheur.web.bean.ClientCertificateBean;
import com.atolcd.parapheur.web.bean.ParapheurBean;
import fr.bl.iparapheur.srci.SrciService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.adullact.libersign.util.signature.DigestComputer;
import org.adullact.libersign.util.signature.PKCS7VerUtil;
import org.adullact.libersign.util.signature.PesDigest;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.servlet.FacesHelper;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.MapNode;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.bean.repository.User;
import org.alfresco.web.ui.common.SortableSelectItem;
import org.alfresco.web.ui.common.component.data.UIRichList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.quartz.Scheduler;
import org.springframework.extensions.surf.util.Base64;
import org.springframework.util.Assert;

/**
 * 
 * @author Stephane Vast - Adullact Projet
 */
public class WorkflowDialog extends BaseDialogBean
{
    public static final long TWO_MINS = 2 * 60 * 1000;

    public static final long FIVE_MINS = 5 * 60 * 1000;
    
    private static Log logger = LogFactory.getLog(WorkflowDialog.class);

    private NodeRef dossierRef = null;
    private String annotation = null;
    private String annotationPrivee = null;
    private String signature = null;

    protected ContentService contentService;
    /** The ParapheurService bean reference */
    protected ParapheurService parapheurService;
    /** Ajout du CorbeillesService pour signer papier */
    protected CorbeillesService corbeillesService;

    private S2lowService s2lowService;

    // BLEX
    private SrciService srciService;

    private Scheduler scheduler;

    private AuthenticationComponent authenticationComponent;

    /** The Parapheur bean reference */
    protected ParapheurBean parapheurBean;
    protected PersonService personService;

    /* Informations ACTES */
    private String actesNomDossier;
    private String actesNature;
    private String actesClassification;
    private String actesNumero;
    private Date actesDateActe;
    private String actesObjet;

    private List<SortableSelectItem> actesNatures;

    private List<SortableSelectItem> actesClassifications;

    private Boolean inTdtMode;

    private Boolean inActesMode;
    
    private Boolean annotationMandatory;

    private boolean hasConsecutiveSteps;
    private boolean validateAllSteps;
    private List<Node> consecutiveSteps;
    private UIRichList consecutiveStepsRichList;
    protected DossierService dossierService;
    
    
    /**
     * @param contentService The ContentService to set.
     */
    public void setContentService(ContentService contentService) {
	this.contentService = contentService;
    }

    /**
     * @param parapheurService The ParapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
	this.parapheurService = parapheurService;
    }

    /**
     * @param corbeillesService The CorbeillesService to set.
     */
    public void setCorbeillesService(CorbeillesService corbeillesService) {
	this.corbeillesService = corbeillesService;
    }
    
    public void setDossierService(DossierService dossierService) {
	this.dossierService = dossierService;
    }

    public void setAuthenticationComponent(AuthenticationComponent authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }

    /**
     * @param s2lowService The S2lowService to set.
     */
    public void setS2lowService(S2lowService s2lowService) {
        this.s2lowService = s2lowService;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    /**
     * @param personService the personService to set
     */
    public void setPersonService(PersonService personService) {
	this.personService = personService;
    }

    /**
     * @param parapheurBean The ParapheurBean to set.
     */
    public void setParapheurBean(ParapheurBean parapheurBean) {
	this.parapheurBean = parapheurBean;
    }

    /**
     * BLEX code.
     * @param srciService The SrciService to set.
     */
    public void setSrciService(SrciService srciService) {
        this.srciService = srciService;
    }

    public String getAnnotation() {
	return this.annotation;
    }

    public void setAnnotation(String annotation) {
	this.annotation = annotation;
    }

    public String getAnnotationPrivee() {
	return this.annotationPrivee;
    }

    public void setAnnotationPrivee(String annotationPrivee) {
	this.annotationPrivee = annotationPrivee;
    }

    public String getActesClassification() {
        return actesClassification;
    }

    public void setActesClassification(String actesClassification) {
        this.actesClassification = actesClassification;
    }

    public Date getActesDateActe() {
        return actesDateActe;
    }

    public void setActesDateActe(Date actesDateActe) {
        this.actesDateActe = actesDateActe;
    }

    public String getActesNature() {
        return actesNature;
    }

    public void setActesNature(String actesNature) {
        this.actesNature = actesNature;
    }

    public String getActesNomDossier() {
        return actesNomDossier;
    }

    public void setActesNomDossier(String actesNomDossier) {
        this.actesNomDossier = actesNomDossier;
    }

    public String getActesNumero() {
        return actesNumero;
    }

    public void setActesNumero(String actesNumero) {
        this.actesNumero = actesNumero;
    }

    public String getActesObjet() {
        return actesObjet;
    }

    public void setActesObjet(String actesObjet) {
        this.actesObjet = actesObjet;
    }

    // BLEX : voir workflow-dialog.jsp
    String extraInformations="";
    /**
     * Des informations a presenter a l'utilisateur
     * @return un texte (le nom du dossier, par exemple)
     */
    public String getExtraInformations() {
        return extraInformations;
    }
    
    public Boolean getAnnotationMandatory() {
        return this.annotationMandatory;
    }

    public boolean getHasConsecutiveSteps() {
        return hasConsecutiveSteps;
    }

    public boolean getValidateAllSteps() {
        return validateAllSteps;
    }

    public void setValidateAllSteps(boolean validateAllSteps) {
        this.validateAllSteps = validateAllSteps;
    }
    
    public List<Node> getConsecutiveSteps() {
        return consecutiveSteps;
    }
    
    public UIRichList getConsecutiveStepsRichList() {
        if (this.consecutiveStepsRichList != null) {
            this.consecutiveStepsRichList.setValue(null);
        }
        return this.consecutiveStepsRichList;
        
    }
    
    public void setConsecutiveStepsRichList(UIRichList consecutiveStepsRichList) {
        this.consecutiveStepsRichList = consecutiveStepsRichList;
        if (this.consecutiveStepsRichList != null) {
            this.consecutiveStepsRichList.setValue(null);
        }
    }

    @Override
    public void init(Map<String, String> parameters) {
	super.init(parameters);

        inTdtMode = null;
        inActesMode = null;
        annotationMandatory = false;
        if (this.parameters.containsKey("action")) {
            annotationMandatory = this.parameters.get("action").equals("parapheur_reject");
            if (this.parameters.containsKey("id")) {
                dossierRef = new NodeRef(Repository.getStoreRef(), this.parameters.get("id"));
            } else {
                dossierRef = this.navigator.getCurrentNode().getNodeRef();
            }
            if (!this.parapheurService.isDossier(dossierRef)) {
                throw new AlfrescoRuntimeException("WorkflowDialog called on a non-dossier element");
            }
            // BLEX
            this.extraInformations = (String) new Node(dossierRef).getProperties().get("cm:title");
            if (isInTdtMode()) {
                String typeMetier = (String) getNodeService().getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER);
                Map<QName, Serializable> typeProps = parapheurService.getTypeMetierProperties(typeMetier);
                this.extraInformations += " [" + typeProps.get(ParapheurModel.PROP_TDT_NOM) + "] ";
            }

	    this.annotation = this.parapheurService.getAnnotationPublique(dossierRef);
	    this.annotationPrivee = this.parapheurService.getAnnotationPrivee(dossierRef);
	}

        /* infos pour ecran S2low Actes */
        this.actesNatures = new ArrayList<SortableSelectItem>();
        Map<Integer, String> naturesS2Low = this.s2lowService.getS2lowActesNatures();
        if (naturesS2Low != null) {
            for (Entry<Integer, String> nature : naturesS2Low.entrySet()) {
                this.actesNatures.add(new SortableSelectItem(nature.getKey().toString(), nature.getValue(), nature.getValue()));
            }
            Collections.sort(this.actesNatures);
            this.actesNature = this.actesNatures.get(0).getValue().toString();
        } else {
            throw new AlfrescoRuntimeException("Impossible de récupérer la liste de natures");
        }

        this.actesClassifications = new ArrayList<SortableSelectItem>();
        Map<String, String> classificationsS2Low = this.s2lowService.getS2lowActesClassifications();
        if (classificationsS2Low != null) {
            for (Entry<String, String> classification : classificationsS2Low.entrySet()) {
                this.actesClassifications.add(new SortableSelectItem(classification.getKey(), classification.getKey() + " " + classification.getValue(), classification.getKey()));
            }
            Collections.sort(this.actesClassifications);
            this.actesClassification = this.actesClassifications.get(0).getValue().toString();
        } else {
            throw new AlfrescoRuntimeException("Impossible de récupérer la liste de natures");
        }

        if (isInActesMode()) {
            this.actesDateActe = new Date();
        }
        this.validateAllSteps = false;
        this.consecutiveSteps = new ArrayList<Node>();
        if(!this.parameters.get("action").equals("parapheur_reject")) {
            List<EtapeCircuit> steps = dossierService.getConsecutiveSteps(dossierRef, AuthenticationUtil.getRunAsUser());
            for (EtapeCircuit step : steps) {
                Node node = new MapNode(step.getNodeRef());
                node.getProperties().put("icon", "/images/parapheur/iw-" + step.getActionDemandee().trim().toLowerCase() + ".jpg");
                node.getProperties().put("parapheurName", step.getParapheurName());
                this.consecutiveSteps.add(node);
            }
        }
        this.hasConsecutiveSteps = (this.consecutiveSteps.size() > 1);
    }

    @Override
    public String finishImpl(FacesContext context, String outcome) throws Exception
    {
	if (this.parameters.containsKey("action"))
	{
	    NodeRef corbeilleRef = this.parapheurService.getParentCorbeille(dossierRef);

	    if (this.parameters.get("action").equals("parapheur_approve")
		    || this.parameters.get("action").equals("parapheur_sign"))
	    {
		ClientCertificateBean cert = (ClientCertificateBean) FacesHelper.getManagedBean(context, "ClientCertificateBean");
		logger.debug("Le client "
			+ ((cert != null && cert.getX509Certificate() != null) ? "utilise un" : "n'utilise pas de")
			+ " certificat client.");

		X509Certificate[] certs = (cert != null) ? cert.getX509Certificate() : null;
		X509Certificate certificate = (certs != null && certs.length > 0) ? certs[0] : null;
                X509CertificateHolder[] signatureCerts=null ;//= certs;
                byte[] laSignature = null;
                if ((certificate != null) && this.isInSignatureMode()) {
                    /**
                     * en cas de XAdES enveloppé multi IDs, on a plusieurs 
                     * signatures (par exemple si PESv2). 
                     * Alors on ne decode pas tout de suite dans ce cas précis!!!
                     */
                    if (getSignature().contains(",")) {
                        laSignature = getSignature().getBytes();
                    } else {
                        laSignature = Base64.decode(getSignature());
                    }
                        String sigFormat = ((String) getNodeService().getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT)).toLowerCase().trim();
                        System.out.print("##(" + sigFormat + ")## ");
                        if (sigFormat.startsWith("pkcs#7")) {
                            byte[] derSign = PKCS7VerUtil.pem2der(laSignature, "-----BEGIN".getBytes(), "-----END".getBytes());
                            signatureCerts = new X509CertificateHolder[] {
                                PKCS7VerUtil.getSignatureCertificateHolder(null, derSign)
                            };
                        }
                }
                User currentUser = Application.getCurrentUser(context);
                String currentUserFullName = currentUser.getFullName(this.getNodeService());
                int nbSteps = (this.validateAllSteps)? this.consecutiveSteps.size() : 1;
                for (int i = 0; i < nbSteps; i++) {
                    if (i == (nbSteps - 1)) { // We put the annotation only on the last step
                        this.parapheurService.setAnnotationPublique(dossierRef, this.annotation);
                        this.parapheurService.setAnnotationPrivee(dossierRef, this.annotationPrivee);
                    }
                    
                    // Le signataire enregistré doit être le propriétaire du parapheur (cas de signature papier)
                    NodeRef parapheurRef = this.parapheurService.getParentParapheur(dossierRef);
                    
                    String fullName = null;
                    if (this.parapheurService.isParapheurSecretaire(parapheurRef, currentUser.getUserName())) {
                        //NodeRef ownerRef = this.personService.getPerson();
                        fullName = String.format("%s pour le compte de \"%s\"", currentUserFullName, parapheurService.getNomParapheur(parapheurRef));
                    } else if (this.parapheurService.isParapheurOwner(parapheurRef, currentUser.getUserName())) {
                        fullName = currentUserFullName;
                    } else if (this.parapheurService.isParapheurDelegate(parapheurRef, currentUser.getUserName())) {
                        fullName = String.format("%s par délégation de \"%s\"", currentUserFullName, parapheurService.getNomParapheur(parapheurRef));
                    }
                    
                    this.parapheurService.setSignataire(dossierRef, fullName, signatureCerts);
                    if (this.isInSignatureMode()) { // do the current step has to be signed?
                        if (laSignature != null ) {
                            parapheurService.setSignature(dossierRef, laSignature, null);
                        } else {
                            throw new RuntimeException("L'operation de signature n'a pas ete effectuee");
                        }
                    }
                    

                    if (isInTdtMode()) {
                        // Teletransmission (le cas echeant)
                        String typeMetier = (String) getNodeService().getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER);
                        Map<QName, Serializable> typeProps = parapheurService.getTypeMetierProperties(typeMetier);
                        String tdtName = (String)typeProps.get(ParapheurModel.PROP_TDT_NOM);
                        if (tdtName.equals(S2lowService.PROP_TDT_NOM_S2LOW) || tdtName.equals(FastServiceImpl.PROP_TDT_NOM_FAST)) {
                            String protocol = (String) typeProps.get(ParapheurModel.PROP_TDT_PROTOCOLE);

                            if ("HELIOS".equals(protocol)) {
                                logger.debug("Envoi protocole HELIOS");
                                try {
                                    s2lowService.envoiS2lowHelios(dossierRef);
                                } catch (IOException ex) {
                                    throw new RuntimeException("Erreur de télétransmission", ex);
                                }
                            }
                            if ("ACTES".equals(protocol)) {
                                Assert.notNull(actesDateActe, "La date de la décision est obligatoire");
                                logger.debug("Envoi protocole ACTES");

                                Assert.isTrue(actesNumero.matches("[A-Z0-9_]{1,15}"), "Le numero de l'acte contient des caractères interdits");
                                String actesDate = new SimpleDateFormat("yyyy-MM-dd").format(actesDateActe);
                                try {
                                    this.getNodeService().setProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_DATE, actesDate);
                                    this.getNodeService().setProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_CLASSIFICATION, actesClassification);
                                    this.getNodeService().setProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_NATURE, actesNature);
                                    this.getNodeService().setProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_OBJET, actesObjet);

                                    s2lowService.envoiS2lowActes(dossierRef, actesNature, actesClassification, actesNumero, actesObjet, actesDate);
                                } catch (IOException ex) {
                                    throw new RuntimeException("Erreur de télétransmission", ex);
                                }
                                catch (Exception e) {
                                    logger.error("ooch, une exception", e);
                                    // e.printStackTrace();
                                    throw e;
                                }
                            }
                        }
                        // BLEX
                        else if (typeProps.get(ParapheurModel.PROP_TDT_NOM).equals(SrciService.K.tdtName)) {
                            String protocole = (String) typeProps.get(ParapheurModel.PROP_TDT_PROTOCOLE);
                            if (SrciService.K.protocol.helios.equals(protocole)) {
                                srciService.sendHelios(dossierRef);
                                logger.debug("dossier envoye a SRCI : "+dossierRef);
                            } else {
                                throw new Exception("Le protocole ["+protocole+"] n'est pas supporte par le TDT ["+SrciService.K.tdtName+"]");
                            }
                        }
                    } else {
                        this.parapheurService.approveV4(dossierRef, this.parapheurService.getCurrentParapheur());
                    }
                }
	    }
            else {
                this.parapheurService.setAnnotationPublique(dossierRef, this.annotation);
                this.parapheurService.setAnnotationPrivee(dossierRef, this.annotationPrivee);
                
                if (this.parameters.get("action").equals("parapheur_reject")) {
                    if ((this.annotation == null) || this.annotation.trim().isEmpty()) {
                        throw new RuntimeException("Un rejet doit obligatoirement être motivé par une annotation publique");
                    }
                    ClientCertificateBean cert = (ClientCertificateBean) FacesHelper.getManagedBean(context,
                            "ClientCertificateBean");
                    this.parapheurService.setSignataire(dossierRef, Application.getCurrentUser(context).getFullName(
                            this.getNodeService()), (cert != null) ? cert.getX509Certificate() : null);
                    this.parapheurService.reject(dossierRef);
                }
                else if (this.parameters.get("action").equals("parapheur_secretariat")) {
                    this.parapheurService.secretariat(dossierRef);
                }
            }
            
            if (!this.parameters.get("action").equals("parapheur_annoter")) {
                this.browseBean.updateUILocation(corbeilleRef);
            }
            else {
                this.browseBean.updateUILocation(dossierRef);
            }

	}
	return null;
    }

    @Override
    public boolean getFinishButtonDisabled()
    {
	return false;
    }

    @Override
    public String getFinishButtonLabel()
    {
        
        logger.debug("dossierRef="+dossierRef);

	if (this.parameters.get("action").equals("parapheur_approve")) {
        logger.debug("navigator.getCurrentNode()="+navigator.getCurrentNode().getName());
        // BLEX : ce truc la ne marche pas quand on demande, par exemple, a teletransmettre
        // en cliquant sur le petit bouton "Envoyer Tdt" indique sous un dossier
        // depuis la corbeille "Dossier a traiter".
        // En effet, le "currentNode" est ... la corbeille
	    // return parapheurBean.getStep(navigator.getCurrentNode());

        // Si le dossierRef est valorise (et ce doit etre le cas - j'imagine),
        // c'est lui, le dossier a traiter

            if (dossierRef != null && parapheurService.isDossier(dossierRef)) {
                return parapheurBean.getStep(new Node(dossierRef));
            } else {
                // en desespoir de cause ...
                logger.error("pas de dossierRef setted");
                return parapheurBean.getStep(navigator.getCurrentNode());
            }
        } else if (this.parameters.get("action").equals("parapheur_reject")) {
            return Application.getMessage(FacesContext.getCurrentInstance(), "workflow_rejeter");
        } else if (this.parameters.get("action").equals("parapheur_sign")) {
            return Application.getMessage(FacesContext.getCurrentInstance(), "workflow_signature_papier");
        } else {
            return Application.getMessage(FacesContext.getCurrentInstance(), "ok");
        }
    }

    private byte[] getBytesToSign()
    {
	ContentReader reader = contentService.getReader(parapheurService.getDocuments(dossierRef).get(0),
		ContentModel.PROP_CONTENT);
	//  on se limite à Integer.MAX_VALUE, quid si le fichier est plus gros ?
	// Solution: on envoie à signer un HASH SHA1 !! (v2.1.5+)
	byte[] bytesToSign = new byte[(int) reader.getSize()];
	try
	{
	    reader.getContentInputStream().read(bytesToSign);
	} catch (IOException e)
	{
	    bytesToSign = null;
	}
	return bytesToSign;
    }
    
    /**
     * Differents formats tels que connus de i-parapheur:
     * PKCS#7/single
     * PKCS#7/multiple
     * XAdES/enveloped
     * XAdES/DIA
     * XAdES/detached
     * XAdES132/detached
     * XAdES-T/enveloped
     */
    public String getSignatureFormat()
    {
	String format = "cms";
        String sigFormat = (String) getNodeService().getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT);

        if ("PKCS#7/single".equals(sigFormat)) {
            format = "CMS";
        } else if ("PKCS#7/multiple".equals(sigFormat)) {
            format = "CMS-Allin1";
        } else if ("XAdES/enveloped".equals(sigFormat)) {
            format = "XADES-env";
        } else if ("XAdES/DIA".equals(sigFormat)) {
            format = "XADES-env-xpath";
        } else if ("XAdES/detached".equals(sigFormat)) {
            format = "XADES";
        } else if ("XAdES132/detached".equals(sigFormat)) {
            format = "XADES132";
        } else if ("XAdES-T/enveloped".equals(sigFormat)) {
            format = "XADES-T-env";
        } else {
            throw new UnsupportedOperationException("Unknown signature format: " + sigFormat);
        }

        return format;
    }

    /**
     * Calcule le haché d'un fichier et le restitue sous forme de tableau d'octets.
     * 
     * @param fileToDigest
     *                le fichier à hacher
     * @param digestAlgorithm
     *                l'algorithme de hachage que l'on souhaite utiliser : "SHA1", "MD5", etc...
     * @param bufSize
     *                Taille du buffer de lecture
     * @return le hash du fichier
     * @throws java.io.IOException
     *                 IOException
     * @throws net.netheos.libraries.utils.crypto.CryptoException
     *                 CryptoException
     */
    public static byte[] getFileDigest(final File fileToDigest, final String digestAlgorithm, final int bufSize) throws IOException {
	Security.addProvider(new BouncyCastleProvider());	// CryptoUtil.addBouncyCastle();
	FileInputStream givenStream = new FileInputStream(fileToDigest);
	MessageDigest digest;
	byte[] hash = null;
	try {
	    digest = MessageDigest.getInstance(digestAlgorithm, "BC");
	    // @SuppressWarnings("unused")
	    int c = 0;
	    final byte[] buffer = new byte[bufSize];
	    while ((c = givenStream.read(buffer)) != -1) {
		digest.update(buffer);
	    }
	    givenStream.close();
	    hash = digest.digest();
	} catch (NoSuchAlgorithmException e) {
	    logger.error("getFileDigest::NoSuchAlgorithmException ", e);
	} catch (NoSuchProviderException e) {
	    logger.error("getFileDigest::NoSuchProviderException ", e);
	}
	return hash;
    }

    public static byte[] getBytesDigest(final byte[] bytesToDigest, final String digestAlgorithm) {
	Security.addProvider(new BouncyCastleProvider());	// CryptoUtil.addBouncyCastle();

	MessageDigest digest;
	byte[] hash = null;
	try {
	    digest = MessageDigest.getInstance(digestAlgorithm, "BC");
	    digest.update(bytesToDigest);
	    hash = digest.digest();
	} catch (NoSuchAlgorithmException e) {
	    logger.error("getFileDigest::NoSuchAlgorithmException ", e);
	} catch (NoSuchProviderException e) {
	    logger.error("getFileDigest::NoSuchProviderException ", e);
	}
	return hash;
    }
    
    /**
     * Calcul du hash necessaire à la signature electronique
     *
     * @return le condensat formatté hexa (base64?)
     */
    public String getShaDigestToSign()
    {
	if (this.isInSignatureMode())
	{
	    byte[] myhash;
            String signatureFormat = this.getSignatureFormat();
	    if (!signatureFormat.equalsIgnoreCase("pesv2")
                    && !signatureFormat.toLowerCase().startsWith("xades")) {
		myhash = getBytesDigest(getBytesToSign(), "SHA1");
                if (signatureFormat.equalsIgnoreCase("CMS-Allin1")) {
                    // Il faut donner la signature courante!
                    this.setP7sSignatureString();
                }
            }
	    else { // cas: "PESv2", "XADES", "XADES-env" "XADES-env-xpath" "XADES-T-env" voir specs Libersign
		ContentReader reader = contentService.getReader(parapheurService.getDocuments(dossierRef).get(0), ContentModel.PROP_CONTENT);
                if (reader==null) {
                    return null;
                }
                java.io.InputStream is = reader.getContentInputStream();
                if (is == null) {
                    return null;
                }

                if (signatureFormat.equalsIgnoreCase("XADES-env-xpath")) {  // Cas particulier signature DIA
                    /**
                     * Pour ce cas TRES particulier, le flux va descendre en
                     * entier. On canonicalise, avant d'envoyer
                     */
                    try {
                        java.io.ByteArrayOutputStream baStream = new java.io.ByteArrayOutputStream();
                        nu.xom.Builder builder = new nu.xom.Builder();
                        nu.xom.canonical.Canonicalizer canonicalizer = new nu.xom.canonical.Canonicalizer(baStream, nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);
                        logger.debug("c4 ");
                        nu.xom.Document document = builder.build(is);
                        if (null == document.getRootElement()) {
                            this.setPesV2SignParameters(null);
                            return null;
                        }
                        this.setPesV2SignParameters(document.getRootElement().getAttributeValue("Id"));
                        canonicalizer.write(document);
                        myhash = baStream.toByteArray();
                    } catch (nu.xom.ParsingException ex) {
                        logger.error(ex);
                        return null;
                    } catch (IOException ex) {
                        logger.error(ex);
                        return null;
                    }

                    NodeRef parapheurRef = parapheurService.getParentParapheur(dossierRef);
                    // On chope la DESCRIPTION construite ainsi: "pCodePostal pCity"
                    String descriptionString = (String) getNodeService().getProperty(parapheurRef, ContentModel.PROP_DESCRIPTION);
                    if (descriptionString != null) {
                        descriptionString = descriptionString.trim();
                        if (descriptionString.length() > 5) {
                            pPostalCode = descriptionString.substring(0, 5);
                            pCity = descriptionString.substring(5).trim();
                        }
                    }

                } else if (signatureFormat.equalsIgnoreCase("XADES132")) {  // utilise pour ANTS-ECD
                    /*
                     * en attendant mieux, ici on impose le format SHA-256.
                     * C'est péremptoire, mais j'assume ce couplage fort.
                     * 
                     * Amusant: on la joue comme pour PKCS7: on presume qu'il ne s'agit pas de XML...
                     */
                    // TODO : un jour, détecter le format source (XML vs non-XML) pour la canonicalisation éventuelle
                    // FIXME : pas besoin de signature policy ???
                    myhash = getBytesDigest(getBytesToSign(), "SHA-256");

                } else  {  // Cas general de XAdES (non DIA, ni ANTS-ECD)
//                    PesDigest pesDigest = DigestComputer.computeDigest(is,
//                        parapheurService.getXPathSignature(dossierRef));
//                    if (pesDigest == null) {
//                        this.setPesV2SignParameters(null);
//                        return null;
//                    }
//                    this.setPesV2SignParameters(pesDigest.getId());
//                    myhash = pesDigest.getDigest();
                    /*
                     * Changement d'algo pour support Xpath multi-resultats
                     */
                    String xPathSignature = parapheurService.getXPathSignature(dossierRef);
                    System.out.println("xPathSignature = " + xPathSignature);
                    List<PesDigest> pesDigest = DigestComputer.computeDigests(is, xPathSignature);
                    if (pesDigest == null) {
                        this.setPesV2SignParameters(null);
                        return null;
                    } else {
                        int nbre = pesDigest.size();
                        boolean first = true;
                        
                        // serialisation des ID de PES
                        StringBuilder idBuilder = new StringBuilder();
                        // List<String> idsList = new ArrayList<String>(nbre);
                        for (int i = 0; i < nbre; i++) {
                            if (first) {
                                first = false;
                            } else {
                                idBuilder.append(",");
                            }
                            idBuilder.append(pesDigest.get(i).getId());
                            // idsList.add(pesDigest.get(i).getId());
                        }
                        // JSONArray json = new JSONArray(idsList);  <- le JSON n'est pas une bonne idee
                        // this.setPesV2SignParameters(json.toString());
                        this.setPesV2SignParameters(idBuilder.toString());

                        // serialisation des Digests
                        StringBuilder hashesBuilder = new StringBuilder();
                        // List<String> hashList = new ArrayList<String>(nbre);
                        first = true;
                        for (int i = 0; i < nbre; i++) {
                            if (first) {
                                first = false;
                            } else {
                                hashesBuilder.append(",");
                            }
                            
                            myhash = pesDigest.get(i).getDigest();
                            
                            StringBuilder builder=new StringBuilder();
                            if (myhash == null) {
                                // hashList.add("");
                                hashesBuilder.append(" ");
                            } else {
                                for (byte dataByte: myhash) {
                                    builder.append(Integer.toHexString((dataByte & 0xF0)>>4));
                                    builder.append(Integer.toHexString(dataByte & 0x0F));
                                }
                                // hashList.add( builder.toString());
                                hashesBuilder.append( builder.toString());
                            }
                        }
                        // JSONArray jsonHashArray = new JSONArray(hashList);
                        // return jsonHashArray.toString();
                        return hashesBuilder.toString();
                    }
                }
	    }
	    
	    StringBuilder builder=new StringBuilder();
            if (myhash != null) {
                for(byte dataByte: myhash)
                {  // Un octet est représenté par 2 caractères hexadécimaux
                   builder.append(Integer.toHexString((dataByte & 0xF0)>>4));
                   builder.append(Integer.toHexString(dataByte & 0x0F));
                }
            }
	    return builder.toString();
	} else
	{
	    return null;
	}
    }

    private String p7sSignatureString = null;
    private void setP7sSignatureString()
    {
        // Attraper la signature courante du dossier...
        byte[] p7Sig = parapheurService.getSignature(dossierRef);
        if (p7Sig != null) {
            //p7sSignatureString = new String(Base64.encodeBytes(p7Sig));
            p7sSignatureString = Base64.encodeBytes(p7Sig);
//            StringBuilder builder=new StringBuilder();
//            for(byte dataByte: p7Sig)
//            {  // Un octet est représenté par 2 caractères hexadécimaux
//               builder.append(Integer.toHexString((dataByte & 0xF0)>>4));
//               builder.append(Integer.toHexString(dataByte & 0x0F));
//            }
//            p7sSignatureString = builder.toString();
        } else {
            p7sSignatureString = "null";
        }
    }

    public String getP7sSignatureString() {
        return p7sSignatureString;
    }
    
    private String pPesId = null, pPolicyIdentifierID = null, pPolicyIdentifierDescription = null, 
    	pPolicyDigest = null, pSPURI = null, pCity = null, 
    	pPostalCode = null, pCountryName = null, pClaimedRole = null, pEncoding = null;

    private void setPesV2SignParameters(String docId) {
        pPesId = docId;
        Properties props = this.parapheurService.getXadesSignatureProperties(dossierRef);
        pCity = props.getProperty("pCity");
        pPostalCode = props.getProperty("pPostalCode");
        pCountryName = props.getProperty("pCountryName");
        // Le ROLE du signataire dans une signature XAdES est le TITRE de son Parapheur.
        String typeMetier = (String) getNodeService().getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER);
        Map<QName, Serializable> typeProps = parapheurService.getTypeMetierProperties(typeMetier);
        String tdtName = (String) typeProps.get(ParapheurModel.PROP_TDT_NOM);

        if (tdtName != null && tdtName.equals(FastServiceImpl.PROP_TDT_NOM_FAST)) {
            pClaimedRole = props.getProperty("pClaimedRole");
        } else {
            pClaimedRole = this.parapheurService.getNomParapheur(this.parapheurService.getParentParapheur(dossierRef));
        }
// Ce code ne sert à rien:   ne pas confondre signature policy et certificate policy
//        ClientCertificateBean cert = (ClientCertificateBean) FacesHelper.getManagedBean(FacesContext.getCurrentInstance(), "ClientCertificateBean");
//        if (cert == null) {
//            logger.warn("Could not get ClientCertificateBean");
//        } else {
//            Map<String, String> policyProps = parapheurService.getSignaturePolicy(cert.getX509Certificate()[0], tdtName);
//            pPolicyIdentifierID = policyProps.get("pPolicyIdentifierID");
//            pPolicyIdentifierDescription = policyProps.get("pPolicyIdentifierDescription");
//            pSPURI = policyProps.get("pSPURI");
//            pPolicyDigest = policyProps.get("pPolicyDigest");
//        }
        pPolicyIdentifierID = props.getProperty("pPolicyIdentifierID");
        pPolicyIdentifierDescription = props.getProperty("pPolicyIdentifierDescription");
        pSPURI = props.getProperty("pSPURI");
        pPolicyDigest = props.getProperty("pPolicyDigest");

        pEncoding = getFileEncoding(dossierRef);
    }
    
    public String getPesId() {
        return pPesId;
    }

    public String getPolicyIdentifierID() {
        return pPolicyIdentifierID;
    }

    public String getPolicyIdentifierDescription() {
        return pPolicyIdentifierDescription;
    }

    public String getPolicyDigest() {
        return pPolicyDigest;
    }

    public String getSPURI() {
        return pSPURI;
    }

    public String getCity() {
        return pCity;
    }

    public String getPostalCode() {
        return pPostalCode;
    }

    public String getCountryName() {
        return pCountryName;
    }

    public String getClaimedRole() {
        return pClaimedRole;
    }

    public String getEncoding() {
        return pEncoding;
    }

    public String getFileEncoding(NodeRef dossierRef) {
        ContentReader reader = contentService.getReader(parapheurService.getDocuments(dossierRef).get(0),
                ContentModel.PROP_CONTENT);

        return reader.getEncoding();
    }

    public String getBase64BytesToSign() {
        if (this.isInSignatureMode()) {
            return Base64.encodeBytes(getBytesToSign());
        } else {
            return null;
        }
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
            
    public boolean isInSignatureMode() {
	boolean res = false;
	logger.debug("Is in signature mode?");
	if (dossierService.hasReadDossier(dossierRef, AuthenticationUtil.getRunAsUser())
                || !parapheurService.isReadingMandatory(dossierRef))
	{
	    logger.debug("Aspect lu OK");
	    if ("parapheur_approve".equals(this.parameters.get("action")))
	    {
		logger.debug("Approve OK");
		ArrayList<EtapeCircuit> circuit = (ArrayList<EtapeCircuit>) parapheurService.getCircuit(dossierRef);
		if (!circuit.isEmpty())
		{
		    if (!parapheurService.isTermine(dossierRef))
		    {
			logger.debug("Non-termine OK");
			if (circuit.get(0).isApproved())
			{
                            if (circuit.get(0).getActionDemandee()==null)
                            {   // compatibilité ascendante
                                if (circuit.get(circuit.size() - 2).isApproved())
                                {
                                    logger.debug("Derniere etape OK");
                                    HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
                                            .getExternalContext().getRequest();
                                    if (request != null && request.isSecure()) {
                                            logger.debug("Secure OK");
                                            res = true;
                                    }
                                }
                            }
                            else if (doesCurrentStepContainsSign()) {
                                logger.debug("Etape de Signature OK");
                                if (isInSecureMode()) {
                                        logger.debug("Secure OK");
                                        res = true;
                                }
                            }
			}
		    }
		}
	    }
	}
	return res;
    }
    
    private boolean isInSecureMode() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
                .getExternalContext().getRequest();
        return ((request != null) && (request.isSecure()));
    }

    public boolean isInTdtMode() {
        if (inTdtMode == null) {
            try {
                if (!parameters.get("action").equals("parapheur_approve")) {
                    inTdtMode = false;
                } else {
                    inTdtMode = parapheurService.getCurrentEtapeCircuit(dossierRef)
                            .getActionDemandee()
                            .equals(EtapeCircuit.ETAPE_TDT);
                }
            } catch (NullPointerException e) {
                // In some cases, one of the values can be null (ie, old workflows)
                inTdtMode = false;
            }
        }

        return inTdtMode;
    }

    public boolean isInActesMode() {
        if (inActesMode == null) {
            if (!isInTdtMode()) {
                inActesMode = false;
            } else {
                Map<QName, Serializable> props = parapheurService.getTypeMetierProperties((String) getNodeService().getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER));
                String protocol = (String) props.get(ParapheurModel.PROP_TDT_PROTOCOLE);
                if ("actes".equalsIgnoreCase(protocol)) {
                    inActesMode = true;
                } else {
                    inActesMode = false;
                }
            }
        }

        return inActesMode;
    }

    public String getAppletURL()
    {
	String url = this.parapheurService.getSignAppletURL();
	if (url == null) {
            url = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
                    + "/jsp/parapheur/dossier/parapheur-applet.jar";
        }

	return url;
    }

    /**
     * @return Returns the natures.
     */
    public List<SortableSelectItem> getActesNatures() {
        return actesNatures;
    }

    /**
     * @return Returns the classifications.
     */
    public List<SortableSelectItem> getActesClassifications() {
        return actesClassifications;
    }
    
    private boolean doesCurrentStepContainsSign() {
        return (parapheurService.getCurrentEtapeCircuit(dossierRef).getActionDemandee().trim()
                .equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE));
    }
}
