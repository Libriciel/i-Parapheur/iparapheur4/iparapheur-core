/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.bean.ParapheurBean;
import org.adullact.utils.StringUtils;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.ui.common.SortableSelectItem;

import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import java.util.*;

public class MoveDossierDialog extends BaseDialogBean
{
   private UISelectOne comboParapheurs;
   private List<SortableSelectItem> items;
   
   private NodeRef dossierCourant;
   private String dossierName;
   
   private ParapheurService parapheurService;
   private ParapheurBean parapheurBean;

   /**
    * @return Returns the comboParapheurs.
    */
   public UISelectOne getComboParapheurs()
   {
      return comboParapheurs;
   }

   /**
    * @param comboParapheurs The comboParapheurs to set.
    */
   public void setComboParapheurs(UISelectOne comboParapheurs)
   {
      this.comboParapheurs = comboParapheurs;
   }
   
   /**
    * @param parapheurService The parapheurService to set.
    */
   public void setParapheurService(ParapheurService parapheurService)
   {
      this.parapheurService = parapheurService;
   }

    public void setParapheurBean(ParapheurBean parapheurBean) {
        this.parapheurBean = parapheurBean;
    }

    @SuppressWarnings("unchecked")
   public List<SortableSelectItem> getItems()
   {
      List<NodeRef> parapheurs = this.parapheurService.getParapheurs();
      this.items = new ArrayList<SortableSelectItem>(parapheurs.size());
      
      for (NodeRef parapheur : parapheurs)
      {
          NodeRef parapheurCourant = parapheurService.getParentParapheur(dossierCourant);

         if (!parapheurCourant.equals(parapheur))
         {
            String paraphName = this.parapheurService.getNomParapheur(parapheur);
            String ownerName = StringUtils.generateStringFromList(this.parapheurService.getNomProprietaires(parapheur));
            SortableSelectItem item = new SortableSelectItem(parapheur.toString(), paraphName+"  ("+ownerName+")", paraphName);
            this.items.add(item);
         }
      }
      Collections.sort(items);
      
      return this.items;
   }

   /**
    * @return Returns the dossierName.
    */
   public String getDossierName()
   {
      return dossierName;
   }

   /**
    * @see org.alfresco.web.bean.dialog.BaseDialogBean#init(java.util.Map)
    */
   @Override
   public void init(Map<String, String> arg0)
   {
      super.init(arg0);
      if(this.parameters.containsKey("id"))
      {
         dossierCourant = new NodeRef(Repository.getStoreRef(),this.parameters.get("id"));
      }
      else
      {
         throw new AlfrescoRuntimeException("MoveDossierDialog called on a null element");
      }
      if(!this.parapheurService.isDossier(dossierCourant))
         throw new AlfrescoRuntimeException("MoveDossierDialog called on a non-dossier element");
      
      this.dossierName = (String)this.getNodeService().getProperty(dossierCourant, ContentModel.PROP_NAME) +
              " - " + (String)this.getNodeService().getProperty(dossierCourant, ContentModel.PROP_TITLE);
      
   }

   /**
    * @see org.alfresco.web.bean.dialog.BaseDialogBean#finishImpl(javax.faces.context.FacesContext, java.lang.String)
    */
   @Override
   protected String finishImpl(FacesContext context, String outcome) throws Exception
   {
      // Récupération du parapheur sélectionné
      String value = (String) comboParapheurs.getValue();
      NodeRef parapheur = new NodeRef(value);
      
      // Appel au ParapheurService pour transférer le dossier
      this.parapheurService.moveDossier(dossierCourant, parapheur);
      
      return outcome;
   }

    @Override
    public boolean getFinishButtonDisabled() {
        return false;
    }

}
