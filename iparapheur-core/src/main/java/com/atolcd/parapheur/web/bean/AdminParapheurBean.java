/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.bean;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.web.ui.repo.component.tree.TreeModel;

import java.io.File;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.context.IContextListener;
import org.alfresco.web.app.context.UIContextService;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.FileUploadBean;
import org.alfresco.web.bean.repository.MapNode;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.NodePropertyResolver;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.ui.common.Utils;
import org.alfresco.web.ui.common.component.UIActionLink;
import org.alfresco.web.ui.common.component.data.UIRichList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AdminParapheurBean implements IContextListener {
    private static Log logger = LogFactory.getLog(AdminParapheurBean.class);

    protected NodeService nodeService;
    protected ParapheurService parapheurService;
    protected DossierService dossierService;
    protected S2lowService s2lowService;
    protected ContentService contentService;
    protected AuthenticationService authenticationService;
    protected BrowseBean browseBean;
    protected ParapheurBean parapheurBean;
   
    protected UIRichList parapheurRichList;
    protected UIRichList dossierRichList;
   
    private List<Node> parapheurs;
    private List<Node> dossiers;
    private NodeRef dossierCourant = null;
    private NodeRef parapheurCourant = null;
   
    private File file;
    private String fileName;

    private TreeModel<Node> sampleTree;

    private int nombreDossiersInaccessibles;

    public AdminParapheurBean() {
        UIContextService.getInstance(FacesContext.getCurrentInstance()).registerBean(this);
    }

    /**
     * @return Returns the nodeService.
     */
    public NodeService getNodeService() {
        return nodeService;
    }

    /**
     * @param nodeService The nodeService to set.
     */
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    /**
     * @return Returns the parapheurService.
     */
    public ParapheurService getParapheurService() {
        return parapheurService;
    }

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }
    
    /**
     * @return Returns the parapheurService.
     */
    public DossierService getDossierService() {
        return dossierService;
    }

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setDossierService(DossierService dossierService) {
        this.dossierService = dossierService;
    }

    /**
     * @param service the s2lowService to set
     */
    public void setS2lowService(S2lowService service) {
        s2lowService = service;
    }

    /**
     * @return Returns the contentService.
     */
    public ContentService getContentService() {
        return contentService;
    }

    /**
     * @param contentService The contentService to set.
     */
    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    /**
     * @param browseBean The browseBean to set.
     */
    public void setBrowseBean(BrowseBean browseBean) {
        this.browseBean = browseBean;
    }

    /**
     * @param parapheurBean The parapheurBean to set.
     */
    public void setParapheurBean(ParapheurBean parapheurBean) {
        this.parapheurBean = parapheurBean;
    }

    /**
     * @param authenticationService The authenticationService to set.
     */
    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    /**
     * @return Returns the parapheurRichList.
     */
    public UIRichList getParapheurRichList() {
        return parapheurRichList;
    }

    /**
     * @param parapheurRichList The parapheurRichList to set.
     */
    public void setParapheurRichList(UIRichList racineRichList) {
        this.parapheurRichList = racineRichList;
    }

    /**
     * @return Returns the dossierRichList.
     */
    public UIRichList getDossierRichList() {
        return dossierRichList;
    }

    /**
     * Extension d'un node dédié au parapheur, permettant 
     * l'accès à la délégation.
     */
    public class TreeParapheurNode extends Node {
        
        /**
         * @param un nodeRef vers un parapheur, ou root
         */
        public TreeParapheurNode(NodeRef nodeRef) {
            super(nodeRef);            
        }

        /**
         * Accès au Node pointant vers le parapheur de delegation (programmée ou
         * non).
         * @return un Node parapheur ou <code>null</code> si pas de delegation
         * ou si <code>this</code> ne pointe pas vers un parapheur (eg : root)
         */
        public Node getDelegation() {
            if (!parapheurService.isParapheur(getNodeRef())) {
                return null;
            } 
            NodeRef delegationRef = parapheurService.getDelegation(getNodeRef());
            if (delegationRef == null) {
                delegationRef = parapheurService.getDelegationProgrammee(getNodeRef());
                if (delegationRef == null) {
                    return null;
                }
            }
            return new Node(delegationRef);
        }
        
        public String getCreneau() {
            String creneau = "";
            Date debut = parapheurService.getDateDebutDelegation(getNodeRef());
            Date fin = parapheurService.getDateFinDelegation(getNodeRef());
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            if (debut != null) {
                boolean programmee = debut.after(new Date());
                if (fin != null) {
                    if (programmee) {
                        creneau += "programmée ";
                    }
                    creneau += "du " + format.format(debut) + " au " + format.format(fin);
                }
                else {
                    if (programmee) {
                        creneau += "à partir de ";
                    }
                    else {
                        creneau += "depuis le ";
                    }
                    creneau += format.format(debut);
                }
            }
            return creneau;
        }
    }
    
    public TreeModel<Node> getSampleTree() {
        if (sampleTree == null) {
            sampleTree = new TreeModel<Node>() {

                private Map<Node, List<Node>> children;

                {
                    children = new HashMap<Node, List<Node>>();
                }

                @Override
                public Node getRoot() {
                    return new TreeParapheurNode(parapheurService.getParapheursHome());
                }

                @Override
                public boolean isLeaf(Node object) {
                    return false;
                }

                @Override
                public Node getChild(Node object, int childNumber) {
                    return getChildren(object).get(childNumber);
                }

                @Override
                public int getChildrenCount(Node object) {
                    return getChildren(object).size();
                }

                public List<Node> getChildren(Node object) {
                    if (children.containsKey(object)) {
                        return children.get(object);
                    }

                    List<Node> nodes = new ArrayList<Node>();
                    if (parapheurService.isParapheur(object.getNodeRef())) {
                        for (NodeRef nodeRef : parapheurService.getChildParapheurs(object.getNodeRef())) {
                            nodes.add(new TreeParapheurNode(nodeRef));
                        }
                    } else { // Root node : first step (fake parapheur)
                        String username = AuthenticationUtil.getRunAsUser();
                        List<NodeRef> parapheurRefs;
                        if (parapheurService.isAdministrateurFonctionnel(username)) {
                            // On récupère les parapheur les plus haut dans la hierarchie
                            // parmis ceux que l'admin fonctionnel peut éditer
                            parapheurRefs = parapheurService.getRootManagedParapheursByOpAdmin(username);
                            for (NodeRef parapheur : parapheurRefs) {
                                NodeRef resp = parapheurService.getParapheurResponsable(parapheur);
                                if ((resp == null) || !parapheurRefs.contains(resp)) {
                                    nodes.add(new TreeParapheurNode(parapheur));
                                }
                            }
                        }
                        else { // On récupère les parapheur les plus haut dans la hierarchie
                            parapheurRefs = parapheurService.getParapheurs();
                            for (NodeRef parapheur : parapheurRefs) {
                                if (parapheurService.getParapheurResponsable(parapheur) == null) {
                                    nodes.add(new TreeParapheurNode(parapheur));
                                }
                            }
                        }
                    }

                    children.put(object, nodes);

                    return nodes;
                }

            };
        }

        return sampleTree;
    }

    /**
     * @param dossierRichList The dossierRichList to set.
     */
    public void setDossierRichList(UIRichList dossierRichList) {
        this.dossierRichList = dossierRichList;
    }

    public String getParapheurCourant() {
        String res = "Inapplicable";
        if (this.parapheurCourant != null) {
            res = (String) this.nodeService.getProperty(this.parapheurCourant, ContentModel.PROP_NAME);
        }

        return res;
    }

    public String getParapheurPrecedent() {
        String res = "";
        if (this.parapheurCourant != null) {
            NodeRef prec = this.parapheurService.getParapheurResponsable(this.parapheurCourant);
            if (prec != null) {
                res = (String) this.nodeService.getProperty(prec, ContentModel.PROP_NAME);
            } else {
                res = "Espace de stockage des parapheurs";
            }
        }

        return res;
    }

    public Node getParapheurNode() {
        if (this.parapheurCourant != null) {
            return new Node(this.parapheurCourant);
        } else {
            if (this.getParapheurs().size() > 0) {
                return this.getParapheurs().get(0);
            } else {
                return null;
            }
        }
    }

    public Node getDossierNode() {
        if (this.dossierCourant != null) {
            return new Node(this.dossierCourant);
        } else {
            return null;
        }
    }

    /**
     * @return Returns the parapheurs.
     */
    public List<Node> getParapheurs() {
        if (this.parapheurs == null) {
            // Récupération des parapheurs
            String username = authenticationService.getCurrentUserName();
            List<NodeRef> parapheurRefs;
            if (parapheurService.isAdministrateurFonctionnel(username)) {
                parapheurRefs = this.parapheurService.getAllManagedParapheursByOpAdmin(username);
            }
            else {
                parapheurRefs = this.parapheurService.getParapheurs();
            }
            this.parapheurs = new ArrayList<Node>(parapheurRefs.size());
            if (this.parapheurCourant == null) {
                for (NodeRef tmpParapheur : parapheurRefs) {
                    // Les parapheurs sans supérieur hiérarchique sont ajoutés à la liste de parapheurs
                    if (this.parapheurService.getParapheurResponsable(tmpParapheur) == null) {
                        MapNode racine = new MapNode(tmpParapheur);
                        this.parapheurs.add(racine);
                    }
                }
            } else {
                for (NodeRef tmpParapheur : parapheurRefs) {
                    // Les parapheurs ayant pour supérieur hiérarchique le parapheur courant sont ajoutés à la liste de parapheurs
                    if (this.parapheurCourant.equals(this.parapheurService.getParapheurResponsable(tmpParapheur))) {
                        MapNode parapheur = new MapNode(tmpParapheur);
                        this.parapheurs.add(parapheur);
                    }
                }
            }
        }
        return this.parapheurs;
    }

    /**
     * @param parapheurs The parapheurs to set.
     */
    public void setParapheurs(List<Node> racines) {
        this.parapheurs = racines;
    }

    /**
     * @return Returns the dossiers.
     */
    public List<Node> getDossiers() {
        if (this.dossiers == null) {
            this.dossiers = new ArrayList<Node>();
            java.util.List<NodeRef> parapheursList = null;
            String username = authenticationService.getCurrentUserName();

            if (parapheurService.isAdministrateur(username)) {
                parapheursList = this.parapheurService.getParapheurs();

                if (parapheursList != null) {
                    for (NodeRef parapheur : parapheursList) {
                        if (this.parapheurService.getCorbeilles(parapheur) != null) {
                            for (NodeRef corbeille : this.parapheurService.getCorbeilles(parapheur)) {
                                if (this.parapheurService.getDossiers(corbeille) != null) {
                                    for (NodeRef dossier : this.parapheurService.getDossiers(corbeille)) {
                                        Node dossierNode = new MapNode(dossier);
                                        dossierNode.addPropertyResolver("current", this.parapheurBean.resolverCurrent);
                                        dossierNode.addPropertyResolver("etat", this.resolverState);
                                        this.dossiers.add(dossierNode);
                                    }
                                }
                            }
                        }
                    }
                }
                nombreDossiersInaccessibles = 0; // by design
            }
            else if (parapheurService.isAdministrateurFonctionnel(username)) {
                parapheursList = this.parapheurService.getAllManagedParapheursByOpAdmin(username);
                System.out.println("Admin Fonctionnel Parapheurlist = " + parapheursList.toString());
                int nbDossiersInaccessibles = 0;

                if (parapheursList != null) {
                    for (NodeRef parapheur : parapheursList) {
                        if (this.parapheurService.getCorbeilles(parapheur) != null) {
                            for (NodeRef corbeille : this.parapheurService.getCorbeilles(parapheur)) {
                                if (this.parapheurService.getDossiers(corbeille) != null) {
                                    for (NodeRef dossier : this.parapheurService.getDossiers(corbeille)) {
                                        Node dossierNode = new MapNode(dossier);
                                        if (dossierNode.hasPermission(org.alfresco.service.cmr.security.PermissionService.READ)) {
                                            dossierNode.addPropertyResolver("current", this.parapheurBean.resolverCurrent);
                                            dossierNode.addPropertyResolver("etat", this.resolverState);
                                            this.dossiers.add(dossierNode);
                                            System.out.println("\t got dossier = " + dossierNode.getName());
                                        } else {
                                            logger.error("### OOOCH no READ permission on dossier = " + dossier);

                                            /**
                                             * FIXME: COMMENT FAIRE ????
                                             * Il faudrait pouvoir construire le dossierNode en mode 'admin'....
                                             * 
                                             * ou au moins présenter un compteur (ou une simple liste) des dossiers que cet utilisateur n'a pas le droit de voir?...
                                             */
                                            nbDossiersInaccessibles++;

//                                            try {
//                                                dossierNode = parapheurService.getMapNode(dossier);
//                                            } catch (Exception e) {
//                                                System.out.println("Aïe...: " + e.getLocalizedMessage());
//                                            }
//                                            if (dossierNode!=null) {
//                                                dossierNode.addPropertyResolver("current", this.parapheurBean.resolverCurrent);
//                                                dossierNode.addPropertyResolver("etat", this.resolverState);
//                                                this.dossiers.add(dossierNode);
//                                                System.out.println("\t Finally got dossier = " + dossierNode.getName());
//                                            } else {
//                                                logger.error("### OOOCH dossierNode is NULL :-(");
//                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                nombreDossiersInaccessibles = nbDossiersInaccessibles;
            }
        }
        return dossiers;
    }

    public int getNombreDossiersInaccessibles() {
        return nombreDossiersInaccessibles;
    }
    
    /**
     * @param dossiers The dossiers to set.
     */
    public void setDossiers(List<Node> dossiers) {
        this.dossiers = dossiers;
    }

    public void clickDossier(ActionEvent event) {
        UIActionLink link = (UIActionLink) event.getComponent();
        Map<String, String> params = link.getParameterMap();
        String id = params.get("id");
        if (id != null && id.length() != 0) {
            this.dossierCourant = new NodeRef(Repository.getStoreRef(), id);
        }
    }

    public void clickParapheur(ActionEvent event) {
        UIActionLink link = (UIActionLink) event.getComponent();
        Map<String, String> params = link.getParameterMap();
        String id = params.get("id");
        NodeRef ref = null;
        if (id != null && id.length() != 0) {
            ref = new NodeRef(Repository.getStoreRef(), id);
            if (!parapheurService.isParapheur(ref)) {
                ref = null;
            }
        } else {
            if (this.parapheurCourant != null) {
                ref = this.parapheurService.getParapheurResponsable(this.parapheurCourant);
            }
        }

        try {
            this.parapheurCourant = ref;
            this.parapheurs = null;
            UIContextService.getInstance(FacesContext.getCurrentInstance()).notifyBeans();
        } catch (InvalidNodeRefException refErr) {
            Utils.addErrorMessage(MessageFormat.format(Application.getMessage(
                    FacesContext.getCurrentInstance(), Repository.ERROR_NODEREF), new Object[]{id}));
        }
    }
   
    /**
     * Called when an administrator confirms to delete a "dossier"
     * 
     * @return The outcome
     */
    public String deleteDossierOK() {
        String outcome = "cancel";
        if (dossierCourant != null) {
            /**
             * Email the creator that its "dossier" is being deleted.
             * 
             * There is no "extra" message to be given at the moment...
             */
            parapheurService.auditAndMailAboutDossier(dossierCourant, "EFFACER", null);

            String userName = AuthenticationUtil.getRunAsUser();
            if (parapheurService.isAdministrateurFonctionnel(userName)) {
                AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Void>() {
                    @Override
                    public Void doWork() throws Exception {
                        try {
                            deleteCurrentDossier();
                            UIContextService.getInstance(FacesContext.getCurrentInstance()).notifyBeans();
                            
                        } catch (Exception e) {
                            logger.error("Une erreur s'est produite lors de la suppression du dossier.");
                        }
                        return null;
                    }
                }, AuthenticationUtil.getAdminUserName());
                if (dossierCourant == null) {
                    outcome = "dossierDeleted";
                }
            }
            else {
                deleteCurrentDossier();
                UIContextService.getInstance(FacesContext.getCurrentInstance()).notifyBeans();
                outcome = "dossierDeleted";
            }
        }
        return outcome;
   }
    
    private void deleteCurrentDossier() {
        dossierService.deleteDossier(dossierCourant, true);
        dossierCourant = null;
    }
   
   /**
    * Called when the user confirms they wish to delete a "dossier" space
    * 
    * @return The outcome
    */
   public String deleteParapheurOK()
   {
      String outcome = "cancel";
      
      if (parapheurCourant != null)
      {
         // On vérifie le contenu du parapheur avant de lancer la suppression
         boolean suppression = true;
         List<NodeRef> corbeilles = this.parapheurService.getCorbeilles(this.parapheurCourant);
         for (NodeRef corbeille : corbeilles)
         {
            List<NodeRef> l_dossiers = this.parapheurService.getDossiers(corbeille);
            if (l_dossiers != null && !l_dossiers.isEmpty())
            {
               suppression = false;
               break;
            }
         }
         
         if (suppression)
         {
            // FIXME : doit-on changer les associations de hiérarchie des subordonnés ?
            /*
            List<AssociationRef> lstSubordonnes = this.nodeService.getSourceAssocs(parapheurRef, ParapheurModel.ASSOC_HIERARCHIE);
            for (AssociationRef assoc : lstSubordonnes)
            {
               this.nodeService.removeAssociation(assoc.getSourceRef(), assoc.getTargetRef(), ParapheurModel.ASSOC_HIERARCHIE);
               this.nodeService.createAssociation(assoc.getSourceRef(),this.parapheurService.getParapheurResponsable(parapheurRef),ParapheurModel.ASSOC_HIERARCHIE);
            }
            */
            
            // Suppression effective du parapheur
            NodeRef parentRef = this.nodeService.getPrimaryParent(this.parapheurCourant).getParentRef();
            List<String> userNames = this.parapheurService.getParapheurOwners(this.parapheurCourant);
            this.nodeService.deleteNode(this.parapheurCourant);
            this.parapheurCourant = null;
            this.parapheurs = null;
            
            UIContextService.getInstance(FacesContext.getCurrentInstance()).notifyBeans();
            
            // Suppression ok
            outcome = "parapheurDeleted";
            
            // Si l'admin vient de supprimer son propre parapheur, on met l'UI à jour
            if (userNames != null && userNames.contains(authenticationService.getCurrentUserName()))
               this.browseBean.updateUILocation(parentRef);
         }
         else
         {
            Utils.addErrorMessage(MessageFormat.format(Application.getMessage(
                    FacesContext.getCurrentInstance(), "error_generic"), "Le parapheur contient des dossiers"));
            logger.error("Impossible de supprimer le parapheur, car il contient des dossiers : id = " + this.parapheurCourant);
            outcome = "";
         }
      }
      return outcome;
   }
   
    public String getFileName() {
        // try and retrieve the file and filename from the file upload bean
        // representing the file we previously uploaded.
        FacesContext ctx = FacesContext.getCurrentInstance();
        FileUploadBean fileBean = (FileUploadBean) ctx.getExternalContext().getSessionMap().
                get(FileUploadBean.FILE_UPLOAD_BEAN_NAME);
        if (fileBean != null) {
            this.file = fileBean.getFile();
            this.fileName = fileBean.getFileName();
        }

        return this.fileName;
    }
   
    public String cancel() {
        return "adminConsole";
    }
      
    /**
     * @see org.alfresco.web.app.context.IContextListener#contextUpdated()
     */
    @Override
    public void contextUpdated() {
        if (logger.isDebugEnabled()) {
            logger.debug("Invalidating \"AdminParapheur\" components...");
        }

        if (this.parapheurRichList != null) {
            this.parapheurRichList.setValue(null);
        }
        this.parapheurs = null;

        if (this.dossierRichList != null) {
            this.dossierRichList.setValue(null);
        }
        this.dossiers = null;
    }
   
   // Property Resolvers
   
   public NodePropertyResolver resolverState = new NodePropertyResolver() {
        @Override
      public Object get(Node node) {
         String res = "";
         NodeRef dossierRef = node.getNodeRef();
         if(parapheurService.isDossier(dossierRef))
         {
            if ((Boolean) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TERMINE))
            {
               // Dossier terminé
               NodeRef corbeilleRef = parapheurService.getParentCorbeille(dossierRef); 
               if (ParapheurModel.NAME_RETOURNES.equals(nodeService.getPrimaryParent(corbeilleRef).getQName()))
                  res = "Refusé";
               else
                  res = "Approuvé";
            }
            else
            {
               if ((Boolean) parapheurService.isEmis(dossierRef) != true)
                  res = "En préparation";
               else
               {
                  // Dossier en cours
                  List<String> currentUsers = parapheurService.getActeursCourant(dossierRef);

                   if (currentUsers != null && !currentUsers.isEmpty()) {
                     List<EtapeCircuit> circuit = parapheurService.getCircuit(dossierRef);
                     EtapeCircuit etapeCourante = null, etapeSuivante = null;
                     for (EtapeCircuit etape : circuit)
                     {
                        if (!etape.isApproved())
                        {
                           if (etapeCourante == null)
                           {
                              etapeCourante = etape;
                           }
                           else if (etapeSuivante == null)
                           {
                              etapeSuivante = etape;
                              break;
                           }
                        }
                   }

                     if (etapeSuivante == null)
                        res = "A la signature";
                     else
                     {


                        List<String> proprietaires = parapheurService.getNomProprietaires(etapeSuivante.getParapheur());

                         if (proprietaires != null && !proprietaires.isEmpty()) {
                             if (proprietaires.size() > 1) {
                                res = "Acteurs suivants: ";
                             }
                             else {
                                 res = "Acteur suivant :";
                             }

                            res += org.adullact.utils.StringUtils.generateStringFromList(proprietaires);
                         } else {
                             res = "Acteur suivant :";
                         }

                     }
                  }
                  else
                  {
                     res += "Aucun acteur courant";
                  }
               }
            }
            
         }
         return res;
      }
   };

    @Override
    public void areaChanged() {
        // TODO Auto-generated method stub
    }

    @Override
    public void spaceChanged() {
        // TODO Auto-generated method stub
    }
}
