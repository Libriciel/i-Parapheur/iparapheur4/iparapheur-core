/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.bean;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.transaction.UserTransaction;

import org.adullact.utils.StringUtils;
import org.alfresco.model.ApplicationModel;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.alfresco.web.app.AlfrescoNavigationHandler;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.context.IContextListener;
import org.alfresco.web.app.context.UIContextService;
import org.alfresco.web.app.servlet.DownloadContentServlet;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.NavigationBean;
import org.alfresco.web.bean.repository.MapNode;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.NodePropertyResolver;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.config.ViewsConfigElement;
import org.alfresco.web.ui.common.Utils;
import org.alfresco.web.ui.common.component.IBreadcrumbHandler;
import org.alfresco.web.ui.common.component.UIActionLink;
import org.alfresco.web.ui.common.component.UIBreadcrumb;
import org.alfresco.web.ui.common.component.UIModeList;
import org.alfresco.web.ui.common.component.data.UIRichList;
import org.alfresco.web.ui.repo.component.IRepoBreadcrumbHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import java.io.Serializable;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import fr.bl.iparapheur.web.BlexContext;
import javax.faces.component.html.HtmlCommandLink;
import org.alfresco.service.cmr.preference.PreferenceService;

/**
 * Bean providing properties and behaviour for the "parapheur" screens.
 * @author tbr
 * 
 */
public class ParapheurBean implements IContextListener {

    private static Log logger = LogFactory.getLog(ParapheurBean.class);

    private static final String PAGE_NAME_PARAPHEUR = "parapheur";

    private static final String PAGE_NAME_CORBEILLE = "corbeille";

    private static final String PAGE_NAME_DOSSIER = "dossier";
    
    public static final String PREF_PAGE_SIZE = "org.adullact.iparapheur.pagesize";
    public static final String PREF_VIEW_MODE = "org.adullact.iparapheur.viewmode";

    private NodeService nodeService;

    private ContentService contentService;

    private SearchService searchService;

    private NamespaceService namespaceService;

    private BrowseBean browseBean;

    private NavigationBean navigator;

    private ParapheurService parapheurService;

    /** The CorbeillesService reference*/
    private CorbeillesService corbeillesService;
    
    private DossierService dossierService;
    
    private PreferenceService preferenceService;

    private ViewsConfigElement viewsConfig = null;

    private UIRichList parapheurRichList;

    private UIRichList corbeilleRichList;

    private UIRichList corbeilleVirtuelleRichList;

    private UIRichList dossierRichList;

    private UIRichList parapheursRichList;

    private List<Node> corbeilles;

    private List<Node> dossiers;

    private List<Node> dossiersVirtuels;

    private List<Node> documents;

    private List<Node> parapheurs;

    private Map<NodeRef, Integer> corbeillesChildNumber;

    private String parapheurViewMode;

    private int parapheurPageSize;

    private String corbeilleViewMode;

    private int corbeillePageSize;

    private String dossierViewMode;

    private int dossierPageSize;

    private boolean hasVisual;

    private NodeRef parapheurCourant;

    private List<Node> currentParapheurCibleDelegation;

    private List<CachedNodePropertyResolver> cachedResolvers = new ArrayList<CachedNodePropertyResolver>();

    private static final String MSG_ERROR_DELETE_FILE = "error_delete_file";

    private static final String MSG_ERROR_DELETE_SPACE = "error_delete_space";

    private Node parapheursHomeNode;

    private String corbeillePrefPagesize = null;
    private String corbeillePrefViewMode = null;

    public ParapheurBean() {
        parapheurCourant = null;
        UIContextService.getInstance(FacesContext.getCurrentInstance()).registerBean(this);
        initFromClientConfig();   
    }

    public boolean isPreviewEnabled() {
        return parapheurService.isPreviewEnabled();
    }

    public String getHabillage() {
        if (logger.isDebugEnabled()) {
            logger.debug("##### getHabillage = " + parapheurService.getHabillage());
        }
        return parapheurService.getHabillage();
    }

    /**
     * @param parapheurRichList The parapheurRichList to set.
     */
    public void setParapheurRichList(UIRichList parapheurRichList) {
        this.parapheurRichList = parapheurRichList;
    }

    /**
     * @return Returns the parapheurRichList.
     */
    public UIRichList getParapheurRichList() {
        return this.parapheurRichList;
    }

    /**
     * @return Returns the "parapheur" View mode. See UIRichList
     */
    public String getParapheurViewMode() {
        return this.parapheurViewMode;
    }

    /**
     * @param parapheurViewMode      The "parapheur" View mode to set. See UIRichList.
     */
    public void setParapheurViewMode(String parapheurViewMode) {
        this.parapheurViewMode = parapheurViewMode;
    }

    /**
     * @return Returns the parapheurPageSize.
     */
    public int getParapheurPageSize() {
        return this.parapheurPageSize;
    }

    /**
     * @param parapheurPageSize The parapheurPageSize to set.
     */
    public void setParapheurPageSize(int parapheurPageSize) {
        this.parapheurPageSize = parapheurPageSize;
    }

    /**
     * @param corbeilleRichList The corbeilleRichList to set.
     */
    public void setCorbeilleRichList(UIRichList corbeilleRichList) {
        this.corbeilleRichList = corbeilleRichList;
        if (this.corbeilleRichList != null) {
            // set the initial sort column and direction
            this.corbeilleRichList.setInitialSortColumn(
                    this.viewsConfig.getDefaultSortColumn(PAGE_NAME_CORBEILLE));
            this.corbeilleRichList.setInitialSortDescending(
                    this.viewsConfig.hasDescendingSort(PAGE_NAME_CORBEILLE));
        }
    }

    /**
     * @return Returns the corbeilleRichList.
     */
    public UIRichList getCorbeilleRichList() {
        return this.corbeilleRichList;
    }

    /**
     * @return Returns the "corbeille" View mode. See UIRichList
     */
    public String getCorbeilleViewMode() {
        if (corbeillePrefViewMode == null && this.navigator != null && this.navigator.getCurrentUser() != null) {
            String username = this.navigator.getCurrentUser().getUserName();
            corbeillePrefViewMode = (String) preferenceService.getPreferences(username).get(PREF_VIEW_MODE);
        }
        return ((corbeillePrefViewMode != null)?
                corbeillePrefViewMode :
                this.corbeilleViewMode);
    }

    /**
     * @param corbeilleViewMode      The "corbeille" View mode to set. See UIRichList.
     */
    public void setCorbeilleViewMode(String corbeilleViewMode) {
        this.corbeilleViewMode = corbeilleViewMode;
    }

    /**
     * @return Returns the corbeillePageSize.
     */
    public int getCorbeillePageSize() {
        if (corbeillePrefPagesize == null && this.navigator != null && this.navigator.getCurrentUser() != null) {
            String username = this.navigator.getCurrentUser().getUserName();
            Map<String, Serializable> preferenceMap = preferenceService.getPreferences(username);
            if (preferenceMap.get(PREF_PAGE_SIZE) != null) {
                // Sometimes, a new user have this preference automatically but as an int...
                corbeillePrefPagesize = "" + preferenceMap.get(PREF_PAGE_SIZE);
            }
        }
        return ((corbeillePrefPagesize != null)?
                Integer.parseInt(corbeillePrefPagesize, 10) :
                this.corbeillePageSize);
    }
    
    public void setUserPreference(String preference, Serializable value) {
        if(this.navigator != null && this.navigator.getCurrentUser() != null) {
            String username = this.navigator.getCurrentUser().getUserName();

            Map<String, Serializable> preferenceMap = preferenceService.getPreferences(username);
            preferenceMap.put(preference, value);
            preferenceService.setPreferences(username, preferenceMap);
        }
    }

    /**
     * @param corbeillePageSize The corbeillePageSize to set.
     */
    public void setCorbeillePageSize(int corbeillePageSize) {
        this.corbeillePageSize = corbeillePageSize;
    }

    /**
     * @param corbeilleVirtuelleRichList The corbeilleVirtuelleRichList to set.
     */
    public void setCorbeilleVirtuelleRichList(UIRichList corbeilleVirtuelleRichList) {
        this.corbeilleVirtuelleRichList = corbeilleVirtuelleRichList;
        if (this.corbeilleVirtuelleRichList != null) {
            // set the initial sort column and direction
            this.corbeilleVirtuelleRichList.setInitialSortColumn(
                    this.viewsConfig.getDefaultSortColumn(PAGE_NAME_CORBEILLE));
            this.corbeilleVirtuelleRichList.setInitialSortDescending(
                    this.viewsConfig.hasDescendingSort(PAGE_NAME_CORBEILLE));
        }
    }

    /**
     * @return Returns the corbeilleVirtuelleRichList.
     */
    public UIRichList getCorbeilleVirtuelleRichList() {
        return this.corbeilleVirtuelleRichList;
    }

    /**
     * @param dossierRichList The dossierRichList to set.
     */
    public void setDossierRichList(UIRichList dossierRichList) {
        this.dossierRichList = dossierRichList;
    }

    /**
     * @return Returns the dossierRichList.
     */
    public UIRichList getDossierRichList() {
        return this.dossierRichList;
    }

    /**
     * @return Returns the dossiers View mode. See UIRichList
     */
    public String getDossierViewMode() {
        return this.dossierViewMode;
    }

    /**
     * @param dossierViewMode      The dossier View mode to set. See UIRichList.
     */
    public void setDossierViewMode(String dossierViewMode) {
        this.dossierViewMode = dossierViewMode;
    }

    /**
     * @return Returns the dossiersPageSize.
     */
    public int getDossierPageSize() {
        return this.dossierPageSize;
    }

    /**
     * @param dossierPageSize The dossierPageSize to set.
     */
    public void setDossierPageSize(int dossierPageSize) {
        this.dossierPageSize = dossierPageSize;
    }

    /**
     * @param parapheursRichList The parapheursRichList to set.
     */
    public void setParapheursRichList(UIRichList parapheursRichList) {
        this.parapheursRichList = parapheursRichList;
    }

    /**
     * @return Returns the parapheursRichList.
     */
    public UIRichList getParapheursRichList() {
        return this.parapheursRichList;
    }

    public boolean isHasVisual() {
        return hasVisual;
    }

    public void setHasVisual(boolean hasVisual) {
        this.hasVisual = hasVisual;
    }

    public List<Node> getCorbeilles() {
        if (this.corbeilles == null) {
            boolean habilitations = nodeService.hasAspect(getParapheurCourant(),
                    ParapheurModel.ASPECT_HABILITATIONS);
            boolean habTransmettre = true;
            boolean habTraiter = true;
            boolean habArchivage = true;
            boolean habSecretariat = true;

            if (habilitations) {
                Map<QName, Serializable> properties = nodeService.getProperties(getParapheurCourant());
                habTransmettre = (Boolean) properties.get(ParapheurModel.PROP_HAB_TRANSMETTRE);
                habTraiter = (Boolean) properties.get(ParapheurModel.PROP_HAB_TRAITER);
                habArchivage = (Boolean) properties.get(ParapheurModel.PROP_HAB_ARCHIVAGE);
                habSecretariat = (Boolean) properties.get(ParapheurModel.PROP_HAB_SECRETARIAT);
            }

            corbeilles = new ArrayList<Node>();
            if (habTransmettre) {
                corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_EN_PREPARATION, false));
                corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_RETOURNES, false));
                corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_EN_COURS, true));
            }
            if (habTraiter) {
                // BLEX ; changement de l'ordre : dossier A_VENIR avant A_TRAITER
                if (parapheurService.showAVenir(this.parapheurCourant)) {
                    corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_A_VENIR, true));
                }
                corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_A_TRAITER, false));
                corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_DOSSIERS_DELEGUES, true));
                corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_TRAITES, true));
            }
            if (habTraiter || habTransmettre) {
                corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_RECUPERABLES, true));
            }
            if (habArchivage) {
                corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_A_ARCHIVER, false));
            }
            if (habSecretariat) {
                corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_SECRETARIAT, false));
                corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_A_IMPRIMER, true));
            }
            corbeilles.add(getCorbeilleNode(ParapheurModel.NAME_EN_RETARD, true));
        }

        return this.corbeilles;
    }

    protected Node getCorbeilleNode(QName corbeilleName, boolean virtuelle) {
        NodeRef corbeille = parapheurService.getCorbeille(getParapheurCourant(), corbeilleName);

        MapNode node = new MapNode(corbeille, nodeService, true);
        node.addPropertyResolver("icon", browseBean.resolverSpaceIcon);
        node.addPropertyResolver("smallIcon", browseBean.resolverSmallIcon);
        if (virtuelle) {
            node.addPropertyResolver("childCount", resolverChildNumber);
            node.addPropertyResolver("late", resolverCorbeilleVirtuelleLate);
            node.addPropertyResolver("notLate", resolverCorbeilleVirtuelleNotLate);
        } else {
            node.addPropertyResolver("childCount", resolverChildNumber);
            node.addPropertyResolver("late", resolverCorbeilleLate);
            node.addPropertyResolver("notLate", resolverCorbeilleNotLate);
        }

        return node;
    }

    public List<Node> getDossiers() {
        if (this.dossiers == null) {
            long startTime = 0;
            if (logger.isDebugEnabled()) {
                startTime = System.currentTimeMillis();
            }
            dossiers = new ArrayList<Node>();
            NodeRef corbeille = navigator.getCurrentNode().getNodeRef();
            /* Some kind of optim ?*/

            try {
                /*
                List<NodeRef> dossiersRefs = parapheurService.getDossiers(corbeille);

               long endTime = System.currentTimeMillis();
               logger.error(" Time for search: " + (endTime - startTime) + "ms");


               for (NodeRef dossier : dossiersRefs) {
                   dossiers.add(getDossierNode(dossier));
               }

               logger.error(" Time For node & resolvers= "+ (System.currentTimeMillis() - endTime +"ms"));

                */



                List<ChildAssociationRef> children = nodeService.getChildAssocs(corbeille,
                        ContentModel.ASSOC_CONTAINS,
                        RegexQNamePattern.MATCH_ALL);
                if (logger.isDebugEnabled()) {
                    long endTime = System.currentTimeMillis();
                    logger.debug(" Time for request: " + (endTime - startTime) + "ms");
                }
                for (ChildAssociationRef child : children) {
                    dossiers.add(getDossierNode(child.getChildRef()));
                }
                if (logger.isDebugEnabled()) {
                    long endTime = System.currentTimeMillis();
                    logger.debug(" Total Time for getDossiers: " + (endTime - startTime) + "ms");
                }

            } catch (InvalidNodeRefException exception) {
                logger.warn("Current navigation node is not a 'corbeille': "+ corbeille + ", cause: " + exception.getLocalizedMessage());
            }
        }

        return this.dossiers;
    }

    public List<Node> getDossiersVirtuels() {
        if (this.dossiersVirtuels == null) {
            dossiersVirtuels = new ArrayList<Node>();
            NodeRef corbeilleVirtuelle = navigator.getCurrentNode().getNodeRef();
            for (NodeRef dossier : corbeillesService.getDossiersFromCorbeilleVirtuelle(corbeilleVirtuelle)) {
                Node node = getDossierNode(dossier);
                if (node != null) {
                    dossiersVirtuels.add(node);
                }
            }
        }

        return this.dossiersVirtuels;
    }

    protected Node getDossierNode(NodeRef dossier) {
        //MapNode node = new MapNode(dossier, nodeService, true);
        MapNode node = null;
        try {
            node = parapheurService.getMapNode(dossier);
        } catch (Exception e) {
            System.out.println("Aïe...");
        }
        if (node == null)
            return null;

        node.addPropertyResolver("icon", browseBean.resolverSpaceIcon);
        node.addPropertyResolver("smallIcon", browseBean.resolverSmallIcon);
        node.addPropertyResolver("late", resolverDossierLate);
        node.addPropertyResolver("notLate", resolverDossierNotLate);
        node.addPropertyResolver("step", resolverStep);
        node.addPropertyResolver("circuit", resolverCircuit);

        return node;
    }

    public Node getCurrentParapheurDelegation() {
        NodeRef delegation = parapheurService.getDelegation(getParapheurCourant());

        if (delegation == null) {
            return null;
        }

        return new Node(delegation);
    }

    public List<Node> getCurrentParapheurCibleDelegation() {
        if (currentParapheurCibleDelegation == null) {
            currentParapheurCibleDelegation = new ArrayList<Node>();
            for (NodeRef titulaire : parapheurService.getTitulaires(parapheurCourant)) {
                currentParapheurCibleDelegation.add(new Node(titulaire));
            }
        }
        return currentParapheurCibleDelegation;
    }

    public int getCorbeilleChildNumber(NodeRef corbeilleRef) {
        return (Integer) nodeService.getProperty(corbeilleRef, ParapheurModel.PROP_CHILD_COUNT);
    }

    public List<Node> getDocuments() {
        if (this.documents == null) {
            documents = new ArrayList<Node>();
            NodeRef dossier = navigator.getCurrentNode().getNodeRef();
            List<NodeRef> documentsRef = parapheurService.getDocuments(dossier);
            for (NodeRef document : documentsRef) {
                if (document != null) {
                    Node finalNode = getDocumentNode(document);
                    if (finalNode != null) {
                        documents.add(finalNode);
                    }
                }
            }
        }

        return this.documents;
    }

    protected Node getDocumentNode(NodeRef document) {
        MapNode node = new MapNode(document, nodeService, true);

        this.browseBean.setupCommonBindingProperties(node);

        node.addPropertyResolver("smallIcon", browseBean.resolverSmallIcon);
        node.addPropertyResolver("message", resolverContent);
        node.addPropertyResolver("lu", resolverLu);
        node.addPropertyResolver("visuel", resolverVisuel);
        node.addPropertyResolver("visuelUrl", resolverVisuelUrl);

        // BLEX : determine si la visualisation PES par XWV est possible
        node.addPropertyResolver("pesViewEnabled", this.resolverPesViewEnabled);
        return node;
    }

    public List<Node> getParapheurs() {
        if (this.parapheurs == null) {
            parapheurs = new ArrayList<Node>();
            for (NodeRef parapheurRef : parapheurService.getParapheursSorted()) {
                String user = Application.getCurrentUser(FacesContext.getCurrentInstance()).getUserName();
                if (parapheurService.isParapheurOwner(parapheurRef, user)
                        || parapheurService.isParapheurSecretaire(parapheurRef, user)) {
                    parapheurs.add(getParapheurNode(parapheurRef));
                }
            }
        }
        return this.parapheurs;
    }

    protected Node getParapheurNode(NodeRef parapheurRef) {
        MapNode node = new MapNode(parapheurRef);

        node.addPropertyResolver("icon", browseBean.resolverSpaceIcon);
        node.addPropertyResolver("smallIcon", browseBean.resolverSmallIcon);

        return node;
    }

    /**
     * Action called when a folder space is clicked.
     * Navigate into the space.
     */
    public void clickSpace(ActionEvent event)
    {
       UIActionLink link = (UIActionLink)event.getComponent();
       Map<String, String> params = link.getParameterMap();
       String id = params.get("id");
       if (id != null && id.length() != 0)
       {
          try
          {
             NodeRef ref = new NodeRef(Repository.getStoreRef(), id);

             // handle special folder link node case
             if (ApplicationModel.TYPE_FOLDERLINK.equals(this.getNodeService().getType(ref)))
             {
                ref = (NodeRef)this.getNodeService().getProperty(ref, ContentModel.PROP_LINK_DESTINATION);
             }

             clickSpace(ref);
          }
          catch (InvalidNodeRefException refErr)
          {
             Utils.addErrorMessage(MessageFormat.format(Application.getMessage(
                FacesContext.getCurrentInstance(), Repository.ERROR_NODEREF), new Object[] {id}) );
          }
       }
    }


    public void clickSpace(NodeRef nodeRef) {
        setParapheurCourant(nodeRef);

        browseBean.clickSpace(nodeRef);
    }

    public void setParapheurCourant(NodeRef parapheur) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map session = fc.getExternalContext().getSessionMap();
        session.put("CURRENT_PARAPHEUR", parapheur);
        this.parapheurCourant = parapheur;
        this.currentParapheurCibleDelegation = null;
    }


    public Node getParapheursHomeNode()
      {
         if (this.parapheursHomeNode == null)
         {
            try
            {
               FacesContext fc = FacesContext.getCurrentInstance();
               String xpath = Application.getRootPath(fc) + "/ph:parapheurs";
               List<NodeRef> parapheursHomeRefs = searchService.selectNodes(
                       this.getNodeService().getRootNode(Repository.getStoreRef()),
                       xpath, null, namespaceService, false);
               if (parapheursHomeRefs.size() == 1)
               {
                  this.parapheursHomeNode = new Node(parapheursHomeRefs.get(0));
               }
            }
            catch (InvalidNodeRefException err1)
            {
               // cannot continue if this occurs
            }
            catch (AccessDeniedException err2)
            {
               // cannot see node if this occurs
            }
         }
         return this.parapheursHomeNode;
      }


    public void buildBreadcrumb() {
        FacesContext context = FacesContext.getCurrentInstance();
        //System.out.println("clearBreadcrumb()");
        List<IBreadcrumbHandler> location = navigator.getLocation();
        IBreadcrumbHandler handler = location.get(location.size() - 1);
        if (handler instanceof IRepoBreadcrumbHandler) {
            location.removeAll(location);
            //System.out.println("clearBreadcrumb(): clear");
            NodeRef currentRef = this.navigator.getCurrentNode().getNodeRef();
            if (currentRef.equals(navigator.getCompanyHomeNode().getNodeRef())) {

                navigator.setCurrentNodeId(getParapheursHomeNode().getNodeRef().getId());

            }
            else {
                if (!currentRef.equals(getParapheursHomeNode().getNodeRef())) {
                    NodeRef parent = nodeService.getPrimaryParent(currentRef).getParentRef();
                    String name;
                    if (parapheurService.isDossier(currentRef)) {
                        name = (String) nodeService.getProperty(currentRef, ContentModel.PROP_TITLE);
                    }
                    else {
                        name = Repository.getNameForNode(nodeService, currentRef);
                    }
                    location.add(0, navigator.new NavigationBreadcrumbHandler(currentRef, name));
                    //System.out.println("clearBreadcrumb(): adding" + Repository.getNameForNode(nodeService, currentRef));
                    while (!parent.equals(getParapheursHomeNode().getNodeRef())) {
                        location.add(0, navigator.new NavigationBreadcrumbHandler(parent, Repository.getNameForNode(nodeService, parent)));
                        //System.out.println("clearBreadcrumb(): adding" + Repository.getNameForNode(nodeService, parent));
                        parent = nodeService.getPrimaryParent(parent).getParentRef();
                    }
                }
            }
            //adding ph in front.
            NavigationBean.NavigationBreadcrumbHandler phHandler = navigator.new NavigationBreadcrumbHandler(getParapheursHomeNode().getNodeRef(), "Parapheurs");
            location.add(0, phHandler);
        }
    }

    protected void checkAndRepairBreadcrumb() {
        NodeRef currentRef = this.navigator.getCurrentNode().getNodeRef();

        List<IBreadcrumbHandler> location = navigator.getLocation();
        if (location.size() > 1) {
            IBreadcrumbHandler handler = location.get(location.size() - 1);
            IBreadcrumbHandler prevHandler = location.get(location.size() -2);
            if (handler instanceof IRepoBreadcrumbHandler && prevHandler instanceof IRepoBreadcrumbHandler) {
                NodeRef lastRef = ((IRepoBreadcrumbHandler) handler).getNodeRef();
                NodeRef lastLastRef = ((IRepoBreadcrumbHandler) prevHandler).getNodeRef();
                boolean currentIsCorbeille = parapheurService.isCorbeille(currentRef) || parapheurService.isCorbeilleVirtuelle(currentRef);
                boolean currentIsDossier = parapheurService.isDossier(currentRef);

                boolean lastIsCorbeille = parapheurService.isCorbeille(lastRef) || parapheurService.isCorbeilleVirtuelle(lastRef);
                boolean lastLastIsCorbeille = parapheurService.isCorbeille(lastLastRef) || parapheurService.isCorbeilleVirtuelle(lastLastRef);
                boolean lastIsDossier = parapheurService.isDossier(lastRef);

                if (currentIsCorbeille && lastIsCorbeille && lastLastIsCorbeille) {
                    location.remove(handler);
                    location.remove(prevHandler);
                    this.browseBean.updateUILocation(currentRef);
                }

                if (currentIsDossier && lastIsDossier) {
                    location.remove(handler);
                    //this.browseBean.updateUILocation(currentRef);
                }
            }
        }
    }

    protected NodeRef getContainingParapheurOfCurrentNode() {
        NodeRef currentRef = this.navigator.getCurrentNode().getNodeRef();
        NodeRef retVal = null;
        if (nodeService.exists(currentRef) && !parapheurService.isParapheur(currentRef)) {
            NodeRef candidate = this.parapheurService.getParentParapheur(currentRef);
            if (candidate != null &&
                    this.parapheurService.isParapheur(candidate) &&
                    this.parapheurService.isParapheurOwner(candidate, AuthenticationUtil.getRunAsUser())) {
                retVal = candidate;

            }
        }
        return retVal;
    }

    public NodeRef getParapheurCourant() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map session = fc.getExternalContext().getSessionMap();
        this.parapheurCourant = (NodeRef)session.get("CURRENT_PARAPHEUR");

        NodeRef myContainingParapheur = getContainingParapheurOfCurrentNode();

        if (this.parapheurCourant == null) {
            if (myContainingParapheur != null) {
                setParapheurCourant(myContainingParapheur);
                buildBreadcrumb();
            }
        }
        else {
           if (myContainingParapheur != null) {
               // if we change current parapheur then set the new parapheur
               // and rebuildBreadcrumb path for good.
               if (!parapheurCourant.getId().equals(myContainingParapheur.getId())) {
                   setParapheurCourant(myContainingParapheur);
                   buildBreadcrumb();
               }
               else {
                   // we are still using the same ph but we need to check breadcrumb constitency
                   // remove the last BreadCrumb component if it's the "same" type as currentNode.
                   checkAndRepairBreadcrumb();
               }

           }
        }
        /*
        if (this.parapheurCourant == null) {
            restoreBreadcrumb();
            browseBean.updateUILocation(getParapheursHomeNode().getNodeRef());
        }*/

        return this.parapheurCourant;
    }

    public boolean getCurrentUserOwner() {
        boolean res = false;

        if (getParapheurCourant() != null) {
            String username = this.navigator.getCurrentUser().getUserName();
            if (parapheurService.isParapheurOwner(getParapheurCourant(), username)) {
                res = true;
            }
        }

        return res;
    }
    
    public boolean getCurrentUserSecretaire() {
        boolean res = false;

        if (getParapheurCourant() != null) {
            String username = this.navigator.getCurrentUser().getUserName();
            if (parapheurService.isParapheurSecretaire(getParapheurCourant(), username)) {
                res = true;
            }
        }

        return res;
    }

    public String getCurrentUserFullName() {
        return navigator.getCurrentUser().getFullName(nodeService);
    }

    public String getStep(Node node) {
        if (logger.isDebugEnabled()) logger.debug("node.getName()=["+node.getName()+"]");
        NodeRef nodeRef = node.getNodeRef();
        String res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_emettre");

        if (parapheurService.isDossier(nodeRef)) {
            List<EtapeCircuit> circuit = null;
            if (node.containsPropertyResolver("circuit")) {
                circuit = (List<EtapeCircuit>)node.getProperties().get("circuit");
            } else {
                // cas d'un node créé par Alfresco et non par le code i-parapheur: le nodePropertyResolver n'existe pas
                circuit = parapheurService.getCircuit(nodeRef);
            }
            // List<EtapeCircuit> circuit = (List<EtapeCircuit>) node.getProperties().get("circuit");
            if (!circuit.isEmpty()) {
                if (circuit.get(0).getActionDemandee() == null) { // compatibilité ascendante
                    if (!parapheurService.isTermine(nodeRef)) {
                        if (circuit.get(0).isApproved()) {
                            res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_viser");
                            if (circuit.get(circuit.size() - 2).isApproved()) {
                                res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_signer");
                            }
                        }
                    } else {
                        res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_raz");
                    }
                } else {
                    if (!parapheurService.isTermine(nodeRef)) {
                        if (circuit.get(0).isApproved()) {
                            EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(circuit);
                            String action = etape.getActionDemandee().trim();
                            if (action.equalsIgnoreCase(EtapeCircuit.ETAPE_VISA)) {
                                res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_viser");
                            } else if (action.equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {
                                res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_signer");
                            } else if (action.equalsIgnoreCase(EtapeCircuit.ETAPE_DIFF_EMAIL)) {
                                res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_diffuser_email");
                            } else if (action.equalsIgnoreCase(EtapeCircuit.ETAPE_TDT)) {
                                res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_envoyer_tdt");
                            }
                        }
                    } else {   // rejet ou archivage
                        String status = (String) nodeService.getProperty(nodeRef, ParapheurModel.PROP_STATUS_METIER);
                        if (status.startsWith("Rejet")) {
                            res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_raz");
                        }
                    }
                }
            }
        }
        return res;
    }

    public String getSecretariat(NodeRef dossier) {
        String res = "";

        if (parapheurService.isDossier(dossier)) {
            if (nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT)) {
                res = Application.getMessage(FacesContext.getCurrentInstance(), "action_retour_secretariat");
            } else {
                res = Application.getMessage(FacesContext.getCurrentInstance(), "action_secretariat");
            }
        }

        return res;
    }

    public String getSecretariat() {
        return getSecretariat(this.navigator.getCurrentNode().getNodeRef());
    }

    public String getStep() {
        return getStep(this.navigator.getCurrentNode());
    }

    public boolean isAdminFonctionnel() {
        String username = this.navigator.getCurrentUser().getUserName();
        return parapheurService.isAdministrateurFonctionnel(username);
    }

    public boolean isGestionnaireCircuit(){
        String username = this.navigator.getCurrentUser().getUserName();
        return parapheurService.isGestionnaireCircuits(username);
    }

    public boolean isAdministrateur() {
        String username = this.navigator.getCurrentUser().getUserName();
        return parapheurService.isAdministrateur(username);
    }

    // ------------------------------------------------------------------------------
    // IContextListener implementation
    /**
     * @see org.alfresco.web.app.context.IContextListener#contextUpdated()
     */
    @Override
    public void contextUpdated() {
        if (logger.isDebugEnabled()) {
            logger.debug("Invalidating \"parapheur\" components...");
        }

        corbeillesChildNumber = null;

        // clear the value for the list components - will cause re-bind to it's data and refresh
        if (this.parapheurRichList != null) {
            this.parapheurRichList.setValue(null);
        }

        if (this.corbeilleRichList != null) {
            this.corbeilleRichList.setValue(null);
            if (this.corbeilleRichList.getInitialSortColumn() == null) {
                // set the initial sort column and direction
                this.corbeilleRichList.setInitialSortColumn(
                        this.viewsConfig.getDefaultSortColumn(PAGE_NAME_CORBEILLE));
                this.corbeilleRichList.setInitialSortDescending(
                        this.viewsConfig.hasDescendingSort(PAGE_NAME_CORBEILLE));
            }
        }

        if (this.corbeilleVirtuelleRichList != null) {
            this.corbeilleVirtuelleRichList.setValue(null);
            if (this.corbeilleVirtuelleRichList.getInitialSortColumn() == null) {
                // set the initial sort column and direction
                this.corbeilleVirtuelleRichList.setInitialSortColumn(
                        this.viewsConfig.getDefaultSortColumn(PAGE_NAME_CORBEILLE));
                this.corbeilleVirtuelleRichList.setInitialSortDescending(
                        this.viewsConfig.hasDescendingSort(PAGE_NAME_CORBEILLE));
            }
        }

        if (this.dossierRichList != null) {
            this.dossierRichList.setValue(null);
        }

        if (this.parapheursRichList != null) {
            this.parapheursRichList.setValue(null);
        }

        // reset the lists
        this.corbeilles = null;
        this.dossiers = null;
        this.dossiersVirtuels = null;
        this.documents = null;
        this.parapheurs = null;

        for (CachedNodePropertyResolver resolver : cachedResolvers) {
            resolver.clear();
        }
    }

    /**
     * Called when the user confirms they wish to delete a "dossier" space
     *
     * @return The outcome
     */
    public String deleteDossierOK() {
        String outcomeOverride = "browse";

        // find out what the parent type of the node being deleted
        Node node = this.browseBean.getActionSpace();
        ChildAssociationRef assoc = nodeService.getPrimaryParent(node.getNodeRef());
        NodeRef corbeille = null;
        String corbeilleName = null;
        if (assoc != null) {
            corbeille = assoc.getParentRef();
            if (parapheurService.isCorbeille(corbeille)) {
                corbeilleName = (String) nodeService.getProperty(corbeille, ContentModel.PROP_NAME);
                outcomeOverride = "dossierDeleted";
            }
        }

        String outcome = null;
        Node node_space = this.browseBean.getActionSpace();
        if (node_space != null) {
            try {
                if (logger.isDebugEnabled()) {
                    logger.debug("Trying to delete space: " + node_space.getId());
                }

                String dossierName = node_space.getName();
                dossierService.deleteDossier(node_space.getNodeRef(), true);

                // remove this node from the breadcrumb if required
                List<IBreadcrumbHandler> location = navigator.getLocation();
                IBreadcrumbHandler handler = location.get(location.size() - 1);
                if (handler instanceof BrowseBreadcrumbHandler) {
                    // see if the current breadcrumb location is our node
                    if (((BrowseBreadcrumbHandler) handler).getNodeRef().equals(node_space.getNodeRef()) == true) {
                        location.remove(location.size() - 1);

                        // now work out which node to set the list to refresh against
                        if (!location.isEmpty()) {
                            handler = location.get(location.size() - 1);
                            if (handler instanceof BrowseBreadcrumbHandler) {
                                // change the current node Id
                                navigator.setCurrentNodeId(((BrowseBreadcrumbHandler) handler).getNodeRef().getId());
                            } else {
                                // TODO: shouldn't do this - but for now the user home dir is the root!
                                navigator.setCurrentNodeId(Application.getCurrentUser(FacesContext.getCurrentInstance()).getHomeSpaceId());
                            }
                        }
                    }
                } // Apparemment le test du dessus ne fonctionne pas très bien..
                else if (handler.toString().equals(dossierName))
                {
                    location.remove(location.size() - 1);
                    if (!location.isEmpty() && (corbeilleName != null))
                    {
                        handler = location.get(location.size() - 1);
                        if (handler.toString().equals(corbeilleName)) {
                            // change the current node Id
                            navigator.setCurrentNodeId(corbeille.getId());
                        }
                    }
                }

                // add a message to inform the user that the delete was OK
                String statusMsg = MessageFormat.format(
                        Application.getMessage(FacesContext.getCurrentInstance(), MSG_ERROR_DELETE_FILE),
                        new Object[]{node_space.getName()});
                Utils.addStatusMessage(FacesMessage.SEVERITY_INFO, statusMsg);

                // clear action context
                this.browseBean.setActionSpace(null);

                // setting the outcome will show the browse view again
                outcome = AlfrescoNavigationHandler.CLOSE_DIALOG_OUTCOME
                        + AlfrescoNavigationHandler.OUTCOME_SEPARATOR + "browse";
            } catch (Throwable err) {
                Utils.addErrorMessage(Application.getMessage(
                        FacesContext.getCurrentInstance(), MSG_ERROR_DELETE_SPACE) + err.getMessage(), err);
            }
        } else {
            logger.warn("WARNING: deleteSpaceOK called without a current Space!");
        }

        // if the delete was successful update the outcome
        if (outcome != null) {
            //outcome = AlfrescoNavigationHandler.CLOSE_DIALOG_OUTCOME +
            //          AlfrescoNavigationHandler.OUTCOME_SEPARATOR + outcomeOverride;
            outcome = outcomeOverride;
        }

        UIContextService.getInstance(FacesContext.getCurrentInstance()).notifyBeans();

        return outcome;
    }

    /**
     * Called when the user confirms they wish to delete a "document" space
     *
     * @return The outcome
     */
    public String deleteDocumentOK() {

        String outcome = null;

        Node node = this.browseBean.getDocument();
        if (node != null) {
            try {
                if (logger.isDebugEnabled()) {
                    logger.debug("Trying to delete content node: " + node.getId());
                }

                nodeService.deleteNode(node.getNodeRef());

                // clear action context
                this.browseBean.setDocument(null);

                // setting the outcome will show the browse view again
                outcome = AlfrescoNavigationHandler.CLOSE_DIALOG_OUTCOME
                        + AlfrescoNavigationHandler.OUTCOME_SEPARATOR + "browse";
            } catch (Throwable err) {
                Utils.addErrorMessage(Application.getMessage(
                        FacesContext.getCurrentInstance(), "error_delete_file") + err.getMessage(), err);
            }
        } else {
            logger.warn("WARNING: deleteFileOK called without a current Document!");
        }

        // if the delete was successful update the outcome
        if (outcome != null) {
            // outcome = AlfrescoNavigationHandler.CLOSE_DIALOG_OUTCOME;
            outcome = "documentDeleted";
        }

        return outcome;
    }

    /**
     * Change the current parapheur view mode based on user selection
     *
     * @param event      ActionEvent
     */
    public void parapheurViewModeChanged(ActionEvent event) {
        UIModeList viewList = (UIModeList) event.getComponent();

        // get the view mode ID
        String viewMode = viewList.getValue().toString();

        // set the page size based on the style of display 
        setParapheurPageSize(this.viewsConfig.getDefaultPageSize(PAGE_NAME_PARAPHEUR,
                viewMode));
        if (logger.isDebugEnabled()) {
            logger.debug("Parapheur view page size set to: " + getParapheurPageSize());
        }

        // push the view mode into the lists
        setParapheurViewMode(viewMode);
    }

    /**
     * Change the current corbeille view mode based on user selection
     *
     * @param event      ActionEvent
     */
    public void corbeilleViewModeChanged(ActionEvent event) {
        UIModeList viewList = (UIModeList) event.getComponent();
        // get the view mode ID
        String viewMode = viewList.getValue().toString();

        // set the page size based on the style of display
        setCorbeillePageSize(this.viewsConfig.getDefaultPageSize(PAGE_NAME_CORBEILLE,
            viewMode));
        if (logger.isDebugEnabled()) {
            logger.debug("Corbeille view page size set to: " + getCorbeillePageSize());
        }
        if ((viewMode != null) && !viewMode.equals(corbeillePrefViewMode)) {
            this.corbeillePrefViewMode = viewMode;
            setUserPreference(PREF_VIEW_MODE, viewMode);
        }
    }
    
    public void PagesizeValueChanged(ActionEvent event) {
        String value = (String) event.getComponent().getAttributes().get("pagesizeValue");
        if ((value != null) && !value.equals(corbeillePrefPagesize)) {
            this.corbeillePrefPagesize = value;
            setUserPreference(PREF_PAGE_SIZE, value);
        }
    }

    //  ------------------------------------------------------------------------------
    // Listeners
    /**
     * Event handler called to reset a dossier
     *
     * @param event The event that was triggered
     */
    public void raz(ActionEvent event) {
        UIActionLink link = (UIActionLink) event.getComponent();
        Map<String, String> params = link.getParameterMap();
        String id = params.get("id");

        NodeRef dossierRef = new NodeRef(Repository.getStoreRef(), id);
        NodeRef parentRef = parapheurService.getParentCorbeille(dossierRef);

        UserTransaction tx = null;
        try {
            tx = Repository.getUserTransaction(FacesContext.getCurrentInstance());

            tx.begin();
            parapheurService.reprendreDossier(dossierRef);
            tx.commit();
        } catch (Throwable e) {
            // rollback the transaction
            try {
                if (tx != null) {
                    tx.rollback();
                }
            } catch (Exception ex) {
            }
            Utils.addErrorMessage(MessageFormat.format(Application.getMessage(
                    FacesContext.getCurrentInstance(), "error_generic"), e.getMessage()), e);
            logger.error(e.getMessage(), e);
        }

        // Mise à jour de l'interface
        UIContextService.getInstance(FacesContext.getCurrentInstance()).notifyBeans();
        this.browseBean.updateUILocation(parentRef);
        this.browseBean.updateUILocation(parapheurService.getParentParapheur(dossierRef));
        this.browseBean.updateUILocation(parapheurService.getParentCorbeille(dossierRef));
        this.browseBean.updateUILocation(dossierRef);
    }

    /**
     * Event handler called to get a dossier back
     *
     * @param event The event that was triggered
     */
    public void recuperer(ActionEvent event) {
        UIActionLink link = (UIActionLink) event.getComponent();
        Map<String, String> params = link.getParameterMap();
        String id = params.get("id");

        NodeRef dossierRef = new NodeRef(Repository.getStoreRef(), id);
        NodeRef currentRef = parapheurService.getCorbeille(getParapheurCourant(), ParapheurModel.NAME_RECUPERABLES);

        UserTransaction tx = null;
        try {
            tx = Repository.getUserTransaction(FacesContext.getCurrentInstance());

            tx.begin();
            parapheurService.recupererDossier(dossierRef);
            tx.commit();
        } catch (Throwable e) {
            // rollback the transaction
            try {
                if (tx != null) {
                    tx.rollback();
                }
            } catch (Exception ex) {
            }
            Utils.addErrorMessage(MessageFormat.format(Application.getMessage(
                    FacesContext.getCurrentInstance(), "error_generic"), e.getMessage()), e);
            logger.error(e.getMessage(), e);
        }

        // Mise à jour de l'interface
        UIContextService.getInstance(FacesContext.getCurrentInstance()).notifyBeans();
        NodeRef parentRef = nodeService.getPrimaryParent(currentRef).getParentRef();
        this.browseBean.updateUILocation(parentRef);
        parentRef = nodeService.getPrimaryParent(dossierRef).getParentRef();
        this.browseBean.updateUILocation(parentRef);
        this.browseBean.updateUILocation(dossierRef);
    }

    public void imprimerDossier(ActionEvent event) {
        // TODO
        logger.debug("imprimerDossier");
    }
    // ------------------------------------------------------------------------------
    // Property Resolvers

    public NodePropertyResolver resolverContent = new CachedNodePropertyResolver(new NodePropertyResolver() {

        @Override
        public Object get(Node node) {
            String content = null;

            // get the content property from the node and retrieve the
            // full content as a string (obviously should only be used
            // for small amounts of content)
            ContentReader reader = contentService.getReader(node.getNodeRef(),
                    ContentModel.PROP_CONTENT);

            if (reader != null) {
                content = reader.getContentString();
            }

            return content;
        }

    });

    public NodePropertyResolver resolverChildNumber = new CachedNodePropertyResolver(new NodePropertyResolver() {

        @SuppressWarnings("unchecked")
        @Override
        public Object get(Node node) {
            int noDossiers = getCorbeilleChildNumber(node.getNodeRef());
            return (noDossiers < 0 ? 0 : noDossiers);
        }

    });

    public NodePropertyResolver resolverCurrent = new CachedNodePropertyResolver(new NodePropertyResolver() {

        @Override
        public Object get(Node node) {
            long startTime = 0;
            if (logger.isDebugEnabled()) {
                startTime = System.currentTimeMillis();
            }

            String res = "";
            NodeRef nodeRef = node.getNodeRef();

            if (parapheurService.isDossier(nodeRef)) {
                NodeRef parapheurRef = parapheurService.getParentParapheur(nodeRef);
                res = parapheurService.getNomParapheur(parapheurRef);
                if (nodeService.hasAspect(nodeRef, ParapheurModel.ASPECT_SECRETARIAT)) {
                    res += " (Secrétariat)";
                } else {
                    res += " " + StringUtils.generateStringFromListWithQuotes(parapheurService.getNomProprietaires(parapheurRef), "(", ")");

                }
            }
            if (logger.isDebugEnabled()) {
                long endTime = System.currentTimeMillis();
                logger.debug("Resolver Current: " + (endTime - startTime) + "ms");
            }
            return res;
        }

    });

    public NodePropertyResolver resolverLimit = new CachedNodePropertyResolver(new NodePropertyResolver() {

        @Override
        public Object get(Node node) {
            long startTime = 0;
            if (logger.isDebugEnabled()) {
                startTime = System.currentTimeMillis();
            }

            String res = "";
            NodeRef nodeRef = node.getNodeRef();
            if (parapheurService.isDossier(nodeRef)) {
                Date limit = (Date) nodeService.getProperty(nodeRef, ParapheurModel.PROP_DATE_LIMITE);
                if (limit != null) {
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/y");
                    res = format.format(limit);
                }
            }
            if (logger.isDebugEnabled()) {
                long endTime = System.currentTimeMillis();
                logger.debug("Resolver Limit: " + (endTime - startTime) + "ms");
            }
            return res;
        }

    });

    public NodePropertyResolver resolverStep = new CachedNodePropertyResolver(new NodePropertyResolver() {

        @Override
        public Object get(Node node) {
            long startTime = 0;
            if (logger.isDebugEnabled()) {
                startTime = System.currentTimeMillis();
            }
            String res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_emettre");

            //List<EtapeCircuit> circuit = (List<EtapeCircuit>) node.getProperties().get("circuit");
            NodeRef nodeRef = node.getNodeRef();
            List<EtapeCircuit> circuit = null;
            if (node.containsPropertyResolver("circuit")) {
                circuit = (List<EtapeCircuit>)node.getProperties().get("circuit");
            } else {
                // cas d'un node créé par Alfresco et non par le code i-parapheur: le nodePropertyResolver n'existe pas
                circuit = parapheurService.getCircuit(nodeRef);
            }
            if (parapheurService.isDossier(nodeRef)) {
                EtapeCircuit firstEtapeCircuit = circuit.get(0);
                if (firstEtapeCircuit != null) { //  if (!circuit.isEmpty()) {
                    if (firstEtapeCircuit.getActionDemandee() == null) { // compatibilité ascendante
                    // if (circuit.get(0).getActionDemandee() == null) { // compatibilité ascendante
                        if (!parapheurService.isTermine(nodeRef)) {
                            if (firstEtapeCircuit.isApproved()) {
                            // if (circuit.get(0).isApproved()) {
                                res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_viser");
                                if (circuit.get(circuit.size() - 2).isApproved()) {
                                    res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_signer");
                                }
                            }
                        } else {
                            res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_raz");
                        }
                    } else {
                        if (!parapheurService.isTermine(nodeRef)) {
                            if (firstEtapeCircuit.isApproved()) {
                                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(circuit);
                                String action = etape.getActionDemandee().trim();
                                if (action.equalsIgnoreCase(EtapeCircuit.ETAPE_VISA)) {
                                    res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_viser");
                                } else if (action.equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {
                                    res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_signer");
                                } else if (action.equalsIgnoreCase(EtapeCircuit.ETAPE_DIFF_EMAIL)) {
                                    res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_diffuser_email");
                                } else if (action.equalsIgnoreCase(EtapeCircuit.ETAPE_TDT)) {
                                    res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_envoyer_tdt");
                                }
                            }
                        } else {   // rejet ou archivage
                            String status = (String) nodeService.getProperty(nodeRef, ParapheurModel.PROP_STATUS_METIER);
                            if (status.startsWith("Rejet")) {
                                res = Application.getMessage(FacesContext.getCurrentInstance(), "workflow_raz");
                            }
                        }
                    }
                }
            }
            if (logger.isDebugEnabled()) {
                long endTime = System.currentTimeMillis();
                logger.debug("Resolver Step: " + (endTime - startTime) + "ms");
            }
            return res;
        }

    });

    public NodePropertyResolver resolverLu = new CachedNodePropertyResolver(new NodePropertyResolver() {

        @Override
        public Object get(Node node) {
            return dossierService.hasReadDocument(node.getNodeRef(), navigator.getCurrentUser().getUserName());
        }

    });

    public NodePropertyResolver resolverVisuel = new CachedNodePropertyResolver(new NodePropertyResolver() {

        @Override
        public Object get(Node node) {
            return nodeService.getProperty(node.getNodeRef(), ParapheurModel.PROP_VISUEL_PDF);
        }

    });

    public NodePropertyResolver resolverVisuelUrl = new CachedNodePropertyResolver(new NodePropertyResolver() {

        @Override
        public Object get(Node node) {
            String res = null;
            NodeRef ref = node.getNodeRef();
            String name = (String) nodeService.getProperty(ref, ContentModel.PROP_NAME);
            res = DownloadContentServlet.generateBrowserURL(ref, (name != null ? name : "Document") + ".pdf")
                    + "?property=" + ParapheurModel.PROP_VISUEL_PDF;
            return res;
        }

    });

    public NodePropertyResolver resolverDossierLate = new CachedNodePropertyResolver(new NodePropertyResolver() {

        @Override
        public Object get(Node node) {
            NodeRef dossier = node.getNodeRef();
            Date dateLimite = (Date) nodeService.getProperty(dossier, ParapheurModel.PROP_DATE_LIMITE);

            if (dateLimite != null && new Date().after(dateLimite)) {
                return true;
            }

            return false;
        }

    });

    public NodePropertyResolver resolverDossierNotLate = new CachedNodePropertyResolver(new InvertNodePropertyResolver(resolverDossierLate));

    public NodePropertyResolver resolverCorbeilleLate = new CachedNodePropertyResolver(new LateNodePropertyResolver());

    public NodePropertyResolver resolverCorbeilleVirtuelleLate = new CachedNodePropertyResolver(new LateNodePropertyResolver());

    public NodePropertyResolver resolverCorbeilleNotLate = new CachedNodePropertyResolver(new InvertNodePropertyResolver(resolverCorbeilleLate));

    public NodePropertyResolver resolverCorbeilleVirtuelleNotLate = new CachedNodePropertyResolver(new InvertNodePropertyResolver(resolverCorbeilleVirtuelleLate));

    public NodePropertyResolver resolverCircuit = new CachedNodePropertyResolver(new NodePropertyResolver() {

        @Override
        public Object get(Node node) {
            return parapheurService.getCircuit(node.getNodeRef());
        }

    });

    public static class InvertNodePropertyResolver implements NodePropertyResolver {

        private NodePropertyResolver original;

        public InvertNodePropertyResolver(NodePropertyResolver original) {
            this.original = original;
        }

        @Override
        public Object get(Node node) {
            Object result = original.get(node);
            boolean bResult = (Boolean) result;
            return !bResult;
        }

    }

    public class LateNodePropertyResolver implements NodePropertyResolver {

        @Override
        public Object get(Node node) {
            Object late = nodeService.getProperty(node.getNodeRef(), ParapheurModel.PROP_CORBEILLE_LATE);
            if (late == null) {
                return false;
            }
            return late;
        }
    }

    public class CachedNodePropertyResolver implements NodePropertyResolver {

        private Map<NodeRef, Object> mappedCache = new HashMap<NodeRef, Object>();

        private NodePropertyResolver originalPropertyResolver;

        public CachedNodePropertyResolver(NodePropertyResolver originalPropertyResolver) {
            cachedResolvers.add(this);
            this.originalPropertyResolver = originalPropertyResolver;
        }

        @Override
        public Object get(Node node) {
            NodeRef nodeRef = node.getNodeRef();
            if (mappedCache.containsKey(nodeRef)) {
                // System.out.println("Cache hit");
                return mappedCache.get(nodeRef);
            }

            // System.out.println("Cache miss");
            Object originalValue = originalPropertyResolver.get(node);
            mappedCache.put(nodeRef, originalValue);
            return originalValue;
        }

        public void clear() {
            mappedCache.clear();
        }

    }

    /**
     * Initialise default values from client configuration
     */
    private void initFromClientConfig() {
        this.viewsConfig = (ViewsConfigElement) Application.getConfigService(
                FacesContext.getCurrentInstance()).getConfig("Views").
                getConfigElement(ViewsConfigElement.CONFIG_ELEMENT_ID);

        // get the defaults for the "parapheur" page
        this.parapheurViewMode = this.viewsConfig.getDefaultView(PAGE_NAME_PARAPHEUR);
        this.parapheurPageSize = this.viewsConfig.getDefaultPageSize(PAGE_NAME_PARAPHEUR,
                this.parapheurViewMode);

        // get the default for the "corbeille" page
        this.corbeilleViewMode = this.viewsConfig.getDefaultView(PAGE_NAME_CORBEILLE);
        this.corbeillePageSize = this.viewsConfig.getDefaultPageSize(PAGE_NAME_CORBEILLE,
                this.corbeilleViewMode);
        
        

        // get the default for the "dossier" page
        this.dossierViewMode = this.viewsConfig.getDefaultView(PAGE_NAME_DOSSIER);
        this.dossierPageSize = this.viewsConfig.getDefaultPageSize(PAGE_NAME_DOSSIER,
                this.dossierViewMode);

        if (logger.isDebugEnabled()) {
            logger.debug("Set default \"parapheur\" view mode to: " + this.parapheurViewMode);
            logger.debug("Set default \"parapheur\" page size to: " + this.parapheurPageSize);
            logger.debug("Set default \"corbeille\" view mode to: " + this.corbeilleViewMode);
            logger.debug("Set default \"corbeille\" page size to: " + this.corbeillePageSize);
            logger.debug("Set default \"dossier\" view mode to: " + this.dossierViewMode);
            logger.debug("Set default \"dossier\" page size to: " + this.dossierPageSize);
        }
    }

    /**
     * Class to handle breadcrumb interaction for Browse pages
     */
    private class BrowseBreadcrumbHandler implements IRepoBreadcrumbHandler {

        private static final long serialVersionUID = 3833183653173016630L;

        /**
         * Constructor
         *
         * @param NodeRef    The NodeRef for this browse navigation element
         * @param label      Element label
         */
        public BrowseBreadcrumbHandler(NodeRef nodeRef, String label) {
            this.label = label;
            this.nodeRef = nodeRef;
        }

        /**
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return this.label;
        }

        /**
         * @see org.alfresco.web.ui.common.component.IBreadcrumbHandler#navigationOutcome(org.alfresco.web.ui.common.component.UIBreadcrumb)
         */
        @SuppressWarnings("unchecked")
        @Override
        public String navigationOutcome(UIBreadcrumb breadcrumb) {
            // All browse breadcrumb element relate to a Node Id - when selected we
            // set the current node id
            navigator.setCurrentNodeId(this.nodeRef.getId());
            navigator.setLocation((List) breadcrumb.getValue());

            // setup the dispatch context
            navigator.setupDispatchContext(new Node(this.nodeRef));

            // return to browse page if required
            return (isViewCurrent() ? null : "browse");
        }

        @Override
        public NodeRef getNodeRef() {
            return this.nodeRef;
        }

        private NodeRef nodeRef;

        private String label;

    }

    /**
     * @return whether the current View ID is the "browse" screen
     */
    private boolean isViewCurrent() {
        return (FacesContext.getCurrentInstance().getViewRoot().getViewId().equals(BrowseBean.BROWSE_VIEW_ID));
    }

    @Override
    public void areaChanged() {
    }

    @Override
    public void spaceChanged() {
    }

    /**
     * Returns a reference to the ParapheurService of this application.
     *
     * This method shouldn't exists (bean dependencies shouldn't be available to
     * other classes), but removing this getter breaks some JSP pages.
     *
     * This method is deprecated because it shouldn't be used anymore. It
     * remains present in the code base only for legacy JSP pages already using
     * it.
     * 
     * @return a reference to the ParapheurService
     * @deprecated since 3.1
     */
    @Deprecated
    public ParapheurService getParapheurService() {
        return parapheurService;
    }

    @Deprecated
    public BrowseBean getBrowseBean() {
        return browseBean;
    }

    @Deprecated
    public NodeService getNodeService() {
        return nodeService;
    }

    // <editor-fold defaultstate="collapsed" desc="Setters for DI">
    public void setNodeService(NodeService _nodeService) {
        nodeService = _nodeService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setSearchService(SearchService _searchService) {
        searchService = _searchService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setBrowseBean(BrowseBean browseBean) {
        this.browseBean = browseBean;
    }

    public void setNavigator(NavigationBean navigator) {
        this.navigator = navigator;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setCorbeillesService(CorbeillesService corbeillesService) {
        this.corbeillesService = corbeillesService;
    }
    
    public void setDossierService(DossierService dossierService) {
        this.dossierService = dossierService;
    }
    
    public void setPreferenceService(PreferenceService preferenceService) {
        this.preferenceService = preferenceService;
    }

    // </editor-fold>
	// BLEX  :  le type de metier PES
	private static final String TYPE_METIER_PES="PES";

    /**
     * Détermine si la visualisation PES est disponible pour un noeud
     * (document) :                                                             <br/><br/>
     *
     * - il faut que le serveur XWV, qui permet de visualiser les PES, soit
     *   utilisable                                                             <br/>
     * - il faut que le document corresponde à un dossier PES                   <br/>
     *
     * @author BLEX
     */
    private NodePropertyResolver resolverPesViewEnabled = new NodePropertyResolver() {
        @Override
        public Object get(Node node) {

            {	// serveur XMV accessible ?
                if (!BlexContext.getInstance().getXwvConfig().isServerUsable())
                return Boolean.FALSE;
            }

            {	// on verifie que le dossier est bien un PES
                NodeRef parentDossierNodeRef = parapheurService.getParentDossier(node.getNodeRef());
                if (parentDossierNodeRef==null) {
                    // impossible de retrouver le dossier parent
                    return Boolean.FALSE;
                }

                if (! ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS.equals(
                        nodeService.getProperty(parentDossierNodeRef, ParapheurModel.PROP_TDT_PROTOCOLE))) {
                    return Boolean.FALSE;
                }

            }

            // bon, ça doit être possible ...
            return Boolean.TRUE;

        }
    };

	/**
	 * L'utilisateur souhaite examiner le PES contenu dans un document
	 *
	 * @author BLEX
	 *
	 * @param event
	 *     nodeRef : le noeud (document xml) a examiner
	 *
	 */
	public void onPesViewClick(ActionEvent event) {

		logger.debug("clic");

		try {
			UIActionLink link = (UIActionLink)event.getComponent();
			Map<String, String> params = link.getParameterMap();
			String paramNodeRef= params.get("nodeRef");
			logger.debug("event.getComponent().getParameterMap().get(\"nodeRef\")="+paramNodeRef);


			if (! NodeRef.isNodeRef(paramNodeRef)) {
				throw new Exception("nodeRef mal forme : "+paramNodeRef);
			}
			// used to work ... :  NodeRef nodeRef = new NodeRef(Repository.getStoreRef(), paramNodeRef);
			NodeRef nodeRef = new NodeRef(paramNodeRef);

			logger.debug("nodeRef="+nodeRef);

			{	// on verifie que le dossier est bien un PES
				NodeRef parentDossierNodeRef = parapheurService.getParentDossier(nodeRef);
				if (parentDossierNodeRef==null) {
					throw new Exception("impossible de retrouver le dossier parent");
				}
				logger.debug("parentDossierNodeRef="+parentDossierNodeRef);

                if (! ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS.equals(
                        nodeService.getProperty(parentDossierNodeRef, ParapheurModel.PROP_TDT_PROTOCOLE))) {
                    throw new Exception("demande d'acces a un noeud dont le dossier n'est pas de type "
                            +ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS+" : "
                            +nodeService.getProperty(parentDossierNodeRef, ParapheurModel.PROP_TDT_PROTOCOLE));
                }
			}

			ContentReader mainDocReader=contentService.getReader(nodeRef, ContentModel.PROP_CONTENT);

			// le nom du document PES (noeud)
			String documentName=(String) getNodeService().getProperties(nodeRef).get(ContentModel.PROP_NAME);

			// appel helper pour contact serveur et récupération url de redirect
			String xwvUrl=BlexContext.getInstance().getXwvConfig()
			.buildViewer()
			.setDatasStream(mainDocReader.getContentInputStream())
			.doPrepare()
			.getRedirectUrl();

			String redirectUrl;
			redirectUrl=xwvUrl;

			logger.debug("redirect to : "+redirectUrl);

			// redirect effectif
			FacesContext.getCurrentInstance()
			.getExternalContext()
			.redirect(redirectUrl);



		} catch (Exception e) {
			String message="Impossible de visualiser le flux PES : "+e.getMessage();
			logger.warn(e);
			Utils.addErrorMessage(MessageFormat.format(Application.getMessage(
					FacesContext.getCurrentInstance(), Repository.ERROR_GENERIC),
					message), e);
		}
	}
}
