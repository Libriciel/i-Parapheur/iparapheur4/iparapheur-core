package com.atolcd.parapheur.web.bean.dialog.converter;

import org.alfresco.service.cmr.repository.NodeRef;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 01/10/12
 * Time: 13:53
 * To change this template use File | Settings | File Templates.
 */
public class NodeRefConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) throws ConverterException {
        return new NodeRef(s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) throws ConverterException {
        String s = null;

        if (o != null) {
            s = o.toString();
        }

        return s;
    }
}
