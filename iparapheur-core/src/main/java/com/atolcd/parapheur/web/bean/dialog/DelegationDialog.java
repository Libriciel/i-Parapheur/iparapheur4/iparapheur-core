/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.bean.dialog;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.transaction.UserTransaction;

import org.adullact.utils.StringUtils;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.servlet.FacesHelper;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.MapNode;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.ui.common.SortableSelectItem;
import org.alfresco.web.ui.common.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.bean.ParapheurBean;
import java.util.Date;
import org.adullact.iparapheur.comparator.SelectItemNodeRefComparator;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeService;

public class DelegationDialog extends BaseDialogBean
{
   private static Log logger = LogFactory.getLog(DelegationDialog.class);
   
   protected ParapheurService parapheurService;
   private NodeRef parapheurCourant;
   
   protected List<Node> parapheursPossibles;
   
   protected SelectItem[] items;
   
   protected NodeRef delegue;

   private boolean activated;
   
   private boolean presentsDelegues;
   
   private Date dateDebut;
   private Date dateFin;

    @Override
    public boolean getFinishButtonDisabled() {
        return false;
    }
   
   @Override
   protected String finishImpl(FacesContext context, String outcome) throws Exception
   {
      UserTransaction tx = null;
      try
      {
         tx = Repository.getUserTransaction(FacesContext.getCurrentInstance(), true);
         tx.begin();
         
         if (activated) {
            this.parapheurService.programmerDelegation(parapheurCourant,
                (NodeRef) delegue,
                dateDebut,
                dateFin,
                this.presentsDelegues);
         }
         else {
             this.parapheurService.supprimerDelegation(parapheurCourant);
         }
         //commit la transaction
         tx.commit();
      }
      catch (Exception err)
      {
         logger.error(err.getMessage(), err);
         
         try { if (tx != null) {tx.rollback();} } catch (Exception tex) {}

         throw new RuntimeException(MessageFormat.format(Application.getMessage(
               FacesContext.getCurrentInstance(), Repository.ERROR_GENERIC), err.getMessage()));
      }
      
      // return the default outcome
      return outcome;
   }
   
   public List<Node> getParapheursPossibles()
   {
      return parapheursPossibles;
   }
   
   public void setParapheursPossibles(List<Node> parapheursPossibles)
   {
      this.parapheursPossibles = parapheursPossibles;
   }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }
    
    public boolean getPresentsDelegues() {
        return presentsDelegues;
    }

    public void setPresentsDelegues(boolean presentsDelegues) {
        this.presentsDelegues = presentsDelegues;
    }
    
    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }
    
    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }
   
   public ParapheurService getParapheurService()
   {
      return parapheurService;
   }
   
   public void setParapheurService(ParapheurService parapheurService)
   {
      this.parapheurService = parapheurService;
   }
   
    public NodeRef getDelegue() {
        if ((delegue == null) && (this.parapheurCourant != null)) {
            delegue = parapheurService.getDelegation(this.parapheurCourant);
            if (delegue == null) {
                delegue = parapheurService.getDelegationProgrammee(this.parapheurCourant);
                if (delegue == null) {
                    delegue = parapheurService.getOldDelegation(this.parapheurCourant);
                }
            }
        }
        return delegue;
    }
   
   public void setDelegue(NodeRef delegue)
   {
      this.delegue = delegue;
   }
   
    public SelectItem[] getItems() {
        return this.items;
    }
   
    public void initListes() {
        
        this.parapheursPossibles = new ArrayList<Node>();

        // on remplit parapheursPossibles
        List<NodeRef> listeParaph = this.parapheurService.getDelegationsPossibles(parapheurCourant);

        String paraphName;
        List<String> proprietaires;
        MapNode node;

        for(NodeRef paraphNode : listeParaph) {
            paraphName = this.parapheurService.getNomParapheur(paraphNode);
            proprietaires = this.parapheurService.getNomProprietaires(paraphNode);

            node = new MapNode(paraphNode);
            node.put("fullName", paraphName + " "+ StringUtils.generateStringFromListWithQuotes(proprietaires, "(", ")"));

            this.parapheursPossibles.add(node);
        }
      
        List<SelectItem> itemsToAdd = new ArrayList<SelectItem>();
        NodeRef parapheurRef;
        String name;
        SelectItem item;

        String nameCurrentUser = AuthenticationUtil.getRunAsUser();
        for (Node currentNode : this.parapheursPossibles) {
            parapheurRef = currentNode.getNodeRef();
            List<String> ownerParapheurs = parapheurService.getParapheurOwners(parapheurRef);
            if (((ownerParapheurs.size() != 1) || !ownerParapheurs.contains(nameCurrentUser))
                    && !parapheurRef.equals(parapheurCourant))
            {
               name = this.parapheurService.getNomParapheur(parapheurRef);
               item = new SelectItem(parapheurRef, (String)currentNode.getProperties().get("fullName"), name);
               itemsToAdd.add(item);
            }
        }
        //Item non selectionnable
        item = new SelectItem("", "-- Choisir une délégation --", "", true);

        itemsToAdd.add(item);
        this.items = new SelectItem[itemsToAdd.size()];
        this.items = itemsToAdd.toArray(items);
        try {
            Arrays.sort(this.items, new SelectItemNodeRefComparator());
        }
        catch(ClassCastException e) {
            if (logger.isDebugEnabled()) {
                logger.debug("Erreur lors du tri des délégations possibles : " + e.getMessage());
            }
        }
      
   }
   
    @Override
    public void init(Map<String, String> arg0)
    {
        super.init(arg0);
        ParapheurBean parapheurBean = (ParapheurBean)FacesHelper.getManagedBean(FacesContext.getCurrentInstance(), "ParapheurBean");
        this.parapheurCourant = parapheurBean.getParapheurCourant();
        initListes();
        this.dateDebut = parapheurService.getDateDebutDelegation(parapheurCourant);
        this.dateFin = parapheurService.getDateFinDelegation(parapheurCourant);
        if ((dateFin != null) && dateFin.before(new Date())) {
            dateFin = null;
        }
        this.presentsDelegues = parapheurService.arePresentsDelegues(parapheurCourant);
        activated = ((parapheurService.getDelegation(parapheurCourant) != null) ||
                    (parapheurService.getDelegationProgrammee(parapheurCourant) != null));
    }
   
}
