/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.web.bean.wizard;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.job.AbstractBatchJob;
import com.atolcd.parapheur.repo.job.AbstractJob;
import com.atolcd.parapheur.web.action.evaluator.ArchiveEvaluator;
import com.atolcd.parapheur.web.bean.wizard.batch.DossierFilter;
import org.adullact.iparapheur.repo.ged.ArchilandConnector;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.repository.Node;

import javax.faces.context.FacesContext;
import javax.transaction.UserTransaction;
import java.util.ArrayList;
import java.util.List;


public class ArchivageBatchWorkflowWizard extends AbstractBatchWorkflowWizard {

    private final static String PDF_EXTENSION = ".pdf";

    private ArchilandConnector archilandConnector;
    private Boolean archilandEnabled;

    public void setArchilandConnector(ArchilandConnector archilandConnector) {
        this.archilandConnector = archilandConnector;
    }

    @Override
    protected String getSearchPath() {
        NodeRef parapheurCourant = parapheurBean.getParapheurCourant();

        String parapheurName = (String) nodeService.getProperty(parapheurCourant, ContentModel.PROP_NAME);
        Path path = nodeService.getPath(parapheurCourant);
        Path.Element e = path.last();

        String elementString = e.getElementString();


        QName corbeille = ParapheurModel.NAME_A_ARCHIVER.getPrefixedQName(namespaceService);
        corbeilleName = ParapheurModel.NAME_A_ARCHIVER.toString();
        return String.format("/app:company_home/ph:parapheurs/%s/%s/*", e.getPrefixedString(namespaceService), corbeille.getPrefixString());

    }

    private String _getUniqueArchiveName(String baseName, int it) {
        String uniqueName;

        if (it == 0) {
            uniqueName = baseName + PDF_EXTENSION;
        } else {
            uniqueName = baseName + it + PDF_EXTENSION;
        }

        DossierFilter dossierFilter = new DossierFilter();
        dossierFilter.setSearchPath("/app:company_name/ph:archives/*");

        dossierFilter.addFilterElement(ContentModel.PROP_NAME.getPrefixedQName(namespaceService), baseName);

        ResultSet rs = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_LUCENE, dossierFilter.buildFilterQuery());

        List<NodeRef> list = rs.getNodeRefs();

        rs.close();

        if (list.size() == 0) {
            if (it > 0) {
                return baseName + it + PDF_EXTENSION;
            } else {
                return baseName + PDF_EXTENSION;
            }
        } else {
            return _getUniqueArchiveName(baseName, it + 1);
        }
    }


    private String getUniqueArchiveNameForNodeRef(NodeRef nodeRef) {
        String uniqueName = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_NAME);

        return _getUniqueArchiveName(uniqueName, 0);
    }

    @Override
    public String getFinishButtonLabel() {
        return "Archiver";
    }

    protected boolean isArchilandEnabled() {
        if (this.archilandEnabled == null) {
            UserTransaction tx = this.getTransactionService().getUserTransaction();

            try {
                tx.begin();

                String archilandEnabledString = (String) archilandConnector.getArchilandConfig().get("enabled");
                this.archilandEnabled = archilandEnabledString != null ? "true".equals(archilandEnabledString) : false;
                tx.commit();
            } catch (Exception e) {
                try {
                    tx.rollback();
                } catch (Exception e2) {
                    //System.out.println("exception e2");
                }
                this.archilandEnabled = false;
                System.out.println("Erreur lors de l'initialisation archiland");
                e.printStackTrace();
            }
        }
        return this.archilandEnabled;
    }

    @Override
    protected boolean shallSelectDossier(NodeRef dossier) {
        ArchiveEvaluator evaluator = new ArchiveEvaluator();
        Boolean retval = false;

        if (isArchilandEnabled()) {
            boolean isArchivable = evaluator.evaluate(new Node(dossier));
            boolean hasS2lowAspect = nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW);

            retval = isArchivable && !hasS2lowAspect;
        }
        else {
            retval = evaluator.evaluate(new Node(dossier));
        }
        return retval;
    }

    @Override
    protected String finishImpl(FacesContext facesContext, String s) throws Throwable {
        final NodeRef parapheurRef = this.parapheurBean.getParapheurCourant();
        final String username = AuthenticationUtil.getRunAsUser();

        List<NodeRef> nodesToLock = new ArrayList<NodeRef>();

        for (SelectableDossier d : this.dossiers) {
            if (d.isSelected()) {
                nodesToLock.add(d.getDossier().getNodeRef());
            }
        }

        final List<NodeRef> fNodesToLock = nodesToLock;
        AbstractJob archivageJob = new AbstractBatchJob() {
            @Override
            public Void doWorkUnitInTransaction(NodeRef dossier) {
                //System.out.println(getUniqueArchiveNameForNodeRef(dossier));
                try {
                    parapheurService.archiver(dossier, getUniqueArchiveNameForNodeRef(dossier));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

        };

        // desactivate auto unlocking of handled NodeRefs.
        archivageJob.setShallUnlock(false);
        jobService.postJobAndLockNodes(archivageJob, nodesToLock);

        corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurRef, ParapheurModel.NAME_A_ARCHIVER));

        forceShelfUpdate();

        this.annotation = null;
        this.annotationPrivee = null;
        this.dossiers = null;
        this.dossiersModel = null;
        this.browseBean.updateUILocation(parapheurRef);

        return "success";
    }

    @Override
    public String getStepDescription() {
        return "Veuillez selectionner les dossiers à archiver.";
    }

    @Override
    public String getStepTitle() {
        switch (this.currentStep) {
            case 0:
                return "Dossiers à Archiver";
            default:
                return super.getStepTitle();

        }
    }
}

