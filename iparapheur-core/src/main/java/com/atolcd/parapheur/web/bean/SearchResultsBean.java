/*
 * Version 3.1
 * CeCILL Copyright (c) 2010 ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-Projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.web.bean;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.ActionEvent;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.app.servlet.DownloadContentServlet;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.repository.MapNode;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.NodePropertyResolver;
import org.alfresco.web.ui.common.component.UIActionLink;

/**
 *
 * @author Vivien Barousse
 */
public class SearchResultsBean {

    private BrowseBean browseBean;

    private ParapheurBean parapheurBean;

    private SearchService searchService;

    private NodeService nodeService;

    private ParapheurService parapheurService;

    private ContentService contentService;

    private SearchParameters searchParameters;

    private List<Node> results;

    private int querySize;
    
    public List<Node> getResults() {
        
        Map<NodeRef, MapNode> resultsMap = new HashMap<NodeRef, MapNode>();
        
        if (results == null) {
            results = new ArrayList<Node>();
            // BLEX System.out.println("++++++++ Bench 1searchParameters ++ " + searchParameters);

            if (searchParameters.getQuery() == null) {
                return results;
            }
            
            long begining = System.currentTimeMillis();
            
            ResultSet queryResults = searchService.query(searchParameters);
            
            long searchTime = System.currentTimeMillis() - begining;
            System.out.println(String.format("search time : %d", searchTime));
            
            begining = System.currentTimeMillis();
            
            List<NodeRef> resultsRef = queryResults. getNodeRefs();
            for (NodeRef resultRef : resultsRef) {
                /**
                 * Transforms raw results into valid results.
                 * For example, documents aren't directly displayed, dossiers
                 * containing them are displayed
                 */ 
                NodeRef validResult = getValidSearchResult(resultRef);

                if (validResult != null) {
                    QName typeOfResult = nodeService.getType(resultRef);
                    MapNode result = new MapNode(validResult);
                    if (nodeService.getType(validResult).equals(ParapheurModel.TYPE_DOSSIER)) {
                        // BLEX System.out.println("+++++++++++ found Result ++ is TYPE_DOSSIER");
                        result.addPropertyResolver("icon", resolverFullSpaceIcon);
                        result.put("type", "dossier");
                    } else {
                        // BLEX System.out.print("+++++++++++ found Result ++ is not TYPE_DOSSIER");
                        result.addPropertyResolver("icon", browseBean.resolverFileType32);
                        result.put("type", "archive");
                        /**
                         * Is it really an archive?
                         */
                        if (ParapheurModel.TYPE_ARCHIVE.equals(typeOfResult)) {
                            result.addPropertyResolver("archivesigned", resolverSigned);
                            if (this.nodeService.hasAspect(resultRef, ParapheurModel.ASPECT_SIGNED)) {
                                // System.out.println(", is TYPE_ARCHIVE, signed!");
                                result.addPropertyResolver("mainDoc", resolverMainDoc);
                                result.addPropertyResolver("sig", resolverSig);
                            } else {
                                // do nothing
                                // System.out.println(", is TYPE_ARCHIVE, not signed...");
                            }
                        } else if (ContentModel.TYPE_CONTENT.equals(typeOfResult)) {
                            NodeRef parentRef = nodeService.getPrimaryParent(resultRef).getParentRef();  // attention ça claque si pas autorisé!?!?
                            String parentName = (String)nodeService.getProperty(parentRef, ContentModel.PROP_NAME);
                            if (parentName != null && parentName.equals("Archives")) {
                                ContentReader reader = contentService.getReader(result.getNodeRef(), ContentModel.PROP_CONTENT);
                                if (MimetypeMap.MIMETYPE_PDF.equals(reader.getMimetype())) {
                                    result.addPropertyResolver("archivesigned", this.resolverSigned);
                                    if (this.nodeService.hasAspect(resultRef, ParapheurModel.ASPECT_SIGNED))
                                    {
                                        // System.out.println(", is TYPE_CONTENT, in Archives space as PDF, and signed!");
                                        result.addPropertyResolver("mainDoc", this.resolverMainDoc);
                                        result.addPropertyResolver("sig", this.resolverSig);
                                    } else {
                                        // do nothing
                                        // System.out.println(", is TYPE_CONTENT, in Archives space as PDF, not signed...");
                                    }
                                    //results.add(result);
                                } else {
                                    // do nothing??
                                    // BLEX System.out.println(", is TYPE_CONTENT, in Archives space but not PDF... WTF??");
                                }
                            } else {
                                // do nothing
                                // BLEX System.out.println(", is TYPE_CONTENT, but not in Archives space... WTF??");
                            }
                        } else {
                            // do nothing??
                            // BLEX System.out.println(", is " + typeOfResult.getLocalName() + "... WTF??");
                        }
                    }
                    result.addPropertyResolver("late", parapheurBean.resolverDossierLate);
                    result.addPropertyResolver("notLate", parapheurBean.resolverDossierNotLate);
                    result.addPropertyResolver("download", browseBean.resolverDownload);
                    result.addPropertyResolver("size", browseBean.resolverSize);
                    
                    resultsMap.put(validResult, result);
                    
                    //results.add(result);
                }
            }
            //results.clear();
            results.addAll(resultsMap.values());
            
            System.out.println(String.format("handling time %d", System.currentTimeMillis() - begining));
            
            queryResults.close();
        }
       
        return results;
    }

    public void resultClicked(ActionEvent event) throws IOException {
        UIActionLink comp = (UIActionLink) event.getComponent();

        String id = comp.getParameterMap().get("id");
        NodeRef ref = new NodeRef("workspace://SpacesStore/" + id);
        QName nodeType = nodeService.getType(ref);
        
        if (nodeType.equals(ParapheurModel.TYPE_DOSSIER)) {
            browseBean.clickSpace(event);
        } 
        
        /*
         else if (nodeType.equals(ParapheurModel.TYPE_ARCHIVE)) {
            String downloadUrl = (String) browseBean.resolverDownload.get(new MapNode(ref));
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            context.redirect(context.getRequestContextPath() + downloadUrl);
        }
         */
    }
        
    /**
     * Les resolver de base d'Alfresco ne sont pas consistants.
     *
     * Le resolver de base pour les icones d'espace renvoie uniquement le nom de
     * l'image à afficher (sans le chemin), alors que le resolver pour les 
     * icones de fichier renvoie le chemin complet de l'image à afficher.
     *
     * Ce resolver a pour unique but de rajouter le chemin de l'image au nom 
     * récupéré depuis le resolver de base d'Alfresco.
     */
    private NodePropertyResolver resolverFullSpaceIcon = new NodePropertyResolver() {
        @Override
        public Object get(Node node) {
            return "/images/icons/"
                    + browseBean.resolverSpaceIcon.get(node).toString()
                    + ".gif";
        }
    };

    public NodePropertyResolver resolverMainDoc = new NodePropertyResolver() {
        @Override
        public Object get(Node node) {
            String res = null;
            if (node.hasAspect(ParapheurModel.ASPECT_SIGNED)) {
                NodeRef ref = node.getNodeRef();
                String name = (String) nodeService.getProperty(ref, ParapheurModel.PROP_ORIGINAL_NAME);
                res = DownloadContentServlet.generateBrowserURL(ref, (name != null ? name : "Document"))
                        + "?property=" + ParapheurModel.PROP_ORIGINAL;
            }
            return res;
        }
    };

    public NodePropertyResolver resolverSig = new NodePropertyResolver() {
        @Override
        public Object get(Node node) {
            String res = null;
            if (node.hasAspect(ParapheurModel.ASPECT_SIGNED)) {
                NodeRef ref = node.getNodeRef();
                String name = (String) nodeService.getProperty(ref, ParapheurModel.PROP_ORIGINAL_NAME);
                res = DownloadContentServlet.generateBrowserURL(ref, (name != null ? name + ".zip" : "Signatures.zip"))
                        + "?property=" + ParapheurModel.PROP_SIG;
            }
            return res;
        }
    };

    public NodePropertyResolver resolverSigned = new NodePropertyResolver() {
        @Override
        public Object get(Node node) {
            return node.hasAspect(ParapheurModel.ASPECT_SIGNED) && ParapheurModel.TYPE_ARCHIVE.equals(nodeService.getType(node.getNodeRef()));
        }
    };


   protected NodeRef getValidSearchResult(NodeRef resultRef) {
        QName resultType = nodeService.getType(resultRef);

        // Yoda condition!
        // If archive is the type
        
        if (ParapheurModel.TYPE_DOSSIER.equals(resultType) || ParapheurModel.TYPE_ARCHIVE.equals(resultType)) {
            return resultRef;
        } else if (ContentModel.TYPE_CONTENT.equals(resultType)) {
            NodeRef dossier = parapheurService.getParentDossier(resultRef);
            if (dossier != null) {
                return dossier;
            }
        }

        return null;
    }

    public SearchParameters getSearchParameters() {
        return searchParameters;
    }

    public void setSearchParameters(SearchParameters searchParameters) {
        this.searchParameters = searchParameters;

        // When search parameters are modified, results are reinitialized
        results = null;
    }

    public void setBrowseBean(BrowseBean browseBean) {
        this.browseBean = browseBean;
    }

    public void setParapheurBean(ParapheurBean parapheurBean) {
        this.parapheurBean = parapheurBean;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public int getQuerySize() {
        return querySize;
    }

    public void setQuerySize(int querySize) {
        this.querySize = querySize;
    }

}
