/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.action.executer.MailWithAttachmentActionExecuter;
import com.atolcd.parapheur.repo.action.executer.ParapheurMailActionExecuter;
import com.atolcd.parapheur.repo.action.executer.PatchedMailActionExecuter;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Repository;
import org.apache.commons.validator.EmailValidator;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.File;
import java.util.*;

public class SendDossierByMailDialog extends BaseDialogBean {
    private String subject;
    private String emailAdresses;
    private String body;

    /* Mail securisé specifics */
    private Boolean mailsecActivated;
    private Boolean shallSendPlainTextPassword;
    private String password;

    private NodeRef dossierRef;
    private String dossierName;

    private ParapheurService parapheurService;
    private ActionService actionService;

    //private S2lowService s2lowService;

    private Properties configuration;
    
    private List<NodeRef> selectedAttachments;
    private List<SelectItem> attachments;
    private boolean showAttachments;

    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    public Boolean getMailsecActivated() {
        return mailsecActivated;
    }

    public void setMailsecActivated(Boolean mailsecActivated) {
        this.mailsecActivated = mailsecActivated;
    }

    /**
     * @return Returns the subject.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject The subject to set.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return Returns the email adresses string.
     */
    public String getEmailAdresses() {
        return emailAdresses;
    }

    /**
     * @param emailAdresses the emailAdresses to set
     */
    public void setEmailAdresses(String emailAdresses) {
        this.emailAdresses = emailAdresses;
    }

    /**
     * @return Retunrs the email body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body The email body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return Returns the dossierRef.
     */
    public NodeRef getDossierRef() {
        return dossierRef;
    }

    /**
     * @param dossierRef The dossierRef to set.
     */
    public void setDossierRef(NodeRef dossierRef) {
        this.dossierRef = dossierRef;
    }

    /**
     * @return Returns the dossierName.
     */
    public String getDossierName() {
        return dossierName;
    }

    public Boolean getShallSendPlainTextPassword() {
        return shallSendPlainTextPassword;
    }

    public void setShallSendPlainTextPassword(Boolean shallSendPlainTextPassword) {
        this.shallSendPlainTextPassword = shallSendPlainTextPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    /**
     * @param actionService The actionService to set.
     */
    public void setActionService(ActionService actionService) {
        this.actionService = actionService;
    }

/*
    public void setS2lowService(S2lowService s2lowService) {
        this.s2lowService = s2lowService;
    }
*/
    
    public List<SelectItem> getAttachments() {
        return attachments;
    }
    
    public List<NodeRef> getSelectedAttachments() {
        return this.selectedAttachments;
    }
    
    public void setSelectedAttachments(List<NodeRef> selectedAttachments) {
        this.selectedAttachments = selectedAttachments;
    }

    public boolean getShowAttachments() {
        return showAttachments;
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#init(java.util.Map)
     */
    @Override
    public void init(Map<String, String> parameters) {
        super.init(parameters);

        if (this.parameters.containsKey("id")) {
            dossierRef = new NodeRef(Repository.getStoreRef(), this.parameters.get("id"));
        } else {
            dossierRef = this.navigator.getCurrentNode().getNodeRef();
        }

        if (!this.parapheurService.isDossier(dossierRef))
            throw new AlfrescoRuntimeException("SendDossierByMail called on a non-dossier element");

        this.dossierName = (String) getNodeService().getProperty(dossierRef, ContentModel.PROP_TITLE);
        this.emailAdresses = "";

        //FIXME: do it in configuration !
        this.body = "Veuillez trouver en pièce jointe le contenu du dossier \"" + this.dossierName + "\".\nJe vous souhaite bonne réception.";
        this.subject = "Dossier : " + this.dossierName;

        this.mailsecActivated = false;
        
        this.showAttachments = false;
        this.attachments = new ArrayList<SelectItem>();
        this.selectedAttachments = new ArrayList<NodeRef>();
        List<NodeRef> documents = parapheurService.getAttachments(dossierRef);
        if ((documents != null) && !documents.isEmpty()) {
            this.showAttachments = true;
            for (NodeRef document : documents) { 
                this.attachments.add(new SelectItem(document, (String) getNodeService().getProperty(document, ContentModel.PROP_NAME)));
            }
            if (parapheurService.areAttachmentsIncluded(dossierRef)) {
                selectedAttachments = documents;
            }
        }

    }


    private boolean isValidAddress(String address) {
        EmailValidator emailValidator = EmailValidator.getInstance();
        return emailValidator.isValid(address);
    }

    protected void handleEmailAdresses(Action mailAction) {
        List<String> destinataires;
        if (this.emailAdresses != null) {

            destinataires = splitEmails(this.emailAdresses);

            if (destinataires.size() > 1) {
                mailAction.setParameterValue(PatchedMailActionExecuter.PARAM_TO_MANY, (ArrayList)destinataires);
            }
            else {
                  mailAction.setParameterValue(PatchedMailActionExecuter.PARAM_TO, this.emailAdresses);
            }
        }
    }

    protected List<String> splitEmails(String emails) {
        List<String> retVal = new ArrayList<String>();
        if (emails.contains(",")) {
            String[] tab = emails.split(",");
            for (int i = 0; i < tab.length; i++) {
                String email = tab[i].trim();
                if (email.length() > 0) {
                    retVal.add(email);
                }
            }
        }
        else {
            retVal.add(emails);
        }

        return retVal;
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#finishImpl(javax.faces.context.FacesContext, java.lang.String)
     */
    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Exception {
        finishStandardMail();

        this.browseBean.updateUILocation(this.dossierRef);

        return null;
    }



/*
    protected void finishMailSec() {
        List<String> mails = splitEmails(this.emailAdresses);

        try {
            int mail_id = s2lowService.sendSecureMail(mails, null, null, this.subject, this.body, this.password, this.shallSendPlainTextPassword, this.dossierRef);
            //FIXME: Store the mail id in the node properties
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }*/

    protected void finishStandardMail() {
        // generating PDF File
        File pdfFile = null;
        try {
            pdfFile = this.parapheurService.genererDossierPDF(this.dossierRef, false, this.selectedAttachments, true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Building Mails

        Action mailAction = this.actionService.createAction(MailWithAttachmentActionExecuter.NAME);

        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_TEMPLATE, "parapheur-dossier-email.ftl");
        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_SUBJECT, this.subject);
        this.handleEmailAdresses(mailAction);

        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_TEXT, this.body);

        HashMap<String, File> attachments = new HashMap<String, File>();

        attachments.put(this.dossierName + ".pdf", pdfFile);

        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_ATTACHMENTS, attachments);

        // Building from

        String user = AuthenticationUtil.getRunAsUser();
        String fullName = parapheurService.getPrenomNomFromLogin(user);
        String from = parapheurService.getValidAddressForUser(fullName);
        mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_FROM, from);

        /* "Dirty hack" pour libellé  BLEX */
        if ("blex".equals(parapheurService.getHabillage())) {
            mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_FOOTER, ParapheurMailActionExecuter.PARAM_FOOTER_BLEX);
        } else {
            mailAction.setParameterValue(MailWithAttachmentActionExecuter.PARAM_FOOTER, ParapheurMailActionExecuter.PARAM_FOOTER_LIBRICIEL);
        }
        this.actionService.executeAction(mailAction, dossierRef);
    }

    @Override
    public boolean getFinishButtonDisabled() {
        return false;
    }

}
