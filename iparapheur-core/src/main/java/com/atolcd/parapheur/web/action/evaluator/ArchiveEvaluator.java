/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.action.evaluator;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.app.Application;
import org.alfresco.web.bean.repository.Node;

import javax.faces.context.FacesContext;
import java.util.List;

public class ArchiveEvaluator extends BlindedActionEvaluator {
    @Override
    protected boolean evaluateImpl(Node node, NodeRef ref) {
        boolean res = false;
        NodeRef corbeille = parapheurService.getParentCorbeille(ref);
        String currentUser = Application.getCurrentUser(FacesContext.getCurrentInstance()).getUserName();

        if (parapheurService.isDossier(ref)) {
            List<EtapeCircuit> circuit = null;
            if (node.containsPropertyResolver("circuit")) {
                circuit = (List<EtapeCircuit>) node.getProperties().get("circuit");
            } else {
                // cas d'un node créé par Alfresco et non par le code i-parapheur: le nodePropertyResolver n'existe pas
                circuit = parapheurService.getCircuit(ref);
            }
            /**
             *  compatibilité ascendante, circuits pré-v3...
             */
            if (circuit.get(0).getActionDemandee() == null) { //
                if (currentUser != null &&
                        (parapheurService.isActeurCourant(ref, currentUser) || isSecretaire(currentUser, ref))) {
                    if (parapheurService.isTermine(ref)
                            && ParapheurModel.NAME_A_ARCHIVER.equals(nodeService.getPrimaryParent(corbeille).getQName()))
                        res = true;
                }
            }
            /**
             * Depuis les nouveaux circuits (avec action demandée) on archive si
             *   - en étape d'archivage
             *    ET currentUser pas null 
             *    ET  currentUser propriétaire/secrétaire du parapheur courant détenant le dossier
             *    ET  dossier terminé
             *    ET  dans la corbeille "Dossiers en fin de circuit"
             */
            /**
             * Since 3.3:
             * On doit pouvoir archiver AUSSI les dossiers rejetés.
             *   - L'info d'étape courante n'est plus pertinente...
             * Il faut que: currentUser pas null
             *    ET  currentUser propriétaire/secrétaire du parapheur courant détenant le dossier
             *    ET  dossier terminé
             *    ET  dossier "rejeté" (cf statut métier commence par "Rejet")
             *        ET  dans la corbeille "Dossiers rejetés"
             *    ET Si pas rejeté, alors le dossier doit être en étape d'archivage
             *        ET dans la bannette "en fin de circuit".
             */
            else {

                res = currentUser != null &&
                      (parapheurService.isActeurCourant(ref, currentUser) || isSecretaire(currentUser, ref)) &&
                      parapheurService.isTermine(ref) &&
                      (
                            (parapheurService.isRejete(ref) &&
                            ParapheurModel.NAME_RETOURNES.equals(nodeService.getPrimaryParent(corbeille).getQName()))
                            ||
                            (parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee().trim().equalsIgnoreCase(EtapeCircuit.ETAPE_ARCHIVAGE) &&
                            ParapheurModel.NAME_A_ARCHIVER.equals(nodeService.getPrimaryParent(corbeille).getQName()))
                      );
            }
        }

        return res;
    }

}
