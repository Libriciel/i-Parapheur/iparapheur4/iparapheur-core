/*
 * Version 3.2
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT Projet S.A.
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.bean.dialog;

import java.util.Map;
import javax.faces.context.FacesContext;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.app.Application;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Repository;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;
import fr.bl.iparapheur.srci.SrciService;
import javax.transaction.UserTransaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * BLEX-SRCI
 * backbean pour la boite de dialogue permettant d'interroger l'etat d'un
 * dossier sur un TdT (S*LOW ou autre)
 *
 * @author BLEX - Berger Levrault
 */
public class TdtStatusDialog extends BaseDialogBean {

    private static Log logger = LogFactory.getLog(TdtStatusDialog.class);

    private ParapheurService parapheurService;
    public void setParapheurService(ParapheurService parapheurService) { this.parapheurService = parapheurService; }

    private S2lowService s2lowService;
    public void setS2lowService(S2lowService service) { s2lowService = service; }

    private SrciService srciService;
    public void setSrciService(SrciService srciService) { this.srciService = srciService; }

    private NodeService nodeService;
    @Override
    public void setNodeService(NodeService nodeService) { this.nodeService = nodeService; }

    private NodeRef dossierRef;

    private String statut="statut inconnu";
    public String getStatut() { return statut; }
    public void setStatut(String statut) { this.statut = statut; }

    private String nomDossier;
    public String getNomDossier() {
        return this.nomDossier;
    }

    private String transactionDossier;
    public String getTransactionDossier() {
        return this.transactionDossier;
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#init(java.util.Map)
     */
    @Override
    public void init(Map<String, String> arg0) {
        super.init(arg0);

        logger.debug("init : "+arg0);
    
        if (parameters.containsKey("id")) {
            dossierRef = new NodeRef(Repository.getStoreRef(), this.parameters.get("id"));
        } else {
            dossierRef = this.navigator.getCurrentNode().getNodeRef();
        }

        if (! parapheurService.isDossier(dossierRef)) {
            throw new AlfrescoRuntimeException("n'est pas un dossier ["+dossierRef+"]");
        }

        nomDossier = (String) this.nodeService.getProperty(this.dossierRef, ContentModel.PROP_TITLE);

        if (nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_S2LOW)) {
            statut = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_STATUS);
            String tdtName = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_NOM);
            if ("FAST".equals(tdtName)) {
                transactionDossier = tdtName + " : " + (String)nodeService.getProperty(parapheurService.getDocuments(dossierRef).get(0), ContentModel.PROP_NAME);
            }
            else {
                transactionDossier = tdtName + " : " + ((Integer) this.nodeService.getProperty(this.dossierRef, ParapheurModel.PROP_TRANSACTION_ID)).toString();
            }
        } else if (nodeService.hasAspect(dossierRef, SrciService.K.aspect_srciTransaction)) {
            statut = SrciService.tdtStatusCodeToText(
                    (String) nodeService.getProperty(dossierRef, SrciService.K.property_srciTransaction_status));
            transactionDossier = "SRCI : " + this.nodeService.getProperty(this.dossierRef, SrciService.K.property_srciTransaction_documentName).toString();
        } else {
            throw new AlfrescoRuntimeException("ce dossier n'a pas été envoyé à un TdT");
        }

        if (statut == null) {
            statut = "statut inconnu";
        }


    }

    /**
     * This dialog isn't finishable.
     *
     * The finish button corresponding to this dialog should be hidden, so this method
     * shouldn't be invoked by the dialog container.
     *
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#finishImpl(javax.faces.context.FacesContext, java.lang.String)
     * @throws UnsupportedOperationException when invoked
     */
    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Exception {
        throw new UnsupportedOperationException("Dialog isn't finishable");
    }

    /**
     * Demande d'inteerogation du statut sur le TDT
     */
    public void updateStatus() {
        try {
            if (nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_S2LOW)) {
                statut = s2lowService.statutS2lowToString(this.s2lowService.getInfosS2low(dossierRef));

            } else if (nodeService.hasAspect(dossierRef, SrciService.K.aspect_srciTransaction)) {
                UserTransaction tx = this.getTransactionService().getUserTransaction();
                tx.begin();
                try {
                    statut = srciService.getTdtStatusText(dossierRef);
                    tx.commit();
                } catch (Exception ex) {
                    try {
                        tx.rollback();
                    } catch (Exception rollbackEx) {
                        logger.error("rollback", rollbackEx);
                    }
                    statut="ERREUR ["+ex.getMessage()+"]";
                }

            }
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#getCancelButtonLabel()
     */
    @Override
    public String getCancelButtonLabel() {
        return Application.getMessage(FacesContext.getCurrentInstance(), "s2low_fermer");
    }
}
