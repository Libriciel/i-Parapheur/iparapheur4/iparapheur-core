/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.web.bean;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.ParapheurService;
import javax.faces.event.ActionEvent;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.NavigationBean;
import org.apache.log4j.Logger;



/**
 *
 * @author eperalta
 */
public class SignerPapierBean {
    private NavigationBean navigator;
    private BrowseBean browseBean;
    private NodeService nodeService;
    private TenantService tenantService;
    private ParapheurService parapheurService;
    private CorbeillesService corbeillesService;
    private AuthenticationComponent authenticationComponent;

    private static Logger logger = Logger.getLogger(SignerPapierBean.class);

    public void changeSignatureToSignaturePapier(ActionEvent event) {
        NodeRef dossierRef = navigator.getCurrentNode().getNodeRef();

        boolean signature_papier = this.nodeService.hasAspect(dossierRef, ParapheurModel.PROP_SIGNATURE_PAPIER);

        if (!signature_papier) {
            final NodeRef fDossierRef = dossierRef;
            this.parapheurService.changeSignatureToSignaturePapier(dossierRef);

            this.corbeillesService.insertIntoCorbeilleAImprimer(dossierRef);

            this.browseBean.updateUILocation(dossierRef);
        }
        else {
            if (logger.isDebugEnabled()) {
                logger.debug("Le dossier possède déja la propriété PROP_SIGNATURE_PAPIER ("+dossierRef+")");
            }
        }
    }

    /**
     * @param corbeillesService The CorbeillesService to set.
     */
    public void setCorbeillesService(CorbeillesService corbeillesService)
    {
	this.corbeillesService = corbeillesService;
    }
    /**
     * @param parapheurService The ParapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }
    public void setNodeService(NodeService nodeService)
    {
	this.nodeService = nodeService;
    }

    public void setNavigator(NavigationBean navigator) {
        this.navigator = navigator;
    }

    public void setBrowseBean(BrowseBean browseBean) {
        this.browseBean = browseBean;
    }
    
    public void setAuthenticationComponent(AuthenticationComponent authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }


}
