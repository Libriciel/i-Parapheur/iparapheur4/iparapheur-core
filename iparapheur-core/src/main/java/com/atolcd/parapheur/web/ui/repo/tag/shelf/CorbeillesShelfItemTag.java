/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.ui.repo.tag.shelf;

import com.atolcd.parapheur.web.ui.repo.component.shelf.UICorbeillesShelfItem;
import javax.faces.FacesException;
import javax.faces.component.UIComponent;
import javax.faces.el.MethodBinding;
import org.springframework.extensions.webscripts.ui.common.tag.BaseComponentTag;

/**
 * @author bma
 */
public class CorbeillesShelfItemTag extends BaseComponentTag
{
   /** the value */
   private String value;
   
   /** the clickActionListener */
   private String clickActionListener;
   
   
   /**
    * @see javax.faces.webapp.UIComponentTag#getComponentType()
    */
    @Override
   public String getComponentType()
   {
      return "org.atolcd.faces.CorbeillesShelfItem";
   }

   /**
    * @see javax.faces.webapp.UIComponentTag#getRendererType()
    */
    @Override
   public String getRendererType()
   {
      // self rendering component
      return null;
   }
   
   /**
    * @see javax.faces.webapp.UIComponentTag#setProperties(javax.faces.component.UIComponent)
    */
    @Override
   protected void setProperties(UIComponent component)
   {
      super.setProperties(component);
      setStringBindingProperty(component, "value", this.value);
      if (isValueReference(this.clickActionListener))
      {
         MethodBinding vb = getFacesContext().getApplication().createMethodBinding(this.clickActionListener, ACTION_CLASS_ARGS);
         ((UICorbeillesShelfItem)component).setClickActionListener(vb);
      }
      else
      {
         throw new FacesException("Click Action listener method binding incorrectly specified: " + this.clickActionListener);
      }
   }
   
   /**
    * @see javax.servlet.jsp.tagext.Tag#release()
    */
    @Override
   public void release()
   {
      super.release();
      
      this.value = null;
      this.clickActionListener = null;
   }
   
   /**
    * Set the value used to bind the corbeilles list to the component
    *
    * @param value     the value
    */
   public void setValue(String value)
   {
      this.value = value;
   }
   
   /**
    * Set the clickActionListener
    *
    * @param clickActionListener     the clickActionListener
    */
   public void setClickActionListener(String clickActionListener)
   {
      this.clickActionListener = clickActionListener;
   }
}

