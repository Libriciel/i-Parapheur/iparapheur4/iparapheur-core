/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.bean.dialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.ui.common.SortableSelectItem;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;

public class EnvoiS2lowDialog extends BaseDialogBean {

    private NodeRef dossierRef;
    private String nature;
    protected List<SortableSelectItem> natures;
    private String classification;
    protected List<SortableSelectItem> classifications;
    private String numero;
    private String objet;
    private Date dateActe;
    private String nomDossier;
    protected ParapheurService parapheurService;
    protected S2lowService s2lowService;

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    /**
     * @param service the s2lowService to set
     */
    public void setS2lowService(S2lowService service) {
        s2lowService = service;
    }

    /**
     * @return Returns the dateActe.
     */
    public Date getDateActe() {
        return dateActe;
    }

    /**
     * @param dateActe The dateActe to set.
     */
    public void setDateActe(Date dateActe) {
        this.dateActe = dateActe;
    }

    /**
     * @return Returns the natures.
     */
    public List<SortableSelectItem> getNatures() {
        return natures;
    }

    /**
     * @return Returns the classifications.
     */
    public List<SortableSelectItem> getClassifications() {
        return classifications;
    }

    /**
     * @return Returns the numero.
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero The numero to set.
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return Returns the objet.
     */
    public String getObjet() {
        return objet;
    }

    /**
     * @param objet The objet to set.
     */
    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getNomDossier() {
        if (this.nomDossier == null) {
            this.nomDossier = (String) this.getNodeService().getProperty(this.dossierRef, ContentModel.PROP_TITLE);
        }

        return this.nomDossier;
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#init(java.util.Map)
     */
    @Override
    @SuppressWarnings("unchecked")
    public void init(Map<String, String> arg0) {
        super.init(arg0);
        if (this.parameters.containsKey("id")) {
            dossierRef = new NodeRef(Repository.getStoreRef(), this.parameters.get("id"));
        } else {
            dossierRef = this.navigator.getCurrentNode().getNodeRef();
        }
        if (!this.parapheurService.isDossier(dossierRef)) {
            throw new AlfrescoRuntimeException("EnvoiS2lowDialog called on a non-dossier element");
        }

        // Récupération des natures et classifications d'actes
        this.natures = new ArrayList<SortableSelectItem>();
        Map<Integer, String> naturesS2Low = this.s2lowService.getS2lowActesNatures();
        if (naturesS2Low == null) {
            throw new AlfrescoRuntimeException("Impossible de récupérer la liste de natures");
        }

        for (Entry<Integer, String> nature : naturesS2Low.entrySet()) {
            this.natures.add(new SortableSelectItem(nature.getKey().toString(), nature.getValue(), nature.getValue()));
        }
        Collections.sort(this.natures);
        this.nature = this.natures.get(0).getValue().toString();


        this.classifications = new ArrayList<SortableSelectItem>();
        Map<String, String> classificationsS2Low = this.s2lowService.getS2lowActesClassifications();

        for (Entry<String, String> classification : classificationsS2Low.entrySet()) {
            this.classifications.add(new SortableSelectItem(classification.getKey(), classification.getKey() + " " + classification.getValue(), classification.getKey()));
        }
        Collections.sort(this.classifications);
        this.classification = this.classifications.get(0).getValue().toString();

    }

   @Override
    public boolean getFinishButtonDisabled() {
        return false;
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#finishImpl(javax.faces.context.FacesContext, java.lang.String)
     */
    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Exception {
        // Transformation de la date
        String date = "";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        if (this.dateActe != null) {
            date = formatter.format(this.dateActe);
        }

        // Envoi à la plate-forme S2LOW
        this.s2lowService.envoiS2lowActes(this.dossierRef, this.nature, this.classification, this.numero, this.objet, date);

        return outcome;
    }

    public void changeNature(ValueChangeEvent event) {
        UISelectOne combo = (UISelectOne) event.getComponent().findComponent("comboNatures");
        if (combo != null) {
            if (combo.getValue() != null && !"".equals(combo.getValue())) {
                this.nature = (String) combo.getValue();
            }
        }
    }

    public void changeClassification(ValueChangeEvent event) {
        UISelectOne combo = (UISelectOne) event.getComponent().findComponent("comboClassifications");
        if (combo != null) {
            if (combo.getValue() != null && !"".equals(combo.getValue())) {
                this.classification = (String) combo.getValue();
            }
        }
    }
    
}
