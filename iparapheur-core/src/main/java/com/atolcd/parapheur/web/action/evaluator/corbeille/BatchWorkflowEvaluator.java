/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2012, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.web.action.evaluator.corbeille;

import com.atolcd.parapheur.model.ParapheurModel;
import javax.faces.context.FacesContext;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.action.ActionEvaluator;
import org.alfresco.web.app.servlet.FacesHelper;
import org.alfresco.web.bean.repository.Node;

/**
 *
 * @author Vivien Barousse
 */
public class BatchWorkflowEvaluator implements ActionEvaluator {

    /**
     * Determine si le noeud passé en paramètre (correspondant à une bannette)
     * peut afficher l'action "Signature par lot".
     *
     * L'action "Signature par lot" est disponnible si et seulement si :
     * <ul>
     *     <li>Le noeud est une corbeille "A traiter"</li>
     * </ul>
     *
     * Les habilitations ne sont pas testées dans ce cas là. Le but des
     * habilitationsont pour but uniquement de simplifier l'affichage principal,
     * et la console de validation par lot est toujours disponible depuis la
     * corbeille "A traiter".
     *
     * @param node Le noeud à tester
     * @return true si l'action est à afficher, false sinon.
     */
    @Override
    public boolean evaluate(Node node) {
        NodeRef nodeRef = node.getNodeRef();
        NodeService nodeService = (NodeService) FacesHelper.getManagedBean(FacesContext.getCurrentInstance(), "NodeService");

        QName assocName = nodeService.getPrimaryParent(nodeRef).getQName();

        return assocName.equals(ParapheurModel.NAME_A_TRAITER);
    }

    @Override
    public boolean evaluate(Object o) {
        return evaluate((Node) o);
    }

}
