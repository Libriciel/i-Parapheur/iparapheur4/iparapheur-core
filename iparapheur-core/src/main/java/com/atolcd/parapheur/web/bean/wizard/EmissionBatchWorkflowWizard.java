/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.web.bean.wizard;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.job.AbstractBatchJob;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;

import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;

public class EmissionBatchWorkflowWizard extends AbstractBatchWorkflowWizard {

    @Override
    public String getFinishButtonLabel() {
        return "Emettre";
    }

    @Override
    protected boolean shallSelectDossier(NodeRef dossier) {
        return !parapheurService.isEmis(dossier);

    }

    @Override
    protected String finishImpl(FacesContext facesContext, String s) throws Throwable {
        final NodeRef parapheurRef = this.parapheurBean.getParapheurCourant();
        final String username = AuthenticationUtil.getRunAsUser();

        List<NodeRef> nodesToLock = new ArrayList<NodeRef>();


        for (SelectableDossier d : this.dossiers) {
            if (d.isSelected()) {
                nodesToLock.add(d.getDossier().getNodeRef());
            }
        }

        final List<NodeRef> fNodesToLock = nodesToLock;

        /* when should we serialize the jobInformation ?*/
        jobService.postJobAndLockNodes(new AbstractBatchJob() {
            @Override
            public Void doWorkUnitInTransaction(NodeRef dossier) {
                parapheurService.setAnnotationPrivee(dossier, annotationPrivee);
                parapheurService.setAnnotationPublique(dossier, annotation);
                parapheurService.approveV4(dossier, parapheurService.getCurrentParapheur());
                return null;
            }


        }, nodesToLock);

        if (jobService.isBackgroundWorkEnabled()) {
            corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurRef, ParapheurModel.NAME_EN_PREPARATION));
        }
        this.forceShelfUpdate();
        this.annotation = null;
        this.annotationPrivee = null;
        this.dossiers = null;
        this.dossiersModel = null;
        this.browseBean.updateUILocation(parapheurRef);

        return "success";
    }

    @Override
    public String getStepDescription() {
        return "Veuillez selectionner les dossiers à émettre.";
    }


    @Override
    public String getStepTitle() {
        switch (this.currentStep) {
            case 0:
                return "Dossiers à Emettre";
            default:
                return super.getStepTitle();

        }
    }
}
