/*
 * Version 3.4
 * CeCILL Copyright (c) 2008-2012, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.bean.ParapheurBean;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.apache.log4j.Logger;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.faces.event.ActionEvent;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.web.bean.repository.MapNode;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.ui.common.component.data.UIRichList;

/**
 * 
 * @author jmaire
 */
public class ActeursExternesDialog extends BaseDialogBean {
    private static Logger logger = Logger.getLogger(ActeursExternesDialog.class);

    private ParapheurService parapheurService;
    private PersonService personService;
    private AuthenticationComponent authenticationComponent;
    
    private NodeRef currentDossier;
    private Set<NodeRef> acteursNotifies;
    private UIRichList SearchResultsRichList;
    private List<Node> searchResults;
    private UIRichList actorsToAddRichList;
    private List<Node> actorsToAdd;
    private String actorToSearch;
    private Set<NodeRef> notifiesOriginaux;
    

    @Override
    public void init(Map<String, String> parameters) {
        super.init(parameters);
        this.searchResults = new ArrayList<Node>();
        this.actorsToAdd = new ArrayList<Node>();
        this.actorToSearch = "";
        this.currentDossier = this.navigator.getCurrentNode().getNodeRef();
        List<NodeRef> notifies = parapheurService.getListeNotification(currentDossier);
        this.acteursNotifies = new HashSet<NodeRef>(notifies);
        this.notifiesOriginaux = parapheurService.getListeNotificationOriginale(currentDossier);
        for (NodeRef notifie : notifies) {
            Node node = new MapNode(notifie);
            node.getProperties().put("editable", !notifiesOriginaux.contains(notifie));
            actorsToAdd.add(node);
        }
    }
    
    @Override
    protected String finishImpl(FacesContext facesContext, String s) throws Throwable {
        parapheurService.ajouterActeursExternes(getCurrentDossier(), new ArrayList<NodeRef>(acteursNotifies));
        return s;
    }

    @Override
    public boolean getFinishButtonDisabled() {
        return false;
    }
    
    public ParapheurService getParapheurService() {
        return parapheurService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }
    
    public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }
    
    public AuthenticationComponent getAuthenticationComponent() {
        return authenticationComponent;
    }

    public void setAuthenticationComponent(AuthenticationComponent authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }
    
    public NodeRef getCurrentDossier() {
        return currentDossier;
    }
    
    public String getActorToSearch() {
        return this.actorToSearch;
    }
    
    public void setActorToSearch(String actorToSearch) {
        this.actorToSearch = actorToSearch;
    }
    
    public List<Node> getSearchResults() {
        return searchResults;
    }
    
    public List<Node> getActorsToAdd() {
        return actorsToAdd;
    }
    
    public UIRichList getSearchResultsRichList() {
        return this.SearchResultsRichList;
    }
    
    public void setSearchResultsRichList(UIRichList searchResultsRichList) {
        this.SearchResultsRichList = searchResultsRichList;
        if (this.SearchResultsRichList != null) {
            this.SearchResultsRichList.setValue(null);
            if (this.SearchResultsRichList.getInitialSortColumn() == null) {
                // set the initial sort column and direction
                this.SearchResultsRichList.setInitialSortColumn("name");
            }
        }
    }
    
    public UIRichList getActorsToAddRichList() {
        return this.actorsToAddRichList;
    }
    
    public void setActorsToAddRichList(UIRichList actorsToAddRichList) {
        this.actorsToAddRichList = actorsToAddRichList;
        if (this.actorsToAddRichList != null) {
            this.actorsToAddRichList.setValue(null);
            this.actorsToAddRichList.setRefreshOnBind(true);
            if (this.actorsToAddRichList.getInitialSortColumn() == null) {
                // set the initial sort column and direction
                this.actorsToAddRichList.setInitialSortColumn("name");
            }
        }
    }
    
    public void ajouterActeurExterne() {
        NodeRef actor = new NodeRef((String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("dialog:dialog-body:to-add"));
        if (!acteursNotifies.contains(actor)) {
            acteursNotifies.add(actor);
            Node node = new MapNode(actor);
            node.getProperties().put("editable", !notifiesOriginaux.contains(actor));
            actorsToAdd.add(node);
        }
    }
    
    public void enleverActeurExterne() {
        NodeRef actor = new NodeRef((String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("dialog:dialog-body:to-remove"));
        if (!notifiesOriginaux.contains(actor)) {
            acteursNotifies.remove(actor);
            removeActor(actor);
        }
    }
    
    private void removeActor(NodeRef actor) {
        int index = -1;
        for (int i = 0; ((index == -1) && (i < actorsToAdd.size())); i++) {
            if (actorsToAdd.get(i).getNodeRef().equals(actor)) {
                index = i;
            }
        }
        if (index != -1) {
            actorsToAdd.remove(index);
        }
    }
    
    public void search() {
        List<NodeRef> actors = parapheurService.searchParapheurInAssociateList(parapheurService.getParentParapheur(currentDossier), actorToSearch.trim());
        this.searchResults = new ArrayList<Node>();
        if (actors != null) {
            for (NodeRef actor : actors) {
                Node node = new MapNode(actor);
                node.getProperties().put("editable", !notifiesOriginaux.contains(actor));
                this.searchResults.add(node);
            }
        }
    }
    
    public void showAll() {
        List<NodeRef> actors = parapheurService.getDelegationsPossibles(parapheurService.getParentParapheur(currentDossier));
        this.searchResults = new ArrayList<Node>();
        if (actors != null) {
            for (NodeRef actor : actors) {
                Node node = new MapNode(actor);
                node.getProperties().put("editable", !notifiesOriginaux.contains(actor));
                this.searchResults.add(node);
            }
        }
    }
}
