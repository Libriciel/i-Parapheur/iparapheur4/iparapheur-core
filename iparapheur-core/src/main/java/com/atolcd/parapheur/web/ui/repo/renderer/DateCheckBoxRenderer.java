/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.ui.repo.renderer;

import java.io.IOException;
import java.text.DateFormatSymbols;
import java.util.*;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.component.ValueHolder;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.convert.ConverterException;
import javax.faces.model.SelectItem;
import org.alfresco.web.app.Application;
import org.alfresco.web.ui.common.Utils;
import org.alfresco.web.ui.common.renderer.BaseRenderer;

public class DateCheckBoxRenderer extends BaseRenderer {

    private static final String FIELD_YEAR = "_year";
    private static final String FIELD_MONTH = "_month";
    private static final String FIELD_DAY = "_day";
    private static final String FIELD_HOUR = "_hour";
    private static final String FIELD_MINUTE = "_minute";
    private static final String FIELD_NULL = "_null";
    private static final String FIELD_CMD = "_cmd";
    private static final int CMD_SET = 1;
    private static final int CMD_RESET = 2;
    public String disabled = "disabled";

    /**
     * @see javax.faces.render.Renderer#decode(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent)
     *
     * The decode method takes the parameters from the external requests, finds
     * the ones revelant to this component and decodes the results into an
     * object known as the "submitted value".
     */
    @Override
    public void decode(FacesContext context, UIComponent component) {
        try {
            String clientId = component.getClientId(context);
            Map params = context.getExternalContext().getRequestParameterMap();
            boolean nullIfNotChecked = Boolean.parseBoolean((String) params.get(clientId + FIELD_NULL));
            String cmd = (String)params.get(clientId + FIELD_CMD);
            
            if (cmd != null && cmd.length() > 0) { // Clic on checkbox (set date to null or to now)
                if (CMD_RESET == Integer.parseInt(cmd)) {
                    ((EditableValueHolder)component).setSubmittedValue(null);
                    ((EditableValueHolder)component).setValue(null);
                }
                else {
                    int[] parts = new int[5];
                    Calendar date = Calendar.getInstance();
                    parts[0] = date.get(Calendar.YEAR);
                    parts[1] = date.get(Calendar.MONTH);
                    parts[2] = date.get(Calendar.DAY_OF_MONTH);
                    parts[3] = date.get(Calendar.HOUR_OF_DAY);
                    parts[4] = date.get(Calendar.MINUTE);
                    ((EditableValueHolder)component).setSubmittedValue(parts);
                }
            }
            else {
                String year = (String) params.get(clientId + FIELD_YEAR);
                if (year != null) {
                    // found data for our component
                    String month = (String) params.get(clientId + FIELD_MONTH);
                    String day = (String) params.get(clientId + FIELD_DAY);
                    String hour = (String) params.get(clientId + FIELD_HOUR);
                    String minute = (String) params.get(clientId + FIELD_MINUTE);
                    // we encode the values needed for the component as we see fit
                    int[] parts = new int[5];
                    parts[0] = Integer.parseInt(year);
                    parts[1] = Integer.parseInt(month);
                    parts[2] = Integer.parseInt(day);
                    parts[3] = Integer.parseInt(hour);
                    parts[4] = Integer.parseInt(minute);

                    // save the data in an object for our component as the "EditableValueHolder"
                    // all UI Input Components support this interface for the submitted value
                    ((EditableValueHolder) component).setSubmittedValue(parts);
                }
                else if (nullIfNotChecked) {
                    ((EditableValueHolder) component).setSubmittedValue(null);
                    ((EditableValueHolder) component).setValue(null);
                }
            }
        } catch (NumberFormatException nfe) {
            // just ignore the error and skip the update of the property
        }
    }

    /**
     * @see
     * javax.faces.render.Renderer#getConvertedValue(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.Object)
     *
     * In the Process Validations phase, this method is called to convert the
     * values to the datatype as required by the component.
     *
     * It is possible at this point that a custom Converter instance will be
     * used - this is why we have not yet converted the values to a data type.
     */
    @Override
    public Object getConvertedValue(FacesContext context, UIComponent component, Object val) throws ConverterException {
        int[] parts = (int[]) val;
        Calendar date = new GregorianCalendar(parts[0], parts[1], parts[2], parts[3], parts[4]);
        return date.getTime();
    }

    /**
     * @see
     * javax.faces.render.Renderer#encodeBegin(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent)
     *
     * All rendering logic for this component is implemented here. A renderer
     * for an input component must render the submitted value if it's set, and
     * use the local value only if there is no submitted value.
     */
    @Override
    public void encodeBegin(FacesContext context, UIComponent component)
            throws IOException {
        // always check for this flag - as per the spec
        if (component.isRendered() == true) {
            Date date = null;
            // this is part of the spec:
            // first you attempt to build the date from the submitted value
            int[] submittedValue = (int[]) ((EditableValueHolder) component).getSubmittedValue();
            if (submittedValue != null) {
                date = (Date) getConvertedValue(context, component, submittedValue);
            } else {
                // second if no submitted value is found, default to the current value
                Object value = ((ValueHolder) component).getValue();
                // finally check for null value and create default if needed
                date = value instanceof Date ? (Date) value : null;
            }

            // get the attributes from the component we need for rendering
            int nStartYear;
            Integer startYear = (Integer) component.getAttributes().get("startYear");
            if (startYear != null) {
                nStartYear = startYear.intValue();
            } else {
                nStartYear = new GregorianCalendar().get(Calendar.YEAR) + 2;
            }

            int nYearCount = 25;
            Integer yearCount = (Integer) component.getAttributes().get("yearCount");
            if (yearCount != null) {
                nYearCount = yearCount.intValue();
            }

            // now we render the output for our component
            // we create 3 drop-down menus for day, month and year and 
            // two text fields for the hour and minute 
            String clientId = component.getClientId(context);
            ResponseWriter out = context.getResponseWriter();

            // affichage de la checkbox
            out.write("<input type=\"checkbox\" ");
            if (component.getAttributes().get("disabled") != null && (Boolean) component.getAttributes().get("disabled") == true) {
                outputAttribute(out, true, "disabled");
            }
            if (date != null) {
                outputAttribute(out, true, "checked");
            }

            out.write(" onClick=\"");
            out.write(Utils.generateFormSubmit(context,
                    component,
                    clientId + FIELD_CMD,
                    Integer.toString((date == null)? CMD_SET : CMD_RESET)));
            out.write("\" />");

            // note that we build a client id for our form elements that we are then
            // able to decode() as above.
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date == null ? new Date() : date);
            renderMenu(out, component, getDays(), calendar.get(Calendar.DAY_OF_MONTH), clientId + FIELD_DAY, date != null);
            renderMenu(out, component, getMonths(), calendar.get(Calendar.MONTH), clientId + FIELD_MONTH, date != null);
            renderMenu(out, component, getYears(nStartYear, nYearCount), calendar.get(Calendar.YEAR), clientId + FIELD_YEAR, date != null);

            // make sure we have a flag to determine whether to show the time
            Boolean showTime = (Boolean) component.getAttributes().get("showTime");
            if (showTime == null) {
                showTime = Boolean.FALSE;
            }

            out.write("&nbsp;");
            renderTimeElement(out, component, calendar.get(Calendar.HOUR_OF_DAY), clientId + FIELD_HOUR, showTime.booleanValue(), date != null);
            if (showTime.booleanValue()) {
                out.write("&nbsp;:&nbsp;");
            }
            renderTimeElement(out, component, calendar.get(Calendar.MINUTE), clientId + FIELD_MINUTE, showTime.booleanValue(), date != null);
            
            Boolean nullIfNotChecked = (Boolean) component.getAttributes().get("nullIfNotChecked");
            if (nullIfNotChecked == null) {
                nullIfNotChecked = Boolean.FALSE;
            }
            out.write("<input type=\"hidden\" ");
            outputAttribute(out, clientId + FIELD_NULL, "name");
            outputAttribute(out, Boolean.toString(nullIfNotChecked), "value");
            out.write(" />");
        }
    }

    /**
     * Render a drop-down menu to represent an element for the date picker.
     *
     * @param out Response Writer to output too
     * @param component The compatible component
     * @param items To display in the drop-down list
     * @param selected Which item index is selected
     * @param clientId Client Id to use
     *
     * @throws IOException
     */
    private void renderMenu(ResponseWriter out, UIComponent component, List items,
            int selected, String clientId, boolean hasValue)
            throws IOException {
        out.write("<select");
        outputAttribute(out, clientId, "name");

        if (component.getAttributes().get("styleClass") != null) {
            outputAttribute(out, component.getAttributes().get("styleClass"), "class");
        }
        if (component.getAttributes().get("style") != null) {
            outputAttribute(out, component.getAttributes().get("style"), "style");
        }
        if ((component.getAttributes().get("disabled") != null && (Boolean) component.getAttributes().get("disabled") == true) || !hasValue) {
            outputAttribute(out, true, "disabled");
        }
        out.write(">");

        for (Iterator i = items.iterator(); i.hasNext(); /**/) {
            SelectItem item = (SelectItem) i.next();
            Integer value = (Integer) item.getValue();
            out.write("<option");
            outputAttribute(out, value, "value");

            // show selected value
            if (value.intValue() == selected) {
                outputAttribute(out, "selected", "selected");
            }
            out.write(">");
            out.write(item.getLabel());
            out.write("</option>");
        }
        out.write("</select>");
    }

    /**
     * Renders either the hour or minute field
     *
     * @param out The ResponseWriter
     * @param currentValue The value of the hour or minute
     * @param clientId The id to use for the field
     */
    private void renderTimeElement(ResponseWriter out, UIComponent component,
            int currentValue, String clientId, boolean showTime, boolean hasValue) throws IOException {
        out.write("<input");
        outputAttribute(out, clientId, "name");

        if (showTime) {
            out.write(" type='text' size='1' maxlength='2'");

            if ((component.getAttributes().get("disabled") != null && (Boolean) component.getAttributes().get("disabled") == true) || !hasValue) {
                outputAttribute(out, true, "disabled");
            }
        } else {
            out.write(" type='hidden'");
        }

        // make sure there are always 2 digits
        String strValue = Integer.toString(currentValue);
        if (strValue.length() == 1) {
            strValue = "0" + strValue;
        }

        outputAttribute(out, strValue, "value");
        out.write("/>");
    }

    private List getYears(int startYear, int yearCount) {
        List<SelectItem> years = new ArrayList<SelectItem>();
        for (int i = startYear; i > startYear - yearCount; i--) {
            Integer year = i;
            years.add(new SelectItem(year, year.toString()));
        }
        return years;
    }

    private List getMonths() {
        // get names of the months for default locale
        Locale locale = Application.getLanguage(FacesContext.getCurrentInstance());
        if (locale == null) {
            locale = Locale.getDefault();
        }
        DateFormatSymbols dfs = new DateFormatSymbols(locale);
        String[] names = dfs.getMonths();
        List<SelectItem> months = new ArrayList<SelectItem>(12);
        for (int i = 0; i < 12; i++) {
            Integer key = i;
            months.add(new SelectItem(key, names[i]));
        }
        return months;
    }

    private List getDays() {
        List<SelectItem> days = new ArrayList<SelectItem>(31);
        for (int i = 1; i < 32; i++) {
            Integer day = i;
            days.add(new SelectItem(day, day.toString()));
        }
        return days;
    }
}
