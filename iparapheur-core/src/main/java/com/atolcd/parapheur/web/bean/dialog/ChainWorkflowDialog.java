/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.web.bean.ParapheurBean;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.ui.common.SortableSelectItem;

import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author viv
 */
public class ChainWorkflowDialog extends BaseDialogBean {

    private ParapheurService parapheurService;

    private TypesService typesService;

    private WorkflowService workflowService;

    private ParapheurBean parapheurBean;

    private List<SortableSelectItem> types;

    private List<SortableSelectItem> subtypes;

    private List<SortableSelectItem> workflows;

    private String type;

    private String subtype;

    private String workflow;

    @Override
    public void init(Map<String, String> parameters) {
        super.init(parameters);

        // Managed bean are session-scoped, all properties need to be nullified
        types = null;
        subtypes = null;
        workflows = null;
        type = null;
        subtype = null;
        workflow = null;
    }

    public void loadSubtypes() {
        subtype = null;
        subtypes = null;
        workflow = null;
        workflows = null;
    }

    public void loadWorkflows() {
        workflow = null;
        workflows = null;
    }

    @Override
    protected String finishImpl(FacesContext fc, String outcome) throws Throwable {
        NodeRef dossier = navigator.getCurrentNode().getNodeRef();
        NodeRef parapheur = parapheurService.getParentParapheur(dossier);

        List<EtapeCircuit> newCircuit;
        if ("hier".equals(workflow)) {
            newCircuit = parapheurService.getCircuitHierarchique(parapheur);
        } else if ("subtype".equals(workflow)) {
            //newCircuit = workflowService.getSavedWorkflow(parapheurService.getCircuitRef(type, subtype), true, parapheur).getCircuit();
            newCircuit = workflowService.getSavedWorkflow(typesService.getWorkflow(type, subtype), true, parapheur, dossier).getCircuit();
        } else {
            newCircuit = workflowService.getSavedWorkflow(new NodeRef(workflow), true, parapheur, dossier).getCircuit();
        }

        parapheurService.appendCircuit(dossier, newCircuit);

        parapheurService.restartCircuit(dossier, type, subtype, workflow);

        return outcome;
    }

    public List<SortableSelectItem> getTypes() {
        if (types == null) {
            List<String> sTypes = parapheurService.getSavedTypes(parapheurBean.getParapheurCourant());
            types = new ArrayList<SortableSelectItem>(sTypes.size());

            for (String sType : sTypes) {
                SortableSelectItem item = new SortableSelectItem(sType, sType, sType);
                types.add(item);
            }

            Collections.sort(types);
        }

        return types;
    }

    public List<SortableSelectItem> getSubtypes() {
        if (subtypes == null) {
            if (type != null) {
                List<String> ssTypes = parapheurService.getSavedSousTypes(type, parapheurBean.getParapheurCourant());
                subtypes = new ArrayList<SortableSelectItem>(ssTypes.size());

                for (String ssType : ssTypes) {
                    SortableSelectItem item = new SortableSelectItem(ssType, ssType, ssType);
                    subtypes.add(item);
                }

                Collections.sort(subtypes);
            }
        }

        return subtypes;
    }

    public List<SortableSelectItem> getWorkflows() {
        if (workflows == null) {
            if (type != null && subtype != null) {

                NodeRef parapheurCourant = parapheurBean.getParapheurCourant();

                String currentUser = AuthenticationUtil.getRunAsUser();
                if (!parapheurService.isParapheurOwnerOrDelegate(parapheurCourant, currentUser) && !parapheurService.isParapheurSecretaire(parapheurCourant, currentUser)) {
                    throw new RuntimeException("L'utilisateur n'est pas un acteur courant de ce dossier.");
                }
                List<SavedWorkflow> savedWorkflows = workflowService.getWorkflows(currentUser);


                workflows = new ArrayList<SortableSelectItem>(savedWorkflows.size() + 2);

                for (SavedWorkflow savedWorkflow : savedWorkflows) {
                    workflows.add(new SortableSelectItem(savedWorkflow.getNodeRef().toString(), savedWorkflow.getName(), savedWorkflow.getName()));
                }

                /*
                for (Map.Entry<String, NodeRef> mWorkflow : mWorkflows.entrySet()) {
                    workflows.add(new SortableSelectItem(mWorkflow.getValue().toString(), mWorkflow.getKey(), mWorkflow.getKey()));
                }
                */

                Collections.sort(workflows);

                if (typesService.isCircuitHierarchiqueVisible(type, subtype)) {
                    workflows.add(0, new SortableSelectItem("hier", "-- Circuit hiérarchique --", "-- Circuit hiérarchique --"));
                }

                workflows.add(0, new SortableSelectItem("subtype", "-- Circuit " + type + "/" + subtype + " --", "-- Circuit " + type + "/" + subtype + " --"));
            }
        }
        
        return workflows;
    }

    public String getSubtype() {
        if (subtype == null) {
            subtype = (String) getSubtypes().get(0).getValue();
        }

        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getType() {
        if (type == null) {
            type = (String) getTypes().get(0).getValue();
        }

        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWorkflow() {
        if (workflow == null) {
            workflow = (String) getWorkflows().get(0).getValue();
        }
        
        return workflow;
    }

    public void setWorkflow(String workflow) {
        this.workflow = workflow;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setTypesService(TypesService typesService) {
        this.typesService = typesService;
    }

    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    public void setParapheurBean(ParapheurBean parapheurBean) {
        this.parapheurBean = parapheurBean;
    }

}
