package com.atolcd.parapheur.web.action.evaluator;


import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.repository.Node;

public class SecureMailInfosEvalutor extends BlindedActionEvaluator {
    @Override
    protected boolean evaluateImpl(Node node, NodeRef nodeRef) {
        boolean result = false;
        String currentStep = parapheurService.getCurrentEtapeCircuit(nodeRef).getActionDemandee();

        if (EtapeCircuitImpl.ETAPE_MAILSEC.equals(currentStep)) {
           result = nodeService.getProperty(nodeRef, ParapheurModel.PROP_MAILSEC_MAIL_ID) != null;
        }
        return result;
    }
}
