/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.action.evaluator;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;
import fr.bl.iparapheur.srci.SrciService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.servlet.FacesHelper;
import org.alfresco.web.bean.repository.Node;

import javax.faces.context.FacesContext;


public class EnvoiS2lowEvaluator extends BlindedActionEvaluator
{

    @Override
    protected boolean evaluateImpl(Node node, NodeRef nodeRef) {
        boolean res = false;
        FacesContext context = FacesContext.getCurrentInstance();
        S2lowService s2lowService = (S2lowService)FacesHelper.getManagedBean(context, "S2lowService");
        String username = Application.getCurrentUser(FacesContext.getCurrentInstance()).getUserName();
        if (s2lowService != null && s2lowService.isEnabled())  {
            ParapheurService parapheurService = (ParapheurService)FacesHelper.getManagedBean(context, "ParapheurService");
            NodeService nodeService = (NodeService)FacesHelper.getManagedBean(context, "NodeService");

            NodeRef ref = node.getNodeRef();

            if (!nodeService.exists(ref) || !parapheurService.isDossier(ref)) {
                return false;
            }

            NodeRef corbeille = parapheurService.getParentCorbeille(ref);
            NodeRef parapheur = parapheurService.getParentParapheur(corbeille);

            String tdt = (String) nodeService.getProperty(ref, ParapheurModel.PROP_TDT_NOM);

            boolean isInTDT = (tdt != null && !(tdt.equals("pas de TdT")));

            boolean hasAspectTdtTransaction=
                    nodeService.hasAspect(ref, ParapheurModel.ASPECT_S2LOW)
                    || nodeService.hasAspect(ref, SrciService.K.aspect_srciTransaction);

            if (parapheurService.isTermine(ref)
                // BLEX && !nodeService.hasAspect(ref, ParapheurModel.ASPECT_S2LOW)
                && (!hasAspectTdtTransaction) // BLEX
                && ParapheurModel.NAME_A_ARCHIVER.equals(nodeService.getPrimaryParent(corbeille).getQName())
                && isInTDT
                && parapheurService.isParapheurOwnerOrDelegate(parapheur, username)) {
                res = true;
            }
        }

        return res;
    }

}
