/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atolcd.parapheur.web.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;
import org.adullact.iparapheur.domain.CertificatesDAO;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.security.authentication.AuthenticationException;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.servlet.AbstractAuthenticationFilter;
import org.alfresco.web.app.servlet.AuthenticationHelper;
import org.alfresco.web.bean.LoginBean;
import org.alfresco.web.bean.LoginOutcomeBean;
import org.alfresco.web.bean.repository.User;
import org.alfresco.web.config.LanguagesConfigElement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;
import org.springframework.extensions.config.ConfigService;
import org.springframework.extensions.surf.util.I18NUtil;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author viv
 */
public class CASAuthenticationFilter extends AbstractAuthenticationFilter implements Filter {

    private static Log logger = LogFactory.getLog(CASAuthenticationFilter.class);

    private ServletContext context = null;

    private static final String LOCALE = "locale";

    public static final String MESSAGE_BUNDLE = "alfresco.messages.webclient";

    private AuthenticationComponent authComponent;

    private AuthenticationService authService;

    private TransactionService transactionService;

    private PersonService personService;

    private NodeService nodeService;

    private SearchService searchService;

    private List<String> m_languages;

    private CertificatesDAO certificatesDAO;

    @Override
    public void init(FilterConfig config) throws ServletException {
        this.context = config.getServletContext();
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
        ServiceRegistry serviceRegistry = (ServiceRegistry) ctx.getBean(ServiceRegistry.SERVICE_REGISTRY);
        transactionService = serviceRegistry.getTransactionService();
        nodeService = serviceRegistry.getNodeService();

        authComponent = (AuthenticationComponent) ctx.getBean("authenticationComponent");
        authService = (AuthenticationService) ctx.getBean("authenticationService");
        personService = (PersonService) ctx.getBean("personService");
        searchService = (SearchService) ctx.getBean("searchService");
        certificatesDAO = (CertificatesDAO) ctx.getBean("certificatesDAO");

        // Get a list of the available locales
        ConfigService configServiceService = (ConfigService) ctx.getBean("webClientConfigService");
        LanguagesConfigElement configElement = (LanguagesConfigElement) configServiceService.getConfig("Languages").getConfigElement(LanguagesConfigElement.CONFIG_ELEMENT_ID);

        m_languages = configElement.getLanguages();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String casUsername = getCasUsername(req);
        String alfrescoUsername = getAlfrescoUsername(req);

        if (casUsername != null) {
            setAuthenticatedUser(req, req.getSession(true), casUsername);
            if (casUsername.equals(alfrescoUsername)) {
                chain.doFilter(request, response);
            } else {
                Map<String, String[]> parameters = req.getParameterMap();
                /* If the LoginOutcomeBean.PARAM_REDIRECT_URL is present in the queryString then do the redirect otherwise redirect to parapheurs.jsp.*/
                if (parameters.containsKey(LoginOutcomeBean.PARAM_REDIRECT_URL)) {
                    res.sendRedirect(parameters.get(LoginOutcomeBean.PARAM_REDIRECT_URL)[0]);
                }
                // il se peut que l'on ne soit pas redirigé sur le loginbean et que l'on ne soit pas redirigé
                // on laisse tomber les redirection de tout ce qui est dans faces mais à voir.
                else if (!req.getRequestURI().startsWith("/alfresco/faces")) {
                    res.sendRedirect(req.getRequestURI());
                }
                else {
                    res.sendRedirect(req.getContextPath() + "/faces/jsp/parapheur/parapheurs.jsp");
                }
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    private String getCasUsername(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        Assertion assertion;
        if (session != null) {
            assertion = (Assertion) session.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION);
        } else {
            assertion = (Assertion) req.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION);
        }

        if (assertion == null) {
            return null;
        }

        AttributePrincipal principal = assertion.getPrincipal();
        if (principal == null) {
            return null;
        }

        return principal.getName();
    }

    private String getAlfrescoUsername(HttpServletRequest req) {
        HttpSession httpSess = req.getSession(true);
        User user = (User) httpSess.getAttribute(AuthenticationHelper.AUTHENTICATION_USER);

        if (user == null) {
            return null;
        }

        return user.getUserName();
    }

    @Override
    public void destroy() {
        this.context = null;
    }

    /**
     * Set the authenticated user.
     *
     * It does not check that the user exists at the moment.
     *
     * @param req
     * @param httpSess
     * @param userName
     */
    private void setAuthenticatedUser(HttpServletRequest req, HttpSession httpSess, String userName) {
        // Set up the user information
        UserTransaction tx = transactionService.getUserTransaction();
        NodeRef homeSpaceRef = null;
        User user;

        try {
            tx.begin();
            // Set the authentication
            authComponent.setCurrentUser(userName);
            user = new User(userName, authService.getCurrentTicket(), personService.getPerson(userName));

            homeSpaceRef = (NodeRef) nodeService.getProperty(personService.getPerson(userName),
                    ContentModel.PROP_HOMEFOLDER);
            user.setHomeSpaceId(homeSpaceRef.getId());
            tx.commit();
        } catch (Throwable ex) {
            try {
                tx.rollback();
            } catch (Exception ex2) {
                logger.error("Failed to rollback transaction", ex2);
            }

            if (ex instanceof AuthenticationException) {
                logger.error("Impossible d'authentifier l'utilisateur : " + userName);
                throw new RuntimeException("Utilisateur inconnu");
            } else if (ex instanceof RuntimeException) {
                logger.error(ex);
                throw (RuntimeException) ex;
            } else {
                logger.error(ex);
                throw new RuntimeException("Failed to set authenticated user", ex);
            }
        }

        // Store the user
        httpSess.setAttribute(AuthenticationHelper.AUTHENTICATION_USER, user);
        httpSess.setAttribute(LoginBean.LOGIN_EXTERNAL_AUTH, Boolean.TRUE);

        // Set the current locale from the Accept-Lanaguage header if available
        Locale userLocale = parseAcceptLanguageHeader(req, m_languages);

        if (userLocale != null) {
            httpSess.setAttribute(LOCALE, userLocale);
            httpSess.removeAttribute(MESSAGE_BUNDLE);
        }

        // Set the locale using the session
        I18NUtil.setLocale(Application.getLanguage(httpSess));
    }

}
