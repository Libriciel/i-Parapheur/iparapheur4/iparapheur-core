/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.action.evaluator;


import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import fr.bl.iparapheur.srci.SrciService;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.app.Application;
import org.alfresco.web.bean.repository.Node;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * BLEX (comment)
 * Contrairement a ce que son nom indique, ce "validateur" est utilise
 * pour calculer la disponibilité de diverses option : "Emettre", "Signer," "envoyer TdT"...
 */
public /*final*/ class SignEvaluator extends BlindedActionEvaluator {

    @Override
    public boolean evaluateImpl(Node node, NodeRef ref) {

        boolean res = false;
        List<EtapeCircuit> circuit = null;
        if (node.containsPropertyResolver("circuit")) {
            circuit = (List<EtapeCircuit>)node.getProperties().get("circuit");
        } else {
            // cas d'un node créé par Alfresco et non par le code i-parapheur: le nodePropertyResolver n'existe pas
            circuit = parapheurService.getCircuit(ref);
        }

        boolean isASecureMailStep = false;
        String currentStep = parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee();

        if (!isOwner(AuthenticationUtil.getRunAsUser(), ref) && !isSecretaire(AuthenticationUtil.getRunAsUser(), ref)) {
            return false;
        }

        // BLEX-SRCI if (nodeService.hasAspect(ref, ParapheurModel.ASPECT_S2LOW) &&
        boolean hasAspectTdtTransaction=
                nodeService.hasAspect(ref, ParapheurModel.ASPECT_S2LOW)
                || nodeService.hasAspect(ref, SrciService.K.aspect_srciTransaction);
        if (hasAspectTdtTransaction &&
                currentStep.equals(EtapeCircuit.ETAPE_TDT)) {
            return false;
        }

        if (EtapeCircuit.ETAPE_MAILSEC.equals(currentStep)) {
            return false;
        }

        String username = Application.getCurrentUser(FacesContext.getCurrentInstance()).getUserName();
        NodeRef parapheur = parapheurService.getParentParapheur(ref);

        Boolean signPapier = (Boolean) nodeService.getProperty(ref, ParapheurModel.PROP_SIGNATURE_PAPIER);
        boolean derniereEtape = false;
        if (!circuit.isEmpty()) {
            if (!parapheurService.isTermine(ref)) {
                if (circuit.get(circuit.size() - 2).isApproved()) {
                    derniereEtape = true;
                }
            }
        }
        // Dans le cas d'une dernière étape de signature papier, on passe sur une action spécialisée
        if (!signPapier || !derniereEtape) {
            if (parapheurService.isParapheurOwnerOrDelegate(parapheur, username)) {
                if (!nodeService.hasAspect(node.getNodeRef(), ParapheurModel.ASPECT_SECRETARIAT)) {
                    if (!parapheurService.isTermine(ref)) {
                        res = true;
                    }
                }
            }
        }

        // Ne pas afficher le bouton "signer" en HTTP
        if (parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee().equals(EtapeCircuit.ETAPE_SIGNATURE)) {
            boolean isInSecureMode = isCurrentRequestSecure();
                    /*false;
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

            if (request != null) {
                isInSecureMode = request.isSecure();
            }
            */
            res = res && isInSecureMode;
        }

        return res;
    }

}
