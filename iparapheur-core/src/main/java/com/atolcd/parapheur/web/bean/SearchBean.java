/*
 * Version 3.1
 * CeCILL Copyright (c) 2010 ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-Projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.web.bean;

import com.atolcd.parapheur.web.ui.repo.component.UISimpleSearch;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.search.LimitBy;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.web.app.Application;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.NavigationBean;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.bean.search.SearchContext;

/**
 * Nouvelle classe de recherche avancee.
 *
 * @author Vivien Barousse, Stephane Vast
 */
public class SearchBean {

    private NavigationBean aligator;

    private SearchResultsBean resultsBean;

    private String query;

    public void search(ActionEvent evt) {
        UISimpleSearch search = (UISimpleSearch) evt.getComponent();
        SearchContext searchContext = search.getSearchContext();

        aligator.setSearchContext(searchContext);
        searchContext.addSimpleAttributeQuery(ContentModel.PROP_TITLE);
        String searchQuery = searchContext.buildQuery(BrowseBean.getMinimumSearchLength());
        SearchParameters sp = new SearchParameters();
        sp.setLanguage(SearchService.LANGUAGE_LUCENE);
        sp.setQuery(searchQuery);
        sp.addStore(Repository.getStoreRef());

        int searchLimit = Application.getClientConfig(FacesContext.getCurrentInstance()).getSearchMaxResults();
        if (searchLimit > 0) {
            sp.setLimitBy(LimitBy.FINAL_SIZE);
            sp.setLimit(searchLimit);
        }

        resultsBean.setSearchParameters(sp);
        if (searchQuery != null) {
            resultsBean.setQuerySize(searchQuery.length());
        }
        else {
            resultsBean.setQuerySize(0);
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.getApplication().getNavigationHandler().handleNavigation(fc, null, "search");
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setAligator(NavigationBean aligator) {
        this.aligator = aligator;
    }

    public void setResultsBean(SearchResultsBean resultsBean) {
        this.resultsBean = resultsBean;
    }

}
