package com.atolcd.parapheur.web.bean.wizard.batch;

public class Predicate<K, V> {
    K key;
    V value;

    public Predicate(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K first() {
        return key;
    }

    public V second() {
        return value;
    }
}