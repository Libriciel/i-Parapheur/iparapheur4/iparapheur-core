/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.atolcd.parapheur.web.ui.common.converter;

import java.util.Collection;
import java.util.Iterator;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import org.alfresco.web.bean.repository.Node;

/**
 * This converter transforms a parapheur node list into a comma-separated string.
 *
 * @author Vivien Barousse
 */
public class ParapheurNodeListConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) throws ConverterException {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) throws ConverterException {
        if (!(o instanceof Collection)) {
            throw new UnsupportedOperationException("Not supported for type " + o.getClass().getName() + ".");
        }

        Collection<Node> nodes = (Collection<Node>) o;
        Iterator<Node> nodesIt = nodes.iterator();
        boolean first = true;

        StringBuilder builder = new StringBuilder();

        while (nodesIt.hasNext()) {
            Node node = nodesIt.next();

            if (!first) {
                builder.append(", ");
            }
            first = false;

            builder.append(node.getProperties().get("title"));
        }

        return builder.toString();
    }

}
