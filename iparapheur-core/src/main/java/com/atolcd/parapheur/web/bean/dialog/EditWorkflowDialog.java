/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.web.bean.ParapheurBean;
import org.adullact.iparapheur.rules.bean.CustomProperty;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.ui.common.SortableSelectItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.*;

public class EditWorkflowDialog extends BaseDialogBean {

    private static Log logger = LogFactory.getLog(EditWorkflowDialog.class);

    private ContentService contentService;

    /** The ParapheurService bean reference */
    private ParapheurService parapheurService;

    private TypesService typesService;

    private WorkflowService workflowService;

    private MetadataService metadataService;

    private ParapheurBean parapheurBean;

    private String typeMetier;

    private List<SelectItem> typesMetier;

    private String sousTypeMetier;

    private List<SelectItem> sousTypesMetier;

    private String circuit;

    private List<SortableSelectItem> circuits;

    private String subtypeWorkflowNoderef;

    @Override
    public void init(Map<String, String> parameters) {
        typeMetier = null;
        typesMetier = null;
        sousTypeMetier = null;
        sousTypesMetier = null;
        circuit = null;
        circuits = null;
        subtypeWorkflowNoderef = null;
    }

    public void loadSousTypes() {
        sousTypeMetier = null;
        sousTypesMetier = null;
        circuit = null;
        circuits = null;
        subtypeWorkflowNoderef = null;
    }

    public void loadCircuits() {
        circuit = null;
        circuits = null;
        subtypeWorkflowNoderef = null;
    }

    @Override
    protected String finishImpl(FacesContext fc, String string) throws Exception {
        NodeRef dossier = navigator.getCurrentNode().getNodeRef();
        NodeRef document = parapheurService.getDocuments(dossier).get(0);
        NodeRef parapheur = parapheurService.getParentParapheur(dossier);

        if (getNodeService().hasAspect(dossier, ParapheurModel.ASPECT_TYPAGE_METIER)) {
            getNodeService().removeAspect(dossier, ParapheurModel.ASPECT_TYPAGE_METIER);
        }
        // Format de signature par defaut : PKCS#7
        getNodeService().setProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT, "PKCS#7/single");

        List<CustomProperty<?>> scriptCustomProperties = new ArrayList<CustomProperty<?>>();
        Map<QName, Serializable> customProperties = new HashMap<QName, Serializable>();
        if (getNamespaceService().getPrefixes().contains("cu") &&
                    getNodeService().hasAspect(dossier, QName.createQName("cu:customMetadata", getNamespaceService()))) {
            for (CustomMetadataDef def : metadataService.getMetadataDefs()) {
                Object value = getNodeService().getProperty(dossier, def.getName());
                customProperties.put(def.getName(), (Serializable) value);
                scriptCustomProperties.add(new CustomProperty<Object>(def.getName().getLocalName(), value));
            }
        }

        boolean s2lowActes = false;
        boolean isDigitalSignatureMandatory = false;
        boolean readingMandatory = true;
        boolean includeAttachments = true;
        if ("hier".equals(circuit)) {
            parapheurService.setCircuit(dossier, parapheurService.getCircuitHierarchique(parapheur));
        } else if ("subtype".equals(circuit)) {
            parapheurService.setCircuit(dossier, workflowService.getSavedWorkflow(                                // parapheurService.loadSavedWorkflow(
                    typesService.getWorkflow(dossier, typeMetier, sousTypeMetier, scriptCustomProperties)).getCircuit());  // parapheurService.getCircuitRef(typeMetier, sousTypeMetier)).getCircuit());
        } else {
            NodeRef circuitRef = new NodeRef(circuit);
            parapheurService.setCircuit(dossier,
                    workflowService.getSavedWorkflow(circuitRef).getCircuit());           // parapheurService.loadSavedWorkflow(circuitRef).getCircuit());
        }

        Map<QName, Serializable> typageProps = parapheurService.getTypeMetierProperties(typeMetier);
        getNodeService().addAspect(dossier, ParapheurModel.ASPECT_TYPAGE_METIER, typageProps);
        if ("S²LOW".equals(typageProps.get(ParapheurModel.PROP_TDT_NOM)) &&
                "ACTES".equals(typageProps.get(ParapheurModel.PROP_TDT_PROTOCOLE))) {
            s2lowActes = true;
        }
        isDigitalSignatureMandatory = typesService.isDigitalSignatureMandatory(typeMetier, sousTypeMetier);
        readingMandatory = typesService.isReadingMandatory(typeMetier, sousTypeMetier);
        includeAttachments = typesService.areAttachmentsIncluded(typeMetier, sousTypeMetier);

        getNodeService().setProperty(dossier, ParapheurModel.PROP_TYPE_METIER, typeMetier);
        getNodeService().setProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER, sousTypeMetier);
        getNodeService().setProperty(dossier, ParapheurModel.PROP_WORKFLOW, circuit);
        getNodeService().setProperty(dossier, ParapheurModel.PROP_DIGITAL_SIGNATURE_MANDATORY, isDigitalSignatureMandatory);
        getNodeService().setProperty(dossier, ParapheurModel.PROP_READING_MANDATORY, readingMandatory);
        getNodeService().setProperty(dossier, ParapheurModel.PROP_INCLUDE_ATTACHMENTS, includeAttachments);

        if (s2lowActes) {
            ContentReader reader = contentService.getReader(document, ContentModel.PROP_CONTENT);
            ContentWriter writer = contentService.getWriter(document, ContentModel.PROP_CONTENT, true);

            String filename = (String) getNodeService().getProperty(document, ContentModel.PROP_NAME);

            writer.setMimetype(MimetypeMap.MIMETYPE_PDF);
            writer.setEncoding(reader.getEncoding());

            contentService.transform(reader, writer);

            if (!filename.endsWith(".pdf")) {
                filename += ".pdf";
                getNodeService().setProperty(document, ContentModel.PROP_NAME, filename);
            }
        }

        return string;
    }

    public List<SelectItem> getTypesMetier() {
        if (typesMetier == null) {
            typesMetier = new ArrayList<SelectItem>();
            List<String> savedTypes = parapheurService.getSavedTypes(parapheurBean.getParapheurCourant());
            for (String savedType : savedTypes) {
                typesMetier.add(new SelectItem(savedType, savedType));
            }
        }
        return typesMetier;
    }

    public List<SelectItem> getSousTypesMetier() {
        if (sousTypesMetier == null
                && getTypeMetier() != null) {
            sousTypesMetier = new ArrayList<SelectItem>();
            List<String> savedSousTypes = parapheurService.getSavedSousTypes(typeMetier, parapheurBean.getParapheurCourant());
            for (String savedSousType : savedSousTypes) {
                sousTypesMetier.add(new SelectItem(savedSousType, savedSousType));
            }
        }

        return sousTypesMetier;
    }

    public List<SortableSelectItem> getCircuits() {
        if (circuits == null
                && getTypeMetier() != null
                && getSousTypeMetier() != null) {
            NodeRef dossier = navigator.getCurrentNode().getNodeRef();
            circuits = new ArrayList<SortableSelectItem>();

            // Workflow for selected type / subtype
            List<CustomProperty<?>> customProperties = new ArrayList<CustomProperty<?>>();
            if (getNamespaceService().getPrefixes().contains("cu") &&
                    getNodeService().hasAspect(dossier, QName.createQName("cu:customMetadata", getNamespaceService()))) {
                for (CustomMetadataDef def : metadataService.getMetadataDefs()) {
                    Object value = getNodeService().getProperty(dossier, def.getName());
                    customProperties.add(new CustomProperty<Object>(def.getName().getLocalName(), value));
                }
            }
            NodeRef zeRef = typesService.getWorkflow(dossier, this.typeMetier, this.sousTypeMetier, customProperties);
            if (zeRef != null) {
                subtypeWorkflowNoderef = zeRef.toString();
                circuits.add(new SortableSelectItem("subtype", "-- Circuit " + this.typeMetier + " // " + this.sousTypeMetier + " --", ""));
            }

            if (typesService.isCircuitHierarchiqueVisible(typeMetier, sousTypeMetier)) {
                circuits.add(new SortableSelectItem("hier", "-- Circuit hiérarchique --", ""));
            }
            NodeRef parapheurCourant = parapheurBean.getParapheurCourant();
            List<String> owners = parapheurService.getParapheurOwners(parapheurCourant);
            String currentUser = AuthenticationUtil.getRunAsUser();

            if (owners != null && !owners.contains(currentUser)) {
                throw new RuntimeException("L'utilisateur courant n'est pas propriétaire de ce parapheur.");
            }
            List<SavedWorkflow> savedWorkflows = workflowService.getWorkflows(currentUser);

            List<SortableSelectItem> savedworkflowsItems = new ArrayList<SortableSelectItem>();
            for (SavedWorkflow lecircuit : savedWorkflows) {
                savedworkflowsItems.add(new SortableSelectItem(lecircuit.getNodeRef().toString(), lecircuit.getName(), lecircuit.getName()));
            }
            Collections.sort(savedworkflowsItems);
            circuits.addAll(savedworkflowsItems);
        }
        
        return circuits;
    }

    public String getCircuit() {
        if (circuit == null) {
            NodeRef dossier = navigator.getCurrentNode().getNodeRef();
            if (getNodeService().getProperty(dossier, ParapheurModel.PROP_TYPE_METIER).equals(typeMetier)
                    && getNodeService().getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER).equals(sousTypeMetier)) {
                circuit = (String) getNodeService().getProperty(dossier, ParapheurModel.PROP_WORKFLOW);
            } else {
                circuit = (String) getCircuits().get(0).getValue();
            }
        }

        return circuit;
    }

    public void setCircuit(String circuit) {
        this.circuit = circuit;
    }

    public String getSousTypeMetier() {
        if (sousTypeMetier == null) {
            NodeRef dossier = navigator.getCurrentNode().getNodeRef();
            if (getNodeService().getProperty(dossier, ParapheurModel.PROP_TYPE_METIER).equals(typeMetier)) {
                sousTypeMetier = (String) getNodeService().getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER);
            } else {
                sousTypeMetier = (String) getSousTypesMetier().get(0).getValue();
            }
        }

        return sousTypeMetier;
    }

    public void setSousTypeMetier(String sousTypeMetier) {
        this.sousTypeMetier = sousTypeMetier;
    }

    public String getTypeMetier() {
        if (typeMetier == null) {
            NodeRef dossier = navigator.getCurrentNode().getNodeRef();
            typeMetier = (String) getNodeService().getProperty(dossier, ParapheurModel.PROP_TYPE_METIER);
        }

        return typeMetier;
    }

    public void setTypeMetier(String typeMetier) {
        this.typeMetier = typeMetier;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setTypesService(TypesService typesService) {
        this.typesService = typesService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    public void setParapheurBean(ParapheurBean parapheurBean) {
        this.parapheurBean = parapheurBean;
    }

    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }
    
}
