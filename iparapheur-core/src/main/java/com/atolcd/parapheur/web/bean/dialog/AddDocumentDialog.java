/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.repo.ParapheurService;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.MLText;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.app.context.UIContextService;
import org.alfresco.web.bean.FileUploadBean;
import org.alfresco.web.bean.content.BaseContentWizard;
import org.alfresco.web.bean.repository.Repository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Bean implementation for adding a document to a "dossier"
 * Modified by bma, for the "parapheur" module
 * 
 * @author gavinc
 */

public class AddDocumentDialog extends BaseContentWizard
{
   private static Log logger = LogFactory.getLog(AddDocumentDialog.class);
   protected File file;
    private ParapheurService parapheurService;
   
    public ParapheurService getParapheurService() {
        return parapheurService;
    }
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

   // ------------------------------------------------------------------------------
   // Dialog implementation
   
   @Override
   protected String finishImpl(FacesContext context, String outcome)
         throws Exception
   {
      // Try and extract metadata from the file
      ContentReader cr = new FileContentReader(this.file);
      cr.setMimetype(this.mimeType);
      // create properties for content type
      Map<QName, Serializable> contentProps = new HashMap<QName, Serializable>(5, 1.0f);
      
      if (Repository.extractMetadata(FacesContext.getCurrentInstance(), cr, contentProps))
      {
         this.author = (String)(contentProps.get(ContentModel.PROP_AUTHOR));
         if (contentProps.get(ContentModel.PROP_TITLE) instanceof MLText) {
             this.title = ((MLText)contentProps.get(ContentModel.PROP_TITLE)).getDefaultValue();
         } else {
             this.title = (String)(contentProps.get(ContentModel.PROP_TITLE));
         }
         if (contentProps.get(ContentModel.PROP_DESCRIPTION) instanceof MLText) {
             this.description = ((MLText)contentProps.get(ContentModel.PROP_DESCRIPTION)).getDefaultValue();
         } else {
             this.description = (String)(contentProps.get(ContentModel.PROP_DESCRIPTION));
         }
      }
      
      // default the title to the file name if not set
      if (this.title == null)
      {
         this.title = this.fileName;
      }
         
      saveContent(this.file, null);
      
      // return default outcome
      return outcome;
   }
   
   @Override
   public void init(Map<String, String> parameters)
   {
      super.init(parameters);
      this.otherPropertiesChoiceVisible = false;
      clearUpload();

      this.objectType = "cm:content";
   }
   
   @Override
   protected String doPostCommitProcessing(FacesContext context, String outcome)
   {
      // just return
      return outcome;
   }

   @Override
   protected String getDefaultFinishOutcome()
   {
      // as we are using this dialog outside the dialog framework 
      // just go back to the main page
	   
	   UIContextService.getInstance(FacesContext.getCurrentInstance()).notifyBeans();
	   
	   return "browse";
   }

   // ------------------------------------------------------------------------------
   // Bean getters and setters

    public String getUploadFileSizeLimit() {
        return this.parapheurService.getUploadFileSizeLimit();
    }

   /**
    * @return Returns the message to display when a file has been uploaded
    */
   public String getFileUploadSuccessMsg()
   {
      // NOTE: This is a far from ideal solution but will do until we have 
      //       a pure JSF upload solution working. This method is only called
      //       after a file is uploaded, so we can calculate the mime type and
      //       determine whether to enable inline editing in here. 
      this.mimeType = Repository.getMimeTypeForFileName(
            FacesContext.getCurrentInstance(), this.fileName);
      this.inlineEdit = (this.mimeType.equals(MimetypeMap.MIMETYPE_HTML));
      
      // get the file upload message
      String msg = " ajouté avec succès"; // Application.getMessage(FacesContext.getCurrentInstance(), "file_upload_success");
      return getFileName() + msg; // MessageFormat.format(msg, new Object[] {getFileName()});
   }
   
   /**
    * @return Returns the name of the file
    */
   @Override
   public String getFileName()
   {
      // try and retrieve the file and filename from the file upload bean
      // representing the file we previously uploaded.
      FacesContext ctx = FacesContext.getCurrentInstance();
      FileUploadBean fileBean = (FileUploadBean)ctx.getExternalContext().getSessionMap().
         get(FileUploadBean.FILE_UPLOAD_BEAN_NAME);
      if (fileBean != null)
      {
         this.file = fileBean.getFile();
         this.fileName = fileBean.getFileName();
      }
      
      return this.fileName;
   }

   /**
    * @param fileName The name of the file
    */
   @Override
   public void setFileName(String fileName)
   {
      this.fileName = fileName;
      
      // we also need to keep the file upload bean in sync
      FacesContext ctx = FacesContext.getCurrentInstance();
      FileUploadBean fileBean = (FileUploadBean)ctx.getExternalContext().getSessionMap().
         get(FileUploadBean.FILE_UPLOAD_BEAN_NAME);
      if (fileBean != null)
      {
         fileBean.setFileName(this.fileName);
      }
   }
   
   // ------------------------------------------------------------------------------
   // Action event handlers
   
   /**
    * Action listener called when the add content dialog is called
    */
   public void start(ActionEvent event)
   {
      // NOTE: this is a temporary solution to allow us to use the new dialog
      //       framework beans outside of the dialog framework, we need to do
      //       this because the uploading requires a separate non-JSF form, this
      //       approach can not be used in the current dialog framework. Until
      //       we have a pure JSF upload solution we need this initialisation
   	logger.debug("initialisation");
      init(null);
   }
   
   /**
    * Action handler called when the user wishes to remove an uploaded file
    */
   public String removeUploadedFile()
   {
      clearUpload();
      
      // also clear the file name
      this.fileName = null;
      
      // refresh the current page
      return null;
   }
   
   /**
    * Action handler called when the dialog is cancelled
    */
   @Override
   public String cancel()
   {
      clearUpload();
      
      return "cancel";
   }
   
   // ------------------------------------------------------------------------------
   // Helper Methods
   
   /**
    * Deletes the uploaded file and removes the FileUploadBean from the session
    */
   protected void clearUpload()
   {
      // delete the temporary file we uploaded earlier
      if (this.file != null)
      {
         this.file.delete();
      }
      
      this.file = null;
      
      // remove the file upload bean from the session
      FacesContext ctx = FacesContext.getCurrentInstance();
      ctx.getExternalContext().getSessionMap().remove(FileUploadBean.FILE_UPLOAD_BEAN_NAME);
   }
}
