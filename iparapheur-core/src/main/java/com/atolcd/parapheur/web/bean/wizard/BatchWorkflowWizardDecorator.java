/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package com.atolcd.parapheur.web.bean.wizard;

import com.atolcd.parapheur.web.bean.wizard.batch.FilterableContent;
import com.atolcd.parapheur.web.bean.wizard.batch.SortableContent;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.dialog.IDialogBean;
import org.alfresco.web.bean.wizard.IWizardBean;
import org.alfresco.web.config.DialogsConfigElement;

import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class BatchWorkflowWizardDecorator implements IDialogBean, IWizardBean, SortableContent, FilterableContent {
    private AbstractBatchWorkflowWizard decorated;
    private String defaultDecoratedKey;
    private String currentDecoratedKey;
    private List<SelectItem> decoratedItems;
    public final static String EMISSION = "emission";
    public final static String ARCHIVAGE = "archivage";
    public final static String VISA = "visa";
    public final static String SIGNATURE = "signature";
    public final static String SIGNATURE_SEULE = "signatureSeule";

    Map<String, AbstractBatchWorkflowWizard> decoratedMap;


    public AbstractBatchWorkflowWizard getDecorated() {
        return decorated;
    }

    public List<SelectItem> getDecoratedItems() {
        if (decoratedItems == null) {
            decoratedItems = new ArrayList<SelectItem>();

            decoratedItems.add(new SelectItem(EMISSION, "Emission"));
            decoratedItems.add(new SelectItem(ARCHIVAGE, "Archivage"));
            decoratedItems.add(new SelectItem(SIGNATURE, "Visa/Signature"));
            decoratedItems.add(new SelectItem(VISA, "Visa"));
            decoratedItems.add(new SelectItem(SIGNATURE_SEULE, "Signature"));
        }
        return decoratedItems;
    }

    public void setDefaultDecoratedKey(String defaultDecoratedKey) {
        this.defaultDecoratedKey = defaultDecoratedKey;
    }

    public void setDecoratedMap(Map<String, AbstractBatchWorkflowWizard> decoratedMap) {
        this.decoratedMap = decoratedMap;
    }

    public String getCurrentDecoratedKey() {
        return currentDecoratedKey;
    }

    public void setCurrentDecoratedKey(String currentDecoratedKey) {

        this.currentDecoratedKey = currentDecoratedKey;

        this.decorated = decoratedMap.get(currentDecoratedKey);

        if (this.decorated == null) {
            throw new RuntimeException(String.format("Il n'y a pas de bean correspondant à la clef : %s.", currentDecoratedKey));
        }
    }

    public List<SelectItem> getNames() {
        return decorated.getNames();
    }

    public List<AbstractBatchWorkflowWizard.SelectableDossier> getDossiers() {
        return decorated.getDossiers();
    }

    public DataModel getDossiersModel() {
        return decorated.getDossiersModel();
    }

    public String getAnnotationPrivee() {
        return decorated.getAnnotationPrivee();
    }

    public void setAnnotationPrivee(String annotationPrivee) {
        decorated.setAnnotationPrivee(annotationPrivee);
    }

    public String getAnnotation() {
        return decorated.getAnnotation();
    }

    public void setAnnotation(String annotation) {
        decorated.setAnnotation(annotation);
    }

    public void setOrderQNameString(String qNameString) {
        decorated.setOrderQNameString(qNameString);
    }

    public String getOrderQNameString() {
        return decorated.getOrderQNameString();
    }

    public List<Map<String, String>> getAppletParams() {
        return decorated.getAppletParams();
    }

    public String getAppletUrl() {
        return decorated.getAppletUrl();
    }

    public int getAppletParamsCount() {
        return decorated.getAppletParamsCount();
    }

    public Boolean getConnectionSecure() {
        return decorated.getConnectionSecure();
    }

    @Override
    public void buildFilter(Map<QName, String> qNameStringMap) {
        decorated.buildFilter(qNameStringMap);
    }

    @Override
    public List<NodeRef> filterContent() {
        return decorated.filterContent();
    }

    @Override
    public void setSortQName(QName qName) {
        decorated.setSortQName(qName);
    }

    @Override
    public void sortContent() {
        decorated.sortContent();
    }

    @Override
    public String next() {
        return decorated.next();
    }

    @Override
    public String back() {
        return decorated.back();
    }

    @Override
    public String getNextButtonLabel() {
        return decorated.getNextButtonLabel();
    }

    @Override
    public String getBackButtonLabel() {
        return decorated.getBackButtonLabel();
    }

    @Override
    public boolean getNextButtonDisabled() {
        return decorated.getNextButtonDisabled();
    }

    @Override
    public String getStepTitle() {
        return decorated.getStepTitle();
    }

    @Override
    public String getStepDescription() {
        return decorated.getStepDescription();
    }

    @Override
    public void init(Map<String, String> stringStringMap) {
        assert (defaultDecoratedKey != null);

        currentDecoratedKey = defaultDecoratedKey;

        for (String key: decoratedMap.keySet()) {
            decoratedMap.get(key).init(stringStringMap);
        }

        decorated = decoratedMap.get(currentDecoratedKey);
        decorated.init(stringStringMap);
    }

    @Override
    public void restored() {
        decorated.restored();
    }

    @Override
    public String cancel() {
        return decorated.cancel();
    }

    @Override
    public String finish() {
        return decorated.finish();
    }

    @Override
    public List<DialogsConfigElement.DialogButtonConfig> getAdditionalButtons() {
        return decorated.getAdditionalButtons();
    }

    @Override
    public String getCancelButtonLabel() {
        return decorated.getCancelButtonLabel();
    }

    @Override
    public String getFinishButtonLabel() {
        return decorated.getFinishButtonLabel();
    }

    @Override
    public boolean getFinishButtonDisabled() {
        return decorated.getFinishButtonDisabled();
    }

    @Override
    public String getContainerTitle() {
        return decorated.getContainerTitle();
    }

    @Override
    public String getContainerSubTitle() {
        return decorated.getContainerSubTitle();
    }

    @Override
    public String getContainerDescription() {
        return decorated.getContainerDescription();
    }

    @Override
    public Object getActionsContext() {
        return decorated.getActionsContext();
    }

    @Override
    public String getActionsConfigId() {
        return decorated.getActionsConfigId();
    }

    @Override
    public String getMoreActionsConfigId() {
        return decorated.getMoreActionsConfigId();
    }

    @Override
    public boolean isFinished() {
        return decorated.isFinished();
    }

    @Override
    public void setSortOrder(String sortOrder) {
        decorated.setSortOrder(sortOrder);
    }

    public String getSortOrder() {
        return decorated.getSortOrder();
    }
    
    public void setIncludeDelegates(String includeDelegates) {
        decorated.setIncludeDelegates(includeDelegates);
    }

    public String getIncludeDelegates() {
        return decorated.getIncludeDelegates();
    }
}
