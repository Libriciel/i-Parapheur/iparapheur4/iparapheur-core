/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.bean;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.app.ParapheurApplication;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.web.app.Application;
import org.alfresco.web.bean.LoginOutcomeBean;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.bean.repository.User;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class LoginBean extends org.alfresco.web.bean.LoginBean {
    private static Logger logger = Logger.getLogger(LoginBean.class);
    
    protected ParapheurService parapheurService;
    protected SearchService searchService;
    protected NamespaceService namespaceService;
    
    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService)
    {
        this.parapheurService = parapheurService;
    }

    /**
     * @param searchService The searchService to set.
     */
    public void setSearchService(SearchService searchService)
    {
        this.searchService = searchService;
    }
    /**
     * @param namespaceService The namespaceService to set.
     */
    public void setNamespaceService(NamespaceService namespaceService)
    {
        this.namespaceService = namespaceService;
    }

    @Override
    public String login() {
        FacesContext fc = FacesContext.getCurrentInstance();

        // Lors d'un accès à http://serveur:8080/alfresco/, on est redirigé vers browse.jsp
        // en supprimant cette redirection (via la suppression de la clé dans la session),
        // on part sur une utilisation standard des "outcome"s
        // On effectue également ce traitement lors d'accès à parapheur/parapheurs.jsp ou
        // parapheur/parapheur.jsp, par soucis de complétude.
        /*Map session = fc.getExternalContext().getSessionMap();
        String uri = (String) session.get(LOGIN_REDIRECT_KEY);
        if (uri != null)
        {
            if (logger.isDebugEnabled())
                logger.debug("Présence d'une redirection après login vers: <" + uri + ">");

            String uriPrefix = fc.getExternalContext().getRequestContextPath() + BaseServlet.FACES_SERVLET + "/jsp/";
            if (uri.indexOf(uriPrefix + "browse/browse.jsp") != -1 || uri.indexOf(uriPrefix + "parapheur/parapheur") != -1)
            {
                session.remove(LOGIN_REDIRECT_KEY);

                if (logger.isDebugEnabled())
                    logger.debug("Suppression de la redirection après login, on utilisera l'outcome et la navigation JSF.");
            }
        }*/

        // Phase de login standard
        String outcome = null;
        try {
            outcome = super.login();
        } catch (Exception e) {
            // Catches standard login exception, and pool corresponding message.
            String msg = e.getMessage();
            msg = msg.replace("Tenant does not exist", "Cette collectivité n'existe pas");
            FacesMessage message = new FacesMessage(msg);
            fc.addMessage(null, message);
        }

        logger.debug("LoginBean: outcome = " + outcome);

        // Si on ne fait pas de redirection mais qu'on utilise un "outcome", on peut
        // se permettre le "luxe" d'envoyer directement vers le parapheur duquel on
        // est propriétaire (le cas échéant), puisque le "dispatch" aura bien lieu.
        if (StringUtils.equals(outcome, "success")) {
            User user = Application.getCurrentUser(fc);

            NodeRef parapheur = null;
            String xpath = Application.getRootPath(fc) + "/" +
                           ParapheurApplication.getParapheursFolderName(fc);

            List<NodeRef> results = null;
            results = searchService.selectNodes(
                    getNodeService().getRootNode(Repository.getStoreRef()),
                    xpath,
                    null,
                    namespaceService,
                    false);

            if (results != null && results.size() == 1) {
                parapheur = results.get(0);
            }

            //            //NodeRef parapheur = this.parapheurService.getOwnedParapheur(user.getUserName());
            //            NodeRef parapheur = null;
            //            List<NodeRef> parapheurs = this.parapheurService.getOwnedParapheurs(user.getUserName());
            //
            //            if (parapheurs != null && !parapheurs.isEmpty())
            //            {
            //                if (logger.isDebugEnabled())
            //                    logger.debug("L'utilisateur "+ user.getUserName() +" est propriétaire d'un parapheur.");
            //
            //                // Si l'utilisateur n'est pas propriétaire d'un parapheur
            //                // (cas des secrétaires), on va sur le conteneur des parapheurs
            //                // Sinon on ira sur le parapheur directement.
            //                String xpath = Application.getRootPath(fc) + "/" +
            //                               ParapheurApplication.getParapheursFolderName(fc);
            //
            //                List<NodeRef> results = null;
            //                    results = searchService.selectNodes(
            //                            getNodeService().getRootNode(Repository.getStoreRef()),
            //                            xpath,
            //                            null,
            //                            namespaceService,
            //                            false);
            //
            //                if (results != null && results.size() == 1)
            //                {
            //                    parapheur = results.get(0);
            //                }
            //            }
            //            else {
            //                if (logger.isDebugEnabled())
            //                    logger.debug("LoginBean: getOwnedParapheur raté pour " + user.getUserName());
            //            }
            // put the User object in the Session - the authentication servlet will then allow
            // the app to continue without redirecting to the login page
            Application.setCurrentUser(fc, user);

            // Programatically retrieve the LoginOutcomeBean from JSF
            LoginOutcomeBean loginOutcomeBean = (LoginOutcomeBean) fc.getApplication().createValueBinding(
                    "#{LoginOutcomeBean}").getValue(fc);

            // if a redirect URL has been provided then use that
            // this allows servlets etc. to provide a URL to return too after a successful login
            String redirectURL = loginOutcomeBean.getRedirectURL();
            if (redirectURL != null && redirectURL.length() > 0) {
                logger.debug("Redirect URL found: " + redirectURL);

                try {
                    fc.getExternalContext().redirect(redirectURL);
                    fc.responseComplete();
                    return null;
                } catch (IOException ioErr) {
                    logger.warn("Unable to redirect to url: " + redirectURL);
                }
            } else if (parapheur != null) {
                logger.debug("Direction vers l'espace: " + parapheur + ".");

                // On a trouvé soit un parapheur dont l'utilisateur est propriétaire,
                // soit le conteneur des parapheurs s'il n'est propriétaire d'aucun
                // parapheur (cas des secrétaires); on y va.
                user.setHomeSpaceId(parapheur.getId());
                this.navigator.setCurrentNodeId(parapheur.getId());
                this.navigator.setupDispatchContext(new Node(parapheur));
            }
        }

        return outcome;
    }
    
    @Override
    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();

        // The outcome is decided in advance (before session expiry) and included as a parameter
        Map<?, ?> params = context.getExternalContext().getRequestParameterMap();
        String outcome = getLogoutOutcome();

        /*
        // Original alfresco code
        String outcome = (String) params.get(PARAM_OUTCOME);
        if (outcome == null) {
            outcome = OUTCOME_LOGOUT;
        }*/

        Locale language = Application.getLanguage(context);

        // Perform log out actions
        Application.logOut(context);

        // set language to last used on the login page
        Application.setLanguage(context, language.toString());

        return outcome;
    }

    private String OUTCOME_RELOGIN = "relogin";
    @Override
    public String getLogoutOutcome() {
       String outcome = super.getLogoutOutcome();

       Map<?, ?> session = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
       return session.get("_adlCertAuth") == null ? outcome : OUTCOME_RELOGIN;

    }




}
