/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.bean;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.context.IContextListener;
import org.alfresco.web.app.context.UIContextService;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.NavigationBean;
import org.alfresco.web.bean.repository.MapNode;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.ui.common.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.ui.repo.component.shelf.UICorbeillesShelfItem;
import java.util.Collections;
import org.alfresco.service.namespace.QName;

public class CorbeillesBean implements IContextListener {

    private static Log logger = LogFactory.getLog(CorbeillesBean.class);

    protected NodeService nodeService;

    protected SearchService searchService;

    protected ParapheurService parapheurService;

    protected NavigationBean navigator;

    protected BrowseBean browseBean;

    protected ParapheurBean parapheurBean;

    private List<Map> emission;

    private List<Map> reception;

    private List<Map> secretariat;

    public CorbeillesBean() {
        UIContextService.getInstance(FacesContext.getCurrentInstance()).registerBean(this);
    }

    public List<Map> getEmission() {
        if (this.emission == null) {
            if (parapheurBean.getParapheurCourant() == null) {
                if (logger.isWarnEnabled()) {
                    logger.warn("getEmission: pas possible, pas de parapheur courant, donc renvoi liste vide!");
                }
                return Collections.emptyList();
            }

            emission = new ArrayList<Map>();
            emission.add(getCorbeilleMapNode(ParapheurModel.NAME_EN_PREPARATION, false));
            emission.add(getCorbeilleMapNode(ParapheurModel.NAME_A_ARCHIVER, false));
            emission.add(getCorbeilleMapNode(ParapheurModel.NAME_RETOURNES, false));
            emission.add(getCorbeilleMapNode(ParapheurModel.NAME_EN_COURS, true));
            emission.add(getCorbeilleMapNode(ParapheurModel.NAME_TRAITES, true));
            emission.add(getCorbeilleMapNode(ParapheurModel.NAME_RECUPERABLES, true));
            emission.add(getCorbeilleMapNode(ParapheurModel.NAME_EN_RETARD, true));
        }
        return this.emission;
    }

    public List<Map> getReception() {
        if (this.reception == null) {
            NodeRef parapheurCourant = parapheurBean.getParapheurCourant();
            if (parapheurCourant == null) {
                if (logger.isWarnEnabled()) {
                    logger.warn("getReception: pas possible, pas de parapheur courant, donc renvoi liste vide!");
                }
                return Collections.emptyList();
            }

            reception = new ArrayList<Map>();
            reception.add(getCorbeilleMapNode(ParapheurModel.NAME_A_TRAITER, false));
            if (parapheurService.showAVenir(parapheurCourant)) {
                reception.add(getCorbeilleMapNode(ParapheurModel.NAME_A_VENIR, true));
            }
            reception.add(getCorbeilleMapNode(ParapheurModel.NAME_DOSSIERS_DELEGUES, true));
        }
        return this.reception;
    }

    public List<Map> getSecretariat() {
        if (this.secretariat == null) {
            if (parapheurBean.getParapheurCourant() == null) {
                if (logger.isWarnEnabled()) {
                    logger.warn("getSecretariat: pas possible, pas de parapheur courant, donc renvoi liste vide!");
                }
                return Collections.emptyList();
            }

            secretariat = new ArrayList<Map>();
            secretariat.add(getCorbeilleMapNode(ParapheurModel.NAME_A_ARCHIVER, false));
            secretariat.add(getCorbeilleMapNode(ParapheurModel.NAME_SECRETARIAT, false));
            secretariat.add(getCorbeilleMapNode(ParapheurModel.NAME_A_IMPRIMER, true));
        }
        return this.secretariat;
    }

    protected MapNode getCorbeilleMapNode(QName name, boolean virtuelle) {
        NodeRef parapheurCourant = parapheurBean.getParapheurCourant();
        NodeRef corbeille = parapheurService.getCorbeille(parapheurCourant, name);

        MapNode node = new MapNode(corbeille);
        node.put("virtuelle", virtuelle);
        node.addPropertyResolver("childCount", parapheurBean.resolverChildNumber);
        if (virtuelle) {
            node.addPropertyResolver("late", parapheurBean.resolverCorbeilleVirtuelleLate);
        } else {
            node.addPropertyResolver("late", parapheurBean.resolverCorbeilleLate);
        }

        return node;
    }

    /**
     * Action handler bound to the user shortcuts Shelf component called when a node is clicked
     */
    public void click(Node selectedNode) {
        try {
            if (nodeService.exists(selectedNode.getNodeRef()) == false) {
                throw new InvalidNodeRefException(selectedNode.getNodeRef());
            }

            DictionaryService dd = Repository.getServiceRegistry(FacesContext.getCurrentInstance()).getDictionaryService();
            if (dd.isSubClass(selectedNode.getType(), ContentModel.TYPE_FOLDER)) {
                // On navigue jusqu'au noeud sélectionné en passant par son père (pour éviter des problèmes de breadcrumb)
                NodeRef parentRef = nodeService.getPrimaryParent(selectedNode.getNodeRef()).getParentRef();
                this.browseBean.updateUILocation(parentRef);
                this.browseBean.updateUILocation(selectedNode.getNodeRef());
            }
        } catch (InvalidNodeRefException refErr) {
            Utils.addErrorMessage(MessageFormat.format(Application.getMessage(
                    FacesContext.getCurrentInstance(), Repository.ERROR_NODEREF), new Object[]{selectedNode.getId()}));
            logger.error(refErr.getMessage(), refErr);
        }
    }

    public void clickEmission(ActionEvent event) {
        UICorbeillesShelfItem.CorbeilleEvent corbeilleEvent = (UICorbeillesShelfItem.CorbeilleEvent) event;
        Node selectedNode = (MapNode) getEmission().get(corbeilleEvent.Index);
        this.click(selectedNode);
    }

    public void clickReception(ActionEvent event) {
        UICorbeillesShelfItem.CorbeilleEvent corbeilleEvent = (UICorbeillesShelfItem.CorbeilleEvent) event;
        Node selectedNode = (MapNode) getReception().get(corbeilleEvent.Index);
        this.click(selectedNode);
    }

    public void clickSecretariat(ActionEvent event) {
        UICorbeillesShelfItem.CorbeilleEvent corbeilleEvent = (UICorbeillesShelfItem.CorbeilleEvent) event;
        Node selectedNode = (MapNode) getSecretariat().get(corbeilleEvent.Index);
        this.click(selectedNode);
    }

    public void contextUpdated() {
        if (logger.isDebugEnabled()) {
            logger.debug("Invalidating \"corbeilles\" components...");
        }

        this.emission = null;
        this.reception = null;
        this.secretariat = null;
    }

    public void areaChanged() {
    }

    public void spaceChanged() {
    }

    // <editor-fold defaultstate="collapsed" desc="Setters for dependency injection">
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setNavigator(NavigationBean navigator) {
        this.navigator = navigator;
    }

    public void setBrowseBean(BrowseBean browseBean) {
        this.browseBean = browseBean;
    }

    public void setParapheurBean(ParapheurBean parapheurBean) {
        this.parapheurBean = parapheurBean;
    }
    
    // </editor-fold>
}
