package com.atolcd.parapheur.web.action.evaluator;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import org.adullact.iparapheur.status.StatusMetier;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.repository.Node;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 10/10/12
 * Time: 10:48
 * To change this template use File | Settings | File Templates.
 */
public class DemandeAvisComplementaireEvaluator extends BlindedActionEvaluator {
    @Override
    protected boolean evaluateImpl(Node node, NodeRef nodeRef) {

        EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(nodeRef);
        EtapeCircuit firstEtape = parapheurService.getFirstEtape(nodeRef);
        if (!etapeCourante.equals(firstEtape) && !parapheurService.isRejete(nodeRef) && !parapheurService.isTermine(nodeRef)
                && !((String) nodeService.getProperty(nodeRef, ParapheurModel.PROP_STATUS_METIER)).startsWith(StatusMetier.STATUS_EN_COURS)) {
            if (isOwner(AuthenticationUtil.getRunAsUser(), nodeRef)) {
                return true;
            }
        }

        return false;
    }
}
