/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.bean;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.app.context.IContextListener;
import org.alfresco.web.app.context.UIContextService;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.NavigationBean;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.ui.common.Utils;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

public class SpaceDossierDetailsBean implements IContextListener {

    private NavigationBean navigator;
    private BrowseBean browseBean;
    private ParapheurBean parapheurBean;
    private NodeService nodeService;
    private ParapheurService parapheurService;
    private MetadataService metadataService;
    private NamespaceService namespaceService;
    private List<CustomMetadataDef> metadatas;
    private Map<QName, Serializable> metadataValues;
    private TypesService typesService;
    private Map<String, Map<String, String>> metadatasMap;
    private List<CustomMetadataDef> metadataDefs;
    private Map<QName, CustomMetadataDef> metadataDefsMap;

    //  private Node currentNode;
    public SpaceDossierDetailsBean() {
        UIContextService.getInstance(FacesContext.getCurrentInstance()).registerBean(this);
        //  currentNode = null;
    }

    public TypesService getTypesService() {
        return typesService;
    }

    public void setTypesService(TypesService typesService) {
        this.typesService = typesService;
    }

    /**
     * Returns the id of the current space
     *
     * @return The id
     */
    public String getId() {
        return this.browseBean.getActionSpace().getId();
    }

    /**
     * Returns the name of the current space
     *
     * @return Name of the current space
     */
    public String getName() {
        return this.browseBean.getActionSpace().getName();
    }
    
    /**
     * Returns the title of the current space
     *
     * @return Title of the current space
     */
    public String getTitle() {
        return (String) this.browseBean.getActionSpace().getProperties().get("cm:title");
    }

    /**
     * Returns the Space this bean is currently representing
     *
     * @return The Space Node
     */
    public Node getSpace() {
        return this.browseBean.getActionSpace();
    }

    public boolean isMetadataEnabled() {
        if (!getMetadatas().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public List<CustomMetadataDef> getMetadatas() {
        // if (metadatas == null) {
        List<CustomMetadataDef> defs = metadataService.getMetadataDefs();

        // filter out the Mds not defined in the subtype
        metadatas = new ArrayList<CustomMetadataDef>();

        for (CustomMetadataDef def : defs) {
            if (getMetadatasMap().get(def.getName()) != null) {
                metadatas.add(def);
            }
        }
        // }

        return metadatas;
    }

    public Map<QName, Map<String, String>> getMetadatasMap() {

        metadataDefs = metadataService.getMetadataDefs();
        metadataDefsMap = new HashMap<QName, CustomMetadataDef>();

        for (CustomMetadataDef def : metadataDefs) {
            metadataDefsMap.put(def.getName(), def);
        }

        // if (metadatasMap == null) {
        String type = (String) nodeService.getProperty(navigator.getCurrentNode().getNodeRef(), ParapheurModel.PROP_TYPE_METIER);
        String sstype = (String) nodeService.getProperty(navigator.getCurrentNode().getNodeRef(), ParapheurModel.PROP_SOUSTYPE_METIER);
        Map<QName, Map<String, String>> retVal = new HashMap<QName, Map<String, String>>();

        if (type != null && sstype != null && !type.isEmpty() && !sstype.isEmpty()) {
            metadatasMap = typesService.getMetadatasMap(type, sstype);
            if (metadatasMap != null) {
                for (String key : metadatasMap.keySet()) {
                    retVal.put(QName.createQName(key), metadatasMap.get(key));
                }
            }
        }

        return retVal;
    }

    public boolean isNotEmis() {
        NodeRef dossier = navigator.getCurrentNode().getNodeRef();
        String currentUser = AuthenticationUtil.getRunAsUser();

        boolean isActeurCourant = parapheurService.isActeurCourant(dossier, currentUser);
        boolean isSecretaireActeurCourant = parapheurService.isParapheurSecretaire(parapheurService.getParentParapheur(dossier), currentUser);

        return !parapheurService.isEmis(dossier) && (isSecretaireActeurCourant || isActeurCourant);
    }

    public void updateMetadataValues() {
        try {
            HashMap<QName, Serializable> prop = new HashMap<QName, Serializable>();
            for (CustomMetadataDef def : metadatas) {

                if (def != null) {
                    CustomMetadataType type = def.getType();
                    final QName key = def.getName();
                    String valueStr = (String) metadataValues.get(key);
                    System.out.println(key.toPrefixString());
                    Map<String, String> metadataProperties = metadatasMap.get(key.toString());
                    Serializable value = null;
                    if (valueStr != null) {
                        value = (Serializable) metadataService.getValueFromString(type, valueStr);
                    } else if ("true".equals(metadataProperties.get("mandatory"))) {
                        throw new RuntimeException("Veuillez renseigner les métadonnées obligatoires");
                    }

                    if (type.equals(CustomMetadataType.DATE) && (valueStr == null || valueStr.length() == 0)) {
                        value = null;
                    }
                    final Serializable fvalue = value;

                    AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Void>() {
                        @Override
                        public Void doWork() throws Exception {
                            try {
                                nodeService.setProperty(navigator.getCurrentNode().getNodeRef(), key, fvalue);
                            } catch (Exception e) {
                                Utils.addErrorMessage("Erreur lors de la mise a jour des métadonnées : " + e.getMessage(), e);
                            }
                            return null;
                        }
                    }, AuthenticationUtil.getAdminUserName());
                }
            }


        } catch (Exception e) {
            Utils.addErrorMessage("Erreur : " + e.getMessage(), e);
            //          logger.error(e.getMessage(), e);
        }
        //nodeService.setProperties(navigator.getCurrentNode().getNodeRef(), metadataValues);
    }

    public void updateDossierValues() {
        Node node = this.browseBean.getActionSpace();
        //this.navigator.setCurrentNodeId(node.getId());
        String newName = (String) node.getProperties().get(ContentModel.PROP_NAME.toPrefixString());
        String newTitle = (String) node.getProperties().get(ContentModel.PROP_TITLE.toPrefixString());
        Date newDateLimite = (Date) node.getProperties().get("ph:dateLimite");
        nodeService.setProperty(node.getNodeRef(), ContentModel.PROP_NAME, newName);
        nodeService.setProperty(node.getNodeRef(), ContentModel.PROP_TITLE, newTitle);
        nodeService.setProperty(node.getNodeRef(), ParapheurModel.PROP_DATE_LIMITE, newDateLimite);

        // force update of the node name in BreadCrumb
        this.browseBean.removeSpaceFromBreadcrumb(node);
        this.browseBean.updateUILocation(node.getNodeRef());

    }

    public Map<QName, Serializable> getMetadataValues() {
        if (metadataValues == null) {
            metadataValues = nodeService.getProperties(navigator.getCurrentNode().getNodeRef());
        }

        for (QName key : metadataValues.keySet()) {
            if (metadataValues.get(key) instanceof Date) { // si null ?
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String stringValue = format.format((Date) metadataValues.get(key));
                metadataValues.put(key, stringValue);
            } else if (!(metadataValues.get(key) instanceof String)) {
                metadataValues.put(key, String.valueOf(metadataValues.get(key)));
            }
        }

        return metadataValues;
    }

    public boolean isDossierInCurrentParapheur() {
        boolean retVal = false;
        NodeRef dossier = navigator.getCurrentNode().getNodeRef();
        EtapeCircuit currentStep = parapheurService.getCurrentEtapeCircuit(dossier);

        if (currentStep != null) {
            NodeRef lastParapheur = currentStep.getParapheur();
            NodeRef currentParapheur = parapheurBean.getParapheurCourant();
            if (lastParapheur.equals(currentParapheur)) {
                retVal = true;
            }
        }

        return retVal;
    }

    public boolean isCurrentParapheurOwnerOrDelegateOfDossier() {
        NodeRef dossier = navigator.getCurrentNode().getNodeRef();
        return parapheurService.isOwnerOrDelegateOfDossier(parapheurBean.getParapheurCourant(), dossier);
    }

    @Override
    public void contextUpdated() {
        metadataValues = null;
    }

    @Override
    public void spaceChanged() {
    }

    @Override
    public void areaChanged() {
    }

    // <editor-fold defaultstate="collapsed" desc="Setters for dependency injection">
    public void setBrowseBean(BrowseBean browseBean) {
        this.browseBean = browseBean;
    }

    public void setNavigator(NavigationBean navigator) {
        this.navigator = navigator;
    }

    public void setParapheurBean(ParapheurBean parapheurBean) {
        this.parapheurBean = parapheurBean;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }
    // </editor-fold>
}
