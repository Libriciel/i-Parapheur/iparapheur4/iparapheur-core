package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.bean.ParapheurBean;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.apache.log4j.Logger;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 01/10/12
 * Time: 10:35
 * To change this template use File | Settings | File Templates.
 */
public class ChangeSignataireDialog extends BaseDialogBean {
    private static Logger logger = Logger.getLogger(ChangeSignataireDialog.class);

    private ParapheurService parapheurService;
    private ParapheurBean parapheurBean;
    private String annotationPrivee;
    private String annotationPublique;
    private NodeRef bureauRef;

    private SelectItem[] bureaux;

    @Override
    protected String finishImpl(FacesContext facesContext, String s) throws Throwable {
        NodeRef dossier = navigator.getCurrentNode().getNodeRef();
        parapheurService.changeSignataire(dossier, bureauRef, annotationPublique, annotationPrivee);
        return s;
    }

    @Override
    public boolean getFinishButtonDisabled() {
        return false;
    }

    public ParapheurService getParapheurService() {
        return parapheurService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public String getAnnotationPrivee() {
        return annotationPrivee;
    }

    public void setAnnotationPrivee(String annotationPrivee) {
        this.annotationPrivee = annotationPrivee;
    }

    public String getAnnotationPublique() {
        return annotationPublique;
    }

    public void setAnnotationPublique(String annotationPublique) {
        this.annotationPublique = annotationPublique;
    }

    public NodeRef getBureauRef() {
        return bureauRef;
    }

    public void setBureauRef(NodeRef bureauRef) {
        this.bureauRef = bureauRef;
    }

    private class NodeTitleComparator implements Comparator<NodeRef> {
        @Override
        public int compare(NodeRef o1, NodeRef o2) {
            NodeService nodeService = getNodeService();
            String n1 = (String) nodeService.getProperty(o1, org.alfresco.model.ContentModel.PROP_TITLE);
            String n2 = (String) nodeService.getProperty(o2, org.alfresco.model.ContentModel.PROP_TITLE);
            return n1.compareToIgnoreCase(n2);
        }
    }

    public SelectItem[] getBureaux() {
        //Doit etre initialisé à chaque fois dans le cas de transferts successifs
        //if (bureaux == null) {
        List<NodeRef> parapheurs = parapheurService.getDelegationsPossibles(parapheurBean.getParapheurCourant());

        Collections.sort(parapheurs, new NodeTitleComparator());

        List<SelectItem> bureauxTmp = new ArrayList<SelectItem>();

        for (NodeRef bureau : parapheurs) {
            SelectItem item = new SelectItem(bureau, parapheurService.getNomParapheur(bureau));
            bureauxTmp.add(item);
        }

        bureaux = new SelectItem[bureauxTmp.size()];
        bureaux = bureauxTmp.toArray(bureaux);
        //}
        return bureaux;
    }

    public void setBureaux(SelectItem[] bureaux) {
        this.bureaux = bureaux;
    }

    public ParapheurBean getParapheurBean() {
        return parapheurBean;
    }

    public void setParapheurBean(ParapheurBean parapheurBean) {
        this.parapheurBean = parapheurBean;
    }
}
