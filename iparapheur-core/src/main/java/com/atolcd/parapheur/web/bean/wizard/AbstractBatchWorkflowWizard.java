/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.web.bean.wizard;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.web.bean.CorbeillesBean;
import com.atolcd.parapheur.web.bean.ParapheurBean;
import com.atolcd.parapheur.web.bean.wizard.batch.AbstractFilter;
import com.atolcd.parapheur.web.bean.wizard.batch.DossierFilter;
import com.atolcd.parapheur.web.bean.wizard.batch.FilterableContent;
import com.atolcd.parapheur.web.bean.wizard.batch.SortableContent;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.bean.wizard.BaseWizardBean;

import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.*;

public abstract class AbstractBatchWorkflowWizard extends BaseWizardBean implements FilterableContent, SortableContent {

    protected SearchService searchService;
    protected AbstractFilter dossierFilter;
    protected ParapheurService parapheurService;
    protected ParapheurBean parapheurBean;
    protected CorbeillesService corbeillesService;
    protected NodeService nodeService;
    protected MetadataService metadataService;
    protected QName sortQName;
    protected String searchRoot;
    protected NamespaceService namespaceService;
    protected CorbeillesBean corbeillesBean;
    protected List<SelectableDossier> dossiers;
    protected DataModel dossiersModel;
    protected JobService jobService;
    protected int currentStep;
    private Boolean connectionSecure;
    private String filterValue;
    private String filterOperator;
    private String filterQNameString;
    private String includeDelegates;
    protected String corbeilleName;
    private List<SelectItem> operators;
    private String sortOrder;
    /**
     * Public annotation used during signature
     */
    protected String annotation;
    /**
     * Private annotation used during signature
     */
    protected String annotationPrivee;

    public String getFilterQNameString() {
        return filterQNameString;
    }

    public void setFilterQNameString(String filterQNameString) {
        if (!filterQNameString.equals(this.filterQNameString)) {
            reloadDossiersList();
            this.filterQNameString = filterQNameString;
        }
    }

    public List<SelectItem> getOperators() {
        return operators;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        if (!filterValue.equals(this.filterValue)) {
            reloadDossiersList();
        }
        this.filterValue = filterValue;
    }

    public String getFilterOperator() {
        return filterOperator;
    }

    public void setFilterOperator(String filterOperator) {
        this.filterOperator = filterOperator;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {

        if (!sortOrder.equals(this.sortOrder)) {
            reloadDossiersList();
        }
        this.sortOrder = sortOrder;
    }

    protected void reloadDossiersList() {
        this.dossiers = null;
        this.dossiersModel = null;
    }

    protected String getSearchPath() {
        NodeRef parapheurCourant = parapheurBean.getParapheurCourant();

        String parapheurName = (String) nodeService.getProperty(parapheurCourant, ContentModel.PROP_NAME);
        Path path = nodeService.getPath(parapheurCourant);
        Path.Element e = path.last();

        String elementString = e.getElementString();
        QName corbeille = ParapheurModel.NAME_EN_PREPARATION.getPrefixedQName(namespaceService);
        corbeilleName = ParapheurModel.NAME_EN_PREPARATION.toString();
        return String.format("/app:company_home/ph:parapheurs/%s/%s/*", e.getPrefixedString(namespaceService), corbeille.getPrefixString());
    }

    @Override
    public void init(Map<String, String> parameters) {
        super.init(parameters);
        dossiersModel = null;
        dossierFilter = new DossierFilter();
        dossierFilter.setObjectType(ParapheurModel.TYPE_DOSSIER.toString());
        sortQName = null;
        dossiers = null;
        connectionSecure = null;
        currentStep = 0;
        dossierFilter.setSearchPath(getSearchPath());
        sortOrder = "ASC";
        includeDelegates = "current";

        filterValue = "";
        filterOperator = null;
        filterQNameString = "";

        operators = new ArrayList<SelectItem>();
        operators.add(new SelectItem("=", "="));
        operators.add(new SelectItem(">", ">"));
        operators.add(new SelectItem("<", "<"));

    }

    @Override
    public void buildFilter(Map<QName, String> qNameStringMap) {
        dossierFilter.addAllFilterElement(qNameStringMap);
    }

    @Override
    public List<NodeRef> filterContent() {
        List<NodeRef> results = new ArrayList<NodeRef>();

        if ("current".equals(includeDelegates) || "both".equals(includeDelegates)) {
            dossierFilter.clearFilterElements();
            if (filterQNameString != null && !filterQNameString.isEmpty()) {
                dossierFilter.addFilterElement(QName.createQName(filterQNameString, namespaceService), filterValue);
            }

            SearchParameters searchParameters = new SearchParameters();

            searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
            searchParameters.setQuery(dossierFilter.buildFilterQuery());
            searchParameters.addStore(Repository.getStoreRef());

            ResultSet resultSet = searchService.query(searchParameters);

            results.addAll(resultSet.getNodeRefs());
            resultSet.close();
        }
        if ("delegates".equals(includeDelegates) || "both".equals(includeDelegates)) {
            //Récupération des dossiers en délégation
            NodeRef parapheurCourant = parapheurBean.getParapheurCourant();
            List<NodeRef> dossiersDelegues = corbeillesService.getDossiersDelegues(parapheurCourant);
            for (NodeRef dossier : dossiersDelegues) {
                String cName = QName.createQName(nodeService.getPath(parapheurService.getParentCorbeille(dossier)).last().getElementString()).toString();
                if (cName.equals(corbeilleName)) {
                    if (filterQNameString != null && !filterQNameString.isEmpty()) {
                        String propToTest = nodeService.getProperty(dossier, QName.createQName(filterQNameString, namespaceService)).toString();
                        if (propToTest.contains(filterValue)) {
                            results.add(dossier);
                        }
                    } else {
                        results.add(dossier);
                    }
                }
            }
        }

        /*
         // FIXME: it doesn't work with +PATH directive
         if (sortQName != null) {
         searchParameters.addSort("@"+sortQName.getPrefixedQName(namespaceService).getPrefixString(), false);
         searchParameters.addSort("@cm\\:title", false);
         System.out.println("@"+sortQName.getPrefixedQName(namespaceService).getPrefixString());
         }
         */


        return results;
    }

    @Override
    public void setSortQName(QName qName) {
        this.sortQName = qName;
    }

    @Override
    public void sortContent() {
        this.dossiers = null;
        this.dossiers = getDossiers();
    }

    public void setOrderQNameString(String qNameString) {
        if (qNameString != null && !qNameString.isEmpty()) {
            QName tmp = QName.createQName(qNameString, namespaceService);
            if (sortQName != null && tmp.equals(sortQName)) {
            } else {
                reloadDossiersList();
                this.sortQName = tmp;
            }
        } else {
            this.sortQName = null;
            //reloadDossiersList();
        }
    }

    public String getOrderQNameString() {
        if (sortQName == null) {
            return "";
        } else {
            return sortQName.toPrefixString(namespaceService);
        }
    }

    public List<SelectItem> getNames() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        items.add(new SelectItem("cm:title", "Nom du dossier"));
        items.add(new SelectItem("ph:typeMetier", "Type Métier"));
        items.add(new SelectItem("ph:soustypeMetier", "Sous Type Métier"));

        List<CustomMetadataDef> metadatas = metadataService.getMetadataDefs();

        for (CustomMetadataDef def : metadatas) {
            items.add(new SelectItem(def.getName().toPrefixString(namespaceService), def.getTitle()));
        }

        return items;
    }

    public List<Map<String, String>> getAppletParams() {
        return null;
    }

    public String getAppletUrl() {
        return "";
    }

    public int getAppletParamsCount() {
        return 0;
    }

    public boolean getConnectionSecure() {
        if (connectionSecure == null) {
            connectionSecure = false;

            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().
                    getExternalContext().
                    getRequest();
            boolean secure = request.isSecure();

            if (secure) {
                connectionSecure = true;
            }
        }
        return connectionSecure;

    }

    public List<SelectableDossier> getDossiers() {

        if (this.dossiers == null) {

            List<SelectableDossier> retVal = new ArrayList<SelectableDossier>();

            List<NodeRef> mesDossiers = filterContent();

            for (NodeRef dossier : mesDossiers) {

                if (shallSelectDossier(dossier)) {
                    SelectableDossier selectableDossier = new SelectableDossier(new Node(dossier), false, false);

                    if (sortQName != null) {

                        Serializable value = (Serializable) nodeService.getProperty(dossier, sortQName);

                        if (value != null) {
                            selectableDossier.setFilterValue(value.toString());
                        } else {
                            selectableDossier.setFilterValue(null);
                        }
                    }

                    retVal.add(selectableDossier);
                }
            }
            this.dossiers = retVal;

            if (sortQName != null) {
                Collections.sort(dossiers, dossierComparator);
            }

        }

        return this.dossiers;
    }

    protected abstract boolean shallSelectDossier(NodeRef dossier);

    @Override
    public String back() {
        this.currentStep -= 1;
        return super.back();
    }

    @Override
    public String next() {
        this.currentStep += 1;
        return super.next();
    }

    /**
     * Creates a JSF DataModel based on folders returned by {@code getDossiers}.
     *
     * @return a JSF data model
     * @see BatchWorkflowWizard#getDossiers()
     */
    public DataModel getDossiersModel() {
        if (dossiersModel == null) {
            dossiersModel = new ListDataModel(getDossiers());
        }
        return dossiersModel;
    }

    public void setDossiersModel(DataModel dossiersModel) {
        this.dossiersModel = dossiersModel;
    }

    public String getAnnotationPrivee() {
        return annotationPrivee;
    }

    public void setAnnotationPrivee(String annotationPrivee) {
        this.annotationPrivee = annotationPrivee;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    /* Dependecy Injection */
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setParapheurBean(ParapheurBean parapheurBean) {
        this.parapheurBean = parapheurBean;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setCorbeillesService(CorbeillesService corbeillesService) {
        this.corbeillesService = corbeillesService;
    }

    public void setCorbeillesBean(CorbeillesBean corbeillesBean) {
        this.corbeillesBean = corbeillesBean;
    }

    public void setJobService(JobService jobService) {
        this.jobService = jobService;
    }

    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    public String getIncludeDelegates() {
        return includeDelegates;
    }

    public void setIncludeDelegates(String includeDelegates) {
        if (!includeDelegates.equals(this.includeDelegates)) {
            reloadDossiersList();
        }
        this.includeDelegates = includeDelegates;
    }

    public static class SelectableDossier {

        private Node dossier;
        private boolean read;
        /**
         * Whether the folder is selected or not.
         */
        private boolean selected;
        private String filterValue;

        public SelectableDossier() {
        }

        public SelectableDossier(Node dossier, boolean read, boolean selected) {
            this.dossier = dossier;
            this.read = read;
            this.selected = selected;
        }

        public Node getDossier() {
            return dossier;
        }

        public void setDossier(Node dossier) {
            this.dossier = dossier;
        }

        public boolean isRead() {
            return read;
        }

        public void setRead(boolean read) {
            this.read = read;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public String getFilterValue() {
            return this.filterValue;
        }

        public void setFilterValue(String filterValue) {
            this.filterValue = filterValue;
        }
    }
    private Comparator<SelectableDossier> dossierComparator = new Comparator<SelectableDossier>() {
        public int compare(SelectableDossier t, SelectableDossier t1) {
            int retval = 0;

            Node n = t.getDossier();
            Node n2 = t1.getDossier();
            NodeRef dossier1 = n.getNodeRef();
            NodeRef dossier2 = n2.getNodeRef();

            if (sortQName != null) {

                String value1 = t.getFilterValue();
                String value2 = t1.getFilterValue();

                if ("DESC".equals(sortOrder)) {
                    String tmp = value1;
                    value1 = value2;
                    value2 = tmp;
                }

                if (value1 != null && !value1.isEmpty()) {
                    if (value2 != null && !value2.isEmpty()) {
                        retval = value1.compareTo(value2);
                    } else {
                        retval = -1;
                    }
                } else if (value2 != null && !value2.isEmpty()) {
                    retval = 1;
                } else {
                    retval = 0;
                }

            }

            if (retval == 0) {
                String nom1 = (String) getNodeService().getProperty(dossier1, ContentModel.PROP_NAME);
                String nom2 = (String) getNodeService().getProperty(dossier2, ContentModel.PROP_NAME);
                if (("DESC").equals(sortOrder)) {
                    retval = nom2.compareTo(nom1);
                } else {
                    retval = nom1.compareTo(nom2);
                }
            }

            return retval;

        }
    };

    public void forceShelfUpdate() {
        corbeillesBean.contextUpdated();
    }
}
