/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.ui.repo.component;

import java.io.IOException;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.component.NamingContainer;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import org.alfresco.web.app.Application;
import org.alfresco.web.ui.common.Utils;

public class UISimpleSearch extends org.alfresco.web.ui.repo.component.UISimpleSearch {

    private static final String MSG_ADVANCED_SEARCH = "advanced_search";

    private static final String MSG_OPTIONS = "options";

    private static final String MSG_GO = "go";

    private static final String MSG_SPACE_NAMES_ONLY = "space_names";

    private static final String MSG_FILE_NAMES_ONLY = "file_names";

    private static final String MSG_FILE_NAMES_CONTENTS = "file_names_contents";

    private static final String MSG_ALL_ITEMS = "all_items";

    private static final String OPTION_PARAM = "_option";

    private static final String ADVSEARCH_PARAM = "_advsearch";

    /**
     * @see org.alfresco.web.ui.repo.component.UISimpleSearch#encodeBegin(javax.faces.context.FacesContext)
     */
    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        if (isRendered() == false) {
            return;
        }

        ResponseWriter out = context.getResponseWriter();

        ResourceBundle bundle = (ResourceBundle) Application.getBundle(context);

        // script for dynamic simple search menu drop-down options
        out.write("<script type='text/javascript'>");
        out.write("function _noenter(event) {"
                + "if (event && event.keyCode == 13) {"
                + "   _searchSubmit();return false; }"
                + "else {"
                + "   return true; } }");
        out.write("function _searchSubmit() {");
        out.write(Utils.generateFormSubmit(context, this, Utils.getActionHiddenFieldName(context, this), getClientId(context)));
        out.write("}");
        out.write("</script>");

        // outer table containing search drop-down icon, text box and search Go image button
        out.write("<table cellspacing='4' cellpadding='0'>");
        out.write("<tr><td style='padding-top:2px'>");

        String searchImage = Utils.buildImageTag(context, "/images/icons/search_icon.gif", 15, 15,
                bundle.getString(MSG_GO), "_searchSubmit();");

        out.write(Utils.buildImageTag(context, "/images/icons/search_controls.gif", 27, 13,
                bundle.getString(MSG_OPTIONS), "javascript:_toggleMenu(event, 'alfsearch_menu');"));

        // dynamic DIV area containing search options
        out.write("<br><div id='alfsearch_menu' style='position:absolute;display:none'>");
        out.write("<table border='0' class='moreActionsMenu' cellspacing='4' cellpadding='0'>");

        // output each option - setting the current one to CHECKED
        String optionFieldName = getClientId(context) + NamingContainer.SEPARATOR_CHAR + OPTION_PARAM;
        String radioOption = "<tr><td class='userInputForm' style='white-space:nowrap;'><input type='radio' name='" + optionFieldName + "'";
        out.write(radioOption);
        out.write(" value='0'");
        int searchMode = getSearchMode();
        if (searchMode == 0) {
            out.write(" checked='checked'");
        }
        out.write("/>" + bundle.getString(MSG_ALL_ITEMS) + "</td></tr>");
        out.write(radioOption);
        out.write(" value='1'");
        if (searchMode == 1) {
            out.write(" checked='checked'");
        }
        out.write("/>" + bundle.getString(MSG_FILE_NAMES_CONTENTS) + "</td></tr>");
        out.write(radioOption);
        out.write(" value='2'");
        if (searchMode == 2) {
            out.write(" checked='checked'");
        }
        out.write("/>" + bundle.getString(MSG_FILE_NAMES_ONLY) + "</td></tr>");
        out.write(radioOption);
        out.write(" value='3'");
        if (searchMode == 3) {
            out.write(" checked='checked'");
        }
        out.write("/>" + bundle.getString(MSG_SPACE_NAMES_ONLY) + "</td></tr>");

        // row with table containing advanced search link and Search Go button
        out.write("<tr><td><table width='100%'><tr><td>");
        // generate a link that will cause an action event to navigate to the advanced search screen
        out.write("<a class='small' href='#' onclick=\"");
        out.write(Utils.generateFormSubmit(context, this, Utils.getActionHiddenFieldName(context, this), ADVSEARCH_PARAM));
        out.write("\">");
        out.write(bundle.getString(MSG_ADVANCED_SEARCH));
        out.write("</a>");
        out.write("</td><td align='right'>");
        out.write(searchImage);
        out.write("</td></tr></table></td></tr>");
        out.write("</table></div>");

        // input text box
        out.write("</td><td>");
        out.write("<input name='");
        out.write(getClientId(context));
        // TODO: style and class from component properties!
        out.write("' onkeypress=\"return _noenter(event)\"");
        out.write(" type='text' maxlength='1024' style='width:130px;padding-top:3px;font-size:10px' value=\"");
        // output previous search text stored in this component!
        out.write(Utils.replace(getLastSearch(), "\"", "&quot;"));
        out.write("\">");

        // search Go image button
        out.write("</td><td>");
        out.write(searchImage);

        // end outer table
        out.write("</td></tr></table>");
    }

    /**
     * @see org.alfresco.web.ui.repo.component.UISimpleSearch#decode(javax.faces.context.FacesContext)
     */
    @Override
    public void decode(FacesContext context) {
        Map requestMap = context.getExternalContext().getRequestParameterMap();
        String fieldId = Utils.getActionHiddenFieldName(context, this);
        String value = (String) requestMap.get(fieldId);
        // we are clicked if the hidden field contained our client id
        if (value != null) {
            if (value.equals(this.getClientId(context))) {
                String searchText = (String) requestMap.get(getClientId(context));
                int option = -1;
                String optionFieldName = getClientId(context) + NamingContainer.SEPARATOR_CHAR + OPTION_PARAM;
                String optionStr = (String)requestMap.get(optionFieldName);
                if (optionStr.length() != 0)
                {
                   option = Integer.parseInt(optionStr);
                }
                if (searchText.length() != 0) {
                    // queue event so system can perform a search and update the component
                    SearchEvent event = new SearchEvent(this, searchText, option);
                    this.queueEvent(event);
                }
            } else if (value.equals(ADVSEARCH_PARAM)) {
                AdvancedSearchEvent event = new AdvancedSearchEvent(this, "dialog:advancedSearch");
                this.queueEvent(event);
            }
        }
    }

}
