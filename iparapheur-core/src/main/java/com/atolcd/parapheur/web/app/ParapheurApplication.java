/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.app;

import java.util.Properties;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.jsf.FacesContextUtils;

import com.atolcd.parapheur.repo.ParapheurService;

public class ParapheurApplication
{
    private static final String BEAN_PARAPHEUR_SERVICE = "parapheurService";
    
    private static String parapheursFolderName;
    private static String savedWorkflowsFolderName;
    private static String archivesFolderName;
    private static String templateSignataireName;

    /**
     * Private constructor to prevent instantiation of this class 
     */
    private ParapheurApplication()
    {
    }

    /**
     * @return Return the Parapheurs folder name
     */
    public static String getParapheursFolderName(ServletContext context)
    {
       return getParapheursFolderName(WebApplicationContextUtils.getRequiredWebApplicationContext(context));
    }
    
    /**
     * @return  Return the Parapheurs folder name
     */
    public static String getParapheursFolderName(FacesContext context)
    {
       return getParapheursFolderName(FacesContextUtils.getRequiredWebApplicationContext(context));
    }

    /**
     * Returns the Parapheurs folder name name
     * 
     * @param context The spring context
     * @return The Parapheurs folder name
     */
    private static String getParapheursFolderName(WebApplicationContext context)
    {
       if (parapheursFolderName == null)
       {
          ParapheurService parapheurService = (ParapheurService)context.getBean(BEAN_PARAPHEUR_SERVICE);
          Properties configuration = parapheurService.getConfiguration();
          parapheursFolderName = configuration.getProperty("spaces.parapheurs.childname");
       }
       
       return parapheursFolderName;
    }

    /**
     * @return Return the Saved Workflows folder name
     */
    public static String getSavedWorkflowsFolderName(ServletContext context)
    {
       return getSavedWorkflowsFolderName(WebApplicationContextUtils.getRequiredWebApplicationContext(context));
    }
    
    /**
     * @return  Return the Saved Workflows folder name
     */
    public static String getSavedWorkflowsFolderName(FacesContext context)
    {
       return getSavedWorkflowsFolderName(FacesContextUtils.getRequiredWebApplicationContext(context));
    }

    /**
     * Returns the Saved Workflows folder name name
     * 
     * @param context The spring context
     * @return The Parapheurs folder name
     */
    private static String getSavedWorkflowsFolderName(WebApplicationContext context)
    {
       if (savedWorkflowsFolderName == null)
       {
      	 ParapheurService parapheurService = (ParapheurService)context.getBean(BEAN_PARAPHEUR_SERVICE);
          Properties configuration = parapheurService.getConfiguration();
          savedWorkflowsFolderName = configuration.getProperty("spaces.savedworkflows.childname");
       }
       
       return savedWorkflowsFolderName;
    }
    
    /**
     * @return Return the Archives folder name
     */
    public static String getArchivesFolderName(ServletContext context)
    {
       return getArchivesFolderName(WebApplicationContextUtils.getRequiredWebApplicationContext(context));
    }
    
    /**
     * @return  Return the Archives folder name
     */
    public static String getArchivesFolderName(FacesContext context)
    {
       return getArchivesFolderName(FacesContextUtils.getRequiredWebApplicationContext(context));
    }

    /**
     * Returns the Archives folder name
     * 
     * @param context The spring context
     * @return The Archives folder name
     */
    private static String getArchivesFolderName(WebApplicationContext context)
    {
       if (archivesFolderName == null)
       {
      	 ParapheurService parapheurService = (ParapheurService)context.getBean(BEAN_PARAPHEUR_SERVICE);
          Properties configuration = parapheurService.getConfiguration();
          archivesFolderName = configuration.getProperty("spaces.archives.childname");
       }
       
       return archivesFolderName;
    }
    
    /**
     * @return Return the template "signataires" file name
     */
    public static String getTemplateSignataireName(ServletContext context)
    {
       return getTemplateSignataireName(WebApplicationContextUtils.getRequiredWebApplicationContext(context));
    }
    
    /**
     * @return Return the template "signataires" file name
     */
    public static String getTemplateSignataireName(FacesContext context)
    {
       return getTemplateSignataireName(FacesContextUtils.getRequiredWebApplicationContext(context));
    }

    /**
     * @return Return the template "signataires" file name
     * 
     * @param context The spring context
     * @return The template file name
     */
    private static String getTemplateSignataireName(WebApplicationContext context)
    {
       if (templateSignataireName == null)
       {
      	 ParapheurService parapheurService = (ParapheurService)context.getBean(BEAN_PARAPHEUR_SERVICE);
          Properties configuration = parapheurService.getConfiguration();
          templateSignataireName = configuration.getProperty("templates.signataires.childname");
       }
       
       return templateSignataireName;
    }
}
