/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.web.servlet;

import com.atolcd.parapheur.repo.ParapheurService;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.web.app.servlet.AuthenticationStatus;
import org.alfresco.web.app.servlet.BaseServlet;
import org.alfresco.web.bean.repository.Repository;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This Servlet generates the print preview for the given dossier, and sends it
 * to the client.
 * 
 * This can usually be done using a JSF Managed Bean extracting the
 * HttpServletRequest and sending raw data to the client, but this technique
 * breaks the Alfresco navigation scheme.
 *
 * @author Vivien Barousse
 */
//public class PrintServlet extends HttpServlet {
public class PrintServlet extends BaseServlet {

    private static Logger logger = Logger.getLogger(PrintServlet.class);

    private NodeService nodeService;

    private ParapheurService parapheurService;

    private static final Pattern URL_PATTERN = Pattern.compile("/([a-zA-Z0-9-]+)/(.*)$");

    @Override
    public void init() throws ServletException {
        WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        nodeService = (NodeService) applicationContext.getBean("NodeService");
        parapheurService = (ParapheurService) applicationContext.getBean("ParapheurService");
    }

    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
        Matcher urlMatcher = URL_PATTERN.matcher(req.getPathInfo());
        logger.debug(req.getPathInfo());

        if (!urlMatcher.matches()) {
            res.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalid URL");
            return;
        }
        
        /**
         * Do authenticate !
         */
        if (logger.isDebugEnabled()) {
            String queryString = req.getQueryString();
            logger.debug("Authenticating (GET) request to URL: " + req.getRequestURI() +
                    ((queryString != null && queryString.length() > 0) ? ("?" + queryString) : ""));
        }
        AuthenticationStatus status = servletAuthenticate(req, res);
        if (status == AuthenticationStatus.Failure) {
            return;
        }

        final String refStr = urlMatcher.group(1);
        String docName = urlMatcher.group(2);

        ServiceRegistry serviceRegistry = getServiceRegistry(getServletContext());
        TransactionService transactionService = serviceRegistry.getTransactionService();
        RetryingTransactionCallback<Void> processCallback = new RetryingTransactionCallback<Void>()
        {
            @Override
            public Void execute() throws Throwable {
                doProceedThePrint(req, res, refStr, null, true);
                return null;
            }
        };
        transactionService.getRetryingTransactionHelper().doInTransaction(processCallback, false);
    }

    protected void doProceedThePrint(HttpServletRequest req, HttpServletResponse res, String refStr, List<NodeRef> documents, boolean includeFirstPage)
            throws ServletException, IOException, Exception  {
        NodeRef dossier = new NodeRef(Repository.getStoreRef(), refStr);

        if (!nodeService.exists(dossier)) {
            res.sendError(HttpServletResponse.SC_NOT_FOUND, "Node ref doesn't exists");
            return;
        }

        res.setContentType(MimetypeMap.MIMETYPE_PDF);
    //        resp.setHeader("Content-disposition", "attachment; filename=" + URLEncoder.encode(docName, "UTF-8"));

        File printContent = parapheurService.genererDossierPDF(dossier,
                false,
                (documents == null)? parapheurService.getAttachments(dossier) : documents,
                includeFirstPage);
        logger.debug(printContent.length() + " bytes to send");

        FileInputStream fis = new FileInputStream(printContent);

        // Buffered streams are used for performance reasons.
        BufferedInputStream in = new BufferedInputStream(fis);
        BufferedOutputStream out = new BufferedOutputStream(res.getOutputStream());

        try {
            // Balance la sauce
            byte[] buffer = new byte[2048];
            int readed;
            while ((readed = in.read(buffer)) >= 0) {
                out.write(buffer, 0, readed);
                // System.out.println("Writing " + readed + " bytes");
            }
            out.flush();
        } finally {
            in.close();
            fis.close();
            out.close();
        }
    }
    
    /**
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
        
        AuthenticationStatus status = servletAuthenticate(req, res);
        if (status == AuthenticationStatus.Failure) {
            return;
        }
        
        final List<NodeRef> documents = new ArrayList<NodeRef>();
        String[] attachments = req.getParameterValues("attachments");
        if (attachments != null) {
            for (String attachment : attachments) {
                documents.add(new NodeRef(attachment));
            }
        }
        final String refStr = req.getParameter("dossierId");

        String includeFirstPageStr = req.getParameter("includeFirstPage");
        // Si pas d'option, on imprime le bordereau de signature.
        final boolean includeFirstPage = (includeFirstPageStr == null)? true : Boolean.valueOf(includeFirstPageStr);

        ServiceRegistry serviceRegistry = getServiceRegistry(getServletContext());
        TransactionService transactionService = serviceRegistry.getTransactionService();
        RetryingTransactionCallback<Void> processCallback = new RetryingTransactionCallback<Void>()
        {
            @Override
            public Void execute() throws Throwable {
                doProceedThePrint(req, res, refStr, documents, includeFirstPage);
                return null;
            }
        };
        transactionService.getRetryingTransactionHelper().doInTransaction(processCallback, false);
    }

}
