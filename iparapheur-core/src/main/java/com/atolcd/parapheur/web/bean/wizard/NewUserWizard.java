/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.bean.wizard;

import java.util.List;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.servlet.FacesHelper;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.ui.common.Utils;

import com.atolcd.parapheur.web.app.ParapheurApplication;


public class NewUserWizard extends org.alfresco.web.bean.wizard.NewUserWizard
{
   /**
    * @see org.alfresco.web.bean.wizard.NewUserWizard#init()
    */
   @Override
   public void init()
   {
      super.init();
      this.setHomeSpaceLocation(null);
   }

   /**
    * @see org.alfresco.web.bean.wizard.NewUserWizard#finish()
    */
   @Override
   public String finish()
   {
      //Avant de créer, faire check sur l'existence pour éviter les doublons...
      FacesContext fc = FacesContext.getCurrentInstance();
      PersonService personService = (PersonService)FacesHelper.getManagedBean(fc, "PersonService");
       //si utilisateur existe, alors message d'erreur et return null
      if (personService.personExists(this.getUserName()) && !this.isInEditMode())
      {
	  Utils.addErrorMessage(Application.getMessage(fc, "Création impossible: Utilisateur déjà existant."));
	  return null;
      }
       
      String res = super.finish();
      
      NamespaceService namespaceService = (NamespaceService)FacesHelper.getManagedBean(fc, "NamespaceService");
      String XPath =  Application.getRootPath(fc) + "/" +
                             ParapheurApplication.getParapheursFolderName(fc);
      
      
      NodeRef rootNodeRef = this.getNodeService().getRootNode(Repository.getStoreRef());
      List<NodeRef> nodes = this.getSearchService().selectNodes(rootNodeRef, XPath, null, namespaceService, false);
      
      if (nodes.size() == 0)
      {
         throw new IllegalStateException("Impossible de trouver l'espace de stockage des parapheurs: " + XPath);
      }
      
      this.getNodeService().setProperty( personService.getPerson(this.getUserName()), ContentModel.PROP_HOMEFOLDER, nodes.get(0));
      
      return res;
   }
   
   /**
    * @return Returns the summary data for the wizard.
    */
   @Override
   public String getSummary()
   {
      ResourceBundle bundle = Application.getBundle(FacesContext.getCurrentInstance());

      return buildSummary(new String[] { bundle.getString("name"), bundle.getString("username"), bundle.getString("password") }, 
                          new String[] { this.getFirstName() + " " + this.getLastName(), this.getUserName(), "********" });
   }
}
