/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.archiland.wsmodel._1.ActeDescription;
import com.atolcd.archiland.wsmodel._1.Document;
import com.atolcd.archiland.wsmodel._1.TypeActe;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.transaction.UserTransaction;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.adullact.iparapheur.repo.ged.ArchilandConnector;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Repository;


public class ArchiveDialog extends BaseDialogBean {

    private String archiveMode;

    /* Archivage Parapheur */
    private String archiveName;
    private NodeRef dossierRef;
    private String dossierName;
    /* Commun a Actes et Finance */
    private String collectivite;
    private String service;
    private String nom;

    /* specifique Actes */
    private String typeActe;
    private String intitule;
    private Date dateDebutDUA;
    private Date dateSeance;

    /* Spécifique Finance */
    private int budget;
    private String typeFinance;
    private Date dateRecuperation;
    private Date dateGeneration;
    private Date dateFinance;

    private boolean gedModeEnabled;
    private ParapheurService parapheurService;
    private ContentService contentService;
    private ArchilandConnector archilandConnector;
    /* Internals */
    private String tdtProtocol;

    private String store;

    private List<SelectItem> services;
    private List<SelectItem> intitules;
    
    private List<NodeRef> selectedAttachments;
    private List<SelectItem> attachments;
    private boolean showAttachments;

    public ArchilandConnector getArchilandConnector() {
        return archilandConnector;
    }

    public void setArchilandConnector(ArchilandConnector archilandConnector) {
        this.archilandConnector = archilandConnector;
    }

    public List<SelectItem> getServices() {
        return services;
    }

    public void setServices(List<SelectItem> services) {
        this.services = services;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public ContentService getContentService() {
        return contentService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    /**
     * @return Returns the archiveName.
     */
    public String getArchiveName() {
        return archiveName;
    }

    /**
     * @param archiveName The archiveName to set.
     */
    public void setArchiveName(String archiveName) {
        this.archiveName = archiveName;
    }

    /**
     * @return Returns the dossierRef.
     */
    public NodeRef getDossierRef() {
        return dossierRef;
    }

    /**
     * @param dossierRef The dossierRef to set.
     */
    public void setDossierRef(NodeRef dossierRef) {
        this.dossierRef = dossierRef;
    }

    /**
     * @return Returns the dossierName.
     */
    public String getDossierName() {
        return dossierName;
    }

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public String getArchiveMode() {
        return archiveMode;
    }

    public void setArchiveMode(String archiveMode) {
        this.archiveMode = archiveMode;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String getCollectivite() {
        return collectivite;
    }

    public void setCollectivite(String collectivite) {
        this.collectivite = collectivite;
    }

    public Date getDateDebutDUA() {
        return dateDebutDUA;
    }

    public void setDateDebutDUA(Date dateDebutDUA) {
        this.dateDebutDUA = dateDebutDUA;
    }

    public Date getDateFinance() {
        return dateFinance;
    }

    public void setDateFinance(Date dateFinance) {
        this.dateFinance = dateFinance;
    }

    public Date getDateGeneration() {
        return dateGeneration;
    }

    public void setDateGeneration(Date dateGeneration) {
        this.dateGeneration = dateGeneration;
    }

    public Date getDateRecuperation() {
        return dateRecuperation;
    }

    public void setDateRecuperation(Date dateRecuperation) {
        this.dateRecuperation = dateRecuperation;
    }

    public Date getDateSeance() {
        return dateSeance;
    }

    public void setDateSeance(Date dateSeance) {
        this.dateSeance = dateSeance;
    }

    public String getIntitule() {
        return intitule;
    }
    
    public List<SelectItem> getIntitules() {
        // dans certain cas (mal identifies) :  NPE possible sur archive-dialog.jsf
        if (intitules==null) {
            this.intitules = new ArrayList<SelectItem>();
            this.intitules.add(new SelectItem("", ""));
        }
        return intitules;        
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getTdtProtocol() {
        return tdtProtocol;
    }

    public void setTdtProtocol(String tdtProtocol) {
        this.tdtProtocol = tdtProtocol;
    }

    public String getTypeActe() {
        return typeActe;
    }

    public void setTypeActe(String typeActe) {
        this.typeActe = typeActe;
    }

    public String getTypeFinance() {
        return typeFinance;
    }

    public void setTypeFinance(String typeFinance) {
        this.typeFinance = typeFinance;
    }

    public boolean getGedModeEnabled() {
        return gedModeEnabled;
    }

    public void setGedModeEnabled(boolean gedModeEnabled) {
        this.gedModeEnabled = gedModeEnabled;
    }
    
    public List<SelectItem> getAttachments() {
        return attachments;
    }
    
    public List<NodeRef> getSelectedAttachments() {
        return this.selectedAttachments;
    }
    
    public void setSelectedAttachments(List<NodeRef> selectedAttachments) {
        this.selectedAttachments = selectedAttachments;
    }

    public boolean getShowAttachments() {
        return showAttachments;
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#init(java.util.Map)
     */
    @Override
    public void init(Map<String, String> parameters) {
        super.init(parameters);

        if (this.parameters.containsKey("id")) {
            dossierRef = new NodeRef(Repository.getStoreRef(), this.parameters.get("id"));
        } else {
            dossierRef = this.navigator.getCurrentNode().getNodeRef();
        }

        this.tdtProtocol = getTDTProtocolForNode(dossierRef);

        if (!this.parapheurService.isDossier(dossierRef)) {
            throw new AlfrescoRuntimeException("ArchiveDialog called on a non-dossier element");
        }

        this.dossierName = (String) getNodeService().getProperty(dossierRef, ContentModel.PROP_TITLE);
        this.archiveName = this.dossierName + ".pdf";

        this.nom = this.dossierName;
        this.service = "";
        this.intitule = "";
        this.gedModeEnabled = false;
        
        this.showAttachments = false;
        this.attachments = new ArrayList<SelectItem>();
        this.selectedAttachments = new ArrayList<NodeRef>();
        List<NodeRef> documents = parapheurService.getAttachments(dossierRef);
        if ((documents != null) && !documents.isEmpty()) {
            this.showAttachments = true;
            for (NodeRef document : documents) { 
                this.attachments.add(new SelectItem(document, (String) getNodeService().getProperty(document, ContentModel.PROP_NAME)));
            }
            if (parapheurService.areAttachmentsIncluded(dossierRef)) {
                selectedAttachments = documents;
            }
        }

        UserTransaction tx = this.getTransactionService().getUserTransaction();

        try {
            tx.begin();
            this.collectivite = (String) archilandConnector.getArchilandConfig().get("collectivite");
            this.services = (List<SelectItem>) archilandConnector.getArchilandConfig().get("services");
            String enabled = (String)archilandConnector.getArchilandConfig().get("enabled");
            // warn: the string 'enabled' may be null.
            this.gedModeEnabled = "true".equalsIgnoreCase(enabled);
            
            if (this.gedModeEnabled) {
                List<String> classification = archilandConnector.getClassification();
                this.intitules = new ArrayList<SelectItem>();
                // ajout d'un item vide même si ce n'est pas
                this.intitules.add(new SelectItem("", ""));
                for (String _intitule : classification) {
                    this.intitules.add(new SelectItem(_intitule, _intitule));
                }
            }
            
            tx.commit();
        } catch (Exception e) {
            try {
                tx.rollback();
            } catch (Exception e2) {
                //System.out.println("exception e2");
            }
            
           System.out.println("Erreur lors de l'initialisation archiland");
           e.printStackTrace();
        }

        if (this.tdtProtocol.equals("ACTES")) {
            ArchilandConnector.ActesMetaData data = archilandConnector.getActeDataForNodeRef(dossierRef);
            this.dateSeance = data.date;
            if (data.nature != null) {
                this.typeActe = archilandConnector.getTypeDocumentFromS2LowNature(data.nature).value();
            }
            else {
                this.typeActe=null;
            }
        }
        
        this.archiveMode  = "1";
        this.dateDebutDUA = null;
        
    }

    private String getTDTProtocolForNode(NodeRef nodeRef) {
        String retVal;
        retVal = (String) getNodeService().getProperty(nodeRef, ParapheurModel.PROP_TDT_PROTOCOLE);
        retVal = retVal == null ? "" : retVal;
        return retVal;
    }

    protected void archiveiParapheur() {
        try {
            this.parapheurService.archiver(this.dossierRef, this.archiveName, this.selectedAttachments);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void archiveGEDActes() {
        // checks
        if (dateDebutDUA == null)
            throw new RuntimeException("Veuillez spécifier une date de debut de DUA.");
        if (dateSeance == null)
            throw new RuntimeException("Veuillez spécifier une date de seance.");
        if (nom.length() == 0)
            throw new RuntimeException("Veuillez spécifier un nom pour l'archive.");
        if (intitule.length() == 0) {
            throw new RuntimeException("Veuillez sélectionner un intiutlé dans la liste.");
        }

        try {
            
            ActeDescription acte = new ActeDescription();
            List<Document> documents = archilandConnector.getDocumentsForNode(dossierRef, typeActe);
            
            
            System.out.println("SizeOf Documents = " + documents.size());

            acte.setCollectivite(collectivite);
            acte.setIntitule(intitule);
            if (service != null && !service.isEmpty()) {
                acte.setService(service);
            }
            acte.setNom(nom);
            acte.setType(TypeActe.fromValue(typeActe));
            acte.getFichiers().addAll(documents);

            GregorianCalendar gCalendar = new GregorianCalendar();
            gCalendar.setTime(dateDebutDUA);
            
            XMLGregorianCalendar xmlDatedebutDUA = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
            gCalendar.setTime(dateSeance);

            XMLGregorianCalendar xmlDateSeance = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
            acte.setDateDebutDUA(xmlDatedebutDUA);
            acte.setDateSeance(xmlDateSeance);

           

            archilandConnector.verserActe(acte);
            //versement.deposeFichierActesEnGED(acte);
            
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(ArchiveDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
        archiveiParapheur();
    }

    protected void archiveGEDFinance() {
        //TODO: Implementer le versement de flux financiers dans la ged
    }

   

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#finishImpl(javax.faces.context.FacesContext, java.lang.String)
     */
    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Exception {
        NodeRef corbeilleRef = null;
        try {
            corbeilleRef = this.parapheurService.getParentCorbeille(this.dossierRef);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        if (archiveMode.equals("1")) {
            archiveiParapheur();
        } else {
            if (tdtProtocol.equals("HELIOS")) {
                archiveGEDFinance();
            } else {
                archiveGEDActes();
            }
        }

        if (corbeilleRef != null) {
            this.browseBean.updateUILocation(corbeilleRef);
        }

        return null;
    }

    @Override
    public boolean getFinishButtonDisabled() {
        return false;
    }
}
