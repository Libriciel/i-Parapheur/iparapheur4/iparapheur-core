/*
 * Version 3.1
 * CeCILL Copyright (c) 2010, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.web.bean.SearchResultsBean;
import java.util.Map;
import javax.faces.context.FacesContext;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.bean.search.SearchContext;

/**
 * Fonctionnalité de recherche avancée
 * 
 * @author Vivien Barousse
 */
public class AdvancedSearchDialog extends BaseDialogBean {

    private SearchResultsBean resultsBean;

    private String searchFor;

    private String type;

    private String mimeType;

    private String dossierName;

    private Map<String, String> customProperties;

    @Override
    protected String finishImpl(FacesContext fc, String string) throws Throwable {
        throw new UnsupportedOperationException("Can't be finished");
    }

    @Override
    public void init(Map<String, String> parameters) {
        super.init(parameters);
    }

    public String search() {
        SearchContext context = new SearchContext();

        context.setText(searchFor);

        if ("ph:archive".equals(type)) {
            context.setContentType("ph:archive");
        } else if ("ph:dossier".equals(type)) {
            context.setContentType("cm:content");
            context.setFolderType("ph:dossier");
        }

        if (mimeType != null && !mimeType.isEmpty()) {
            context.setMimeType(mimeType);
        }

        if (dossierName != null && !dossierName.isEmpty()) {
            context.addAttributeQuery(ContentModel.PROP_NAME, dossierName);
        }

        SearchParameters params = new SearchParameters();
        params.setLanguage(SearchService.LANGUAGE_LUCENE);
        params.setQuery(context.buildQuery(BrowseBean.getMinimumSearchLength()));
        params.addStore(Repository.getStoreRef());

        resultsBean.setSearchParameters(params);

        return "search";
    }

    public Map<String, String> getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(Map<String, String> customProperties) {
        this.customProperties = customProperties;
    }

    public String getDossierName() {
        return dossierName;
    }

    public void setDossierName(String dossierName) {
        this.dossierName = dossierName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getSearchFor() {
        return searchFor;
    }

    public void setSearchFor(String searchFor) {
        this.searchFor = searchFor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setResultsBean(SearchResultsBean resultsBean) {
        this.resultsBean = resultsBean;
    }

}
