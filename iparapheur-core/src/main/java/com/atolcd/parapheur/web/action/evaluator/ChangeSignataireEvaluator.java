package com.atolcd.parapheur.web.action.evaluator;

import com.atolcd.parapheur.repo.EtapeCircuit;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.repository.Node;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 10/10/12
 * Time: 10:56
 * To change this template use File | Settings | File Templates.
 */
public class ChangeSignataireEvaluator extends BlindedActionEvaluator {

    @Override
    protected boolean evaluateImpl(Node node, NodeRef nodeRef) {

        EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(nodeRef);

        if (etapeCourante.getActionDemandee().equals(EtapeCircuit.ETAPE_SIGNATURE) && !parapheurService.isRejete(nodeRef)) {
            if (isOwner(AuthenticationUtil.getRunAsUser(), nodeRef)) {
                return true;
            }
        }

        return false;
    }
}


