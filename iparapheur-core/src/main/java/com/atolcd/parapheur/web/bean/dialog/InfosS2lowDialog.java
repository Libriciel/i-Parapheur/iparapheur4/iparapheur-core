/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.bean.dialog;

import java.util.Map;
import javax.faces.context.FacesContext;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.app.Application;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Repository;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;

public class InfosS2lowDialog extends BaseDialogBean
{
   private NodeRef dossierRef;
   private String statut;
   private ParapheurService parapheurService;
   protected S2lowService s2lowService;
   private NodeService nodeService;
   
   private String nomDossier;
   private String transactionDossier;
   
   /**
    * @return Returns the statut.
    */
   public String getStatut()
   {
      return statut;
   }

   /**
    * @param statut The statut to set.
    */
   public void setStatut(String statut)
   {
      this.statut = statut;
   }

   /**
    * @param parapheurService The parapheurService to set.
    */
   public void setParapheurService(ParapheurService parapheurService)
   {
      this.parapheurService = parapheurService;
   }

   /**
    * @param service the s2lowService to set
    */
   public void setS2lowService(S2lowService service)
   {
      s2lowService = service;
   }

   /**
    * @param nodeService The nodeService to set.
    */
   public void setNodeService(NodeService nodeService)
   {
      this.nodeService = nodeService;
   }
   
   public String getNomDossier()
   {
      if (this.nomDossier == null)
      {
         this.nomDossier = (String)this.nodeService.getProperty(this.dossierRef, ContentModel.PROP_TITLE);
      }
      
      return this.nomDossier;
   }
   
   public String getTransactionDossier()
   {
      if (this.transactionDossier == null)
      {
         this.transactionDossier = ((Integer)this.nodeService.getProperty(this.dossierRef, ParapheurModel.PROP_TRANSACTION_ID)).toString();
      }
      
      return this.transactionDossier;
   }

   /**
    * @see org.alfresco.web.bean.dialog.BaseDialogBean#init(java.util.Map)
    */
   @Override
   public void init(Map<String, String> arg0)
   {
      super.init(arg0);
      if(this.parameters.containsKey("id"))
      {
         dossierRef = new NodeRef(Repository.getStoreRef(),this.parameters.get("id"));
      }
      else
      {
         dossierRef = this.navigator.getCurrentNode().getNodeRef();
      }
      
      if (!this.parapheurService.isDossier(dossierRef))
         throw new AlfrescoRuntimeException("InfosS2lowDialog called on a non-dossier element");
      if (!this.nodeService.hasAspect(dossierRef,ParapheurModel.ASPECT_S2LOW))
         throw new AlfrescoRuntimeException("InfosS2lowDialog called on a non-sent dossier element");
      
      this.statut = (String)this.nodeService.getProperty(dossierRef, ParapheurModel.PROP_STATUS);
      
      if (this.statut == null)
      {
        this.statut = Application.getMessage(FacesContext.getCurrentInstance(),"s2low_no_infos");
      }
   }

    /**
     * This dialog isn't finishable.
     *
     * The finish button corresponding to this dialog should be hidden, so this method
     * shouldn't be invoked by the dialog container.
     *
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#finishImpl(javax.faces.context.FacesContext, java.lang.String)
     * @throws UnsupportedOperationException when invoked
     */
    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Exception {
        throw new UnsupportedOperationException("Dialog isn't finishable");
    }

   /**
    * Update S2low Status
    */
    public void updateStatus() {
        try {
            this.statut = this.s2lowService.statutS2lowToString(this.s2lowService.getInfosS2low(dossierRef));
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

   /**
    * @see org.alfresco.web.bean.dialog.BaseDialogBean#getCancelButtonLabel()
    */
   @Override
   public String getCancelButtonLabel()
   {
      return Application.getMessage(FacesContext.getCurrentInstance(),"s2low_fermer");
   }

}
