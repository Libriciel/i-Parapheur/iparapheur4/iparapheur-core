package com.atolcd.parapheur.web.action.evaluator;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.repository.Node;

public class SecureMailEvaluator extends BlindedActionEvaluator {
    @Override
    protected boolean evaluateImpl(Node node, NodeRef nodeRef) {
        Boolean result = false;
        EtapeCircuit currentStep = parapheurService.getCurrentEtapeCircuit(nodeRef);
        if  (!parapheurService.isRejete(nodeRef) && EtapeCircuitImpl.ETAPE_MAILSEC.equals(currentStep.getActionDemandee())) {
            // verifier l'absence de PROP_MAILSEC_MAIL_ID
            result = nodeService.getProperty(nodeRef, ParapheurModel.PROP_MAILSEC_MAIL_ID) == null;
        }
        return result;
    }
}
