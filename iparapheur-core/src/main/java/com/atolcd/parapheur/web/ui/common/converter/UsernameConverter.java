/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.ui.common.converter;

import java.util.ArrayList;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.web.app.servlet.FacesHelper;

public class UsernameConverter implements Converter
{
   /**
    * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
    */
    @Override
   public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException
   {
      if (value != null)
         return getName(value,context);
      else
         return null;
   }

   /**
    * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
    */
    @Override
   public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException
   {
      return getName(value, context);
   }
   
   private String getName(String username, FacesContext context)
   {
      String res = null;
      
      if (username != null && !username.equals(""))
      {
         NodeService nodeService = (NodeService)FacesHelper.getManagedBean(context, "NodeService");
         PersonService personService = (PersonService)FacesHelper.getManagedBean(context, "PersonService");
         
         NodeRef proprietaireRef = personService.getPerson(username);
         res = (String)nodeService.getProperty(proprietaireRef,ContentModel.PROP_FIRSTNAME) + " " + 
               (String)nodeService.getProperty(proprietaireRef,ContentModel.PROP_LASTNAME);
      }
      
      return res;
   }
   
   @SuppressWarnings("unchecked")
   private String getName(Object username, FacesContext context)
   {
      if (username == null)
         return null;
      
      String className = username.getClass().getName();
      
      if ("java.lang.String".equals(className))
         return getName(username.toString(), context);
      else
      {
         if ("java.util.ArrayList".equals(className))
         {
            StringBuilder res = new StringBuilder("");
            NodeService nodeService = (NodeService)FacesHelper.getManagedBean(context, "NodeService");
            PersonService personService = (PersonService)FacesHelper.getManagedBean(context, "PersonService");
            
            ArrayList<String> usernames = (ArrayList<String>)username;
            for (String tmpUsername : usernames)
            {
               if (tmpUsername != null && !tmpUsername.equals(""))
               {
                  NodeRef proprietaireRef = personService.getPerson(tmpUsername);
                  String nom = (String)nodeService.getProperty(proprietaireRef,ContentModel.PROP_FIRSTNAME) + " " + 
                               (String)nodeService.getProperty(proprietaireRef,ContentModel.PROP_LASTNAME);
                  
                  res.append(nom)
                          .append("\n");
               }
            }
            
            return res.toString();
         }
         else
            return null;
      }
   }
}
