package com.atolcd.parapheur.web.bean.wizard.batch;
/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * contact@atolcd.com
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

/**
 * CakeFilter
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 * Date: 11/06/12 Time: 10:16
 */
import com.atolcd.parapheur.model.ParapheurModel;
import java.util.ArrayList;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.repository.Repository;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Dict Format "and" =>
 */
public class CakeFilter {

    Pattern dateRangePattern = Pattern.compile("^\\[ *[^ ]+ +TO +.+\\ *]$");
    String searchPath;
    String type;
    String aspect;
    boolean includeAspect;
    NodeRef parent;
    Map<NodeRef, QName> parents;
    QName parentType;
    Predicate<String, List<Predicate<?, ?>>> clause;

    public CakeFilter() {
        this.parent = null;
        this.aspect = null;
    }

    public String generateClauseString(Predicate<String, List<Predicate<?, ?>>> clause) {
        String retVal = "";
        String operator = "";
        if (clause.first().equalsIgnoreCase("and")) {
            operator = "AND";
        } else if (clause.first().equalsIgnoreCase("or")) {
            operator = "OR";
        } else {
            throw new RuntimeException("le type de clause: " + clause.first() + "est inconnu");
        }

        retVal += " (";


        boolean first = true;

        Iterator<Predicate<?, ?>> it = clause.second().iterator();
        while (it.hasNext()) {
            Predicate<?, ?> predicate = it.next();

            if (predicate.first() instanceof QName) {

                Matcher m = dateRangePattern.matcher((String) predicate.second());
                if (m.matches()) {
                    retVal += "@" + Repository.escapeQName((QName) predicate.first()) + ":" + (String) predicate.second();
                } else {
                    retVal += "@" + Repository.escapeQName((QName) predicate.first()) + ":\"" + luceneEscapeString((String) predicate.second()) + "\"";
                }


            } else if (predicate.first() instanceof String) {
                retVal += generateClauseString((Predicate<String, List<Predicate<?, ?>>>) predicate);
            }

            if (it.hasNext()) {
                retVal += String.format(" %s ", operator);
            }


        }

        retVal += ")";

        return retVal;

    }

    public String generateHeader() {
        String retVal = "";
        if (searchPath != null) {
            retVal += String.format("+PATH:\"%s\" ", searchPath);
        }

        if (parent != null && parentType != null) {
            String field = "";
            if (parentType.equals(ParapheurModel.TYPE_CORBEILLE)) {
                field = "PARENT";
            } else if (parentType.equals(ParapheurModel.TYPE_CORBEILLE_VIRTUELLE)) {
                field = "VIRTUALPARENT";
            } else {
                throw new RuntimeException(String.format("Type de parent non supporté: %s", parentType.toPrefixString()));
            }
            retVal += String.format("+%s:\"%s\" ", field, parent.toString());
        } else if (parents != null) {
            String fparent = "";
            List<String> parentList = new ArrayList<String>();
            String fvparent = "";
            List<String> vparentList = new ArrayList<String>();
            for (Map.Entry<NodeRef, QName> entry : parents.entrySet()) {
                NodeRef nodeRef = entry.getKey();
                QName qName = entry.getValue();
                if (qName.equals(ParapheurModel.TYPE_CORBEILLE)) {
                    parentList.add(nodeRef.toString());
                } else if (qName.equals(ParapheurModel.TYPE_CORBEILLE_VIRTUELLE)) {
                    vparentList.add(nodeRef.toString());
                }
            }
            if (!vparentList.isEmpty()) {
                fvparent = "VIRTUALPARENT:(";
                for (int i = 0; i < vparentList.size(); i++) {
                    if (i != 0) {
                        fvparent += " OR ";
                    }
                    fvparent += "\"" + vparentList.get(i) + "\"";
                }
                fvparent += ")";
            }
            if (!parentList.isEmpty()) {
                fparent = "PARENT:(";
                for (int i = 0; i < parentList.size(); i++) {
                    if (i != 0) {
                        fparent += " OR ";
                    }
                    fparent += "\"" + parentList.get(i) + "\"";
                }
                fparent += ")";
            }
            retVal += "+(" + fvparent + " OR " + fparent + ") ";
        }

        if(aspect != null) {
            retVal += (includeAspect ? "+" : "-") + "ASPECT:\""+aspect+"\" ";
        }


        if (type != null) {
            retVal += String.format("+TYPE:\"%s\" ", type);
        }
        /*
         if (multipleTypes.size() > 0) {
         for (int i = 0; i < multipleTypes.size() - 1; i++) {
         retVal += String.format("(+TYPE:\"%s\" OR ", multipleTypes.get(0));
         }
         retVal += String.format("+TYPE:\"%s\") ", multipleTypes.get(multipleTypes.size() - 1));
         }*/

        return retVal;
    }

    public void setAspect(String aspect, boolean include) {
        this.aspect = aspect;
        this.includeAspect = include;
    }

    public String buildFilterQuery() {
        String retVal = "";
        retVal += generateHeader();

        if (clause != null && clause.second() != null) {
            retVal += " AND " + generateClauseString(clause);
        }

        return retVal;
    }

    public static String luceneEscapeString(String toEscape) {
        String escaped = LUCENE_PATTERN.matcher(toEscape).replaceAll(REPLACEMENT_STRING);
        return escaped;
    }
    private static final String LUCENE_ESCAPE_CHARS = "[\\.\\\\+\\-\\!\\(\\)\\:\\^\\]\\{\\}\\~\\?\\_\\ \\'\\/\\[]";
    private static final Pattern LUCENE_PATTERN = Pattern.compile(LUCENE_ESCAPE_CHARS);
    private static final String REPLACEMENT_STRING = "\\\\$0";

    public void setSearchPath(String searchPath) {
        this.searchPath = searchPath;
    }

    public void setParent(NodeRef parent) {
        this.parent = parent;
    }

    public void setParents(Map<NodeRef, QName> parents) {
        this.parents = parents;
    }

    public void setParentType(QName parentType) {
        this.parentType = parentType;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setClause(Predicate<String, List<Predicate<?, ?>>> clause) {
        this.clause = clause;
    }
}
