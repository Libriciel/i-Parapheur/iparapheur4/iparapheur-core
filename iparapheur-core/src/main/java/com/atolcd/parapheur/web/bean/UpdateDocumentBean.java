/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package com.atolcd.parapheur.web.bean;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;

import java.util.List;
import javax.faces.context.FacesContext;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.web.bean.coci.CCUpdateFileDialog;
import org.alfresco.web.bean.repository.Repository;

/**
 * Custom JSF bean to update Alfresco documents.
 * <p/>
 * This bean extends the default Alfresco CCUpdateFileDialog for traditional content
 * update.
 * <p/>
 * On document update, the old document name is updated to the new file name.
 *
 * @author Vivien Barousse
 */
public class UpdateDocumentBean extends CCUpdateFileDialog {

    private ContentService contentService;
    private ParapheurService parapheurService;

    private NamespaceService namespaceService;
    private NodeService nodeService;
    private SearchService searchService;

    public ParapheurService getParapheurService() {
        return parapheurService;
    }
    /*
    @Override
    public void init(Map<String, String> parameters) {
        super.init(parameters);
        
    }*/


    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    @Override
    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    @Override
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @Override
    protected NodeService getNodeService() {
        if (this.nodeService == null) {
            this.nodeService = Repository.getServiceRegistry(FacesContext.getCurrentInstance()).getNodeService();
        }
        return this.nodeService;
    }

    @Override
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    @Override
    public String getFileUploadSuccessMsg() {
        // get the file upload message
        String msg = " ajouté avec succès";
        return getFileName() + msg;
    }

    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Exception {
        NodeRef document = property.getDocument().getNodeRef();
        NodeService daNodeService = getNodeService();
        NodeRef dossier = daNodeService.getPrimaryParent(document).getParentRef();
        String fileName = getFileName();
        String mimeType = null;
        super.finishImpl(context, outcome);


        if ("S²LOW".equals(daNodeService.getProperty(dossier, ParapheurModel.PROP_TDT_NOM))
                && "ACTES".equals(daNodeService.getProperty(dossier, ParapheurModel.PROP_TDT_PROTOCOLE))) {
            ContentReader reader = contentService.getReader(document, ContentModel.PROP_CONTENT);
            ContentWriter writer = contentService.getWriter(document, ContentModel.PROP_CONTENT, true);

            writer.setMimetype(MimetypeMap.MIMETYPE_PDF);
            writer.setEncoding(reader.getEncoding());

            contentService.transform(reader, writer);

            if (!fileName.endsWith(".pdf")) {
                fileName += ".pdf";
            }
            mimeType = MimetypeMap.MIMETYPE_PDF;
        } else {
            // NOTE: This is a far from ideal solution but will do until we have
            //       a pure JSF upload solution working. This method is only called
            //       after a file is uploaded, so we can calculate the mime type and
            //       determine whether to enable inline editing in here.
            mimeType = Repository.getMimeTypeForFileName(
                    FacesContext.getCurrentInstance(), fileName);
        }

        daNodeService.setProperty(document, ContentModel.PROP_NAME, fileName);

        List<NodeRef> documents = this.parapheurService.getDocuments(dossier);

        NodeRef piecePrincipale = documents.get(0);

        /**
         * Generation d'aperçu si et seulement si c'est un PDF, ET que ce doc
         * est le document principal
         */

        /*String previewXPath = "/app:company_home/app:dictionary/ph:previews/*[@cm:name='" + dossier.getId() + ".swf']";

        List<NodeRef> previewNodes = searchService.selectNodes(daNodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                previewXPath,
                null,
                namespaceService,
                false);

        if (previewNodes != null && previewNodes.size() > 0) {
            NodeRef previewNode = previewNodes.get(0);
            daNodeService.deleteNode(previewNode);
        }*/

        if (MimetypeMap.MIMETYPE_PDF.equals(mimeType) && piecePrincipale.equals(document)) {
            daNodeService.removeProperty(document, ParapheurModel.PROP_VISUEL_PDF);
        }
        else if (!MimetypeMap.MIMETYPE_PDF.equals(mimeType) && piecePrincipale.equals(document)) {
            // 3.3.0 génération automatique d'un visuel PDF

            ContentWriter pdfwriter = contentService.getWriter(document, ParapheurModel.PROP_VISUEL_PDF, Boolean.TRUE);
            pdfwriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
            pdfwriter.setEncoding("UTF-8");

            ContentReader reader = contentService.getReader(document, ContentModel.PROP_CONTENT);
            reader.setMimetype(mimeType);
            reader.setEncoding("UTF-8");

            contentService.transform(reader, pdfwriter);
        }

        parapheurService.generatePreviewForNodeRef(document, true);
        return "browse";
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public String getUploadFileSizeLimit() {
        return this.parapheurService.getUploadFileSizeLimit();
    }
}
