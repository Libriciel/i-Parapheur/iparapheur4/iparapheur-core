/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.ui.repo.tag;

import com.atolcd.parapheur.web.bean.ParapheurVersionPropertiesBean;
import java.io.IOException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.servlet.FacesHelper;

public class PageTag extends org.alfresco.web.ui.repo.tag.PageTag
{
    private static final long serialVersionUID = 1L;

    /**
     * Finalisation de toutes les pages : affichage d'un pied de page indiquant 
     * le numéro de version, avec lien vers la page "à propos".
     * 
     * @return EVAL_PAGE
     */
    @Override
    public int doEndTag() throws JspException
    {
       try
       {
          if (Application.inPortalServer() == false)
          {
             String contextPath = ((HttpServletRequest)pageContext.getRequest()).getContextPath();
             pageContext.getOut().write("<center><span class='footer'>");
             ParapheurVersionPropertiesBean pptesBean = 
                (ParapheurVersionPropertiesBean) FacesHelper.getManagedBean(FacesContext.getCurrentInstance(), "ParapheurVersionPropertiesBean");
             if (pptesBean != null && pptesBean.getProperties() != null)
             {
                String versionInfo = "i-Parapheur version "
                   + pptesBean.getProperties().getVersionNumber() 
                   + " build " + pptesBean.getProperties().getBuildNumber();
                pageContext.getOut().write("<a target=\"_blank\" href=\"" + contextPath + "/faces/jsp/parapheur/about.jsp\">");
                pageContext.getOut().write(versionInfo);
                pageContext.getOut().write("</a>");
             }
             else
             {
                pageContext.getOut().write("La version du parapheur n'a pu être déterminée");
             }
             
             pageContext.getOut().write("</span></center>");   
             pageContext.getOut().write("\n</body></html>");
          }  
       }
       catch (IOException ex)
       {
          throw new JspException(ex.toString());
       }
       
       return EVAL_PAGE;
    }
}
