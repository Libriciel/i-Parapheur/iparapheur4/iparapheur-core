/*
 * Version 3.2
 * CeCILL Copyright (c) 2006-2012, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.web.bean.ParapheurBean;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.app.Application;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.bean.repository.User;
import org.apache.commons.validator.EmailValidator;

import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.*;
import javax.faces.model.SelectItem;

/**
 * 
 * @author Emmanuel Peralta - Adullact Projet
 */
public class SendDossierBySecureMailDialog extends BaseDialogBean {
    private String subject;
    private String emailAdresses;
    private String emailAdressesCC;
    private String emailAdressesCCI;
    private String body;

    /* Mail securisé specifics */
    private Boolean mailsecActivated;
    private Boolean shallSendPlainTextPassword;
    private Boolean shallSendAnnexes;
    private String password;

    private NodeRef dossierRef;
    private String dossierName;

    private ParapheurBean parapheurBean;
    private ParapheurService parapheurService;

    private S2lowService s2lowService;

    private Properties configuration;
    
    private List<NodeRef> selectedAttachments;
    private List<SelectItem> attachments;
    private boolean showAttachments;

    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    public Boolean getMailsecActivated() {
        return mailsecActivated;
    }

    public void setMailsecActivated(Boolean mailsecActivated) {
        this.mailsecActivated = mailsecActivated;
    }

    /**
     * @return Returns the subject.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject The subject to set.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return Returns the email adresses string.
     */
    public String getEmailAdresses() {
        return emailAdresses;
    }

    /**
     * @param emailAdresses the emailAdresses to set
     */
    public void setEmailAdresses(String emailAdresses) {
        this.emailAdresses = emailAdresses;
    }

    /**
     * @return Returns the CC email adresses string.
     */
    public String getEmailAdressesCC() {
        return emailAdressesCC;
    }

    /**
     * @param emailAdressesCC the emailAdressesCC to set
     */
    public void setEmailAdressesCC(String emailAdressesCC) {
        this.emailAdressesCC = emailAdressesCC;
    }

    /**
     * @return Returns the BCC email adresses string.
     */
    public String getEmailAdressesCCI() {
        return emailAdressesCCI;
    }

    /**
     * @param emailAdressesCCI the emailAdressesCCI to set
     */
    public void setEmailAdressesCCI(String emailAdressesCCI) {
        this.emailAdressesCCI = emailAdressesCCI;
    }

    /**
     * @return Returns the email body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body The email body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return Returns the dossierRef.
     */
    public NodeRef getDossierRef() {
        return dossierRef;
    }

    /**
     * @param dossierRef The dossierRef to set.
     */
    public void setDossierRef(NodeRef dossierRef) {
        this.dossierRef = dossierRef;
    }

    /**
     * @return Returns the dossierName.
     */
    public String getDossierName() {
        return dossierName;
    }

    public Boolean getShallSendPlainTextPassword() {
        return shallSendPlainTextPassword;
    }

    public void setShallSendPlainTextPassword(Boolean shallSendPlainTextPassword) {
        this.shallSendPlainTextPassword = shallSendPlainTextPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getShallSendAnnexes() {
        return shallSendAnnexes;
    }

    public void setShallSendAnnexes(Boolean shallSendAnnexes) {
        this.shallSendAnnexes = shallSendAnnexes;
    }

    public void setParapheurBean(ParapheurBean parapheurBean) {
        this.parapheurBean = parapheurBean;
    }

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setS2lowService(S2lowService s2lowService) {
        this.s2lowService = s2lowService;
    }
    
    public List<SelectItem> getAttachments() {
        return attachments;
    }
    
    public List<NodeRef> getSelectedAttachments() {
        return this.selectedAttachments;
    }
    
    public void setSelectedAttachments(List<NodeRef> selectedAttachments) {
        this.selectedAttachments = selectedAttachments;
    }

    public boolean getShowAttachments() {
        return showAttachments;
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#init(java.util.Map)
     */
    @Override
    public void init(Map<String, String> parameters) {
        super.init(parameters);

        if (this.parameters.containsKey("id")) {
            dossierRef = new NodeRef(Repository.getStoreRef(), this.parameters.get("id"));
        } else {
            dossierRef = this.navigator.getCurrentNode().getNodeRef();
        }

        if (!this.parapheurService.isDossier(dossierRef))
            throw new AlfrescoRuntimeException("SendDossierByMail called on a non-dossier element");

        this.dossierName = (String) getNodeService().getProperty(dossierRef, ContentModel.PROP_TITLE);
        this.emailAdresses = "";
        this.emailAdressesCC = "";
        this.emailAdressesCCI = "";

        this.body = s2lowService.getSecureMailTemplate(dossierRef);
        this.subject = "Dossier : " + dossierName;

        this.shallSendAnnexes = false;
        this.mailsecActivated = true;
        
        this.showAttachments = false;
        this.attachments = new ArrayList<SelectItem>();
        this.selectedAttachments = new ArrayList<NodeRef>();
        List<NodeRef> documents = parapheurService.getAttachments(dossierRef);
        if ((documents != null) && !documents.isEmpty()) {
            this.showAttachments = true;
            for (NodeRef document : documents) { 
                this.attachments.add(new SelectItem(document, (String) getNodeService().getProperty(document, ContentModel.PROP_NAME)));
            }
            if (parapheurService.areAttachmentsIncluded(dossierRef)) {
                selectedAttachments = documents;
            }
        }

    }


    private boolean isValidAddress(String address) {
        EmailValidator emailValidator = EmailValidator.getInstance();
        return emailValidator.isValid(address);
    }
/*
    protected void handleEmailAdresses(Action mailAction) {
        List<String> destinataires;
        if (this.emailAdresses != null) {

            destinataires = splitEmails(this.emailAdresses);

            if (destinataires.size() > 1) {
                mailAction.setParameterValue(PatchedMailActionExecuter.PARAM_TO_MANY, (ArrayList)destinataires);
            }
            else {
                mailAction.setParameterValue(PatchedMailActionExecuter.PARAM_TO, this.emailAdresses);
            }
        }
    }
*/
    protected List<String> splitEmails(String emails) {
        List<String> retVal = new ArrayList<String>();
        if (emails.contains(",")) {
            String[] tab = emails.split(",");
            for (int i = 0; i < tab.length; i++) {
                String email = tab[i].trim();
                if (email.length() > 0) {
                    retVal.add(email);
                }
            }
        }
        else {
            retVal.add(emails);
        }

        return retVal;
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#finishImpl(javax.faces.context.FacesContext, java.lang.String)
     */
    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Exception {
        if (mailsecActivated) {
            finishMailSec(context);
        }
        else {
          //  finishStandardMail();
        }

        this.browseBean.updateUILocation(this.dossierRef);

        return null;
    }

    protected void finishMailSec(FacesContext context) {
        List<String> mails = splitEmails(this.emailAdresses);
        List<String> mailsCC = splitEmails(this.emailAdressesCC);
        List<String> mailsCCI = splitEmails(this.emailAdressesCCI);

        try {
            int mail_id = s2lowService.sendSecureMail(mails, mailsCC, mailsCCI, this.subject, this.body, this.password, this.shallSendPlainTextPassword, this.dossierRef, this.selectedAttachments, true);

            NodeRef parapheurRef = parapheurBean.getParapheurCourant();
            User currentUser = Application.getCurrentUser(context);
            String fullName = null;

            if (this.parapheurService.isParapheurSecretaire(parapheurRef, currentUser.getUserName())) {
                fullName = String.format("%s pour le compte de \"%s\"", currentUser.getFullName(this.getNodeService()), parapheurService.getNomParapheur(parapheurRef));
            } else if (this.parapheurService.isParapheurOwner(parapheurRef, currentUser.getUserName())) {
                fullName = Application.getCurrentUser(context).getFullName(this.getNodeService());
            }

            parapheurService.setSignataire(dossierRef, fullName);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean getFinishButtonDisabled() {
        return false;
    }

}
