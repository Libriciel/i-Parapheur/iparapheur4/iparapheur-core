/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package com.atolcd.parapheur.web.bean.dialog;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.bean.AdminParapheurBean;
import org.adullact.iparapheur.comparator.SelectItemNodeRefComparator;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ApplicationModel;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.app.Application;
import org.alfresco.web.app.servlet.FacesHelper;
import org.alfresco.web.bean.dialog.BaseDialogBean;
import org.alfresco.web.bean.repository.MapNode;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.ui.common.SortableSelectItem;
import org.alfresco.web.ui.common.Utils;
import org.alfresco.web.ui.common.component.data.UIRichList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;

public class EditParapheurDialog extends BaseDialogBean {
    private static Log logger = LogFactory.getLog(EditParapheurDialog.class);

    protected ParapheurService parapheurService;
    protected PersonService personService;
    protected TenantService tenantService;
    protected AuthenticationComponent authenticationComponent;
    protected NodeService nodeService;

    private AuthorityService authorityService;

    private NodeRef parapheurRef;
    private String nom;
    private String titre;
    private String description;
    
    @Deprecated
    private String proprietaire;
    private UIRichList ownersRichList;
    private List<Node> owners;
    private UIRichList ownerSearchRichList;
    private List<Node> ownerSearchResults;
    private String ownerToSearch;
    private List<String> previousOwners;
    
    private UIRichList secretariatRichList;
    private List<Node> secretariat;
    private UIRichList secretarySearchRichList;
    private List<Node> secretarySearchResults;
    private String secretaryToSearch;
    private List<String> previousSecretaires;

    private String hierarchie;
    private Boolean delegation;
    private Boolean presentsDelegues;
    private NodeRef delegate;
    private Date dateDebut;
    private Date dateFin;

    private Boolean habilitationsEnabled;
    private Map<QName, Serializable> habilitations;
    
    protected SelectItem[] delegationsPossibles;
    protected SelectItem[] parapheurs;

    private boolean editMode;
    private boolean hideAVenir;
    
    private String admin;
    private boolean adminFonctionnel;
    private String finishOutcome;
    private Exception finishException;

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Returns the hierarchie.
     */
    public String getHierarchie() {
        return hierarchie;
    }

    /**
     * @param hierarchie The hierarchie to set.
     */
    public void setHierarchie(String hierarchie) {
        this.hierarchie = hierarchie;
    }

    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    public boolean isDelegation() {
        if (delegation == null) {
            delegation = ((parapheurService.getDelegation(parapheurRef) != null) ||
                    (parapheurService.getDelegationProgrammee(parapheurRef) != null));
        }
        return delegation;
    }

    public boolean getDelegation() {
        return delegation;
    }

    public void setDelegation(boolean delegation) {
        this.delegation = delegation;
    }
    
    public boolean getPresentsDelegues() {
        return presentsDelegues;
    }

    public void setPresentsDelegues(boolean presentsDelegues) {
        this.presentsDelegues = presentsDelegues;
    }

    public NodeRef getDelegate() {
        if (delegate == null) {
            if (this.parapheurRef != null) {
                delegate = parapheurService.getDelegation(this.parapheurRef);
                if (delegate == null) {
                    delegate = parapheurService.getDelegationProgrammee(this.parapheurRef);
                    if (delegate == null) {
                        delegate = parapheurService.getOldDelegation(this.parapheurRef);
                    }
                }
            }
        }
        return delegate;
    }

    public void setDelegate(NodeRef delegate) {
        this.delegate = delegate;
    }
    
    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }
    
    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }
    
    public boolean getHideAVenir() {
        return hideAVenir;
    }

    public void setHideAVenir(boolean hideAVenir) {
        this.hideAVenir = hideAVenir;
    }
    
    /**
     * @return Returns the parapheurRef.
     */
    public NodeRef getParapheurRef() {
        return parapheurRef;
    }

    /**
     * @return Returns the nom.
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom The nom to set.
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return Returns the titre.
     */
    public String getTitre() {
        return titre;
    }

    /**
     * @param title The titre to set.
     */
    public void setTitre(String title) {
        this.titre = title;
    }

    /**
     * @return Returns the proprietaire.
     */
    public String getProprietaire() {
        return proprietaire;
    }

    /**
     * @param proprietaire The proprietaire to set.
     */
    public void setProprietaire(String proprietaire) {
        this.proprietaire = proprietaire;
    }

    public List<Node> getOwners() {
        return this.owners;
    }
    
    public void setOwners(List<Node> owners) {
        this.owners = owners;
    }
    
    public String getOwnerToSearch() {
        return this.ownerToSearch;
    }
    
    public void setOwnerToSearch(String ownerToSearch) {
        this.ownerToSearch = ownerToSearch;
    }

    public UIRichList getOwnersRichList() {
        return this.ownersRichList;
    }
    
    public void setOwnersRichList(UIRichList ownersRichList) {
        this.ownersRichList = ownersRichList;
        if (this.ownersRichList != null) {
            this.ownersRichList.setValue(null);
        }
    }
    
    public UIRichList getOwnerSearchRichList() {
        return this.ownerSearchRichList;
    }
    
    public void setOwnerSearchRichList(UIRichList ownerSearchRichList) {
        this.ownerSearchRichList = ownerSearchRichList;
        if (this.ownerSearchRichList != null) {
            this.ownerSearchRichList.setValue(null);
        }
    }
    
    public List<Node> getOwnerSearchResults() {
        return this.ownerSearchResults;
    }
    
    public List<Node> getSecretariat() {
        return this.secretariat;
    }
    
    public void setSecretariat(List<Node> secretariat) {
        this.secretariat = secretariat;
    }
    
    public String getSecretaryToSearch() {
        return this.secretaryToSearch;
    }
    
    public void setSecretaryToSearch(String secretaryToSearch) {
        this.secretaryToSearch = secretaryToSearch;
    }

    public UIRichList getSecretariatRichList() {
        return this.secretariatRichList;
    }
    
    public void setSecretariatRichList(UIRichList secretariatRichList) {
        this.secretariatRichList = secretariatRichList;
        if (this.secretariatRichList != null) {
            this.secretariatRichList.setValue(null);
        }
    }
    
    public UIRichList getSecretarySearchRichList() {
        return this.secretarySearchRichList;
    }
    
    public void setSecretarySearchRichList(UIRichList secretarySearchRichList) {
        this.secretarySearchRichList = secretarySearchRichList;
        if (this.secretarySearchRichList != null) {
            this.secretarySearchRichList.setValue(null);
        }
    }
    
    public List<Node> getSecretarySearchResults() {
        return this.secretarySearchResults;
    }

    /**
     * @return Returns the parapheurService.
     */
    public ParapheurService getParapheurService() {
        return parapheurService;
    }

    /**
     * @param parapheurService The parapheurService to set.
     */
    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    /**
     * @return Returns the personService.
     */
    public PersonService getPersonService() {
        return personService;
    }

    /**
     * @param personService The personService to set.
     */
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    public void setAuthenticationComponent(AuthenticationComponent authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }

    public SelectItem[] getParapheurs() {
        if (this.parapheurs == null) {
            List<NodeRef> lstParapheurs = (adminFonctionnel)? parapheurService.getAllManagedParapheursByOpAdmin(admin) : parapheurService.getParapheurs();
            this.parapheurs = new SelectItem[lstParapheurs.size() + 1];
            int index = 0;
            for (NodeRef tmpParaph : lstParapheurs) {
                String name = (String) getNodeService().getProperty(tmpParaph, ContentModel.PROP_NAME);
                SelectItem item = new SortableSelectItem(tmpParaph.toString(), name, name);
                this.parapheurs[index] = item;
                index++;
            }
            this.parapheurs[index] = new SortableSelectItem("", "", "");
        }

        Arrays.sort(this.parapheurs);
        return this.parapheurs;
    }
    
    
    public SelectItem[] getDelegationsPossibles() {
        
        List<SelectItem> bureauxTmp = new ArrayList<SelectItem>();
        List<NodeRef> bureauxDelegables;
        if (this.parameters.containsKey("new")) {
            bureauxDelegables = parapheurService.getParapheurs();
        }
        else {
            bureauxDelegables = parapheurService.getDelegationsPossibles(this.parapheurRef);
        }
        for (NodeRef bureau : bureauxDelegables) {
            SelectItem item = new SelectItem(bureau, parapheurService.getNomParapheur(bureau));
            bureauxTmp.add(item);
        }
        bureauxTmp.add(new SelectItem("", "-- Choisir une délégation --", "", true));
        this.delegationsPossibles = new SelectItem[bureauxTmp.size()];
        this.delegationsPossibles = bureauxTmp.toArray(this.delegationsPossibles);
        Arrays.sort(this.delegationsPossibles, new SelectItemNodeRefComparator());
        return this.delegationsPossibles;
    }

    public Boolean getHabilitationsEnabled() {
        if (habilitationsEnabled == null) {
            if (parapheurRef != null) {
                habilitationsEnabled = getNodeService().hasAspect(parapheurRef, ParapheurModel.ASPECT_HABILITATIONS);
            } else {
                habilitationsEnabled = false;
            }
        }

        return habilitationsEnabled;
    }

    public void setHabilitationsEnabled(Boolean habilitationsEnabled) {
        this.habilitationsEnabled = habilitationsEnabled;
    }

    public Boolean getHabTransmettreEnabled() {
        return (Boolean) getHabilitations().get(ParapheurModel.PROP_HAB_TRANSMETTRE);
    }

    public void setHabTransmettreEnabled(Boolean value) {
        getHabilitations().put(ParapheurModel.PROP_HAB_TRANSMETTRE, value);
    }

    public Boolean getHabSecretariatEnabled() {
        return (Boolean) getHabilitations().get(ParapheurModel.PROP_HAB_SECRETARIAT);
    }

    public void setHabSecretariatEnabled(Boolean value) {
        getHabilitations().put(ParapheurModel.PROP_HAB_SECRETARIAT, value);
    }

    public Boolean getHabArchivageEnabled() {
        return (Boolean) getHabilitations().get(ParapheurModel.PROP_HAB_ARCHIVAGE);
    }

    public void setHabArchivageEnabled(Boolean value) {
        getHabilitations().put(ParapheurModel.PROP_HAB_ARCHIVAGE, value);
    }

    public Boolean getHabTraiterEnabled() {
        return (Boolean) getHabilitations().get(ParapheurModel.PROP_HAB_TRAITER);
    }

    public void setHabTraiterEnabled(Boolean value) {
        getHabilitations().put(ParapheurModel.PROP_HAB_TRAITER, value);
    }

    public Map<QName, Serializable> getHabilitations() {
        if (habilitations == null) {
            habilitations = new HashMap<QName, Serializable>();
            if (parapheurRef != null) {
                Map<QName, Serializable> properties = getNodeService().getProperties(parapheurRef);
                habilitations.put(ParapheurModel.PROP_HAB_ARCHIVAGE, (Boolean) properties.get(ParapheurModel.PROP_HAB_ARCHIVAGE));
                habilitations.put(ParapheurModel.PROP_HAB_SECRETARIAT, (Boolean) properties.get(ParapheurModel.PROP_HAB_SECRETARIAT));
                habilitations.put(ParapheurModel.PROP_HAB_TRAITER, (Boolean) properties.get(ParapheurModel.PROP_HAB_TRAITER));
                habilitations.put(ParapheurModel.PROP_HAB_TRANSMETTRE, (Boolean) properties.get(ParapheurModel.PROP_HAB_TRANSMETTRE));
            }
        }

        return habilitations;
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#init(java.util.Map)
     */
    @Override
    public void init(Map<String, String> parameters) {
        super.init(parameters);

        this.editMode = false;

        this.description = "";
        this.hierarchie = "";
        this.nom = "";
        this.titre = "";
        this.proprietaire = "";
        this.secretariat = null;

        this.parapheurs = null;
        this.delegationsPossibles = null;

        this.delegation = null;
        this.presentsDelegues = false;
        this.dateDebut = null;
        this.dateFin = null;
        this.delegate = null;
        this.habilitations = null;
        this.habilitationsEnabled = null;
        this.hideAVenir = false;
        
        this.finishOutcome = null;
        this.finishException = null;
        this.owners = new ArrayList<Node>();
        this.previousOwners = new ArrayList<String>();
        this.previousSecretaires = new ArrayList<String>();
        this.ownerToSearch = "";
        this.ownerSearchResults = new ArrayList<Node>();
        this.secretariat = new ArrayList<Node>();
        this.secretaryToSearch = "";
        this.secretarySearchResults = new ArrayList<Node>();


        
        if (!this.parameters.containsKey("new")) {
            this.editMode = true;
            AdminParapheurBean adminBean = (AdminParapheurBean) FacesHelper.getManagedBean(FacesContext.getCurrentInstance(), "AdminParapheurBean");
            Node paraphNode = adminBean.getParapheurNode();

            if (paraphNode != null) {
                this.parapheurRef = paraphNode.getNodeRef();
                Map<QName, Serializable> pptes = this.getNodeService().getProperties(this.parapheurRef);
                this.nom = (String) pptes.get(ContentModel.PROP_NAME);
                this.titre = (String) pptes.get(ContentModel.PROP_TITLE);
                this.description = (String) pptes.get(ContentModel.PROP_DESCRIPTION);

                previousSecretaires = (List<String>) pptes.get(ParapheurModel.PROP_SECRETAIRES);
                for (String user : previousSecretaires) {
                    this.secretariat.add(new MapNode(personService.getPerson(user)));
                }
                this.proprietaire = (String) pptes.get(ParapheurModel.PROP_PROPRIETAIRE_PARAPHEUR);
                previousOwners = (List<String>) pptes.get(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);
                for (String user : previousOwners) {
                    this.owners.add(new MapNode(personService.getPerson(user)));
                }

                NodeRef superieur = this.parapheurService.getParapheurResponsable(this.parapheurRef);
                if (superieur != null) {
                    this.hierarchie = superieur.toString();
                } else {
                    this.hierarchie = "";
                }
                if (getHabilitationsEnabled()) {
                    getHabilitations();
                }
                
                this.dateDebut = parapheurService.getDateDebutDelegation(parapheurRef);
                this.dateFin = parapheurService.getDateFinDelegation(parapheurRef);
                if ((dateFin != null) && dateFin.before(new Date())) {
                    dateFin = null;
                }
                this.presentsDelegues = parapheurService.arePresentsDelegues(parapheurRef);
                this.hideAVenir = !parapheurService.showAVenir(parapheurRef);
            }
        }
        
        this.admin = AuthenticationUtil.getRunAsUser();
        this.adminFonctionnel = parapheurService.isAdministrateurFonctionnelOf(admin, this.parapheurRef);
    }

    /**
     * @see org.alfresco.web.bean.dialog.BaseDialogBean#finishImpl(javax.faces.context.FacesContext, java.lang.String)
     */
    @Override
    protected String finishImpl(FacesContext context, String outcome) throws Exception {
        if (adminFonctionnel) {
            final FacesContext fcontext = context;
            final String foutcome = outcome;
            AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Void>() {
                @Override
                public Void doWork() throws Exception {
                    try {
                        finishOutcome = finishImplAdmin(fcontext, foutcome);
                    } catch (Exception e) {
                        logger.error("Une erreur s'est produite lors de l'édition du parapheur.");
                        finishException = e;
                    }
                    return null;
                }
            }, AuthenticationUtil.getAdminUserName());
            if (finishException != null) {
                throw finishException;
            }
        }
        else {
            finishOutcome = finishImplAdmin(context, outcome);
        }
        return finishOutcome;
    }
    
    private String finishImplAdmin(FacesContext context, String outcome) throws Exception {
        UserTransaction tx = null;
        try {
            // verification des caracteres interdits
            if (getNom()!=null && ! getNom().trim().isEmpty()) {
                char[] forbidens="&\"£*/<>?%|+".toCharArray();
                for (char ch : forbidens) {
                    if (getNom().indexOf(ch)>=0)  {
                        Utils.addErrorMessage("Le nom indique contient un ou plusieurs caractères interdits");
                        return null;
                    }
                }
            }

            if (this.editMode) {
                if (this.hierarchie != null && !this.hierarchie.isEmpty()) {
                    if (this.parapheurRef != null) {
                        NodeRef superieur = new NodeRef(this.hierarchie);
                        while (superieur != null) {
                            if (superieur.equals(this.parapheurRef)) {
                                Utils.addErrorMessage(MessageFormat.format(Application.getMessage(FacesContext.getCurrentInstance(), Repository.ERROR_GENERIC), "Attention, le responsable sélectionné provoque une boucle dans la hiérarchie"));
                                logger.error("Le responsable sélectionné provoque une boucle dans la hiérarchie");
                                return null;
                            }
                            superieur = this.parapheurService.getParapheurResponsable(superieur);
                        }
                    }
                }
            }

            Set<Node> intersection = new HashSet<Node>(secretariat);
            if (this.owners != null) {
                intersection.retainAll(this.owners);
            }
            if (intersection.isEmpty()) {
                ArrayList<String> proprietaires = new ArrayList<String>();
                for (Node node : this.owners) {
                    proprietaires.add((String) node.getProperties().get(ContentModel.PROP_USERNAME));
                }
                ArrayList<String> secretaires = new ArrayList<String>();
                for (Node node : this.secretariat) {
                    secretaires.add((String) node.getProperties().get(ContentModel.PROP_USERNAME));
                }
                String oldName = this.nom;
                Map<QName, Serializable> props = new HashMap<QName, Serializable>();
                props.put(ContentModel.PROP_NAME, this.nom);
                props.put(ApplicationModel.PROP_ICON, "space-icon-default");
                props.put(ContentModel.PROP_TITLE, this.titre);
                props.put(ContentModel.PROP_DESCRIPTION, this.description);
                props.put(ParapheurModel.PROP_PROPRIETAIRE_PARAPHEUR, this.proprietaire);
                props.put(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, (ArrayList) proprietaires);
                props.put(ParapheurModel.PROP_SECRETAIRES, (ArrayList) secretaires);
                props.put(ParapheurModel.PROP_SHOW_A_VENIR, !this.hideAVenir);
                
                if (!this.parameters.containsKey("new")) {
                    // Les delegations possibles sont gérées par un webscript, on ne change donc pas la propriété ici.
                    props.put(ParapheurModel.PROP_DELEGATIONS_POSSIBLES,
                            (ArrayList<NodeRef>) getNodeService().getProperty(this.parapheurRef, ParapheurModel.PROP_DELEGATIONS_POSSIBLES));
                }

                tx = Repository.getUserTransaction(context, true);
                tx.begin();

                if (!this.editMode) {
                    this.parapheurRef = parapheurService.createParapheur(props);
                } else {
                    oldName = (String) getNodeService().getProperty(this.parapheurRef, ContentModel.PROP_NAME);
                    logger.debug("parapheur-name oldName: " + oldName);
                    props.put(ContentModel.PROP_CREATOR, getNodeService().getProperty(this.parapheurRef, ContentModel.PROP_CREATOR));
                    props.put(ContentModel.PROP_CREATED, getNodeService().getProperty(this.parapheurRef, ContentModel.PROP_CREATED));
                    getNodeService().setProperties(this.parapheurRef, props);
                }

                /**
                 * Supérieur hiérarchique
                 */
                if (this.editMode) {
                    NodeRef superieur = this.parapheurService.getParapheurResponsable(this.parapheurRef);
                    if (superieur != null)
                        getNodeService().removeAssociation(this.parapheurRef, superieur, ParapheurModel.ASSOC_HIERARCHIE);
                }

                if (this.hierarchie != null && !this.hierarchie.isEmpty()) {
                    if (this.editMode) {
                        if (this.parapheurRef != null) {
                            NodeRef superieur = this.parapheurService.getParapheurResponsable(new NodeRef(this.hierarchie));
                            while (superieur != null) {
                                if (superieur.equals(this.parapheurRef)) {
                                    throw new AlfrescoRuntimeException("Le responsable sélectionné provoque une boucle dans la hiérarchie");
                                }
                                superieur = this.parapheurService.getParapheurResponsable(superieur);
                            }
                        }
                    }

                    getNodeService().createAssociation(this.parapheurRef, new NodeRef(this.hierarchie), ParapheurModel.ASSOC_HIERARCHIE);
                }
                
                if (getNodeService().hasAspect(parapheurRef, ParapheurModel.ASPECT_HABILITATIONS)) {
                    getNodeService().removeAspect(parapheurRef, ParapheurModel.ASPECT_HABILITATIONS);
                }

                if (this.habilitationsEnabled) {
                    getNodeService().addAspect(parapheurRef, ParapheurModel.ASPECT_HABILITATIONS, getHabilitations());
                }

                /**
                 * Create the ROLE_PHOWNER and ROLE_PHDELEGATES corresponding to this parapheur
                 */

                if (!authorityService.authorityExists("ROLE_PHOWNER_" + parapheurRef.getId())) {
                    authorityService.createAuthority(AuthorityType.ROLE, "PHOWNER_" + parapheurRef.getId());
                }

                if (!authorityService.authorityExists("ROLE_PHDELEGATES_" + parapheurRef.getId())) {
                    authorityService.createAuthority(AuthorityType.ROLE, "PHDELEGATES_" + parapheurRef.getId());
                }

                /**
                 * Update owners permissions
                 */
                // Instead of deleting and recreate the whole role, we add the new owners and
                // delete the old ones (that are not owners anymore)
                List<String> newOwners = new ArrayList<String>(proprietaires);
                newOwners.removeAll(previousOwners);
                List<String> ownersToRemove = new ArrayList<String>(previousOwners);
                ownersToRemove.removeAll(proprietaires);

                if (!ownersToRemove.isEmpty()) {
                    for (String ownerToRemove : ownersToRemove) {
                        if (!ownerToRemove.isEmpty()) {
                            authorityService.removeAuthority("ROLE_PHOWNER_" + parapheurRef.getId(),
                                    authorityService.getName(AuthorityType.USER, ownerToRemove));
                        }
                    }
                }

                if (!newOwners.isEmpty()) {
                    for (String newOwner : newOwners) {
                        if (!newOwner.isEmpty()) {
                            authorityService.addAuthority("ROLE_PHOWNER_" + parapheurRef.getId(),
                                    authorityService.getName(AuthorityType.USER, newOwner));
                        }
                    }
                }

                /**
                 * Update secretaires permissions
                 */
                List<String> newSecretaires = new ArrayList<String>(secretaires);
                newSecretaires.removeAll(previousSecretaires);
                List<String> secretairesToRemove = new ArrayList<String>(previousSecretaires);
                secretairesToRemove.removeAll(secretaires);

                if (!secretairesToRemove.isEmpty()) {
                    for (String secretaireToRemove : secretairesToRemove) {
                        if (!secretaireToRemove.isEmpty()) {
                            authorityService.removeAuthority("ROLE_PHSECRETARIAT_" + parapheurRef.getId(),
                                    authorityService.getName(AuthorityType.USER, secretaireToRemove));
                        }
                    }
                }

                if (!newSecretaires.isEmpty()) {
                    if (!authorityService.authorityExists("ROLE_PHSECRETARIAT_" + parapheurRef.getId())) {
                        authorityService.createAuthority(AuthorityType.ROLE, "PHSECRETARIAT_" + parapheurRef.getId());
                    }
                    for (String newSecretaire : newSecretaires) {
                        if (!newSecretaire.isEmpty()) {
                            authorityService.addAuthority("ROLE_PHSECRETARIAT_" + parapheurRef.getId(),
                                    authorityService.getName(AuthorityType.USER, newSecretaire));
                        }
                    }
                }

                /**
                 * Update delegations
                 */
                if (this.delegation && (delegate != null)) {
                    this.parapheurService.programmerDelegation(parapheurRef,
                            delegate,
                            dateDebut,
                            dateFin,
                            presentsDelegues);
                } else {
                    this.parapheurService.supprimerDelegation(parapheurRef);
                }

                tx.commit();
            } else {
                Utils.addErrorMessage(MessageFormat.format(Application.getMessage(FacesContext.getCurrentInstance(), Repository.ERROR_GENERIC), "Attention, le propriétaire du parapheur ne peut pas être membre de son secrétariat"));
                logger.error("Le propriétaire du parapheur ne peut pas être membre de son secrétariat");
                return null;
            }
        } catch (Throwable err) {
            Utils.addErrorMessage(MessageFormat.format(Application.getMessage(
                    context, Repository.ERROR_GENERIC), err.getMessage()), err);
            logger.error(err.getMessage(), err);
            try {
                if (tx != null) {
                    tx.rollback();
                }
            } catch (Exception tex) {
            }
        }

        return outcome;
    }

    /*private void getPersonnes() {
        //this.proprietairesList = new ArrayList<SortableSelectItem>();
        this.secretairesList = new ArrayList<SortableSelectItem>();

        Set<String> users = new HashSet<String>();
        
        List<NodeRef> parapheurs = (adminFonctionnel)? parapheurService.getAllManagedParapheursByOpAdmin(admin) : parapheurService.getParapheurs();
        Set<String> proprietaires = new HashSet<String>();
        Set<String> secretaires = new HashSet<String>();

        for (NodeRef parapheur : parapheurs) {
            List<String> owners = parapheurService.getParapheurOwners(parapheur);
            if (owners != null) {
                proprietaires.addAll(owners);
            }

            List<String> secretariat = parapheurService.getSecretariatParapheur(parapheur);
            if (secretaires != null) {
                secretaires.addAll(secretariat);
            }
        }
        
        if (adminFonctionnel) {
            // Limit the users to those we have in charge
            users.addAll(proprietaires);
            users.addAll(secretaires);
        }
        else {
            // Retrieve all users existing in the current tenant
            for (NodeRef person : personService.getAllPeople()) {
                users.add((String) getNodeService().getProperty(person, ContentModel.PROP_USERNAME));
            }
        }
        users.remove(authenticationComponent.getGuestUserName(tenantService.getUserDomain(authenticationComponent.getCurrentUserName())));

        for (String user : users) {
            NodeRef person = personService.getPerson(user);
            String firstName = (String) getNodeService().getProperty(person, ContentModel.PROP_FIRSTNAME);
            String lastName = (String) getNodeService().getProperty(person, ContentModel.PROP_LASTNAME);
            String fullName = firstName + " " + lastName;

            /*
            exclusion mutuelle du secretariat
            Attention aux effets de bords si ce paramétrage à été mis en place
            l'utilisateur n'apparaitra dans aucune des deux listes.
            - lors de la premiere sauvegarde uniquement après tout devrait rentrer
            dans l'ordre
            *//*
            if (!secretaires.contains(user)) {
                //this.proprietairesList.add(new SortableSelectItem(user, fullName, fullName));
            }

            if (!proprietaires.contains(user)) {
                this.secretairesList.add(new SortableSelectItem(user, fullName, fullName));
            }

            // cas buggé introduit par la v3.3.0
            // on l'ajoute dans les deux listes afin qu'il soit deselectionable
            if (proprietaires.contains(user) && secretaires.contains(user)) {
                //this.proprietairesList.add(new SortableSelectItem(user, fullName, fullName));
                this.secretairesList.add(new SortableSelectItem(user, fullName, fullName));
            }

        }
    }*/
    
    public void searchOwner() {
        if (this.ownerToSearch != null && this.ownerToSearch.trim().length() > 0) {
            this.ownerSearchResults.clear();
            for (NodeRef nodeRef : parapheurService.searchUser(this.ownerToSearch)) {
                Node node = new MapNode(nodeRef);
                String username = (String)node.getProperties().get(ContentModel.PROP_USERNAME);
                node.getProperties().put("editable", parapheurService.getSecretariatParapheurs(username).isEmpty());
                this.ownerSearchResults.add(node);
            }
        }
    }
    
    public void addOwner() {
        NodeRef userRef = new NodeRef((String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("dialog:dialog-body:owner-to-add"));
        Node userNode = new MapNode(userRef);
        if (!contains(owners, userRef)) {
            owners.add(userNode);
            removeSecretary(userRef);
        }
    }
    
    public void removeOwner() {
        NodeRef userRef = new NodeRef((String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("dialog:dialog-body:owner-to-remove"));
        removeOwner(userRef);
    }
    
    public void clearOwnerSearch() {
        this.ownerSearchResults.clear();
    }
    
    public void searchSecretary() {
        if (this.secretaryToSearch != null && this.secretaryToSearch.trim().length() > 0) {
            this.secretarySearchResults.clear();
            for (NodeRef nodeRef : parapheurService.searchUser(this.secretaryToSearch)) {
                Node node = new MapNode(nodeRef);
                node.getProperties().put("editable", !parapheurService.isOwner((String)node.getProperties().get(ContentModel.PROP_USERNAME)));
                this.secretarySearchResults.add(node);
            }
        }
    }
    
    public void addSecretary() {
        NodeRef userRef = new NodeRef((String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("dialog:dialog-body:secretary-to-add"));
        Node userNode = new MapNode(userRef);
        if (!contains(secretariat, userRef)) {
            secretariat.add(userNode);
            removeOwner(userRef);
        }
    }
    
    public void removeSecretary() {
        NodeRef userRef = new NodeRef((String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("dialog:dialog-body:secretary-to-remove"));
        removeSecretary(userRef);
    }
    
    public void clearSecretarySearch() {
        this.secretarySearchResults.clear();
    }
    
    private void removeOwner(NodeRef user) {
        for (int i = 0; i < owners.size(); i++) {
            if (owners.get(i).getNodeRef().equals(user)) {
                owners.remove(i);
                break;
            }
        }
    }
    
    private void removeSecretary(NodeRef user) {
        for (int i = 0; i < secretariat.size(); i++) {
            if (secretariat.get(i).getNodeRef().equals(user)) {
                secretariat.remove(i);
                break;
            }
        }
    }
    
    private boolean contains(List<Node> list, NodeRef value) {
        boolean found = false;
        for (Node node : list) {
            if (node.getNodeRef().equals(value)) {
                found = true;
                break;
            }
        }
        return found;
    }
}
