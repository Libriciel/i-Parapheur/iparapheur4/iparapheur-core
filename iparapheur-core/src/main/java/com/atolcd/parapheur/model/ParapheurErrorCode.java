package com.atolcd.parapheur.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * These are defined at http://aduwiki.extranet.adullact.org/doku.php?id=open_parapheur#code_d_erreurs
 */
public enum ParapheurErrorCode {

    // Generic errors
    INTERNAL_SERVER_ERROR(999, "Erreur de serveur interne"),

    // Node errors
    INVALID_DOSSIER_NODE(1001, "L'id du dossier renseigné n'est pas valide"),
    INVALID_BUREAU_NODE(1002, "L'id du bureau renseigné n'est pas valide"),
    INVALID_DOCUMENT_NODE(1003, "L'id du document renseigné n'est pas valide"),
    INVALID_ANNOTATION_NODE(1004, "L'id de l'annotation renseigné n'est pas valide"),
    INVALID_WORKFLOW_NODE(1005, "L'id du circuit renseigné n'est pas valide"),

    // Errors on save/emit (Protocol)
    XML_DOCUMENTS_FORBIDDEN_BY_TYPE_OR_SUBTYPE(2001, "Le Type/Sous-type sélectionné n'autorise pas les documents XML"),
    XML_DOCUMENTS_FORBIDDEN_IN_ANNEXES(2002, "Les documents XML sont interdits en annexes"),
    HELIOS_ONLY_ACCEPTS_XML_DOCUMENTS(2011, "Le protocole Helios requiert un XML en document principal"),
    ANNEXES_FORBIDDEN_WITH_HELIOS_PROTOCOL(2012, "Le protocole Helios n'autorise pas les documents en annexes"),
    PADES_ONLY_ACCEPTS_PDF_DOCUMENTS(2021, "La signature PAdES n'autorise que des PDF en documents principaux"),

    // Errors on save/emit (Integrity)
    CANT_RENAME_A_CURRENTLY_USED_WORKFLOW(2501, "Le circuit ne peut être renommé, il est actuellement utilisé"),
    INVALID_METADATA_URL(2601, "Erreur inconnue à la télétransmission"),

    // Documents errors
    PDF_PROTECTED(3001, "Le document PDF est protégé"),
    MAIN_DOCUMENT_IS_MISSING(3002, "Document principal manquant"),
    DOCUMENT_ALREADY_EXISTS(3003, "Le document existe déjà"),
    INVALID_PDF(3004, "Document PDF non valide"),
    XDOCUMENTS_NOT_ACCEPTED(3005, "Documents DOCX/XLSX/PPTX non-supportés"),
    PDF_PROTECTED_ERROR(3006, "Impossible de débloquer le PDF protégé"),

    // TDT errors
    UNKNOwN_TDT_ERROR(4000, "Erreur inconnue à la télétransmission"),
    TDT_NOT_SET_IN_TYPE(4001, "Aucun TDT sélectionné pour le Type du dossier"),
    ACTES_IS_NOT_SUPPORTED_IN_SELECTED_TDT(4002, "Le protocole ACTES n'est pas supporté par le TDT sélectionné"),
    TDT_CERTIFICATE_ERROR(4050, "Erreur avec le certificat utilisé pour la télétransmission"),
    TDT_CERTIFICATE_EXPIRED(4051, "Le certificat utilisé pour la télétransmission a expiré");

    private int value;
    private String defaultDescription;

    ParapheurErrorCode(int value, @NotNull String defaultDescription) {
        this.value = value;
        this.defaultDescription = defaultDescription;
    }

    public int getCode() {
        return value;
    }

    public @NotNull String getDefaultMessage() {
        return defaultDescription;
    }

    public static @Nullable ParapheurErrorCode getByCode(@NotNull String code) {
        int internalErrorCode = -1;
        try { internalErrorCode = Integer.valueOf(code); }
        catch (NumberFormatException ex) { /* no need to log */ }
        return ParapheurErrorCode.getByCode(internalErrorCode);
    }

    public static @Nullable ParapheurErrorCode getByCode(int code) {

        for(ParapheurErrorCode error : values())
            if (error.getCode() == code)
                return error;

        return null;
    }

}

