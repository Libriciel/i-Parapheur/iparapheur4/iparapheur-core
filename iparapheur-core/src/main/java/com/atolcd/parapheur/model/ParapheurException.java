package com.atolcd.parapheur.model;

import org.jetbrains.annotations.NotNull;

public class ParapheurException extends Exception {

    private ParapheurErrorCode errorCode;

    public ParapheurException(@NotNull ParapheurErrorCode errorCode) {
        super(errorCode.getDefaultMessage());
        this.errorCode = errorCode;
    }

    public int getInternalCode() {
        return errorCode.getCode();
    }
}
