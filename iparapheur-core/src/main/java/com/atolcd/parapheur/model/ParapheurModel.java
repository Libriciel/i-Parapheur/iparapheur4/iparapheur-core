/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package com.atolcd.parapheur.model;

import com.atolcd.parapheur.repo.impl.MetadataServiceImpl;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;

public interface ParapheurModel {

    static String PARAPHEUR_HABILLAGE_KEY = "parapheur.habillage";

    static String PARAPHEUR_HABILLAGE_VALEUR_DEFAUT = "adullact";

    /**
     * @since 3.3.0
     */
    static String PARAPHEUR_WS_GETDOSSIER_PDF_KEY = "parapheur.ws.getdossier.pdf.docName";

    /**
     * @since 3.3.0
     */
    static String PARAPHEUR_WS_GETDOSSIER_PDF_VALEUR_DEFAUT = "iParapheur_impression_dossier.pdf";

    /**
     * @since 3.3.1p
     */
    static String PARAPHEUR_UPLOAD_FILE_SIZE_LIMIT_KEY = "parapheur.ihm.document.uploadMaxSize";

    /**
     * @since 3.3.1p
     */
    String PARAPHEUR_UPLOAD_FILE_SIZE_LIMIT_DEFAUT = "0";

    // BLEX : BLEX : intervalle de temps pour le job startGetS2lowStatusJob
    //        Valeur définie dans le fichier s2low.properties
    String PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_KEY="parapheur.tdts2low.statutjobinterval";
    String PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_DEFAUT="30";

    String PARAPHEUR_ATTEST_JOBINTERVAL_KEY="parapheur.attest.jobinterval";
    String PARAPHEUR_ATTEST_JOBINTERVAL_DEFAULT="1";

    String PARAPHEUR_MODEL_URI = "http://www.atolcd.com/alfresco/model/parapheur/1.0";

    String PARAPHEUR_MODEL_PREFIX = "ph";

    QName TYPE_PERSON = QName.createQName(PARAPHEUR_MODEL_URI, "person");

    QName TYPE_PARAPHEUR = QName.createQName(PARAPHEUR_MODEL_URI, "parapheur");

    QName TYPE_PARAPHEURS = QName.createQName(PARAPHEUR_MODEL_URI, "parapheurs");

    QName TYPE_CORBEILLE = QName.createQName(PARAPHEUR_MODEL_URI, "corbeille");

    QName TYPE_CORBEILLE_VIRTUELLE = QName.createQName(PARAPHEUR_MODEL_URI, "corbeilleVirtuelle");

    QName TYPE_DOSSIER = QName.createQName(PARAPHEUR_MODEL_URI, "dossier");

    QName TYPE_DOCUMENT = QName.createQName(PARAPHEUR_MODEL_URI, "document");

    QName TYPE_ETAPE_CIRCUIT = QName.createQName(PARAPHEUR_MODEL_URI, "etape-circuit");

    QName TYPE_ARCHIVE = QName.createQName(PARAPHEUR_MODEL_URI, "archive");

    /**
     * Type corresponding to saved workflows.
     *
     * @since 3.2
     */
    QName TYPE_SAVED_WORKFLOW = QName.createQName(PARAPHEUR_MODEL_URI, "saved_workflow");

    QName PROP_ID_CERTIFICAT = QName.createQName(PARAPHEUR_MODEL_URI, "idCertificat");

    QName PROP_CERTIFICAT = QName.createQName(PARAPHEUR_MODEL_URI, "certificat");

    /**
     * the user signature is now inside a child node of cm:person.
     * @deprecated since 3.4.0
     */
    @Deprecated
    QName PROP_USER_SIGNATURE = QName.createQName(PARAPHEUR_MODEL_URI, "user-signature");

    QName PROP_DATE_LIMITE = QName.createQName(PARAPHEUR_MODEL_URI, "dateLimite");

    QName PROP_EFFECTUEE = QName.createQName(PARAPHEUR_MODEL_URI, "effectuee");

    QName PROP_TERMINE = QName.createQName(PARAPHEUR_MODEL_URI, "termine");

    QName PROP_CONFIDENTIEL = QName.createQName(PARAPHEUR_MODEL_URI, "confidentiel");

    QName PROP_PUBLIC = QName.createQName(PARAPHEUR_MODEL_URI, "public");

    QName PROP_SIGNATURE_PAPIER = QName.createQName(PARAPHEUR_MODEL_URI, "signature-papier");

    QName PROP_SIGNATURE_ELECTRONIQUE = QName.createQName(PARAPHEUR_MODEL_URI, "signature-electronique"); // deprecated: porté par dossier

    QName PROP_SIGNATURE_ETAPE = QName.createQName(PARAPHEUR_MODEL_URI, "signature-etape"); // porté par étape de circuit

    QName PROP_CLE_PUBLIQUE_ETAPE = QName.createQName(PARAPHEUR_MODEL_URI, "cle-publique-etape"); // pour PKCS#1

    QName PROP_ANNOTATION = QName.createQName(PARAPHEUR_MODEL_URI, "annotation");

    QName PROP_PASSE_PAR = QName.createQName(PARAPHEUR_MODEL_URI, "passe-par");

    QName PROP_ACTION_DEMANDEE = QName.createQName(PARAPHEUR_MODEL_URI, "action-demandee");

    QName PROP_VALIDATOR = QName.createQName(PARAPHEUR_MODEL_URI, "validator");

    /**
     * @deprecated since 3.0. See {@linkplain ParapheurModel#PROP_LISTE_NOTIFICATION} for replacement.
     * @see ParapheurModel#PROP_LISTE_NOTIFICATION
     */
    @Deprecated
    QName PROP_LISTE_DIFFUSION = QName.createQName(PARAPHEUR_MODEL_URI, "liste-diffusion"); // deprecated: porté par dossier

    QName PROP_LISTE_NOTIFICATION = QName.createQName(PARAPHEUR_MODEL_URI, "liste-notification"); // porté par étape de circuit

    QName PROP_LISTE_METADONNEES = QName.createQName(PARAPHEUR_MODEL_URI, "liste-metadonnees"); // porté par étape de circuit

    QName PROP_LISTE_METADONNEES_REFUS = QName.createQName(PARAPHEUR_MODEL_URI, "liste-metadonnees-refus"); // porté par étape de circuit

    QName PROP_WS_EMETTEUR = QName.createQName(PARAPHEUR_MODEL_URI, "ws-emetteur");

    /**
     * @since 3.3.0
     */
    QName PROP_PROPRIETAIRES_PARAPHEUR = QName.createQName(PARAPHEUR_MODEL_URI, "proprietaires");

    /**
     * @deprecated
     * @see ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR
     */
    @Deprecated
    QName PROP_PROPRIETAIRE_PARAPHEUR = QName.createQName(PARAPHEUR_MODEL_URI, "proprietaire");

    QName PROP_RECUPERABLE = QName.createQName(PARAPHEUR_MODEL_URI, "recuperable");

    /**
     * Definit l'etat de dossier: archive, non-lu , etc...
     */
    QName PROP_STATUS_METIER = QName.createQName(PARAPHEUR_MODEL_URI, "status-metier");

    QName PROP_ANNOTATION_PRIVEE = QName.createQName(PARAPHEUR_MODEL_URI, "annotation-privee");

    QName PROP_SECRETAIRES = QName.createQName(PARAPHEUR_MODEL_URI, "secretaires");

    /**
     * Property of a workflow task, holds the name of the actor who did the job.
     */
    QName PROP_SIGNATAIRE = QName.createQName(PARAPHEUR_MODEL_URI, "signataire");

    QName PROP_DATE_VALIDATION = QName.createQName(PARAPHEUR_MODEL_URI, "dateValidation");
    
    /**
     * @since 3.4.0
     */
    QName PROP_DELEGATIONS_POSSIBLES = QName.createQName(PARAPHEUR_MODEL_URI, "delegations-possibles");
    
    QName PROP_DATE_DEBUT_DELEGATION = QName.createQName(PARAPHEUR_MODEL_URI, "date-debut-delegation");
    
    QName PROP_DATE_FIN_DELEGATION = QName.createQName(PARAPHEUR_MODEL_URI, "date-fin-delegation");

    /**
     *
     * @deprecated since 4.1. See {@linkplain ParapheurModel#PROP_PASSE_PAR} for replacement.
     * @see ParapheurModel#PROP_PASSE_PAR
     */
    @Deprecated
    QName PROP_DELEGATEUR = QName.createQName(PARAPHEUR_MODEL_URI, "delegateur");

    QName PROP_SIGN_INFO = QName.createQName(PARAPHEUR_MODEL_URI, "signInfo");

    /**
     * Property of a workflow task, where a signature is claimed.
     * It holds information: either "signature papier", or some clear-text info
     * about the signing certificate that has been really used.
     */
    QName PROP_SIGNATURE = QName.createQName(PARAPHEUR_MODEL_URI, "signature");

    QName PROP_TRANSACTION_ID = QName.createQName(PARAPHEUR_MODEL_URI, "transactionId");

    QName PROP_ARACTE_XML = QName.createQName(PARAPHEUR_MODEL_URI, "ARActeXml");

    QName PROP_NACKHELIOS_XML = QName.createQName(PARAPHEUR_MODEL_URI, "nackHeliosXml");

    QName PROP_STATUS = QName.createQName(PARAPHEUR_MODEL_URI, "status");

    QName PROP_ORIGINAL = QName.createQName(PARAPHEUR_MODEL_URI, "original");

    QName PROP_ORIGINAL_NAME = QName.createQName(PARAPHEUR_MODEL_URI, "originalName");

    QName PROP_SIG = QName.createQName(PARAPHEUR_MODEL_URI, "sig");

    QName PROP_VISUEL_PDF = QName.createQName(PARAPHEUR_MODEL_URI, "visuel-pdf");

    QName PROP_TYPE_METIER = QName.createQName(PARAPHEUR_MODEL_URI, "typeMetier");

    QName PROP_SOUSTYPE_METIER = QName.createQName(PARAPHEUR_MODEL_URI, "soustypeMetier");

    /**
     * @deprecated since 3.2. See {@linkplain ParapheurModel#PROP_SIGNATURE_FORMAT} for replacement.
     * @see ParapheurModel#PROP_SIGNATURE_FORMAT
     */
    @Deprecated
    QName PROP_TYPE_SIGNATURE = QName.createQName(PARAPHEUR_MODEL_URI, "typeSignature");

    /**
     * Format de signature electronique, identifiant interne incompatible API libersign.
     */
    QName PROP_SIGNATURE_FORMAT = QName.createQName(PARAPHEUR_MODEL_URI, "sigFormat");

    QName PROP_SIGNATURE_LOCATION = QName.createQName(PARAPHEUR_MODEL_URI, "sigLocation");

    QName PROP_SIGNATURE_POSTALCODE = QName.createQName(PARAPHEUR_MODEL_URI, "sigPostalCode");
    /**
     * Valeur possible pour PROP_SIGNATURE_FORMAT : "PKCS#7/single"
     */
    String PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7 = "PKCS#7/single";
    /**
     * Valeur possible pour PROP_SIGNATURE_FORMAT : "PKCS#7/multiple"
     */
    String PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7_Ain1 = "PKCS#7/multiple";
    /**
     * Valeur possible pour PROP_SIGNATURE_FORMAT : "PAdES/basic"
     */
    String PROP_SIGNATURE_FORMAT_VAL_PADES_ISO32000_1 = "PAdES/basic";
    /**
     * Valeur possible pour PROP_SIGNATURE_FORMAT : "PAdES/basicCertifie"
     */
    String PROP_SIGNATURE_FORMAT_VAL_PADES_ISO32000_1_CERT = "PAdES/basicCertifie";
    /**
     * Valeur possible pour PROP_SIGNATURE_FORMAT : "XAdES/enveloped"
     */
    String PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_ENV_PESv2 = "XAdES/enveloped";
    /**
     * Valeur possible pour PROP_SIGNATURE_FORMAT : "XAdES/DIA"
     */
    String PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_ENV_DIA = "XAdES/DIA";
    /**
     * Valeur possible pour PROP_SIGNATURE_FORMAT : "XAdES/detached"
     */
    String PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_1_1 = "XAdES/detached";
    /**
     * Valeur possible pour PROP_SIGNATURE_FORMAT : "XAdES132/detached"
     */
    String PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_3_2 = "XAdES132/detached";
    /**
     * Valeur possible pour PROP_SIGNATURE_FORMAT : XAdES-T/enveloped"
     */
    String PROP_SIGNATURE_FORMAT_VAL_XADES_T_EPES_ENV_1_3_2 = "XAdES-T/enveloped";
    /**
     * Valeur possible pour PROP_SIGNATURE_FORMAT : PKCS#1/sha256"
     */
    String PROP_SIGNATURE_FORMAT_VAL_PKCS1_256 = "PKCS#1/sha256";

    QName PROP_XPATH_SIGNATURE = QName.createQName(PARAPHEUR_MODEL_URI, "xpathSignature");

    QName PROP_TDT_NOM = QName.createQName(PARAPHEUR_MODEL_URI, "tdtNom");

    QName PROP_TDT_PROTOCOLE = QName.createQName(PARAPHEUR_MODEL_URI, "tdtProtocole");
    // BLEX : et si on arretait de mettre des chaines en dur un peu partout ?
    //  -> Adullact veut bien, les contributions sont bienvenues 
    /**
     * Valeur possible pour PROP_TDT_PROTOCOLE : "HELIOS"
     * @author BLEX
     */
    String PROP_TDT_PROTOCOLE_VAL_HELIOS="HELIOS";
    /**
     * Valeur possible pour PROP_TDT_PROTOCOLE : "ACTES"
     * @author Stephane Vast - Adullact Projet
     */
    String PROP_TDT_PROTOCOLE_VAL_ACTES="ACTES";

    QName PROP_MAILSEC_MAIL_ID = QName.createQName(PARAPHEUR_MODEL_URI, "mailsec-mail-id");

    QName PROP_MAILSEC_MAIL_STATUS = QName.createQName(PARAPHEUR_MODEL_URI, "mailsec-mail-status");

    QName PROP_TDT_FICHIER_CONFIG = QName.createQName(PARAPHEUR_MODEL_URI, "tdtFichierConfig");

    QName PROP_TDT_ACTES_NATURE = QName.createQName(PARAPHEUR_MODEL_URI, "tdtActesNature");

    QName PROP_TDT_ACTES_CLASSIFICATION = QName.createQName(PARAPHEUR_MODEL_URI, "tdtActesClassification");

    QName PROP_TDT_ACTES_DATE = QName.createQName(PARAPHEUR_MODEL_URI, "tdtActesDate");

    QName PROP_TDT_ACTES_OBJET = QName.createQName(PARAPHEUR_MODEL_URI, "tdtActesObjet");

    QName PROP_EMAIL_ENABLED = QName.createQName(PARAPHEUR_MODEL_URI, "listenToEmailAspectEnabled");

    QName PROP_EMAILPROTOCOL = QName.createQName(PARAPHEUR_MODEL_URI, "emailProtocol");

    QName PROP_EMAILSERVER = QName.createQName(PARAPHEUR_MODEL_URI, "emailServer");

    QName PROP_EMAILPORT = QName.createQName(PARAPHEUR_MODEL_URI, "emailPort");

    QName PROP_EMAILFOLDER = QName.createQName(PARAPHEUR_MODEL_URI, "emailInbox");

    QName PROP_EMAILUSERNAME = QName.createQName(PARAPHEUR_MODEL_URI, "emailUsername");

    QName PROP_EMAILPASSWORD = QName.createQName(PARAPHEUR_MODEL_URI, "emailPassword");

    QName PROP_CIRCUIT_ACL_PARAPHEURS = QName.createQName(PARAPHEUR_MODEL_URI, "circuit-acl-parapheurs");

    QName PROP_CIRCUIT_ACL_GROUPES = QName.createQName(PARAPHEUR_MODEL_URI, "circuit-acl-groupes");

    QName PROP_WORKFLOW = QName.createQName(PARAPHEUR_MODEL_URI, "workflow");

    QName PROP_READING_MANDATORY = QName.createQName(PARAPHEUR_MODEL_URI, "reading-mandatory");

    /**
     * Propriete de dossier, indique que si la signature electronique doit etre
     * obligatoire (true), ou si la signature papier est autorisee (false).
     * @since 3.2.2
     */
    QName PROP_DIGITAL_SIGNATURE_MANDATORY = QName.createQName(PARAPHEUR_MODEL_URI, "digital-signature-mandatory");

    /**
     * Properties containing a list of parapheurs allowed to access this workflow.
     *
     * This property is part of the private workflow aspect.
     *
     * @since 3.2
     * @see ParapheurModel#ASPECT_PRIVATE_WORKFLOW
     */
    QName PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS = QName.createQName(PARAPHEUR_MODEL_URI, "acl_parapheurs");

    /**
     * Properties containing a list of groups allowed to access this workflow.
     *
     * This property is part of the private workflow aspect.
     *
     * @since 3.2
     * @see ParapheurModel#ASPECT_PRIVATE_WORKFLOW
     */
    QName PROP_PRIVATE_WORKFLOW_ACL_GROUPS = QName.createQName(PARAPHEUR_MODEL_URI, "acl_groups");

    QName PROP_CHILD_COUNT = QName.createQName(PARAPHEUR_MODEL_URI, "childCount");

    QName PROP_CORBEILLE_LATE = QName.createQName(PARAPHEUR_MODEL_URI, "corbeilleLate");

    QName CHILD_ASSOC_PREMIERE_ETAPE = QName.createQName(PARAPHEUR_MODEL_URI, "premiere-etape");

    QName CHILD_ASSOC_PROCHAINE_ETAPE = QName.createQName(PARAPHEUR_MODEL_URI, "prochaine-etape");

    QName CHILD_ASSOC_OLD_PREMIERE_ETAPE = QName.createQName(PARAPHEUR_MODEL_URI, "old-premiere-etape");

    QName ASSOC_HIERARCHIE = QName.createQName(PARAPHEUR_MODEL_URI, "hierarchie");

    QName ASSOC_DOSSIER_ETAPE = QName.createQName(PARAPHEUR_MODEL_URI, "Association_dossier_etape");

    QName ASSOC_DELEGATION = QName.createQName(PARAPHEUR_MODEL_URI, "delegation");

    QName ASSOC_OLD_DELEGATION = QName.createQName(PARAPHEUR_MODEL_URI, "old-delegation");

    QName ASSOC_VIRTUALLY_CONTAINS = QName.createQName(PARAPHEUR_MODEL_URI, "virtuallyContains");

    QName ASPECT_SECRETARIAT = QName.createQName(PARAPHEUR_MODEL_URI, "secretariat");

    QName ASPECT_LU = QName.createQName(PARAPHEUR_MODEL_URI, "lu");

    QName ASPECT_S2LOW = QName.createQName(PARAPHEUR_MODEL_URI, "s2low");

    QName ASPECT_SIGNED = QName.createQName(PARAPHEUR_MODEL_URI, "signed");

    QName ASPECT_TYPAGE_METIER = QName.createQName(PARAPHEUR_MODEL_URI, "typageMetier");

    QName ASPECT_DOSSIER_ORIGINE_WS = QName.createQName(PARAPHEUR_MODEL_URI, "origine-ws");

    QName ASPECT_CIRCUIT_ACCESS_LISTE = QName.createQName(PARAPHEUR_MODEL_URI, "circuit-access-liste");

    QName ASPECT_ETAPE_DELEGATION = QName.createQName(PARAPHEUR_MODEL_URI, "etape-delegation");

    QName ASPECT_EMAIL_LISTENER = QName.createQName(PARAPHEUR_MODEL_URI, "emailListener");

    /**
     * @since 3.4.0
     */
    QName ASPECT_NOTIFICATION = QName.createQName(PARAPHEUR_MODEL_URI, "notification");

    /**
     * @since 4.4.0
     */
    //Pour documents
    QName ASPECT_ATTEST = QName.createQName(PARAPHEUR_MODEL_URI, "attest");
    //Pour dossier
    QName ASPECT_ETAPE_ATTEST = QName.createQName(PARAPHEUR_MODEL_URI, "has-to-attest");
    QName PROP_ATTEST_ID = QName.createQName(PARAPHEUR_MODEL_URI, "attest-id");
    QName PROP_ATTEST_CONTENT = QName.createQName(PARAPHEUR_MODEL_URI, "attest-content");

    QName TYPE_NOTIFICATION = QName.createQName(PARAPHEUR_MODEL_URI, "notification-type");
    QName ASSOC_NOTIFICATION = QName.createQName(PARAPHEUR_MODEL_URI, "notifications");

    @Deprecated
    QName PROP_NOTIFICATION_ENABLED = QName.createQName(PARAPHEUR_MODEL_URI, "notification-enabled");

    QName PROP_NOTIFICATION_MAP = QName.createQName(PARAPHEUR_MODEL_URI, "notification-map");
    QName PROP_NOTIFICATION_UNREAD_LIST = QName.createQName(PARAPHEUR_MODEL_URI, "notification-unread-list");

    /* Special state where the user can't see the dossier or can't act on it */
    public QName PROP_IN_BATCH_QUEUE = QName.createQName(PARAPHEUR_MODEL_URI, "in_batch_queue");

    /**
     * This aspect represents a saved workflow which has to be private.
     *
     * This aspect contains 2 properties:
     * <ul>
     *     <li>acl_parapheurs: a list of parapheurs allowed to access this workflow</li>
     *     <li>acl_groups: a list of groups allowed to access this workflow</li>
     * </ul>
     *
     * @since 3.2
     * @see ParapheurModel#PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS
     * @see ParapheurModel#PROP_PRIVATE_WORKFLOW_ACL_GROUPS
     */
    QName ASPECT_PRIVATE_WORKFLOW = QName.createQName(PARAPHEUR_MODEL_URI, "private_workflow");

    QName NAME_A_TRAITER = QName.createQName(PARAPHEUR_MODEL_URI, "a-traiter");

    QName NAME_A_ARCHIVER = QName.createQName(PARAPHEUR_MODEL_URI, "a-archiver");

    QName NAME_RETOURNES = QName.createQName(PARAPHEUR_MODEL_URI, "retournes");

    QName NAME_EN_PREPARATION = QName.createQName(PARAPHEUR_MODEL_URI, "en-preparation");

    QName NAME_EN_COURS = QName.createQName(PARAPHEUR_MODEL_URI, "en-cours");

    QName NAME_TRAITES = QName.createQName(PARAPHEUR_MODEL_URI, "traites");

    QName NAME_RECUPERABLES = QName.createQName(PARAPHEUR_MODEL_URI, "recuperables");

    QName NAME_SECRETARIAT = QName.createQName(PARAPHEUR_MODEL_URI, "secretariat");

    QName NAME_A_VENIR = QName.createQName(PARAPHEUR_MODEL_URI, "a-venir");

    QName NAME_EN_RETARD = QName.createQName(PARAPHEUR_MODEL_URI, "en-retard");

    QName NAME_A_IMPRIMER = QName.createQName(PARAPHEUR_MODEL_URI, "a-imprimer");

    QName PROP_ORDERED_CHILDREN = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "orderedchildren");

    QName ASPECT_HABILITATIONS = QName.createQName(PARAPHEUR_MODEL_URI, "habilitations");

    QName PROP_HAB_TRANSMETTRE = QName.createQName(PARAPHEUR_MODEL_URI, "hab_transmettre");

    QName PROP_HAB_TRAITER = QName.createQName(PARAPHEUR_MODEL_URI, "hab_traiter");

    QName PROP_HAB_SECRETARIAT = QName.createQName(PARAPHEUR_MODEL_URI, "hab_secretariat");

    QName PROP_HAB_ARCHIVAGE = QName.createQName(PARAPHEUR_MODEL_URI, "hab_archivage");

    QName PROP_HAB_ENCHAINEMENT = QName.createQName(PARAPHEUR_MODEL_URI, "hab_enchainement");

    /**
     * Metadonnée de priorité d'un dossier
     * @since 3.3
     */
    QName METADATA_PRIORITY = QName.createQName(MetadataServiceImpl.METADATA_DEF_CUSTOM_URI, "ph-priority");

    /**
     * @since 3.4.0
     */
    QName ASPECT_SIGNATURESCAN = QName.createQName(PARAPHEUR_MODEL_URI, "signatureScan");
    QName ASSOC_SIGNATURE_SCAN = QName.createQName(PARAPHEUR_MODEL_URI, "scan-signature");

    /**
     * @since 3.4.0
     */
    QName PROP_UNCOMPLETE = QName.createQName(PARAPHEUR_MODEL_URI, "dossier-uncomplete");

    QName PROP_FULL_CREATED = QName.createQName(PARAPHEUR_MODEL_URI, "dossier-created");

    QName PROP_ANNOTATIONS_GRAPHIQUES = QName.createQName(PARAPHEUR_MODEL_URI, "annotations-graphiques");

    QName PROP_ANNOTATIONS_GRAPHIQUES_MULTIDOC = QName.createQName(PARAPHEUR_MODEL_URI, "annotations-graphiques-multidoc");
    
    QName NAME_DOSSIERS_DELEGUES = QName.createQName(PARAPHEUR_MODEL_URI, "dossiers-delegues");

    QName PROP_SHA1 = QName.createQName(PARAPHEUR_MODEL_URI, "sha1-digest");

    QName ASPECT_MAIN_DOCUMENT = QName.createQName(PARAPHEUR_MODEL_URI, "main-document");
    
    QName PROP_DELEGUER_PRESENTS = QName.createQName(PARAPHEUR_MODEL_URI, "deleguer-presents");
    
    QName ASSOC_DELEGATION_PROGRAMMEE = QName.createQName(PARAPHEUR_MODEL_URI, "delegation-programmee");
    
    QName PROP_DOCUMENT_DELETABLE = QName.createQName(PARAPHEUR_MODEL_URI, "document-deletable");
    
    QName ASSOC_VIRTUALLY_REFERS = QName.createQName(PARAPHEUR_MODEL_URI, "virtuallyRefers");
    
    QName PROP_DELEGUE = QName.createQName(PARAPHEUR_MODEL_URI, "delegue");
    
    QName PROP_SHOW_A_VENIR = QName.createQName(PARAPHEUR_MODEL_URI, "show-a-venir");
    
    QName PROP_ACTEURS_EXTERNES = QName.createQName(PARAPHEUR_MODEL_URI, "acteurs-externes");
    
    /**
     * Permet d'identifier les étapes ajoutées en cours de circuit
     * (change signataire, avis complémentaire, ...).
     */
    QName ASPECT_ETAPE_NON_NATIVE = QName.createQName(PARAPHEUR_MODEL_URI, "etape-non-native");
    
    QName PROP_INCLUDE_ATTACHMENTS = QName.createQName(PARAPHEUR_MODEL_URI, "include-attachments");
    
    /**
     * Sert à identifier les étapes complémentaires des étapes non natives pour empêcher le rejet
     * lors d'une étape complémentaire.
     */
    QName ASPECT_ETAPE_COMPLEMENTAIRE = QName.createQName(PARAPHEUR_MODEL_URI, "etape-complementaire");
    
    QName PROP_LECTEURS = QName.createQName(PARAPHEUR_MODEL_URI, "lecteurs");
    
    /**
     * @since 4.0
     */

    QName PROP_DOCUMENT_PAGE_COUNT = QName.createQName(PARAPHEUR_MODEL_URI, "page-count");

    /**
     * Aspect sur un dossier, indiquant qu'il est en retard.
     * TODO : à enrichir avec le temps de traitement sur chaque étape (future évolution).
     *  Peut être une association vers l'étape courante dans cet aspect.
     */
    QName ASPECT_EN_RETARD = QName.createQName(PARAPHEUR_MODEL_URI, "en-retard");;

    QName ASPECT_PENDING = QName.createQName(PARAPHEUR_MODEL_URI, "pending");

    QName PROP_LOCKED = QName.createQName(PARAPHEUR_MODEL_URI, "locked");

    /**
     * Aspect sur un dossier.
     * S'il y a une étape de TDT vers Helios et que le sous-type de ce dossier
     * indique que l'étape doit être automatique, alors cet aspet est rajouté sur
     * le dossier à la création.
     */
    QName ASPECT_ETAPE_TDT_AUTO = QName.createQName(PARAPHEUR_MODEL_URI, "etape-tdt-auto");
    // Idem pour le cachet
    QName ASPECT_ETAPE_CACHET_AUTO = QName.createQName(PARAPHEUR_MODEL_URI, "etape-cachet-auto");

    /**
     * Visibilité des métadonnées sur un bureau
     */
    QName PROP_VISIBILITE_METADONNE = QName.createQName(PARAPHEUR_MODEL_URI, "metadatas-visibility");

    /**
     * Utilisation d'un circuit par un sous-type
     */
    QName PROP_USED_BY = QName.createQName(PARAPHEUR_MODEL_URI, "is-used-by");

    QName PROP_CUSTOM_SIGNATURE_POSITION_RECTANGLE = QName.createQName(PARAPHEUR_MODEL_URI, "customSignaturePositionRectangle");
    QName PROP_CUSTOM_SIGNATURE_PAGE_NUMBER = QName.createQName(PARAPHEUR_MODEL_URI, "customSignaturePageNumber");
    QName PROP_SIGNATURE_TAG_POSITION_RECTANGLE = QName.createQName(PARAPHEUR_MODEL_URI, "signatureTagPositionRectangle");
    QName PROP_SIGNATURE_TAG_PAGE_NUMBER = QName.createQName(PARAPHEUR_MODEL_URI, "signatureTagPageNumber");

    /**
     * @since 4.4
     */
    String PARAPHEUR_XEM_CMD_KEY = "parapheur.exploit.xemelios.command";
    String PARAPHEUR_XEM_CMD_KEY_DEFAULT = "/etc/init.d/xemwebview";

    QName PROP_ACTEURS_VARIABLES = QName.createQName(PARAPHEUR_MODEL_URI, "acteurs-variables");

    /**
     * @since 4.5
     */
    QName PROP_SIGNATURE_PDF = QName.createQName(PARAPHEUR_MODEL_URI, "signature-pdf");

    /**
     * @since 4.6.4
     */
    QName PROP_RESET_PASSWORD_UUID = QName.createQName(PARAPHEUR_MODEL_URI, "reset-password-uuid");


    /**
     * @since 4.7.0
     */
    QName TYPE_SIGNATURES_FOLDER = QName.createQName(PARAPHEUR_MODEL_URI, "signatures-folder");

    /**
     * @since 4.7.8
     */
    QName PROP_RESET_PASSWORD_DATE = QName.createQName(PARAPHEUR_MODEL_URI, "reset-password-date");
}
