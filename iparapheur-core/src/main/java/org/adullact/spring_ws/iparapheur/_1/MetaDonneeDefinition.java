
package org.adullact.spring_ws.iparapheur._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Définition de méta donnée dans le dictionnaire du modèle de contenu dynamique.
 * 
 * <p>Java class for MetaDonneeDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MetaDonneeDefinition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nomCourt" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeNomCourt"/>
 *         &lt;element name="nomLong" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nature" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}NatureMetaDonnee"/>
 *         &lt;element name="obligatoire" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="valeurPossible" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetaDonneeDefinition", propOrder = {
    "nomCourt",
    "nomLong",
    "nature",
    "obligatoire",
    "valeurPossible"
})
public class MetaDonneeDefinition {

    @XmlElement(required = true)
    protected String nomCourt;
    @XmlElement(required = true)
    protected String nomLong;
    @XmlElement(required = true)
    protected NatureMetaDonnee nature;
    protected Boolean obligatoire;
    protected List<String> valeurPossible;

    /**
     * Gets the value of the nomCourt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomCourt() {
        return nomCourt;
    }

    /**
     * Sets the value of the nomCourt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomCourt(String value) {
        this.nomCourt = value;
    }

    /**
     * Gets the value of the nomLong property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomLong() {
        return nomLong;
    }

    /**
     * Sets the value of the nomLong property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomLong(String value) {
        this.nomLong = value;
    }

    /**
     * Gets the value of the nature property.
     * 
     * @return
     *     possible object is
     *     {@link NatureMetaDonnee }
     *     
     */
    public NatureMetaDonnee getNature() {
        return nature;
    }

    /**
     * Sets the value of the nature property.
     * 
     * @param value
     *     allowed object is
     *     {@link NatureMetaDonnee }
     *     
     */
    public void setNature(NatureMetaDonnee value) {
        this.nature = value;
    }

    /**
     * Gets the value of the obligatoire property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isObligatoire() {
        return obligatoire;
    }

    /**
     * Sets the value of the obligatoire property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setObligatoire(Boolean value) {
        this.obligatoire = value;
    }

    /**
     * Gets the value of the valeurPossible property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valeurPossible property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValeurPossible().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getValeurPossible() {
        if (valeurPossible == null) {
            valeurPossible = new ArrayList<String>();
        }
        return this.valeurPossible;
    }

}
