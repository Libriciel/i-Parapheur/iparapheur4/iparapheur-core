/*
 * Version 2.1
 * CeCILL Copyright (c) 2008-2013, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.spring_ws.iparapheur._1;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.impl.FastServiceImpl;
import com.atolcd.parapheur.repo.impl.exceptions.SubTypeNotFoundRuntimeException;
import coop.libriciel.service.CreerDossierService;
import coop.libriciel.service.WsService;
import coop.libriciel.signature.exception.SignatureNotFoundException;
import coop.libriciel.signature.model.Signature;
import coop.libriciel.signature.service.SignatureService;
import fr.bl.iparapheur.srci.SrciService;
import fr.bl.tdtproxysrci.TPS;
import org.adullact.iparapheur.repo.amq.MessagesSender;
import org.adullact.iparapheur.status.StatusMetier;
import org.adullact.iparapheur.tdt.s2low.TransactionStatus;
import org.adullact.utils.StringUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.dictionary.InvalidTypeException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.repository.datatype.DefaultTypeConverter;
import org.alfresco.service.cmr.security.NoSuchPersonException;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.annotation.Resource;
import javax.jws.WebParam;
import javax.servlet.ServletContext;
import javax.transaction.UserTransaction;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.io.*;
import java.math.BigInteger;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Skeleton designed with the help from E. Delmasure - Berger Levrault
 *
 * @author Stephane Vast - ADULLACT Projet
 */
public class InterfaceParapheurImpl implements InterfaceParapheur, InitializingBean {

    private static final Logger logger = Logger.getLogger(InterfaceParapheurImpl.class);
    @Resource
    public WebServiceContext webServiceContext;
    private String pErrMsg = null;
    private ServiceRegistry serviceRegistry;
    private NodeService nodeService;
    private ContentService contentService;
    private AuthenticationComponent authenticationComponent;
    private TransactionService transactionService;
    private PermissionService permissionService;
    private PersonService personService;
    private ParapheurService parapheurService;
    private ParapheurService nakedParapheurService;
    private NamespaceService namespaceService;
    private S2lowService s2lowService;
    // BLEX
    private SrciService srciService = null;
    private TypesService typesService;
    private TenantService tenantService;
    private MetadataService metadataService;
    private WorkflowService workflowService;
    private DossierService dossierService;
    private MessagesSender messagesSender;
    private CreerDossierService creerDossierService;
    private WsService wsService;
    private SignatureService signatureService;

    private static byte[] getBytesFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        // Close the input stream and return bytes
        is.close();
        return bytes;
    }

    /**
     * Lazy initialization
     *
     * @return le serviceRegistry d'Alfresco
     */
    public ServiceRegistry getServiceRegistry() {
        if (null == serviceRegistry) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

            try {
                serviceRegistry = (ServiceRegistry) appContext.getBean("ServiceRegistry");
            } catch (BeansException e) {
                logger.error("Can not get serviceRegistry.", e);
            }
        }
        return serviceRegistry;
    }

    /**
     * Lazy initialization
     *
     * @return le creerDossierService
     */
    public CreerDossierService getCreerDossierService() {
        if (null == creerDossierService) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

            try {
                creerDossierService = (CreerDossierService) appContext.getBean("creerDossierService");
            } catch (BeansException e) {
                logger.error("Can not get serviceRegistry.", e);
            }
        }
        return creerDossierService;
    }

    /**
     * Lazy initialization
     *
     * @return le creerDossierService
     */
    public SignatureService getSignatureService() {
        if (null == signatureService) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

            try {
                signatureService = (SignatureService) appContext.getBean("signatureService");
            } catch (BeansException e) {
                logger.error("Can not get serviceRegistry.", e);
            }
        }
        return signatureService;
    }

    /**
     * Lazy initialization
     *
     * @return le wsService
     */
    public WsService getWsService() {
        if (null == wsService) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

            try {
                wsService = (WsService) appContext.getBean("wsService");
            } catch (BeansException e) {
                logger.error("Can not get serviceRegistry.", e);
            }
        }
        return wsService;
    }


    public void setserviceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public MessagesSender getMessagesSender() {
        if (null == messagesSender) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
            try {
                messagesSender = (MessagesSender) appContext.getBean("messagesSender");
            } catch (BeansException e) {
                logger.error("Can not get messagesSender.", e);
            }
        }
        return messagesSender;
    }

    public void setMessagesSender(MessagesSender messagesSender) {
        this.messagesSender = messagesSender;
    }

    public NodeService getNodeService() {
        if (null == nodeService) {
            nodeService = this.getServiceRegistry().getNodeService();
        }
        return nodeService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public ContentService getContentService() {
        if (null == contentService) {
            contentService = this.getServiceRegistry().getContentService();
        }
        return contentService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public NamespaceService getNamespaceService() {
        if (null == namespaceService) {
            namespaceService = this.getServiceRegistry().getNamespaceService();
        }
        return namespaceService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public AuthenticationComponent getAuthenticationComponent() {
        if (null == authenticationComponent) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
            try {
                authenticationComponent = (AuthenticationComponent) appContext.getBean("authenticationComponent");
            } catch (BeansException e) {
                logger.error("Can not get authenticationComponent.", e);
            }
        }
        return authenticationComponent;
    }

    public void setauthenticationComponent(AuthenticationComponent authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }

    public TransactionService getTransactionService() {
        if (null == transactionService) {
            transactionService = this.getServiceRegistry().getTransactionService();
        }
        return transactionService;
    }

    public void settransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public PermissionService getPermissionService() {
        if (null == permissionService) {
            permissionService = this.getServiceRegistry().getPermissionService();
        }
        return permissionService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public PersonService getPersonService() {
        if (null == personService) {
            personService = this.getServiceRegistry().getPersonService();
        }
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public ParapheurService getParapheurService() {
        if (null == parapheurService) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
            try {
                parapheurService = (ParapheurService) appContext.getBean("_parapheurService");
            } catch (BeansException e) {
                logger.error("Can not get parapheurService.", e);
            }
        }
        return parapheurService;
    }

    public ParapheurService getNakedParapheurService() {
        if (null == nakedParapheurService) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
            try {
                nakedParapheurService = (ParapheurService) appContext.getBean("parapheurService");
            } catch (BeansException e) {
                logger.error("Can not get parapheurService.", e);
            }
        }
        return nakedParapheurService;
    }

    public TenantService getTenantService() {
        if (null == tenantService) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
            try {
                tenantService = (TenantService) appContext.getBean("tenantService");
            } catch (BeansException e) {
                logger.error("Can not get tenantService.", e);
            }
        }
        return tenantService;
    }

    public void setparapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public S2lowService getS2lowService() {
        if (null == s2lowService) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
            try {
                s2lowService = (S2lowService) appContext.getBean("S2lowService");
            } catch (BeansException e) {
                logger.error("Can not get s2lowService.", e);
            }
        }
        return s2lowService;
    }

    public void setS2lowService(S2lowService service) {
        this.s2lowService = service;
    }

    // BLEX
    public SrciService getSrciService() {
        if (null == srciService) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
            try {
                srciService = (SrciService) appContext.getBean("srciService");
            } catch (BeansException e) {
                logger.error("Can not get srciService.", e);
            }
        }
        return srciService;
    }

    public TypesService getTypesService() {
        if (null == typesService) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
            try {
                typesService = (TypesService) appContext.getBean("typesService");
            } catch (BeansException e) {
                logger.error("Can not get typesService.", e);
            }
        }
        return typesService;
    }

    public WorkflowService getWorkflowService() {
        if (null == workflowService) {
            ApplicationContext appContext = getApplicationContext();
            try {
                workflowService = (WorkflowService) appContext.getBean("workflowService");
            } catch (BeansException e) {
                logger.error("Can not get workflowService", e);
            }
        }
        return workflowService;
    }

    public DossierService getDossierService() {
        if (null == dossierService) {
            ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
            try {
                dossierService = (DossierService) appContext.getBean("_dossierService");
            } catch (BeansException e) {
                logger.error("Can not get dossierService.", e);
            }
        }
        return dossierService;
    }

    public void setDossierService(DossierService dossierService) {
        this.dossierService = dossierService;
    }

    public MetadataService getMetadataService() {
        if (null == metadataService) {
            ApplicationContext appContext = getApplicationContext();
            try {
                metadataService = (MetadataService) appContext.getBean("metadataService");
            } catch (BeansException e) {
                logger.error("Can not get metadataService.", e);
            }
        }
        return metadataService;
    }

    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    private ApplicationContext getApplicationContext() {
        ServletContext servletContext = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        ApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        return appContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(nodeService, "There must be a node service");
        Assert.notNull(transactionService, "There must be a transaction service");
        Assert.notNull(parapheurService, "There must be a parapheur service");
        Assert.notNull(s2lowService, "There must be a S2LOW service");
    }

    @Override
    public ArchiverDossierResponse archiverDossier(ArchiverDossierRequest archiverDossierRequest) {
        String action;
        final ArchiverDossierResponse res = new ArchiverDossierResponse();
        final MessageRetour msg = new MessageRetour();
        // Bouchon
        msg.setCodeRetour("KO");
        msg.setSeverite("INFO");
        msg.setMessage("Service non implémenté");
        if (archiverDossierRequest == null) {
            msg.setSeverite("FATAL");
            msg.setMessage("Requete vide: parametre null.");
            res.setMessageRetour(msg);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error(msg.getCodeRetour() + ": " + msg.getMessage());
            }
            return res;
        }
        if (archiverDossierRequest.getArchivageAction() == null) {
            action = "ARCHIVER";    // action par défaut
        } else {
            action = archiverDossierRequest.getArchivageAction().value();
        }
        if (action == null || action.trim().isEmpty()) {
            action = "ARCHIVER";    // action par défaut
        }
        String dossierID = archiverDossierRequest.getDossierID();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        // Check parametres en entrée. validité dossierID, puis de l'action
        final String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService archiverDossier invoqué par " + userName
                    + ": \n\tdossier:" + dossierID + ", \taction=" + action
                    + "/(" + archiverDossierRequest.getArchivageAction() + ").");
        }
        if (userName == null) {
            msg.setMessage(this.pErrMsg);
        } else if (dossierID == null) {
            msg.setMessage("dossierID manquant");
        } else if (!isNomDossierValide(dossierID = dossierID.trim())) {
            msg.setMessage("Le champ dossierID a une syntaxe incorrecte.");
        } else if (action.trim().equalsIgnoreCase("EFFACER") || action.trim().equalsIgnoreCase("ARCHIVER")) {
            final RetryingTransactionHelper tx;
            tx = this.getTransactionService().getRetryingTransactionHelper();

            final AuthenticationComponent authComp = this.getAuthenticationComponent();
            final DossierService finalDossierService = this.getDossierService();
            final String finalAction = action;
            final String finalDossierID = dossierID;

            tx.setMaxRetries(5);
            try {
                tx.doInTransaction(new RetryingTransactionHelper.RetryingTransactionCallback<Object>() {

                    @Override
                    public Object execute() throws Throwable {
                        try {
                            /*String clientEmail = "app-metier@adullact.org";
                            NodeRef emetteurNodeRef = parapheurService.findUserByEmail(clientEmail);
                            if (emetteurNodeRef == null)		{
                            logger.debug("emetteurNodeRef est NULL: email '"+clientEmail+"' est inconnu dans le parapheur!");
                            throw new DocumentException("l'email '"+clientEmail+"' est inconnu dans le parapheur!");		}
                            String userName = (String) nodeService.getProperty(emetteurNodeRef, ContentModel.PROP_USERNAME);
                            authenticationComponent.setCurrentUser(userName);
                            NodeRef parapheur = parapheurService.getOwnedParapheur(userName);	*/

                            authComp.setCurrentUser(userName);
                            NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
                            if (parapheur == null) {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("parapheur == null !!");
                                }
                                msg.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                                throw new DocumentException(userName + " n'a pas de parapheur.");
                            }

                            // Contrôle que le dossierID existe, que c'est un dossier..., qu'il est dans la bannette "à archiver" du bon utilisateur.
                            NodeRef dossierRef = checkForNodeRefFromDossierID(parapheur, finalDossierID);
                            if (dossierRef != null && parapheurService.isDossier(dossierRef)) {
                                NodeRef corbeilleRef = parapheurService.getParentCorbeille(dossierRef);
                                if (finalAction.trim().equalsIgnoreCase("EFFACER")) {
                                    // Est-ce que le dossier est dans la bonne corbeille?
                                    if (!parapheurService.getCorbeille(parapheur, ParapheurModel.NAME_A_ARCHIVER).getId().equals(corbeilleRef.getId())
                                            && !parapheurService.getCorbeille(parapheur, ParapheurModel.NAME_EN_PREPARATION).getId().equals(corbeilleRef.getId())) {
                                        // BLEX
                                        /*
                                        msg.setMessage("Le dossierID '" + dossierID + "' n'est pas accessible.");
                                        throw new DocumentException("Le dossierID '" + dossierID + "' n'est pas accessible.");
                                         */
                                        // BLEX
                                        String text = "Le dossier '" + finalDossierID + "' n'est pas dans une corbeille permettant la suppression.";
                                        msg.setMessage(text);
                                        throw new DocumentException(text);
                                    }

                                    Integer mail_id = (Integer) getNodeService().getProperty(dossierRef, ParapheurModel.PROP_MAILSEC_MAIL_ID);
                                    if ((mail_id != null) && getS2lowService().isMailServiceEnabled()) {
                                        getS2lowService().deleteSecureMail(mail_id);
                                    }
                                    parapheurService.handleDeleteDossier(dossierRef, parapheur, false, userName);
//                                    finalDossierService.deleteDossier(dossierRef, false);
                                    msg.setCodeRetour("OK");
                                    msg.setSeverite("INFO");
                                    msg.setMessage("Dossier " + finalDossierID + " supprimé du Parapheur.");
                                } else {   //on archive...
                                    // Est-ce que le dossier est dans la bonne corbeille?
                                    if (!parapheurService.getCorbeille(parapheur, ParapheurModel.NAME_A_ARCHIVER).getId().equals(corbeilleRef.getId())
                                            || parapheurService.getActeursCourant(dossierRef) == null || parapheurService.getActeursCourant(dossierRef).isEmpty()) {
                                        msg.setMessage("Le dossierID '" + finalDossierID + "' n'est pas archivable.");
                                        throw new DocumentException("Le dossierID '" + finalDossierID + "' n'est pas archivable.");
                                    }
                                    if (!parapheurService.getActeursCourant(dossierRef).contains(userName)) {
                                        msg.setMessage("Vous n'etes pas acteur courant du dossier '" + finalDossierID + "'.");
                                        throw new DocumentException("Vous n'etes pas acteur courant du dossier '" + finalDossierID + "'.");
                                    }
                                    String urlArchivage = parapheurService.archiver(dossierRef, null);
                                    // Delete folder after archive
                                    parapheurService.handleDeleteDossier(dossierRef, parapheur, false, userName);
                                    ObjectFactory objectFactory = new ObjectFactory();
                                    res.setURL(objectFactory.createArchiverDossierResponseURL(urlArchivage));
                                    msg.setCodeRetour("OK");
                                    msg.setSeverite("INFO");
                                    msg.setMessage("Dossier " + finalDossierID + " archivé.");
                                }
                            } else {
                                if (logger.isEnabledFor(Level.WARN)) {
                                    logger.warn("Le dossierID '" + finalDossierID + "' est inconnu dans le Parapheur.");
                                }
                                msg.setMessage("Le dossierID '" + finalDossierID + "' est inconnu dans le Parapheur.");
                                throw new DocumentException("Le dossierID '" + finalDossierID + "' est inconnu dans le Parapheur.");
                            }
                        } catch (org.alfresco.service.cmr.model.FileExistsException efile) {
                            //logger.error(efile.getMessage(), efile);
                            msg.setMessage("Archivage impossible: une archive existante porte déjà ce nom de dossier.");
                            throw efile;
                        } catch (Exception e) {
                            throw new ConcurrencyFailureException(e.getMessage());
                        }
                        return null;
                    }
                });
            } catch (Exception e) {
                logger.error(e.getLocalizedMessage());
                msg.setMessage(e.getMessage());
            }
        } else {
            msg.setMessage("champ Action inconnu.");
        }
        res.setMessageRetour(msg);
        if (logger.isDebugEnabled()) {
            logger.debug(msg.getCodeRetour() + ": " + msg.getMessage());
        }
        return res;
    }

    /**
     * Création d'un dossier.
     * <br/>Elements principaux:
     * <ul>
     * <li>TypeTechnique et SousType sont obligatoires en toute circonstance.</li>
     * <li>dossierID est obligatoire en toute circonstance.
     * si il est présent mais vide, ET que dossierTitre est présent,
     * alors le dossierID est inventé par i-Parapheur puis retourné. </li>
     * <li>dossierTitre est optionnel. Si absent, le titre = dossierID.</li>
     * <li>nomDocumentPrincipal est optionnel. Si absent: prend le dossierID</li>
     * </ul>
     * <p/>Elements optionnels:
     * <ul>
     * <li> MetaData: permet de caractériser le dossier avec des propriétés
     * supplémentaires définies dans le dictionnaire de méta-données.</li>
     * - MetaDataTdTACTES: ne sert que pour les dossiers à télétransmettre sur plateforme ACTES par WebServices.
     * - CircuitObligatoire: a eviter, utiliser de preference le routage parametre par TypeTechnique/SousType.
     * </ul>
     *
     * @param creerDossierRequest
     * @return
     */
    @Override
    public CreerDossierResponse creerDossier(final CreerDossierRequest creerDossierRequest) {

        if (creerDossierService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("creerDossierService non initialisé...");
            }
            this.getCreerDossierService();
        }

        // Sortie de la création de dossier dans un service spécifique
        return creerDossierService.doCreateFile(creerDossierRequest);
    }

    /**
     * @deprecated since 3.0. Use {@link InterfaceParapheurImpl#creerDossier(org.adullact.spring_ws.iparapheur._1.CreerDossierRequest) instead.
     */
    @Override
    @Deprecated
    public CreerDossierPESResponse creerDossierPES(CreerDossierPESRequest creerDossierPESRequest) {
        CreerDossierPESResponse res = new CreerDossierPESResponse();
        MessageRetour msg = new MessageRetour();
        NodeRef parapheur;
        NodeRef dossierRef;
        NodeRef circuitRef;
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();

        if (parapheurService == null) {
            logger.debug("parapheurService non initialisé...");
            this.getParapheurService();
        }
        if (contentService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("contentService non initialisé...");
            }
            this.getContentService();
        }
        //  check optionnel sur le contenu de la requete : EmailEmetteur, XPathPourSignaturePES, Annotations
        msg.setCodeRetour("KO");
        msg.setMessage("Requete incomplete.");
        msg.setSeverite("FATAL");
        String userName = this.getValidUser();
        logger.info("WebService [=====DEPRECATED=====] creerDossierPES invoqué par " + userName);
        if (userName == null) {
            msg.setMessage(this.pErrMsg);
        } else {
            this.getAuthenticationComponent().setCurrentUser(userName);
            if (creerDossierPESRequest == null) {
                msg.setMessage("Requete incomplete.");
            } else if (creerDossierPESRequest.getTypeTechnique() == null || creerDossierPESRequest.getTypeTechnique().trim().isEmpty()) {
                msg.setMessage("Requete incomplete, champ obligatoire 'TypeTechnique' manquant ou vide.");
            } else if (creerDossierPESRequest.getSousType() == null || creerDossierPESRequest.getSousType().trim().isEmpty()) {
                msg.setMessage("Requete incomplete, champ obligatoire 'SousType' manquant ou vide.");
            } else if (creerDossierPESRequest.getDossierID() == null || creerDossierPESRequest.getDossierID().trim().isEmpty()) {
                msg.setMessage("Requete incomplete, champ obligatoire 'DossierID' manquant ou vide.");
            } else if (creerDossierPESRequest.getFichierPES() == null || creerDossierPESRequest.getFichierPES().getValue().length == 0) {
                msg.setMessage("Requete incomplete, champ obligatoire 'FichierPES' manquant ou vide.");
            } else if (creerDossierPESRequest.getVisuelPDF() == null || creerDossierPESRequest.getVisuelPDF().getValue().length == 0) {
                msg.setMessage("Requete incomplete, champ obligatoire 'VisuelPDF' manquant ou vide.");
            } else if (!creerDossierPESRequest.getVisuelPDF().getContentType().equalsIgnoreCase(MimetypeMap.MIMETYPE_PDF)) {
                msg.setMessage("Requete incorrecte, VisuelPDF doit avoir le type MIME:" + MimetypeMap.MIMETYPE_PDF);
            } else if (creerDossierPESRequest.getDateLimite() != null
                    && !creerDossierPESRequest.getDateLimite().trim().isEmpty()
                    && !creerDossierPESRequest.getDateLimite().trim().matches("[0-3][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]")) {
                msg.setMessage("La DateLimite doit être au format AAAA-MM-JJ, au lieu de: " + creerDossierPESRequest.getDateLimite().trim());
            } else if (creerDossierPESRequest.getVisibilite() == null) {
                msg.setMessage("Requete incomplete, champ obligatoire 'Visibilite' manquant.");
            } else if (!isNomDossierValide(creerDossierPESRequest.getDossierID().trim())) {
                msg.setMessage("Le champ dossierID a une syntaxe incorrecte.");
            } else if (parapheurService.isNomDossierAlreadyExists(creerDossierPESRequest.getDossierID().trim())) {
                msg.setMessage("Le nom de dossier est déjà présent dans le Parapheur: dossierID = " + creerDossierPESRequest.getDossierID().trim());
            } else {
                UserTransaction tx;
                tx = this.getTransactionService().getUserTransaction();
                try {
                    tx.begin();
                    if (logger.isDebugEnabled()) {
                        logger.debug("ID dossier = " + creerDossierPESRequest.getDossierID().trim()
                                + ", Type= " + creerDossierPESRequest.getTypeTechnique().trim()
                                + ", Sous-Type= " + creerDossierPESRequest.getSousType().trim());
                    }

                    parapheur = parapheurService.getUniqueParapheurForUser(userName);
                    if (parapheur == null) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("parapheur == null !!");
                        }
                        msg.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                        throw new DocumentException(userName + " n'a pas de parapheur.");
                    }
                    // trouver le circuit
                    circuitRef = typesService.getWorkflow(creerDossierPESRequest.getTypeTechnique(), creerDossierPESRequest.getSousType());//parapheurService.getCircuitRef(creerDossierPESRequest.getTypeTechnique(), creerDossierPESRequest.getSousType());
                    if (circuitRef == null) {
                        throw new DocumentException("circuit INCONNU dans iParapheur");
                    }
                    // nom Dossier
                    properties.put(ContentModel.PROP_NAME, creerDossierPESRequest.getDossierID());
                    properties.put(ParapheurModel.PROP_DIGITAL_SIGNATURE_MANDATORY,
                            getTypesService().isDigitalSignatureMandatory(creerDossierPESRequest.getTypeTechnique(), creerDossierPESRequest.getSousType()));
                    properties.put(ParapheurModel.PROP_READING_MANDATORY,
                            getTypesService().isReadingMandatory(
                                    creerDossierPESRequest.getTypeTechnique(),
                                    creerDossierPESRequest.getSousType()));
                    // date Limite
                    if (creerDossierPESRequest.getDateLimite() != null && !creerDossierPESRequest.getDateLimite().trim().isEmpty()) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        properties.put(ParapheurModel.PROP_DATE_LIMITE, dateFormat.parse(creerDossierPESRequest.getDateLimite().trim()));
                    }
                    // Visibilite
                    if (creerDossierPESRequest.getVisibilite() != null && !creerDossierPESRequest.getVisibilite().toString().isEmpty()) {
                        if (creerDossierPESRequest.getVisibilite().toString().equalsIgnoreCase("CONFIDENTIEL")) {
                            properties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.TRUE);
                        } else if (creerDossierPESRequest.getVisibilite().toString().equalsIgnoreCase("PUBLIC")) {
                            properties.put(ParapheurModel.PROP_PUBLIC, Boolean.TRUE);
                        }
                    }
                    // Email Emetteur: pas grave si champ vide ou null, mais KO si email renseigné est inconnu
                    if (creerDossierPESRequest.getEmailEmetteur() != null && !creerDossierPESRequest.getEmailEmetteur().isEmpty()) {
                        String l_email = creerDossierPESRequest.getEmailEmetteur();
                        try {
                            NodeRef emetteurRef = parapheurService.findUserByEmail(l_email);
                            if (emetteurRef != null) {
                                String l_username = (String) this.getNodeService().getProperty(emetteurRef, ContentModel.PROP_USERNAME);
                                if (logger.isDebugEnabled()) {
                                    logger.debug("EmailEmetteur(" + l_email + ") trouvé = " + l_username);
                                }
                                properties.put(ParapheurModel.PROP_WS_EMETTEUR, l_username);//properties.put(ParapheurModel.PROP_EMAIL_EMETTEUR, emailEmetteur);
                            } else {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("Propriété emailEmetteur invalide, aucun Utilisateur correspondant.");
                                }
                                msg.setMessage("Requete incorrecte, 'EmailEmetteur'=" + l_email + " est inconnu de i-Parapheur");
                                throw new DocumentException("'EmailEmetteur'=" + l_email + " est inconnu de i-Parapheur");
                            }
                        } catch (NoSuchPersonException nspe) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("catch: Propriété emailEmetteur invalide, aucun Utilisateur correspondant.");
                            }
                            msg.setMessage("Requete incorrecte, 'EmailEmetteur'=" + l_email + " est inconnu de i-Parapheur");
                            throw new DocumentException("'EmailEmetteur'=" + l_email + " est inconnu de i-Parapheur");
                        }
                    }
                    // ------------------------------------- Création dossier
                    dossierRef = dossierService.createDossier(parapheur, properties);
                    dossierService.setDossierPropertiesWithAuditTrail(dossierRef, parapheur, properties, true);
                    // doc principal
                    FileFolderService ffs = this.getServiceRegistry().getFileFolderService();
                    String extension;
                    if (creerDossierPESRequest.getFichierPES().getContentType().toLowerCase().contains("xml")) {
                        extension = ".xml";
                    } else {
                        extension = "";
                    }
                    FileInfo fileInfo = ffs.create(dossierRef, creerDossierPESRequest.getDossierID() + extension, ContentModel.TYPE_CONTENT);
                    NodeRef docNodeRef = fileInfo.getNodeRef();
                    ContentWriter cw = contentService.getWriter(docNodeRef, ContentModel.PROP_CONTENT, Boolean.TRUE);
                    logger.debug("  getFichierPES.getContentType= " + creerDossierPESRequest.getFichierPES().getContentType());
                    cw.setMimetype(creerDossierPESRequest.getFichierPES().getContentType());
                    cw.setEncoding("ISO-8859-1");
                    InputStream cis = new ByteArrayInputStream(creerDossierPESRequest.getFichierPES().getValue());
                    cw.putContent(cis);
                    // Visuel PDF en Propriété du document principal
                    // contrôle sur le type MIME en amont, mais Pas de contrôle sur le format PDF
                    ContentWriter cw2 = contentService.getWriter(docNodeRef, ParapheurModel.PROP_VISUEL_PDF, Boolean.TRUE);
                    cw2.setMimetype(MimetypeMap.MIMETYPE_PDF);
                    cw2.setEncoding("UTF-8");
                    InputStream cis2 = new ByteArrayInputStream(creerDossierPESRequest.getVisuelPDF().getValue());
                    cw2.putContent(cis2);
                    /* Visuel PDF en document secondaire pour le moment
                    FileInfo fileInfo3 = ffs.create(dossierRef, "VisuelPDF_"+creerDossierPESRequest.getDossierID()+".pdf", ContentModel.TYPE_CONTENT);
                    NodeRef docNodeRef3 = fileInfo3.getNodeRef(); ContentWriter cw3 = cs.getWriter(docNodeRef3, ContentModel.PROP_CONTENT, Boolean.TRUE);
                    cw3.setMimetype(MimetypeMap.MIMETYPE_PDF); cw3.setEncoding("UTF-8");
                    InputStream cis3 = new ByteArrayInputStream(creerDossierPESRequest.getVisuelPDF().getValue());
                    cw3.putContent(cis3); */

                    // Typage Metier
                    Map<QName, Serializable> typageProps;
                    typageProps = parapheurService.getTypeMetierProperties(creerDossierPESRequest.getTypeTechnique());
                    typageProps.put(ParapheurModel.PROP_TYPE_METIER, creerDossierPESRequest.getTypeTechnique());
                    typageProps.put(ParapheurModel.PROP_SOUSTYPE_METIER, creerDossierPESRequest.getSousType());
                    typageProps.put(ParapheurModel.PROP_XPATH_SIGNATURE, creerDossierPESRequest.getXPathPourSignaturePES());
                    this.getNodeService().addAspect(dossierRef, ParapheurModel.ASPECT_TYPAGE_METIER, typageProps);
                    // Circuit
                    SavedWorkflow circuit = parapheurService.loadSavedWorkflow(circuitRef);
                    parapheurService.setCircuit(dossierRef, circuit.getCircuit());
                    // Annotations (publique + privee)
                    if (creerDossierPESRequest.getAnnotationPublique() != null) {
                        parapheurService.setAnnotationPublique(dossierRef, creerDossierPESRequest.getAnnotationPublique());
                    }
                    if (creerDossierPESRequest.getAnnotationPrivee() != null) {
                        parapheurService.setAnnotationPrivee(dossierRef, creerDossierPESRequest.getAnnotationPrivee());
                    }
                    // Emission automatique du dossier=> visa de l'émetteur, et approbation
                    // parapheurService.setSignataire(dossierRef, firstName + ((lastName!=null && lastName.length() > 0) ? " " + lastName : ""));
                    parapheurService.setSignataire(dossierRef, parapheurService.getNomProprietaire(parapheur));
                    parapheurService.approveV4(dossierRef, parapheurService.getCurrentParapheur());

                    tx.commit();
                    msg.setCodeRetour("OK");
                    msg.setMessage("Dossier " + creerDossierPESRequest.getDossierID() + " soumis dans le circuit");
                    msg.setSeverite("INFO");
                } catch (Exception e) {
                    doRollback(tx, "creerDossierPES");
                    msg.setCodeRetour("KO");
                    if (msg.getMessage().equalsIgnoreCase("Requete incomplete.") || msg.getMessage().trim().isEmpty()) {
                        msg.setMessage("Requete incorrecte: " + e.getMessage());
                    }
                    msg.setSeverite("ERROR");
                    logger.error(e.getMessage(), e);
                }
            }
        }
        logger.debug("WebService creerDossierPESRequest:\n\tCodeRetour= " + msg.getCodeRetour() + "\n\tMessage= " + msg.getMessage() + "\n\tSeverite= " + msg.getSeverite());
        res.setMessageRetour(msg);
        return res;
    }

    /**
     * The mother of them all...
     * Tout commence par un echo dans la nuit. Alors on en profite pour vérifier
     * que les services de base sont là.
     *
     * @param echoRequest
     * @return
     */
    @Override
    public String echo(String echoRequest) {
        if (null == this.getServiceRegistry() && logger.isEnabledFor(Level.ERROR)) {
            logger.error("  ####### null == this.getServiceRegistry() #######");
        }
        String userName = this.getUsernameFromAuthentication();    // authenticationComponent.getCurrentUserName();
        if (logger.isInfoEnabled()) {
            logger.info("WebService ECHO(\"" + echoRequest + "\"), username=" + userName + ", " + this.getAuthenticationComponent().getCurrentUserName());
        }
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        /*
         * BLEX - correction changement de comportement
         * En version 3.2, lorsque echo est appelle avec un utilisateur innexistant
         * (pour un tenant existant) le service repond l'enigmatique "strikes back"
         * Dans la version 3.0.5, une erreur HTTP 401 etait retournee dans ce cas.
         * On reproduit plus ou moins ce comportement (attendu des applications metier)
         * en levant une exception qui se traduira par une erreur HTTP 500.
         *
         *
        if (userName == null) {
            return "Echo strikes back with what you sent:\"" + echoRequest + "\"!";
        } else if (userName.equalsIgnoreCase("admin")) {
            return "BAD: you should not use that username";
        } else {
            return "[" + userName + "] m'a dit: \"" + echoRequest + "\"!";
        }
        */
        // BLEX
        if (userName == null) {
            throw new RuntimeException("utilisateur inconnu");
        } else {
            return "[" + userName + "] m'a dit: \"" + echoRequest + "\"!";
        }
    }

    @Override
    public EffacerDossierRejeteResponse effacerDossierRejete(String effacerDossierRejeteRequest) {
        EffacerDossierRejeteResponse res = new EffacerDossierRejeteResponse();
        MessageRetour msg = new MessageRetour();
        res.setMessageRetour(msg);
        res.getMessageRetour().setCodeRetour("KO");
        res.getMessageRetour().setSeverite("ERROR");
        res.getMessageRetour().setMessage("non implémenté");

        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getDossier(\"" + effacerDossierRejeteRequest + "\") invoqué par " + userName);
        }
        if (userName == null) {
            res.getMessageRetour().setMessage(this.pErrMsg);
            return res;
        }
        if (effacerDossierRejeteRequest == null || !isNomDossierValide(effacerDossierRejeteRequest)) {
            res.getMessageRetour().setMessage("Syntaxe dossierID incorrecte");
            return res;
        }
        // authenticationComponent.setSystemUserAsCurrentUser();
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        try {
            tx.begin();

            this.getAuthenticationComponent().setCurrentUser(userName);
            NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
            if (parapheur == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("parapheur == null !!");
                }
                msg.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                throw new DocumentException(userName + " n'a pas de parapheur.");
            }
            // Contrôle que le dossierID existe, que c'est un dossier, qu'il est dans la bannette "retournés" du bon utilisateur.
            NodeRef dossierRef = checkForNodeRefFromDossierID(parapheur, effacerDossierRejeteRequest);
            if (dossierRef != null && parapheurService.isDossier(dossierRef)) {
                NodeRef corbeilleRef = parapheurService.getParentCorbeille(dossierRef);
                // Est-ce que le dossier est dans la bonne corbeille?
                if (!parapheurService.getCorbeille(parapheur, ParapheurModel.NAME_RETOURNES).getId().equals(corbeilleRef.getId())) {
                    // logger.debug("Le dossierID '"+ getDossierRequest +"' n'est pas accessible.");
                    res.getMessageRetour().setMessage("Le dossierID '" + effacerDossierRejeteRequest + "' n'est pas accessible.");
                    throw new DocumentException("Le dossierID '" + effacerDossierRejeteRequest + "' n'est pas accessible.");
                }
                parapheurService.handleDeleteDossier(dossierRef, parapheur, true, userName);
//                this.getDossierService().deleteDossier(dossierRef, true);
                res.getMessageRetour().setCodeRetour("OK");
                res.getMessageRetour().setSeverite("INFO");
                res.getMessageRetour().setMessage("Dossier " + effacerDossierRejeteRequest + " supprimé du Parapheur.");
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Le dossierID '" + effacerDossierRejeteRequest + "' est inconnu dans le Parapheur.");
                }
                res.getMessageRetour().setMessage("Le dossierID '" + effacerDossierRejeteRequest + "' est inconnu dans le Parapheur.");
                throw new DocumentException("Le dossierID '" + effacerDossierRejeteRequest + "' est inconnu dans le Parapheur.");
            }
            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "effacerDossierRejete");
            logger.error(e.getMessage(), e);
        }
        return res;
    }

    @Override
    @Deprecated
    public EnvoyerDossierPESResponse envoyerDossierPES(String envoyerDossierPESRequest) {
        EnvoyerDossierPESResponse res = new EnvoyerDossierPESResponse();
        MessageRetour msg = new MessageRetour();
        LogDossier logdossier = new LogDossier();
        //  bouchon
        msg.setCodeRetour("KO");
        msg.setSeverite("INFO");
        msg.setMessage("WebService non implémenté");
        res.setMessageRetour(msg);
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService envoyerDossierPES(\"" + envoyerDossierPESRequest + "\") par " + userName);
        }
        if (userName == null) {
            res.getMessageRetour().setMessage(this.pErrMsg);
            return res;
        }
        if (!isNomDossierValide(envoyerDossierPESRequest)) {
            res.getMessageRetour().setMessage("Syntaxe dossierID incorrecte");
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);

            NodeRef dossierRef = parapheurService.rechercheDossier(envoyerDossierPESRequest);
            if (dossierRef != null && parapheurService.isDossier(dossierRef)) {
                this.getS2lowService().envoiS2lowHelios(dossierRef);

                // Si pas d'exception, c'est que ça c'est bien passé :-)
                // Accessible
                if (parapheurService.getEmetteur(dossierRef).equals(parapheurService.getParentParapheur(dossierRef))) {
                    logdossier.setAccessible("OK");
                } else {
                    logdossier.setAccessible("KO");
                }
                logdossier.setAnnotation("");
                logdossier.setNom(this.getNodeService().getProperty(dossierRef, ContentModel.PROP_NAME).toString());
                logdossier.setStatus(this.getS2lowService().statutS2lowToString(1)); //posté
                // timestamp
                Date maDate = DefaultTypeConverter.INSTANCE.convert(Date.class, this.getNodeService().getProperty(dossierRef, ContentModel.PROP_MODIFIED));
                GregorianCalendar gCalendar = new GregorianCalendar();
                gCalendar.setTime(maDate);
                XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
                logdossier.setTimestamp(date);
                res.setLogDossier(logdossier);
                res.getMessageRetour().setCodeRetour("OK");
                res.getMessageRetour().setSeverite("INFO");
                res.getMessageRetour().setMessage("");
            } else {
                logger.debug("Le dossierID '" + envoyerDossierPESRequest + "' est inconnu dans le Parapheur.");
                throw new DocumentException("Le dossierID '" + envoyerDossierPESRequest + "' est inconnu dans le Parapheur.");
            }
            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "envoyerDossierPES");
            msg.setCodeRetour("KO");
            msg.setSeverite("ERROR");
            msg.setMessage(e.getMessage());
            res.setMessageRetour(msg);
            logger.error(e.getMessage(), e);
        }
        return res;
    }

    @Override
    public EnvoyerDossierTdTResponse envoyerDossierTdT(String envoyerDossierTdTRequest) {
        EnvoyerDossierTdTResponse res = new EnvoyerDossierTdTResponse();
        MessageRetour msg = new MessageRetour();
        LogDossier logdossier = new LogDossier();
        //  bouchon
        msg.setCodeRetour("KO");
        msg.setSeverite("INFO");
        msg.setMessage("WebService non implémenté");
        res.setMessageRetour(msg);
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService envoyerDossierTdT(\"" + envoyerDossierTdTRequest + "\") par " + userName);
        }
        if (userName == null) {
            res.getMessageRetour().setMessage(this.pErrMsg);
            return res;
        }
        if (!isNomDossierValide(envoyerDossierTdTRequest)) {
            res.getMessageRetour().setMessage("Syntaxe dossierID incorrecte");
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        if (nodeService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("nodeService non initialisé...");
            }
            this.getNodeService();
        }
        if (s2lowService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("s2lowService non initialisé...");
            }
            this.getS2lowService();
        }
        try {
            this.getAuthenticationComponent().setCurrentUser(userName);
            NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
            NodeRef dossierRef = null;
            if (parapheur != null) {
                dossierRef = checkForNodeRefFromDossierID(parapheur, envoyerDossierTdTRequest);
                if (dossierRef != null && parapheurService.isDossier(dossierRef)) {
                    int retry = 0, maxRetry = 3;
                    while (this.nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_PENDING) &&
                            retry++ < maxRetry) {
                        // Le dossier est locked... on attend un petit peu pour la fin du traitement ?
                        // Cas spécial dans le cas où creation + TDT immédiat
                        Thread.sleep(1000);
                    }
                }
            }
            tx.begin();
            if (parapheur == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("parapheur == null !!");
                }
                msg.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                throw new DocumentException(userName + " n'a pas de parapheur.");
            }
            if (dossierRef != null && parapheurService.isDossier(dossierRef)) {
                // BLEX
                boolean isSlow = false;
                boolean isSrci = false;
                {
                    java.io.Serializable tdtNom = this.nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_NOM);
                    if (tdtNom != null &&
                            (tdtNom.toString().equals(S2lowService.PROP_TDT_NOM_S2LOW) ||
                                    tdtNom.toString().equals(FastServiceImpl.PROP_TDT_NOM_FAST))) {
                        isSlow = true;
                    } else if (tdtNom != null && tdtNom.toString().equals(SrciService.K.tdtName)) {
                        isSrci = true;
                    } else {
                        throw new Exception("Nom du TDT non reconnu : [" + tdtNom + "]");
                    }
                }
                String protocole = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_PROTOCOLE);
                if (protocole == null) {
                    throw new Exception("Envoi impossible: pas de PROTOCOLE TDT déclaré pour le dossier "
                            + envoyerDossierTdTRequest + ".");
                }

                // Positionne le signataire sur l'étape courante.
                parapheurService.setSignataire(dossierRef, parapheurService.getPrenomNomFromLogin(userName));

                if (isSlow) { // BLEX condition
                    if (protocole.equalsIgnoreCase("ACTES")) {
                        s2lowService.envoiS2lowActes(dossierRef,
                                (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_NATURE),
                                (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_CLASSIFICATION),
                                envoyerDossierTdTRequest,
                                (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_OBJET),
                                (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_DATE));
                    } else if (protocole.equalsIgnoreCase("HELIOS")) {
                        s2lowService.envoiS2lowHelios(dossierRef);
                    } else {
                        throw new Exception("Le protocole " + protocole + " n'est pas supporté par ce WebService.");
                    }
                }

                if (isSrci) { // BLEX
                    if (SrciService.K.protocol.helios.equals(protocole)) {
                        getSrciService().sendHelios(dossierRef);
                    } else {
                        throw new Exception("Le protocole [" + protocole + "] n'est pas supporte par le TDT [" + SrciService.K.tdtName + "]");
                    }
                }

                // Si pas d'exception, c'est que ça c'est bien passé :-)
                // Accessible
                if (parapheurService.getEmetteur(dossierRef).equals(parapheurService.getParentParapheur(dossierRef))) {
                    logdossier.setAccessible("OK");
                } else {
                    logdossier.setAccessible("KO");
                }
                logdossier.setAnnotation("");
                logdossier.setNom(this.getNodeService().getProperty(dossierRef, ContentModel.PROP_NAME).toString());

                // BLEX
                String logStatus;
                if (isSrci) {
                    logStatus = SrciService.tdtStatusCodeToText(TPS.Status.handledByTdt);//posté
                } else {
                    logStatus = this.getS2lowService().statutS2lowToString(TransactionStatus.STATUS_POSTE);//posté
                }
                logdossier.setStatus(logStatus);
                // timestamp
                Date maDate = DefaultTypeConverter.INSTANCE.convert(Date.class, this.getNodeService().getProperty(dossierRef, ContentModel.PROP_MODIFIED));
                GregorianCalendar gCalendar = new GregorianCalendar();
                gCalendar.setTime(maDate);
                XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
                logdossier.setTimestamp(date);
                res.setLogDossier(logdossier);
                res.getMessageRetour().setCodeRetour("OK");
                res.getMessageRetour().setSeverite("INFO");
                res.getMessageRetour().setMessage("");
                tx.commit();
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Le dossierID '" + envoyerDossierTdTRequest + "' est inconnu dans le Parapheur.");
                }
                throw new DocumentException("Le dossierID '" + envoyerDossierTdTRequest + "' est inconnu dans le Parapheur.");
            }
        } catch (Exception e) {
            doRollback(tx, "envoyerDossierTdT");
            msg.setCodeRetour("KO");
            msg.setSeverite("ERROR");
            msg.setMessage(e.getMessage());
            res.setMessageRetour(msg);
            logger.error(e.getMessage(), e);
        }
        return res;
    }

    @Override
    public ForcerEtapeResponse forcerEtape(ForcerEtapeRequest forcerEtapeRequest) {
        ForcerEtapeResponse res = new ForcerEtapeResponse();
        MessageRetour msg = new MessageRetour();

        //  bouchon
        msg.setCodeRetour("KO");
        msg.setSeverite("ERREUR");
        msg.setMessage("");
        res.setMessageRetour(msg);
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService forcerEtape par " + userName);
        }
        if (userName == null) {
            res.getMessageRetour().setMessage(this.pErrMsg);
            return res;
        }
        if (!isNomDossierValide(forcerEtapeRequest.getDossierID())) {
            res.getMessageRetour().setMessage("Syntaxe dossierID incorrecte");
            return res;
        }

        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        try {
            tx.begin();

            this.getAuthenticationComponent().setCurrentUser(userName);
            NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
            if (parapheur == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("parapheur == null !!");
                }
                msg.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                throw new DocumentException(userName + " n'a pas de parapheur.");
            }
            // Contrôle que le dossierID existe, que c'est un dossier, etc.
            String dossierID = forcerEtapeRequest.getDossierID();
            NodeRef dossierRef = checkForNodeRefFromDossierID(parapheur, dossierID);
            if (dossierRef != null && parapheurService.isDossier(dossierRef)) {
                if (parapheurService.isTermine(dossierRef)) {
                    res.getMessageRetour().setMessage("Le dossierID '" + dossierID + "' est terminé.");
                    throw new DocumentException("Le dossierID '" + dossierID + "' est terminé.");
                }
                if (!parapheurService.isEmis(dossierRef)) {
                    res.getMessageRetour().setMessage("Le dossierID '" + dossierID + "' n'a pas été émis.");
                    throw new DocumentException("Le dossierID '" + dossierID + "' n'a pas été émis.");
                }
                com.atolcd.parapheur.repo.EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossierRef);
                if (etape == null) {
                    res.getMessageRetour().setMessage("Le dossierID '" + dossierID + "' n'a pas d'étape courante.");
                    throw new DocumentException("Le dossierID '" + dossierID + "' n'a pas d'étape courante.");
                }
                if (!com.atolcd.parapheur.repo.EtapeCircuit.ETAPE_TDT.equalsIgnoreCase(etape.getActionDemandee())
                        && !com.atolcd.parapheur.repo.EtapeCircuit.ETAPE_VISA.equalsIgnoreCase(etape.getActionDemandee())) {
                    res.getMessageRetour().setMessage("Action impossible: dossierID '" + dossierID + "' est à une étape de "
                            + etape.getActionDemandee());
                    throw new DocumentException("Action impossible: dossierID '" + dossierID + "' est à une étape de "
                            + etape.getActionDemandee());
                }
                if (forcerEtapeRequest.getCodeTransition() == null
                        || forcerEtapeRequest.getCodeTransition().trim().isEmpty()) {
                    res.getMessageRetour().setMessage("Le CodeTransition est vide.");
                    throw new DocumentException("Le CodeTransition est vide.");
                }
                String annotPublique = forcerEtapeRequest.getAnnotationPublique();
                if (annotPublique != null && !annotPublique.trim().isEmpty()) {
                    parapheurService.setAnnotationPublique(dossierRef, annotPublique);
                }
                String annotPrivee = forcerEtapeRequest.getAnnotationPrivee();
                if (annotPrivee != null && !annotPrivee.trim().isEmpty()) {
                    parapheurService.setAnnotationPrivee(dossierRef, annotPrivee);
                }

                if (forcerEtapeRequest.getCodeTransition().equalsIgnoreCase("OK")) {
                    parapheurService.approveV4(dossierRef, parapheurService.getCurrentParapheur());
                } else if (forcerEtapeRequest.getCodeTransition().equalsIgnoreCase("KO")) {
                    parapheurService.reject(dossierRef);
                } else {
                    res.getMessageRetour().setMessage("CodeTransition est inconnu, OK ou KO attendu.");
                    throw new DocumentException("Le CodeTransition est inconnu, OK ou KO attendu.");
                }
                res.getMessageRetour().setCodeRetour("OK");
                res.getMessageRetour().setSeverite("INFO");
                res.getMessageRetour().setMessage("");
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Le dossierID '" + dossierID + "' est inconnu dans le Parapheur.");
                }
                res.getMessageRetour().setMessage("Le dossierID '" + dossierID + "' est inconnu dans le Parapheur.");
                throw new DocumentException("Le dossierID '" + dossierID + "' est inconnu dans le Parapheur.");
            }
            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "forcerEtape");
            logger.error(e.getMessage());
            res.getMessageRetour().setMessage(e.getMessage());
        }
        return res;
    }

    @Override
    public GetClassificationActesTdtResponse getClassificationActesTdt(GetClassificationActesTdtRequest getClassificationActesTdtRequest) {
        GetClassificationActesTdtResponse res = new GetClassificationActesTdtResponse();
        MessageRetour msg = new MessageRetour();
        res.setMessageRetour(msg);
        msg.setCodeRetour("KO");
        msg.setSeverite("INFO");
        msg.setMessage("WebService non implémenté");
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getClassificationActesTdt par " + userName);
        }
        if (userName == null) {
            res.getMessageRetour().setMessage(this.pErrMsg);
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        if (s2lowService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("s2lowService non initialisé...");
            }
            this.getS2lowService();
        }
        if (contentService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("contentService non initialisé...");
            }
            this.getContentService();
        }
        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);

            String type = getClassificationActesTdtRequest.getTypeTechnique();
            String dmdMaj = getClassificationActesTdtRequest.getDemanderMiseAJourTdt();
            String leTdT = null;
            if (type != null && !type.trim().isEmpty()) {
                InputStream xmlStream = parapheurService.getSavedXMLTypes();
                SAXReader saxreader = new SAXReader();
                Document docXml = saxreader.read(xmlStream);
                Element rootElement = docXml.getRootElement();
                if (rootElement.getName().equalsIgnoreCase("MetierTypes")) {
                    Iterator<Element> itMetierType = rootElement.elementIterator("MetierType");
                    Element xmlTdT = null;
                    for (Iterator<Element> ie = itMetierType; ie.hasNext(); ) {
                        Element mtype = ie.next();
                        if (type.trim().equalsIgnoreCase(mtype.element("ID").getTextTrim())) {
                            xmlTdT = mtype.element("TdT");
                            break;
                        }
                    }
                    if (xmlTdT != null) {
                        String protocole = xmlTdT.element("Protocole").getTextTrim();
                        if (protocole.equalsIgnoreCase("ACTES")) {
                            leTdT = xmlTdT.element("Nom").getTextTrim();
                        } else {
                            throw new Exception("Le Type Technique " + type + " ne correspond pas au protocole ACTES attendu.");
                        }
                    } else {
                        throw new Exception("Type technique " + type + " incorrect.");
                    }
                } else {
                    throw new Exception("Problème de récupération du paramétrage des Types Techniques");
                }
            }
            if (leTdT == null) {
                leTdT = "S²LOW";    // valeur par défaut
            }
            if (dmdMaj != null && dmdMaj.trim().equalsIgnoreCase("OUI")) {
                s2lowService.updateS2lowActesClassifications();
                msg.setCodeRetour("OK");
                msg.setMessage("Demande de mise à jour en cours.");
            } else {
                // renvoyer la classification existante.
                if (logger.isDebugEnabled()) {
                    logger.debug(" demande classif TDT: '" + leTdT + "'");
                }
                if (leTdT.equalsIgnoreCase("S²LOW")) {
                    // envoyer le XML
                    NodeRef configRef = s2lowService.getS2lowActesClassificationNodeRef();
                    if (configRef == null) {
                        if (logger.isEnabledFor(Level.ERROR)) {
                            logger.error("getS2lowActesClassificationNodeRef is null");
                        }
                        msg.setSeverite("FATAL");
                        msg.setMessage("Classification inexistante: faire une mise à jour.");
                    } else {
                        ContentReader contentreader = contentService.getReader(configRef, ContentModel.PROP_CONTENT);
                        TypeDoc xmlDoc = new TypeDoc();
                        xmlDoc.setContentType(contentreader.getMimetype());
                        File fic = TempFileProvider.createTempFile("xmlDoc", "xml");
                        contentreader.getContent(fic);
                        xmlDoc.setValue(getBytesFromFile(fic));
                        res.setClassification(xmlDoc);
                        msg.setCodeRetour("OK");
                        msg.setMessage("Classification envoyée.");
                    }
                } else {
                    msg.setMessage("Le TDT '" + leTdT + "' est non supporté actuellement.");
                }
            }

            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getClassificationActesTdt");
            logger.error(e.getMessage());
            res.getMessageRetour().setMessage(e.getMessage());
        }
        return res;
    }

    @Override
    public ExercerDroitRemordDossierResponse exercerDroitRemordDossier(String exercerDroitRemordDossierRequest) {
        ExercerDroitRemordDossierResponse res = new ExercerDroitRemordDossierResponse();
        MessageRetour msg = new MessageRetour();
        res.setMessageRetour(msg);
        msg.setCodeRetour("KO");
        msg.setSeverite("INFO");
        msg.setMessage("WebService non implémenté");
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService exercerDroitRemordDossier(\"" + exercerDroitRemordDossierRequest + "\") par " + userName);
        }
        if (userName == null) {
            res.getMessageRetour().setMessage(this.pErrMsg);
            return res;
        }
        if (!isNomDossierValide(exercerDroitRemordDossierRequest)) {
            res.getMessageRetour().setMessage("Syntaxe dossierID incorrecte");
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);
            NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
            if (parapheur == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("parapheur == null !!");
                }
                msg.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                throw new DocumentException(userName + " n'a pas de parapheur.");
            }

            // Contrôle que le dossierID existe, que c'est un dossier, etc.
            NodeRef dossierRef = checkForNodeRefFromDossierID(parapheur, exercerDroitRemordDossierRequest);
            if (dossierRef != null && parapheurService.isDossier(dossierRef)) {
                if (parapheurService.isTermine(dossierRef)) {
                    res.getMessageRetour().setMessage("Le dossierID '" + exercerDroitRemordDossierRequest + "' est terminé.");
                    throw new DocumentException("Le dossierID '" + exercerDroitRemordDossierRequest + "' est terminé.");
                }
                if (!parapheurService.isEmis(dossierRef)) {
                    res.getMessageRetour().setMessage("Le dossierID '" + exercerDroitRemordDossierRequest + "' n'a pas été émis.");
                    throw new DocumentException("Le dossierID '" + exercerDroitRemordDossierRequest + "' n'a pas été émis.");
                }
                if (!parapheurService.isRecuperable(dossierRef)
                        || this.getNodeService().hasAspect(dossierRef, ParapheurModel.ASPECT_SECRETARIAT)) {
                    res.getMessageRetour().setMessage("Le dossierID '" + exercerDroitRemordDossierRequest + "' n'est pas récupérable.");
                    throw new DocumentException("Le dossierID '" + exercerDroitRemordDossierRequest + "' n'est pas récupérable.");
                }
                //NodeRef corbeilleRef = parapheurService.getParentCorbeille(dossierRef);
                // Est-ce que le dossier est vraiment récupérable? est-il dans la bonne corbeille?
                List<com.atolcd.parapheur.repo.EtapeCircuit> circuit = parapheurService.getCircuit(dossierRef);
                int index = 0;
                for (com.atolcd.parapheur.repo.EtapeCircuit etape : circuit) {
                    if (!etape.isApproved()) {
                        index = circuit.indexOf(etape);
                        break;
                    }
                }
                if (index > 0) {
                    com.atolcd.parapheur.repo.EtapeCircuit etapePrecedente = circuit.get(index - 1);
                    NodeRef parapheurPrecedent = etapePrecedente.getParapheur();
                    if (parapheurPrecedent != null && parapheurPrecedent.equals(parapheur)) {
                        parapheurService.recupererDossier(dossierRef);
                        res.getMessageRetour().setCodeRetour("OK");
                        res.getMessageRetour().setSeverite("INFO");
                        res.getMessageRetour().setMessage("Dossier " + exercerDroitRemordDossierRequest + " récupéré.");
                    } else {
                        res.getMessageRetour().setMessage("Le dossierID '" + exercerDroitRemordDossierRequest + "' n'est pas récupérable.");
                        throw new DocumentException("Le dossierID '" + exercerDroitRemordDossierRequest + "' n'est pas récupérable.");
                    }
                } else {
                    res.getMessageRetour().setMessage("Le dossierID '" + exercerDroitRemordDossierRequest + "' n'est pas récupérable.");
                    throw new DocumentException("Le dossierID '" + exercerDroitRemordDossierRequest + "' n'est pas récupérable.");
                }
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Le dossierID '" + exercerDroitRemordDossierRequest + "' est inconnu dans le Parapheur.");
                }
                res.getMessageRetour().setMessage("Le dossierID '" + exercerDroitRemordDossierRequest + "' est inconnu dans le Parapheur.");
                throw new DocumentException("Le dossierID '" + exercerDroitRemordDossierRequest + "' est inconnu dans le Parapheur.");
            }
            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "exercerDroitRemordDossier");
            logger.error(e.getMessage());
        }
        return res;
    }

    /**
     * Interrogation du contenu du circuit par défaut selon Type + Sous-Type fournis
     *
     * @param getCircuitRequest
     * @return un GetCircuitResponse
     */
    @Override
    public GetCircuitResponse getCircuit(GetCircuitRequest getCircuitRequest) {
        GetCircuitResponse res = new GetCircuitResponse();
        if (getCircuitRequest == null) {
            return res;
        }
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getCircuit(\"" + getCircuitRequest.getTypeTechnique() + "\", \"" + getCircuitRequest.getSousType() + "\") invoqué par " + userName);
        }
        if (userName == null) {
            return res;
        } else if (getCircuitRequest.getTypeTechnique() == null || getCircuitRequest.getTypeTechnique().trim().isEmpty()
                || getCircuitRequest.getSousType() == null || getCircuitRequest.getSousType().trim().isEmpty()) {
            return res;
        }
        // authenticationComponent.setSystemUserAsCurrentUser();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        if (workflowService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("workflowService non initialisé...");
            }
            this.getWorkflowService();
        }

        /* ----------------------------------
         *  Berger Levrault - Mathieu Passenaud
         *           TMC capability
         *              BEGIN
         * ----------------------------------
         */
        if ("blex".equals(parapheurService.getHabillage())) {
            // Dans le cas du TMC, si nous demandons un type PES nous devons
            // chercher l'existance d'un type <idCollec>.PES en priorité
            String idCollectivite = "";
            if (userName.contains(".")) {
                idCollectivite = userName.substring(0, userName.indexOf(".")) + ".";
            }
            List<String> sousTypes = null;
            try {

                sousTypes = parapheurService.getSavedSousTypes(getCircuitRequest.getTypeTechnique(), parapheurService.getUniqueParapheurForUser(userName));
            } catch (Exception ex) {
                logger.debug("type does not exists, continue with given typename");
            }
            if (sousTypes != null) {
                getCircuitRequest.setTypeTechnique(idCollectivite + getCircuitRequest.getTypeTechnique());
            }
        }
        /* ----------------------------------
         *               END
         * ----------------------------------
         */


        if (typesService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("typesService non initialisé...");
            }
            this.getTypesService();
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);

            // NodeRef circuitRef = parapheurService.getCircuitRef(getCircuitRequest.getTypeTechnique(), getCircuitRequest.getSousType());
            try {
                NodeRef circuitRef = typesService.getWorkflow(getCircuitRequest.getTypeTechnique(), getCircuitRequest.getSousType());
                if (circuitRef == null) {
                    throw new RuntimeException("circuitRef == null");
                }
                SavedWorkflow circuit = workflowService.getSavedWorkflow(circuitRef); //parapheurService.loadSavedWorkflow(circuitRef);

                List<EtapeCircuit> etapesCircuit = circuit.getCircuit();
                if (etapesCircuit == null) {
                    throw new RuntimeException("etapesCircuit == null");
                }

                //FIXME: trouver une solution
                // on demarre toujours de l'emetteur.
                NodeRef parapheurPrecedent = parapheurService.getUniqueParapheurForUser(userName); //null;
                for (EtapeCircuit etape : etapesCircuit) {
                    org.adullact.spring_ws.iparapheur._1.EtapeCircuit etapeWs = new org.adullact.spring_ws.iparapheur._1.EtapeCircuit();

                    NodeRef parapheur;
                    if (EtapeCircuit.TRANSITION_PARAPHEUR.equals(etape.getTransition())) {
                        parapheur = etape.getParapheur();
                    } else if (EtapeCircuit.TRANSITION_CHEF_DE.equals(etape.getTransition())) {
                        NodeRef responsable = parapheurService.getParapheurResponsable(parapheurPrecedent);
                        parapheur = (responsable != null ? responsable : parapheurPrecedent);
                    } else if (EtapeCircuit.TRANSITION_EMETTEUR.equals(etape.getTransition())) {
                        parapheur = parapheurService.getUniqueParapheurForUser(userName);
                    } else if (EtapeCircuit.TRANSITION_VARIABLE.equals(etape.getTransition())) {
                        // Non géré via WS, donc == EMETTEUR
                        parapheur = parapheurService.getUniqueParapheurForUser(userName);
                    } else {
                        throw new IllegalStateException("Unknown workflow transition: " + etape.getTransition());
                    }
                    etapeWs.setParapheur(parapheurService.getNomParapheur(parapheur));
                    etapeWs.setNom(StringUtils.generateStringFromList(parapheurService.getNomProprietaires(parapheur)));
                    etapeWs.setPrenom(""); // Unused field

                    if (etape.getActionDemandee() != null) {
                        if (EtapeCircuit.ETAPE_VISA.equals(etape.getActionDemandee())) {
                            etapeWs.setRole("VISEUR");
                        } else if (EtapeCircuit.ETAPE_SIGNATURE.equals(etape.getActionDemandee())) {
                            etapeWs.setRole("SIGNATAIRE");
                        } else if (EtapeCircuit.ETAPE_ARCHIVAGE.equals(etape.getActionDemandee())) {
                            etapeWs.setRole("ARCHIVAGE");
                        } else if (EtapeCircuit.ETAPE_TDT.equals(etape.getActionDemandee())) {
                            etapeWs.setRole("TDT");
                        } else if (EtapeCircuit.ETAPE_MAILSEC.equals(etape.getActionDemandee())) {
                            etapeWs.setRole("ENVOI PAR MAIL SECURISE");
                        } else {
                            etapeWs.setRole("INCONNU");
                        }
                    } else {
                        EtapeCircuit derniereEtape = etapesCircuit.get(etapesCircuit.size() - 1);
                        if (!etape.equals(derniereEtape)) {
                            etapeWs.setRole("VISEUR");
                        } else {
                            etapeWs.setRole("SIGNATAIRE");
                        }
                    }

                    res.getEtapeCircuit().add(etapeWs);

                    parapheurPrecedent = parapheur;
                }

                tx.commit();
            } catch (SubTypeNotFoundRuntimeException subTypeNotFoundException) {
                // Exception levée dasn le ca où on ne trouve pas de circuit associé au Type / Sous-Type
                doRollback(tx, "getCircuit");
                logger.error(subTypeNotFoundException.getLocalizedMessage());
            }
        } catch (Exception e) {
            doRollback(tx, "getCircuit");
            logger.error(e.getMessage(), e);
        }
        return res;
    }

    /**
     * Interrogation du contenu du circuit par défaut selon Type + Sous-Type
     * pour l'utilisateur fourni en paramètre
     *
     * @param getCircuitPourUtilisateurRequest , combinaison de type/sstype/user
     * @return un GetCircuitPourUtilisateurResponse
     */
    @Override
    public GetCircuitPourUtilisateurResponse getCircuitPourUtilisateur(GetCircuitPourUtilisateurRequest getCircuitPourUtilisateurRequest) {
        GetCircuitPourUtilisateurResponse res = new GetCircuitPourUtilisateurResponse();
        if (getCircuitPourUtilisateurRequest == null) {
            return res;
        }
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getCircuitPourUtilisateur(\"" + getCircuitPourUtilisateurRequest.getTypeTechnique() + "\", \""
                    + getCircuitPourUtilisateurRequest.getSousType() + "\", \""
                    + getCircuitPourUtilisateurRequest.getIdUtilisateur() + "\") invoqué par " + userName);
        }
        if (userName == null) {
            return res;
        } else if (getCircuitPourUtilisateurRequest.getTypeTechnique() == null || getCircuitPourUtilisateurRequest.getTypeTechnique().trim().isEmpty()
                || getCircuitPourUtilisateurRequest.getSousType() == null || getCircuitPourUtilisateurRequest.getSousType().trim().isEmpty()
                || getCircuitPourUtilisateurRequest.getIdUtilisateur() == null || getCircuitPourUtilisateurRequest.getIdUtilisateur().trim().isEmpty()) {
            return res;
        }
        /**
         * Lazy init
         */
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        if (workflowService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("workflowService non initialisé...");
            }
            this.getWorkflowService();
        }
        if (typesService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("typesService non initialisé...");
            }
            this.getTypesService();
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();

        try {
            tx.begin();
            if (!isUsernameExist(getCircuitPourUtilisateurRequest.getIdUtilisateur())) {
                return res;
            }
            userName = getCircuitPourUtilisateurRequest.getIdUtilisateur().trim();
            this.getAuthenticationComponent().setCurrentUser(userName);

            NodeRef circuitRef = typesService.getWorkflow(getCircuitPourUtilisateurRequest.getTypeTechnique(), getCircuitPourUtilisateurRequest.getSousType());
            if (circuitRef == null) {
                throw new RuntimeException("circuitRef == null");
            }
            SavedWorkflow circuit = workflowService.getSavedWorkflow(circuitRef); //parapheurService.loadSavedWorkflow(circuitRef);

            List<EtapeCircuit> etapesCircuit = circuit.getCircuit();
            if (etapesCircuit == null) {
                throw new RuntimeException("etapesCircuit == null");
            }

            //FIXME: trouver une solution
            // on demarre toujours de l'emetteur.
            NodeRef parapheurPrecedent = parapheurService.getUniqueParapheurForUser(userName); //null;
            for (EtapeCircuit etape : etapesCircuit) {
                org.adullact.spring_ws.iparapheur._1.EtapeCircuit etapeWs = new org.adullact.spring_ws.iparapheur._1.EtapeCircuit();

                NodeRef parapheur;
                if (EtapeCircuit.TRANSITION_PARAPHEUR.equals(etape.getTransition())) {
                    parapheur = etape.getParapheur();
                } else if (EtapeCircuit.TRANSITION_CHEF_DE.equals(etape.getTransition())) {
                    NodeRef responsable = parapheurService.getParapheurResponsable(parapheurPrecedent);
                    parapheur = (responsable != null ? responsable : parapheurPrecedent);
                } else if (EtapeCircuit.TRANSITION_EMETTEUR.equals(etape.getTransition())) {
                    parapheur = parapheurService.getUniqueParapheurForUser(userName);
                } else if (EtapeCircuit.TRANSITION_VARIABLE.equals(etape.getTransition())) {
                    // Non géré via WS, donc == EMETTEUR
                    parapheur = parapheurService.getUniqueParapheurForUser(userName);
                } else {
                    throw new IllegalStateException("Unknown workflow transition: " + etape.getTransition());
                }
                etapeWs.setParapheur(parapheurService.getParapheurName(parapheur));
                etapeWs.setNom(StringUtils.generateStringFromList(parapheurService.getNomProprietaires(parapheur)));
                etapeWs.setPrenom(""); // Unused field

                if (etape.getActionDemandee() != null) {
                    if (EtapeCircuit.ETAPE_VISA.equals(etape.getActionDemandee())) {
                        etapeWs.setRole("VISEUR");
                    } else if (EtapeCircuit.ETAPE_SIGNATURE.equals(etape.getActionDemandee())) {
                        etapeWs.setRole("SIGNATAIRE");
                    } else if (EtapeCircuit.ETAPE_ARCHIVAGE.equals(etape.getActionDemandee())) {
                        etapeWs.setRole("ARCHIVAGE");
                    } else if (EtapeCircuit.ETAPE_TDT.equals(etape.getActionDemandee())) {
                        etapeWs.setRole("TDT");
                    } else if (EtapeCircuit.ETAPE_MAILSEC.equals(etape.getActionDemandee())) {
                        etapeWs.setRole("ENVOI PAR MAIL SECURISE");
                    } else {
                        etapeWs.setRole("INCONNU");
                    }
                } else {
                    EtapeCircuit derniereEtape = etapesCircuit.get(etapesCircuit.size() - 1);
                    if (!etape.equals(derniereEtape)) {
                        etapeWs.setRole("VISEUR");
                    } else {
                        etapeWs.setRole("SIGNATAIRE");
                    }
                }

                res.getEtapeCircuit().add(etapeWs);

                parapheurPrecedent = parapheur;
            }

            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getCircuitPourUtilisateur");
            logger.error(e.getMessage(), e);
        }
        return res;
    }

    /* (non-Javadoc)
     * @see org.adullact.spring_ws.iparapheur._1.InterfaceParapheur#getDossier(java.lang.String)
     */
    @Override
    public GetDossierResponse getDossier(String getDossierRequest) {
        // Ce service permet de récupérer le contenu d'un dossier en fin de parcours (ie: bannette "à archiver", pas plus.)
        GetDossierResponse res = new GetDossierResponse();
        MessageRetour msg = new MessageRetour();
        res.setMessageRetour(msg);
        res.getMessageRetour().setCodeRetour("KO");
        res.getMessageRetour().setSeverite("ERROR");
        res.getMessageRetour().setMessage("");
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        if (nodeService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("nodeService non initialisé...");
            }
            this.getNodeService();
        }
        if (contentService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("contentService non initialisé...");
            }
            this.getContentService();
        }
        if (metadataService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("metadataService non initialisé...");
            }
            this.getMetadataService();
        }
        if (typesService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("typesService non initialisé...");
            }
            this.getTypesService();
        }
        if (dossierService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("dossierService non initialisé...");
            }
            this.getDossierService();
        }

        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getDossier(\"" + getDossierRequest + "\") invoqué par " + userName);
        }
        if (userName == null) {
            res.getMessageRetour().setMessage(this.pErrMsg);
            return res;
        }
        if (!isNomDossierValide(getDossierRequest)) {
            res.getMessageRetour().setMessage("Syntaxe dossierID incorrecte");
            return res;
        }
        // authenticationComponent.setSystemUserAsCurrentUser();
        UserTransaction tx;
        tx = this.getTransactionService().getNonPropagatingUserTransaction();
        try {
            tx.begin();

            this.getAuthenticationComponent().setCurrentUser(userName);
            NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
            if (parapheur == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("parapheur == null !!");
                }
                msg.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                throw new DocumentException(userName + " n'a pas de parapheur.");
            }

            // Contrôle que le dossierID existe, que c'est un dossier, qu'il
            // se trouve sur le bureau de l'utilisateur
            NodeRef dossierRef = checkForNodeRefFromDossierID(parapheur, getDossierRequest);
            if (dossierRef != null && parapheurService.isDossier(dossierRef)) {
                // Est-ce que le dossier est (anciennement: dans la bonne banette) sur le bureau de l'utilisateur?
                if (!parapheurService.getParentParapheur(dossierRef).equals(parapheur)) {
                    res.getMessageRetour().setMessage("Le dossierID '" + getDossierRequest + "' n'est pas accessible.");
                    if (logger.isDebugEnabled()) {
                        logger.debug("\n\tparapheur=" + parapheur + "\n\tparent   =" + parapheurService.getParentParapheur(dossierRef));
                    }
                    throw new DocumentException("Le dossierID '" + getDossierRequest + "' n'est pas accessible.");
                }
                // contrôles Ok, renvoyer le dossier.
                if (this.getNodeService().hasAspect(dossierRef, ParapheurModel.ASPECT_TYPAGE_METIER)) {
                    res.setTypeTechnique(this.getNodeService().getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER).toString());
                    res.setSousType(this.getNodeService().getProperty(dossierRef, ParapheurModel.PROP_SOUSTYPE_METIER).toString());
                }
                if (this.getNodeService().getProperty(dossierRef, ParapheurModel.PROP_WS_EMETTEUR) != null) {
                    String username = (String) this.getNodeService().getProperty(dossierRef, ParapheurModel.PROP_WS_EMETTEUR);
                    res.setEmailEmetteur(parapheurService.findEmailForUsername(username));
                }
                res.setDossierID(getDossierRequest);

                // List<NodeRef> documents = this.parapheurService.getDocuments(dossierRef);
                List<NodeRef> documentsP = this.parapheurService.getMainDocuments(dossierRef);

                // 1er Document principal
                ContentReader mainDocReader = contentService.getReader(documentsP.get(0), ContentModel.PROP_CONTENT);

                // Nom 1er document principal
                String mainDocName = (String) nodeService.getProperty(documentsP.get(0), ContentModel.PROP_NAME);
                res.setNomDocPrincipal(mainDocName);

                boolean isDossierHelios = false;
                boolean isDossierSigne = false;
                String signatureFormat = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT);
                if ("HELIOS".equalsIgnoreCase((String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_PROTOCOLE))) {
                    TypeDoc pes = new TypeDoc();
                    pes.setContentType(mainDocReader.getMimetype());
                    File fic = TempFileProvider.createTempFile("pes", "xml");
                    mainDocReader.getContent(fic);
                    pes.setValue(getBytesFromFile(fic));
                    res.setFichierPES(pes);
                    isDossierHelios = true;
                    // par abus de language, on va considerer que le dossier HELIOS est toujours signe.
                    isDossierSigne = true;
                } else {
                    TypeDoc mainDoc = new TypeDoc();
                    mainDoc.setContentType(mainDocReader.getMimetype());
                    File fic = TempFileProvider.createTempFile(mainDocName, "tmp");
                    mainDocReader.getContent(fic);
                    mainDoc.setValue(getBytesFromFile(fic));
                    res.setDocPrincipal(mainDoc);

                    // Attention cas PAdES/basic: on ne remplit pas la structure 'SignatureDocPrincipal'
                    if ("PKCS#7/multiple".equals(signatureFormat)) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("CMS-AllInOne - prendre une seule signature");
                        }
                        byte[] signature = parapheurService.getSignature(dossierRef);
                        if (signature != null) {
                            TypeDoc sigDoc = new TypeDoc();
                            sigDoc.setContentType("application/pkcs7-signature");
                            sigDoc.setValue(signature);
                            res.setSignatureDocPrincipal(sigDoc);
                            isDossierSigne = true;
                        }
                    } else if (signatureFormat != null
                            && !signatureFormat.startsWith("PAdES")
                            && !signatureFormat.equals("XAdES/DIA")) {
                        TypeDoc sigDoc = new TypeDoc();
                        sigDoc.setContentType("application/zip");
                        File zipFic = parapheurService.produceZipSignatures(mainDocName, dossierRef);
                        if (zipFic != null) {
                            sigDoc.setValue(getBytesFromFile(zipFic));
                            res.setSignatureDocPrincipal(sigDoc);
                            isDossierSigne = true;
                        }
                    }
                }

                /**
                 * l'usine à gaz pour détecter si un dossier est effectivement signé...
                 * Le défi est sur les signatures enveloppées ! DIA, PAdES
                 */
                if (!isDossierSigne && signatureFormat != null
                        // && "archive".equals((String)nodeService.getProperty(dossierRef, ParapheurModel.PROP_STATUS_METIER))
                        && (signatureFormat.startsWith("PAdES") || signatureFormat.equals("XAdES/DIA"))
                        && !((Boolean) nodeService.getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_PAPIER))) {
                    List<LogDossier> logsDossier = dossierService.getLogsDossier(dossierRef);
                    for (LogDossier logDossier : logsDossier) {
                        if (logDossier.getStatus().equals(StatusMetier.STATUS_SIGNE)) {
                            isDossierSigne = true;
                            break;
                        }
                    }
                }

                // Cas multi-documents principaux (aka: docs supplementaires).
                if (typesService.isMultiDocument(res.getTypeTechnique(), res.getSousType()) && documentsP.size() > 1) {
                    // alors y faut donner les autres....
                    TypeDocAnnexes listeDocsSupp = new TypeDocAnnexes();
                    int nbDocSupp = documentsP.size();
                    for (int i = 1; i < nbDocSupp; i++) {
                        NodeRef docSuppRef = documentsP.get(i);
                        if (docSuppRef != null) {
                            ContentReader docSuppContentReader = contentService.getReader(docSuppRef, ContentModel.PROP_CONTENT);
                            DocAnnexe docAnnexe = new DocAnnexe();
                            docAnnexe.setNom((String) nodeService.getProperty(docSuppRef, ContentModel.PROP_NAME));
                            docAnnexe.setMimetype(docSuppContentReader.getMimetype());
                            docAnnexe.setEncoding(docSuppContentReader.getEncoding());
                            TypeDoc leDoc = new TypeDoc();
                            leDoc.setContentType(docSuppContentReader.getMimetype());
                            File fic = TempFileProvider.createTempFile(docAnnexe.getNom(), "tmp");
                            docSuppContentReader.getContent(fic);//bug!!?
                            leDoc.setValue(getBytesFromFile(fic));
                            docAnnexe.setFichier(leDoc);
                            listeDocsSupp.getDocAnnexe().add(docAnnexe);
                        }
                    }
                    res.setDocumentsSupplementaires(listeDocsSupp);
                }

                // Documents annexes ... (c'est pas des docs principaux)
                boolean hasPrintPdfToGive = parapheurService.isParapheurWsGetdossierPdfEnabled();
                // BLEX
                boolean isSrci = this.nodeService.hasAspect(dossierRef, SrciService.K.aspect_srciTransaction);
                boolean hasPesAquitToGive = isDossierHelios && (this.nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_S2LOW) || isSrci);
                List<NodeRef> docAnnexes = this.parapheurService.getAttachments(dossierRef);
                int nbDocAnnexes = docAnnexes.size();
                if (nbDocAnnexes > 0 || hasPrintPdfToGive || hasPesAquitToGive) {
                    TypeDocAnnexes typeDocAnnexes = new TypeDocAnnexes();
                    for (int i = 0; i < nbDocAnnexes; i++) {
                        NodeRef idAnnexe = docAnnexes.get(i);
                        if (idAnnexe != null) {
                            ContentReader annexeContentReader = contentService.getReader(idAnnexe, ContentModel.PROP_CONTENT);
                            DocAnnexe docAnnexe = new DocAnnexe();
                            docAnnexe.setNom((String) nodeService.getProperty(idAnnexe, ContentModel.PROP_NAME));
                            docAnnexe.setMimetype(annexeContentReader.getMimetype());
                            docAnnexe.setEncoding(annexeContentReader.getEncoding());
                            TypeDoc leDoc = new TypeDoc();
                            leDoc.setContentType(annexeContentReader.getMimetype());
                            File fic = TempFileProvider.createTempFile(docAnnexe.getNom(), "tmp");
                            annexeContentReader.getContent(fic);//bug!!
                            leDoc.setValue(getBytesFromFile(fic));
                            docAnnexe.setFichier(leDoc);
                            typeDocAnnexes.getDocAnnexe().add(docAnnexe);
                        }
                    }
                    if (hasPrintPdfToGive) {
                        try {
                            DocAnnexe docAnnexe = new DocAnnexe();
                            docAnnexe.setNom(parapheurService.getParapheurWsGetdossierPdfDocName());
                            docAnnexe.setMimetype(MimetypeMap.MIMETYPE_PDF);
                            docAnnexe.setEncoding("utf-8"); // j'ai bon là??
                            TypeDoc leDoc = new TypeDoc();
                            leDoc.setContentType(MimetypeMap.MIMETYPE_PDF);
                            File fic = parapheurService.genererDossierPDF(dossierRef, false, parapheurService.areAttachmentsIncluded(dossierRef), true);
                            leDoc.setValue(getBytesFromFile(fic));
                            docAnnexe.setFichier(leDoc);
                            typeDocAnnexes.getDocAnnexe().add(docAnnexe);
                        } catch (org.alfresco.service.cmr.repository.ContentIOException ioe) {
                            res.getMessageRetour().setMessage("Impossible de produire le PDF d'impression pour le dossier '" + getDossierRequest + "'.");
                            res.getMessageRetour().setSeverite("WARN");
                            if (logger.isEnabledFor(Level.WARN)) {
                                logger.warn("Impossible de produire le PDF d'impression pour le dossier '" + getDossierRequest + "'.", ioe);
                            }
                            // NON BLOQUANT, pas d'exception. throw new DocumentException("");
                        }
                    }
                    /**
                     * Si c'est un dossier HELIOS et qu'il a été télétransmis
                     * par i-parapheur, alors on a un retour TDT à fournir...
                     */
                    if (hasPesAquitToGive) {
                        try {    // Ajouter le pesACQUIT si il est présent.

                            ContentReader reader;
                            // BLEX
                            if (isSrci) {
                                reader = contentService.getReader(dossierRef, SrciService.K.property_srciTransaction_nackHeliosXml);
                            } else {
                                String tdtNom = (String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_NOM);

                                if ("FAST".equals(tdtNom)) {
                                    reader = contentService.getReader(dossierRef, ParapheurModel.PROP_ARACTE_XML);
                                } else {
                                    reader = contentService.getReader(dossierRef, ParapheurModel.PROP_NACKHELIOS_XML);

                                }
                            }

                            if (reader != null) {
                                DocAnnexe docAnnexe = new DocAnnexe();
                                docAnnexe.setNom("Fichier_retour_HELIOS.xml");
                                docAnnexe.setMimetype(reader.getMimetype());
                                docAnnexe.setEncoding(reader.getEncoding());
                                TypeDoc leDoc = new TypeDoc();
                                leDoc.setContentType(reader.getMimetype());
                                File fic = TempFileProvider.createTempFile("retour_pes", "xml");
                                reader.getContent(fic);
                                leDoc.setValue(getBytesFromFile(fic));
                                docAnnexe.setFichier(leDoc);
                                typeDocAnnexes.getDocAnnexe().add(docAnnexe);
                            }
                        } catch (org.alfresco.service.cmr.repository.ContentIOException ioe) {
                            res.getMessageRetour().setMessage("Impossible de fournir le fichier retour TDT pour le dossier '" + getDossierRequest + "'.");
                            res.getMessageRetour().setSeverite("WARN");
                            if (logger.isEnabledFor(Level.WARN)) {
                                logger.warn("Impossible de fournir le fichier retour TDT pour le dossier '" + getDossierRequest + "'.", ioe);
                            }
                            // NON BLOQUANT, pas d'exception. throw new DocumentException("");
                        } catch (InvalidNodeRefException ex) {
                            logger.warn(ex);
                        } catch (InvalidTypeException ex) {
                            logger.warn(ex);
                        } catch (IOException ex) {
                            logger.warn(ex);
                        }
                    }
                    res.setDocumentsAnnexes(typeDocAnnexes);
                }

                if (this.getNodeService().getProperty(dossierRef, ParapheurModel.PROP_XPATH_SIGNATURE) != null) {
                    res.setXPathPourSignaturePES(this.getNodeService().getProperty(dossierRef, ParapheurModel.PROP_XPATH_SIGNATURE).toString());
                }
                if (this.parapheurService.isTermine(dossierRef)) {
                    // Annotation sur dossier terminé....(ben on prend celle de la dernière étape, quitte à aller la chercher!!)
                    if (logger.isDebugEnabled()) {
                        logger.debug("get annotations: dossier terminé.");
                    }
                    String pubAnnot = null;
                    String prvAnnot = null;
                    for (com.atolcd.parapheur.repo.EtapeCircuit etape : parapheurService.getCircuit(dossierRef)) {
                        if (!etape.getActionDemandee().equalsIgnoreCase(EtapeCircuit.ETAPE_ARCHIVAGE)
                                && etape.isApproved()) {
                            pubAnnot = etape.getAnnotation();
                            prvAnnot = etape.getAnnotationPrivee();
                        }
                    }
                    res.setAnnotationPublique((pubAnnot == null) ? "" : pubAnnot);
                    res.setAnnotationPrivee((prvAnnot == null) ? "" : prvAnnot);
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("get annotations: dossier PAS terminé.");
                    }
                    String pubAnnot = parapheurService.getAnnotationPublique(dossierRef);
                    String prvAnnot = parapheurService.getAnnotationPrivee(dossierRef);
                    res.setAnnotationPublique((pubAnnot == null) ? "" : pubAnnot);
                    res.setAnnotationPrivee((prvAnnot == null) ? "" : prvAnnot);
                }
                /**
                 * Visibilité.
                 */
                if (this.getNodeService().getProperty(dossierRef, ParapheurModel.PROP_CONFIDENTIEL) == Boolean.TRUE) {
                    res.setVisibilite(Visibilite.CONFIDENTIEL);
                } else if (this.getNodeService().getProperty(dossierRef, ParapheurModel.PROP_PUBLIC) == Boolean.TRUE) {
                    res.setVisibilite(Visibilite.PUBLIC);
                } else {
                    res.setVisibilite(Visibilite.SERVICE);
                }
                if (this.getNodeService().getProperty(dossierRef, ParapheurModel.PROP_DATE_LIMITE) != null) {
                    res.setDateLimite(this.getNodeService().getProperty(dossierRef, ParapheurModel.PROP_DATE_LIMITE).toString());
                }

                /**
                 * MetaDonnees.
                 */
                List<CustomMetadataDef> mdliste = metadataService.getMetadataDefs();
                TypeMetaDonnees mdonnees = new TypeMetaDonnees();
                // Changement 3.5 : on ajoute le titre du dossier en méta-donnée.
                MetaDonnee mdTitreDossier = new MetaDonnee();
                mdTitreDossier.setNom("ph:dossierTitre");
                mdTitreDossier.setValeur((String) this.getNodeService().getProperty(dossierRef, ContentModel.PROP_TITLE));
                mdonnees.getMetaDonnee().add(mdTitreDossier);
                // Ajout pour 4.2.07+ : ajout du format de signature en méta-donnée.
                if (isDossierSigne) {
                    MetaDonnee mdSigFormat = new MetaDonnee();
                    mdSigFormat.setNom("ph:signatureFormat");
                    mdSigFormat.setValeur((String) signatureFormat);
                    mdonnees.getMetaDonnee().add(mdSigFormat);
                }

                // Ajout pour 4.4.2 : informations de télétransmission ACTES
                if (nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_S2LOW)) {
                    try {
                        MetaDonnee mdActeDate = new MetaDonnee();
                        mdActeDate.setNom("ph:" + ParapheurModel.PROP_TDT_ACTES_DATE.getLocalName());
                        mdActeDate.setValeur((String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_DATE));
                        mdonnees.getMetaDonnee().add(mdActeDate);

                        MetaDonnee mdActeClassif = new MetaDonnee();
                        mdActeClassif.setNom("ph:" + ParapheurModel.PROP_TDT_ACTES_CLASSIFICATION.getLocalName());
                        mdActeClassif.setValeur((String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_CLASSIFICATION));
                        mdonnees.getMetaDonnee().add(mdActeClassif);

                        MetaDonnee mdActeNature = new MetaDonnee();
                        mdActeNature.setNom("ph:" + ParapheurModel.PROP_TDT_ACTES_NATURE.getLocalName());
                        mdActeNature.setValeur((String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_NATURE));
                        mdonnees.getMetaDonnee().add(mdActeNature);

                        MetaDonnee mdActeObjet = new MetaDonnee();
                        mdActeObjet.setNom("ph:" + ParapheurModel.PROP_TDT_ACTES_OBJET.getLocalName());
                        mdActeObjet.setValeur((String) nodeService.getProperty(dossierRef, ParapheurModel.PROP_TDT_ACTES_OBJET));
                        mdonnees.getMetaDonnee().add(mdActeObjet);
                    } catch (Exception e) {
                        // Don't break any GetDossier with this, but log the exception
                        if (logger.isEnabledFor(Level.WARN)) {
                            logger.warn("Impossible de fournir les information de TDT ACTES pour le dossier '" + getDossierRequest + "'.", e);
                        }
                    }
                }

                if (!mdliste.isEmpty() && this.nodeService.hasAspect(dossierRef, QName.createQName("cu:customMetadata", this.getNamespaceService()))) {
                    for (CustomMetadataDef customMetadataDef : mdliste) {
                        if (this.getNodeService().getProperty(dossierRef, customMetadataDef.getName()) != null) {
                            MetaDonnee md = new MetaDonnee();
                            md.setNom(customMetadataDef.getName().getLocalName());
                            switch (customMetadataDef.getType()) {
                                case DATE:
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    Date date = (Date) this.getNodeService().getProperty(dossierRef, customMetadataDef.getName());
                                    md.setValeur(dateFormat.format(date));
                                    break;
                                case DOUBLE:
                                    Double vdouble = (Double) this.getNodeService().getProperty(dossierRef, customMetadataDef.getName());
                                    md.setValeur(vdouble.toString());
                                    break;
                                case INTEGER:
                                    Integer vinteger = (Integer) this.getNodeService().getProperty(dossierRef, customMetadataDef.getName());
                                    md.setValeur(vinteger.toString());
                                    break;
                                case BOOLEAN:
                                    Boolean vboolean = (Boolean) this.getNodeService().getProperty(dossierRef, customMetadataDef.getName());
                                    md.setValeur(vboolean.toString());
                                case STRING:
                                default:
                                    md.setValeur((String) this.getNodeService().getProperty(dossierRef, customMetadataDef.getName()).toString());
                            }
                            mdonnees.getMetaDonnee().add(md);
                        }
                    }
                }
                if (!mdonnees.getMetaDonnee().isEmpty()) {
                    res.setMetaDonnees(mdonnees);
                }
                
                /* ----------------------------------
                *  Berger Levrault - TMC capability
                *              BEGIN
                * ----------------------------------
                * Correction Mantis 58808
                * Repositionnement à la fin de la méthode pour ne pas interférer avec les actions qui peuvent être fait 
                * avant et qui nécvessiteraient le "vrai" type.
                */
                if ("blex".equals(parapheurService.getHabillage()) &&
                    this.getNodeService().hasAspect(dossierRef, ParapheurModel.ASPECT_TYPAGE_METIER)) {
                    // Dans le cas du TMC, nous reçevons un type <idCollec>.PES mais nous
                    // devons renvoyer juste PES
                    String idCollectivite = "";
                    if (userName.contains(".")) {
                        idCollectivite = userName.substring(0, userName.indexOf(".")) + ".";
                    }
                    String typeTechnique = res.getTypeTechnique();
                    res.setTypeTechnique(typeTechnique.replace(idCollectivite, ""));
                }
                /* ----------------------------------
                *   Berger Levrault - TMC capability
                *               END
                * ----------------------------------
                */  
                
                res.getMessageRetour().setCodeRetour("OK");
                if (!"WARN".equalsIgnoreCase(res.getMessageRetour().getSeverite())) {
                    res.getMessageRetour().setSeverite("INFO");
                    res.getMessageRetour().setMessage("");
                }
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Le dossierID '" + getDossierRequest + "' est inconnu dans le Parapheur.");
                }
                res.getMessageRetour().setMessage("Le dossierID '" + getDossierRequest + "' est inconnu dans le Parapheur.");
                throw new DocumentException("Le dossierID '" + getDossierRequest + "' est inconnu dans le Parapheur.");
            }
            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getDossier");
            res.getMessageRetour().setMessage(e.getLocalizedMessage());
            logger.error(e.getLocalizedMessage()); // (e.getMessage(), e);
        }
        return res;
    }

    @Override
    public GetHistoDossierResponse getHistoDossier(String getHistoDossierRequest) {

        GetHistoDossierResponse retVal = new GetHistoDossierResponse();

        MessageRetour retMsg = new MessageRetour();

        retVal.setMessageRetour(retMsg);
        retVal.getMessageRetour().setCodeRetour("KO");
        retVal.getMessageRetour().setSeverite("ERROR");
        retVal.getMessageRetour().setMessage("");

        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getHistoDossier(\"" + getHistoDossierRequest + "\") invoqué par " + userName);
        }
        if (userName == null) {
            retVal.getMessageRetour().setMessage(this.pErrMsg);
            return retVal;
        }
        if (!isNomDossierValide(getHistoDossierRequest)) {
            if (logger.isDebugEnabled()) {
                logger.debug("   Syntaxe dossierID=[" + getHistoDossierRequest + "] incorrecte");
            }
            retVal.getMessageRetour().setMessage("Syntaxe dossierID=[" + getHistoDossierRequest + "] incorrecte.");
            return retVal;
        }
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        // Lazy initialization
        if (nakedParapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("nakedParapheurService non initialisé...");
            }
            this.getNakedParapheurService();
        }
        if (dossierService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("dossierService non initialisé...");
            }
            this.getDossierService();
        }

        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();

        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);
            NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
            if (parapheur == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("parapheur == null !!");
                }
                //msg.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                throw new DocumentException(userName + " n'a pas de parapheur.");
            }

            // recherche locale
            NodeRef dossierRef = nakedParapheurService.getDossierFromBureauAndName(parapheur, getHistoDossierRequest);
            if (dossierRef == null) {
                // recherche etendue
                dossierRef = nakedParapheurService.rechercheDossier(getHistoDossierRequest);
                // oui, parfois la recherche ne donne rien, alors qu'il existe!
                // alorsqu'une seconde tentative passe... WTF? on le fait qd mm
                if (dossierRef == null) {
                    if (logger.isInfoEnabled()) {
                        logger.info("1ere recherche infructueuse 2nde tentative pour '" + getHistoDossierRequest + "'");
                    }
                    dossierRef = nakedParapheurService.rechercheDossier(getHistoDossierRequest);
                    if (dossierRef != null && logger.isInfoEnabled()) {
                        logger.info("2nde tentative de recherche OK pour '" + getHistoDossierRequest + "'");
                    }
                }
            }
            if (logger.isDebugEnabled()) {
                logger.debug("With '" + getHistoDossierRequest + "' Found dossierRef=" + dossierRef);
            }
            if (dossierRef != null && nakedParapheurService.isDossier(dossierRef)) {
                retVal.getMessageRetour().setCodeRetour("OK");
                retVal.getMessageRetour().setSeverite("INFO");
                retVal.getMessageRetour().setMessage("");

                if (logger.isDebugEnabled()) {
                    logger.debug("Avant dossierService.getLogsDossier: " + dossierRef);
                }
                List<LogDossier> logsDossier = dossierService.getLogsDossier(dossierRef);
                for (LogDossier logDossier : logsDossier) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Add logDossier : " + logDossier.getTimestamp().toString());
                    }
                    /* compatibilite ascendante pour les clients WebServices "PretVisa"->"EnCours" */
                    if (logDossier.getStatus().equals(StatusMetier.STATUS_PRET_VISA)) {
                        logDossier.setStatus(StatusMetier.STATUS_EN_COURS_VISA);
                    }
                    retVal.getLogDossier().add(logDossier);
                }
                /* On s'assure que les logDossier sont ordonnés selon les timestamps */
                List<LogDossier> log = retVal.getLogDossier();

                Comparator xmlGregorianComparator = new Comparator() {
                    @Override
                    public int compare(Object t, Object t1) {
                        LogDossier l = (LogDossier) t;
                        LogDossier l1 = (LogDossier) t1;

                        return l.getTimestamp().compare(l1.getTimestamp());
                    }
                };
                Collections.sort(log, xmlGregorianComparator);

            } else {   // traiter le cas inconnu
                if (dossierRef == null) {
                    logger.warn("Le dossierID '" + getHistoDossierRequest + "' est inconnu dans le Parapheur.   cause: dossierRef = null , introuvable");
                } else if (logger.isDebugEnabled()) {
                    logger.debug("Le dossierID '" + getHistoDossierRequest + "' est inconnu dans le Parapheur.   cause: dossierRef n'est pas un dossier");
                }
                throw new DocumentException("Le dossierID '" + getHistoDossierRequest + "' est inconnu dans le Parapheur.");
            }
            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getHistoDossier");
            retMsg.setCodeRetour("KO");
            retMsg.setSeverite("ERROR");
            retMsg.setMessage("" + e.getMessage());
            retVal.setMessageRetour(retMsg);
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return retVal;
    }

    @Override
    public GetListeMetaDonneesResponse getListeMetaDonnees(Object getListeMetaDonneesRequest) {
        GetListeMetaDonneesResponse res = new GetListeMetaDonneesResponse();
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getListeMetaDonnees invoqué par " + userName);
        }
        if (userName == null) {
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (metadataService == null) {
            if (logger.isDebugEnabled()) {
                logger.warn("metadataService non initialisé...");
            }
            this.getMetadataService();
        }
        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);

            List<CustomMetadataDef> mdliste = metadataService.getMetadataDefs();
            if (logger.isDebugEnabled()) {
                logger.debug(" listeMetaDonnées.size = " + mdliste.size());
            }
            if (!mdliste.isEmpty()) {
                for (CustomMetadataDef customMetadataDef : mdliste) {
                    MetaDonneeDefinition mdd = new MetaDonneeDefinition();
                    mdd.setNomCourt(customMetadataDef.getName().getLocalName());
                    mdd.setNomLong(customMetadataDef.getTitle());
                    CustomMetadataType type = customMetadataDef.getType();
                    switch (type) {
                        case BOOLEAN:
                            mdd.setNature(NatureMetaDonnee.BOOLEAN);
                            break;
                        case DATE:
                            mdd.setNature(NatureMetaDonnee.DATE);
                            break;
                        case DOUBLE:
                            mdd.setNature(NatureMetaDonnee.FLOAT);
                            break;
                        case INTEGER:
                            mdd.setNature(NatureMetaDonnee.INT);
                            break;
                        case STRING:
                        default:
                            mdd.setNature(NatureMetaDonnee.STRING);
                    }
                    res.getMetaDonnee().add(mdd);
                }
            }

            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getListeMetaDonnees");
            logger.error(e.getMessage(), e);
        }
        return res;
    }

    @Override
    public GetMetaDonneesRequisesPourTypeSoustypeResponse getMetaDonneesRequisesPourTypeSoustype(GetMetaDonneesRequisesPourTypeSoustypeRequest getMetaDonneesRequisesPourTypeSoustypeRequest) {
        GetMetaDonneesRequisesPourTypeSoustypeResponse res = new GetMetaDonneesRequisesPourTypeSoustypeResponse();
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getMetaDonneesRequisesPourTypeSoustype invoqué par " + userName);
        }
        if (userName == null
                || getMetaDonneesRequisesPourTypeSoustypeRequest.getTypeTechnique() == null
                || getMetaDonneesRequisesPourTypeSoustypeRequest.getSousType() == null
                || getMetaDonneesRequisesPourTypeSoustypeRequest.getTypeTechnique().trim().isEmpty()
                || getMetaDonneesRequisesPourTypeSoustypeRequest.getSousType().trim().isEmpty()) {
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (metadataService == null) {
            if (logger.isDebugEnabled()) {
                logger.warn("metadataService non initialisé...");
            }
            this.getMetadataService();
        }
        if (typesService == null) {
            if (logger.isDebugEnabled()) {
                logger.warn("typesService non initialisé...");
            }
            this.getTypesService();
        }
        try {
            tx.begin();
            /**
             * ce bloc est-il vraiment utile?
             */     /*
            if (!isUsernameExist(getMetaDonneesRequisesPourTypeSoustype.getIdUtilisateur())) {
                return res;
            }
            userName = getMetaDonneesRequisesPourTypeSoustype.getIdUtilisateur().trim();  */
            this.getAuthenticationComponent().setCurrentUser(userName);

            List<CustomMetadataDef> mdliste = metadataService.getMetadatas(
                    getMetaDonneesRequisesPourTypeSoustypeRequest.getTypeTechnique(),
                    getMetaDonneesRequisesPourTypeSoustypeRequest.getSousType());
            List<String> mandatoryMetadataDefs = typesService.getMandatoryMetadatas(
                    getMetaDonneesRequisesPourTypeSoustypeRequest.getTypeTechnique(),
                    getMetaDonneesRequisesPourTypeSoustypeRequest.getSousType());
            if (logger.isDebugEnabled()) {
                logger.debug(" listeMetaDonnées.size = " + mdliste.size());
            }
            if (!mdliste.isEmpty()) {
                for (CustomMetadataDef customMetadataDef : mdliste) {
                    MetaDonneeDefinition mdd = new MetaDonneeDefinition();
                    mdd.setNomCourt(customMetadataDef.getName().getLocalName());
                    mdd.setNomLong(customMetadataDef.getTitle());
                    CustomMetadataType type = customMetadataDef.getType();

                    List<String> values = customMetadataDef.getEnumValues();
                    if (values != null && !values.isEmpty()) {
                        for (String value : values) {
                            mdd.getValeurPossible().add(value);
                        }
                    }

                    switch (type) {
                        case BOOLEAN:
                            mdd.setNature(NatureMetaDonnee.BOOLEAN);
                            break;
                        case DATE:
                            mdd.setNature(NatureMetaDonnee.DATE);
                            break;
                        case DOUBLE:
                            mdd.setNature(NatureMetaDonnee.FLOAT);
                            break;
                        case INTEGER:
                            mdd.setNature(NatureMetaDonnee.INT);
                            break;
                        default:
                            mdd.setNature(NatureMetaDonnee.STRING);
                    }

                    // est-ce obligatoire ?
                    if (mandatoryMetadataDefs.contains(customMetadataDef.getName().toString())) {
                        mdd.setObligatoire(Boolean.TRUE);
                    } else {
                        mdd.setObligatoire(Boolean.FALSE);
                    }
                    res.getMetaDonnee().add(mdd);
                }
            }

            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getMetaDonneesRequisesPourTypeSoustype");
            logger.error(e.getMessage(), e);
        }
        return res;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    //    @SuppressWarnings("unchecked")
    @Override
    public GetListeSousTypesResponse getListeSousTypes(String getListeSousTypesRequest) {
        GetListeSousTypesResponse res = new GetListeSousTypesResponse();
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getListeSousTypes(\"" + getListeSousTypesRequest + "\") invoqué par " + userName);
        }
        if (getListeSousTypesRequest == null
                || getListeSousTypesRequest.trim().isEmpty()
                || userName == null) {
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }

        /* ----------------------------------
         *  Berger Levrault - Mathieu Passenaud
         *           TMC capability
         *              BEGIN
         * ----------------------------------
         */
        if ("blex".equals(parapheurService.getHabillage())) {
            // Dans le cas du TMC, si nous demandons un type PES nous devons
            // chercher l'existance d'un type <idCollec>.PES en priorité
            String idCollectivite = "";
            if (userName.contains(".")) {
                idCollectivite = userName.substring(0, userName.indexOf(".")) + ".";
            }
            List<String> sousTypes = null;
            try {

                sousTypes = parapheurService.getSavedSousTypes(getListeSousTypesRequest, parapheurService.getUniqueParapheurForUser(userName));
            } catch (Exception ex) {
                logger.debug("type does not exists, continue with given typename");
            }
            if (sousTypes != null) {
                getListeSousTypesRequest = (idCollectivite + getListeSousTypesRequest);
            }
        }
        /* ----------------------------------
         *               END
         * ----------------------------------
         */


        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);

            res.getSousType().addAll(parapheurService.getSavedSousTypes(getListeSousTypesRequest, parapheurService.getUniqueParapheurForUser(userName)));

            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getListeSousTypes");
            logger.error(e.getMessage(), e);
        }
        return res;
    }

    @Override
    public GetListeSousTypesPourUtilisateurResponse getListeSousTypesPourUtilisateur(GetListeSousTypesPourUtilisateurRequest getListeSousTypesPourUtilisateurRequest) {
        GetListeSousTypesPourUtilisateurResponse res = new GetListeSousTypesPourUtilisateurResponse();
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getListeSousTypesPourUtilisateur(\"" + getListeSousTypesPourUtilisateurRequest.getTypeTechnique()
                    + "\", \"" + getListeSousTypesPourUtilisateurRequest.getIdUtilisateur() + "\") invoqué par " + userName);
        }
        if (getListeSousTypesPourUtilisateurRequest == null
                || getListeSousTypesPourUtilisateurRequest.getTypeTechnique() == null
                || getListeSousTypesPourUtilisateurRequest.getTypeTechnique().trim().isEmpty()
                || getListeSousTypesPourUtilisateurRequest.getIdUtilisateur() == null
                || getListeSousTypesPourUtilisateurRequest.getIdUtilisateur().trim().isEmpty()
                || userName == null) {
            if (userName == null) {
                logger.warn(" Erreur sur getListeTypesPourUtilisateur car: " + this.pErrMsg);
            }
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        try {
            tx.begin();
            if (!isUsernameExist(getListeSousTypesPourUtilisateurRequest.getIdUtilisateur())) {
                return res;
            }
            userName = getListeSousTypesPourUtilisateurRequest.getIdUtilisateur().trim();
            this.getAuthenticationComponent().setCurrentUser(userName);
            res.getSousType().addAll(parapheurService.getSavedSousTypes(getListeSousTypesPourUtilisateurRequest.getTypeTechnique(), parapheurService.getUniqueParapheurForUser(userName)));

            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getListeTypesPourUtilisateur");
            logger.error(e.getMessage(), e);
        }
        return res;
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //    @SuppressWarnings("unchecked")
    @Override
    public GetListeTypesResponse getListeTypes(Object getListeTypesRequest) {
        GetListeTypesResponse res = new GetListeTypesResponse();
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getListeTypes invoqué par " + userName);
        }
        if (userName == null) {
            logger.warn(" Erreur sur getListeTypes car: " + this.pErrMsg);
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        if (typesService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("typesService non initialisé...");
            }
            this.getTypesService();
        }
        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);

// ne pas utiliser parapheurService.getSavedTypesFull(), car il est possible de récupérer des types auxquels le WS n'a pas droit d'acces
            /* ----------------------------------
             *  Berger Levrault - Mathieu Passenaud
             *           TMC capability
             *              BEGIN
             * ----------------------------------
             */
            if ("blex".equals(parapheurService.getHabillage())) {
                // Dans le cas du TMC, si nous demandons un type PES nous devons
                // chercher l'existance d'un type <idCollec>.PES en priorité
                String idCollectivite = "";
                if (userName.contains(".")) {
                    idCollectivite = userName.substring(0, userName.indexOf(".")) + ".";
                }

                Map<String, List<String>> typesAsList = typesService.optimizedGetSavedType(false, parapheurService.getUniqueParapheurForUser(userName));
                for (String type : typesAsList.keySet()) {
                    res.getTypeTechnique().add(type.replace(idCollectivite, ""));
                }
            } else {
                res.getTypeTechnique().addAll(typesService.optimizedGetSavedType(false, parapheurService.getUniqueParapheurForUser(userName)).keySet());
            }
            /* ----------------------------------
             *               END
             * ----------------------------------
             */

            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getListeTypes");
            logger.error(e.getMessage(), e);
        }
        return res;
    }

    @Override
    public GetListeTypesPourUtilisateurResponse getListeTypesPourUtilisateur(String getListeTypesPourUtilisateurRequest) {
        GetListeTypesPourUtilisateurResponse res = new GetListeTypesPourUtilisateurResponse();
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getListeTypesPourUtilisateur(\"" + getListeTypesPourUtilisateurRequest + "\") invoqué par " + userName);
        }
        if (userName == null) {
            logger.warn(" Erreur sur getListeTypesPourUtilisateur car: " + this.pErrMsg);
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        if (typesService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("typesService non initialisé...");
            }
            this.getTypesService();
        }
        try {
            tx.begin();
            if (!isUsernameExist(getListeTypesPourUtilisateurRequest)) {
                return res;
            }
            userName = getListeTypesPourUtilisateurRequest.trim();
            this.getAuthenticationComponent().setCurrentUser(userName);
            // System.out.println("\tUser=" + userName );
            // System.out.println("\tparapheur=" + parapheurService.getUniqueParapheurForUser(userName));
            res.getTypeTechnique().addAll(typesService.optimizedGetSavedType(false, parapheurService.getUniqueParapheurForUser(userName)).keySet());

            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getListeTypesPourUtilisateur");
            logger.error(e.getMessage(), e);
        }
        return res;
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public IsUtilisateurExisteResponse isUtilisateurExiste(String isUtilisateurExisteRequest) {
        IsUtilisateurExisteResponse res = new IsUtilisateurExisteResponse();
        MessageRetour msg = new MessageRetour();
        msg.setCodeRetour("KO");
        msg.setSeverite("INFO");
        msg.setMessage("WebService non implémenté.");
        res.setMessageRetour(msg);
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService isUtilisateurExiste(\"" + isUtilisateurExisteRequest + "\") invoqué par " + userName);
        }
        if (userName == null) {
            res.getMessageRetour().setMessage(this.pErrMsg);
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        try {
            tx.begin();
            if (isUsernameExist(isUtilisateurExisteRequest)) {
                res.getMessageRetour().setCodeRetour("OK");
                res.getMessageRetour().setSeverite("INFO");
                res.getMessageRetour().setMessage(isUtilisateurExisteRequest + " existe.");
            } else {
                res.getMessageRetour().setCodeRetour("KO");
                res.getMessageRetour().setSeverite("INFO");
                res.getMessageRetour().setMessage("Utilisateur inconnu.");
            }
            if (logger.isDebugEnabled()) {
                logger.debug(res.getMessageRetour().getMessage());
            }
            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "isUtilisateurExiste");
            logger.error(e.getMessage(), e);
        }
        return res;
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    private boolean isUsernameExist(String username) {
        if (username != null && !username.trim().isEmpty()) {
            if (personService == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("personService non initialisé...");
                }
                this.getPersonService();
            }
            //
            return personService.personExists(username);
        } else {
            return false;
        }
    }

    @Override
    public GetStatutTdTResponse getStatutTdT(String getStatutTdTRequest) {
        GetStatutTdTResponse res = new GetStatutTdTResponse();
        MessageRetour msg = new MessageRetour();
        LogDossier logdossier = new LogDossier();
        msg.setCodeRetour("KO");
        msg.setSeverite("INFO");
        msg.setMessage("WebService non implémenté.");
        res.setMessageRetour(msg);
        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService getStatutTdT(\"" + getStatutTdTRequest + "\") invoqué par " + userName);
        }
        if (userName == null) {
            res.getMessageRetour().setMessage(this.pErrMsg);
            return res;
        }
        if (!isNomDossierValide(getStatutTdTRequest)) {
            res.getMessageRetour().setMessage("Syntaxe dossierID incorrecte");
            return res;
        }
        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);
            NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
            if (parapheur == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("parapheur == null !!");
                }
                msg.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                throw new DocumentException(userName + " n'a pas de parapheur.");
            }

            NodeRef nodeRef = checkForNodeRefFromDossierID(parapheur, getStatutTdTRequest);
            if (nodeRef != null && parapheurService.isDossier(nodeRef)) {
                // Timestamp
                Date maDate = DefaultTypeConverter.INSTANCE.convert(Date.class, this.getNodeService().getProperty(nodeRef, ContentModel.PROP_MODIFIED));
                GregorianCalendar gCalendar = new GregorianCalendar();
                gCalendar.setTime(maDate);
                XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
                logdossier.setTimestamp(date);
                // Nom
                logdossier.setNom(this.getNodeService().getProperty(nodeRef, ContentModel.PROP_NAME).toString());
                // Status: appel au S2LOW-SERVICE
                // BLEX
                // logdossier.setStatus(this.getS2lowService().statutS2lowToString(this.getS2lowService().getInfosS2low(nodeRef)));
                if (getNodeService().hasAspect(nodeRef, ParapheurModel.ASPECT_S2LOW)) {
                    logdossier.setStatus(this.getS2lowService().statutS2lowToString(this.getS2lowService().getInfosS2low(nodeRef)));
                } else if (getNodeService().hasAspect(nodeRef, SrciService.K.aspect_srciTransaction)) {
                    logdossier.setStatus(getSrciService().getTdtStatusText(nodeRef));
                } else {
                    // une exception serait bienvenue ... mais on ne peut pas
                    // savoir comment cette methode (WS) est exploite. Donc pas.
                    // throw new IllegalArgumentException( "Le dossier ["+nodeService.getProperty(nodeRef, ContentModel.PROP_NAME)+"] n'a pas ete envoye au TDT.");
                }

                //logdossier.setAnnotation(??)  FIXME
                logdossier.setAnnotation("");
                // Accessible
                // Refaire une recherche si le noeud a bougé de bannette
                nodeRef = checkForNodeRefFromDossierID(parapheur, getStatutTdTRequest);
                if (nodeRef == null || !parapheurService.isDossier(nodeRef)) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Le dossierID '" + getStatutTdTRequest + "' est inconnu dans le Parapheur (2nde recherche!).");
                    }
                    throw new DocumentException("Le dossierID '" + getStatutTdTRequest + "' est inconnu dans le Parapheur.");
                }
                if (parapheurService.getEmetteur(nodeRef).equals(parapheurService.getParentParapheur(nodeRef))) {
                    logdossier.setAccessible("OK");
                } else {
                    logdossier.setAccessible("KO");
                }
                res.setLogDossier(logdossier);
                res.getMessageRetour().setCodeRetour("OK");
                res.getMessageRetour().setSeverite("INFO");
                res.getMessageRetour().setMessage("");
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Le dossierID '" + getStatutTdTRequest + "' est inconnu dans le Parapheur.");
                }
                throw new DocumentException("Le dossierID '" + getStatutTdTRequest + "' est inconnu dans le Parapheur.");
            }

            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "getStatutTdT");
            msg.setCodeRetour("KO");
            msg.setSeverite("ERROR");
            msg.setMessage(e.getMessage());
            res.setMessageRetour(msg);
            logger.error(e.getMessage(), e);
        }
        return res;
    }

    @Override
    public RechercherDossiersResponse rechercherDossiers(RechercherDossiersRequest rechercherDossiersRequest) {


        String type = rechercherDossiersRequest.getTypeTechnique();
        String sstype = rechercherDossiersRequest.getSousType();
        String status = rechercherDossiersRequest.getStatus();
        BigInteger nbre = rechercherDossiersRequest.getNombreDossiers();
        Boolean err = Boolean.FALSE;
        RechercherDossiersResponse res = new RechercherDossiersResponse();
        String userName = this.getValidUser();

        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }

        /* ----------------------------------
         *  Berger Levrault - Mathieu Passenaud
         *           TMC capability
         *              BEGIN
         * ----------------------------------
         */
        if ("blex".equals(parapheurService.getHabillage())) {                 //FIXME : parapheurService non initialisé, et en plus hors transaction??!
            // Dans le cas du TMC, si nous demandons un type PES nous devons
            // chercher l'existence d'un type <idCollec>.PES en priorité
            String idCollectivite = "";
            if (userName.contains(".")) {
                idCollectivite = userName.substring(0, userName.indexOf(".")) + ".";
            }
            List<String> sousTypes = null;
            try {
                // Blindage Adullact: type a le droit d'etre NULL en "recherche"
                if (type != null) {
                    sousTypes = parapheurService.getSavedSousTypes(type, parapheurService.getUniqueParapheurForUser(userName));
                }
            } catch (Exception ex) {
                logger.debug("type does not exists, continue with given typename");
            }
            if (sousTypes != null) {
                type = (idCollectivite + type);
            }
        }
        /* ----------------------------------
         *               END
         * ----------------------------------
         */


        if (logger.isInfoEnabled()) {
            logger.info("WebService rechercherDossiers invoqué par " + userName + ": '" + type + "'/'" + sstype + "', statut='" + status + "', nombreID=" + nbre);
        }
        if (userName == null) {
            logger.warn(" Erreur sur rechercherDossiers car: " + this.pErrMsg);
            return res;
        }
        if (nbre == null) // ANALYSE INPUT
        { // Traitement par défaut: Filtres type/ssType/Status
            if (type != null) {
                type = type.trim();
                if (type.isEmpty()) {
                    type = null;
                } else {
                    if (!type.matches("([A-Za-z])[A-Za-z0-9 .àâäéêèëïîöôùüç_\\-\\(\\)]{0,31}")) {
                        if (logger.isEnabledFor(Level.WARN)) {
                            logger.warn("\t type mal formatté.");
                        }
                        err = Boolean.TRUE;
                    }
                }
            }
            if (sstype != null) {
                sstype = sstype.trim();
                if (sstype.isEmpty()) {
                    sstype = null;
                } else {
                    if (!sstype.matches("([A-Za-z])[A-Za-z0-9 àâäéêèëïîöôùüç_\\-\\(\\)]{0,63}")) {
                        if (logger.isEnabledFor(Level.WARN)) {
                            logger.warn("\t sous-type mal formatté.");
                        }
                        err = Boolean.TRUE;
                    }
                }
            }
            if (status != null) {
                status = status.trim();
                if (status.isEmpty()) {
                    status = null;
                } else if (status.equals(StatusMetier.STATUS_EN_COURS_VISA)) {
                    //Compatibilité ascendante
                    status = StatusMetier.STATUS_PRET_VISA;
                } else {    // Les états: NonLu, Lu, Signe, RejetSignataire, EnCoursTransmission, TransmissionOK, TransmissionKO, Archive, ATraiter (cas PES_RETOUR)
                    if (!status.matches("^(NonLu|Lu|Signe|RejetSignataire|PretVisa|RejetVisa|PretTdT|EnCoursTransmission|TransmissionOK|TransmissionKO|RejetTransmission|Archive|ATraiter|PretCachet|RejetCachet|CachetOK)$")) {
                        if (logger.isEnabledFor(Level.WARN)) {
                            logger.warn("\t status mal formatté.");
                        }
                        err = Boolean.TRUE;
                    }
                }
            }
            if (err) {
                if (logger.isEnabledFor(Level.WARN)) {
                    logger.warn("Requete invalide avec type=" + type + " / sousType=" + sstype + " / état='" + status + "'");
                }
                return res;
            }
        } else {  // MODE LISTE: controle cohérence de nombre
            int taille = nbre.intValue();
            if ((taille < 1) || (taille != rechercherDossiersRequest.getDossierID().size())) {
                return res;
            }
        }

//        RetryingTransactionCallback<Object> txnWork = new RetryingTransactionCallback<Object>()
//                {
//                    public Object execute() throws Exception
//                    {
//                        // Do stuff
//                        Object result = null;// ...
//                        return result;
//                    }
//                };
        //      Object result = this.getTransactionService().getRetryingTransactionHelper().doInTransaction(txnWork, true);

        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        if (permissionService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("permissionService non initialisé...");
            }
            this.getPermissionService();
        }
        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);
            NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
            if (parapheur == null) {
                if (logger.isDebugEnabled()) {
                    logger.warn("parapheur == null !!");
                }
                // msg.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                // throw new DocumentException(userName + " n'a pas de parapheur.");
            }

            List<NodeRef> liste;    // RECHERCHE LISTE DE NODEREFS
            if (nbre == null) {   // MODE FILTRE
                liste = this.parapheurService.rechercheDossiers(type, sstype, status);
                if (logger.isDebugEnabled()) {
                    logger.debug(" rechercheDossiers( " + type + " , " + sstype + " , " + status + " )");
                }
            } else {  // MODE LISTE de dossiers
                liste = new ArrayList<NodeRef>();
                List<String> lID = rechercherDossiersRequest.getDossierID();
                for (int i = 0; i < nbre.intValue(); i++) {
                    NodeRef lref = null;
                    if (this.isNomDossierValide(lID.get(i))) {
                        lref = checkForNodeRefFromDossierID(parapheur, lID.get(i));
                    }
                    if (lref != null) {
                        liste.add(lref);
                    }
                }
                if (logger.isDebugEnabled()) {
                    logger.debug(" rechercheDossiers( " + liste.size() + " / " + nbre.toString() + " dossiers)");
                }
            }
            if (liste != null) {
                Iterator<NodeRef> it = liste.iterator();
                for (Iterator<NodeRef> ie = it; ie.hasNext(); ) {
                    NodeRef nodeRef = ie.next();
                    try {
//                        if (logger.isEnabledFor(Level.WARN)) {
//                            AccessStatus accessStatus = permissionService.hasPermission(nodeRef, PermissionService.CONSUMER);
//                            System.out.println("\n\n###===------  -----=====#####  PERMISSION CONSUMER = " + accessStatus.toString());
//                            java.util.Set<AccessPermission> perms = permissionService.getPermissions(nodeRef);
//                            if (perms.isEmpty()) {
//                                System.out.println("\n\n###===----- Arrrrggh  -----=====##### Y A PAS DE PERMISSION !!!!!!!!!!!!!!!!!!!!!!!!\n\n\n");
//                            } else {
//                                for (AccessPermission accessPermission : perms) {
//                                    System.out.println("Permission=" + accessPermission.getPermission() + ", to authority=" + accessPermission.getAuthority());
//                                }
//                            }
//                        }
                        if (permissionService.hasPermission(nodeRef, PermissionService.CONSUMER).toString().equals("DENIED")) {
                            if (logger.isEnabledFor(Level.INFO)) {
                                logger.info("Access Denied to node " + nodeRef.getId());
                            }
                        } else if (parapheurService.isDossier(nodeRef)) {
                            LogDossier logdossier = new LogDossier();
                            // Timestamp
                            Date maDate = DefaultTypeConverter.INSTANCE.convert(Date.class, this.getNodeService().getProperty(nodeRef, ContentModel.PROP_MODIFIED));
                            GregorianCalendar gCalendar = new GregorianCalendar();
                            gCalendar.setTime(maDate);
                            XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
                            logdossier.setTimestamp(date);
                            // Nom
                            logdossier.setNom(this.getNodeService().getProperty(nodeRef, ContentModel.PROP_NAME).toString());
                            // Status
                            if (this.getNodeService().getProperty(nodeRef, ParapheurModel.PROP_STATUS_METIER) == null) {
                                logdossier.setStatus("");
                            } else if (this.getNodeService().getProperty(nodeRef, ParapheurModel.PROP_STATUS_METIER).toString().equals(StatusMetier.STATUS_PRET_VISA)) {
                                logdossier.setStatus(StatusMetier.STATUS_EN_COURS_VISA);
                            } else {
                                logdossier.setStatus(this.getNodeService().getProperty(nodeRef, ParapheurModel.PROP_STATUS_METIER).toString());
                            }
                            if (!parapheurService.isTermine(nodeRef)) {
                                String annotation = parapheurService.getAnnotationPublique(nodeRef);
                                if (annotation == null) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("Annotation publique = NULL ???");
                                    }
                                    logdossier.setAnnotation("");
                                } else {
                                    logdossier.setAnnotation(annotation);
                                }
                                if (parapheurService.isActeurCourant(nodeRef, userName)) {
                                    logdossier.setAccessible("OK");
                                } else if (parapheurService.isRecuperable(nodeRef)) {
                                    // String fullName = parapheurService.getNomProprietaire(parapheurService.getUniqueParapheurForUser(userName));
                                    String fullName = parapheurService.getPrenomNomFromLogin(userName);
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("dossier recuperable, fullName=[" + fullName + "].");
                                    }
                                    //com.atolcd.parapheur.repo.EtapeCircuit curStep = parapheurService.getCurrentEtapeCircuit(nodeRef);
                                    com.atolcd.parapheur.repo.EtapeCircuit myStep = null;
                                    List<com.atolcd.parapheur.repo.EtapeCircuit> leCircuit = parapheurService.getCircuit(nodeRef);

                                    for (com.atolcd.parapheur.repo.EtapeCircuit etape : leCircuit) {
                                        if (!etape.isApproved()) {
                                            break;
                                        } else {
                                            myStep = etape;
                                        }
                                    }

                                    logdossier.setAccessible("KO");
                                    if (myStep != null) {
                                        // SI le signataire de l'étape précédente correspond à pUserName...
                                        String signataire = myStep.getSignataire();
                                        if (logger.isDebugEnabled()) {
                                            logger.debug("Signataire = [" + signataire + "], fullName = [" + fullName + "]");
                                        }
                                        if (signataire.equalsIgnoreCase(fullName)) {
                                            logdossier.setAccessible("Recuperable");
                                        }
                                    }
                                    /*           if (curStep == null) {
                                    if (logger.isDebugEnabled()) {logger.debug("curStep = NULL ???"); }
                                    } else {
                                    int rang = leCircuit.indexOf(curStep);
                                    if (rang < 1) {
                                    if (logger.isDebugEnabled()) {logger.debug("leCircuit.indexOf(curStep) = ["+rang+"] ???"); }
                                    } else {
                                    // SI le signataire de l'étape précédente correspond à pUserName...
                                    String signataire = leCircuit.get(rang -1).getSignataire();
                                    String fullName = parapheurService.getNomProprietaire(parapheurService.getOwnedParapheur(pUserName));
                                    if (logger.isDebugEnabled()) {
                                    logger.debug("Signataire = ["+signataire+"], fullName = ["+fullName+"]");
                                    }
                                    if (signataire.equalsIgnoreCase(fullName))
                                    logdossier.setAccessible("Recuperable");
                                    }
                                    } */
                                } else {
                                    logdossier.setAccessible("KO");
                                }
                            } else {   // Annotation sur dossier terminé....(ben on prend celle de la dernière étape, quitte à aller la chercher!!)
                                List<com.atolcd.parapheur.repo.EtapeCircuit> leCircuit = parapheurService.getCircuit(nodeRef);
                                String annot;
                                if (leCircuit != null) {
                                    annot = leCircuit.get(leCircuit.size() - 1).getAnnotation();
                                } else {
                                    annot = null;
                                }
                                if (annot == null) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("Annotation publique d'étape = NULL !!???");
                                    }
                                    logdossier.setAnnotation("");
                                } else {
                                    logdossier.setAnnotation(annot);
                                }
                                // Pour un dossier terminé: OK ou KO, car 'Recuperable' n'y a pas de sens.
                                //FIXME : le WS n'est pas forcement emetteur
//                                if (parapheurService.getEmetteur(nodeRef).equals(parapheurService.getParentParapheur(nodeRef))) {
//                                    logdossier.setAccessible("OK");
//                                } else {
//                                    logdossier.setAccessible("KO");
//                                }

                                // Relever le bureau que possede l'utilisateur WS
                                // NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
                                if (parapheur == null) {
                                    logger.warn("parapheur == null !!");
                                }
                                // Est-ce que le dossier est sur le bureau de l'utilisateur WS?
                                if (parapheurService.getParentParapheur(nodeRef).equals(parapheur)) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("\n\tparapheur=" + parapheur + "\n\tparent   =" + parapheurService.getParentParapheur(nodeRef));
                                    }
                                    logdossier.setAccessible("OK");
                                } else {
                                    logdossier.setAccessible("KO");
                                }
                            }
                            res.getLogDossier().add(logdossier);
                        }
                    } catch (InvalidNodeRefException e) {
                        logger.error("Traitement du noeud " + nodeRef.getId());
                        logger.error(e.getMessage(), e);
                    } catch (DatatypeConfigurationException e) {
                        logger.error("Traitement du noeud " + nodeRef.getId());
                        logger.error(e.getMessage(), e);
                    }
                }
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Pas de dossier trouvé dans la recherche");
                }
            }
            tx.commit();
        } catch (Exception e) {
            doRollback(tx, "rechercherDossiers");
            logger.error(e.getMessage(), e);
        }
        return res;
    }

    @Override
    public EnvoyerDossierMailSecuriseResponse envoyerDossierMailSecurise(
            @WebParam(name = "EnvoyerDossierMailSecuriseRequest", targetNamespace = "http://www.adullact.org/spring-ws/iparapheur/1.0", partName = "EnvoyerDossierMailSecuriseRequest")
                    EnvoyerDossierMailSecuriseRequest envoyerDossierMailSecuriseRequest) {
        EnvoyerDossierMailSecuriseResponse response = new EnvoyerDossierMailSecuriseResponse();

        String userName = this.getValidUser();
        if (logger.isInfoEnabled()) {
            logger.info("WebService envoyerDossierMailSecurise(\"" + envoyerDossierMailSecuriseRequest.getDossier() + "\",...) invoqué par " + userName);
        }

        MessageRetour retour = new MessageRetour();
        retour.setCodeRetour("OK");
        retour.setMessage("Toto aime les haricots");
        response.setMessageRetour(retour);


        String dossierName = envoyerDossierMailSecuriseRequest.getDossier();
        String rcptTo = envoyerDossierMailSecuriseRequest.getRcptTo();
        String subject = envoyerDossierMailSecuriseRequest.getSubject();
        String message = envoyerDossierMailSecuriseRequest.getMessage();
        String password = envoyerDossierMailSecuriseRequest.getPassword();
        Boolean sendPassword = envoyerDossierMailSecuriseRequest.isSendPassword();

        if (sendPassword == null) {
            sendPassword = Boolean.FALSE;
        }

        if (dossierName == null || rcptTo == null) {
            retour.setCodeRetour("KO");
            retour.setMessage("dossierName et rcptTo sont des paramètres obligatoires.");
            return response;
        }

        UserTransaction tx;
        tx = this.getTransactionService().getUserTransaction();

        try {
            tx.begin();
            this.getAuthenticationComponent().setCurrentUser(userName);

            NodeRef parapheur = parapheurService.getUniqueParapheurForUser(userName);
            if (parapheur == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("parapheur == null !!");
                }
                retour.setMessage("Requete incorrecte, '" + userName + "' n'a pas de parapheur.");
                throw new DocumentException(userName + " n'a pas de parapheur.");
            }

            List<String> emails = new ArrayList<String>();

            NodeRef dossierRef = checkForNodeRefFromDossierID(parapheur, dossierName);

            if (dossierRef != null) {

                // Using default template or using the string passed as message as freemarker.
                if (message == null) {
                    message = s2lowService.getSecureMailTemplate(dossierRef);
                } else {
                    message = s2lowService.getSecureMailMessageWithTemplate(dossierRef, message);
                }

                EtapeCircuit currentStep = getParapheurService().getCurrentEtapeCircuit(dossierRef);

                if (EtapeCircuit.ETAPE_MAILSEC.equals(currentStep.getActionDemandee())) {
                    //TODO: Handle multiple mail addresses !
                    emails.add(rcptTo);
                    try {
                        Integer mail_id = getS2lowService().sendSecureMail(emails, null, null, subject, message, password, sendPassword, dossierRef, parapheurService.getAttachments(dossierRef), true);
                    } catch (IOException e) {
                        retour.setCodeRetour("KO");
                        retour.setMessage(e.getMessage());
                    }
                } else {
                    retour.setCodeRetour("KO");
                    retour.setMessage(String.format("L'étape courante du dossier %s n'est pas une étape de type Mail sécurisé.", dossierName));
                }
            } else {
                retour.setCodeRetour("KO");
                retour.setMessage("Le dossier est introuvable");
            }
            tx.commit();
        } catch (Exception t) {
            doRollback(tx, "envoyerDossierMailSecurise");
        }

        return response;
    }

    private String getValidUser() {
        String lUserName = this.getUsernameFromAuthentication(); // authenticationComponent.getCurrentUserName();
        // logger.debug("Username=" + lUserName);
        if (lUserName == null) {
            pErrMsg = "Authentification invalide.";
            return null;
        } else if (lUserName.equalsIgnoreCase("admin")) {
            pErrMsg = "Utilisateur invalide ou session expirée.";
            return null;
        } else {
            return lUserName;
        }
    }

    /**
     * Récupération des données de l'utilisateur authentifié par ACEGI
     *
     * @return username
     */
    private String getUsernameFromAuthentication() {
//	String username = null;
//	Authentication auth = (UsernamePasswordAuthenticationToken) SecureContextUtils.getSecureContext().getAuthentication();
//	if (logger.isDebugEnabled() && auth==null)
//	    logger.debug("  auth is NULL");
//	if (auth!=null && auth.getPrincipal() instanceof User)
//	{
//	    username = ((User) auth.getPrincipal()).getUsername();
//	}
        String username = getAuthenticationComponent().getCurrentUserName();

        return username;
    }

    /**
     * Retrouve le noeud Alfresco correspondant au idDossier passé en paramètre
     *
     * @param idDossierString
     * @return le nodeRef, ou null
     */
    private NodeRef checkForNodeRefFromDossierID(NodeRef bureauRef, String idDossierString) {
        if (parapheurService == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("parapheurService non initialisé...");
            }
            this.getParapheurService();
        }
        /**
         * la "plupart" du temps, le dossier recherché est dans le bureau en
         * cours!  Got luck?
         */
        NodeRef result = parapheurService.getDossierFromBureauAndName(bureauRef, idDossierString);
        if (result != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("I got it directly through XPATH: " + idDossierString);
            }
            return result;
        } else {
            //le dossier n'est pas sur le bureau, alors recherche plus large...
            return parapheurService.rechercheDossier(idDossierString);
        }
    }

    /**
     * Controle syntaxique sur le dossierID passé en paramètre.
     * On n'accepte pas n'importe quoi...
     *
     * @param nom chaine de caracteres candidate
     * @return true si syntaxe correcte, false sinon.
     */
    private boolean isNomDossierValide(String nom) {
        if (nom == null || nom.trim().isEmpty()) {
            logger.warn("nom de dossier null ou vide!");
            return false;
        }
        // Vérifier la syntaxe du dossierID ("\w([\w@\.\(\)\{\}\-_éèêàùçëïü]){0,200}") ?
        // longueur >1, <=200
        // caractères interdits Alfresco       : & " £  * / <  > ? %  | + ;
        return !(nom.length() < 2 || nom.length() > 200 || nom.contains(":")
                || nom.contains("&") || nom.contains("\"") || nom.contains("£")
                || nom.contains("*") || nom.contains("/") || nom.contains("<")
                || nom.contains(">") || nom.contains("?") || nom.contains("%")
                || nom.contains("|") || nom.contains("+") || nom.contains(";"));
    }

    /**
     * Factorisation rollback. A compter de desormais, le rollback est fait
     * systematiquement.
     * <br/><br/>
     * Motivation : dans une precedente version de ce code on avait
     * <pre>
     * if (txt.status == STATUS_ACTIVE) rollback else rien.
     * </pre>
     * Dans ce cas, lorsqu'une transacation était STATUS_MARKED_ROLLBACK, elle
     * n'etait pas rollack-ee. Situation dramatique pour toute tentative
     * future de quoi-que-ce-soit : la transaction obtenue au prochain
     * appel WS est la meme, marquee pour rollback et incommit-able.
     * <br/>
     * <br/>
     * On rollback donc a tous les coups, en esperant que meme si le rollback
     * echoue, la transaction obtenue le coup d'apres aura meilleurs mine.
     *
     * @param tx      la transaction a rollbacker
     * @param context indication sur le contexte d'appel de cette methode
     */
    private void doRollback(UserTransaction tx, String context) {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug(context + " rollback, tx.getStatus()=" + tx.getStatus());
            }
            tx.rollback();
        } catch (Exception e1) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error(context + " rollback KO : \"" + e1.getMessage() + "\", tx=" + tx, e1);
            }
        }
    }
}
