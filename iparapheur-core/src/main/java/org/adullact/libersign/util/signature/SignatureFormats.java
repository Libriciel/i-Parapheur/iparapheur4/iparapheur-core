/*
 * Version 3.6
 * CeCILL Copyright (c) 2014, ADULLACT-projet
 * Developpment by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.libersign.util.signature;

/**
 * Formats de signature supportés par le projet Libersign.
 * Ils sont conformes à l'API de liberSign
 * 
 * @author Stephane Vast - ADULLACT Projet
 */
public interface SignatureFormats {
    /** Le célèbre et universel format de signature détaché CMS/PKCS#7. */
    static final String CMS_PKCS7        = "CMS";
    /** <b>NE PAS UTILISER</b>.<br/> C'est un format pénible, qui necessite de l'horodatage... Bref c'est insupportable! */
    static final String CMS_PKCS7_Ain1   = "CMS-Allin1";

    /** PAdES simple, ISO32000-1, sans fioriture. */
    static final String PADES_ISO32000_1 = "PADES-basic";

    /** XAdES profil EPES enveloppé v1.1.1, pour signature de flux PESv2. */
    static final String XADES_EPES_ENV_PESv2  = "XADES-env";
    static final String XADES_EPES_ENV_PESv2_SHA256 = "xades-env-1.2.2-sha256";

    /** XAdES profil EPES enveloppé, spécifique aux flux notariés 'DIA'.  */
    static final String XADES_EPES_ENV_DIA    = "XADES-env-xpath";
    /** XAdES profil EPES détaché v1.1.1, non utilisé. Pas fiable non plus, car pas testé.  */
    static final String XADES_EPES_DET_1_1_1  = "XADES";
    /** XAdES profil EPES détaché v1.3.2, utilisé pour flux ANTS/ECD. */
    static final String XADES_EPES_DET_1_3_2  = "XADES132";
    /** <b>NON SUPPORTE</b>, Format XAdES avec timeStamp. N'existe pas. */
    static final String XADES_T_EPES_ENV_1_3_2  = "XADES-T-env";
    /** Signature brute sha256, compatible 'EBICS-TS' */
    static final String PKCS1_SHA256_RSA = "PKCS1_SHA256_RSA";
}
