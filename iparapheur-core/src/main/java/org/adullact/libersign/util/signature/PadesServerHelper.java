/*
 * Version 2.1
 * CeCILL Copyright (c) 2014, ADULLACT-projet
 * Developped by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.libersign.util.signature;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import org.adullact.iparapheur.util.PdfSealUtils;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Pades Helper.
 *
 * @author Paul Merlin
 * @author Stephane Vast - Adullact Projet
 */
public class PadesServerHelper {
    private static final int SIGNATURE_MAX_SIZE = 12 * 1024;
    static final int SIGNATURE_MAX_SIZE_HEX = SIGNATURE_MAX_SIZE * 2 + 2;
    private static final String TEMP_PLACEHOLDER = "-- iParapheur PDF Signer temporary placeholder -- 4A84CBFB 41289900 0320CDF3 2C38D3DF --";

    private PadesServerHelper() {
    }

    private static String getPlaceHolder() {
        return toHexString(getPlaceHolderBytes());
    }

    private static byte[] getPlaceHolderBytes() {
        byte[] placeHolderArray = new byte[SIGNATURE_MAX_SIZE];
        System.arraycopy(TEMP_PLACEHOLDER.getBytes(), 0, placeHolderArray, 0, TEMP_PLACEHOLDER.getBytes().length);
        return placeHolderArray;
    }

    private static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            int hexaValue = 0xFF & bytes[i];
            sb.append(String.format("%02x", hexaValue));
        }
        return sb.toString();
    }

    public static class DocumentHasher {
        private final String signatureReason;
        private final String signatureLocation;
        private final Calendar signatureDate;
        private final String layer2Text;
        private final String applicationCreator;
        private final InputStream input;
        private final OutputStream output;
        private final Image userSignature;
        private Rectangle position;
        private int fontSize;
        private int page;
        private boolean hasToCertified = false;


        public DocumentHasher(
                String signatureReason,
                String signatureLocation,
                Calendar signatureDate,
                String layer2Text,
                String applicationCreator,
                InputStream input,
                OutputStream output,
                Image userSignature
        ) {
            this.signatureReason = signatureReason;
            this.signatureLocation = signatureLocation;
            this.signatureDate = signatureDate;
            this.layer2Text = layer2Text;
            this.applicationCreator = applicationCreator;
            this.input = input;
            this.output = output;
            this.position = new Rectangle(10, 10, 50, 30);
            this.fontSize = 0;
            this.page = 1;
            this.userSignature = userSignature;
        }

        public void setCertified(boolean certified) {
            this.hasToCertified = certified;
        }

        public void setVisuel(int x, int y, int width, int height, int fontSize, int page) {
            this.position = new Rectangle(x, y, x + width, y + height);
            this.fontSize = fontSize;
            this.page = page;
        }

        public byte[] hashDocument()
                throws IOException, DocumentException, NoSuchAlgorithmException, NoSuchProviderException {
            PdfReader reader = new PdfReader(input);
            int numberPages = reader.getNumberOfPages();
            int realPage = page < 1 || page > numberPages ? numberPages : page;

            // Lors de la signature PAdES, on doit suivre 2 méthodes distinctes dans le cas où le document est déjà signé ou non
            // Si le document est déjà signé, on doit se positionner en "append mode" et créer manuellement le champ de formulaire pour la signature
            // Dans le cas ou le document n'est pas signé, on ne doit pas se mettre en append mode et juste préparer une signature classique

            // On récupère l'information, le document est-il signé ?
            List<String> sigs = reader.getAcroFields().getSignatureNames();
            boolean append = !sigs.isEmpty();

            PdfStamper stamper;
            PdfSignatureAppearance sap;

            // Si le document est déjà signé, on rajoute manuellement le "field" de la signature puis on se met en append mode
            if (append) {
                String field = PdfSealUtils.getNewSignatureField(reader);

                // On doit d'abord générer un champ de formulaire pour y placer la signature.
                // La fonction itext relative ne fonctionne pas on doit donc faire ça à la main
                File tempFile = PdfSealUtils.addSignatureField(reader, position, field, realPage);
                // le document "tempFile" est le nouveau document à signer

                // Ici, on crée l'annotation de signature sur le champ de formulaire créé précédement
                reader = new PdfReader(new FileInputStream(tempFile));
                stamper = PdfStamper.createSignature(reader, output, '\0', null, true);
                sap = stamper.getSignatureAppearance();
                sap.setVisibleSignature(field);
            } else {
                stamper = PdfStamper.createSignature(reader, output, '\0');
                sap = stamper.getSignatureAppearance();
                sap.setVisibleSignature(position, realPage, null);
            }

            sap.setReason(signatureReason);
            sap.setLocation(signatureLocation);
            sap.setSignDate(signatureDate);
            sap.setSignatureCreator(applicationCreator);
            sap.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
            // sap.setCertificationLevel( PdfSignatureAppearance.CERTIFIED_FORM_FILLING_AND_ANNOTATIONS );
            sap.setLayer2Font(new Font(Font.FontFamily.HELVETICA, fontSize));
            if (this.userSignature != null) {
                sap.setImage(this.userSignature);
                // -1 pour scale automatique dans le rectangle défini
                sap.setImageScale(-1);
            }
            sap.setLayer2Text(layer2Text);

            PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, PdfName.ADBE_PKCS7_DETACHED);

            dic.setReason(sap.getReason());
            dic.setLocation(sap.getLocation());
            // dic.setContact( sap.getContact() );
            dic.setDate(new PdfDate(sap.getSignDate()));
            dic.setSignatureCreator(sap.getSignatureCreator());

            // ATTENTION !!! Ça c'est pour certifier le document... Donc interdit de certifier 2 fois ! (PADES/baseCertifie)
            // ATTENTION BIS !!! La certification est interdite sur un document déjà signé
            // Si le document est déjà signé, on signe sans certifier pour ne pas bloquer la signature du document
            if (this.hasToCertified && sigs.isEmpty()) {
                PdfDictionary transformParams = new PdfDictionary();
                // v4.3: positionné à la valeur 1 pour bloquer les autres signatures
                transformParams.put(PdfName.P, new PdfNumber(1));
                transformParams.put(PdfName.V, new PdfName("1.2"));
                transformParams.put(PdfName.TYPE, PdfName.TRANSFORMPARAMS);
                PdfDictionary reference = new PdfDictionary();
                reference.put(PdfName.TRANSFORMMETHOD, PdfName.DOCMDP);
                reference.put(PdfName.TYPE, new PdfName("SigRef"));
                reference.put(PdfName.TRANSFORMPARAMS, transformParams);
                PdfArray types = new PdfArray();
                types.add(reference);
                dic.put(PdfName.REFERENCE, types);
            }

            sap.setCryptoDictionary(dic);

            HashMap exc = new HashMap();
            exc.put(PdfName.CONTENTS, SIGNATURE_MAX_SIZE_HEX);
            sap.preClose(exc);

            InputStream data = sap.getRangeStream();
            final MessageDigest messageDigest = MessageDigest.getInstance("SHA1", "BC");

            byte buf[] = new byte[8192];
            int n;
            while ((n = data.read(buf)) > 0) {
                messageDigest.update(buf, 0, n);
            }
            byte[] hash = messageDigest.digest();

            PdfDictionary update = new PdfDictionary();
            byte[] placeholderSignature = getPlaceHolderBytes();
            update.put(PdfName.CONTENTS, new PdfString(placeholderSignature).setHexWriting(true));
            sap.close(update);
            return hash;
        }
    }

    public static class SignatureInjector {
        private final byte[] signature;
        private final File file;

        public SignatureInjector(byte[] signature, File file) {
            this.signature = signature;
            this.file = file;
        }

        public void injectSignature()
                throws IOException {
            String encodedSigHex = toHexString(signature);

            byte[] placeHolderBytes = getPlaceHolder().getBytes();

            byte[] paddedSignature = new byte[placeHolderBytes.length];
            // fill with harmless data
            for (int i = 0; i < paddedSignature.length; i++) {
                paddedSignature[i] = 0x30;
            }
            // Prepend actual signature
            System.arraycopy(encodedSigHex.getBytes(), 0, paddedSignature, 0, encodedSigHex.getBytes().length);
            // Patch!
            FilePatch.replace(file.getPath(), placeHolderBytes, paddedSignature);
        }
    }
}
