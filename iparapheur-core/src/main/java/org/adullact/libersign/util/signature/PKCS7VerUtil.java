/**
 *  CeCILL Copyright (c) 2008-2012, ADULLACT-Projet, Netheos
 *  CeCILL Copyright (c) 2012-2014, ADULLACT-Projet
 *  Initiated by ADULLACT-Projet S.A.
 *  Developped by Netheos, ADULLACT-Projet
 *
 *  contact@netheos.net
 *  contact@adullact-projet.coop
 *
 *  Ce logiciel est un programme informatique servant à produire et verifier
 *  des signatures numeriques.
 *
 *  Ce logiciel est régi par la licence CeCILL soumise au droit français et
 *  respectant les principes de diffusion des logiciels libres. Vous pouvez
 *  utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *  de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 *  sur le site "http://www.cecill.info".
 *
 *  En contrepartie de l'accessibilité au code source et des droits de copie,
 *  de modification et de redistribution accordés par cette licence, il n'est
 *  offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 *  seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 *  titulaire des droits patrimoniaux et les concédants successifs.
 *
 *  A cet égard  l'attention de l'utilisateur est attirée sur les risques
 *  associés au chargement,  à l'utilisation,  à la modification et/ou au
 *  développement et à la reproduction du logiciel par l'utilisateur étant
 *  donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 *  manipuler et qui le réserve donc à des développeurs et des professionnels
 *  avertis possédant  des  connaissances  informatiques approfondies.  Les
 *  utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 *  logiciel à leurs besoins dans des conditions permettant d'assurer la
 *  sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 *  à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 *  Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 *  pris connaissance de la licence CeCILL, et que vous en avez accepté les
 *  termes.
 */

package org.adullact.libersign.util.signature;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.Store;
import org.springframework.extensions.surf.util.Base64;

/**
 * Read and Verify CMS/PKCS#7 objects
 *
 * @author Augustin Sarr - Netheos
 * @author Stephane Vast - Adullact Projet
 */
public class PKCS7VerUtil {

    private static Logger logger = Logger.getLogger(PKCS7VerUtil.class);

    public PKCS7VerUtil() {
    }

    /**
     * Validate a p7 contained following the "whole or nothing rule".
     * 
     * @param data the data upon which the signature is sgoing to be verified
     * @param p7signature p7 signatures container
     * @return true if all signatures are valid, false otherwise.
     */
    public static boolean verify(byte[] data, byte[] p7signature) {
        int[] ret = verifyAndCount(data, p7signature);
        return (ret[0] == ret[1]);
    }

    /**
     * Verify the signatures and return the number of valid signatures
     * 
     * @param data to upon which the signatures are supposed generated
     * @param p7signature pkcs7 signatures container
     * @return an array of two integers, the first integer indicates the number
     *         of valid signatures and the second, the number of signatures
     *         (valid and invalid ones).
     *         It returns null if the signature format is invalid
     */
    public static int[] verifyAndCount(byte[] data, byte[] p7signature) {
        return verifyAndCount(new CMSProcessableByteArray(data), p7signature);
    }

    /**
     * Verify the signatures and return the number of valid signatures
     * 
     * @param file  upon which the signatures are supposed generated
     * @param p7signature pkcs7 signatures container
     * @return an array of two integers, the first integer indicates the number
     *         of valid signatures and the second, the number of signatures
     *         (valid and invalid ones).
     *         It returns null if the signature format is invalid
     */
    public static int[] verifyAndCount(File file, byte[] p7signature) {
        return verifyAndCount(new CMSProcessableFile(file), p7signature);
    }

    /**
     * Validate a p7 contained following the "whole or nothing rule".
     * 
     * @param file the file upon which the signature is sgoing to be verified
     * @param p7signature p7 signatures container
     * @return true if all signatures are valid, false otherwise.
     */
    public static boolean verify(File file, byte[] p7signature) {
        int[] ret = verifyAndCount(file, p7signature);
        return (ret[0] == ret[1]);
    }

    public static X509Certificate getSignatureCertificate(CMSProcessable data, byte[] p7signature) {
        try {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            org.bouncycastle.cms.CMSSignedData signed = new CMSSignedData(data, p7signature);

            org.bouncycastle.util.Store certStore = signed.getCertificates();   // deprecated  CertStore certs = signed.getCertificatesAndCRLs("Collection", "BC");
            SignerInformationStore signers = signed.getSignerInfos();
            Collection coll = signers.getSigners();
            Iterator it = coll.iterator();
            SignerInformation signer;            //X509Certificate cert;

            while (it.hasNext()) {
                signer = (SignerInformation) it.next();
                Collection certColl = certStore.getMatches(signer.getSID()); // certs.getCertificates(signer.getSID());

                java.util.Iterator certIt = certColl.iterator();
                while (certIt.hasNext()) {
                    org.bouncycastle.cert.X509CertificateHolder certificateHolder = (org.bouncycastle.cert.X509CertificateHolder)certIt.next();
                    return new JcaX509CertificateConverter().setProvider( "BC" ).getCertificate( certificateHolder );
                      //certIt = ((org.bouncycastle.cert.X509CertificateHolder)certIt.next()).getSubject();
                    //cert = (X509Certificate) certIt.next();
                    //return cert;
                }
            }
        } catch (Exception ex) {
            logger.fatal(null, ex);
        }
        return null;
    }

    public static X509CertificateHolder getSignatureCertificateHolder(CMSProcessable data, byte[] p7signature) {
        try {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            org.bouncycastle.cms.CMSSignedData signed = new CMSSignedData(data, p7signature);

            org.bouncycastle.util.Store certStore = signed.getCertificates();
            SignerInformationStore signers = signed.getSignerInfos();
            Collection coll = signers.getSigners();
            Iterator it = coll.iterator();
            SignerInformation signer;

            while (it.hasNext()) {
                signer = (SignerInformation) it.next();
                Collection certColl = certStore.getMatches(signer.getSID());

                java.util.Iterator certIt = certColl.iterator();
                while (certIt.hasNext()) {
                    return  (X509CertificateHolder)certIt.next();
                }
            }
        } catch (Exception ex) {
            logger.fatal(null, ex);
        }
        return null;
    }

    private static int[] verifyAndCount(CMSProcessable data, byte[] p7signature) {
        int[] cnt = {0, 0};
        try {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            org.bouncycastle.cms.CMSSignedData signed = new CMSSignedData(data, p7signature);

            // CertStore certs = signed.getCertificatesAndCRLs("Collection", "BC");
            Store certs = signed.getCertificates();
            SignerInformationStore signers = signed.getSignerInfos();
            cnt[1] = signers.size();
            
            Collection coll = signers.getSigners();
            Iterator it = coll.iterator();
            SignerInformation signer;
            X509Certificate cert;

            while (it.hasNext()) {
                signer = (SignerInformation) it.next();
                
                Collection certColl = certs.getMatches(signer.getSID());
                java.util.Iterator certIt = certColl.iterator();
                while (certIt.hasNext()) {
                    cert = (X509Certificate) certIt.next();
                    try {
                        // TODO : validate certificate
                        // if (signer.verify(cert.getPublicKey(), "BC")) {
                        if (signer.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(cert))) {
                            if (logger.isDebugEnabled()) {
                                logger.info("== validation sucess");
                            }
                            ++(cnt[0]);
                        } else {
                            if (logger.isInfoEnabled()) {
                                logger.info("signature verification failed");
                            }
                        }
                    } catch (CMSException ex) {
                        logger.error(null, ex);
                        continue;
                    } catch (OperatorCreationException ex) {
                        logger.error(null, ex);
                    }
                }
            }
            return cnt;
        } catch (CMSException ex) {
            logger.error(null, ex);
            return null;
//        } catch (CertStoreException ex) {
//            logger.error(null, ex);
//            return null;
//        } catch (NoSuchAlgorithmException ex) {
//            logger.error(null, ex);
//            return null;
//        } catch (NoSuchProviderException ex) {
//            logger.error(null, ex);
//            return null;
        }
    }

    public static int indexOf(byte[] mainArray, byte[] searchSequence) {
        return indexOf(mainArray, searchSequence, 0);
    }

    public static int indexOf(byte[] mainArray, byte[] searchSequence, int fromIndex) {
        byte v1[] = mainArray;
        byte v2[] = searchSequence;
        int max = mainArray.length;

        if (fromIndex >= max) {
            if (mainArray.length == 0 && fromIndex == 0 && searchSequence.length == 0) {
                return 0;
            }
            return -1;  // from index too large
        }

        if (fromIndex < 0) {
            fromIndex = 0;
        }
        if (searchSequence.length == 0) {
            return fromIndex;
        }

        byte first = v2[0];
        int i = fromIndex;

        startSearchForFirstChar:
        while (true) {
            /* Look for first character. */
            while (i < max && v1[i] != first) {
                i++;
            }

            if (i >= max) // didn't find the sequence: return -1
            {
                return -1;
            }

            /* Found first character, now look at the rest of v2 */
            int j = i + 1;
            int end = j + searchSequence.length - 1;
            int k = 1;
            while (j < end) {
                if (v1[j++] != v2[k++]) {
                    i++;
                    /* Look for str's first char again. */
                    continue startSearchForFirstChar;
                }
            }
            return i;    // Found whole string!!!
        }
    }

    public static int indexOf(byte[] mainArray, byte searchByte, int fromIndex) {
        int len = mainArray.length;

        if (fromIndex < 0) {
            fromIndex = 0;
        } else if (fromIndex >= len) {
            return -1;
        }

        for (int i = fromIndex; i < len; i++) {
            if (mainArray[i] == searchByte) {
                return i;
            }
        }

        return -1;                     // did not find anything...
    }

    public static byte[] pem2der(byte[] pem, byte[] header, byte[] footer) {
        int start, end;
        start = indexOf(pem, header);
        end = indexOf(pem, footer);
        if (start == -1 || end == -1) {
            return null;
        }  //  no headers!
        start = indexOf(pem, (byte) '\n', start) + 1;

        // skip past any more text, by avoiding all lines less than 64 characters long...
        int next;
        while ((next = indexOf(pem, (byte) '\n', start)) < start + 60) {
            if (next == -1) // really shouldn't ever happen...
            {
                break;
            }
            start = next + 1;
        }

        if (start == -1) // something wrong - no end of line after '-----BEGIN...'
        {
            return null;
        }
        int len = end - start;
        byte[] data = new byte[len];

        System.arraycopy(pem, start, data, 0, len); // remove the PEM fluff from tbe base 64 data, stick in 'data'
        return Base64.decode(data);
    }

    public static byte[] der2pem(byte[] der) {
        String start = "-----BEGIN PKCS7-----\n";
        String end = "\n-----END PKCS7-----\n";
        ByteArrayOutputStream out = new ByteArrayOutputStream(der.length +
                start.length() + end.length());

        try {
            out.write(start.getBytes());
            out.write(Base64.encodeBytes(der).getBytes());
            out.write(end.getBytes());
        } catch (IOException ex) {
        }

        return out.toByteArray();
    }

    public static byte[] decode(String s) {
        return decode(s.toCharArray());
    }

    public static byte[] decode(char[] in) {
        int iLen = in.length;
        if (iLen % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (iLen > 0 && in[iLen - 1] == '=') {
            iLen--;
        }
        int oLen = (iLen * 3) / 4;
        byte[] out = new byte[oLen];
        int ip = 0;
        int op = 0;
        while (ip < iLen) {
            int i0 = in[ip++];
            int i1 = in[ip++];
            int i2 = ip < iLen ? in[ip++] : 'A';
            int i3 = ip < iLen ? in[ip++] : 'A';
            if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int b0 = map2[i0];
            int b1 = map2[i1];
            int b2 = map2[i2];
            int b3 = map2[i3];
            if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int o0 = (b0 << 2) | (b1 >>> 4);
            int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
            int o2 = ((b2 & 3) << 6) | b3;
            out[op++] = (byte) o0;
            if (op < oLen) {
                out[op++] = (byte) o1;
            }
            if (op < oLen) {
                out[op++] = (byte) o2;
            }
        }
        return out;
    }

    private static char[] map1 = new char[64];
    static {
        int i = 0;
        for (char c = 'A'; c <= 'Z'; c++) {
            map1[i++] = c;
        }
        for (char c = 'a'; c <= 'z'; c++) {
            map1[i++] = c;
        }
        for (char c = '0'; c <= '9'; c++) {
            map1[i++] = c;
        }
        map1[i++] = '+';
        map1[i++] = '/';
    }

    private static byte[] map2 = new byte[128];
    static {
        for (int i = 0; i < 128; i++) {
            map2[i] = -1;
        }
        for (int i = 0; i < 64; i++) {
            map2[map1[i]] = (byte) i;
        }
    }

}
