/*
 * Version 2.1
 * CeCILL Copyright (c) 2014, ADULLACT-projet
 * Developped by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.libersign.util.signature;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * FilePatch.
 * 
 * @author Paul Merlin
 * @author Stephane Vast - Adullact Projet
 */
class FilePatch
{
    private static boolean matches( MappedByteBuffer bb, byte[] sought, int pos )
    {
        for( int j = 0; j < sought.length; ++j )
        {
            if( sought[j] != bb.get( pos + j ) )
            {
                return false;
            }
        }
        return true;
    }

    private static void replace( MappedByteBuffer bb, byte[] sought, byte[] replacement, int pos )
    {
        for( int j = 0; j < sought.length; ++j )
        {
            byte b = ( j < replacement.length ) ? replacement[j] : (byte) ' ';
            bb.put( pos + j, b );
        }
    }

    private static void searchAndReplace( MappedByteBuffer bb, byte[] sought, byte[] replacement, int sz )
    {
        int replacementsCount = 0;
        for( int pos = 0; pos <= sz - sought.length; ++pos )
        {
            if( matches( bb, sought, pos ) )
            {
                replace( bb, sought, replacement, pos );
                pos += sought.length - 1;
                ++replacementsCount;
            }
        }
        System.out.println( "" + replacementsCount + " replacements done." );
    }

    // Search for occurrences of the input pattern in the given file
    private static void patch( File f, byte[] sought, byte[] replacement )
        throws
        IOException
    {

        // Open the file and then get a channel from the stream
        RandomAccessFile raf = new RandomAccessFile( f, "rw" ); // "rws", "rwd"

        try
        {
            FileChannel fc = raf.getChannel();

            // Get the file's size and then map it into memory
            int sz = (int) fc.size();
            MappedByteBuffer bb = fc.map( FileChannel.MapMode.READ_WRITE, 0, sz );

            searchAndReplace( bb, sought, replacement, sz );

            bb.force(); // Write back to file, like "flush()"

        }
        finally
        {
            // Close the channel and the stream
            raf.close();
        }
    }

    /* package */ static void replace( String filename, byte[] sought, byte[] replacement )
        throws IOException
    {
        if( sought.length != replacement.length )
        {
            // Better build-in some support for padding with blanks.
            throw new RuntimeException( "sought length must match replacement length" );
        }

        System.out.println( "looking for " + sought.length + " bytes in '" + filename + "'" );

        File f = new File( filename );
        patch( f, sought, replacement );
    }
}
