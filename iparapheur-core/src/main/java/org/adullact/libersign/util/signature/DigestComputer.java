/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, Netheos, ADULLACT-projet
 * CeCILL Copyright (c) 2008-2014, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by Netheos
 * 
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package org.adullact.libersign.util.signature;

import nu.xom.*;
import org.apache.log4j.Logger;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe de calcul de Condensat (Hash) sur du flux XML.
 * Utilisé dans le cadre de signature XAdES, en conjonction avec Libersign
 *
 * @author A.Sarr (Netheos)
 * @author Stephane Vast (Adullact Projet)
 */
public class DigestComputer {

    private static Logger logger = Logger.getLogger(DigestComputer.class);

    public DigestComputer() {
    }

    public static PesDigest computeDigest(java.io.InputStream inputStream) {
        try {
            java.io.ByteArrayOutputStream baStream = new java.io.ByteArrayOutputStream();
            nu.xom.Builder builder = new nu.xom.Builder();
            nu.xom.canonical.Canonicalizer canonicalizer = new nu.xom.canonical.Canonicalizer(baStream,
                                                                                              nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);
            nu.xom.Document document = builder.build(inputStream);
            // document.query("//")
            PesDigest res = new PesDigest();
            res.setId(document.getRootElement().getAttributeValue("Id"));
            canonicalizer.write(document);
            byte[] canonicalData = baStream.toByteArray();
            java.security.MessageDigest messageDigest = java.security.MessageDigest.getInstance("sha1");
            messageDigest.update(canonicalData);

            res.setDigest(messageDigest.digest());
            return res;

        } catch (NoSuchAlgorithmException ex) {
            logger.error("NoSuchAlgorithmException" + ex.getMessage(), ex);
        } catch (ParsingException ex) {
            logger.error("ParsingException" + ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.error("IOException" + ex.getMessage(), ex);
        }
        return null;
    }

    /**
     * calcul de condensat orienté pour la signature XAdES de flux PESv2.
     * L'operation est en plusieurs phases:
     * - localisation du bloc ou des blocs
     * - pour chaque bloc: canonicalisation, puis hashage (SHA1 only)
     *
     * @param inputStream
     * @param XPath
     * @return
     */
    public static PesDigest computeDigest(java.io.InputStream inputStream, String XPath) {
        try {
            java.io.ByteArrayOutputStream baStream = new java.io.ByteArrayOutputStream();
            nu.xom.Builder builder = new nu.xom.Builder();
            nu.xom.canonical.Canonicalizer canonicalizer = new nu.xom.canonical.Canonicalizer(baStream,
                                                                                              nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);
            nu.xom.Document document = builder.build(inputStream);
            nu.xom.Element root = document.getRootElement();
            // Aller au bon endroit d'arborescence
            // document.query("//")
            // Ejecter les blocs signatures
            if (root == null) {
                return null;
            }
            PesDigest res = new PesDigest();

            if (XPath.trim().equals(".")) {
                res.setId(document.getRootElement().getAttributeValue("Id"));
                canonicalizer.write(document);

            } else { // utilisé pour la signature de bordereaux de PESv2
                // NamespaceContext nsContext = new XadesNSContext();
                String prefixe = root.getNamespacePrefix();
                String uriString = root.getNamespaceURI(prefixe);
                nu.xom.XPathContext xPathContext = new nu.xom.XPathContext(prefixe, uriString);
                // cas d'étude : "/PES_Aller/PES_RecetteAller/Bordereau", /p:PES_Aller//Bordereau
                nu.xom.Nodes nodeListe = document.query(XPath.trim(), xPathContext);
                int nombreNodes = nodeListe.size();
                if (logger.isDebugEnabled()) {
                    logger.debug("\tRoot Id=" + root.getAttributeValue("Id")
                                 + "\n\tNameSpace Prefix = '" + prefixe + "'"
                                 + "\n\tNameSpace URI    = '" + uriString + "'"
                                 + "\n\tNombre trouvés   = " + nombreNodes);
                }
                if (nombreNodes == 0) { // ça craint
                    logger.error("No Node found with Xpath=" + XPath);
                    return null;
                }
                /**
                 * Attention ici on ne s'occupe que du 1er element. Tant pis pour les autres...
                 */
                nu.xom.Element elt = (nu.xom.Element) nodeListe.get(0);
                res.setId(elt.getAttributeValue("Id"));
                canonicalizer.write(elt);
            }

            byte[] canonicalData = baStream.toByteArray();
            java.security.MessageDigest messageDigest = java.security.MessageDigest.getInstance("sha1");
            messageDigest.update(canonicalData);

            res.setDigest(messageDigest.digest());
            return res;

        } catch (NoSuchAlgorithmException ex) {
            logger.error("NoSuchAlgorithmException" + ex.getMessage(), ex);
        } catch (ParsingException ex) {
            logger.error("ParsingException" + ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.error("IOException" + ex.getMessage(), ex);
        }
        return null;
    }

    /**
     * calcul de condensat orienté pour la signature XAdES de flux PESv2.
     * L'operation est en plusieurs phases:
     * - localisation du ou des bloc(s)
     * - pour chaque bloc: canonicalisation, puis hashage (SHA1 only)
     *
     * @param inputStream
     * @param xPath
     * @return liste des condensats
     */
    public static List<PesDigest> computeDigests(java.io.InputStream inputStream, String xPath) {
        try {
            ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
            //ObjectOutputStream outputstream = new ObjectOutputStream(bytestream);

            nu.xom.Builder builder = new nu.xom.Builder();
            nu.xom.canonical.Canonicalizer outputter = new nu.xom.canonical.Canonicalizer(bytestream,
                                                                                          nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);

            nu.xom.Document document = builder.build(inputStream);
            nu.xom.Element root = document.getRootElement();

            // Ejecter les blocs signatures ??
            if (root == null) {
                return null;
            }
            List<PesDigest> res;//  = new ArrayList<PesDigest>();

            if (xPath.trim().equals(".")) {
                /*
                 * cas particulier signature globale, qui est en réalité le cas général
                 */
                res = new ArrayList<PesDigest>(1);
                res.add(new PesDigest());
                res.get(0).setId(document.getRootElement().getAttributeValue("Id"));

                outputter.write(document);

                java.security.MessageDigest messageDigest = java.security.MessageDigest.getInstance("sha256");
                messageDigest.update(java.nio.ByteBuffer.wrap(bytestream.toByteArray()));
                res.get(0).setDigest(messageDigest.digest());

            } else {
                // utilisé pour la signature PESv2 au niveau bordereau.

                // NamespaceContext nsContext = new XadesNSContext();
                String prefixe = root.getNamespacePrefix();
                String uriString = root.getNamespaceURI(prefixe);
                //nu.xom.XPathContext xPathContext = new nu.xom.XPathContext(prefixe,uriString);
                // cas d'étude : "/PES_Aller/PES_RecetteAller/Bordereau", /p:PES_Aller//Bordereau
                // nouveau cas: "//Bordereau" ?
                nu.xom.Nodes nodeListe = document.query(xPath.trim());

                //nu.xom.Nodes nodeListe = document.query(XPath.trim(), xPathContext);
                int nombreNodes = nodeListe.size();

                logger.debug("\tRoot Id=" + root.getAttributeValue("Id")
                             + "\n\tNameSpace Prefix = '" + prefixe + "'"
                             + "\n\tNameSpace URI    = '" + uriString + "'"
                             + "\n\tNombre trouvés   = " + nombreNodes);

                nu.xom.Element elt;
                java.security.MessageDigest messageDigest = java.security.MessageDigest.getInstance("sha256");

                switch (nombreNodes) {
                    case 0: // ça craint
                        logger.error("## No Node found with Xpath=\"" + xPath + "\"");
                        return null;

                    case 1: // cas mono-bordereau. Je le laisse parce que "code-historique-qui-marche"
                        /**
                         * Attention ici on ne s'occupe que du 1er element. Tant pis pour les autres, mais y en a pas...
                         */
                        res = new ArrayList<PesDigest>(1);
                        res.add(new PesDigest());
                        elt = (nu.xom.Element) nodeListe.get(0);
                        res.get(0).setId(elt.getAttributeValue("Id"));

                        outputter.write(elt);

                        messageDigest.update(java.nio.ByteBuffer.wrap(bytestream.toByteArray()));

                        byte[] digest = messageDigest.digest();
                        res.get(0).setDigest(digest);
                        break;

                    default: // plein de resultats

                        res = new ArrayList<PesDigest>(nombreNodes);
                        logger.debug(" on a créé " + res.size() + "/" + nombreNodes + " objets PesDigest");

                        for (int i = 0; i < nombreNodes; i++) {
                            res.add(new PesDigest());
                            nu.xom.Element element = (nu.xom.Element) nodeListe.get(i);
                            res.get(i).setId(element.getAttributeValue("Id"));

                            bytestream.reset();
                            outputter.write(element.copy());

                            messageDigest.reset();
                            messageDigest.update(java.nio.ByteBuffer.wrap(bytestream.toByteArray()));

                            res.get(i).setDigest(messageDigest.digest());
                            logger.debug("  numero " + (i + 1) + " : Id=\"" + res.get(i).getId() + "\"");
                        }
                }
            }
            return res;
        } catch (NoSuchAlgorithmException ex) {
            logger.error("NoSuchAlgorithmException" + ex.getMessage(), ex);
        } catch (ParsingException ex) {
            logger.error("ParsingException" + ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.error("IOException" + ex.getMessage(), ex);
        }
        return null;
    }

}
