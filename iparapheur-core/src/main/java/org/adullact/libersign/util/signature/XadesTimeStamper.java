/*
 * Version 3.2
 * CeCILL Copyright (c) 2010-2011, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.libersign.util.signature;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampRequestGenerator;
import org.bouncycastle.tsp.TSPAlgorithms;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.asn1.cmp.PKIStatus;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.tsp.TSPValidationException;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.util.Store;
import org.xml.sax.SAXException;

/**
 * Stamps already signed documents. Notice that the stamping can occur late
 * after the document is signed, so the timestamping code is separated from
 * the signing code
 *
 * @authors Augustin Sarr (NETHEOS),  Stephane Vast (ADULLACT Projet)
 */
public class XadesTimeStamper {

    static final String DEFAULT_DIGEST = "SHA1";

    static final int DEFAULT_NONCE_LENGTH = 20;

    static final Logger logger = Logger.getLogger(XadesTimeStamper.class.getName());

    public static byte[] getSignatureValueFromXML(InputStream signedDocumentStream) throws XMLSignatureException {
        return null;
    }

    public static byte[] computeSignatureDigestFromXML(byte[] signedBytes, String sigId) throws XMLSignatureException {
        if (signedBytes == null || sigId == null) {
            throw new InvalidParameterException("Null argument !");
        }
        try {
            /**
             * set up the existing signature list
             */
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new ByteArrayInputStream(signedBytes));
            NodeList signatureList = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
            int listLength = signatureList.getLength();
            if (listLength == 0) {
                throw new XMLSignatureException("The document is not signed");
            }
            /**
             * now search the good one
             */
            for (int i = 0; i < listLength; i++) {
                Element curSigElement = (Element) signatureList.item(i);
                if (curSigElement.hasAttribute("Id") && sigId.equals(curSigElement.getAttribute("Id"))) {
                    /**
                     * Signature match !
                     */
                    XMLSignatureFactory sigFactory = XMLSignatureFactory.getInstance("DOM");
                    XMLSignature sig = sigFactory.unmarshalXMLSignature(new DOMStructure(curSigElement));
                    /**
                     * compute Hash
                     */
                    MessageDigest messageDigest = MessageDigest.getInstance(DEFAULT_DIGEST);
                    messageDigest.reset();
                    byte[] digest = messageDigest.digest(sig.getSignatureValue().getValue());
                    return digest;
                }
            }
        } catch (Exception ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        }
        // bad luck, no match
        return null;
    }

    public static byte[] injectTokenForSignature(byte[] signedBytes, String sigId, byte[] token) throws XMLSignatureException {
        if (signedBytes == null || sigId == null || token == null) {
            throw new InvalidParameterException("Null argument !");
        }
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Transformer transformer = TransformerFactory.newInstance().newTransformer();

            /**
             * set up the existing signature list
             */
            Document document = builder.parse(new ByteArrayInputStream(signedBytes));
            NodeList signatureList = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
            int listLength = signatureList.getLength();
            if (listLength == 0)
                throw new XMLSignatureException("The document is not signed");
            /**
             * now search the good one
             */
            for (int i = 0; i < listLength; i++) {
                Element curSigElement = (Element) signatureList.item(i);
                if (curSigElement.hasAttribute("Id") && sigId.equals(curSigElement.getAttribute("Id"))) {
                    /**
                     * Signature found, now add the token
                     */
                    Element objectElement = getChildElement(curSigElement, "ds:Object");
                    if (objectElement == null) {
                        throw new XMLSignatureException("Invalid signature!");
                    }

                    Element qualifyingPropElement = getChildElement(objectElement, "xad:QualifyingProperties");
                    if (qualifyingPropElement == null) {
                        throw new XMLSignatureException("Invalid signature!");
                    }
                    Element unsignedPropElement = getChildElement(qualifyingPropElement, "xad:UnsignedProperties");
                    if (unsignedPropElement == null)
                    {
                        unsignedPropElement = createXades122Element(document, Xades.xadesNS122, "UnsignedProperties");
                        qualifyingPropElement.appendChild(unsignedPropElement);
                    }
                    Element unsignedSignPropElement = getChildElement(unsignedPropElement, "xad:UnsignedSignatureProperties");
                    if (unsignedSignPropElement == null)
                    {
                        unsignedSignPropElement = createXades122Element(document, Xades.xadesNS122, "UnsignedSignatureProperties");
                        unsignedPropElement.appendChild(unsignedSignPropElement);
                    }
                    /**
                     * WARNING: this complies with TimeStamp Element as described
                     *  for XAdES 1.2.2
                     */
                        //	<xsd:element name="TimeStamp" type="TimeStampType"/>
                        //	<xsd:complexType name="TimeStampType">
                        //		<xsd:sequence>
                        //			<xsd:element name="Include" type="IncludeType" maxOccurs="unbounded"/>
                        //			<xsd:element ref="ds:CanonicalizationMethod" minOccurs="0"/>
                        //			<xsd:choice>
                        //				<xsd:element name="EncapsulatedTimeStamp" type="EncapsulatedPKIDataType"/>
                        //				<xsd:element name="XMLTimeStamp" type="AnyType"/>
                        //			</xsd:choice>
                        //		</xsd:sequence>
                        //		<xsd:attribute name="Id" type="xsd:ID" use="optional"/>
                        //	</xsd:complexType>
                        //
                        //	<xsd:complexType name="IncludeType">
                        //		<xsd:attribute name="URI" type="xsd:anyURI" use="required"/>
                        //		<xsd:attribute name="referencedData" type="xsd:boolean" use="optional"/>
                        //	</xsd:complexType>

                    Element sigTsElement = createXades122Element(document, Xades.xadesNS122, "SignatureTimeStamp");
                    unsignedSignPropElement.appendChild(sigTsElement);

//                    Element hashDataInfoElmt = createXades122Element(document, xades.xadesNS122, "HashDataInfo");
//                    hashDataInfoElmt.setAttribute("uri", "#" + sigId);
//                    sigTsElement.appendChild(hashDataInfoElmt);
//                    // was: Element transforms = createXades122Element(document, xades.xadesNS122, "Transforms");
//                    Element transforms = document.createElement("ds:Transforms");
//                    hashDataInfoElmt.appendChild(transforms);
//                    Element transformsElmt = document.createElement("ds:Transform");
//                    transformsElmt.setAttribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#rsa-sha1");
//                    transforms.appendChild(transformsElmt);
                    /**
                     * build the "Include" tag
                     */
                    Element includeElmt = createXades122Element(document, Xades.xadesNS122, "Include");
                    includeElmt.setAttribute("URI", "#" + sigId);
                    sigTsElement.appendChild(includeElmt);

                    /**
                     * Build the "ds:CanonicalizationMethod" tag?  Unecessary
                     */
                    //Element canonMethodeElmt = document.createElement("ds:CanonicalizationMethod");

                    Element stampElmt = createXades122Element(document, Xades.xadesNS122, "EncapsulatedTimeStamp");
                    stampElmt.setAttribute("Id", sigId + "_TS");
                    Text stString = document.createTextNode(new String(Base64.encode(token), "UTF8"));
                    stampElmt.appendChild(stString);
                    sigTsElement.appendChild(stampElmt);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    transformer.transform(new DOMSource(document), new StreamResult(byteArrayOutputStream));
                    return byteArrayOutputStream.toByteArray();

                }
            }
        } catch (Exception ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        }
        return null;
    }

    /**
     * produces timestamps for XAdES 1.1.1
     *
     * @param signedDocumentStream
     * @param serverURLStr
     * @return
     * @throws XMLSignatureException
     */
    public static byte[] stampSignatures(InputStream signedDocumentStream, String serverURLStr) throws XMLSignatureException
    {
        if (signedDocumentStream == null || serverURLStr == null) {
            throw new InvalidParameterException("Null argument!");
        }
        try
        {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss");
            byte[] nonceBuffer = new byte[DEFAULT_NONCE_LENGTH];
            Map<BigInteger, TimeStampRequest> requests = new HashMap<BigInteger, TimeStampRequest>();
            Map<BigInteger, Element> signatures = new HashMap<BigInteger, Element>();
            Map<BigInteger, byte[]> digests = new HashMap<BigInteger, byte[]>();
            BigInteger nonce;

            SecureRandom random = new SecureRandom();
            random.setSeed(dateFormat.format(new Date()).getBytes());

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(signedDocumentStream);
            NodeList signatureList = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
            int listLength = signatureList.getLength();
            if (listLength == 0) {
                throw new XMLSignatureException("The document is not signed");
            }
            for (int i = 0; i < listLength; i++)
            {
                random.nextBytes(nonceBuffer);
                nonce = new BigInteger(nonceBuffer);
                signatures.put(nonce, (Element) signatureList.item(i));
            }

            Iterator<BigInteger> iter = signatures.keySet().iterator();
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            MessageDigest messageDigest = MessageDigest.getInstance(DEFAULT_DIGEST);
            TimeStampRequestGenerator requestGenerator = new TimeStampRequestGenerator();
            byte[] digest = null;

            XMLSignatureFactory sigFactory = XMLSignatureFactory.getInstance("DOM");
            XMLSignature sig = null;
            while (iter.hasNext())
            {
                nonce = iter.next();
                sig = sigFactory.unmarshalXMLSignature(new DOMStructure(signatures.get(nonce)));
                messageDigest.reset();
                digest = messageDigest.digest(sig.getSignatureValue().getValue());
                digests.put(nonce, digest);
                requests.put(nonce, requestGenerator.generate(TSPAlgorithms.SHA1, digest, nonce));
            }

            // TODO : rewrite this loop to be more efficient
            iter = requests.keySet().iterator();
            byte[] stamp = null;
            Element signature = null;
            while (iter.hasNext())
            {
                nonce = iter.next();
                stamp = queryStamp(requests.get(nonce), serverURLStr);
                if (stamp == null) {
                    throw new XMLSignatureException("Stamp generation failed!");
                }
                TimeStampResponse tsResponse = new TimeStampResponse(stamp);
                if (validateStamp(nonce, requests.get(nonce), tsResponse) == false) {
                    throw new XMLSignatureException("Invalid time stamp!");
                }

                signature = signatures.get(nonce);
                Element objectElement = getChildElement(signature, "ds:Object");
                if (objectElement == null) {
                    throw new XMLSignatureException("Invalid signature!");
                }

                Element qualifyingPropElement = getChildElement(objectElement, "xad:QualifyingProperties");
                if (qualifyingPropElement == null) {
                    throw new XMLSignatureException("Invalid signature!");
                }
                Element unsignedPropElement = getChildElement(qualifyingPropElement, "xad:UnsignedProperties");
                if (unsignedPropElement == null)
                {
                    unsignedPropElement = createXadesElement(document, Xades.xadesNS, "UnsignedProperties");
                    qualifyingPropElement.appendChild(unsignedPropElement);
                }
                Element unsignedSignPropElement = getChildElement(unsignedPropElement, "xad:UnsignedSignatureProperties");
                if (unsignedSignPropElement == null)
                {
                    unsignedSignPropElement = createXadesElement(document, Xades.xadesNS, "UnsignedSignatureProperties");
                    unsignedPropElement.appendChild(unsignedSignPropElement);
                }

                Element sigTsElement = createXadesElement(document, Xades.xadesNS, "SignatureTimeStamp");
                unsignedSignPropElement.appendChild(sigTsElement);

                Element hashDataInfoElmt = createXadesElement(document, Xades.xadesNS, "HashDataInfo");
                hashDataInfoElmt.setAttribute("uri", "#" + signature.getAttribute("Id"));
                sigTsElement.appendChild(hashDataInfoElmt);

                Element transforms = createXadesElement(document, Xades.xadesNS, "Transforms");
                hashDataInfoElmt.appendChild(transforms);

                Element transformsElmt = document.createElement("ds:Transform");
                transformsElmt.setAttribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#rsa-sha1");
                transforms.appendChild(transformsElmt);
                Element stampElmt = createXadesElement(document, Xades.xadesNS, "EncapsulatedTimeStamp");

                stampElmt.setAttribute("Id", signature.getAttribute("Id") + "_TS");
                Text stString = document.createTextNode(new String(Base64.encode(stamp), "UTF8"));
                stampElmt.appendChild(stString);
                sigTsElement.appendChild(stampElmt);
            }


            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(document), new StreamResult(byteArrayOutputStream));
            return byteArrayOutputStream.toByteArray();

        } catch (MarshalException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (TSPValidationException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CertStoreException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateExpiredException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateNotYetValidException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (TSPException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (TransformerException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (NoSuchAlgorithmException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);  //should never happen
        } catch (SAXException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (ParserConfigurationException ex) {
            logger.error(null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        }
    }

    static Element getChildElement(Element element, String childTagName)
    {
        NodeList childs = element.getChildNodes();
        Element childElement = null;
        for (int i = 0; i < childs.getLength(); i++) {
            Node child = childs.item(i);
            if (child instanceof Element) {
                if (!((Element) child).getTagName().equals(childTagName)) {
                    continue;
                }
                else {
                    childElement = (Element) child;
                    break;
                }
            }
        }
        return childElement;
    }

    /**
     * query a timestamp for a single request.
     */
    static byte[] queryStamp(TimeStampRequest request, String serverURLStr)
    {

        try
        {
            URL url = new URL(serverURLStr);
            URLConnection connection = url.openConnection();
            connection.setRequestProperty("Content-Type", "application/timestamp-query");

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);

            DataOutputStream outStream = new DataOutputStream(connection.getOutputStream());
            outStream.write(request.getEncoded()); //outStream.write(Base64.encode(request.getEncoded()));
            outStream.flush();
            outStream.close();

            DataInputStream inStream = new DataInputStream(connection.getInputStream());
            while (inStream.available() == 0)
            {
                try
                {
                    Thread.sleep(300);
                } catch (InterruptedException ex)
                {
                    logger.error(null, ex);
                    return null;
                }
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[2048];
            int readen = 0;
            do
            {
                readen = inStream.read(buffer);
                if (readen > 0)
                    baos.write(buffer, 0, readen);
            }
            while (readen != -1);
            return baos.toByteArray();

        } catch (IOException ex)
        {
            logger.error(null, ex);
            return null;
        }
    }

    static boolean validateStamp(BigInteger nonce, TimeStampRequest req, TimeStampResponse response) throws TSPValidationException, CertStoreException, CertificateExpiredException, CertificateNotYetValidException
    {
        try
        {
            if (response.getStatus() != PKIStatus.GRANTED && response.getStatus() != PKIStatus.GRANTED_WITH_MODS) {
                return false;
            }
            if (!nonce.equals(response.getTimeStampToken().getTimeStampInfo().getNonce())) {
                return false;
            }
            response.validate(req);

            if (!validate(response.getTimeStampToken().toCMSSignedData().getSignerInfos(),
                    response.getTimeStampToken().getCertificates(), response)) {
                return false;
            }
            return true;
        } catch (TSPException ex) {
            logger.error(null, ex);
            throw new TSPValidationException(ex.getMessage());
        } catch (NoSuchAlgorithmException ex) {
            logger.error(null, ex);
            throw new TSPValidationException(ex.getMessage());
        } catch (NoSuchProviderException ex) {
            logger.error(null, ex);
            throw new TSPValidationException(ex.getMessage());
        } catch (CMSException ex) {
            logger.error(null, ex);
            throw new TSPValidationException(ex.getMessage());
        } catch (OperatorCreationException ex) {
            logger.error(null, ex);
            throw new TSPValidationException(ex.getMessage());
        }
    }

    static boolean validate(SignerInformationStore signers, Store certStore, TimeStampResponse response) throws CertStoreException, NoSuchAlgorithmException, NoSuchProviderException, CMSException, TSPException, TSPValidationException, CertificateExpiredException, CertificateNotYetValidException, OperatorCreationException
    {
        Iterator iter = signers.getSigners().iterator(); // there may be more than one signers
        SignerInformation signer;
        X509Certificate certificate;
        Date today = new Date();
        TimeStampToken tsToken = response.getTimeStampToken();
        while (iter.hasNext())
        {
            signer = (SignerInformation) iter.next();
            Collection certColl = certStore.getMatches(signer.getSID());

            java.util.Iterator certIter = certColl.iterator();
            while (certIter.hasNext())
            {
                certificate = (X509Certificate) certIter.next();

                // TODO: if needed, perform here further validations about the TS authority's certificate

                if (certificate.getNotBefore().before(today)
                        && today.before(certificate.getNotAfter())) {
                    tsToken.validate(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(certificate));
                    // tsToken.validate(certificate, "BC");
                }
                else {
                    return false;
                }
            }
        }
        return true;
    }

    static org.w3c.dom.Element createElement(org.w3c.dom.Document document, String namespaceURI, String qualifiedName) {
        org.w3c.dom.Element ret = document.createElementNS(namespaceURI, "ds:" + qualifiedName);
        return ret;
    }

    static  org.w3c.dom.Element createXadesElement(org.w3c.dom.Document document, String namespaceURI, String qualifiedName) {
        org.w3c.dom.Element ret = document.createElementNS(namespaceURI, "xad:" + qualifiedName);
        ret.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns", Xades.xadesNS);
        return ret;
    }

    static  org.w3c.dom.Element createXades122Element(org.w3c.dom.Document document, String namespaceURI, String qualifiedName) {
        org.w3c.dom.Element ret = document.createElementNS(namespaceURI, "xad:" + qualifiedName);
        ret.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns", Xades.xadesNS122);
        return ret;
    }
}
