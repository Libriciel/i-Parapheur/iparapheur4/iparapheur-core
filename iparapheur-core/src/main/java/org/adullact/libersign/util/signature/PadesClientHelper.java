/*
 * Version 2.1
 * CeCILL Copyright (c) 2014, ADULLACT-projet
 * Developped by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.libersign.util.signature;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.X509Certificate;
import java.util.Date;
import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.PKCS9Attribute;
import sun.security.pkcs.PKCS9Attributes;
import sun.security.pkcs.SignerInfo;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

/**
 * Pades Client Helper.
 * 
 * @author Paul Merlin
 * @author Stephane Vast - Adullact Projet
 */
public class PadesClientHelper
{
    private final String OS_NAME = System.getProperty( "os.name" );
    private final X509Certificate[] certChain;
    private final PrivateKey privateKey;
    private final byte[] hash;

    public PadesClientHelper( X509Certificate[] certChain, PrivateKey privateKey, byte[] hash )
    {
        this.certChain = certChain;
        this.privateKey = privateKey;
        this.hash = hash;
    }

    public byte[] signHash()
        throws NoSuchAlgorithmException, NoSuchProviderException, IOException, InvalidKeyException, SignatureException
    {
        AlgorithmId digestAlg = AlgorithmId.get( "SHA1" );

        Signature sig = ( privateKey instanceof java.security.interfaces.RSAPrivateKey )
                        ? Signature.getInstance( "SHA1WithRSA", "BC" )
                        : ( OS_NAME.startsWith( "Windows" ) ? Signature.getInstance( "SHA1WithRSA", "SunMSCAPI" )
                            : Signature.getInstance( "SHA1WithRSA", "SunRsaSign" ) );

        PKCS9Attribute[] authenticatedAttributeList =
        {
            new PKCS9Attribute( PKCS9Attribute.SIGNING_TIME_OID, new Date() ),
            new PKCS9Attribute( PKCS9Attribute.CONTENT_TYPE_OID, ContentInfo.DATA_OID ),
            new PKCS9Attribute( PKCS9Attribute.MESSAGE_DIGEST_OID, hash )
        };
        PKCS9Attributes authenticatedAttributes = new PKCS9Attributes( authenticatedAttributeList );

        sig.initSign( privateKey );
        sig.update( authenticatedAttributes.getDerEncoding() );
        byte[] signedAttributes = sig.sign();

        // detached signature
        ContentInfo contentInfo = new ContentInfo( ContentInfo.DATA_OID, null );

        // signer serial
        java.math.BigInteger serial = certChain[0].getSerialNumber();
        SignerInfo signerInfo = new SignerInfo(
            X500Name.asX500Name( certChain[0].getIssuerX500Principal() ),
            serial, digestAlg, authenticatedAttributes,
            new AlgorithmId( AlgorithmId.RSAEncryption_oid ), signedAttributes, null
        );
        AlgorithmId[] algs =
        {
            digestAlg
        };
        SignerInfo[] infos =
        {
            signerInfo
        };
        PKCS7 p7Signature = new PKCS7( algs, contentInfo, certChain, infos );
        ByteArrayOutputStream nbaos = new ByteArrayOutputStream();
        p7Signature.encodeSignedData( nbaos );
        byte[] signature = nbaos.toByteArray();
        return signature;
    }
}
