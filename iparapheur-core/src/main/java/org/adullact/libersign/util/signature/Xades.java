/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, AtolCD, ADULLACT-projet
 * CeCILL Copyright (c) 2008-2014, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * contact@atolcd.com
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package org.adullact.libersign.util.signature;

import org.apache.log4j.Logger;
import org.apache.xpath.XPathAPI;
import org.springframework.extensions.surf.util.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Utilitaires autour de la Signature XAdES.
 * C'est compliqué: signature, co-signature, contre-signature.
 * Il faut un chemin XPath pour savoir où poser la signature dans le doc XML
 *
 * @author Stephane Vast (Adullact Projet)
 */
public class Xades {
    static final String xadesNS = "http://uri.etsi.org/01903/v1.1.1#";
    /**
     * specific namespace for DIA signing schema: required is XAdES 1.2.2
     */
    static final String xadesNS122 = "http://uri.etsi.org/01903/v1.2.2#";
    // departement node xpath
    public static final String DIA_DP_PATH = "/dia:renonciationAnticipee/dia:titulairesDroitPreemption/dia:departement";
    // conservatoire node xpath
    public static final String DIA_CL_PATH = "/dia:renonciationAnticipee/dia:titulairesDroitPreemption/dia:conservatoireLittoral";
    // commune node xpath
    public static final String DIA_CM_PATH = "/dia:renonciationAnticipee/dia:titulairesDroitPreemption/dia:commune";
    /**
     * Signature IDs as described in the dictionnary for DIA
     */
    public static final String DIA_CM_SIG = "sigOrgaCom";
    public static final String DIA_CL_SIG = "sigOrgaConsLit";
    public static final String DIA_DP_SIG = "sigOrgaDep";
    // namespace for DIA
    static final String DIA_NS = "http://xmlschema.ok-demat.com/DIA";
    // identification node xpath
    static final String DIA_ID_PATH = "/dia:renonciationAnticipee/dia:identificationDIA";
    private static Logger logger = Logger.getLogger(Xades.class);


    public Xades() {
        // constructeur vide
    }

    /**
     * Injecte une signature XAdES dans un fichier XML
     *
     * @param strXml       Le flux XML qui doit etre enrichi
     * @param strSig       La signature XAdES (ou les signatures, en CSV+base64)
     * @param strXpath     Le chemin XPATH localisant la signature
     * @param pXmlencoding Encodage de caracteres à utiliser
     * @return Le flux XML enrichi de la signature
     */
    public static InputStream injectXadesSigIntoXml(InputStream strXml, String strSig, String strXpath, String pXmlencoding) {

        ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
        nu.xom.Builder builder = new nu.xom.Builder();

        ByteArrayInputStream l_str = null;

        try {
            nu.xom.Document document = builder.build(strXml);
            nu.xom.Element root = document.getRootElement();

            String ID;
            String tmpID;
            String xpath;
            String xpath1;
            nu.xom.Nodes sigNodeList;
            nu.xom.XPathContext nsctx = addNameSpace(document);
            // Si "Id" est absent, on essaie de gerer quand meme...
            if (strXpath.trim().equals(".")) {
                ID = root.getAttribute("Id") != null ? root.getAttribute("Id").getValue() : "";
                if (ID == null || ID.isEmpty()) {
                    xpath1 = "//*/" + strXpath;
                } else {
                    xpath1 = "//*[@Id='" + ID + "']/" + strXpath;
                }

                // canal historique....
                tmpID = ID + "_SIG_";
                xpath = xpath1 + "/ds:Signature[fn:starts-with(@Id,'" + tmpID + "')]";
                logger.debug("injectXadesSigIntoXml: xpath1=" + xpath1 + ", xpath=" + xpath);

                nu.xom.Nodes nTmp = document.query(xpath, nsctx);
                tmpID = tmpID + Integer.toString(nTmp.size()+1);

                logger.debug("injectXadesSigIntoXml: " + nTmp.size() + " node(s). tmpID=" + tmpID);

                sigNodeList = document.query(xpath1, nsctx);

                if (sigNodeList.size() == 0) { // strXpath est pourri
                    logger.warn("injectXadesSigIntoXml:  strXpath est pourri");
                    return null;
                }

                root.addNamespaceDeclaration("ds", "http://www.w3.org/2000/09/xmldsig#");

                nu.xom.Document sigDoc = builder.build(new ByteArrayInputStream(Base64.decode(strSig)));

                nu.xom.Node elmToAdd = sigDoc.getRootElement().getChild(0);
                elmToAdd.detach();
                nu.xom.Element sigElement = (nu.xom.Element) sigNodeList.get(0);
                sigElement.insertChild(elmToAdd, sigElement.getChildCount());
            } else {
                // Warn: il y a potentiellement plusieurs noeuds !
                xpath1 = strXpath;
                sigNodeList = document.query(xpath1, nsctx);
                if (sigNodeList.size() > 0) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("injectXadesSigIntoXml: on a trouve " + sigNodeList.size() + "resultats à traiter.");
                    }
                }

                ByteArrayInputStream[] strSigTab = new ByteArrayInputStream[sigNodeList.size()];
                if (sigNodeList.size() > 1) {
                    // défaire le CSV reçu et base64.decode chaque morceau
                    String[] strSigTabBase64 = strSig.split(",");
                    for (int i = 0; i < sigNodeList.size(); i++) {
                        strSigTab[i] = new ByteArrayInputStream(Base64.decode(strSigTabBase64[i]));
                    }
                } else {
                    // un seul candidat, fastoche
                    strSigTab[0] = new ByteArrayInputStream(Base64.decode(strSig));
                }

                //Loop 'ntil the end of the world.
                for (int i = 0; i < sigNodeList.size(); i++) {
                    // Parser chaque signature String --> arbre DOM XML

                    ID = ((nu.xom.Element)sigNodeList.get(i)).getAttributeValue("Id");


                    //Traitement nouveau, adapté aux résultats multiples.
                    tmpID = ID + "_SIG_";
                    xpath = xpath1 + "/ds:Signature[fn:starts-with(@Id,'" + tmpID + "')]";
                    nu.xom.Nodes nTmp;
                    if (logger.isDebugEnabled()) {
                        logger.debug("injectXadesSigIntoXml: xpath1=" + xpath1 + ", xpath=" + xpath);
                    }
                    nTmp = document.query(xpath, nsctx);
                    tmpID = tmpID + Integer.toString(nTmp.size() +1);
                    if (logger.isDebugEnabled()) {
                        logger.debug("injectXadesSigIntoXml: " + nTmp.size() + " node(s). tmpID=" + tmpID);
                    }
                    sigNodeList = document.query(xpath1, nsctx);
                    if (sigNodeList.size() == 0) { // strXpath est pourri
                        logger.warn("injectXadesSigIntoXml:  strXpath est pourri");
                        return null;
                    }

                    root.addNamespaceDeclaration("ds", "http://www.w3.org/2000/09/xmldsig#");

                    nu.xom.Document sigDoc = builder.build(strSigTab[i]);

                    nu.xom.Node elmToAdd = sigDoc.getRootElement().getChild(0);
                    elmToAdd.detach();
                    nu.xom.Element sigElement = (nu.xom.Element) sigNodeList.get(i);
                    sigElement.insertChild(elmToAdd, sigElement.getChildCount());
                }
            }

            nu.xom.Serializer serializer = new nu.xom.Serializer(bytestream, pXmlencoding);

            serializer.write(document);

            l_str = new ByteArrayInputStream(bytestream.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.debug("injectXadesSigIntoXml: SORTIE");
        return l_str;
    }

    public static NodeList getNodeFromIdAndXpath(Document document, String strId, String strXpath) {
        String l_xpath = "//*[@Id='" + strId + "']/" + strXpath;
        Element l_nsctx;
        NodeList l_elt = null;
        try {
            l_nsctx = addNameSpace(document);
            l_elt = XPathAPI.selectNodeList(document, l_xpath, l_nsctx);
        } catch (Exception e) {
            logger.debug("getNodeFromIdAndXpath: Exception\n" + e.getMessage());
            logger.debug(e);
        }
        return l_elt;
    }

    public static NodeList getExistingSig(Document document, String strId) {
        // String l_xpath = "*[starts-with(@Id, '" + strId + "_SIG_')]";
        String l_xpath = "//*[fn:starts-with(@Id, '" + strId + "_SIG_')]";
        Element l_nsctx;
        NodeList l_elt = null;
        try {
            l_nsctx = addNameSpace(document);
            l_elt = XPathAPI.selectNodeList(document, l_xpath, l_nsctx);
        } catch (Exception e) {
            logger.debug("getExistingSig: Exception\n" + e.getMessage());
            logger.debug(e);
        }
        return l_elt;
    }

    private static nu.xom.XPathContext addNameSpace(nu.xom.Document document) {
        if (logger.isDebugEnabled()) {
            logger.debug("addNameSpace");
        }
        nu.xom.XPathContext l_nsctx = nu.xom.XPathContext.makeNamespaceContext(document.getRootElement());

        l_nsctx.addNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
        l_nsctx.addNamespace("xad", "http://uri.etsi.org/01903/v1.1.1#");
        l_nsctx.addNamespace("xenc", "http://www.w3.org/2001/04/xmlenc#");
        return l_nsctx;
    }

    private static Element addNameSpace(Document document) {
        if (logger.isDebugEnabled()) {
            logger.debug("addNameSpace");
        }
        String prefixPES = document.lookupPrefix(XadesNSContext.PESaller_NS);
        Element l_nsctx = document.createElement("nsctx");
        if (prefixPES != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("document prefix found for PES = " + prefixPES);
            }
            l_nsctx.setAttribute("xmlns:" + prefixPES, XadesNSContext.PESaller_NS);
        }
        if (document.getPrefix() != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("document prefix found = " + document.getPrefix());
            }
            l_nsctx.setAttribute("xmlns:" + document.getPrefix(), document.getNamespaceURI());
        }
        l_nsctx.setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
        l_nsctx.setAttribute("xmlns:xad", "http://uri.etsi.org/01903/v1.1.1#");
        l_nsctx.setAttribute("xmlns:xenc", "http://www.w3.org/2001/04/xmlenc#");
        return l_nsctx;
    }
}
