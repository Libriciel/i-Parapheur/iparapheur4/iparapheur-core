/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.adullact.utils;

import org.apache.commons.validator.EmailValidator;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class StringUtils {

    public static String generateStringFromList(List<String> list) {
        String res = "";
        int i;
        for (i = 0; i < list.size() - 1; i++) {
            res += list.get(i) + ", ";
        }

        res += list.get(i);
        return res;
    }

    public static String generateStringFromListWithQuotes(List<String> list, String lquote, String rquote) {
        return lquote + generateStringFromList(list) + rquote;
    }

    public static boolean isUrlRFC3986Valid(@Nullable String url) {

        // Default case

        if (isEmpty(url)) {
            return false;
        }

        // From https://mathiasbynens.be/demo/url-regex => @diegoperini
        String url3986regex = "^(?:(?:[a-z]*)://)(?:\\S+(?::\\S*)?@)?(?:(?!10(?:\\.\\d{1,3}){3})(?!127(?:\\.\\d{1,3}){3})(?!169\\." +
                              "254(?:\\.\\d{1,3}){2})(?!192\\.168(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
                              "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d" +
                              "\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1" +
                              "-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))(?::\\d{2,5})?(?:/[^\\s]" +
                              "*)?$";

        Pattern r = Pattern.compile(url3986regex, Pattern.CASE_INSENSITIVE);
        Matcher m = r.matcher(url);
        return m.find();
    }

    /**
     * Checks every mail given, returns the first valid one.
     * It checks for null, empty, and validate it with the {@link EmailValidator}.
     *
     * @param logger to log warning messages, may be null
     * @param addresses a list of string, some may be null
     * @return a trimmed String, or null if none was correct
     */
    public static String firstValidMailAddress(@Nullable Logger logger, @Nullable String... addresses) {

        // Default case

        if (addresses == null)
            return null;

        //

        String result = null;
        EmailValidator emailValidator = EmailValidator.getInstance();

        for (int i = 0; i < addresses.length; i++) {
            if ((addresses[i] != null) && (addresses[i].trim().length() != 0) && (emailValidator.isValid(addresses[i].trim()))) {
                result = addresses[i].trim();
                break;
            }
            else {
                if (logger != null) {
                    logger.warn("Invalid mail address at index " + i);
                }
            }
        }

        return result;
    }

    /**
     * Checks every mail given, returns the first valid one.
     * It checks for null, empty, and validate it with the {@link EmailValidator}.
     *
     * @param logger to log warning messages, may be null
     * @param addresses a list of string, some may be null
     * @return a trimmed String, or null if none was correct
     */
    public static String[] firstValidMailAddresses(@Nullable Logger logger, @Nullable String[]... addresses) {

        // Default case

        if (addresses == null)
            return null;

        //
        List<String> result = new ArrayList<String>();

        EmailValidator emailValidator = EmailValidator.getInstance();

        for (int i = 0; i < addresses.length; i++) {
            result.clear();
            for(String mail : addresses[i]) {
                if ((mail != null) && (mail.trim().length() != 0) && (emailValidator.isValid(mail.trim()))) {
                    result.add(mail.trim());
                }
                else {
                    if (logger != null) {
                        logger.warn("Invalid mail address at index " + i);
                    }
                }
            }
            if(!result.isEmpty()) {
                break;
            }
        }

        return result.toArray(new String[]{});
    }

    /**
     * Checks every String given, returns the first not-null, not empty one.
     *
     * @param strings a list of String, some may be null
     * @return a trimmed String, or null if none was correct
     */
    public static String firstNotEmpty(@Nullable String... strings) {

        // Default case

        if (strings == null)
            return null;

        //

        for (String string : strings) {
            if (org.apache.commons.lang.StringUtils.isNotEmpty(string)) {
                return string;
            }
        }

        return null;
    }

    /**
     * <p>Check if a String starts with a specified prefix.</p>
     *
     * For some reason, it provokes an NoSuchMethodException if we try to call it form commons.lang.StringUtils.
     * So it was copy-pasted it...
     *
     * @see java.lang.String#startsWith(String)
     * @param str  the String to check, may be null
     * @param prefix the prefix to find, may be null
     *  (case insensitive) or not.
     * @return <code>true</code> if the String starts with the prefix or
     *  both <code>null</code>
     */
    public static boolean startsWithIgnoreCase(@Nullable String str, @Nullable String prefix) {

        if (str == null || prefix == null) {
            return (str == null && prefix == null);
        }

        if (prefix.length() > str.length()) {
            return false;
        }

        return str.regionMatches(true, 0, prefix, 0, prefix.length());
    }

    /**
     * <p>Checks if a String is empty ("") or null.</p>
     *
     * <pre>
     * StringUtils.isEmpty(null)      = true
     * StringUtils.isEmpty("")        = true
     * StringUtils.isEmpty(" ")       = false
     * StringUtils.isEmpty("bob")     = false
     * StringUtils.isEmpty("  bob  ") = false
     * </pre>
     *
     * <p>NOTE: This method changed in Lang version 2.0.
     * It no longer trims the String.
     * That functionality is available in isBlank().</p>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if the String is empty or null
     */
    public static boolean isEmpty(@Nullable String str) {
        return str == null || str.length() == 0;
    }

    /**
     * <p>Compares two Strings, returning <code>true</code> if they are equal ignoring
     * the case.</p>
     *
     * <p><code>null</code>s are handled without exceptions. Two <code>null</code>
     * references are considered equal. Comparison is case insensitive.</p>
     *
     * <pre>
     * StringUtils.equalsIgnoreCase(null, null)   = true
     * StringUtils.equalsIgnoreCase(null, "abc")  = false
     * StringUtils.equalsIgnoreCase("abc", null)  = false
     * StringUtils.equalsIgnoreCase("abc", "abc") = true
     * StringUtils.equalsIgnoreCase("abc", "ABC") = true
     * </pre>
     *
     * @see java.lang.String#equalsIgnoreCase(String)
     * @param str1  the first String, may be null
     * @param str2  the second String, may be null
     * @return <code>true</code> if the Strings are equal, case insensitive, or
     *  both <code>null</code>
     */
    public static boolean equalsIgnoreCase(@Nullable String str1, @Nullable String str2) {
        return str1 == null ? str2 == null : str1.equalsIgnoreCase(str2);
    }

}
