/*
 * Version 2.1
 * CeCILL Copyright (c) 2008-2012, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 * 
 */
package org.adullact.iparapheur.status;

/**
 * Liste des status métier pour les étapes d'un dossier
 *
 * @author jmaire
 */
public class StatusMetier {
    
    public static final String STATUS_NON_LU = "NonLu";
    public static final String STATUS_LU = "Lu";
    public static final String STATUS_SIGNE = "Signe";
    public static final String STATUS_REJET_SIGNATAIRE = "RejetSignataire";
    public static final String STATUS_PRET_VISA = "PretVisa";
    public static final String STATUS_VISE = "Vise";
    public static final String STATUS_REJET_VISA = "RejetVisa";
    
    public static final String STATUS_PRET_TDT = "PretTdT";
    public static final String STATUS_EN_COURS_TRANSMISSION = "EnCoursTransmission";
    public static final String STATUS_TRANSMISSION_OK = "TransmissionOK";
    public static final String STATUS_TRANSMISSION_KO = "TransmissionKO";
    public static final String STATUS_REJET_TRANSMISSION = "RejetTransmission";
    
    public static final String STATUS_PRET_MAILSEC = "PretMailSec";
    public static final String STATUS_EN_COURS_MAILSEC = "EnCoursMailSec";
    public static final String STATUS_MAILSEC_OK = "MailSecOK";
    public static final String STATUS_REJET_MAILSEC = "RejetMailSec";

    public static final String STATUS_PRET_MAILSEC_PASTELL = "PretMailSecPastell";
    public static final String STATUS_EN_COURS_MAILSEC_PASTELL = "EnCoursMailSecPastell";
    public static final String STATUS_OK_MAILSEC_PASTELL = "MailSecPastellOK";
    public static final String STATUS_REJET_MAILSEC_PASTELL = "RejetMailSecPastell";

    public static final String STATUS_EN_COURS_ATTEST = "EnCoursAttest";
    public static final String STATUS_ATTEST_KO = "AttestKO";
    public static final String STATUS_ATTEST_OK = "AttestOK";

    public static final String STATUS_PRET_CACHET = "PretCachet";
    public static final String STATUS_REJET_CACHET = "RejetCachet";
    public static final String STATUS_CACHET_OK = "CachetOK";

    public static final String STATUS_ARCHIVE = "Archive";
    public static final String STATUS_A_TRAITER = "ATraiter";
    
    // Useful for evaluators
    public static final String STATUS_EN_COURS = "EnCours";

    /**
     * Still used by web-services, so do not erase.
     * FR: La compatibilite ascendante c'est hyper important en web-services.
     */
    @Deprecated
    public static final String STATUS_EN_COURS_VISA = "EnCoursVisa";
}
