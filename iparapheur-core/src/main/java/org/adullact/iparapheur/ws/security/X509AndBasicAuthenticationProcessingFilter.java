/*
 * Version 2.1
 * CeCILL Copyright (c) 2008-2009, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.ws.security;

import java.io.IOException;
import java.security.cert.X509Certificate;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import net.sf.acegisecurity.Authentication;
import net.sf.acegisecurity.AuthenticationException;
import net.sf.acegisecurity.AuthenticationManager;
import net.sf.acegisecurity.BadCredentialsException;
import net.sf.acegisecurity.UserDetails;
import net.sf.acegisecurity.context.security.SecureContextUtils;
import net.sf.acegisecurity.intercept.web.AuthenticationEntryPoint;
import net.sf.acegisecurity.providers.UsernamePasswordAuthenticationToken;
import net.sf.acegisecurity.providers.anonymous.AnonymousAuthenticationToken;
import net.sf.acegisecurity.providers.dao.event.AuthenticationSuccessEvent;
import net.sf.acegisecurity.ui.AbstractProcessingFilter;
import net.sf.acegisecurity.ui.WebAuthenticationDetails;
import net.sf.acegisecurity.ui.rememberme.RememberMeServices;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.web.app.servlet.AbstractAuthenticationFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.extensions.surf.util.Base64;
import org.springframework.util.Assert;

/**
 * Filtre d'authentification prenant en compte deux methodes: authentification
 * basique (obligatoire) + authentification par certificat client.
 * L'authentification resultante est la concatenation de l'identite extraite du
 * certificat et du nom d'utilisateur passe en authentification basique. 
 * Voir setDealWithCertificate, setX509SubjectDNRegex, setTokenSeparator...
 * 
 * @author Emmanuel Delmasure - Berger Levrault
 * @author Stepahne Vast - Adullact Projet
 */
public class X509AndBasicAuthenticationProcessingFilter extends AbstractAuthenticationFilter implements Filter, InitializingBean, ApplicationEventPublisherAware
{
    class X509AndBasicAuthenticationException extends AuthenticationException {
	private static final long serialVersionUID = 1L;
	public X509AndBasicAuthenticationException(String msg) {
	    super(msg);
	}
    }
    private static final Log logger = LogFactory.getLog(X509AndBasicAuthenticationProcessingFilter.class);
    private ApplicationEventPublisher eventPublisher;
    private AuthenticationManager authenticationManager;
    private TransactionService transactionService;
    private AuthenticationComponent authenticationComponent;
    private AuthenticationEntryPoint basicAuthenticationEntryPoint;
    private RememberMeServices rememberMeServices;
    private String x509SubjectDNRegex;
    private boolean ignoreFailure = false;
    private String tokenSeparator;
    private Pattern subjectDNPattern;
    private boolean dealWithCertificate = true;

    public boolean isDealWithCertificate()
    {
	return dealWithCertificate;
    }

    /**
     * Definit si il faut tenir compte du nom issu du certificat pour la construction du nom d'utilisateur.
     * 
     * @param dealWithCertificate
     */
    public void setDealWithCertificate(boolean dealWithCertificate)
    {
	this.dealWithCertificate = dealWithCertificate;
    }

    public String getX509SubjectDNRegex()
    {
	return x509SubjectDNRegex;
    }

    /**
     * Definit l'expression reguliere a appliquer a la chaine du certificat pour
     * en extraire une identite.
     * 
     * @param subjectDNRegex
     */
    public void setX509SubjectDNRegex(String subjectDNRegex)
    {
	x509SubjectDNRegex = subjectDNRegex;
    }

    public String getTokenSeparator() 
    {
	return tokenSeparator;
    }

    /**
     * Definit le separateur a utiliser pour construire le nom d'utilisateur resultant de la concatenation de
     * l'identite du certificat et du nom d'utilisateur passe par l'authentification basique.
     * 
     * @param tokenSeparator
     */
    public void setTokenSeparator(String tokenSeparator)
    {
	this.tokenSeparator = tokenSeparator;
    }

    public boolean isIgnoreFailure()
    {
	return ignoreFailure;
    }

    public void setIgnoreFailure(boolean ignoreFailure)
    {
	this.ignoreFailure = ignoreFailure;
    }

    public AuthenticationEntryPoint getBasicAuthenticationEntryPoint()
    {
	return basicAuthenticationEntryPoint;
    }

    public void setBasicAuthenticationEntryPoint(AuthenticationEntryPoint authenticationEntryPoint)
    {
	this.basicAuthenticationEntryPoint = authenticationEntryPoint;
    }

    public RememberMeServices getRememberMeServices()
    {
	return rememberMeServices;
    }

    public void setRememberMeServices(RememberMeServices rememberMeServices)
    {
	this.rememberMeServices = rememberMeServices;
    }

    public void destroy()
    {
    }

    public void afterPropertiesSet() throws Exception
    {
	Assert.notNull(authenticationManager, "An AuthenticationManager must be set");
        Assert.notNull(authenticationComponent, "An AuthenticationComponent must be set");
	Assert.notNull(transactionService, "There must be a transaction service");

	if (dealWithCertificate)
	{
	    Perl5Compiler compiler = new Perl5Compiler();

	    try
	    {
		subjectDNPattern = compiler.compile(x509SubjectDNRegex, Perl5Compiler.READ_ONLY_MASK | Perl5Compiler.CASE_INSENSITIVE_MASK);
	    } catch (MalformedPatternException mpe)
	    {
		throw new IllegalArgumentException("Malformed regular expression: " + x509SubjectDNRegex);
	    }
	}
    }

    private String extractUserNameFromX509Certificate(X509Certificate clientCert) throws AuthenticationException
    {
	String subjectDN = clientCert.getSubjectDN().getName();
	PatternMatcher matcher = new Perl5Matcher();
	if (!matcher.contains(subjectDN, subjectDNPattern))
	{
	    throw new BadCredentialsException("DaoX509AuthoritiesPopulator.noMatching: no matching pattern was found in subjectDN: {0}");
	}
	MatchResult match = matcher.getMatch();
	if (match.groups() != 2)
	{ // 2 = 1 + the entire match
	    throw new IllegalArgumentException("Regular expression must contain a single group ");
	}
	String userName = match.group(1);
	return userName;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
	HttpServletRequest httpRequest = (HttpServletRequest) request;
	HttpServletResponse httpResponse = (HttpServletResponse) response;

	if (!(request instanceof HttpServletRequest))
	{
	    throw new ServletException("Can only process HttpServletRequest");
	}

	if (!(response instanceof HttpServletResponse))
	{
	    throw new ServletException("Can only process HttpServletResponse");
	}

	String header = httpRequest.getHeader("Authorization");

	if (logger.isInfoEnabled())
	    logger.info("Authorization header: " + header);
	
	if (header == null || !header.startsWith("Basic "))
	{
	    basicAuthenticationEntryPoint.commence(request, response, new X509AndBasicAuthenticationException("No authorization header, asking for one..."));
	    return;
	} else {
	    if (SecureContextUtils.getSecureContext().getAuthentication() == null)
	    {
		String username = "";
		if (dealWithCertificate)
		{
		    X509Certificate clientCertificate = extractClientCertificate(httpRequest);
		    username = extractUserNameFromX509Certificate(clientCertificate) + getTokenSeparator();
		}
		String base64Token = header.substring(6);
		// if (logger.isDebugEnabled()) logger.debug(" base64Token=" + base64Token);
		    
		String token = new String(Base64.decode(base64Token));
		// if (logger.isDebugEnabled()) logger.debug(" token=" + token);

		String password = "";
		int delim = token.indexOf(":");

		if (delim != -1) {
		    username += token.substring(0, delim);
		    password = token.substring(delim + 1);
		}

		if (authenticationIsRequired(username)) {
		    UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
		    authRequest.setDetails(new WebAuthenticationDetails((HttpServletRequest) request));
		    if (logger.isDebugEnabled())
			logger.debug(" username=" + username + " password=" + password);

//		    Authentication authResult;
		    UserTransaction tx = transactionService.getNonPropagatingUserTransaction();
		    try {
			tx.begin();
//			authResult = authenticationManager.authenticate(authRequest);
                        authenticationComponent.authenticate(username, password.toCharArray());
			tx.commit();
		    } catch (AuthenticationException failed)
		    {
			try
			{
                            if (tx.getStatus() == javax.transaction.Status.STATUS_ACTIVE) {
                                tx.rollback();
                            }
			}
			catch (Exception ex2)
			{
			    logger.error("Failed to rollback transaction", ex2);
			}
			// Authentication failed
			if (logger.isDebugEnabled()) {
			    logger.debug("Authentication request for user: " + username + " failed: " + failed.toString());
			}

			SecureContextUtils.getSecureContext().setAuthentication(null);

			if (rememberMeServices != null) {
			    rememberMeServices.loginFail(httpRequest, httpResponse);
			}

			if (ignoreFailure) {
			    chain.doFilter(request, response);
			} else {
			    basicAuthenticationEntryPoint.commence(request, response, failed);
			}

			return;
		    }
                    catch (org.alfresco.repo.security.authentication.AuthenticationException ex) {
                        
                        try
			{
                            if (tx.getStatus() == javax.transaction.Status.STATUS_ACTIVE) {
                                tx.rollback();
                            }
			}
			catch (Exception ex2)
			{
			    logger.error("Failed to rollback transaction", ex2);
			}
                        
                        chain.doFilter(request, response);
                        return;
                    }
		    catch (Throwable ex)
		    {
			try
			{
                            if (tx.getStatus() == javax.transaction.Status.STATUS_ACTIVE) {
                                tx.rollback();
                            }
			}
			catch (Exception ex2)
			{
			    logger.error("Failed to rollback transaction", ex2);
			}

			if(ex instanceof AuthenticationException)
			{
			    logger.error("Impossible d'authentifier l'utisateur");
			    throw new RuntimeException("Utilisateur inconnu", ex);
			}
			else if(ex instanceof RuntimeException)
			{
			    logger.error(ex);
			    throw (RuntimeException)ex;
			}
			else
			{
			    logger.error(ex);
			    throw new RuntimeException("Failed to set authenticated user", ex);
			}
		    }

		    // Authentication success
//                    if (logger.isInfoEnabled())
//                        logger.info(username + " ("+password+"), from "+authResult.getDetails().toString());
//		    if (logger.isDebugEnabled())
//			logger.debug("doFilter: Authentication success: " + authResult.toString());

//		    SecureContextUtils.getSecureContext().setAuthentication(authResult);
//		    httpRequest.getSession().setAttribute("ACEGI_SECURITY_TOKEN", authResult);

//		    if (rememberMeServices != null) {
//			rememberMeServices.loginSuccess(httpRequest, httpResponse, authResult);
//		    }
		}
	    }

	}
	chain.doFilter(request, response);
    }


    /**
     * Puts the <code>Authentication</code> instance returned by the authentication manager into the secure context.
     * 
     * @param request     DOCUMENT ME!
     * @param response    DOCUMENT ME!
     * @param authResult  DOCUMENT ME!
     * 
     * @throws IOException DOCUMENT ME!
     */
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) throws IOException
    {
	if (logger.isDebugEnabled())
	    logger.debug("[successfulAuthentication]Authentication success: " + authResult);

	SecureContextUtils.getSecureContext().setAuthentication(authResult);

	// Fire event
	if (this.eventPublisher != null)
	{
	    eventPublisher.publishEvent(new AuthenticationSuccessEvent(authResult, (UserDetails) authResult.getDetails()));
	}
    }

    /**
     * Ensures the authentication object in the secure context is set to null when authentication fails.
     * 
     * @param request  DOCUMENT ME!
     * @param response DOCUMENT ME!
     * @param failed   DOCUMENT ME!
     */
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed)
    {
	SecureContextUtils.getSecureContext().setAuthentication(null);

	if (logger.isDebugEnabled())
	    logger.debug("Updated SecurityContextHolder to contain null Authentication");

	request.getSession().setAttribute(AbstractProcessingFilter.ACEGI_SECURITY_LAST_EXCEPTION_KEY, failed);
    }

    public AuthenticationManager getAuthenticationManager()
    {
	return authenticationManager;
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager)
    {
	this.authenticationManager = authenticationManager;
    }

    public TransactionService gettransactionService()
    {
	return transactionService;
    }

    public AuthenticationComponent getAuthenticationComponent() {
        return authenticationComponent;
    }

    public void setAuthenticationComponent(AuthenticationComponent authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }

    public void setTransactionService(TransactionService transactionService)
    {
	this.transactionService = transactionService;
    }

    private boolean authenticationIsRequired(String username)
    {
	// Only reauthenticate if username doesn't match SecurityContextHolder and user isn't authenticated
	// (see SEC-53)
	Authentication existingAuth = SecureContextUtils.getSecureContext().getAuthentication();

	if (existingAuth == null || !existingAuth.isAuthenticated()) {
	    return true;
	}

	// Limit username comparison to providers which use usernames (ie UsernamePasswordAuthenticationToken)
	// (see SEC-348)
	if (existingAuth instanceof UsernamePasswordAuthenticationToken && !existingAuth.getName().equals(username)) {
	    return true;
	}

	// Handle unusual condition where an AnonymousAuthenticationToken is already present
	// This shouldn't happen very often, as BasicProcessingFitler is meant to be earlier in the filter
	// chain than AnonymousProcessingFilter. Nevertheless, presence of both an AnonymousAuthenticationToken
	// together with a BASIC authentication request header should indicate reauthentication using the
	// BASIC protocol is desirable. This behavior is also consistent with that provided by form and digest,
	// both of which force re-authentication if the respective header is detected (and in doing so replace
	// any existing AnonymousAuthenticationToken). See SEC-610.
	if (existingAuth instanceof AnonymousAuthenticationToken) {
	    return true;
	}

	return false;
    }

    private X509Certificate extractClientCertificate(HttpServletRequest request)
    {
	X509Certificate[] certs = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");

	if ((certs != null) && (certs.length > 0)) {
	    return certs[0];
	}

	if (logger.isDebugEnabled()) {
	    logger.debug("No client certificate found in request.");
	}

	return null;
    }

    public void init(FilterConfig ignored) throws ServletException
    {
    }

    public void setApplicationEventPublisher(ApplicationEventPublisher context)
    {
	this.eventPublisher = context;
    }

}
