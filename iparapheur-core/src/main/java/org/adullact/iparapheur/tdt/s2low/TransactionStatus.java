/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2010, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package org.adullact.iparapheur.tdt.s2low;

/**
 * Liste des Statuts de transaction S2LOW
 * 
 * @author Stephane Vast - ADULLACT-Projet
 */
public class TransactionStatus {
    public static final int STATUS_ERROR = -1;
    public static final int STATUS_ANNULE = 0;
    public static final int STATUS_POSTE = 1;
    public static final int STATUS_EN_ATTENTE = 2;
    public static final int STATUS_TRANSMIS = 3;
    public static final int STATUS_ACK = 4;
    public static final int STATUS_VALIDE = 5;
    public static final int STATUS_NACK = 6;
    public static final int STATUS_EN_TRAITEMENT = 7;
    /**
     * nouveaute 2012 pour les PES_ACK de la DGFiP
     */
    public static final int STATUS_INFORMATIONS_DISPONIBLES = 8;
    public static final int STATUS_NON_TRANSMIS = 9;
}
