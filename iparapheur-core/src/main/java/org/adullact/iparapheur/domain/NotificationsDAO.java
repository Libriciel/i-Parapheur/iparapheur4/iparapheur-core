package org.adullact.iparapheur.domain;

import org.adullact.iparapheur.repo.notification.Notification;

import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 28/08/12
 * Time: 11:19
 * To change this template use File | Settings | File Templates.
 */
public interface NotificationsDAO {
    /**
     * Adds a notification (for the user idenfied by Notification#username)
     *
     * @param notif the notification to add
     */
    public void addNotification(Notification notif);

    /**
     * removes a notification
     *
     * @param notif
     */
    public void removeNotification(Notification notif);

    /**
     * Gets all the notifications targeting the user identified by username
     *
     * @param username the user
     * @return a List of notifications targeting username
     */
    public List<Notification> getNotificationsForUser(String username);

    /**
     * Gets the unread notifications targeting the user identified by username
     *
     * @param username the user
     * @return a List of unread notifications targeting username
     */
    public List<Notification> getUnreadNotificationsForUser(String username);

    /**
     * Gets the notification identified by the uuid and targeting the user
     * identified by username
     *
     * @param username the user
     * @param uuid     the unique identifier of the notification
     * @return
     */
    public Notification getNotificationWithUsername(String username, UUID uuid);

    /**
     * Marks the notification identified by uuid targeting the user
     * identified by username as Read
     *
     * @param username the user
     * @param uuid the unique identifier of the notification
     */
    public void setNotificationAsRead(String username, UUID uuid);


    /**
     * Marks the notification as unread
     * @param username
     * @param uuid
     */
    public void setNotificationAsUnread(String username, UUID uuid);


}
