package org.adullact.iparapheur.domain.hibernate;

import org.adullact.iparapheur.domain.NotificationsDAO;
import org.adullact.iparapheur.repo.notification.Notification;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 28/08/12
 * Time: 10:04
 * To change this template use File | Settings | File Templates.
 */
public class NotificationsDAOImpl extends HibernateDaoSupport implements NotificationsDAO {

    @Override
    public void addNotification(Notification notif) {
        getHibernateTemplate().merge(notif);
    }

    @Override
    public void removeNotification(Notification notif) {
        getHibernateTemplate().delete(notif);
    }

    @Override
    public List<Notification> getNotificationsForUser(String username) {
        Session session = getSession();
        List<Notification> retVal;
        Query query = session.getNamedQuery("notifications.GetNotificationsForUser");

        query.setParameter("username", username);

        retVal = (List<Notification>)query.list();

        return retVal;
    }

    @Override
    public List<Notification> getUnreadNotificationsForUser(String username) {
        Session session = getSession();
        List<Notification> retVal;
        Query query = session.getNamedQuery("notifications.GetUnreadNotificationsForUser");

        query.setParameter("username", username);

        retVal = (List<Notification>)query.list();
        
        return retVal;
    }

    @Override
    public Notification getNotificationWithUsername(String username, UUID uuid) {
        Session session = getSession();

        Query query = session.getNamedQuery("notifications.GetNotificationForUserAndUUID");
        query.setParameter("uuidString", uuid.toString());
        query.setParameter("username", username);

        Notification notification = (Notification)query.uniqueResult();

        return notification;
    }

    /**
     * up
     * @param username
     * @param n
     */
    protected void updateNotification(String username, Notification n) {
        Session session = getSession();

        n.setModifiedTimestamp(System.currentTimeMillis());
        session.save(n);
    }

    @Override
    public void setNotificationAsRead(String username, UUID uuid) {
        Notification notification = getNotificationWithUsername(username, uuid);
        notification.setRead(true);
        updateNotification(username, notification);
    }

    @Override
    public void setNotificationAsUnread(String username, UUID uuid) {
        Notification notification = getNotificationWithUsername(username, uuid);
        notification.setRead(false);
        updateNotification(username, notification);
    }

}
