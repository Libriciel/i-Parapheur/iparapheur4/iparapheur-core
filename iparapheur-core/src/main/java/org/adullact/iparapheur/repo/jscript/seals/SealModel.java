/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.adullact.iparapheur.repo.jscript.seals;

import org.alfresco.service.namespace.QName;

/**
 * Modèle de définition des certificats cachet serveur
 *
 * Créé par lhameury le 4/19/17.
 */
public interface SealModel {

    String PARAPHEUR_MODEL_URI = "http://www.atolcd.com/alfresco/model/parapheur/1.0";

    // Type d'objet pour certificat cachet serveur
    QName TYPE_SEAL_CERTIFICATE = QName.createQName(PARAPHEUR_MODEL_URI, "seal-certificate");
    // Type d'objet pour le dossier contenant les certificats cachet serveur
    QName TYPE_SEAL_CERTIFICATES = QName.createQName(PARAPHEUR_MODEL_URI, "seal-certificates");
    // Nom original du certificat envoyé
    QName PROP_CERTIFICATE_NAME = QName.createQName(PARAPHEUR_MODEL_URI, "certificate-name");
    // Image à poser sur le calque de signature
    QName PROP_SEAL_IMAGE = QName.createQName(PARAPHEUR_MODEL_URI, "seal-image");
    // Adresse Mail à utiliser pour l'alerte de certificat expiré
    QName PROP_WARN_MAIL = QName.createQName(PARAPHEUR_MODEL_URI, "warn-mail");
    // Text additionel à ajouter lors du tampon cachet serveur
    QName PROP_ADDITIONAL_TEXT = QName.createQName(PARAPHEUR_MODEL_URI, "additional-text");

}
