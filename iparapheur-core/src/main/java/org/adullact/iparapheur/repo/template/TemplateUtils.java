package org.adullact.iparapheur.repo.template;

import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Créé par lhameury le 5/10/17.
 */
public class TemplateUtils {

    private static @NotNull
    NodeRef createTemplateNode(@NotNull final String templateName, @NotNull final NodeRef rootNode, @NotNull final ServiceRegistry serviceRegistry) {
        NodeRef createdNode;

        NodeRef dictionary = serviceRegistry.getSearchService().selectNodes(
                rootNode,
                "/app:company_home/app:dictionary/app:email_templates",
                null,
                serviceRegistry.getNamespaceService(),
                false
        ).get(0);

        Map<QName, Serializable> props = new HashMap<QName, Serializable>();
        props.put(ContentModel.PROP_NAME, templateName);

        createdNode = serviceRegistry.getNodeService().createNode(
                dictionary,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(serviceRegistry.getNamespaceService().getNamespaceURI("cm"), templateName),
                ContentModel.TYPE_CONTENT,
                props
        ).getChildRef();

        ContentWriter writer = serviceRegistry.getContentService().getWriter(createdNode, ContentModel.PROP_CONTENT, true);
        writer.putContent(TemplateUtils.class.getClassLoader().getResourceAsStream(String.format("alfresco/extension/templates/freemarker/%s", templateName)));

        return createdNode;
    }


    public static @NotNull
    String getTemplateFromName(@NotNull String templateName, @NotNull ServiceRegistry serviceRegistry) {

        NodeRef templateNode;

        String templatePath = "/app:company_home/app:dictionary/app:email_templates/cm:" + templateName;
        NodeRef rootNode = serviceRegistry.getNodeService().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);

        List<NodeRef> templatesNodes = serviceRegistry.getSearchService().selectNodes(
                rootNode,
                templatePath,
                null,
                serviceRegistry.getNamespaceService(),
                false
        );

        if (templatesNodes.size() > 0) {
            templateNode = templatesNodes.get(0);
        } else {
            templateNode = createTemplateNode(templateName, rootNode, serviceRegistry);
        }

        return templateNode != null ? templateNode.toString() : "";
    }

}
