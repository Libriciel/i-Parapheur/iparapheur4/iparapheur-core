/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.repo.mc;

import org.adullact.iparapheur.repo.Collectivite;
import org.adullact.iparapheur.repo.MultiCModel;
import org.adullact.iparapheur.repo.amq.ActiveMQConnectionService;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.MutableAuthenticationService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.util.Assert;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * @author vbarousse - ADULLACT Projet
 */
public class MultiCServiceImpl implements MultiCService {

    // BLEX
    private static Logger logger = Logger.getLogger(MultiCService.class);

    private Properties configuration;
    private NodeService nodeService;
    private SearchService searchService;
    private NamespaceService namespaceService;
    private TenantService tenantService;
    private TenantAdminService tenantAdminService;
    private PersonService personService;
    private ContentService contentService;
    private ActiveMQConnectionService activeMQConnectionService;
    //private AuthenticationService authenticationService;
    transient private MutableAuthenticationService authenticationService;

    public Properties getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    public void setTenantAdminService(TenantAdminService tenantAdminService) {
        this.tenantAdminService = tenantAdminService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setActiveMQConnectionService(ActiveMQConnectionService activeMQConnectionService) {
        this.activeMQConnectionService = activeMQConnectionService;
    }

    public void setAuthenticationService(MutableAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    public boolean isEnabled() {
        return tenantAdminService.isEnabled();
    }

    public NodeRef getRootNodeRef(Collectivite c) {
        String cAdmin = tenantAdminService.getDomainUser("admin", c.getTenantDomain());
        StoreRef cStore = tenantService.getName(cAdmin, StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        return nodeService.getRootNode(cStore);
    }

    protected NodeRef getDictionaryHome(Collectivite c) {
        String xpath = configuration.getProperty("spaces.company_home.childname") + "/" + configuration.getProperty("spaces.dictionary.childname");
        List<NodeRef> nodes = searchService.selectNodes(getRootNodeRef(c), xpath, null, namespaceService, false);
        if (nodes.size() != 1) {
            throw new AlfrescoRuntimeException("Data dictionary not found for tenant" + c.getTenantDomain() + "...");
        }
        return nodes.get(0);
    }

    protected NodeRef getMetadataNodeRef(Collectivite c) {
        String xpath = configuration.getProperty("spaces.company_home.childname") +
                "/" + configuration.getProperty("spaces.dictionary.childname") +
                "/" + MultiCModel.NAME_COLLECTIVITE_INFOS.toPrefixString(namespaceService);
        List<NodeRef> nodes = searchService.selectNodes(getRootNodeRef(c), xpath, null, namespaceService, false);
        if (nodes.size() != 1) {
            return nodeService.createNode(getDictionaryHome(c), ContentModel.ASSOC_CONTAINS, MultiCModel.NAME_COLLECTIVITE_INFOS, MultiCModel.TYPE_COLLECTIVITE).getChildRef();
        }
        return nodes.get(0);
    }

    protected NodeRef getS2lowConfigNodeRef(Collectivite c) {
        String xpath = configuration.getProperty("spaces.company_home.childname") +
                "/" + configuration.getProperty("spaces.dictionary.childname") +
                "/" + configuration.getProperty("spaces.certificats.childname") +
                "/" + "cm:s2low_properties.xml";
        List<NodeRef> nodes = searchService.selectNodes(getRootNodeRef(c), xpath, null, namespaceService, false);
        if (nodes.size() != 1) {
            throw new AlfrescoRuntimeException("S2low config node ref not found for tenant " + c.getTenantDomain() + "...");
        }
        return nodes.get(0);
    }

    @Override
    public void createCollectivite(Collectivite c, char[] password) {
        Assert.hasText(c.getTenantDomain());

        /**
         * Ajout de controles sur le champ 'tenantDomain', cause à un bug connu
         * dans Alfresco 3.4.c (socle de i-Parapheur).
         * NB: c'est réglé dans la version 4 d'Alfresco.
         * Voir : https://issues.alfresco.com/jira/browse/MNT-7126
         *
         * De plus , certains administrateurs saisissent un peu n'importe quoi.
         *
         * Donc si syntaxe incorrecte ==> on logge mais on ne fait RIEN.
         */
        String tenant = c.getTenantDomain().trim();
        if (tenant.isEmpty()) {
            logger.error("**************************************************");
            logger.error("* tentative de creation de TENANT avec nom vide. *");
            logger.error("*  --> Abandon.                                  *");
            logger.error("**************************************************");
            return;
        }
        if (!tenant.matches("[a-z][0-9a-z\\-]*(\\.[0-9a-z\\-]+)*")) {
            logger.error("**************************************************");
            logger.error("* Creation de TENANT avec nom invalide = \'" + tenant + "\'");
            logger.error("*  --> Abandon.                                  *");
            logger.error("**************************************************");
            return;
        }

        // BLEX
        logger.info("tenantAdminService.createTenant(" + c.getTenantDomain() + ", " + Arrays.toString(password) + ");");

        tenantAdminService.createTenant(c.getTenantDomain(), password);

        NodeRef dictionary = getDictionaryHome(c);
        nodeService.createNode(dictionary, ContentModel.ASSOC_CONTAINS, MultiCModel.NAME_COLLECTIVITE_INFOS, MultiCModel.TYPE_COLLECTIVITE);
        nodeService.createNode(dictionary, ContentModel.ASSOC_CONTAINS, MultiCModel.NAME_COLLECTIVITE_PES_INFOS, MultiCModel.TYPE_PES_INFOS);

        updateCollectivite(c);

        Tenant currentTenant = tenantAdminService.getTenant(c.getTenantDomain());
        if (currentTenant != null) {
            try {
                activeMQConnectionService.createConsumer(currentTenant);
            } catch (Exception e) {
                logger.error(e.getLocalizedMessage());
            }
        }
    }

    @Override
    public void changeAdminPassword(Collectivite c, char[] newPassword) {
        authenticationService.setAuthentication(tenantService.getDomainUser("admin", c.getTenantDomain()), newPassword);
    }

    @Override
    public Collectivite getCollectivite(String tenantDomain) {
        Collectivite c = new Collectivite(tenantDomain);
        c.setEnabled(tenantAdminService.isEnabledTenant(tenantDomain));

        if (c.isEnabled()) {
            NodeRef metadataNodeRef = getMetadataNodeRef(c);
            c.setTitle((String) nodeService.getProperty(metadataNodeRef, MultiCModel.PROP_TITLE));
            c.setDescription((String) nodeService.getProperty(metadataNodeRef, MultiCModel.PROP_DESCRIPTION));
            c.setSiren((String) nodeService.getProperty(metadataNodeRef, MultiCModel.PROP_SIREN));
            c.setCity((String) nodeService.getProperty(metadataNodeRef, MultiCModel.PROP_CITY));
            c.setPostalCode((String) nodeService.getProperty(metadataNodeRef, MultiCModel.PROP_POSTAL_CODE));
            c.setCountry((String) nodeService.getProperty(metadataNodeRef, MultiCModel.PROP_COUNTRY));
        }

        return c;
    }

    @Override
    public List<Collectivite> getCollectivites() {
        List<Collectivite> collectivites = new ArrayList<Collectivite>();
        for (Tenant t : tenantAdminService.getAllTenants()) {
            Collectivite c = getCollectivite(t.getTenantDomain());
            collectivites.add(c);
        }
        return collectivites;
    }

    @Override
    public void enableCollectivite(Collectivite c) {
        tenantAdminService.enableTenant(c.getTenantDomain());
    }

    @Override
    public void disableCollectivite(Collectivite c) {
        tenantAdminService.disableTenant(c.getTenantDomain());
    }

    @Override
    public void updateCollectivite(Collectivite c) {
        NodeRef metadataNodeRef = getMetadataNodeRef(c);

        nodeService.setProperty(metadataNodeRef, MultiCModel.PROP_TITLE, c.getTitle());
        nodeService.setProperty(metadataNodeRef, MultiCModel.PROP_DESCRIPTION, c.getDescription());
        nodeService.setProperty(metadataNodeRef, MultiCModel.PROP_SIREN, c.getSiren());
        nodeService.setProperty(metadataNodeRef, MultiCModel.PROP_CITY, c.getCity());
        nodeService.setProperty(metadataNodeRef, MultiCModel.PROP_POSTAL_CODE, c.getPostalCode());
        nodeService.setProperty(metadataNodeRef, MultiCModel.PROP_COUNTRY, c.getCountry());
    }

    @Override
    public void deleteCollectivite(Collectivite c) {
        tenantAdminService.deleteTenant(c.getTenantDomain());
    }

    @Override
    public Map<String, String> getInfosPES(Collectivite c) {
        NodeRef s2low = getS2lowConfigNodeRef(c);

        try {
            ContentReader reader = contentService.getReader(s2low, ContentModel.PROP_CONTENT);
            Document doc = new SAXReader().read(reader.getContentInputStream());
            Element helios = doc.getRootElement().element("helios");

            Map<String, String> pesInfos = new HashMap<String, String>();
            pesInfos.put("policyIdentifierID", helios.element("pPolicyIdentifierID").getTextTrim());
            pesInfos.put("policyIdentifierDescription", helios.element("pPolicyIdentifierDescription").getTextTrim());
            pesInfos.put("policyDigest", helios.element("pPolicyDigest").getTextTrim());
            pesInfos.put("spuri", helios.element("pSPURI").getTextTrim());

            return pesInfos;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setInfosPES(Collectivite c, Map<String, String> infosPES) {
        NodeRef s2low = getS2lowConfigNodeRef(c);

        try {
            ContentReader reader = contentService.getReader(s2low, ContentModel.PROP_CONTENT);
            Document doc = new SAXReader().read(reader.getContentInputStream());
            Element helios = doc.getRootElement().element("helios");

            helios.element("pPolicyIdentifierID").setText(infosPES.get("policyIdentifierID"));
            helios.element("pPolicyIdentifierDescription").setText(infosPES.get("policyIdentifierDescription"));
            helios.element("pPolicyDigest").setText(infosPES.get("policyDigest"));
            helios.element("pSPURI").setText(infosPES.get("spuri"));

            ContentWriter writer = contentService.getWriter(s2low, ContentModel.PROP_CONTENT, true);
            writer.putContent(doc.asXML());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<String, Object> getStats(Collectivite c) {
        Map<String, Object> stats = new HashMap<String, Object>();

        stats.put("people", getPeople(c));
        stats.put("parapheurs", getParapheurs(c));
        stats.put("dossiers", getDossiers(c));
        stats.put("dossiersAArchiver", getDossiersAArchiver(c));
        stats.put("dossiersAArchiverDiskUsage", getDossiersAArchiverDiskUsage(c));
        stats.put("diskUsage", getDiskUsage(c));

        return stats;
    }

    @Override
    public void reloadModelsForTenant(String tenant) {
        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Void>() {
            @Override
            public Void doWork() throws Exception {
                try {
                    String xpath = "app:company_home/app:dictionary/app:email_templates/*[like(@cm:name, 'parapheur*')]";

                    List<NodeRef> templates = searchService.selectNodes(
                            nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                            xpath,
                            null,
                            namespaceService,
                            false);

                    for (NodeRef template : templates) {
                        InputStream is = getClass().getClassLoader().getResourceAsStream("alfresco/module/parapheur/bootstrap/" + nodeService.getProperty(template, ContentModel.PROP_NAME));
                        ContentWriter writer = contentService.getWriter(template, ContentModel.PROP_CONTENT, true);
                        writer.putContent(new ByteArrayInputStream(IOUtils.toByteArray(is)));
                    }

                } catch (Exception e) {
                    logger.error("Une erreur s'est produite lors du rechargement des templates");
                }
                return null;
            }
        }, "admin" + TenantService.SEPARATOR + tenant);
    }

    protected Set<NodeRef> getPeople(final Collectivite c) {
        RunAsWork<Set<NodeRef>> getPeople = new RunAsWork<Set<NodeRef>>() {

            @Override
            public Set<NodeRef> doWork() throws Exception {
                Set<NodeRef> people = personService.getAllPeople();

                // Remove guest from list
                Iterator<NodeRef> it = people.iterator();
                while (it.hasNext()) {
                    NodeRef person = it.next();
                    String username = (String) nodeService.getProperty(person, ContentModel.PROP_USERNAME);
                    username = tenantService.getBaseNameUser(username);
                    if ("guest".equals(username)) {
                        it.remove();
                    }
                }

                return people;
            }
        };

        return AuthenticationUtil.runAs(getPeople,
                                        tenantService.getDomainUser("admin", c.getTenantDomain()));
    }

    protected List<NodeRef> getDossiers(Collectivite c) {
        String xpath = "//*[subtypeOf('ph:dossier')]";
        return searchService.selectNodes(getRootNodeRef(c), xpath, null, namespaceService, false);
    }

    protected List<NodeRef> getParapheurs(Collectivite c) {
        String xpath = "//*[subtypeOf('ph:parapheur')]";
        return searchService.selectNodes(getRootNodeRef(c), xpath, null, namespaceService, false);
    }

    protected List<NodeRef> getDossiersAArchiver(Collectivite c) {
        String xpath = "//ph:a-archiver/*[subtypeOf('ph:dossier')]";
        return searchService.selectNodes(getRootNodeRef(c), xpath, null, namespaceService, false);
    }

    protected double getDiskUsage(Collectivite c) {
        String xpath = "//*[@cm:content]";
        List<NodeRef> documents = searchService.selectNodes(getRootNodeRef(c), xpath, null, namespaceService, false);
        return getDiskUsage(documents);
    }

    protected double getDossiersAArchiverDiskUsage(Collectivite c) {
        String xpath = "//ph:a-archiver//*[@cm:content]";
        List<NodeRef> documents = searchService.selectNodes(getRootNodeRef(c), xpath, null, namespaceService, false);
        return getDiskUsage(documents);
    }

    protected double getDiskUsage(List<NodeRef> documents) {
        double diskUsage = 0;
        for (NodeRef doc : documents) {
            ContentReader reader = contentService.getReader(doc, ContentModel.PROP_CONTENT);
            diskUsage += reader.getSize();
        }
        return diskUsage;
    }
}
