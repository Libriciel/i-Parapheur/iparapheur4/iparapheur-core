package org.adullact.iparapheur.repo.amq.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import org.adullact.iparapheur.repo.amq.MessagesSender;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.JMSException;
import javax.jms.QueueSender;
import javax.jms.TextMessage;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lhameury on 14/05/14.
 */
public class MessageSenderImpl implements MessagesSender {

    private final Logger logger = Logger.getLogger(MessagesSender.class);

    @Autowired
    private NodeService nodeService;
    @Autowired
    private ActiveMQConnectionServiceImpl activeMQConnectionService;
    @Autowired
    private TransactionService transactionService;

    private static final String WORKSPACE = "workspace://SpacesStore/";

    @Override
    public void sendWorker(String objString, String id) {
        //Récupération du NodeRef
        NodeRef nodeRef = new NodeRef(WORKSPACE + id);
        //Si le noeud possède l'aspect 'pending', on l'ignore
        if(!nodeService.hasAspect(nodeRef, ParapheurModel.ASPECT_PENDING)) {
            QueueSender sender = activeMQConnectionService.getSender();
            //Si la connexion est possible, envoi du JSON dans la MQ, sinon on pose le flag
            UserTransaction ut = transactionService.getNonPropagatingUserTransaction();
            try {
                ut.begin();
                Map<QName, Serializable> props = new HashMap<QName, Serializable>();
                props.put(ParapheurModel.PROP_LOCKED, true);
                nodeService.addAspect(nodeRef, ParapheurModel.ASPECT_PENDING, props);
                ut.commit();
                TextMessage m = activeMQConnectionService.createTextMessage();
                m.setText(objString);
                logger.info("-----Sending message : " + m.getText());
                sender.send(m);
            } catch (JMSException e) {
                logger.error("Exception when sending message to activemq : " + e.getMessage());
                nodeService.removeAspect(nodeRef, ParapheurModel.ASPECT_PENDING);
            } catch (Exception e2) {
                logger.error("Exception when sending message to activemq : " + e2.getMessage());
                try {
                    ut.rollback();
                } catch(Exception e3) {
                    logger.error("Exception when rollback after sending message to activemq : " + e3.getMessage());
                }
            }
        }
    }

    @Override
    public void sendWorker(String objString) {
        //Si le noeud possède l'aspect 'pending', on l'ignore
        QueueSender sender = activeMQConnectionService.getSender();
        //Si la connexion est possible, envoi du JSON dans la MQ, sinon on pose le flag
        try {
            TextMessage m = activeMQConnectionService.createTextMessage();
            m.setText(objString);
            sender.send(m);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
