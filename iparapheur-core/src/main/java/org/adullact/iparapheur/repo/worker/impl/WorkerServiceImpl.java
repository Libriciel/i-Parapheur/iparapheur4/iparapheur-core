package org.adullact.iparapheur.repo.worker.impl;

import com.atolcd.parapheur.model.ParapheurException;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import com.atolcd.parapheur.repo.impl.S2lowServiceImpl;
import coop.libriciel.crypto.service.SignatureHandlerProvider;
import org.adullact.iparapheur.repo.amq.MessagesSender;
import org.adullact.iparapheur.repo.attest.AttestService;
import org.adullact.iparapheur.repo.jscript.pastell.mailsec.MailsecPastellService;
import org.adullact.iparapheur.repo.notification.NotificationService;
import org.adullact.iparapheur.repo.notification.socket.SocketMessage;
import org.adullact.iparapheur.repo.notification.socket.SocketServer;
import org.adullact.iparapheur.repo.worker.SchedulerService;
import org.adullact.iparapheur.repo.worker.WorkerService;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.Assert;

import javax.transaction.UserTransaction;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static com.atolcd.parapheur.repo.DossierService.ACTION_DOSSIER;

public class WorkerServiceImpl implements WorkerService {

    private static Logger logger = Logger.getLogger(WorkerService.class);

    @Autowired
    @Qualifier("_dossierService")
    private DossierService dossierService;
    @Autowired
    private NodeService nodeService;
    @Autowired
    @Qualifier("_parapheurService")
    private ParapheurService parapheurService;
    @Autowired
    private CorbeillesService corbeillesService;
    @Autowired
    private S2lowService s2lowService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private SchedulerService schedulerService;
    @Autowired
    private AuthenticationComponent authenticationComponent;
    @Autowired
    private MessagesSender messagesSender;
    @Autowired
    private ContentService contentService;
    @Autowired
    private SocketServer socketServer;
    @Autowired
    private AttestService attestService;
    @Autowired
    private TypesService typesService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private MailsecPastellService mailsecPastellService;
    @Autowired
    private SignatureHandlerProvider signatureHandlerProvider;

    private String baseVerification(ACTION_DOSSIER action, NodeRef node, NodeRef... moreNodes) {
        if(moreNodes != null && moreNodes.length == 1 && moreNodes[0] != null) {
            return dossierActionBaseVerification(action, node, moreNodes[0]);
        }
        return "OK";
    }

    private String dossierActionBaseVerification(ACTION_DOSSIER action, NodeRef dossier, NodeRef bureauCourant) {
        String msg = "OK";
        String id = dossier.getId();
        if (!(nodeService.exists(bureauCourant) && parapheurService.isParapheur(bureauCourant))) {
            msg = "Le noeud d'id " + id + " n'existe pas ou n'est pas un bureau.";
        }
        if (!(nodeService.exists(dossier) && parapheurService.isDossier(dossier))) {
            msg = "Le noeud d'id " + id + " n'existe pas ou n'est pas un dossier.";
        }
        if(action != ACTION_DOSSIER.SUPPRESSION) {
            if (nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER) == null) {
                msg = "Le dossier d'id " + id + " ne possède pas de sous-type.";
            }
            if (nodeService.getProperty(dossier, ParapheurModel.PROP_UNCOMPLETE) != null && (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_UNCOMPLETE)) {
                msg = "Le dossier d'id " + id + " est incomplet.";
            }
        }
        if(!dossierService.getActions(dossier, bureauCourant).contains(action)) {
            msg = "L'action " + action + " est impossible sur le dossier d'id " + id + ".";
        }
        return msg;
    }

    @Override
    public Runnable getHandler(Map request) {
        ACTION_DOSSIER action = ACTION_DOSSIER.valueOf((String) request.get(ACTION));
        Runnable runnable = null;
        switch (action) {
            case REMORD:
                runnable = remorseHandler(request);
                break;
            case SUPPRESSION:
                runnable = deleteHandler(request);
                break;
            case RAZ:
                runnable = razHandler(request);
                break;
            case SECRETARIAT:
                runnable = secretariatHandler(request);
                break;
            case REJET:
                runnable = rejetHandler(request);
                break;
            case ARCHIVAGE:
                runnable = archiveHandler(request);
                break;
            case VISA:
                runnable = visaHandler(request);
                break;
            case TDT_ACTES:
                runnable = tdtActesHandler(request);
                break;
            case TDT_HELIOS:
                runnable = tdtHeliosHandler(request);
                break;
            case MAILSEC:
                runnable = mailsecHandler(request);
                break;
            case SIGNATURE:
                runnable = signatureHandler(request);
                break;
            case EMAIL:
                runnable = mailHandler(request);
                break;
            case TRANSFERT_ACTION:
                runnable = transfertHandler(request);
                break;
            case AVIS_COMPLEMENTAIRE:
                runnable = avisHandler(request);
                break;
            case ENCHAINER_CIRCUIT:
                break;
            case EDITION:
                break;
            case JOURNAL:
                break;
            case ENREGISTRER:
                break;
            case TRANSFORM:
                runnable = transformHandler(request);
                break;
            case GET_ATTEST:
                runnable = getAttestHandler(request);
                break;
            case SIGN_INFO:
                runnable = signInfoHandler(request);
                break;
            case CACHET:
                runnable = cachetHandler(request);
                break;
            case CORBEILLE_EVENT:
                runnable = eventHandler(request);
                break;
        }
        return runnable;
    }

    /**
     * Fonction spéciale pour l'envoi de mail étant donné que ça n'agit pas sur le dossier
     * @param request requête à traiter
     * @return runnable de l'envoi de mail
     */
    private Runnable mailHandler(final Map request) {
        final String DOSSIERS = "dossiers";
        final String ATTACHMENTS = "attachments";
        final String DESTINATAIRES = "destinataires";
        final String OBJET = "objet";
        final String MESSAGE = "message";
        final String ANNEXESINCLUDED = "annexesIncluded";
        final String INCLUDEFIRSTPAGE = "includeFirstPage";
        return new Runnable() {
            @Override
            public void run() {
                try {
                    final String username = (String) request.get(USERNAME);
                    AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<String>() {
                        @Override
                        public String doWork() throws Exception {
                            authenticationComponent.setCurrentUser(username);
                            List<NodeRef> dossiers = new ArrayList<NodeRef>();

                            //Arrivé ici, la requête minimale est forcément construite
                            if(request.containsKey(DOSSIERS) && request.get(DOSSIERS) instanceof List && request.containsKey(DESTINATAIRES) && request.get(DESTINATAIRES) instanceof List) {
                                //Minimum OK
                                List arrayDossiers = (List) request.get(DOSSIERS);
                                for (Object arrayDossier : arrayDossiers) {
                                    dossiers.add(new NodeRef(WORKSPACE + ((Map) arrayDossier).get(ID)));
                                }

                                List<NodeRef> attachmentsRef = new ArrayList<NodeRef>();
                                if(request.containsKey(ATTACHMENTS) && request.get(ATTACHMENTS) instanceof List) {
                                    List arrayAttachments = (List) request.get(ATTACHMENTS);
                                    for (Object arrayAttachment : arrayAttachments) {
                                        Boolean selected = (Boolean) ((Map) arrayAttachment).get("selected");
                                        if (selected == null || selected) {
                                            attachmentsRef.add(new NodeRef(WORKSPACE + ((Map) arrayAttachment).get(ID)));
                                        }
                                    }
                                }

                                List destinataires = (List) request.get(DESTINATAIRES);

                                String objet = "";
                                if(request.containsKey(OBJET) && request.get(OBJET) instanceof String) {
                                    objet = (String) request.get(OBJET);
                                }
                                String message = "";
                                if(request.containsKey(MESSAGE) && request.get(MESSAGE) instanceof String) {
                                    message = (String) request.get(MESSAGE);
                                }

                                Boolean annexesIncluded = true;
                                if(request.containsKey(ANNEXESINCLUDED)  && request.get(ANNEXESINCLUDED) instanceof Boolean) {
                                    annexesIncluded = (Boolean) request.get(ANNEXESINCLUDED);
                                }

                                Boolean includeFirstPage = true;
                                if(request.containsKey(INCLUDEFIRSTPAGE)  && request.get(INCLUDEFIRSTPAGE) instanceof Boolean) {
                                    includeFirstPage = (Boolean) request.get(INCLUDEFIRSTPAGE);
                                }
                                //Vérification OK
                                dossierService.sendMail(dossiers, destinataires, objet, message, attachmentsRef, annexesIncluded, includeFirstPage);

                                for(NodeRef dossierRef: dossiers) {
                                    // Audit this mail in dossier history
                                    List<Object> list = new ArrayList<Object>();
                                    list.add(nodeService.getProperty(dossierRef, ParapheurModel.PROP_STATUS_METIER));
                                    StringBuilder auditString = new StringBuilder("Envoi du document par mail à : ");
                                    boolean isFirst = true;
                                    for(Object s : destinataires) {
                                        if(isFirst) isFirst = false;
                                        else auditString.append(", ");
                                        auditString.append(s);
                                    }
                                    parapheurService.auditWithNewBackend("ParapheurServiceCompat", auditString.toString(), dossierRef, list);
                                }
                            } else {
                                //TODO ERROR HANDLER
                            }
                            return "OK";
                        }
                    }, username);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * Fonction pour l'envoi par mail sécurisé sur S2low
     * @param request requête à traiter
     * @return runnable de l'envoi de mail
     */
    private Runnable mailsecHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.MAILSEC, request, new DossierAction() {

            List<String> destinataires;
            List<String> destinatairesCC;
            List<String> destinatairesCCI;
            String objet;
            String message;
            String password;
            boolean showPass;
            boolean includeFirstPage;
            List<NodeRef> includedAnnexes = new ArrayList<NodeRef>();

            String username;
            String fullname;

            String dossierName;
            String body;
            String subject;

            @Override
            public boolean before() {

                username = (String) request.get(USERNAME);
                fullname = parapheurService.getPrenomNomFromLogin(username);

                destinataires = (List<String>) request.get(DESTINATAIRES);
                destinataires = destinataires.isEmpty() ? null: destinataires;
                if(request.containsKey(DESTINATAIRESCC)) {
                    destinatairesCC = (List<String>) request.get(DESTINATAIRESCC);
                    destinatairesCC = destinatairesCC.isEmpty() ? null: destinatairesCC;
                }
                if(request.containsKey(DESTINATAIRESCCI)) {
                    destinatairesCCI = (List<String>) request.get(DESTINATAIRESCCI);
                    destinatairesCCI = destinatairesCCI.isEmpty() ? null: destinatairesCCI;
                }

                objet = (String) request.get(OBJET);
                message = (String) request.get(MESSAGE);
                password = (String) request.get(PASSWORD);
                showPass = (Boolean) request.get(SHOWPASS);

                if(request.containsKey(ATTACHMENTS)) {
                    List<Map> annexes = (List) request.get(ATTACHMENTS);

                    if(annexes != null) {
                        for (Map annexe : annexes) {
                            if((Boolean) annexe.get("selected")) {
                                includedAnnexes.add(new NodeRef(WORKSPACE + annexe.get("id")));
                            }
                        }
                    }
                }

                includeFirstPage = (Boolean) request.get(INCLUDEFIRSTPAGE);

                dossierName = (String) nodeService.getProperty(node, ContentModel.PROP_TITLE);

                body = (!message.isEmpty()) ? message : s2lowService.getSecureMailTemplate(node);
                subject = (!objet.isEmpty()) ? objet :  "Dossier : " + dossierName;

                return true;
            }

            @Override
            public boolean call() throws Exception {

                String action = dossierService.getActionDemandee(node);

                if(action.equals(EtapeCircuit.ETAPE_MAILSEC)) {
                    //FIXME Do something with this ID ?
                    int mail_id = s2lowService.sendSecureMail(destinataires, destinatairesCC, destinatairesCCI, subject, body, password, showPass, node, includedAnnexes, includeFirstPage);
                } else if (action.equals(EtapeCircuit.ETAPE_MAILSEC_PASTELL)) {
                    mailsecPastellService.sendMailsec(destinataires, destinatairesCC, destinatairesCCI, subject, body, node, includedAnnexes, includeFirstPage);
                }

                if (parapheurService.isParapheurSecretaire(bureauCourant, username)) {
                    fullname = String.format("%s pour le compte de \"%s\"", fullname, parapheurService.getNomParapheur(bureauCourant));
                }

                parapheurService.setSignataire(node, fullname);

                return false;
            }

            @Override
            public void after() {
                notificationService.notifierPourTdT(node, bureauCourant);

                if (dossierService.isCurrentTDTEtapeAuto(node)) {
                    sendTdtByWorker();
                }
                if (dossierService.isCurrentCachetEtapeAuto(node)) {
                    stampByWorker();
                }
            }
        });
    }

    private Runnable visaHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.VISA, request, new DossierAction() {

            boolean consecutiveSteps;
            boolean isEmis;
            String username;
            String fullname;
            String pub;
            String priv;
            String actionDemandee;

            @Override
            public boolean before() {
                List<EtapeCircuit> circuit = parapheurService.getCircuit(node);

                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(circuit);

                if (etape == null) {
                    errorMsg = "Le dossier d'id " + node.getId() + " n'a pas d'étape courrante.";
                    return false;
                }

                if(etape.getActionDemandee().equals(EtapeCircuit.ETAPE_SIGNATURE) &&
                        Boolean.FALSE.equals(nodeService.getProperty(node, ParapheurModel.PROP_SIGNATURE_PAPIER))) {
                    errorMsg = "Le dossier doit être signé.";
                    return false;
                }

                if(((String)nodeService.getProperty(node, ParapheurModel.PROP_SIGNATURE_FORMAT)).contains("XAdES") &&
                        !(nodeService.getProperty(node, ParapheurModel.PROP_SIGNATURE_FORMAT)).equals("XAdES/detached") &&
                        !(nodeService.getProperty(node, ParapheurModel.PROP_SIGNATURE_FORMAT)).equals("XAdES132/detached")) {
                    try {
                        NodeRef document = parapheurService.getMainDocuments(node).get(0);
                        if(nodeService.getProperty(document, ParapheurModel.PROP_VISUEL_PDF) == null) {
                            errorMsg = "Le dossier d'id " + node.getId() + " n'a pas de visuel PDF.";
                            return false;
                        }
                    } catch(Exception e) {
                        //Nothing
                    }
                } else if((nodeService.getProperty(node, ParapheurModel.PROP_SIGNATURE_FORMAT)).equals("XAdES/detached") ||
                        (nodeService.getProperty(node, ParapheurModel.PROP_SIGNATURE_FORMAT)).equals("XAdES132/detached")) {
                    NodeRef document = parapheurService.getMainDocuments(node).get(0);
                    ContentReader reader = contentService.getReader(document, ContentModel.PROP_CONTENT);
                    if(reader.getContentData().getMimetype().equals("application/xml")) {
                        // check visual presence
                        if(nodeService.getProperty(document, ParapheurModel.PROP_VISUEL_PDF) == null) {
                            errorMsg = "Le dossier d'id " + node.getId() + " n'a pas de visuel PDF.";
                            return false;
                        }
                    }
                }

                isEmis = parapheurService.isEmis(node);

                actionDemandee = etape.getActionDemandee();

                username = (String) request.get(USERNAME);
                fullname = parapheurService.getPrenomNomFromLogin(username);
                consecutiveSteps = request.containsKey(CONSECUTIVE) ? (Boolean) request.get(CONSECUTIVE) : false;
                pub = request.containsKey(ANNOTPUB) ? (String) request.get(ANNOTPUB) : "";
                priv = request.containsKey(ANNOTPRIV) ? (String) request.get(ANNOTPRIV) : "";
                return true;
            }

            @Override
            public boolean call() throws Exception {
                if (!isEmis) {
                    dossierService.finalizeDossier(node);
                }

                int nbSteps = (consecutiveSteps) ? dossierService.getConsecutiveSteps(node, username).size() : 1;
                for (int i = 0; i < nbSteps; i++) {
                    if (i == (nbSteps - 1)) { // We put the annotation only on the last step
                        if(actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC) && !nodeService.getProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_STATUS).equals(S2lowServiceImpl.STATUS_MAILSEC_FULLY_CONFIRMED)) {
                            Integer mail_id = (Integer) nodeService.getProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_ID);

                            S2lowServiceImpl.SecureMailDetail detail = null;
                            try {
                                detail = s2lowService.getSecureMailDetail(mail_id);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            String mailsecDigest = S2lowServiceImpl.getAnnotation(detail);
                            if(!pub.isEmpty()) mailsecDigest += "\n" + pub;
                            parapheurService.setAnnotationPublique(node, mailsecDigest);
                        } else if(actionDemandee.equalsIgnoreCase(EtapeCircuit.ETAPE_MAILSEC_PASTELL)  && !nodeService.getProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_STATUS).equals(S2lowServiceImpl.STATUS_MAILSEC_FULLY_CONFIRMED)) {
                            parapheurService.setAnnotationPublique(node, mailsecPastellService.getAnnotationFromEvents(mailsecPastellService.getDossierEvents(node)));
                        } else {
                            parapheurService.setAnnotationPublique(node, pub);
                            parapheurService.setAnnotationPrivee(node, priv);
                        }
                    }

                    // Le signataire enregistré doit être le propriétaire du parapheur (cas de signature papier)
                    NodeRef parapheurRef = parapheurService.getParentParapheur(node);

                    String signataire;
                    if (parapheurService.isParapheurSecretaire(parapheurRef, username)) {
                        //NodeRef ownerRef = this.personService.getPerson();
                        signataire = String.format("%s pour le compte de \"%s\"", fullname, parapheurService.getNomParapheur(parapheurRef));
                    } else if (parapheurService.isParapheurDelegate(parapheurRef, username) && !parapheurService.isParapheurOwner(parapheurRef, username)) {
                        signataire =  String.format(parapheurService.getConfiguration().getProperty("parapheur.suppleance.text"), fullname + ", " + parapheurService.getNomParapheur(bureauCourant), parapheurService.getNomParapheur(parapheurRef));
                    } else {
                        signataire = fullname;
                    }

                    parapheurService.setSignataire(node, signataire);

                    parapheurService.approveV4(node, bureauCourant);

                    corbeillesService.moveDossier(node, parentCourant, nodeService.getPrimaryParent(node).getParentRef());
                }

                return true;
            }

            @Override
            public void after() {
                UserTransaction ut = transactionService.getUserTransaction();
                try {
                    ut.begin();
                    if(parapheurService.isTermine(node) &&
                            typesService.hasToAttestSignature(
                                    (String) nodeService.getProperty(node, ParapheurModel.PROP_TYPE_METIER),
                                    (String) nodeService.getProperty(node, ParapheurModel.PROP_SOUSTYPE_METIER)) &&
                            attestService.isServiceEnabled()) {
                        getAttestByWorker(node);
                    }
                    ut.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dossierService.isCurrentTDTEtapeAuto(node)) {
                    sendTdtByWorker();
                }
                if (dossierService.isCurrentCachetEtapeAuto(node)) {
                    stampByWorker();
                }
                if(dossierService.isNextEtapeXadesSig(node)) {
                    handleHashSignature();
                }
            }
        });
    }

    private Runnable rejetHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.REJET, request, new DossierAction() {

            String annotPub;
            String annotPriv;

            @Override
            public boolean before() {
                annotPub = (String) request.get(ANNOTPUB);
                annotPriv = request.containsKey(ANNOTPRIV) ? (String) request.get(ANNOTPRIV) : "";
                return true;
            }

            @Override
            public boolean call() throws Exception {
                parapheurService.setSignataire(node,
                        parapheurService.getPrenomNomFromLogin(
                                AuthenticationUtil.getRunAsUser()));

                parapheurService.setAnnotationPrivee(node, annotPriv);
                parapheurService.setAnnotationPublique(node, annotPub);
                parapheurService.reject(node);
                corbeillesService.moveDossier(node, parentCourant, nodeService.getPrimaryParent(node).getParentRef());

                return true;
            }
        });
    }

    private Runnable archiveHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.ARCHIVAGE, request, new DossierAction() {

            List<NodeRef> annexesRef = new ArrayList<NodeRef>();
            String titreArchive;

            @Override
            public boolean before() {
                titreArchive = (String) request.get(TITREARCHIVE);

                //TODO : Handle annexes array or boolean
                if(request.containsKey(ANNEXESINCLUDED)) {
                    boolean isAnnexesIncluded = (Boolean) request.get(ANNEXESINCLUDED);
                    if(isAnnexesIncluded) {
                        annexesRef = parapheurService.getAttachments(node);
                    }
                } else if(request.containsKey(ANNEXES)) {
                    List<String> annexes = (List<String>) request.get(ANNEXES);

                    if(annexes != null) {
                        for (String annexe : annexes) {
                            annexesRef.add(new NodeRef(WORKSPACE + annexe));
                        }
                    }
                }
                return true;
            }

            @Override
            public boolean call() throws Exception {
                parapheurService.archiver(node, titreArchive, annexesRef);
                return true;
            }

            @Override
            public void after() {
                notificationService.sendPreparedNotificationsForDossier(node.getId());
                // Next, send deletion
                JSONStringer messageJsonStringer = new JSONStringer();
                try {
                    messageJsonStringer.object();
                    messageJsonStringer.key(WorkerService.ID).value(node.getId());
                    messageJsonStringer.key(WorkerService.USERNAME).value(AuthenticationUtil.getRunAsUser());
                    messageJsonStringer.key(WorkerService.ACTION).value(ACTION_DOSSIER.SUPPRESSION);
                    messageJsonStringer.key(WorkerService.TYPE).value(WorkerService.TYPE_DOSSIER);
                    messageJsonStringer.key(WorkerService.NOTIFME).value(false);
                    messageJsonStringer.endObject();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                messagesSender.sendWorker(messageJsonStringer.toString(), node.getId());
            }
        });
    }

    private Runnable deleteHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.SUPPRESSION, request, new DossierAction() {

            boolean notify;

            @Override
            public boolean before() {
                dossierNotDeleted = false;
                notify = request.containsKey(NOTIFME) ? (Boolean) request.get(NOTIFME) : true;
                return true;
            }

            @Override
            public boolean call() throws Exception {
                dossierService.deleteDossier(node, notify);
                return true;
            }
        });
    }

    private Runnable razHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.RAZ, request, new DossierAction() {
            @Override
            public boolean before() {
                return true;
            }

            @Override
            public boolean call() throws Exception {
                parapheurService.reprendreDossier(node);
                corbeillesService.moveDossier(node, parentCourant, nodeService.getPrimaryParent(node).getParentRef());
                return true;
            }
        });
    }

    private Runnable tdtActesHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.TDT_ACTES, request, new DossierAction() {

            String annotPub;
            String annotPriv;
            Date dateActes;
            String objet;
            String nature;
            String classification;
            String numero;

            @Override
            public boolean before() {
                annotPub = (String) request.get(ANNOTPUB);
                annotPriv = (String) request.get(ANNOTPRIV);
                objet = (String) request.get(OBJET);
                nature = (String) request.get(NATURE);
                classification = (String) request.get(CLASSIFICATION);
                numero = (String) request.get(NUMERO);

                dateActes = new Date();
                dateActes.setTime(((Double) request.get(DATEACTES)).longValue());

                return true;
            }

            @Override
            public boolean call() throws Exception {
                dossierService.sendTdtActes(bureauCourant, node, nature, classification, numero, dateActes, objet, annotPub, annotPriv);
                return false;
            }

            @Override
            public void after() {
                notificationService.notifierPourTdT(node, bureauCourant);
            }
        });
    }

    private Runnable cachetHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.CACHET, request, new DossierAction() {

            String annotPub;
            String annotPriv;
            boolean automatic;

            @Override
            public boolean before() {
                annotPub = (String) request.get(ANNOTPUB);
                annotPriv = (String) request.get(ANNOTPRIV);
                automatic = (boolean) request.get(AUTOMATIC);
                return true;
            }

            @Override
            public boolean call() throws Exception {
                dossierService.doStamp(bureauCourant, node, annotPub, annotPriv, automatic);
                parapheurService.getMainDocuments(node).forEach(d -> dossierService.deleteCustomSignatureFromDocument(node, d));
                corbeillesService.moveDossier(node, parentCourant, nodeService.getPrimaryParent(node).getParentRef());
                return false;
            }
        });
    }

    private Runnable tdtHeliosHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.TDT_HELIOS, request, new DossierAction() {

            String annotPub;
            String annotPriv;

            @Override
            public boolean before() {
                annotPub = (String) request.get(ANNOTPUB);
                annotPriv = (String) request.get(ANNOTPRIV);
                return true;
            }

            @Override
            public boolean call() throws Exception {
                dossierService.sendTdtHelios(bureauCourant, node, annotPub, annotPriv);
                return false;
            }

            @Override
            public void after() {
                notificationService.notifierPourTdT(node, bureauCourant);
            }
        });
    }

    private Runnable remorseHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.REMORD, request, new DossierAction() {
            @Override
            public boolean before() {
                return true;
            }

            @Override
            public boolean call() throws Exception {
                parapheurService.recupererDossier(node);
                corbeillesService.moveDossier(node, parentCourant, nodeService.getPrimaryParent(node).getParentRef());

                return true;
            }
        });
    }

    private Runnable secretariatHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.SECRETARIAT, request, new DossierAction() {
            public boolean isInSecretariat;

            String annotPub;
            String annotPriv;

            @Override
            public boolean before() {
                isInSecretariat = nodeService.hasAspect(node, ParapheurModel.ASPECT_SECRETARIAT);
                annotPub = (String) request.get(ANNOTPUB);
                annotPriv = (String) request.get(ANNOTPRIV);
                return true;
            }

            @Override
            public boolean call() throws Exception {
                parapheurService.setSignataire(node,
                        parapheurService.getPrenomNomFromLogin(
                                AuthenticationUtil.getRunAsUser()));
                parapheurService.setAnnotationPrivee(node, annotPriv);
                parapheurService.setAnnotationPublique(node, annotPub);
                parapheurService.secretariat(node);
                corbeillesService.moveDossier(node, parentCourant, nodeService.getPrimaryParent(node).getParentRef());

                return true;
            }
        });
    }

    private Runnable signatureHandler(final Map<?, ?> request) {
        return handleAction(ACTION_DOSSIER.SIGNATURE, request, new DossierAction() {

            String annotPub;
            String annotPriv;
            Map<String, Serializable> signature;
            String certificate;

            @Override
            public boolean before() {
                annotPub = (String) request.get(ANNOTPUB);
                annotPriv = (String) request.get(ANNOTPRIV);
                signature = (Map<String, Serializable>) request.get(SIGNATURE);
                certificate = (String) request.get(CERTIFICATE);
                return true;
            }

            @Override
            public boolean call() throws Exception {
                List<String> signatures = (List<String>) signature.get(SIGNATURE);
                List<Long> signatureDateTime = ((List<Double>) signature.get(SIGNATUREDATETIME)).stream().map(Double::longValue).collect(Collectors.toList());
                signatureHandlerProvider
                        .getService((String) nodeService.getProperty(node, ParapheurModel.PROP_SIGNATURE_FORMAT))
                        .doSignFolder(node, bureauCourant, signatures, signatureDateTime, certificate);
                parapheurService.setAnnotationPublique(node, annotPub);
                parapheurService.setAnnotationPrivee(node, annotPriv);
                parapheurService.approveV4(node, bureauCourant);
                corbeillesService.moveDossier(node, parentCourant, nodeService.getPrimaryParent(node).getParentRef());
                return true;
            }

            @Override
            public void after() {
                UserTransaction ut = transactionService.getUserTransaction();
                try {
                    ut.begin();
                    if(parapheurService.isTermine(node) &&
                            typesService.hasToAttestSignature(
                                    (String) nodeService.getProperty(node, ParapheurModel.PROP_TYPE_METIER),
                                    (String) nodeService.getProperty(node, ParapheurModel.PROP_SOUSTYPE_METIER)) &&
                            attestService.isServiceEnabled()) {
                        getAttestByWorker(node);
                    }
                    ut.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dossierService.isCurrentTDTEtapeAuto(node)) {
                    sendTdtByWorker();
                }
                if (dossierService.isCurrentCachetEtapeAuto(node)) {
                    stampByWorker();
                }
            }
        });
    }

    private Runnable getAttestHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.GET_ATTEST, request, new DossierAction() {

            @Override
            public boolean call() throws Exception {
                if(attestService.isServiceEnabled()) {
                    dossierNotDeleted = !attestService.envoiGetAttest(node);
                }
                return true;
            }

            @Override
            public boolean before() {
                dossierNotDeleted = false;
                return true;
            }

            @Override
            public void after() {
                if (dossierService.isCurrentTDTEtapeAuto(node)) {
                    sendTdtByWorker();
                }
                if (dossierService.isCurrentCachetEtapeAuto(node)) {
                    stampByWorker();
                }
            }
        });
    }

    private Runnable transfertHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.TRANSFERT_ACTION, request, new DossierAction() {

            String annotPub;
            String annotPriv;
            String nextAction;
            Boolean notifMe;
            NodeRef bureauCible;

            @Override
            public boolean before() {
                annotPub = (String) request.get(ANNOTPUB);
                annotPriv = (String) request.get(ANNOTPRIV);
                notifMe = request.containsKey(NOTIFME) && (Boolean) request.get(NOTIFME);
                bureauCible = new NodeRef(WORKSPACE + request.get(BUREAUCIBLE));
                nextAction = (String) request.get(NEXTACTION);
                return true;
            }

            @Override
            public boolean call() throws Exception {
                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(node);
                NodeRef etapeRef = etape.getNodeRef();
                NodeRef parapheur = parapheurService.getParentParapheur(node);
                String user = AuthenticationUtil.getRunAsUser();
                Assert.isTrue(etape.getActionDemandee().equals(nextAction), "L'étape courante n'est pas une étape de " + nextAction);
                Assert.isTrue(parapheurService.isParapheurOwnerOrDelegate(parapheur, user), "L'utilisateur ne fait pas parti des proprietaires ou délégués du bureau.");

                EtapeCircuitImpl nouvelleEtape = new EtapeCircuitImpl();
                nouvelleEtape.setActionDemandee(nextAction);
                nouvelleEtape.setParapheur(bureauCible);
                nouvelleEtape.setListeMetadatas(etape.getListeMetadatas());
                nouvelleEtape.setListeMetadatasRefus(etape.getListeMetadatasRefus());

                NodeRef newEtape = parapheurService.insertEtapeAfter(etapeRef, nouvelleEtape);

                List<NodeRef> notifies = parapheurService.getListeNotification(node);
                if(notifies != null) {
                    if(notifMe) {
                        if (!notifies.contains(bureauCourant)) {
                            notifies.add(bureauCourant);
                        }
                    }
                    if(notifies.contains(bureauCible)) {
                        notifies.remove(bureauCible);
                    }
                } else {
                    notifies = new ArrayList<NodeRef>();
                }


                if(nodeService.hasAspect(etapeRef, ParapheurModel.ASPECT_ETAPE_COMPLEMENTAIRE)) {
                    nodeService.addAspect(newEtape, ParapheurModel.ASPECT_ETAPE_COMPLEMENTAIRE, null);
                }


                nodeService.setProperty(etapeRef, ParapheurModel.PROP_LISTE_NOTIFICATION, new ArrayList<NodeRef>());
                nodeService.setProperty(newEtape, ParapheurModel.PROP_LISTE_NOTIFICATION, new ArrayList<NodeRef>(notifies));


                nodeService.setProperty(etapeRef, ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);

                parapheurService.setAnnotationPrivee(node, annotPriv);
                String annotation = "Transfert de l'action de " + nextAction.toLowerCase() + "." + (annotPub != null && !annotPub.isEmpty() ?  " Commentaire :\n" + annotPub : "");
                parapheurService.setAnnotationPublique(node, annotation);
                parapheurService.setSignataire(node,
                        parapheurService.getPrenomNomFromLogin(
                                AuthenticationUtil.getRunAsUser()));

                dossierService.setPermissionsDossier(node);

                parapheurService.approve(node);

                corbeillesService.moveDossier(node, parentCourant, nodeService.getPrimaryParent(node).getParentRef());

                return false;
            }
        });
    }

    private Runnable transformHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.TRANSFORM, request, new DocumentAction() {

            ContentWriter pdfwriter = null;
            ContentReader reader = null;

            @Override
            public boolean before() {
                return true;
            }

            private void socketNotify(String state) {
                SocketMessage message = new SocketMessage();
                message.setUsers(SocketServer.getUsers());
                JSONObject msg = new JSONObject();
                try {
                    msg.put("id", node.getId());
                    msg.put("action", action.name());
                    msg.put("state", state);
                    msg.put("pageCount", 0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                message.setMsg(msg);
                socketServer.notify(message);
            }

            @Override
            public void after() {
                socketNotify(SocketMessage.State.END.name());
            }

            @Override
            public void error() {
                try {
                    reader.getContentInputStream().close();
                    pdfwriter.getContentOutputStream().close();
                } catch(Exception e) {
                    //Do nothing
                }

                socketNotify(SocketMessage.State.ERROR.name());
            }

            @Override
            public boolean call() throws Exception {
                pdfwriter = contentService.getWriter(node, ParapheurModel.PROP_VISUEL_PDF, Boolean.TRUE);
                pdfwriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
                pdfwriter.setEncoding("UTF-8");
                reader = contentService.getReader(node, ContentModel.PROP_CONTENT);
                if(request.containsKey("mimetype")) {
                    reader.setMimetype((String) request.get("mimetype"));
                }
                reader.setEncoding("UTF-8");
                contentService.transform(reader, pdfwriter);
                return false;
            }
        });
    }

    private Runnable eventHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.CORBEILLE_EVENT, request, new DocumentAction() {
            NodeRef parentRef = null;
            NodeRef oldParentRef = null;
            @Override
            public boolean before() {
                if(request.containsKey(WorkerService.PARENT)) {
                    parentRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, (String) request.get(WorkerService.PARENT));
                }
                if(request.containsKey(WorkerService.OLD_PARENT)) {
                    oldParentRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, (String) request.get(WorkerService.OLD_PARENT));
                }
                return true;
            }

            private void onDossierCreated() {
                Integer currentCount = (Integer) nodeService.getProperty(parentRef, ParapheurModel.PROP_CHILD_COUNT);
                nodeService.setProperty(parentRef, ParapheurModel.PROP_CHILD_COUNT, currentCount + 1);
            }

            private void onDossierMoved() {
//                corbeillesService.insertIntoCorbeillesVirtuelles(node);
//
//                Integer currentCount = (Integer) nodeService.getProperty(oldParentRef, ParapheurModel.PROP_CHILD_COUNT);
//                nodeService.setProperty(oldParentRef, ParapheurModel.PROP_CHILD_COUNT, currentCount - 1);
//
//                currentCount = (Integer) nodeService.getProperty(parentRef, ParapheurModel.PROP_CHILD_COUNT);
//                nodeService.setProperty(parentRef, ParapheurModel.PROP_CHILD_COUNT, currentCount + 1);
            }

            private void onDossierDeleted() {
                NodeRef corbeille = parapheurService.getParentCorbeille(parentRef);
                Integer currentCount = (Integer) nodeService.getProperty(corbeille, ParapheurModel.PROP_CHILD_COUNT);
                nodeService.setProperty(corbeille, ParapheurModel.PROP_CHILD_COUNT, currentCount - 1);
            }

            private void onDossierAddAspect() {
                corbeillesService.removeDossierFromRecuperables(node);
            }

            @Override
            public boolean call() throws Exception {
                switch ((String)request.get(WorkerService.EVENT)) {
                    case "onDossierCreated":
                        onDossierCreated();
                        break;
                    case "onDossierMoved":
                        onDossierMoved();
                        break;
                    case "onDossierDeleted":
                        onDossierDeleted();
                        break;
                    case "onDossierAddAspect":
                        onDossierAddAspect();
                        break;
                    default:
                        logger.error("Action does not exists");
                }
                return false;
            }
        });
    }

    private Runnable avisHandler(final Map request) {
        return handleAction(ACTION_DOSSIER.AVIS_COMPLEMENTAIRE, request, new DossierAction() {

            String annotPub;
            String annotPriv;
            NodeRef bureauCible;

            @Override
            public boolean before() {
                annotPub = (String) request.get(ANNOTPUB);
                annotPriv = (String) request.get(ANNOTPRIV);
                bureauCible = new NodeRef(WORKSPACE + request.get(BUREAUCIBLE));
                return true;
            }

            @Override
            public boolean call() throws Exception {
                EtapeCircuit previous = parapheurService.getPreviousEtapeCircuit(node);
                EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(node);
                NodeRef parapheur = parapheurService.getParentParapheur(node);

                String user = AuthenticationUtil.getRunAsUser();
                Assert.notNull(previous, "Impossible de demander un avis complémentaire quand le dossier n'est pas émis.");
                Assert.isTrue(parapheurService.isParapheurOwnerOrDelegate(parapheur, user), "L'utilisateur ne fait pas parti des proprietaires ou délégués du bureau.");

                ((EtapeCircuitImpl)etape).setActionDemandee(EtapeCircuit.ETAPE_VISA);
                NodeRef newStepRef = parapheurService.insertEtapeAfter(previous.getNodeRef(), etape);


                EtapeCircuitImpl nouvelleEtape = new EtapeCircuitImpl();
                nouvelleEtape.setActionDemandee(EtapeCircuit.ETAPE_VISA);
                nouvelleEtape.setParapheur(bureauCible);

                NodeRef avisStepRef = parapheurService.insertEtapeAfter(newStepRef, nouvelleEtape);
                nodeService.addAspect(avisStepRef, ParapheurModel.ASPECT_ETAPE_COMPLEMENTAIRE, null);


                parapheurService.setAnnotationPrivee(node, annotPriv);

                String annotation = "Demande d'avis complémentaire." + (annotPub != null && !annotPub.isEmpty() ?  " Commentaire :\n" + annotPub : "");
                parapheurService.setAnnotationPublique(node, annotation);
                parapheurService.setSignataire(node,
                        parapheurService.getPrenomNomFromLogin(
                                AuthenticationUtil.getRunAsUser()));

                dossierService.setPermissionsDossier(node);

                parapheurService.approve(node);

                corbeillesService.moveDossier(node, parentCourant, nodeService.getPrimaryParent(node).getParentRef());
                return false;
            }
        });
    }

    private Runnable signInfoHandler(final Map request) {

        return handleAction(ACTION_DOSSIER.SIGN_INFO, request, new DossierAction() {

            private NodeRef dossierCible;
            private NodeRef bureauCible;
            private Map<String, String> infos;

            @Override
            public boolean before() {

                dossierCible = new NodeRef(WORKSPACE + request.get(ID));
                bureauCible = new NodeRef(WORKSPACE + request.get(BUREAUCIBLE));

                return true;
            }

            @Override
            @SuppressWarnings("unchecked")
            public boolean call() throws Exception {

                dossierService.generateSignatureInformations(dossierCible, bureauCible);

                return false;
            }

            @Override
            public void after() { }
        });
    }

    private abstract class Action {
        Map request = null;
        NodeRef node = null;

        ACTION_DOSSIER action;

        String errorMsg = null;
        int errorCode = -1;

        public void init(Map request, ACTION_DOSSIER action, NodeRef node, NodeRef... moreNodes) {
            this.request = request;
            this.node = node;
            this.action = action;
        }

        public abstract boolean before();

        public boolean internBefore() {
            return this.before();
        }

        /**
         * Fonction appelée à la fin de l'exécution d'une action, permet l'enchainement d'étapes
         * automatiques par exemple. Par défaut, ne fait rien.
         */
        public void after() {}

        public void error() {}

        public void success() {}

        public void internSuccess() throws Exception {
            this.success();
        }

        public boolean execute() throws Exception {
            boolean ret = this.call();
            nodeService.removeAspect(node, ParapheurModel.ASPECT_PENDING);
            return ret;
        }

        public boolean hasToRemoveAspectFromNode(boolean error) {
            return node != null && nodeService.exists(node) && nodeService.hasAspect(node, ParapheurModel.ASPECT_PENDING);
        }

        /**
         * Fonction appelée pour l'exécution de l'action. Renvoie vrai si le node a bougé (moveNode),
         * le retour n'est pas encore utilisé.
         * @return vrai si l'action a bouger le node, faux sinon.
         * @throws Exception
         */
        public abstract boolean call() throws Exception;
    }

    private abstract class DocumentAction extends Action {

        Exception exceptionDuringExecute = null;

        public void init(Map request, ACTION_DOSSIER action, NodeRef node, NodeRef... moreNodes) {
            super.init(request, action, node);
        }

        public boolean internBefore() {
            return this.before();
        }

        public void internSuccess() throws Exception {
            if(this.exceptionDuringExecute == null) {
                this.success();
            } else {
                throw this.exceptionDuringExecute;
            }
        }

        public boolean execute() throws Exception {
            boolean ret = false;
            ret = this.call();
            if(nodeService.exists(node)) {
                nodeService.removeAspect(node, ParapheurModel.ASPECT_PENDING);
            }
            return ret;
        }

        public boolean hasToRemoveAspectFromNode(boolean error) {
            return super.hasToRemoveAspectFromNode(error);
        }
    }

    private abstract class DossierAction extends Action {
        NodeRef bureauCourant = null;
        NodeRef bureauCible = null;
        String title;
        NodeRef parentCourant;
        NodeRef parentCible;
        List<String> banettesCourant;
        List<String> banettesCible;

        boolean dossierNotDeleted = true;

        public void init(Map request, ACTION_DOSSIER action, NodeRef node, NodeRef... moreNodes) {
            super.init(request, action, node);
            this.bureauCourant = moreNodes[0];
            this.title = (String) nodeService.getProperty(node, ContentModel.PROP_TITLE);
        }

        public boolean internBefore() {
            boolean ret = this.before();
            this.updateBanettesCourant(bureauCourant);
            return ret;
        }

        public void error() {
            if (errorCode != -1) {
                notificationService.notifierApresErreur(node, String.valueOf(errorCode));
            } else {
                notificationService.notifierApresErreur(node, errorMsg);
            }
        }

        public void internSuccess() {
            if (this.dossierNotDeleted) {
                this.updateBanettesCible(parapheurService.getParentParapheur(node));
            }
            this.success();
        }

        public void updateBanettesCourant(NodeRef bureau) {
            this.banettesCourant = dossierService.getCorbeillesParent(this.node, bureau);
            this.parentCourant = nodeService.getPrimaryParent(this.node).getParentRef();
        }

        public void updateBanettesCible(NodeRef bureau) {
            if(dossierNotDeleted) {
                this.bureauCible = bureau;
                this.banettesCible = dossierService.getCorbeillesParent(this.node, bureau);
            }
        }

        public boolean execute() throws Exception {
            boolean ret = this.call();
            if(dossierNotDeleted) {
                nodeService.removeAspect(node, ParapheurModel.ASPECT_PENDING);
            }
            return ret;
        }

        public boolean hasToRemoveAspectFromNode(boolean error) {
            return (this.dossierNotDeleted || error) && super.hasToRemoveAspectFromNode(error);
        }

        protected void handleHashSignature() {
            NodeRef bureauCourant = parapheurService.getParentParapheur(node);
            JSONStringer messageJsonStringer = new JSONStringer();
            try {
                messageJsonStringer.object();
                messageJsonStringer.key(WorkerService.ACTION).value(DossierService.ACTION_DOSSIER.SIGN_INFO);
                messageJsonStringer.key(WorkerService.TYPE).value(WorkerService.TYPE_DOSSIER);
                messageJsonStringer.key(WorkerService.ID).value(node.getId());
                messageJsonStringer.key(WorkerService.BUREAUCIBLE).value(bureauCourant.getId());
                messageJsonStringer.key(WorkerService.USERNAME).value(AuthenticationUtil.getRunAsUser());
                messageJsonStringer.endObject();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            messagesSender.sendWorker(messageJsonStringer.toString(), node.getId());
        }

        protected void sendTdtByWorker() {
            NodeRef bureauCourant = parapheurService.getParentParapheur(node);
            List<String> owners = parapheurService.getParapheurOwners(bureauCourant);
            if (owners != null && !owners.isEmpty()) {
                String username = owners.get(0);
                String msg = "{" +
                        "\"" + ID + "\": \""+ node.getId() +"\"," +
                        "\"" + BUREAUCOURANT + "\": \""+ bureauCourant.getId() +"\"," +
                        "\"" + TYPE + "\": \"" + TYPE_DOSSIER + "\"," +
                        "\"" + ACTION + "\": \""+ ACTION_DOSSIER.TDT_HELIOS +"\"," +
                        "\"" + USERNAME + "\": \""+ username +"\"," +
                        "\"" + ANNOTPUB + "\": \"Envoi au TdT automatique\"" +
                        "}";
                messagesSender.sendWorker(msg, node.getId());
            }
            else {
                logger.error("Impossible d'envoyer sur Helios, aucun propriétaire trouvé");
            }
        }

        protected void stampByWorker() {
            NodeRef bureauCourant = parapheurService.getParentParapheur(node);
            List<String> owners = parapheurService.getParapheurOwners(bureauCourant);
            if (owners != null && !owners.isEmpty()) {
                String username = owners.get(0);
                String msg = "{" +
                        "\"" + ID + "\": \""+ node.getId() +"\"," +
                        "\"" + BUREAUCOURANT + "\": \""+ bureauCourant.getId() +"\"," +
                        "\"" + TYPE + "\": \"" + TYPE_DOSSIER + "\"," +
                        "\"" + ACTION + "\": \""+ ACTION_DOSSIER.CACHET +"\"," +
                        "\"" + USERNAME + "\": \""+ username +"\"," +
                        "\"" + ANNOTPUB + "\": \"Cachet serveur automatique\"," +
                        "\"" + AUTOMATIC + "\": true" +
                        "}";
                messagesSender.sendWorker(msg, node.getId());
            }
            else {
                logger.error("Impossible d'appliquer le cachet serveur, aucun propriétaire trouvé");
            }
        }
    }

    private Throwable getCause(Throwable e) {
        Throwable cause;
        Throwable result = e;

        while(null != (cause = result.getCause())  && (result != cause) ) {
            result = cause;
        }
        return result;
    }


    private Runnable handleAction(final ACTION_DOSSIER action, final Map request, final Action function) {

        return new Runnable() {

            @Override
            public void run() {
                String msg, id = null;
                boolean error = false;
                NodeRef node = null, bureauCourant = null;

                UserTransaction utx = transactionService.getNonPropagatingUserTransaction();
                try {
                    utx.begin();

                    if(request.containsKey(USERNAME)) {
                        final String username = (String) request.get(USERNAME);
                        AuthenticationUtil.setRunAsUser(username);
                        authenticationComponent.setCurrentUser(username);
                    }

                    id = (String) request.get(ID);
                    node = new NodeRef(WORKSPACE + id);
                    if(request.containsKey(BUREAUCOURANT)) {
                        bureauCourant = new NodeRef(WORKSPACE + request.get(BUREAUCOURANT));
                    }
                    function.init(request, action, node, bureauCourant);

                    schedulerService.startExecution(id);

                    //Vérifications
                    function.errorMsg = baseVerification(action, node, bureauCourant);
                    error = !"OK".equals(function.errorMsg) || !function.internBefore();

                    if(!error) {
                        // Execution de l'action
                        logger.info("ACTION : " + function.action.name() + "; ID : " + id  + "; BEGIN");
                        transactionService.getRetryingTransactionHelper().doInTransaction(
                                new RetryingTransactionHelper.RetryingTransactionCallback<Boolean>() {
                                    @Override
                                    public Boolean execute() throws Throwable {
                                        return function.execute();
                                    }
                                }, false, true);

                        function.internSuccess();
                    }
                    else {
                        logger.error(function.errorMsg);
                        function.error();
                    }

                    utx.commit();
                } catch (Exception e) {
                    error = true;

                    if (e.getCause() instanceof InterruptedException) {
                        function.errorMsg = "Traitement du dossier interrompu par l'administrateur";
                    } else {
                        function.errorMsg = "Exception lors du traitement du dossier  : " + getCause(e).getMessage();

                        if (e.getCause() instanceof ParapheurException) {
                            function.errorCode = ((ParapheurException) e.getCause()).getInternalCode();
                        }

                        e.printStackTrace();
                    }
                    function.error();
                    try {
                        utx.rollback();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } finally {
                    // Si il y a eu une exception, le dossier est toujours "PENDING", il faut donc enlever l'aspect pour
                    // ne pas bloquer le dossier. Cependant le removeAspect doit se faire dans une transaction.
                    if (function.hasToRemoveAspectFromNode(error)) {
                        UserTransaction transaction = transactionService.getNonPropagatingUserTransaction();
                        try {
                            transaction.begin();
                            nodeService.removeAspect(node, ParapheurModel.ASPECT_PENDING);
                            transaction.commit();
                        } catch (Exception e) {
                            try {
                                transaction.rollback();
                            } catch (Exception e1) {e1.printStackTrace();}
                        }
                    }
                    schedulerService.stopExecution(id);
                    if (!error) {
                        function.after();
                        logger.info("ACTION : " + function.action.name() + "; ID : " + id + "; OK");
                    }
                }
            }
        };
    }

    @Override
    public void getAttestByWorker(NodeRef node) {
        NodeRef bureauCourant = parapheurService.getParentParapheur(node);
        List<String> owners = parapheurService.getParapheurOwners(bureauCourant);
        if (owners != null && !owners.isEmpty()) {
            String username = owners.get(0);
            String msg = "{" +
                    "\"" + ID + "\": \""+ node.getId() +"\"," +
                    "\"" + BUREAUCOURANT + "\": \""+ bureauCourant.getId() +"\"," +
                    "\"" + TYPE + "\": \"" + TYPE_DOSSIER + "\"," +
                    "\"" + ACTION + "\": \""+ ACTION_DOSSIER.GET_ATTEST +"\"," +
                    "\"" + USERNAME + "\": \""+ username +"\"," +
                    "\"" + ANNOTPUB + "\": \"Récupération de l'attestation de signature\"" +
                    "}";
            messagesSender.sendWorker(msg, node.getId());
        }
        else {
            logger.error("Impossible de récupérer l'attestation de signature, aucun propriétaire trouvé");
        }
    }

}
