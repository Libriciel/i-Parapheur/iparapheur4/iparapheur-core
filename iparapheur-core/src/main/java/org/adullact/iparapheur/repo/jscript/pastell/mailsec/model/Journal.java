package org.adullact.iparapheur.repo.jscript.pastell.mailsec.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Créé par lhameury le 22/05/18.
 */
public class Journal implements Serializable {
    long id;
    String type;
    long entityId;
    long userId;
    String documentId;
    String action;
    String message;
    Date date;
    String titre;

    public Journal() {
    }

    public Journal(long id, String type, long entityId, long userId, String documentId, String action, String message, Date date, String titre) {
        this.id = id;
        this.type = type;
        this.entityId = entityId;
        this.userId = userId;
        this.documentId = documentId;
        this.action = action;
        this.message = message;
        this.date = date;
        this.titre = titre;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getEntityId() {
        return entityId;
    }

    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }
}