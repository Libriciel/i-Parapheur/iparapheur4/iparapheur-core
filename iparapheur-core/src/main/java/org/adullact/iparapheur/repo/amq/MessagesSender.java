package org.adullact.iparapheur.repo.amq;

/**
 * Created by lhameury on 14/05/14.
 */
public interface MessagesSender {
    /**
     * Envoi d'un message dans la queue à partir de l'objet objString, en prenant en compte l'id du noeud fourni
     * L'aspect pending est ajouté au noeud.
     * @param objString Objet à envoyer dans la queue
     * @param id id du noeud concerné
     */
    public void sendWorker(String objString, String id);

    /**
     * Envoi d'un message dans la queue à partir de l'objet objString
     * @param objString Objet à envoyer dans la queue
     */
    public void sendWorker(String objString);
}
