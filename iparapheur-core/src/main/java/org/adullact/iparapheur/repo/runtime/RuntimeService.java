package org.adullact.iparapheur.repo.runtime;

import java.io.IOException;
import java.security.cert.Certificate;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by lhameury on 29/02/16.
 */
public interface RuntimeService {
    Properties getConfiguration();

    String getXemCmd();

    String execCmd(String cmd) throws IOException;

    void execCmdAt(String cmd) throws IOException;

    String execSQL(String request) throws IOException, ClassNotFoundException, SQLException;

    boolean statusOffice();

    boolean statusRedis();

    Certificate[] getCertChain();
}
