/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Initial Developpment by AtolCD, maintained by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.repo.CustomMetadataDef;
import com.atolcd.parapheur.repo.CustomMetadataType;
import com.atolcd.parapheur.repo.MetadataService;
import org.adullact.utils.StringUtils;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Stephane Vast - ADULLACT Projet
 */
public class MetadataServiceScriptable extends BaseProcessorExtension implements MetadataService {

    private static final Logger log = Logger.getLogger(MetadataServiceScriptable.class);

    private MetadataService metadataService;

    @Autowired
    private NamespaceService namespaceService;

    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    /**
     * Renvoie la liste complete des metadonnées pour l'administrateur
     * @return JSON sous forme de String
     */
    @SuppressWarnings("unused")
    public String getMetaDonneesApi(boolean asUser) {
        //Liste des définitions de metadonnées
        List<CustomMetadataDef> list = metadataService.getMetadataDefs();

        JSONArray mainJson = new JSONArray();

        String realName;
        String keyName;

        for (CustomMetadataDef customMetadataDef : list) {
            //Ajout de la metadonnée dans l'objet Json et des propriétés supplémentaires
            try {
                //Récupération du nom réel et du nom de la clé
                realName = customMetadataDef.getTitle();
                keyName = customMetadataDef.getName().toPrefixString(namespaceService);
                //Mise a jour de l'objet Json
                JSONObject obj = new JSONObject();
                obj.put("id", keyName.replaceFirst("cu:", ""));
                obj.put("name", realName);
                if(!asUser) {
                    obj.put("deletable", customMetadataDef.isDeletable());
                }
                obj.put("isAlphaOrdered", customMetadataDef.isAlphaOrdered());
                obj.put("type", customMetadataDef.getType().toString());
                obj.put("hasValues", customMetadataDef.getEnumValues() != null);
                List<String> addedValues = new ArrayList<String>();
                List<JSONObject> valuesObj = new ArrayList<JSONObject>();

                List<String> used = asUser ? new ArrayList<>() : getUsedValuesForDef(customMetadataDef);

                if (customMetadataDef.getEnumValues() != null) {
                    List<String> values = customMetadataDef.getEnumValues();

                    for (String value : values) {
                        JSONObject o = new JSONObject();
                        addedValues.add(value);
                        o.put("value", value);
                        o.put("deletable", !used.contains(value));
                        valuesObj.add(o);
                    }

                }
                for (String use : used) {
                    if (!addedValues.contains(use)) {
                        JSONObject o = new JSONObject();
                        addedValues.add(use);
                        o.put("value", use);
                        o.put("deletable", false);
                        valuesObj.add(o);
                    }
                }
                obj.put("values", valuesObj);
                mainJson.put(obj);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        return mainJson.toString();
    }

    @Override
    public List<CustomMetadataDef> getMetadataDefs() {
        return metadataService.getMetadataDefs();
    }

    @Override
    public void deleteMetadataDef(QName metadata) {
        metadataService.deleteMetadataDef(metadata);
    }

    @Override
    public void addMetadataDef(CustomMetadataDef def) {
        metadataService.addMetadataDef(def);
    }
    
    @Override
    public void addMetadatasDefs(List<CustomMetadataDef> defsToAdd) {
        metadataService.addMetadatasDefs(defsToAdd);
    }

    @Override
    public void saveMetadataDefs(List<CustomMetadataDef> defs) {
        metadataService.saveMetadataDefs(defs);
    }

    @Override
    public Object getValueFromString(CustomMetadataType customMetadataType, String value) {
        return metadataService.getValueFromString(customMetadataType, value);
    }

    @Override
    public void validateValue(CustomMetadataType type, String value) {
        metadataService.validateValue(type, value);
    }

    @Override
    public List<String> getUsedValuesForDef(CustomMetadataDef def) {
        return metadataService.getUsedValuesForDef(def);
    }

    @Override
    public List<String> getSubtypesReferencingMetadata(QName propName) {
        return metadataService.getSubtypesReferencingMetadata(propName);
    }

    @Override
    public List<CustomMetadataDef> getMetadatas(String type, String sstype) {
        return metadataService.getMetadatas(type, sstype);
    }

    @Override
    public Map<QName, Map<String, String>> getMetadatasMap(String type, String sstype) {
        return metadataService.getMetadatasMap(type, sstype);
    }

    @Override
    public Map<String, Serializable> getMetadataValues(Map<QName, Map<String, Serializable>> map) {
        return metadataService.getMetadataValues(map);
    }

    @Override
    public Map<QName, Map<String, Serializable>> getMetaDonneesDossierWithTypo(NodeRef dossier, String type, String sstype) {
        return metadataService.getMetaDonneesDossierWithTypo(dossier, type, sstype);
    }

    @Override
    public Map<QName, Map<String, Serializable>> getMetaDonneesDossier(NodeRef dossier) {
        return metadataService.getMetaDonneesDossier(dossier);
    }

    @Override
    public void reloadMetadataDefs() {
        metadataService.reloadMetadataDefs();
    }

    @Override
    public boolean areMetadataValid(@Nullable Map<QName, Serializable> metadataValues) throws IllegalArgumentException {
        return metadataService.areMetadataValid(metadataValues);
    }

    @SuppressWarnings("unused")
    public boolean isUrlValid(String value) {
        return StringUtils.isUrlRFC3986Valid(value);
    }

}
