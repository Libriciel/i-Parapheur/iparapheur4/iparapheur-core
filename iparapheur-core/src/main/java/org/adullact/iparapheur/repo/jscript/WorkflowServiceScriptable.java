package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.model.ParapheurErrorCode;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.SavedWorkflow;
import com.atolcd.parapheur.repo.TypesService;
import com.atolcd.parapheur.repo.WorkflowService;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import org.adullact.iparapheur.util.JsonUtils;
import org.adullact.iparapheur.util.NativeUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.NativeObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * User: lhameury
 * Date: 22/10/13
 * Time: 16:05
 */
@SuppressWarnings("UnusedDeclaration")
public class WorkflowServiceScriptable extends BaseProcessorExtension {

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private TypesService typesService;

    @Autowired
    private NodeService nodeService;

    private static Logger logger = Logger.getLogger(WorkflowServiceScriptable.class);

    public void isUsedBy(String title, String type, String subtype) {
        List<NodeRef> workflowsRefs = workflowService.searchWorkflows(title);

        if (workflowsRefs.size() == 1) {
            NodeRef workflowRef = workflowsRefs.get(0);
            Serializable usedBy = nodeService.getProperty(workflowRef, ParapheurModel.PROP_USED_BY);
            if (usedBy == null) {
                //Create property on workflows
            }
            if (usedBy != null) {
                Map<String, List<String>> usedByList = (Map<String, List<String>>) usedBy;
                List<String> subtypesUsed;
                if (!usedByList.containsKey(type)) {
                    subtypesUsed = new ArrayList<String>();
                } else {
                    subtypesUsed = usedByList.get(type);
                }
                if (!subtypesUsed.contains(subtype)) {
                    subtypesUsed.add(subtype);
                }
                usedByList.put(type, subtypesUsed);

                nodeService.setProperty(workflowRef, ParapheurModel.PROP_USED_BY, (Serializable) usedByList);
            }
        }
    }

    public void isNotUsedBy(String title, String type, String subtype) {
        List<NodeRef> workflowsRefs = workflowService.searchWorkflows(title);

        if (workflowsRefs.size() == 1) {
            NodeRef workflowRef = workflowsRefs.get(0);
            Serializable usedBy = nodeService.getProperty(workflowRef, ParapheurModel.PROP_USED_BY);
            if (usedBy == null) {
                //Create property on workflows
            }
            if (usedBy != null) {
                Map<String, List<String>> usedByList = (Map<String, List<String>>) usedBy;
                List<String> subtypesUsed;
                if (!usedByList.containsKey(type)) {
                    subtypesUsed = new ArrayList<String>();
                } else {
                    subtypesUsed = usedByList.get(type);
                }
                if (subtypesUsed.contains(subtype)) {
                    subtypesUsed.remove(subtype);
                }
                if (subtypesUsed.size() == 0) {
                    usedByList.remove(type);
                } else {
                    usedByList.put(type, subtypesUsed);
                }

                nodeService.setProperty(workflowRef, ParapheurModel.PROP_USED_BY, (Serializable) usedByList);
            }
        }
    }

    public void renameTypeForUse(String oldType, String newType) {
        List<NodeRef> workflowsRefs = workflowService.searchWorkflows("*");

        for (NodeRef workflow : workflowsRefs) {
            Serializable prop = nodeService.getProperty(workflow, ParapheurModel.PROP_USED_BY);
            if (prop != null) {
                Map<String, List<String>> usedBy = (Map<String, List<String>>) prop;

                if (usedBy.containsKey(oldType)) {
                    usedBy.put(newType, usedBy.get(oldType));
                    usedBy.remove(oldType);
                    nodeService.setProperty(workflow, ParapheurModel.PROP_USED_BY, (Serializable) usedBy);
                }
            }
        }
    }

    public void removeTypeForUse(String type) {
        List<NodeRef> workflowsRefs = workflowService.searchWorkflows("*");

        for (NodeRef workflow : workflowsRefs) {
            Serializable prop = nodeService.getProperty(workflow, ParapheurModel.PROP_USED_BY);
            if (prop != null) {
                Map<String, List<String>> usedBy = (Map<String, List<String>>) prop;

                if (usedBy.containsKey(type)) {
                    usedBy.remove(type);
                    nodeService.setProperty(workflow, ParapheurModel.PROP_USED_BY, (Serializable) usedBy);
                }
            }
        }
    }

    public JSONArray handleListCircuitsAsAdmin(String title, int maxSize, int page) throws JSONException {
        JSONArray toReturn = new JSONArray();

        List<NodeRef> workflowsRefs;

        if (maxSize == 0 && page == 0) {
            workflowsRefs = workflowService.searchWorkflows(title);
        } else {
            workflowsRefs = workflowService.searchWorkflows(title, maxSize * page, maxSize);
        }

        Map<String, Map<String, List<String>>> subtypes = null;

        int total = workflowService.getWorkflowsSize(title);

        for (NodeRef workflow : workflowsRefs) {
            SavedWorkflow savedWorkflow = workflowService.getSavedWorkflow(workflow);

            if (nodeService.getProperty(workflow, ParapheurModel.PROP_USED_BY) == null) {
                if (subtypes == null) {
                    subtypes = typesService.getUsedWorkflows();
                }
                //Si la propriÃ©tÃ© n'est pas dÃ©finie, on la gÃ©nÃ¨re
                typesService.setUsedWorkflows(workflow, subtypes.get(savedWorkflow.getName()));
            }

            boolean isEditable = workflowService.isWorkflowEditable(savedWorkflow);
            JSONObject obj = workflowToJSONObject(savedWorkflow, isEditable);

            obj.put("total", total);
            toReturn.put(obj);
        }
        return toReturn;
    }

    public JSONObject updateWorkflow(NodeRef workflowRef, String name, NativeArray circuit, NativeArray acl,
                                 NativeArray aclGroups,boolean publicCircuit) {

        SavedWorkflow workflow = workflowService.getSavedWorkflow(workflowRef);

        // Default tests

        boolean isEditable = workflowService.isWorkflowEditable(workflow);
        if (!isEditable) {
            return JsonUtils.createHttpStatusJsonObject(403, ParapheurErrorCode.CANT_RENAME_A_CURRENTLY_USED_WORKFLOW);
        }

        //

        //Circuit a supprimer avec mise a jour (seulement pour un changement de nom)
        boolean toDelete = !workflow.getName().toLowerCase().equals(name.toLowerCase());

        // Recuperation de l'id du circuit de base
        String id = workflowRef.getId();

        // Sauvegarde/Creation du circuit
        workflowService.saveWorkflow(name,
                                     nativeArrayToListEtapes(circuit),
                                     new HashSet<NodeRef>(NativeUtils.nativeArrayToNodeRef(acl)),
                                     new HashSet<String>(NativeUtils.nativeArrayToString(aclGroups)),
                                     publicCircuit);

        // Si le circuit doit etre supprime
        if (toDelete) {
            //Suppression
            nodeService.deleteNode(workflowRef);
            // Recuperation du nouvel ID
            id = workflowService.getWorkflowByName(name).getId();
        }

        return JsonUtils.createHttpStatusJsonObject(200, id);
    }

    public void deleteWorkfow(NodeRef workflowRef) {
        workflowService.removeWorkflow(workflowRef);
    }

    public String createWorkflow(String name,
                                 NativeArray circuit,
                                 NativeArray acl,
                                 NativeArray aclGroups,
                                 boolean publicCircuit) {
        //Création du circuit
        workflowService.saveWorkflow(name,
                                     nativeArrayToListEtapes(circuit),
                                     new HashSet<NodeRef>(NativeUtils.nativeArrayToNodeRef(acl)),
                                     new HashSet<String>(NativeUtils.nativeArrayToString(aclGroups)),
                                     publicCircuit);

        return workflowService.getWorkflowByName(name).getId();
    }

    private List<EtapeCircuit> nativeArrayToListEtapes(NativeArray array) {
        List<Object> objects = NativeUtils.nativeArrayToObject(array);
        List<EtapeCircuit> circuit = new ArrayList<EtapeCircuit>();
        for (Object obj : objects) {
            NativeObject natObj = (NativeObject) obj;
            //noinspection MismatchedQueryAndUpdateOfCollection
            NativeObjectMapAdapter adapter = new NativeObjectMapAdapter(natObj);
            EtapeCircuitImpl etape = new EtapeCircuitImpl();
            etape.setActionDemandee((String) adapter.get("actionDemandee"));
            etape.setTransition((String) adapter.get("transition"));
            if (etape.getTransition().equals("PARAPHEUR") && adapter.containsKey("parapheur")) {
                NodeRef parapheur = new NodeRef("workspace://SpacesStore/" + adapter.get("parapheur"));
                etape.setParapheur(parapheur);
                etape.setParapheurName((String) nodeService.getProperty(parapheur, ContentModel.PROP_NAME));
            }
            if (adapter.containsKey("listeNotification") && adapter.get("listeNotification") instanceof NativeArray) {
                etape.setListeDiffusion(new HashSet<NodeRef>(NativeUtils.nativeArrayToNodeRef((NativeArray) adapter.get("listeNotification"))));
            }
            if (adapter.containsKey("listeMetadatas") && adapter.get("listeMetadatas") instanceof NativeArray) {
                etape.setListeMetadatas(new HashSet<String>(NativeUtils.nativeArrayToString((NativeArray) adapter.get("listeMetadatas"))));
            }
            if (adapter.containsKey("listeMetadatasRefus") && adapter.get("listeMetadatasRefus") instanceof NativeArray) {
                etape.setListeMetadatasRefus(new HashSet<String>(NativeUtils.nativeArrayToString((NativeArray) adapter.get("listeMetadatasRefus"))));
            }
            circuit.add(etape);
        }
        return circuit;
    }

    private List<EtapeCircuit> nativesToEtapes(List<NativeObject> objects) {
        List<EtapeCircuit> circuit = new ArrayList<EtapeCircuit>();
        for (NativeObject obj : objects) {
            //noinspection MismatchedQueryAndUpdateOfCollection
            NativeObjectMapAdapter adapter = new NativeObjectMapAdapter(obj);
            EtapeCircuitImpl etape = new EtapeCircuitImpl();
            etape.setActionDemandee((String) adapter.get("actionDemandee"));
            etape.setTransition((String) adapter.get("transition"));
            if (etape.getTransition().equals("PARAPHEUR") && adapter.containsKey("parapheur")) {
                NodeRef parapheur = new NodeRef("workspace://SpacesStore/" + adapter.get("parapheur"));
                etape.setParapheur(parapheur);
                etape.setParapheurName((String) nodeService.getProperty(parapheur, ContentModel.PROP_NAME));
            }
            if (adapter.containsKey("listeNotification") && adapter.get("listeNotification") instanceof NativeArray) {
                etape.setListeDiffusion(new HashSet<NodeRef>(NativeUtils.nativeArrayToNodeRef((NativeArray) adapter.get("listeNotification"))));
            }
            if (adapter.containsKey("listeMetadatas") && adapter.get("listeMetadatas") instanceof NativeArray) {
                etape.setListeMetadatas(new HashSet<String>(NativeUtils.nativeArrayToString((NativeArray) adapter.get("listeMetadatas"))));
            }
            if (adapter.containsKey("listeMetadatasRefus") && adapter.get("listeMetadatasRefus") instanceof NativeArray) {
                etape.setListeMetadatasRefus(new HashSet<String>(NativeUtils.nativeArrayToString((NativeArray) adapter.get("listeMetadatasRefus"))));
            }
            circuit.add(etape);
        }
        return circuit;
    }

    private JSONObject workflowToJSONObject(SavedWorkflow workflow, boolean editable) {
        JSONObject obj = new JSONObject();
        NodeRef workflowRef = workflow.getNodeRef();
        try {
            if (editable) {
                Map<String, List<String>> usedBy = (Map<String, List<String>>) nodeService.getProperty(workflowRef,
                                                                                                       ParapheurModel.PROP_USED_BY);

                if (usedBy != null && !usedBy.isEmpty()) {
                    obj.put("isUsed", true);
                    obj.put("usedBy", usedBy);
                } else {
                    obj.put("isUsed", false);
                    obj.put("usedBy", "");
                }
            }
            obj.put("id", workflow.getNodeRef().getId());
            obj.put("aclGroupes", workflow.getAclGroupes().toArray());
            obj.put("aclParapheurs", NativeUtils.NodeRefListToId((workflow.getAclParapheurs().toArray())));
            obj.put("name", workflow.getName());
            obj.put("editable", editable);
            JSONArray etapes = new JSONArray();
            for (EtapeCircuit etape : workflow.getCircuit()) {
                JSONObject step = new JSONObject();
                step.put("actionDemandee", etape.getActionDemandee());
                if (etape.getParapheur() != null) {
                    step.put("parapheur", etape.getParapheur().getId());
                    step.put("parapheurName", nodeService.getProperty(etape.getParapheur(), ContentModel.PROP_TITLE).toString());
                }
                if (etape.getTransition() != null) {
                    step.put("transition", etape.getTransition());
                }
                step.put("listeNotification", NativeUtils.NodeRefListToId(etape.getListeNotification().toArray()));
                step.put("listeMetadatas", etape.getListeMetadatas() != null ? etape.getListeMetadatas().toArray(): new String[0]);
                step.put("listeMetadatasRefus", etape.getListeMetadatasRefus() != null ? etape.getListeMetadatasRefus().toArray(): new String[0]);
                etapes.put(step);
            }
            obj.put("etapes", etapes);
            obj.put("isPublic", workflow.isPublic());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
}
