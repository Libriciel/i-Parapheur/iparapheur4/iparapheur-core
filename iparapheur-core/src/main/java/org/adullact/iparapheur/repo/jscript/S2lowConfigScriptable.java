/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2012, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.repo.S2lowService;
import org.adullact.iparapheur.util.X509Util;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.BaseElement;
import org.junit.Assert;
import org.mozilla.javascript.NativeArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.transaction.UserTransaction;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Backend JAVA pour les écrans de paramétrage TDT S2LOW
 * 
 * @author Vivien Barousse
 * @author Stephane Vast - ADULLACT-Projet
 */
public class S2lowConfigScriptable extends BaseProcessorExtension {

    private static final Logger logger = Logger.getLogger(S2lowConfigScriptable.class);

    private NodeService nodeService;

    private NamespaceService namespaceService;

    private SearchService searchService;

    private ContentService contentService;

    private FileFolderService fileFolderService;

    private TransactionService transactionService;

    @Autowired
    @Qualifier("s2lowServiceDecorator")
    private S2lowService s2lowService;

    /*
     * Setters des services utilisés
     */
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public NativeArray getArrayLogin(String file, String password, String port, String server, String certName) throws Exception {
        Map<String, String> properties = new HashMap<String, String>();
        InputStream is;
        if(!file.isEmpty() && !file.equals("undefined")) {
            byte[] decoded = Base64.decodeBase64(file.getBytes());
            is = new ByteArrayInputStream(decoded);
        } else {
            is = readFile(certName);
        }
        properties.put("password", password);
        properties.put("port", port);
        properties.put("server", server);
        properties.put("name", certName);

        List<String> list = s2lowService.isCertificateOk(properties, is);

        return list != null ? new NativeArray(list.toArray()) : null;
    }

    @SuppressWarnings("unused")
    public boolean testLogedConnexion(String file, String password, String port, String server, String certName, String userlogin, String userpassword) throws Exception {
        Map<String, String> properties = new HashMap<String, String>();
        InputStream is;
        if(!file.isEmpty() && !file.equals("undefined")) {
            byte[] decoded = Base64.decodeBase64(file.getBytes());
            is = new ByteArrayInputStream(decoded);
        } else {
            is = readFile(certName);
        }
        properties.put("password", password);
        properties.put("port", port);
        properties.put("server", server);
        properties.put("name", certName);
        properties.put("userlogin", userlogin);
        properties.put("userpassword", userpassword);
        return s2lowService.isConnexionOK(properties, is);
    }

    /*
     * Getters et Setters pour le paramétrage S2LOW
     */
    @SuppressWarnings("unused")
    public Map<String, String> getActesParameters() {
        return getParameters("/s2low/actes", null);
    }

    public void setActesParameters(Map<String, String> params) {
        setParameters("/s2low/actes", params, null);
    }

    public Map<String, String> getHeliosParameters() {
        return getParameters("/s2low/helios", null);
    }

    public void setHeliosParameters(Map<String, String> params) {
        setParameters("/s2low/helios", params, null);
    }

    public Map<String, String> getSecureMailParameters() {
        return getParameters("/s2low/mailsec", null);
    }

    public void setSecureMailParameters(Map<String, String> params) {
        setParameters("/s2low/mailsec", params, null);
    }

    public Map<String, String> getActesParametersForType(String type) {
        return getParameters("/s2low/actes", type);
    }

    public void setActesParametersForType(String type, Map<String, String> params) {
        setParameters("/s2low/actes", params, type);
    }

    public Map<String, String> getSigParametersForType(String type) {
        Document doc = getConfigDocument(type);
        String path = "/s2low/xades";

        List configNodes = doc.selectNodes(path);

        if(configNodes.size() != 1) {
            createParameters(path, type, "sig-snippet.xml");
        }
        return getParameters(path, type);
    }

    public Map<String, String> getPadesParametersForType(String type) {
        Document doc = getConfigDocument(type);
        String path = "/s2low/pades";

        List configNodes = doc.selectNodes(path);

        if(configNodes.size() != 1) {
            createParameters(path, type, "pades-snippet.xml");
        }
        return getParameters(path, type);
    }

    public void setSigParametersForType(String type, Map<String, String> params) {
        setParameters("/s2low/xades", params, type);
    }

    public void setPadesParametersForType(String type, Map<String, String> params) {
        setParameters("/s2low/pades", params, type);
    }

    protected Document getConfigDocument(NodeRef node) {

        ContentReader reader = contentService.getReader(node, ContentModel.PROP_CONTENT);
        SAXReader saxReader = new SAXReader();
        try {
            Document doc = saxReader.read(reader.getContentInputStream());

            return doc;
        } catch (DocumentException ex) {
            // logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    protected void setConfigDocument(NodeRef node, Document doc) {
        ContentWriter writer = contentService.getWriter(node, ContentModel.PROP_CONTENT, true);

        writer.putContent(doc.asXML());
    }

    protected NodeRef getTypeConfigNode(String type) {
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                "/app:company_home/app:dictionary/cm:metiertype",
                null,
                namespaceService,
                false);

        NodeRef toReturn = null;

        List<ChildAssociationRef> childs = nodeService.getChildAssocs(nodes.get(0));

        for(ChildAssociationRef child : childs) {
            NodeRef childNode = child.getChildRef();
            if((nodeService.getProperty(childNode, ContentModel.PROP_NAME)).equals(type + "_s2low_properties.xml")) {
                toReturn = childNode;
                break;
            }
        }

        return toReturn;
    }

    protected Document getConfigPatchDocument(String fileName) {
        InputStream viewStream = getClass().getClassLoader().getResourceAsStream("alfresco/module/parapheur/s2low/"+fileName);
        SAXReader saxReader = new SAXReader();
        try {
            Document doc = saxReader.read(viewStream);
            return doc;
        } catch (DocumentException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    protected void createParameters(String path, String type, String fileName) {
        UserTransaction tx = transactionService.getUserTransaction();
        try {
            tx.begin();

            NodeRef typeNode = getTypeConfigNode(type);
            Document doc = getConfigDocument(typeNode);

            List<Node> configNodes = doc.selectNodes(path);

            if (configNodes.isEmpty()) {
                Document snippet = getConfigPatchDocument(fileName);
                doc.getRootElement().add(snippet.getRootElement());
                setConfigDocument(typeNode, doc);
            }

            tx.commit();
        } catch (Exception e) {
            try {
                tx.rollback();
            } catch(Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    public Map<String, String> getHeliosParametersForType(String type) {
        return getParameters("/s2low/helios", type);
    }

    public void setHeliosParametersForType(String type, Map<String, String> params) {
        setParameters("/s2low/helios", params, type);
    }

    public Map<String, String> getSecureMailParametersForType(String type) {
        return getParameters("/s2low/mailsec", type);
    }

    public void setSecureMailParametersForType(String type, Map<String, String> params) {
        setParameters("/s2low/mailsec", params, type);
    }

    protected Map<String, String> getParameters(String configPath, String type) {
        logger.debug("getParameters avec type=" + type);

        Document doc = getConfigDocument(type);

        List<Node> configNodes = doc.selectNodes(configPath);

        Assert.assertEquals("XPath query returned no results", 1, configNodes.size());
        Assert.assertTrue("Invalid XPath query", configNodes.get(0) instanceof Element);

        Element configNode = (Element) configNodes.get(0);

        List<Element> configElements = configNode.elements();

        Map<String, String> configParams = new HashMap<String, String>();

        for (Element element : configElements) {
            configParams.put(element.getName(), element.getTextTrim());
        }
        if (configParams.containsKey("name") && configParams.get("name")!=null &&
                configParams.containsKey("password") && configParams.get("password")!=null) {
            String res = isPwdValidForCertificate(configParams.get("name"), configParams.get("password"));
            if (res.startsWith("ok")) {
                configParams.put("isPwdGoodForPkcs", "ok");
                configParams.put("dateLimite", res.substring(2));
            } else if (res.startsWith("ex")) {
                configParams.put("isPwdGoodForPkcs", "ex");
                configParams.put("dateLimite", res.substring(2));
            } else {
                configParams.put("isPwdGoodForPkcs", res);
            }
        } else {
            //logger.error("no valid param found 'name' or 'password'.");
            configParams.put("isPwdGoodForPkcs", "ko");
        }

        return configParams;
    }

    protected void setParameters(String configPath, Map<String, String> params, String type) {
        Document doc = getConfigDocument(type);

        //TODO:Create if don't exist
        List<Node> configNodes = doc.selectNodes(configPath);

        Assert.assertEquals("XPath query returned no results", 1, configNodes.size());
        Assert.assertTrue("Invalid XPath query", configNodes.get(0) instanceof Element);

        Element configNode = (Element) configNodes.get(0);

        // Remove all previous elements
        for (Element e : ((List<Element>) configNode.elements())) {
            configNode.remove(e);
        }

        for (Entry<String, String> param : params.entrySet()) {
            Element e = new BaseElement(param.getKey());
            e.setText(param.getValue());

            configNode.add(e);
        }

        setConfigDocument(doc, type);
    }

    protected Document getConfigDocument(String type) {
        NodeRef configNodeRef = getConfigNode(type);
        
        ContentReader reader = contentService.getReader(configNodeRef, ContentModel.PROP_CONTENT);
        SAXReader saxReader = new SAXReader();
        try {
            Document doc = saxReader.read(reader.getContentInputStream());

            return doc;
        } catch (DocumentException ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    protected void setConfigDocument(Document doc, String type) {
        NodeRef configNode = getConfigNode(type);

        ContentWriter writer = contentService.getWriter(configNode, ContentModel.PROP_CONTENT, true);

        writer.putContent(doc.asXML());
    }

    protected NodeRef getConfigNode(String type) {
        
        if (type == null) {
            return getDefaultConfigNode();
        }
        String typeQueryPrefix = "/app:company_home/app:dictionary/cm:metiertype";
        
        // find it !
        String query = typeQueryPrefix + "/cm:" + org.alfresco.webservice.util.ISO9075.encode(type) + "_s2low_properties.xml";
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                query,
                null,
                namespaceService,
                false);
        if (nodes.size() != 1) {
            // on considère qu'il n'existe pas encore. Alors on fait une copie de la source
            return copyDefaultConfigTo(type);
        }
        else {
            return nodes.get(0);
        }
    }
    
    private NodeRef getDefaultConfigNode() {
        List<NodeRef> nodes;
        String query = "/app:company_home/app:dictionary/ph:certificats/cm:s2low_properties.xml";
        nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
            query,
            null,
            namespaceService,
            false);

        if (nodes.size() != 1) {
            throw new RuntimeException("Can't find S2low configuration file");
        }
        return nodes.get(0);
    }
    
    private NodeRef copyDefaultConfigTo(String type) {
        // trouver la source
        NodeRef sourceRef = this.getDefaultConfigNode();
        String typeQueryPrefix = "/app:company_home/app:dictionary/cm:metiertype";
        // trouver le répertoire cible   "/app:company_home/app:dictionary/cm:metiertype"
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                typeQueryPrefix,
                null,
                namespaceService,
                false);
        if (nodes.size() != 1) {
            throw new RuntimeException("Can't find typemetier target folder for s2low");
        }
        NodeRef targetParentRef = nodes.get(0);
        // Copier !
        try {
            // Ne pas encoder ici (sinon double encodage de l'espace..  -> _x0020_ -> _x005f_x0020_)
            return fileFolderService.copy(sourceRef, targetParentRef,
                    type + "_s2low_properties.xml").getNodeRef();
        } catch (FileExistsException ex) {
            logger.error(ex.getLocalizedMessage());
            throw new RuntimeException("Can't create config file for type=" + type);
        } catch (FileNotFoundException ex) {
            logger.error(ex.getLocalizedMessage());
            throw new RuntimeException("Can't find target folder config file for type=" + type);
        }
    }
    
    private InputStream readFile(String nameFile) throws Exception {
        InputStream inputStream;
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                "/app:company_home/app:dictionary/ph:certificats/cm:" + org.alfresco.webservice.util.ISO9075.encode(nameFile),
                null,
                namespaceService,
                false);
        
        if (nodes.size() != 1) {
            throw new RuntimeException("readFile: Can't find the file '" + nameFile + "', SIZE=" + nodes.size());
        }
        NodeRef fileNodeRef = nodes.get(0);
        ContentReader reader = contentService.getReader(fileNodeRef, ContentModel.PROP_CONTENT);
        inputStream = reader.getContentInputStream();
        return inputStream;
    }

    /**
     * Teste le couple certificat PKCS#12 / pwd donné en paramètre
     * 
     * @param name le nom du certificat PKCS#12
     * @param pwdString le mot de passe du certificat
     * @return ok, ko, ou expire
     */
    public String isPwdValidForCertificate(String name, String pwdString) {

        InputStream is = null;
        try { is = readFile(name); } catch (Exception ex) { /* No need */ }
        String res = X509Util.checkPasswordForCertificate(is, pwdString);

        // Closing stream

        try {
            if (is != null) {
                is.close();
            }
        } catch (java.io.IOException ex) {
            logger.error("Close PKCS#12 stream impossible, IOException", ex);
        }

        //

        return res;
    }

    /**
     * Teste le couple certificat PKCS#12 / pwd donné en paramètre
     *
     * @param base64File le fichier certificat PKCS#12, en Base64
     * @param pwdString le mot de passe du certificat
     * @return ok, ko, ou expire
     */
    @SuppressWarnings("unused")
    public String isPwdValidForFileCertificate(String base64File, String pwdString) {

        byte[] decoded = Base64.decodeBase64(base64File.getBytes());
        InputStream is = new ByteArrayInputStream(decoded);
        String res = X509Util.checkPasswordForCertificate(is, pwdString);

        // Closing stream

        try {
            is.close();
        } catch (java.io.IOException ex) {
            logger.error("Close PKCS#12 stream impossible, IOException", ex);
        }

        //

        return res;
    }

    public void createCertificateFile(String filename, String cert) {

        ResultSet result = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_XPATH, "/app:company_home/app:dictionary/ph:certificats");

        if (result.length() > 0) {
            NodeRef confHome = result.getNodeRef(0);
            List<ChildAssociationRef> childs = nodeService.getChildAssocs(confHome);
            NodeRef certRef = null;
            for(ChildAssociationRef child : childs) {
                if(nodeService.getProperty(child.getChildRef(), ContentModel.PROP_NAME).equals(filename)) {
                    certRef = child.getChildRef();
                }
            }

            if(certRef == null) {
                //Création
                FileInfo certInfo = fileFolderService.create(
                        confHome,
                        filename,
                        ContentModel.TYPE_CONTENT);
                certRef = certInfo.getNodeRef();
            }

            ByteArrayInputStream contentInputStream = new ByteArrayInputStream(Base64.decodeBase64(cert.getBytes()));
            // get a writer for the content and put the file
            ContentWriter writer = contentService.getWriter(certRef, ContentModel.PROP_CONTENT, true);
            writer.putContent(contentInputStream);
        }
    }
}
