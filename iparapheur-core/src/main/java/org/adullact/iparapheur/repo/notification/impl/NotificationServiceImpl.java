package org.adullact.iparapheur.repo.notification.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import coop.libriciel.service.DeskUtilsService;
import coop.libriciel.service.FolderUtilsService;
import coop.libriciel.service.NodeUtilsService;
import coop.libriciel.service.UserUtilsService;
import org.adullact.iparapheur.repo.notification.Notification;
import org.adullact.iparapheur.repo.notification.NotificationService;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;

/**
 * Created by lhameury on 14/05/14.
 */
public class NotificationServiceImpl implements NotificationService {

    private static final Logger logger = Logger.getLogger(NotificationService.class);

    @Autowired
    private NodeService nodeService;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private DeskUtilsService deskUtilsService;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private FolderUtilsService folderUtilsService;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private NodeUtilsService nodeUtilsService;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private UserUtilsService userUtilsService;
    @Autowired
    private NotificationCenter notificationCenter;
    @Autowired
    private ParapheurUserPreferences parapheurUserPreferences;

    private boolean notifySecretaire;

    private final Map<String, List<Notification>> preparedNotificationsForDeletedDossier = new HashMap<>();
    private static final String DELEGATION_STR = " (Par délégation de ";
    private static final String NOTIFICATION_LIST_STR = " (Liste de notification)";

    public boolean getNotifySecretaire() {
        return notifySecretaire;
    }

    public void setNotifySecretaire(boolean notifySecretaire) {
        this.notifySecretaire = notifySecretaire;
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourValidation(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourValidation(NodeRef dossier, NodeRef bureauCourant) {

        NotificationCenter.Reason reason = NotificationCenter.Reason.approve;
        EtapeCircuit etapeCourante = null;
        EtapeCircuit etapePrecedente = null;
        List<EtapeCircuit> circuit = folderUtilsService.getWorkflow(dossier);
        int numEtapePrecedente = 0;
        if (circuit != null) {
            if (folderUtilsService.isOver(dossier) && circuit.get(0).getActionDemandee() == null) {
                return;
            }
            for (EtapeCircuit etape : circuit) {
                if (!etape.isApproved()) {
                    etapeCourante = etape;
                    break;
                }
                etapePrecedente = etape;
                numEtapePrecedente++;
            }
        }
        if (etapeCourante != null && etapePrecedente != null) {

            String roleNotifie = deskUtilsService.getDeskName(etapeCourante.getParapheur());
            String username = AuthenticationUtil.getRunAsUser();

            String nomValideur = userUtilsService.getFullname(username);
            String dossierTitle = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);

            NodeRef bureauValideur = (bureauCourant != null) ? bureauCourant : etapePrecedente.getParapheur();
            String roleValideur = (String) nodeService.getProperty(bureauValideur, ContentModel.PROP_TITLE);

            String banetteOwner = (EtapeCircuit.ETAPE_ARCHIVAGE.equals(etapeCourante.getActionDemandee())) ?
                    CorbeillesService.A_ARCHIVER :
                    CorbeillesService.A_TRAITER;

            // Proprietaire
            Notification notificationCourants = new Notification(
                    Notification.Target.current.name(),
                    reason,
                    etapeCourante.getParapheur(),
                    roleNotifie,
                    nomValideur,
                    username,
                    roleValideur,
                    NotificationCenter.VALUE_TYPE_DOSSIER,
                    dossier,
                    dossierTitle,
                    etapeCourante.getActionDemandee(),
                    etapePrecedente.getActionDemandee(),
                    etapePrecedente.getAnnotation(),
                    banetteOwner);
            notificationCenter.broadcastNotification(deskUtilsService.getDeskOwners(etapeCourante.getParapheur()), notificationCourants);

            // Propriétaire de l'étape d'avant
            Notification notificationPrecedent = new Notification(notificationCourants);
            NodeRef bureauPrecedent = etapePrecedente.getParapheur();
            notificationPrecedent.setBureauNotifie(bureauPrecedent);
            notificationPrecedent.setTarget(Notification.Target.previous);
            notificationPrecedent.setRoleNotifie(deskUtilsService.getDeskName(bureauPrecedent));
            // Si l'étape précédente était la première étape, on met à jour la banette à transmettre
            if (numEtapePrecedente == 1) {
                notificationPrecedent.setBanette(CorbeillesService.EN_PREPARATION);
                notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureauPrecedent), notificationPrecedent);
            } else {
                notificationPrecedent.setBanette(CorbeillesService.A_TRAITER);
            }
            notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureauPrecedent), notificationPrecedent);

            // current step delegates
            String deleguePrecedentName = etapeCourante.getParapheurName();
            for (NodeRef bureauDelegue : deskUtilsService.getDesksOnDelegationPath(etapeCourante.getParapheur())) {
                String delegueName = (String) nodeService.getProperty(bureauDelegue, ContentModel.PROP_TITLE);
                Notification notificationDelegues = new Notification(
                        Notification.Target.delegues.name(),
                        reason,
                        bureauDelegue,
                        delegueName + DELEGATION_STR + deleguePrecedentName + ")",
                        nomValideur,
                        username,
                        roleValideur,
                        NotificationCenter.VALUE_TYPE_DOSSIER,
                        dossier,
                        dossierTitle,
                        etapeCourante.getActionDemandee(),
                        etapePrecedente.getActionDemandee(),
                        etapePrecedente.getAnnotation(),
                        CorbeillesService.DELEGUES);
                notificationCenter.broadcastNotification(deskUtilsService.getDeskOwners(bureauDelegue), notificationDelegues);
                deleguePrecedentName = delegueName;
            }

            // Liste de notification de l'étape précédente
            Set<NodeRef> bureauxNotifiesEtape = etapePrecedente.getListeNotification();
            if (bureauxNotifiesEtape != null) {

                for (NodeRef bureauNotifieEtape : bureauxNotifiesEtape) {
                    Notification notificationDiffusion = new Notification(
                            Notification.Target.diff.name(),
                            reason,
                            bureauNotifieEtape,
                            nodeService.getProperty(bureauNotifieEtape, ContentModel.PROP_TITLE) + NOTIFICATION_LIST_STR,
                            nomValideur,
                            username,
                            roleValideur,
                            NotificationCenter.VALUE_TYPE_DOSSIER,
                            dossier,
                            dossierTitle,
                            etapeCourante.getActionDemandee(),
                            etapePrecedente.getActionDemandee(),
                            etapePrecedente.getAnnotation(), null);
                    notificationCenter.broadcastNotification(deskUtilsService.getDeskOwners(bureauNotifieEtape), notificationDiffusion);
                }
            }

            // Emetteur WS
            String emmeteurWS = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_WS_EMETTEUR);
            if (emmeteurWS != null) {
                Notification notificationWS = new Notification(
                        Notification.Target.tiers.name(),
                        reason,
                        null,
                        "Création par WebService",
                        nomValideur,
                        username,
                        roleValideur,
                        NotificationCenter.VALUE_TYPE_DOSSIER,
                        dossier,
                        dossierTitle,
                        etapeCourante.getActionDemandee(),
                        etapePrecedente.getActionDemandee(),
                        etapePrecedente.getAnnotation(), null);
                List<String> emmeteurWSList = new ArrayList<>(1);
                emmeteurWSList.add(emmeteurWS);
                notificationCenter.broadcastNotification(emmeteurWSList, notificationWS);
            }

            // Émetteur du dossier, seulement quand il arrive en fin de circuit et qu'il n'est pas le bureau archiveur,
            // sinon il sera notifié deux fois.

            if (EtapeCircuit.ETAPE_ARCHIVAGE.equals(etapeCourante.getActionDemandee())) {
                NodeRef bureauEmetteur = folderUtilsService.getEmetteur(dossier);
                Notification notificationEmetteur = new Notification(
                        Notification.Target.owner.name(),
                        reason,
                        bureauEmetteur,
                        (String) nodeService.getProperty(bureauEmetteur, ContentModel.PROP_TITLE),
                        nomValideur,
                        username,
                        roleValideur,
                        NotificationCenter.VALUE_TYPE_DOSSIER,
                        dossier,
                        dossierTitle,
                        etapeCourante.getActionDemandee(),
                        etapePrecedente.getActionDemandee(),
                        etapePrecedente.getAnnotation(),
                        CorbeillesService.A_ARCHIVER);
                if (!bureauEmetteur.equals(etapeCourante.getParapheur())) {
                    notificationCenter.broadcastNotification(deskUtilsService.getDeskOwners(bureauEmetteur), notificationEmetteur);
                }
                notificationEmetteur.setTarget(Notification.Target.secretariat);
                notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureauPrecedent), notificationEmetteur);
            }
            // Notification au secretariat
            Boolean isSignPapier = (Boolean) nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_PAPIER);


            Notification notificationSecretariat = null;
            if (isSignPapier != null && isSignPapier && EtapeCircuit.ETAPE_SIGNATURE.equals(etapeCourante.getActionDemandee())) {
                notificationSecretariat = new Notification(
                        Notification.Target.secretariat.name(),
                        NotificationCenter.Reason.print,
                        etapeCourante.getParapheur(),
                        roleNotifie,
                        nomValideur,
                        username,
                        roleValideur,
                        NotificationCenter.VALUE_TYPE_DOSSIER,
                        dossier,
                        dossierTitle,
                        etapeCourante.getActionDemandee(),
                        etapePrecedente.getActionDemandee(),
                        etapePrecedente.getAnnotation(),
                        CorbeillesService.A_IMPRIMER);
            } else if(notifySecretaire) {
                notificationSecretariat = new Notification(
                        Notification.Target.secretariat.name(),
                        reason,
                        etapeCourante.getParapheur(),
                        roleNotifie,
                        nomValideur,
                        username,
                        roleValideur,
                        NotificationCenter.VALUE_TYPE_DOSSIER,
                        dossier,
                        dossierTitle,
                        etapeCourante.getActionDemandee(),
                        etapePrecedente.getActionDemandee(),
                        etapePrecedente.getAnnotation(),
                        banetteOwner);
            }
            if(notificationSecretariat != null) {
                notificationCenter.broadcastNotification(deskUtilsService.getDeskSecretaries(etapeCourante.getParapheur()), notificationSecretariat);
            }
        }
    }


    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourRejet(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourRejet(NodeRef dossier) {

        if (!folderUtilsService.isRejected(dossier)) {
            return;
        }
        NotificationCenter.Reason reason = NotificationCenter.Reason.reject;
        String annotation = null;
        List<EtapeCircuit> circuit = folderUtilsService.getWorkflow(dossier);
        int indexEtapeRejet = 0;
        if ((circuit != null) && !circuit.isEmpty() && folderUtilsService.isOver(dossier) && circuit.get(0).getActionDemandee() != null) {
            for (EtapeCircuit etape : circuit) {
                // L'étape de rejet est la dernière validée.
                if (!etape.isApproved()) {
                    break;
                }
                annotation = etape.getAnnotation(); // annotation de l'étape de rejet
                indexEtapeRejet++;
            }
        }
        String username = AuthenticationUtil.getRunAsUser();
        String nomValideur = userUtilsService.getFullname(username);
        String dossierTitle = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);

        // Emetteur WS
        String emmeteurWS = (String) nodeService.getProperty(dossier, ParapheurModel.PROP_WS_EMETTEUR);
        if (emmeteurWS != null) {
            Notification notificationWS = new Notification(
                    Notification.Target.tiers.name(),
                    reason,
                    null,
                    "Création par WebService",
                    nomValideur,
                    username,
                    null,
                    NotificationCenter.VALUE_TYPE_DOSSIER,
                    dossier,
                    dossierTitle,
                    null,
                    null,
                    annotation, null);
            List<String> emmeteurWSList = new ArrayList<>(1);
            emmeteurWSList.add(emmeteurWS);
            notificationCenter.broadcastNotification(emmeteurWSList, notificationWS);
        }

        if(circuit == null) return;

        // Acteurs (propriétaires et délégués) de toutes les étapes précédemment validées
        int indexEtape = 1;
        for (EtapeCircuit etapeCircuit : circuit) {
            if (indexEtape == 1) {
                // Émetteur du dossier (on passe les délégués)
                Notification notificationCourants = new Notification(
                        Notification.Target.owner.name(),
                        reason,
                        etapeCircuit.getParapheur(),
                        etapeCircuit.getParapheurName(),
                        nomValideur,
                        username,
                        null, // On n'a pas le bureauCourant (par la v4)
                        NotificationCenter.VALUE_TYPE_DOSSIER,
                        dossier,
                        dossierTitle,
                        null,
                        null,
                        annotation,
                        CorbeillesService.RETOURNES);
                notificationCenter.broadcastNotification(deskUtilsService.getDeskOwners(etapeCircuit.getParapheur()), notificationCourants);
            } else { // etapes jusqu'au rejet et après l'émission

                if (indexEtape < indexEtapeRejet) { // etapes avant le rejet
                    List<String> actorsToNotify = new ArrayList<>(deskUtilsService.getDeskOwners(etapeCircuit.getParapheur()));
                    actorsToNotify.remove(AuthenticationUtil.getRunAsUser());
                    Notification notification = new Notification(
                            Notification.Target.previous.name(),
                            reason,
                            etapeCircuit.getParapheur(),
                            etapeCircuit.getParapheurName(),
                            nomValideur,
                            username,
                            null,
                            NotificationCenter.VALUE_TYPE_DOSSIER,
                            dossier,
                            dossierTitle,
                            null,
                            null,
                            annotation,
                            null); // Pas de mise à jour de banette pour les précédents
                    notificationCenter.broadcastNotification(actorsToNotify, notification);

                    String deleguePrecedentName = etapeCircuit.getParapheurName();
                    for (NodeRef delegue : deskUtilsService.getDesksOnDelegationPath(etapeCircuit.getParapheur())) {
                        String delegueName = (String) nodeService.getProperty(delegue, ContentModel.PROP_TITLE);
                        notificationCenter.broadcastNotification(deskUtilsService.getDeskOwners(delegue),
                                new Notification(
                                        Notification.Target.previous.name(),
                                        reason,
                                        delegue,
                                        delegueName + DELEGATION_STR + deleguePrecedentName + ")",
                                        nomValideur,
                                        username,
                                        null,
                                        NotificationCenter.VALUE_TYPE_DOSSIER,
                                        dossier,
                                        dossierTitle,
                                        null,
                                        null,
                                        annotation,
                                        null) // Pas de mise à jour de banette pour les précédents
                        );
                        deleguePrecedentName = delegueName;
                    }
                } else { // etape de rejet (on n'envoi pas de mail pour le bureau qui a rejeté)
                    Notification notification = new Notification(
                            Notification.Target.previous.name(),
                            reason,
                            etapeCircuit.getParapheur(),
                            etapeCircuit.getParapheurName(),
                            nomValideur,
                            username,
                            null,
                            NotificationCenter.VALUE_TYPE_DOSSIER,
                            dossier,
                            dossierTitle,
                            null,
                            null,
                            annotation,
                            CorbeillesService.A_TRAITER);
                    notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(etapeCircuit.getParapheur()), notification);

                    // Liste de notification de l'étape de rejet (étape courante)
                    Set<NodeRef> bureauxNotifiesEtape = etapeCircuit.getListeNotification();
                    if (bureauxNotifiesEtape != null) {

                        for (NodeRef bureauNotifieEtape : bureauxNotifiesEtape) {
                            Notification notificationDiffusion = new Notification(
                                    Notification.Target.previous.name(),
                                    reason,
                                    bureauNotifieEtape,
                                    nodeService.getProperty(bureauNotifieEtape, ContentModel.PROP_TITLE) + NOTIFICATION_LIST_STR,
                                    nomValideur,
                                    username,
                                    null,
                                    NotificationCenter.VALUE_TYPE_DOSSIER,
                                    dossier,
                                    dossierTitle,
                                    null,
                                    null,
                                    annotation, null);
                            notificationCenter.broadcastNotification(deskUtilsService.getDeskOwners(bureauNotifieEtape), notificationDiffusion);
                        }
                    }

                    break;
                }
            }
            indexEtape++;
        }
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourSuppressionAdmin(org.alfresco.service.cmr.repository.NodeRef, String, String)
     */
    @Override
    public void notifierPourSuppressionAdmin(NodeRef dossier, String statusTdt, String ackDate) {

        NodeRef emetteur = folderUtilsService.getEmetteur(dossier);
        String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
        String error = null;
        if (emetteur != null) {
            String roleEmetteur = (String) nodeService.getProperty(emetteur, ContentModel.PROP_TITLE);
            List<String> owners = deskUtilsService.getDeskOwners(emetteur);
            if(owners == null) {
                owners = new ArrayList<>();
            }
            owners.removeIf(new Predicate<String>() {
                @Override
                public boolean test(String s) {
                    return !parapheurUserPreferences.isNotificationsEnabledForUsername(s);
                }
            });
            if (!owners.isEmpty()) {

                // generating PDF File
                HashMap<String, File> attachments = new HashMap<>();
                try {
                    File pdfFile = folderUtilsService.generateFrontPage(dossier, statusTdt, ackDate);
                    // En PJ le bordereau, en souvenir...
                    attachments.put("Bordereau_" + dossierTitre + ".pdf", pdfFile);
                } catch(Exception e) {
                    logger.warn("OpenOffice non démarré, le borderau de signature ne sera pas envoyé au propriétaire du dossier.", e);
                }

                NodeRef bureauParent = nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_PARAPHEUR);
                String corbeilleName = nodeService.getPrimaryParent(nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_CORBEILLE)).getQName().getLocalName();
                String username = AuthenticationUtil.getRunAsUser();

                Notification notif = new Notification(
                        Notification.Target.owner.name(),
                        NotificationCenter.Reason.deleteAdmin,
                        emetteur,
                        roleEmetteur,
                        userUtilsService.getFullname(username),
                        username,
                        null, // pas de bureau, c'est un admin
                        NotificationCenter.VALUE_TYPE_DOSSIER,
                        dossier,
                        dossierTitre,
                        null, null, null,
                        corbeilleName);

                notificationCenter.sendMailToUsers(notif, owners, attachments);

                List<Notification> notificationList = new ArrayList<>();
                notif.setUsersTarget(deskUtilsService.getDeskOwners(bureauParent));
                notificationList.add(notif);
                preparedNotificationsForDeletedDossier.put(dossier.getId(), notificationList);
            } else {
                error = "Le bureau émetteur \"" + roleEmetteur + "\" est actuellement sans propriétaire.";
            }
        } else {
            error = "Le dossier n'a pas d'émetteur.";
        }
        if (error != null) {
            logger.warn(error + " Aucun email ne sera envoyé.");
        }
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourSuppression(org.alfresco.service.cmr.repository.NodeRef, String, boolean, org.alfresco.service.cmr.repository.NodeRef, String)
     */
    @Override
    public void notifierPourSuppression(NodeRef emetteur, String dossierTitre, boolean isInSecretariat, NodeRef parentCorbeille, String id) {

        if (emetteur != null) {
            String roleEmetteur = (String) nodeService.getProperty(emetteur, ContentModel.PROP_TITLE);
            String corbeilleName = nodeService.getPrimaryParent(parentCorbeille).getQName().getLocalName();
            List<String> secretaires = deskUtilsService.getDeskSecretaries(emetteur);
            if (isInSecretariat) {
                if ((secretaires != null) && !secretaires.isEmpty()) {

                    Notification notif = new Notification(
                            Notification.Target.secretariat.name(),
                            NotificationCenter.Reason.delete,
                            emetteur,
                            roleEmetteur,
                            null, null, null,
                            NotificationCenter.VALUE_TYPE_DOSSIER,
                            id,
                            dossierTitre,
                            null, null, null,
                            corbeilleName);

                    notificationCenter.broadcastSocketNotification(secretaires, notif);
                }
            } else {
                List<String> owners = deskUtilsService.getDeskOwners(emetteur);
                if ((owners != null) && !owners.isEmpty()) {

                    Notification notif = new Notification(
                            Notification.Target.owner.name(),
                            NotificationCenter.Reason.delete,
                            emetteur,
                            roleEmetteur,
                            null, null, null,
                            NotificationCenter.VALUE_TYPE_DOSSIER,
                            id,
                            dossierTitre,
                            null, null, null,
                            corbeilleName);

                    notificationCenter.broadcastSocketNotification(owners, notif);

                    notif.setTarget(Notification.Target.secretariat);
                    notificationCenter.broadcastSocketNotification(secretaires, notif);
                }
            }
        }
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourRemord(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourRemord(NodeRef dossier) {

        NotificationCenter.Reason reason = NotificationCenter.Reason.remord;
        // l'ancienne étape courante est l'étape où le dossier se trouvait (étape après l'étape courante)
        // car les notifications se font après l'action de remord.
        EtapeCircuit ancienneEtapeCourante = null;
        EtapeCircuit etapeCourante = null;
        List<EtapeCircuit> circuit = folderUtilsService.getWorkflow(dossier);
        int indexEtapeCourante = 0;
        if ((circuit != null) && !circuit.isEmpty() && !folderUtilsService.isOver(dossier) && circuit.get(0).getActionDemandee() != null) {
            boolean found = false;
            for (EtapeCircuit etape : circuit) {
                if (found) {
                    ancienneEtapeCourante = etape;
                    break;
                } else if (!etape.isApproved()) {
                    etapeCourante = etape;
                    found = true;
                }
                indexEtapeCourante++;
            }
        }

        if (ancienneEtapeCourante != null) {
            boolean isEnPreparation = (indexEtapeCourante == 1);
            String username = AuthenticationUtil.getRunAsUser();
            String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
            String nomValideur = userUtilsService.getFullname(username);

            // Propriétaire de l'ancienne étape courante
            Notification notificationAnciensCourants = new Notification(
                    Notification.Target.previous.name(),
                    reason,
                    ancienneEtapeCourante.getParapheur(),
                    ancienneEtapeCourante.getParapheurName(),
                    nomValideur,
                    username,
                    null, // On n'a pas le bureauCourant (par la v4)
                    NotificationCenter.VALUE_TYPE_DOSSIER,
                    dossier,
                    dossierTitre,
                    ancienneEtapeCourante.getActionDemandee(),
                    etapeCourante.getActionDemandee(),
                    null,
                    CorbeillesService.A_TRAITER);
            notificationCenter.broadcastNotification(
                    deskUtilsService.getDeskOwners(ancienneEtapeCourante.getParapheur()),
                    notificationAnciensCourants);

            // Proprietaires de la nouvelle etape courante (ws)
            Notification notificationNouveauxCourants = new Notification(notificationAnciensCourants);
            notificationNouveauxCourants.setTarget(Notification.Target.current);
            notificationNouveauxCourants.setBureauNotifie(etapeCourante.getParapheur());
            notificationNouveauxCourants.setRoleNotifie(deskUtilsService.getDeskName(etapeCourante.getParapheur()));
            if (isEnPreparation) {
                notificationNouveauxCourants.setBanette(CorbeillesService.EN_PREPARATION);
            }
            notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(etapeCourante.getParapheur()), notificationNouveauxCourants);

            // Delegués de l'ancienne étape courante
            String deleguePrecedentName = ancienneEtapeCourante.getParapheurName();
            for (NodeRef delegue : deskUtilsService.getDesksOnDelegationPath(ancienneEtapeCourante.getParapheur())) {
                String delegueName = (String) nodeService.getProperty(delegue, ContentModel.PROP_TITLE);
                notificationCenter.broadcastNotification(
                        deskUtilsService.getDeskOwners(delegue),
                        new Notification(
                                Notification.Target.previous.name(),
                                reason,
                                delegue,
                                delegueName + DELEGATION_STR + deleguePrecedentName + ")",
                                nomValideur,
                                username,
                                null, // On n'a pas le bureauCourant (par la v4)
                                NotificationCenter.VALUE_TYPE_DOSSIER,
                                dossier,
                                dossierTitre,
                                ancienneEtapeCourante.getActionDemandee(),
                                etapeCourante.getActionDemandee(),
                                null,
                                CorbeillesService.DELEGUES)
                );
                deleguePrecedentName = delegueName;
            }

            // Delegués de la nouvelle étape courante seulement si pas en préparation
            if (!isEnPreparation) {
                deleguePrecedentName = etapeCourante.getParapheurName();
                for (NodeRef delegue : deskUtilsService.getDesksOnDelegationPath(etapeCourante.getParapheur())) {
                    String delegueName = (String) nodeService.getProperty(delegue, ContentModel.PROP_TITLE);
                    notificationCenter.broadcastSocketNotification(
                            deskUtilsService.getDeskOwners(delegue),
                            new Notification(
                                    Notification.Target.delegues.name(),
                                    reason,
                                    delegue,
                                    delegueName + DELEGATION_STR + deleguePrecedentName + ")",
                                    nomValideur,
                                    username,
                                    null, // On n'a pas le bureauCourant (par la v4)
                                    NotificationCenter.VALUE_TYPE_DOSSIER,
                                    dossier,
                                    dossierTitre,
                                    ancienneEtapeCourante.getActionDemandee(),
                                    etapeCourante.getActionDemandee(),
                                    null,
                                    CorbeillesService.DELEGUES)
                    );
                    deleguePrecedentName = delegueName;
                }
            }

            // La liste de diffusion de la nouvelle étape courante.
            Set<NodeRef> bureauxNotifies = etapeCourante.getListeNotification();
            if (bureauxNotifies != null) {
                for (NodeRef bureauNotifie : bureauxNotifies) {
                    notificationCenter.broadcastNotification(
                            deskUtilsService.getDeskOwners(bureauNotifie),
                            new Notification(
                                    Notification.Target.previous.name(),
                                    reason,
                                    bureauNotifie,
                                    nodeService.getProperty(bureauNotifie, ContentModel.PROP_TITLE) + NOTIFICATION_LIST_STR,
                                    nomValideur,
                                    username,
                                    null, // On n'a pas le bureauCourant (par la v4)
                                    NotificationCenter.VALUE_TYPE_DOSSIER,
                                    dossier,
                                    dossierTitre,
                                    ancienneEtapeCourante.getActionDemandee(),
                                    etapeCourante.getActionDemandee(),
                                    null, null)
                    );
                }
            }
        }
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourSecretariat(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourSecretariat(NodeRef dossier) {
        NodeRef bureau = nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_PARAPHEUR);
        String username = AuthenticationUtil.getRunAsUser();
        String nomValideur = userUtilsService.getFullname(username);
        String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
        String banetteName = nodeService.getPrimaryParent(nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_CORBEILLE)).getQName().getLocalName();

        Notification notification = new Notification(
                Notification.Target.secretariat.name(),
                NotificationCenter.Reason.reviewing,
                bureau,
                (String) nodeService.getProperty(bureau, ContentModel.PROP_TITLE),
                nomValideur,
                username,
                null, // On n'a pas le bureauCourant (par la v4)
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                null,
                null,
                null,
                CorbeillesService.SECRETARIAT);

        if (this.nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT)) {

            notificationCenter.broadcastNotification(deskUtilsService.getDeskSecretaries(bureau), notification);

            // Socket notification chez les proprietaires
            Notification notifOwner = new Notification(notification);
            notifOwner.setTarget(Notification.Target.previous);
            NodeRef emetteur = nodeUtilsService.getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);

            Boolean effectue = (Boolean) this.nodeService.getProperty(emetteur, ParapheurModel.PROP_EFFECTUEE);
            if(effectue != null && effectue) {
                notifOwner.setBanette(CorbeillesService.A_TRAITER);
            } else {
                notifOwner.setBanette(CorbeillesService.EN_PREPARATION);
                notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureau), notifOwner);
            }

            notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureau), notifOwner);

        } else {
            Notification notif = new Notification(notification);
            notif.setTarget(Notification.Target.current);
            notif.setBanette(banetteName);
            notificationCenter.broadcastNotification(deskUtilsService.getDeskOwners(bureau), notif);
            notif.setTarget(Notification.Target.secretariat);
            notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureau), notif);

            // Socket notification chez les secretaires
            Notification notifSecretaires = new Notification(notification);
            notifSecretaires.setTarget(Notification.Target.previous);
            notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureau), notifSecretaires);
        }
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourArchivage(org.alfresco.service.cmr.repository.NodeRef, String)
     */
    @Override
    public void notifierPourArchivage(NodeRef dossier, String nomArchive) {
        // Pour l'instant seule la liste de diffusion de l'étape d'archvage est notifiée par mail,
        // Il ne faudrait pas notifier l'émetteur ou autre?
        List<EtapeCircuit> circuit = folderUtilsService.getWorkflow(dossier);
        EtapeCircuit etapeCourante = circuit.get(circuit.size() - 1);
        Set<NodeRef> bureauxNotifiesEtape = etapeCourante.getListeNotification();
        String username = AuthenticationUtil.getRunAsUser();
        String nomValideur = userUtilsService.getFullname(username);
        String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
        if (bureauxNotifiesEtape != null) {
            for (NodeRef bureauNotifieEtape : bureauxNotifiesEtape) {
                notificationCenter.broadcastNotification(
                        deskUtilsService.getDeskOwners(bureauNotifieEtape),
                        new Notification(
                                Notification.Target.diff.name(),
                                NotificationCenter.Reason.archive,
                                bureauNotifieEtape,
                                nodeService.getProperty(bureauNotifieEtape, ContentModel.PROP_TITLE) + NOTIFICATION_LIST_STR,
                                nomValideur,
                                username,
                                null,
                                NotificationCenter.VALUE_TYPE_DOSSIER,
                                dossier,
                                dossierTitre,
                                null,
                                etapeCourante.getActionDemandee(),
                                nomArchive,
                                null)
                );
                // On stocke le nom de l'archive dans le champs prévu pour
                // l'annotation publique, comme on n'en a pas besoin ici.
            }
        }
        NodeRef bureauCourant = etapeCourante.getParapheur();
        Notification notificationCourants = new Notification(
                Notification.Target.previous.name(),
                NotificationCenter.Reason.archive,
                bureauCourant,
                etapeCourante.getParapheurName(),
                nomValideur,
                username,
                null, // On n'a pas le bureauCourant (par la v4)
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                null,
                etapeCourante.getActionDemandee(),
                null,
                folderUtilsService.isRejected(dossier) ? CorbeillesService.RETOURNES : CorbeillesService.A_ARCHIVER);
        List<Notification> notifsToSend = new ArrayList<>();
        notificationCourants.setUsersTarget(deskUtilsService.getDeskOwners(bureauCourant));
        notifsToSend.add(notificationCourants);

        Notification notificationSecretaire = new Notification(
                Notification.Target.previous.name(),
                NotificationCenter.Reason.archive,
                bureauCourant,
                etapeCourante.getParapheurName(),
                nomValideur,
                username,
                null, // On n'a pas le bureauCourant (par la v4)
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                null,
                etapeCourante.getActionDemandee(),
                null,
                folderUtilsService.isRejected(dossier) ? CorbeillesService.RETOURNES : CorbeillesService.A_ARCHIVER);
        notificationSecretaire.setTarget(Notification.Target.secretariat);
        notificationSecretaire.setUsersTarget(deskUtilsService.getDeskSecretaries(bureauCourant));
        notifsToSend.add(notificationSecretaire);

        this.preparedNotificationsForDeletedDossier.put(dossier.getId(), notifsToSend);
    }

    @Override
    public void sendPreparedNotificationsForDossier(String id) {
        List<Notification> notifsToSend = preparedNotificationsForDeletedDossier.get(id);
        if(notifsToSend != null && !notifsToSend.isEmpty()) {
            for(Notification n : notifsToSend) {
                notificationCenter.broadcastSocketNotification(n.getUsersTarget(), n);
            }
            preparedNotificationsForDeletedDossier.remove(id);
        }
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourDeplacement(org.alfresco.service.cmr.repository.NodeRef, String, org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourDeplacement(NodeRef dossier, String banette, NodeRef bureauPrecedent) {
        String username = AuthenticationUtil.getRunAsUser();
        AuthenticationUtil.setRunAsUser(AuthenticationUtil.getAdminUserName());

        NodeRef bureauCourant = nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_PARAPHEUR);
        String roleNotifie = deskUtilsService.getDeskName(bureauCourant);
        NotificationCenter.Reason reason = NotificationCenter.Reason.moveAdmin;
        String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
        EtapeCircuit etapeCourante = folderUtilsService.getCurrentStep(dossier);
        String nomValideur = userUtilsService.getFullname(username);

        // Proprietaires
        Notification notificationCourants = new Notification(
                Notification.Target.current.name(),
                reason,
                bureauCourant,
                roleNotifie,
                nomValideur,
                username,
                null,  // c'est l'admin
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                etapeCourante.getActionDemandee(),
                null,
                null,
                banette);
        notificationCenter.broadcastNotification(deskUtilsService.getDeskOwners(etapeCourante.getParapheur()), notificationCourants);

        // Si le dossier était "à traiter", on notifie les proprietaires et les délégués (ws)
        if (CorbeillesService.A_TRAITER.equals(banette)) {
            // Proprietaires precedents pour mise à jour de banettes (websockets)
            Notification notificationPrecedents = new Notification(notificationCourants);
            notificationPrecedents.setTarget(Notification.Target.previous);
            notificationPrecedents.setBureauNotifie(bureauPrecedent);
            String roleNotifiePrecedent = deskUtilsService.getDeskName(bureauPrecedent);
            notificationPrecedents.setRoleNotifie(roleNotifiePrecedent);
            notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureauPrecedent), notificationPrecedents);

            // Delegués
            String deleguePrecedentName = roleNotifie;
            for (NodeRef bureauDelegue : deskUtilsService.getDesksOnDelegationPath(bureauCourant)) {
                String delegueName = (String) nodeService.getProperty(bureauDelegue, ContentModel.PROP_TITLE);
                Notification notificationDelegues = new Notification(
                        Notification.Target.delegues.name(),
                        reason,
                        bureauDelegue,
                        delegueName + DELEGATION_STR + deleguePrecedentName + ")",
                        nomValideur,
                        username,
                        null,
                        NotificationCenter.VALUE_TYPE_DOSSIER,
                        dossier,
                        dossierTitre,
                        etapeCourante.getActionDemandee(),
                        null,
                        null,
                        CorbeillesService.DELEGUES);
                notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureauDelegue), notificationDelegues);

                deleguePrecedentName = delegueName;
            }

            // Delegués bureau précédent
            deleguePrecedentName = roleNotifiePrecedent;
            for (NodeRef bureauDelegue : deskUtilsService.getDesksOnDelegationPath(bureauPrecedent)) {
                String delegueName = (String) nodeService.getProperty(bureauDelegue, ContentModel.PROP_TITLE);
                Notification notificationDelegues = new Notification(
                        Notification.Target.previous.name(),
                        reason,
                        bureauDelegue,
                        delegueName + DELEGATION_STR + deleguePrecedentName + ")",
                        nomValideur,
                        username,
                        null,
                        NotificationCenter.VALUE_TYPE_DOSSIER,
                        dossier,
                        dossierTitre,
                        etapeCourante.getActionDemandee(),
                        null,
                        null,
                        CorbeillesService.DELEGUES);
                notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureauDelegue), notificationDelegues);

                deleguePrecedentName = delegueName;
            }
        }
        // Si le dossier était au secretariat, on notifie seulement le secretariat (web sockets)
        else if (CorbeillesService.SECRETARIAT.equals(banette)) {
            // Secretariat
            Notification notificationSecretariat = new Notification(notificationCourants);
            notificationSecretariat.setTarget(Notification.Target.secretariat);
            notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureauCourant), notificationSecretariat);

            // Secretariat de l'etape precedente pour mise à jour de banettes (websockets)
            Notification notificationPrecedents = new Notification(notificationCourants);
            notificationPrecedents.setTarget(Notification.Target.previous);
            notificationPrecedents.setBureauNotifie(bureauPrecedent);
            String roleNotifiePrecedent = deskUtilsService.getDeskName(bureauPrecedent);
            notificationPrecedents.setRoleNotifie(roleNotifiePrecedent);
            notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureauPrecedent), notificationPrecedents);
        }
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourCreation(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourCreation(NodeRef dossier, NodeRef bureau) {
        String username = AuthenticationUtil.getRunAsUser();
        boolean isSecretaire = deskUtilsService.isUserSecretary(bureau, username);
        String nomValideur = userUtilsService.getFullname(AuthenticationUtil.getRunAsUser());
        String corbeille = isSecretaire ? CorbeillesService.SECRETARIAT : CorbeillesService.EN_PREPARATION;
        Notification notification = new Notification(
                Notification.Target.owner.name(),
                NotificationCenter.Reason.create,
                bureau,
                null, // non utilisé ici
                nomValideur,
                username,
                null, // non utilisé ici
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                null, // Le titre peut ne pas être renseigné au début de la création.
                null,
                null,
                null, corbeille);
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureau), notification);

        // Secretariat
        Notification notificationSecretariat = new Notification(notification);
        notificationSecretariat.setTarget(Notification.Target.secretariat);
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureau), notificationSecretariat);
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourTdT(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourTdT(NodeRef dossier, NodeRef bureau) {
        String username = AuthenticationUtil.getRunAsUser();
        String nomValideur = userUtilsService.getFullname(AuthenticationUtil.getRunAsUser());
        String corbeille = CorbeillesService.A_TRAITER;
        Notification notification = new Notification(
                Notification.Target.current.name(),
                NotificationCenter.Reason.emit,
                bureau,
                null, // non utilisé ici
                nomValideur,
                username,
                null, // non utilisé ici
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                null, // Le titre peut ne pas être renseigné au début de la création.
                null,
                null,
                null, corbeille);
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureau), notification);

        // Secretariat
        Notification notificationSecretariat = new Notification(notification);
        notificationSecretariat.setTarget(Notification.Target.secretariat);
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureau), notificationSecretariat);
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourPassageSignaturePapier(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourPassageSignaturePapier(NodeRef dossier) {
        NodeRef bureau = nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_PARAPHEUR);
        String username = AuthenticationUtil.getRunAsUser();
        String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
        String nomValideur = userUtilsService.getFullname(username);
        Notification notification = new Notification(
                Notification.Target.secretariat.name(),
                NotificationCenter.Reason.print,
                bureau,
                null, // non utilisé ici
                nomValideur,
                username,
                null, // non utilisé ici
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                null,
                null,
                null,
                CorbeillesService.A_IMPRIMER);

        // Since it's added to the secretary's printable-folder, secretaries has to be notified.
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureau), notification);
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourPassageSignaturePapier(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourSignInfo(@NotNull NodeRef dossier, @NotNull HashMap<String, String> infos) {
        EtapeCircuit etapeCourante = null;
        List<EtapeCircuit> circuit = folderUtilsService.getWorkflow(dossier);
        if (circuit != null) {
            for (EtapeCircuit etape : circuit) {
                if (!etape.isApproved()) {
                    etapeCourante = etape;
                    break;
                }
            }
        }

        NodeRef bureau = nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_PARAPHEUR);
        String username = AuthenticationUtil.getRunAsUser();
        String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
        Notification notification =  new Notification(
                Notification.Target.current.name(),
                NotificationCenter.Reason.signInfo,
                bureau,
                null, // non utilisé ici
                null,
                username,
                null, // non utilisé ici
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                DossierService.ACTION_DOSSIER.SIGN_INFO.toString(),
                DossierService.ACTION_DOSSIER.SIGN_INFO.toString(),
                new JSONObject(infos).toString(),
                null
        );

        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureau), notification);

        if(etapeCourante != null) {
            for (NodeRef bureauDelegue : deskUtilsService.getDesksOnDelegationPath(etapeCourante.getParapheur())) {
                Notification notificationDelegues =  new Notification(
                        Notification.Target.delegues.name(),
                        NotificationCenter.Reason.signInfo,
                        bureauDelegue,
                        null, // non utilisé ici
                        null,
                        username,
                        null, // non utilisé ici
                        NotificationCenter.VALUE_TYPE_DOSSIER,
                        dossier,
                        dossierTitre,
                        DossierService.ACTION_DOSSIER.SIGN_INFO.toString(),
                        DossierService.ACTION_DOSSIER.SIGN_INFO.toString(),
                        new JSONObject(infos).toString(),
                        null
                );
                notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureauDelegue), notificationDelegues);
            }
        }
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourEdition(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourEdition(NodeRef dossier) {
        NodeRef bureau = nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_PARAPHEUR);
        String username = AuthenticationUtil.getRunAsUser();
        boolean isSecretaire = deskUtilsService.isUserSecretary(bureau, username);
        String corbeille = isSecretaire ? CorbeillesService.SECRETARIAT : CorbeillesService.EN_PREPARATION;
        String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
        String nomValideur = userUtilsService.getFullname(username);
        Notification notification = new Notification(
                Notification.Target.owner.name(),
                NotificationCenter.Reason.edit,
                bureau,
                null, // non utilisé ici
                nomValideur,
                username,
                null, // non utilisé ici
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                null,
                null,
                null,
                corbeille);
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureau), notification);
        // Secretariat
        Notification notificationSecretariat = new Notification(notification);
        notificationSecretariat.setTarget(Notification.Target.secretariat);
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureau), notificationSecretariat);
    }

    @Override
    public void notifierPourRetard(NodeRef dossier, NodeRef corbeille, boolean alertByMail) {
        notifierPourRetard(dossier, corbeille, alertByMail, false);
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourRetard(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef, boolean)
     */
    @Override
    public void notifierPourRetard(NodeRef dossier, NodeRef corbeille, boolean alertByMail, boolean doDigest) {
        NodeRef bureau = nodeUtilsService.getFirstParentOfType(corbeille, ParapheurModel.TYPE_PARAPHEUR);
        List<String> users = deskUtilsService.getDeskOwners(bureau);
        Date dateLimite = (Date) nodeService.getProperty(dossier, ParapheurModel.PROP_DATE_LIMITE);
        String dateStr = null;
        if (dateLimite != null) {
            dateStr = new SimpleDateFormat("dd/MM/yyyy").format(dateLimite);
        }
        Notification notification = new Notification(
                Notification.Target.current.name(),
                NotificationCenter.Reason.retard,
                bureau,
                deskUtilsService.getDeskName(bureau),
                null, // non utilisé ici
                null, // non utilisé ici
                null, // non utilisé ici
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE),
                folderUtilsService.getCurrentStep(dossier).getActionDemandee(),
                null,
                dateStr, // Hack : on met la date utilisée dans le ftl dans l'annotation
                null);
        if (alertByMail) {
            notificationCenter.broadcastNotification(users, notification, doDigest, false);
        }
        notificationCenter.broadcastSocketNotification(users, notification);
    }

    @Override
    public void notifierPourRetardSingleUser(NodeRef bureau, NodeRef dossier, String user) {
        Date dateLimite = (Date) nodeService.getProperty(dossier, ParapheurModel.PROP_DATE_LIMITE);
        String dateStr = null;
        if (dateLimite != null) {
            dateStr = new SimpleDateFormat("dd/MM/yyyy").format(dateLimite);
        }
        Notification notification = new Notification(
                Notification.Target.current.name(),
                NotificationCenter.Reason.retard,
                bureau,
                deskUtilsService.getDeskName(bureau),
                null, // non utilisé ici
                null, // non utilisé ici
                null, // non utilisé ici
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE),
                null, // non utilisé ici
                null,
                dateStr, // Hack : on met la date utilisée dans le ftl dans l'annotation
                null);
        notificationCenter.broadcastNotification(Collections.singletonList(user), notification, true, false);
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourDelegation(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef, boolean)
     */
    @Override
    public void notifierPourDelegation(NodeRef titulaire, NodeRef suppleant, boolean created) {
        String target = created ?
                Notification.Target.current.name() :
                Notification.Target.previous.name();
        String username = AuthenticationUtil.getRunAsUser();
        String nomSuppleant = (String) nodeService.getProperty(suppleant, ContentModel.PROP_TITLE);
        String nomTitulaire = (String) nodeService.getProperty(titulaire, ContentModel.PROP_TITLE);
        String nomValideur = userUtilsService.getFullname(username);

        Notification notification = new Notification(
                target,
                NotificationCenter.Reason.delegation,
                suppleant,
                nomSuppleant,
                nomValideur,
                username,
                nomTitulaire,
                NotificationCenter.VALUE_TYPE_BUREAU,
                null,
                null,
                null,
                null,
                null,
                CorbeillesService.DELEGUES);
        List<String> users = deskUtilsService.getDeskOwners(suppleant);
        notificationCenter.broadcastNotification(users, notification);
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierApresErreur(org.alfresco.service.cmr.repository.NodeRef, String)
     */
    @Override
    public void notifierApresErreur(NodeRef dossier, String msg) {
        String username = AuthenticationUtil.getRunAsUser();
        NodeRef bureau = nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_PARAPHEUR);
        String bureauName = (String) nodeService.getProperty(bureau, ContentModel.PROP_TITLE);
        String nomValideur = userUtilsService.getFullname(username);
        String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);

        Notification notification = new Notification(
                Notification.Target.current.name(),
                NotificationCenter.Reason.error,
                bureau,
                bureauName,
                nomValideur,
                username,
                bureauName,
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                null,
                null,
                msg,
                CorbeillesService.AUCUNE);
        List<String> users = new ArrayList<>();
        users.add(AuthenticationUtil.getRunAsUser());
        AuthenticationUtil.getRunAsUser();
        notificationCenter.broadcastSocketNotification(users, notification);
    }

    @Override
    public void notifierPourCorbeilleVirtuelle(NodeRef dossier, NodeRef corbeille, String corbeilleName, boolean toAdd) {
        String username = AuthenticationUtil.getRunAsUser();

        NodeRef bureau = nodeUtilsService.getFirstParentOfType(corbeille, ParapheurModel.TYPE_PARAPHEUR);
        String bureauName = (String) nodeService.getProperty(bureau, ContentModel.PROP_TITLE);
        String nomValideur = userUtilsService.getFullname(username);
        String dossierTitre = "";
        if(nodeService.exists(dossier)) {
            dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);
        }

        Notification notification = new Notification(
                Notification.Target.current.name(),
                toAdd ? NotificationCenter.Reason.create : NotificationCenter.Reason.delete,
                bureau,
                bureauName,
                nomValideur,
                username,
                bureauName,
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                null,
                null,
                null,
                corbeilleName);

        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureau), notification);
        notification.setTarget(Notification.Target.secretariat);
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureau), notification);
    }

    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourRAZ(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourRAZ(NodeRef dossier) {
        String username = AuthenticationUtil.getRunAsUser();
        NodeRef bureau = nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_PARAPHEUR);
        String bureauName = (String) nodeService.getProperty(bureau, ContentModel.PROP_TITLE);
        String nomValideur = userUtilsService.getFullname(username);
        String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);

        Notification notification = new Notification(
                Notification.Target.current.name(),
                NotificationCenter.Reason.raz,
                bureau,
                bureauName,
                nomValideur,
                username,
                bureauName,
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                null,
                null,
                null,
                CorbeillesService.EN_PREPARATION);

        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureau), notification);
        notification.setTarget(Notification.Target.secretariat);
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskSecretaries(bureau), notification);

        // Socket notification pour la banette "retournés"
        Notification notifRetournes = new Notification(notification);
        notifRetournes.setTarget(Notification.Target.previous);
        notifRetournes.setBanette(CorbeillesService.RETOURNES);
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureau), notifRetournes);
    }


    /**
     * @see org.adullact.iparapheur.repo.notification.NotificationService#notifierPourEnchainementCircuit(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void notifierPourEnchainementCircuit(NodeRef dossier) {
        String username = AuthenticationUtil.getRunAsUser();
        NodeRef bureau = nodeUtilsService.getFirstParentOfType(dossier, ParapheurModel.TYPE_PARAPHEUR);
        String bureauName = (String) nodeService.getProperty(bureau, ContentModel.PROP_TITLE);
        String nomValideur = userUtilsService.getFullname(username);
        String dossierTitre = (String) nodeService.getProperty(dossier, ContentModel.PROP_TITLE);

        Notification notification = new Notification(
                Notification.Target.current.name(),
                NotificationCenter.Reason.chain,
                bureau,
                bureauName,
                nomValideur,
                username,
                bureauName,
                NotificationCenter.VALUE_TYPE_DOSSIER,
                dossier,
                dossierTitre,
                null,
                null,
                null,
                CorbeillesService.A_TRAITER);

        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureau), notification);

        // Socket notification pour la banette "retournés"
        Notification notifRetournes = new Notification(notification);
        notifRetournes.setTarget(Notification.Target.previous);
        notifRetournes.setBanette(CorbeillesService.A_ARCHIVER);
        notificationCenter.broadcastSocketNotification(deskUtilsService.getDeskOwners(bureau), notifRetournes);
    }
}
