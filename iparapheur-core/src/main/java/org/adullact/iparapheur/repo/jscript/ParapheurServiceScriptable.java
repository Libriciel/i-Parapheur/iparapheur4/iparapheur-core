/*
 * Version 2.1
 * CeCILL Copyright (c) 2008-2014, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.repo.admin.UsersService;
import org.adullact.iparapheur.comparator.NameNodeRefComparator;
import org.adullact.iparapheur.repo.office.OfficeConnectionTester;
import org.adullact.iparapheur.rules.bean.CustomProperty;
import org.adullact.iparapheur.util.NativeUtils;
import org.adullact.utils.StringUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.jscript.BaseScopableProcessorExtension;
import org.alfresco.repo.jscript.ScriptNode;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.NativeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.util.Base64;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Exposition de certaines méthodes des services ParapheurService, S2lowService,
 * WorkflowService et TypeService vers WebScripts
 *
 * <p>
 * NB: ce n'est pas forcément très malin de tout mélanger ainsi, à la longue...
 *  En effet, le package s'est bien enrichi. TODO: refactoring!
 * <p>
 * Exposé dans les WebScripts sous le nom declaré dans <code>module-context.xml</code>
 * </p>
 * <h3>Declaration</h3>
 * module-context.xml
 * <pre>
 * &lt;bean id="xxx" parent="xxx" class="org.adullact.iparapheur.repo.jscript.<b>ParapheurServiceScriptable</b>"&gt;
 *   &lt;property name="extensionName"&gt;
 *     &lt;value&gt;<b>iparapheur</b>&lt;/value&gt;
 *   &lt;/property&gt;
 * </pre>
 * <h3>Usage</h3>
 * myWebScript.get.js
 * <pre>
 * model.parapheurs = <b>iparapheur</b>.getParapheurs();
 * </pre>
 * 
 * @author S Vast - ADULLACT Projet
 */
public class ParapheurServiceScriptable extends BaseScopableProcessorExtension {

    private static Logger logger = Logger.getLogger(ParapheurServiceScriptable.class);

    private ParapheurService parapheurService;

    private S2lowService s2lowService;

    private NodeService nodeService;

    private AuthorityService authorityService;

    private WorkflowService workflowService;

    private TypesService typesService;

    public MetadataService getMetadataService() {
        return metadataService;
    }

    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }


    @Autowired
    private MetadataService metadataService;

    private ContentService contentService;

    private ServiceRegistry serviceRegistry;

    @Autowired
    private PersonService personService;
    
    private SearchService searchService;

    @Autowired
    private CorbeillesService corbeillesService;

    private NamespaceService namespaceService;

    @Autowired
    private UsersService usersService;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        ParapheurServiceScriptable.logger = logger;
    }

    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public void setS2lowService(S2lowService s2lowService) {
        this.s2lowService = s2lowService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    public void setTypesService(TypesService typesService) {
        this.typesService = typesService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }
    
    public ScriptNode getParapheurAsScriptNode(NodeRef parapheur) {

        return  new ScriptNode(parapheur, serviceRegistry, getScope());
    }

    public JSONObject getOfficeMetadatas() throws JSONException {
        JSONObject result = new JSONObject();
        for(Map.Entry<String, Object> entry :OfficeConnectionTester.getOpenOfficeMetadata().entrySet()) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    public boolean restartOffice() {
        return OfficeConnectionTester.restartOffice();
    }

    public List<ScriptNode> getOwnedParapheursAsScriptNode(String username) {
        List<NodeRef> parapheurs = parapheurService.getOwnedParapheurs(username);
        List<ScriptNode> nodes = null;

        if (parapheurs != null) {
            nodes = new ArrayList<ScriptNode>(parapheurs.size() + 1);
            for (NodeRef parapheur : parapheurs) {
                ScriptNode scriptNode = new ScriptNode(parapheur, serviceRegistry, getScope());
                Map<String, Object> properties = scriptNode.getProperties();
                nodes.add(scriptNode);
            }
        }
        return nodes;
    }

    public List<ScriptNode> getParapheursAsScriptNode() {
        List<NodeRef> parapheurs = parapheurService.getParapheurs();
        List<ScriptNode> nodes = null;

        if (parapheurs != null) {
            nodes = new ArrayList<ScriptNode>(parapheurs.size() + 1);
            for (NodeRef parapheur : parapheurs) {
                ScriptNode scriptNode = new ScriptNode(parapheur, serviceRegistry, getScope());
                nodes.add(scriptNode);
            }
        }
        return nodes;
    }
    
    public List<ScriptNode> getSecretariatParapheurAsScriptNode(String username) {
        List<NodeRef> parapheurs = parapheurService.getSecretariatParapheurs(username);
        List<ScriptNode> nodes = null;

        if (parapheurs != null) {
            nodes = new ArrayList<ScriptNode>(parapheurs.size() + 1);
            for (NodeRef parapheur : parapheurs) {
                ScriptNode scriptNode = new ScriptNode(parapheur, serviceRegistry, getScope());
                Map<String, Object> properties = scriptNode.getProperties();
                nodes.add(scriptNode);
            }
        }
        return nodes;
    }

    /**
     *
     * @deprecated ne gère pas les permissions, utiliser {@link org.adullact.iparapheur.repo.jscript.UsersServiceScriptable#addUserToBureau(String, String, boolean)} à la place.
     */
    @Deprecated
    public void addOwnerToBureau(NodeRef bureau, NodeRef user){
        parapheurService.addOwnerToBureau(bureau, user);
    }

    /**
     *
     * @deprecated ne gère pas les permissions, utiliser {@link org.adullact.iparapheur.repo.jscript.UsersServiceScriptable#removeUserFromBureau(String, String, boolean)} à la place.
     */
    @Deprecated
    public void removeOwnerFromBureau(NodeRef bureau, NodeRef user){
        parapheurService.removeOwnerFromBureau(bureau, user);
    }

    @SuppressWarnings("unused")
    public NodeRef getBestBureau(String username, NodeRef dossier) {

        List<NodeRef> parapheurs = parapheurService.getOwnedParapheurs(username);

        // Get the one that can possibly call Remorse

        List<EtapeCircuit> lstEtapes = parapheurService.getCircuit(dossier);
        EtapeCircuit etapePrecedente = null;
        if (lstEtapes != null) {
            for (EtapeCircuit etape : lstEtapes) {
                if (!etape.isApproved()) { // on a trouvé l'etape courante
                    break;                 // donc on sort de la boucle.
                } else {
                    etapePrecedente = etape;
                }
            }
        }

        if (etapePrecedente != null) {
            NodeRef previousParapheur = etapePrecedente.getParapheur();
            if (parapheurService.isParapheurOwnerOrDelegate(previousParapheur, username) ||
                (parapheurService.isParapheurSecretaire(previousParapheur, username))) {
                return previousParapheur;
            }
        }

        // Default case : the first not-null one

        if (parapheurs != null && parapheurs.size() > 0) {
            return parapheurs.get(0);
        } else {
            parapheurs = parapheurService.getSecretariatParapheurs(username);
            return (parapheurs != null && parapheurs.size() > 0) ? parapheurs.get(0) : null;
        }
    }


    public List<NodeRef> getOwnedParapheurs(String username) {
        return parapheurService.getOwnedParapheurs(username);
    }

    public void approve(NodeRef dossier) {
        parapheurService.approve(dossier);
    }

    
     public NodeRef createParapheur(String name, String title, String description) {
        /**
         * conversion de toutes les props requises en QNAME !!!
         */
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, name);
        properties.put(ContentModel.PROP_TITLE, title);
        properties.put(ContentModel.PROP_DESCRIPTION, description);
        properties.put(ParapheurModel.PROP_SECRETAIRES, new ArrayList<String>());
        properties.put(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, new ArrayList<String>());

        return parapheurService.createParapheur(properties);
     }

    public int getProfondeurHierarchie(NodeRef parapheur) {
        int profondeur = 0;
        List<AssociationRef> assocs = nodeService.getTargetAssocs(parapheur, ParapheurModel.ASSOC_HIERARCHIE);
        List<NodeRef> knownNode = new ArrayList<NodeRef>();
        while(!assocs.isEmpty()) {
            NodeRef target = assocs.get(0).getTargetRef();
            if(knownNode.contains(target)) {
                //Boucle dans la hierarchie
                return 0;
            } else {
                knownNode.add(target);
            }
            profondeur ++;
            assocs = nodeService.getTargetAssocs(target, ParapheurModel.ASSOC_HIERARCHIE);
        }

        return profondeur;
    }

    /**
     * Associer un VisuelPDF à un document principal...
     * 
     * @param docNodeRef  le nodeRef du document principal
     * @param visuelpdffilename  le nom de fichier PDF (sert à rien)
     * @param visuelPdfContent   le contenu byte array?
     * @return true si succes, false sinon
     */
    public boolean setVisuelPDF(org.alfresco.repo.jscript.ScriptNode docNodeRef, String visuelpdffilename, org.springframework.extensions.surf.util.InputStreamContent visuelPdfContent) {
        boolean success;

        if (docNodeRef == null || visuelpdffilename == null || visuelPdfContent == null) {
            success = false;
            System.out.println("\tsetVisuelPDF: un des parametres est NULL, abandon");
        } else {
            NodeRef nodeRef = docNodeRef.getNodeRef();
            System.out.println("\tsetVisuelPDF( docNodeRef=" + nodeRef + ", visuelpdffilename=" + visuelpdffilename + ", soustype=..." + " )");
            ContentWriter cw2 = contentService.getWriter(nodeRef, ParapheurModel.PROP_VISUEL_PDF, Boolean.TRUE);
            cw2.setMimetype(MimetypeMap.MIMETYPE_PDF);
            cw2.setEncoding("UTF-8");
            //if (logger.isDebugEnabled()) {
                logger.warn("### Actual PDF Content mimetype=" + visuelPdfContent.getMimetype());
            //}
            //InputStream cis2 = new ByteArrayInputStream(visuelPdfContent);
            //cw2.putContent(cis2);
            cw2.putContent(visuelPdfContent.getInputStream());
            
            // génération de la preview
            parapheurService.generatePreviewForNodeRef(nodeRef, false);

            success = true;
        }
        return success;
    }
    
    public boolean generatePreview(NodeRef dossier) {
        try{
            //Génération de l'aperçu flash des docs principaux
            for (NodeRef document : parapheurService.getMainDocuments(dossier)) {
                parapheurService.generatePreviewForNodeRef(document, false);
            }
            return true;
        }catch(Exception e){
            System.out.println("Erreur lors de la génération de l'image. Trace : "+e);
            return false;
        }
    }
    
    public List<String> getSavedTypes(NodeRef parapheur) {
        return parapheurService.getSavedTypes(parapheur);
    }

    public boolean isDossier(NodeRef nodeRef) {
        return parapheurService.isDossier(nodeRef);
    }
    
    public boolean isAdministrateurFonctionnelOf(String user, NodeRef nodeRef) {
        return parapheurService.isAdministrateurFonctionnelOf(user, nodeRef);
    }

    public boolean isAdministrateur(String user) {
        return parapheurService.isAdministrateur(user);
    }
    
    public NodeRef getParentParapheur(NodeRef nodeRef) {
        return parapheurService.getParentParapheur(nodeRef);
    }
    
    public Boolean isParapheur(NodeRef nodeRef) {
        return parapheurService.isParapheur(nodeRef);
    }
    
    public JSONArray getParapheurOwners(NodeRef nodeRef) throws JSONException {
        JSONArray ret = new JSONArray();
        List<String> users = parapheurService.getParapheurOwners(nodeRef);
        if(users != null) {
            for (String user : users) {
                if(!personService.personExists(user)) {
                    logger.error("User " + user + " does not exist, but is referenced in desk ID=" + nodeRef.getId());
                    continue;
                }
                NodeRef p = personService.getPerson(user, false);
                JSONObject person = new JSONObject();
                if(p != null && nodeService.exists(p)) {
                    Map<QName, Serializable> properties = nodeService.getProperties(p);
                    person.put("username", properties.get(ContentModel.PROP_USERNAME));
                    person.put("firstName", properties.get(ContentModel.PROP_FIRSTNAME));
                    person.put("lastName", properties.get(ContentModel.PROP_LASTNAME));
                    person.put("id", p.getId());
                }
                ret.put(person);
            }
        }
        return ret;
    }
    
    public JSONArray getParapheurSecretaires(NodeRef nodeRef) throws JSONException {
        JSONArray ret = new JSONArray();
        List<String> users = parapheurService.getSecretariatParapheur(nodeRef);
        for (String user : users) {
            NodeRef p = personService.getPerson(user);
            JSONObject person = new JSONObject();
            if(p != null && nodeService.exists(p)) {
                Map<QName, Serializable> properties = nodeService.getProperties(p);
                person.put("username", properties.get(ContentModel.PROP_USERNAME));
                person.put("firstName", properties.get(ContentModel.PROP_FIRSTNAME));
                person.put("lastName", properties.get(ContentModel.PROP_LASTNAME));
                person.put("id", p.getId());
            }
            ret.put(person);
        }
        return ret;
    }
    
    public List<NodeRef> getSecretariatParapheurs(String username) {
        return parapheurService.getSecretariatParapheurs(username);
    }

    public boolean updateBureau(NodeRef bureau, NativeObject json, NativeObject propNative, NativeObject assocsNative) {
        NativeObjectMapAdapter jsonData = new NativeObjectMapAdapter(json);
        NativeObjectMapAdapter properties = new NativeObjectMapAdapter(propNative);
        NativeObjectMapAdapter associations = new NativeObjectMapAdapter(assocsNative);

        HashMap<QName, Serializable> propertiesMap = new HashMap<QName, Serializable>();
        HashMap<QName, Serializable> assocsMap = new HashMap<QName, Serializable>();

        for (Object key : jsonData.keySet()) {
            //properties.put((String) key, (String) jsonData.get(key));
            Object prop = properties.get(key);
            Object value = jsonData.get(key);
            if(value instanceof NativeArray) {
                if((prop).equals("ph:delegations-possibles")) {
                    value = NativeUtils.nativeArrayToNodeRef((NativeArray) value);
                } else {
                    value = NativeUtils.nativeArrayToString((NativeArray) value);
                }
            }
            if(prop != null) {
                propertiesMap.put(QName.createQName((String) prop, namespaceService), (Serializable) value);
            } else {
                prop = associations.get(key);
                if(prop != null) {
                    assocsMap.put(QName.createQName((String) prop, namespaceService), (Serializable) jsonData.get(key));
                }
            }
        }

        return parapheurService.updateBureau(bureau, propertiesMap, assocsMap);
    }

    public String getParapheurOwnersAsString(NodeRef nodeRef) {
        String retVal = "";
        List<String> owners = parapheurService.getParapheurOwners(nodeRef);

        if (owners != null && !owners.isEmpty()) {
            retVal = StringUtils.generateStringFromList(owners);
        }
        return retVal;
    }

    public void setSignataire(NodeRef dossier, String nomS) {
        // parapheurService.setSignataire(dossier, nomS);
        parapheurService.setSignataire(dossier, parapheurService.getPrenomNomFromLogin(nomS));
    }

    public List<EtapeCircuit> getCircuit(NodeRef dossier) {
        return parapheurService.getCircuit(dossier);
    }

    public List<EtapeCircuit> getCircuit(NodeRef dossier, String type, String soustype) {
        return parapheurService.getCircuit(dossier, type, soustype);
    }
    
    public boolean isRejete(String dossier) {
        return parapheurService.isRejete(new NodeRef(dossier));
    }

    /**
     * Utiliser getActeursCourant(dossier)
     *
     * @param dossier
     * @return
     * @deprecated
     */
    @Deprecated
    public String getActeurCourant(NodeRef dossier) {
        return parapheurService.getActeurCourant(dossier);
    }

    public List<String> getActeursCourant(NodeRef dossier) {
        return parapheurService.getActeursCourant(dossier);
    }

    public boolean isActeurCourant(NodeRef dossier, String userName) {
        return parapheurService.isActeurCourant(dossier, userName);
    }

    public String getAnnotationPublique(NodeRef dossier) {
        return parapheurService.getAnnotationPublique(dossier);
    }

    public void setAnnotationPublique(NodeRef dossier, String textS) {
        parapheurService.setAnnotationPublique(dossier, textS);
    }

    public String getAnnotationPrivee(NodeRef dossier) {
        logger.debug("getAnnotationPrivee IN");
        return parapheurService.getAnnotationPrivee(dossier);
    }

    public String getAnnotationPriveePrecedente(NodeRef dossier) {
        return parapheurService.getAnnotationPriveePrecedente(dossier);
    }

    public void setAnnotationPrivee(NodeRef dossier, String textS) {
        parapheurService.setAnnotationPrivee(dossier, textS);
    }

    public void setCircuitAndAspect(NodeRef dossier, String type, String sousType) {
        System.out.println("\tsetCircuitAndAspect( Noderef=" + dossier + ", type=" + type + ", soustype=" + sousType + " )");
        SavedWorkflow circuit = workflowService.getSavedWorkflow(typesService.getWorkflow(type, sousType));
        if (circuit == null) {
            System.out.println("BAD :  circuit == null");
        } else {
            parapheurService.setCircuit(dossier, circuit.getCircuit());
        }
        /**
         * Aspect
         */
        // Map<QName, Serializable> typageProps = new HashMap<QName, Serializable>();
        Map<QName, Serializable> typageProps = parapheurService.getTypeMetierProperties(type);
        String protocole = (String) typageProps.get(ParapheurModel.PROP_TDT_PROTOCOLE);
        if (protocole != null && protocole.equalsIgnoreCase("HELIOS")) {
            // FIXME EVIL HARD-CODED
            typageProps.put(ParapheurModel.PROP_XPATH_SIGNATURE, ".");
        } else {
            System.out.println("protocole == null, or !=helios");
        }
        nodeService.addAspect(dossier, ParapheurModel.ASPECT_TYPAGE_METIER, typageProps);
    }

    public Map<QName, Map<String, String>> getMetadatasMap(String type, String sstype) {
        Map<QName, Map<String, String>> retVal = new HashMap<QName, Map<String, String>>();

        if (type != null && sstype != null && !type.isEmpty() && !sstype.isEmpty()) {
            Map<String, Map<String, String>> mds = typesService.getMetadatasMap(type, sstype);
            if (mds != null) {
                for (String key : mds.keySet()) {
                    retVal.put(QName.createQName(key), mds.get(key));
                }
            }
        }

        return retVal;
    }

    public NodeRef getCircuitRef(String type, String sousType) {
        // DEPRECATED: return parapheurService.getCircuitRef(type, sousType);
        // Workflow for selected type / subtype
        Map<String, String> metadataValues = new HashMap<String, String>();
        List<CustomProperty<?>> customProperties = new ArrayList<CustomProperty<?>>();

        Map<QName, Map<String, String>> metadata_defaults = getMetadatasMap(type, sousType);
        for (QName qName: metadata_defaults.keySet()) {
            metadataValues.put(qName.getLocalName(), metadata_defaults.get(qName).get("default"));
        }

        for (CustomMetadataDef def : metadataService.getMetadataDefs()) {
            /**
             * Ne renseigner la map QUE SI l'utilisateur a valorise
             * les champs de saisie.
             *
             * ET POUR LA SELECTION DE CIRCUIT AVEC SCRIPT ???
             * RAPPEL : Les métadonnées sont définies APRES la selection de type/sous-type
             */
            String tempVal = metadataValues.get(def.getName().getLocalName());
            if (tempVal != null) {
                if (!tempVal.isEmpty()) {
                    customProperties.add(new CustomProperty<Object>(def.getName().getLocalName(),
                            metadataService.getValueFromString(def.getType(), metadataValues.get(def.getName().getLocalName()))));
                }
            }
        }
        NodeRef circuit = null;
        try {
            circuit = typesService.getWorkflowHandledError(null, type, sousType, customProperties, false, false);
        } catch(Throwable t) {
            logger.error(t.getMessage());
        }

        return circuit;
    }

    public String getTypeMetierSigFormat(String type) {
        return (String) parapheurService.getTypeMetierProperties(type).get(ParapheurModel.PROP_SIGNATURE_FORMAT);
    }
    
    public SavedWorkflow loadSavedWorkflow(NodeRef nodeRef) {
        return workflowService.getSavedWorkflow(nodeRef);
    }

    public SavedWorkflow loadSavedWorkflow(NodeRef nodeRef, boolean resolveHierarchy, NodeRef emetteurRef, NodeRef dossierRef) {
        return workflowService.getSavedWorkflow(nodeRef, resolveHierarchy, emetteurRef, dossierRef);
    }

    public List<EtapeCircuit> getCircuitHierarchique(NodeRef emetteur) {
        return parapheurService.getCircuitHierarchique(emetteur);
    }

//    public Map<String, NodeRef> getSavedWorkflows() {
//        return parapheurService.getSavedWorkflows();
//    }

    public NodeRef getWorkflowByName(String name) {
        return workflowService.getWorkflowByName(name);
    }

    public Map<String, NodeRef> getPublicWorkflows() {
        return parapheurService.getPublicWorkflows();
    }

    public Map<String, NodeRef> getOwnedWorkflows() {
        return parapheurService.getOwnedWorkflows();
    }

    public Map<String, NodeRef> getAllWorkflows() {
        return parapheurService.getAllWorkflows();
    }

    public List<SavedWorkflow> getEditableWorkflowsList() {
        return workflowService.getEditableWorkflows();
    }

    public List<SavedWorkflow> getReadOnlyWorkflowsList() {
        return workflowService.getReadOnlyWorkflows();
    }
    
     public List<NodeRef> getParapheursOnDelegationPath(NodeRef parapheur) {
        return parapheurService.getParapheursOnDelegationPath(parapheur);
    }

    public List<NodeRef> getParapheurs() {
        
        return parapheurService.getParapheurs();
    }
    
    public List<NodeRef> getParapheursFromName(String name){
        return parapheurService.getParapheursFromName(name);
    }

    public boolean isParapheurOwner(NodeRef parapheur, String username) {
        return parapheurService.isParapheurOwner(parapheur, username);
    }

    public boolean isParapheurOwnerOrDelegate(NodeRef parapheur, String username) {
        return parapheurService.isParapheurOwnerOrDelegate(parapheur, username);
    }
    
    public boolean isParapheurSecretaire(NodeRef dossier, String username) {
        return parapheurService.isParapheurSecretaire(dossier, username);
    }

    public List<NodeRef> getParapheursAtLevel(NodeRef levelRef) {
        List<NodeRef> resultList;// = null;
        if (levelRef == null) {
            List<NodeRef> parapheurs = this.parapheurService.getParapheurs();
            resultList = new ArrayList<NodeRef>(parapheurs.size());
            for (NodeRef tmpParapheur : parapheurs) {
                // Les parapheurs sans supérieur hiérarchique sont ajoutés à la liste de parapheurs
                if (this.parapheurService.getParapheurResponsable(tmpParapheur) == null) {
                    resultList.add(tmpParapheur);
                }
            }
        } else {
            resultList = this.parapheurService.getChildParapheurs(levelRef);
            // resultMap.put(tmpParapheur, (this.parapheurService.getChildParapheurs(tmpParapheur)));

        }
        return resultList;
    }

    public Map<String, List<NodeRef>> getParapheursSub(NodeRef rootRef) {
        // Récupération des parapheurs
        List<NodeRef> parapheurs = this.parapheurService.getParapheurs();
        Map<String, List<NodeRef>> resultMap = new HashMap<String, List<NodeRef>>();
//        List<NodeRef> resultList = new ArrayList<NodeRef>(parapheurs.size());
        if (rootRef == null) {
            for (NodeRef tmpParapheur : parapheurs) {
                // Les parapheurs sans supérieur hiérarchique sont ajoutés à la liste de parapheurs
                if (this.parapheurService.getParapheurResponsable(tmpParapheur) == null) {
                    resultMap.put(tmpParapheur.getId(), (this.parapheurService.getChildParapheurs(tmpParapheur)));
                }
            }
        } else {
            for (NodeRef tmpParapheur : parapheurs) {
                // Les parapheurs ayant pour supérieur hiérarchique le parapheur courant sont ajoutés à la liste de parapheurs
                if (rootRef.equals(this.parapheurService.getParapheurResponsable(tmpParapheur))) {
                    //                  resultList.add(tmpParapheur);
                }
            }
        }
        //    return resultList;
        return resultMap;
    }

    private JSONObject jsonParapheurTree(NodeRef root, Set<String> authorities) {
        JSONObject result = null;

        // Si root est un parapheur: remplir, puis récurser sur les ChildParapheurs
        if (parapheurService.isParapheur(root)) {
            try {
                result = new JSONObject();
                this.parapheurService.getNomProprietaires(root);
                String libelle = "<B>" + this.parapheurService.getNomParapheur(root) + "</B> "
                        + StringUtils.generateStringFromListWithQuotes(this.parapheurService.getNomProprietaires(root), "(", ")");
                result.put("title", libelle);
                result.put("key", root.getId());
                result.put("isFolder", true); // Pourquoi systématique? reponse: c'est de la cosmétique
                /**
                 * si root.getId fait partie des autorities alors l'afficher
                 *  (select: true)
                 */
                if (authorities.contains("ROLE_PHADMIN_" + root.getId())) {
                    result.put("select", true);
                }
                result.put("expand", true);
                /**
                 * Récursion...
                 */
                List<NodeRef> prfList = this.parapheurService.getChildParapheurs(root);
                if (!prfList.isEmpty()) {
                    NameNodeRefComparator comparator = new NameNodeRefComparator(nodeService);
                    Collections.sort(prfList, comparator);
                    JSONArray tab = new JSONArray();
                    for (NodeRef prfNodeRef : prfList) {
                        tab.put(jsonParapheurTree(prfNodeRef, authorities));
                    }
                    result.put("children", tab);
                }
            } catch (JSONException ex) {
                logger.error("Probleme JSON", ex);
            }
        }

        return result;
    }

    public String getParapheursAsJSONString(String username) {
        /**
         * Est-il admin fonctionnel ?
         */
        // AuthorityService authorityService = Repository.getServiceRegistry(FacesContext.getCurrentInstance()).getAuthorityService();
        Set<String> authorities = this.authorityService.getAuthoritiesForUser(username);

        List<NodeRef> parapheurs = this.parapheurService.getParapheurs();
        JSONArray tab = new JSONArray();
        for (NodeRef tmpParapheur : parapheurs) {
            if (this.parapheurService.getParapheurResponsable(tmpParapheur) == null) {
                tab.put(jsonParapheurTree(tmpParapheur, authorities));
            }
        }
        return tab.toString().replaceAll("\"title\"", "title");
    }
    

    /**
     * Associer l'utilisateur aux Autorités définies par
     * ROLE_PHADMIN_<paraph-uuid>
     *
     * @param username le nom d'utilisateur concerné
     * @param prfList  liste (csv) des UUIDs de noderefs des parapheurs gérés par l'utilisateur
     * @return
     */
    public String savePartManager(String username, String prfList) {
        System.out.println("savePartManager(" + username + ", " + prfList + ")");
        return parapheurService.setPHAdminAuthoritiesForUser(username, Arrays.asList(prfList.split(",")));
    }

    /**
     * Exposition de workflowService.saveWorkflow, qui permet d'enregistrer un
     * modèle de circuit de validation dans l'entrepot.
     *
     * @param name     le nom du circuit
     * @param circuit  la liste des etapes de validation
     * @param acl      les parapheurs pouvant l'exploiter en direct (deconseille)
     * @param groupes  les groupes pouvant l'exploiter en direct (deconseille)
     * @param isPublic exploitation directe par tout utilisateur (deconseille)
     */
    public void saveWorkflow(String name, List<EtapeCircuit> circuit, Set<NodeRef> acl, Set<String> groupes, boolean isPublic) {
        workflowService.saveWorkflow(name, circuit, acl, groupes, isPublic);
    }

    public int updateActesClassifications() {
        int succeed = -1; // erreur
        try {
            succeed = s2lowService.updateS2lowActesClassifications();
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
        }
        return succeed;
    }

    public boolean isCertificateAbleToConnectToS2low(String protocole, String type) {
        boolean res = false;
        try {
            res = s2lowService.isCertificateAbleToConnect(protocole, type);
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
        }
        return res;
    }

    public List<String> getListLoginForType(String protocole, String type) {
        try {
            return s2lowService.getListLoginForType(protocole, type);
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
        }
        return null;
    }

    public NativeArray getArrayLoginForType(String protocole, String type) {
        NativeArray ret = null;
        try {
            List<String> list = s2lowService.getListLoginForType(protocole, type);
            ret = new NativeArray(list.toArray());
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
        }
        return ret;
    }

    public JSONArray getJSONListLoginForType(String protocole, String type) {
        JSONArray ret = new JSONArray();
        try {
            List<String> list = s2lowService.getListLoginForType(protocole, type);
            for (String el : list) {
                ret.put(el);
            }
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
        }
        return ret;
    }


    public boolean isConnectionOK(String protocole, String type) {
        boolean res = false;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("isConnectionOK(protocole=" + protocole + ", type=" + type + ")");
            }
            res = s2lowService.isConnectionOK(protocole, type);
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
        }
        return res;
    }

    @Deprecated
    public boolean checkSignature(EtapeCircuit etape) {
        boolean res = false;
        try {
            res = parapheurService.checkSignature(etape);
        } catch (Exception e) {
            logger.error("Check signature failed: " + e.getLocalizedMessage());
        }
        return res;
    }

    public List<Map<String, String>> getSignatureProperties(EtapeCircuit etape) {
        return parapheurService.getSignatureProperties(etape);
    }

    public String getTextContentFromBootstrapFile(String fileNameString) {
        InputStream is = getClass().getClassLoader().getResourceAsStream("alfresco/module/parapheur/bootstrap/" + fileNameString);
        if (is == null) {
            logger.error("Problem when trying to OPEN resource stream for " + fileNameString);
            return null;
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;// = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }

            is.close();
        } catch (IOException ex) {
            logger.error("Problem when trying to close resource stream for " + fileNameString);
        }
        return sb.toString();
    }

    /**
     * Exposition de la valeur de 
     * <code>parapheur.habillage</code>,
     * fournie dans  <code>alfresco-global.properties</code>
     * <p>
     * Valeurs connues : 
     * <ul>
     * <li>"<code>blex</code>" : designe un "habillage" Berger-Levrault</li>
     * <li>"<code>adullact</code>" : designe un "habillage" Adullact</li>
     * </ul>
     */
    public String getHabillage() {
        return parapheurService.getHabillage();
    }

    public EtapeCircuit getCurrentEtapeCircuit(NodeRef dossierRef) {
        return parapheurService.getCurrentEtapeCircuit(dossierRef);
    }

    public EtapeCircuit getCurrentOrRejectedEtapeCircuit(NodeRef dossierRef) {
        return parapheurService.getCurrentOrRejectedEtapeCircuit(dossierRef);
    }

    public List<NodeRef> getDelegationsPossibles(NodeRef parapheurRef) {
        return parapheurService.getDelegationsPossibles(parapheurRef);
    }
    
    public void setDelegation(NodeRef bureauCourant, NodeRef bureauCible){
        parapheurService.setDelegation(bureauCourant, bureauCible);
    }
    
    public JSONObject setDelegation(String bureauCourant, String bureauCible, String dateDebut, String dateFin, String enCours) throws ParseException, JSONException {
        JSONObject ret = new JSONObject();
        NodeRef courant = new NodeRef(bureauCourant);
        NodeRef cible = new NodeRef(bureauCible);
        
        Date debut = null;
        Date fin = null;
        if(dateDebut != null && !dateDebut.isEmpty()) {
            debut = new Date(Long.decode(dateDebut));
        }
        if(dateFin != null && !dateFin.isEmpty()) {
            fin = new Date(Long.decode(dateFin));
        }
        
        boolean ajouterEnCours = enCours.equals("true");
        try {
            parapheurService.programmerDelegation(courant, cible, debut, fin, ajouterEnCours);
        } catch(IllegalArgumentException e) {
            if(e.getMessage().equals("Impossible choisir cette délégation, cela créerait une boucle")) {
                ret.put("error", "app.delegation.ajax.boucle");
            }
            else if (e.getMessage().equals("Le créneau de programmation est invalide")) {
                ret.put("error", "app.delegation.ajax.creneau");
            }
            else {
                ret.put("error", "app.delegation.ajax.error");
            }
        } catch(CyclicChildRelationshipException cycleException) {
            ret.put("error", "app.delegation.ajax.boucle");
        }
        return ret;
    }

    public boolean willDelegationLoop(NodeRef parapheur, NodeRef parapheurDelegue, Long debut, Long fin) {
        Date debutDate = null;
        Date finDate = null;
        if(debut != null) {
            debutDate = new Date(debut);
        }
        if(fin != null) {
            finDate = new Date(fin);
        }
        return parapheurService.willDelegationLoop(parapheur, parapheurDelegue, debutDate, finDate);
    }
    
    public JSONObject getDelegationsPossibles(String parapheur) throws JSONException {
        JSONObject ret = new JSONObject();
        List<NodeRef> possible = parapheurService.getDelegationsPossibles(new NodeRef(parapheur));
        for (NodeRef nodeRef : possible) {
            ret.put((String)nodeService.getProperty(nodeRef, ContentModel.PROP_NAME), nodeRef.toString());
        }
        return ret;
    }

    public JSONObject getDelegation(NodeRef parapheurDelegue) throws JSONException, ParseException {
        JSONObject ret = new JSONObject();
        ret.put("id", parapheurDelegue.getId() );
        NodeRef parapheurCible = parapheurService.getDelegation(parapheurDelegue);
        if(parapheurCible == null) {
            parapheurCible = parapheurService.getDelegationProgrammee(parapheurDelegue);
        }
        if(parapheurCible != null) {
            Date debut = (Date)nodeService.getProperty(parapheurDelegue, ParapheurModel.PROP_DATE_DEBUT_DELEGATION);
            Date fin = (Date)nodeService.getProperty(parapheurDelegue, ParapheurModel.PROP_DATE_FIN_DELEGATION);
            ret.put("idCible", parapheurCible.getId());
            ret.put("titreCible", nodeService.getProperty(parapheurCible, ContentModel.PROP_TITLE));
            ret.put("deleguer-presents", nodeService.getProperty(parapheurDelegue, ParapheurModel.PROP_DELEGUER_PRESENTS));
            if(debut != null) {
                ret.put("date-debut-delegation", debut.getTime());
            }
            if(fin != null) {
                ret.put("date-fin-delegation", fin.getTime());
            }
        }

        return ret;
    }

    public boolean hasDelegationActive(NodeRef parapheur) {
        return !parapheurService.getTitulaires(parapheur).isEmpty();
    }
    
    public HashMap<String, String> getDelegation(String parapheur) throws JSONException, ParseException {
        HashMap<String, String> ret = new HashMap<String, String>();
        NodeRef parapheurDelegue = new NodeRef(parapheur);
        NodeRef parapheurCible = parapheurService.getDelegation(parapheurDelegue);
        if(parapheurCible == null) {
            parapheurCible = parapheurService.getDelegationProgrammee(parapheurDelegue);
        }
        if(parapheurCible != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date debut = (Date)nodeService.getProperty(parapheurDelegue, ParapheurModel.PROP_DATE_DEBUT_DELEGATION);
            Date fin = (Date)nodeService.getProperty(parapheurDelegue, ParapheurModel.PROP_DATE_FIN_DELEGATION);
            ret.put("parapheurCible", parapheurCible.getId());
            ret.put("nomCible", (String) nodeService.getProperty(parapheurCible, ContentModel.PROP_NAME));
            ret.put("dossiersActuels", nodeService.getProperty(parapheurDelegue, ParapheurModel.PROP_DELEGUER_PRESENTS).toString());
            if(debut != null) {
                ret.put("dateDebut", String.valueOf(debut.getTime()));
            }
            if(fin != null) {
                ret.put("dateFin", String.valueOf(fin.getTime()));
            }
        }
        
        return ret;
    }
    
    public void supprimerDelegation(String parapheurRef) {
        parapheurService.supprimerDelegation(new NodeRef(parapheurRef));
    }
    
    public String setDelegationsPossibles(NodeRef parapheurRef, String delegationsPossibles) {
        try {
            parapheurService.setDelegationsPossibles(parapheurRef, NodeRef.getNodeRefs(delegationsPossibles));
        } catch(Exception e) {
            return e.getMessage();
        }
        return "Délégations mises à jour";
    }
    
    public String setDelegationsPossibles(NodeRef parapheurRef, ArrayList<NodeRef> delegationsPossibles) {
        try {
            parapheurService.setDelegationsPossibles(parapheurRef, delegationsPossibles);
        } catch(Exception e) {
            return e.getMessage();
        }
        return "Délégations mises à jour";
    }

    public JSONObject getTitulaires(String parapheur) throws JSONException, ParseException {
        JSONObject ret = new JSONObject();
        NodeRef parapheurCourant = new NodeRef(parapheur);
        List<NodeRef> titulaires = parapheurService.getTitulaires(parapheurCourant);
        JSONArray titulairesJSON = new JSONArray();
        for (NodeRef titulaire : titulaires) {
            titulairesJSON.put(parapheurService.getNomParapheur(titulaire));
        }
        ret.put("titulaires", titulairesJSON);
        return ret;
    }
    
    private JSONObject jsonDelegationsTree(NodeRef root, NodeRef parapheurCourant, List<NodeRef> delegationsPossibles, NodeRef delegationCourante) {
        JSONObject result = null;

        // Si root est un parapheur: remplir, puis récurser sur les ChildParapheurs
        if (parapheurService.isParapheur(root)) {
            try {
                result = new JSONObject();
                String libelle = "<B>" + this.parapheurService.getParapheurName(root) + "</B> "
                        + StringUtils.generateStringFromListWithQuotes(this.parapheurService.getNomProprietaires(root), "(", ")");
                result.put("title", libelle);
                result.put("key", root.getId());
                result.put("isFolder", true); // Pourquoi systématique? reponse: c'est de la cosmétique
                // Si root est une délégation en cours, on empeche sa selection
                if (root.equals(delegationCourante)) {
                    result.put("unselectable", true);
                    result.put("select", true);
                    result.put("tooltip", "Actuellement en délégation vers ce bureau");
                }
                if (root.equals(parapheurCourant)) {
                    result.put("unselectable", true);
                    result.put("select", false);
                    result.put("tooltip", "Délégation vers le bureau courant impossible");
                }
                /**
                 * Si root est une délégation possible, on le selectionne
                 */
                if (delegationsPossibles.contains(root)) {
                    result.put("select", true);
                }
                result.put("expand", true);
                /**
                 * Récursion...
                 */
                List<NodeRef> prfList = this.parapheurService.getChildParapheurs(root);
                if (!prfList.isEmpty()) {
                    NameNodeRefComparator comparator = new NameNodeRefComparator(nodeService);
                    Collections.sort(prfList, comparator);
                    JSONArray tab = new JSONArray();
                    for (NodeRef prfNodeRef : prfList) {
                        tab.put(jsonDelegationsTree(prfNodeRef, parapheurCourant, delegationsPossibles, delegationCourante));
                    }
                    result.put("children", tab);
                }
            } catch (JSONException ex) {
                logger.error("Probleme JSON", ex);
            }
        }

        return result;
    }
    
    /**
     * Constructs a json string to use with dynatree (JQuery), checks the possible delegations
     * 
     * @param parapheurCourant
     * @return Json string to use with JQuery dynatree
     * 
     */
    public String getDelegationsAsJSONString(NodeRef parapheurCourant) {
        List<NodeRef> delegationsPossibles = parapheurService.getDelegationsPossibles(parapheurCourant);
        List<NodeRef> parapheurs = this.parapheurService.getParapheurs();
        NodeRef delegationCourante = this.parapheurService.getDelegation(parapheurCourant);
        JSONArray tab = new JSONArray();
        for (NodeRef tmpParapheur : parapheurs) {
            if (this.parapheurService.getParapheurResponsable(tmpParapheur) == null) {
                tab.put(jsonDelegationsTree(tmpParapheur, parapheurCourant, delegationsPossibles, delegationCourante));
            }
        }
        return tab.toString().replaceAll("\"title\"", "title");
    }
    
    public String getNomParapheur(NodeRef parapheurRef) {
        return parapheurService.getNomParapheur(parapheurRef);
    }
    
    public List<NodeRef> getAttachments(NodeRef dossier) {
        return parapheurService.getAttachments(dossier);
    }
    
    public boolean areAttachmentsIncluded(NodeRef dossier) {
        return parapheurService.areAttachmentsIncluded(dossier);
    }
    
    public boolean isOwnerOrDelegateOfDossier(NodeRef parapheurRef, NodeRef dossier) {
        return parapheurService.isOwnerOrDelegateOfDossier(parapheurRef, dossier);
    }

    public JSONArray getBureauxAssocies(String parapheurRef) throws JSONException {
        JSONArray result = new JSONArray();
        TreeMap<String, JSONObject> sortTreeMap = new TreeMap<>();
        List<NodeRef> bureaux = parapheurService.getDelegationsPossibles(new NodeRef(parapheurRef));
        for (NodeRef bureau : bureaux) {
            JSONObject b = new JSONObject();
            b.put("id", bureau.getId());
            b.put("title", nodeService.getProperty(bureau, ContentModel.PROP_TITLE));
            b.put("name", nodeService.getProperty(bureau, ContentModel.PROP_NAME));
            b.put("description", nodeService.getProperty(bureau, ContentModel.PROP_DESCRIPTION));
            b.put("proprietaires", parapheurService.getNomProprietaires(bureau));
            b.put("secretaires", parapheurService.getSecretariatParapheur(bureau));
            sortTreeMap.put(b.getString("name").toLowerCase(), b);
        }
        for(JSONObject o : sortTreeMap.values()) {
            result.put(o);
        }
        return result;
    }

    public JSONArray getBureauxAssociesAsAdmin(String parapheurRef) throws JSONException {
        JSONArray result = new JSONArray();
        List<NodeRef> bureaux = parapheurService.getDelegationsPossibles(new NodeRef(parapheurRef));
        for (NodeRef bureau : bureaux) {
            result.put(bureau.getId());
        }
        return result;
    }
    
    public void deleteBureau(NodeRef nodeRef){
        nodeService.deleteNode(nodeRef);
    }

    
    public JSONObject avisComplementaire(String dossierRef, String bureauCourantRef, String bureauCible, String annotPub, String annotPriv) throws JSONException {
        JSONObject json = new JSONObject();
        JSONObject result = new JSONObject();
        NodeRef bureauCourant = new NodeRef(bureauCourantRef);
        NodeRef dossier = new NodeRef(dossierRef);
        String res;
        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);
        if(!circuit.get(0).equals(parapheurService.getCurrentEtapeCircuit(dossier))) {
            if(bureauCourant.equals(parapheurService.getParentParapheur(dossier))) {
                res = "success";

                parapheurService.avisComplementaire(dossier, new NodeRef(bureauCible), annotPub, annotPriv);
            } else {
                res = "Vous n'avez pas l'autorisation de faire cette action";
            }
        } else {
            res = "Impossible sur la première étape du circuit";
        }
        
        result.put((String)nodeService.getProperty(dossier, ContentModel.PROP_NAME), res);
        
        json.put("result", result);
        return json;
    }
    
    public JSONObject changeSignataire(String dossierRef, String bureauCourantRef, String bureauCible, String annotPub, String annotPriv) throws JSONException {
        JSONObject json = new JSONObject();
        JSONObject result = new JSONObject();
        NodeRef bureauCourant = new NodeRef(bureauCourantRef);
        NodeRef dossier = new NodeRef(dossierRef);
        String res;
        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);
        if(!circuit.get(0).equals(parapheurService.getCurrentEtapeCircuit(dossier))) {
            if(parapheurService.getCurrentEtapeCircuit(dossier).getActionDemandee().equals(EtapeCircuit.ETAPE_SIGNATURE)) {
                if(bureauCourant.equals(parapheurService.getParentParapheur(dossier))) {
                    res = "success";

                    parapheurService.changeSignataire(dossier, new NodeRef(bureauCible), annotPub, annotPriv);
                } else {
                    res = "Vous n'avez pas l'autorisation de faire cette action";
                }
            } else {
                res = "Cette action n'est possible que sur une action de signature";
            }
        } else {
            res = "Impossible sur la première étape du circuit";
        }
        
        result.put((String)nodeService.getProperty(dossier, ContentModel.PROP_NAME), res);
        
        json.put("result", result);
        return json;
    }
    
    public JSONObject changeToSignPapier(NodeRef dossier, String bureauCourantRef) throws JSONException {
        JSONObject json = new JSONObject();
        JSONObject result = new JSONObject();
        NodeRef bureauCourant = new NodeRef(bureauCourantRef);
        String res;
        if(parapheurService.getCurrentEtapeCircuit(dossier).getActionDemandee().equals(EtapeCircuit.ETAPE_SIGNATURE)) {
            if(bureauCourant.equals(parapheurService.getParentParapheur(dossier))) {
                res = "success";

                parapheurService.changeSignatureToSignaturePapier(dossier);
                corbeillesService.insertIntoCorbeilleAImprimer(dossier);
            } else {
                res = "Vous n'avez pas l'autorisation de faire cette action";
            }
        } else {
            res = "Cette action n'est possible que sur une action de signature";
        }
        
        result.put((String)nodeService.getProperty(dossier, ContentModel.PROP_NAME), res);
        
        json.put("result", result);
        return json;
    }
    /**
     * @param parapheur
     * @return true si le parapheur n'a pas de secretaire, false sinon
     * @author Florian Ajir
     */
    public boolean hasSecretaire(NodeRef parapheur){
        List<String> lstSecretaires = parapheurService.getSecretariatParapheur(parapheur);
        return (!lstSecretaires.isEmpty());
    }
    
    public String changeUserSignature(String username, String b64Content) {
        NodeRef user = personService.getPerson(username);
        
        ChildAssociationRef childAssociationRef = null;
        
        /* Création du nœud fils avec pour type d'association ASSOC_SIGNATURE_SCAN */
        for(ChildAssociationRef c : nodeService.getChildAssocs(user)) {
            if(c.getQName().getLocalName().equals(QName.createQName("cm:signature").getLocalName())) {
                childAssociationRef = c;
            }
        }
        
        if(childAssociationRef == null) {
       childAssociationRef = nodeService.createNode(user,
                ParapheurModel.ASSOC_SIGNATURE_SCAN,
                QName.createQName("cm:signature"),
                ContentModel.TYPE_CONTENT);
        }

        InputStream is = new ByteArrayInputStream(Base64.decode(b64Content));

        ContentWriter imageWriter = contentService.getWriter(childAssociationRef.getChildRef(),ContentModel.PROP_CONTENT, true);
        /* Ecriture du contenu de la signature dans PROP_CONTENT du nouveau nœud */
        imageWriter.putContent(is);
        
        return childAssociationRef.getChildRef().getId();
    }

    public JSONArray searchUser(String search) throws JSONException {
        JSONArray ret = new JSONArray();
        List<NodeRef> users = parapheurService.searchUser(search);
        for(NodeRef user : users) {
            JSONObject obj = new JSONObject();
            String username = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
            obj.put("firstName", user.getId());
            obj.put("username", username);
            obj.put("firstName", nodeService.getProperty(user, ContentModel.PROP_FIRSTNAME));
            obj.put("lastName", nodeService.getProperty(user, ContentModel.PROP_LASTNAME));
            obj.put("email", nodeService.getProperty(user, ContentModel.PROP_EMAIL));
            obj.put("isOwner", parapheurService.isOwner(username));
            obj.put("isSecretaire", getOwnedParapheurs(username).isEmpty() && !getSecretariatParapheurs(username).isEmpty());
            ret.put(obj);
        }
        return ret;
    }

    public boolean isOwner(String userName) {
        return parapheurService.isOwner(userName);
    }

    public void moveDossier(NodeRef dossier, NodeRef bureau) {
        parapheurService.moveDossier(dossier, bureau);
    }
}
