/*
 * Version 3.3
 * CeCILL Copyright (c) 2008-2012, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.repo.jscript;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.springframework.extensions.webscripts.DescriptionExtension;

/**
 * Classe de description associée à StreamBinaryData.
 * 
 * <p>
 * Voir http://wiki.alfresco.com/wiki/Web_Scripts#Creating_a_New_Kind_of_Web_Script
 * <br>
 * "The backing bean may be configured via extensions to the Web Script 
 *  Description Document. To access those extensions it is necessary to provide
 *  an implementation of the interface org.alfresco.web.scripts.DescriptionExtension"
 * 
 * @author svast
 */
public class StreamBinaryDataDescription implements DescriptionExtension { //org.alfresco.web.scripts.DescriptionExtension {

    /**
     * Gets the custom description extensions
     *
     * @param serviceDescPath path to service doc
     * @param serviceDesc service doc input stream
     * @return extensions mapped by name
     */
    @Override
    public Map<String, Serializable> parseExtensions(String serviceDescPath, InputStream serviceDesc) {

        Map<String, Serializable> extensions = new HashMap<String, Serializable>();
       /* Un exemple d'enrichissement possible: */
       /* SAXReader reader = new SAXReader();
        try {
            // extract path value from description document
            Document document = reader.read(serviceDesc);
            Element rootElement = document.getRootElement();
            Element pathElement = rootElement.element("path");
            String path = pathElement.getTextTrim();
            extensions.put("path", path);
        } catch (DocumentException e) {
            throw new WebScriptException("Failed to parse", e);
        }*/
        return extensions;
    }
    
}
