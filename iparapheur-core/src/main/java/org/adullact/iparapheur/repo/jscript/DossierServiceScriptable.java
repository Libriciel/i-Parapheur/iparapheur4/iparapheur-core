package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.model.ParapheurErrorCode;
import com.atolcd.parapheur.model.ParapheurException;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import com.atolcd.parapheur.repo.content.Document;
import com.atolcd.parapheur.repo.impl.S2lowServiceImpl;
import com.atolcd.parapheur.web.bean.wizard.batch.CakeFilter;
import com.atolcd.parapheur.web.bean.wizard.batch.Predicate;
import fr.bl.iparapheur.web.BlexContext;
import org.adullact.iparapheur.status.StatusMetier;
import org.adullact.iparapheur.util.CollectionsUtils;
import org.adullact.iparapheur.util.JsonUtils;
import org.adullact.iparapheur.util.NativeUtils;
import org.adullact.spring_ws.iparapheur._1.LogDossier;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.NativeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.Assert;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.UndeclaredThrowableException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA. User: manz Date: 31/08/12 Time: 11:02
 *
 * @author Emmanuel Peralta - ADULLACT Projet
 */
public class DossierServiceScriptable extends BaseProcessorExtension {

    @Autowired
    @Qualifier("_dossierService")
    private DossierService dossierService;
    @Autowired
    private NodeService nodeService;
    @Autowired
    @Qualifier("namespaceService")
    private NamespaceService namespaceService;
    @Autowired
    @Qualifier("_parapheurService")
    private ParapheurService parapheurService;
    @Autowired
    private S2lowService s2lowService;
    @Autowired
    private TypesService typesService;
    @Autowired
    private WorkflowService workflowService;
    @Autowired
    private FileFolderService fileFolderService;
    @Autowired
    private DictionaryService dictionaryService;
    @Autowired
    private SearchService searchService;
    @Autowired
    private PermissionService permissionService;

    private String companyHomeChildname;
    private String parapheursChildname;

    public String getCompanyHomeChildname() {
        return companyHomeChildname;
    }

    public void setCompanyHomeChildname(String companyHomeChildname) {
        this.companyHomeChildname = companyHomeChildname;
    }

    public String getParapheursChildname() {
        return parapheursChildname;
    }

    public void setParapheursChildname(String parapheursChildname) {
        this.parapheursChildname = parapheursChildname;
    }

    // <editor-fold desc="Private methods">

    private List<NodeRef> nativeArrayToNodeRefList(NativeArray array) {
        List<NodeRef> retVal = new ArrayList<>();
        for (int i = 0; i < array.getLength(); i++) {
            retVal.add(new NodeRef((String) array.get(i, null)));
        }
        return retVal;
    }

    private List<String> nativeArrayToString(NativeArray array) {
        List<String> retVal = new ArrayList<>();
        for (int i = 0; i < array.getLength(); i++) {
            retVal.add((String) array.get(i, null));
        }
        return retVal;
    }

    private List<Boolean> nativeArrayToBoolean(NativeArray array) {
        List<Boolean> retVal = new ArrayList<>();
        for (int i = 0; i < array.getLength(); i++) {
            retVal.add((Boolean) array.get(i, null));
        }
        return retVal;
    }

    private @NotNull JSONObject createHttpResult(@NotNull String message) {
        if (StringUtils.equalsIgnoreCase(message, "success")) {
            return JsonUtils.createHttpStatusJsonObject(200, "OK");
        } else {
            return JsonUtils.createHttpStatusJsonObject(400, message);
        }
    }

    // </editor-fold desc="Private methods">

    public String getSecureMailTemplate(String nodeRef) {
        NodeRef dossier = new NodeRef(nodeRef);
        return s2lowService.getSecureMailTemplate(dossier);
    }

    public String beginCreateDossier(String bureauRefStr) {
        String retVal = null;
        NodeRef dossierRef = dossierService.beginCreateDossier(new NodeRef("workspace://SpacesStore/" + bureauRefStr));
        if (dossierRef != null) {
            retVal = dossierRef.getId();
        }
        return retVal;
    }

    public boolean removeDocument(NodeRef document, NodeRef dossier, NodeRef bureauCourant) throws JSONException {
        return dossierService.removeDocument(document, dossier, bureauCourant);
    }

    @SuppressWarnings("unused")
    public JSONObject setDossierProperties(NodeRef dossierRef, NodeRef bureauRef, NativeObject object) {

        String username = AuthenticationUtil.getRunAsUser();
        if ((parapheurService.isActeurCourant(dossierRef, username) && parapheurService.getParentParapheur(dossierRef).equals(bureauRef)) ||
                (parapheurService.isParapheurSecretaire(bureauRef, username) && nodeService.hasAspect(dossierRef, ParapheurModel.ASPECT_SECRETARIAT))) {

            Map<String, Serializable> properties = NativeUtils.nativeToProperties(object);
            if (!(Boolean) nodeService.getProperty(dossierRef, ParapheurModel.PROP_FULL_CREATED)) {
                Map<QName, Serializable> baseProperties = new HashMap<>();
                for (Map.Entry entry : properties.entrySet()) {
                    baseProperties.put(QName.createQName((String) entry.getKey(), namespaceService), (Serializable) entry.getValue());
                }
                //baseProperties.put(ParapheurModel.PROP_TYPE_METIER, nodeService.getProperty(dossierRef, ParapheurModel.PROP_TYPE_METIER));
                //baseProperties.put(ParapheurModel.PROP_SOUSTYPE_METIER, nodeService.getProperty(dossierRef, ParapheurModel.PROP_SOUSTYPE_METIER));
                String result = dossierService.setDossierPropertiesWithAuditTrail(dossierRef, bureauRef, baseProperties, false);
                return createHttpResult(result);
            } else {
                String message = dossierService.setDossierProperties(dossierRef, properties);
                return createHttpResult(message);
            }
        }
        else {
            return JsonUtils.createHttpStatusJsonObject(403, "You don't have rights to modify this resource");
        }
    }

    public void readDossier(NodeRef dossier, String username) {
        dossierService.readDossier(dossier, username);
    }

    @Deprecated
    @SuppressWarnings("unused")
    public void removeDossierProperties(String dossierRefStr, NativeObject object) {
        NodeRef dossierRef = new NodeRef(dossierRefStr);
        Map<String, Serializable> properties = NativeUtils.nativeToProperties(object);

        // dossierService.removeDossierStringProperties(dossierRef, properties);
    }

    public JSONObject addSignature(NodeRef dossierRef, String name, String content) throws JSONException {
        JSONObject ret = new JSONObject();

        Document document = new Document();
        document.setFileName(name);
        document.setBase64Content(content);

        String strRet;
        try {
            dossierService.addSignature(dossierRef, document);
            strRet = "success";
        } catch (Exception e) {
            strRet = e.getMessage();
        }

        ret.put("result", strRet);
        return ret;
    }

    @SuppressWarnings("unused")
    public JSONObject setVisuelDocument(NodeRef dossierRef, NodeRef documentRef, String content) throws Exception {
        JSONObject ret = new JSONObject();

        Document document = new Document();
        document.setFileName("Visuel.pdf");
        document.setBase64Content(content);

        try {
            dossierService.setVisuelDocument(dossierRef, documentRef, document);
            ret.put("success", documentRef.getId());
        } catch (ParapheurException e) {
            ret.put("exception", e.getMessage());
            ret.put("message", String.valueOf(e.getInternalCode()));
        } catch (Exception e) {
            ret.put("exception", e.getMessage());
        }

        return ret;
    }

    private Date cvtToGmt(Date date) {
        TimeZone tz = TimeZone.getDefault();
        Date ret = new Date(date.getTime() - tz.getRawOffset());

        // if we are now in DST, back off by the delta.  Note that we are checking the GMT date, this is the KEY.
        if (tz.inDaylightTime(ret)) {
            Date dstDate = new Date(ret.getTime() - tz.getDSTSavings());

            // check to make sure we have not crossed back into standard time
            // this happens when we are on the cusp of DST (7pm the day before the change for PDT)
            if (tz.inDaylightTime(dstDate)) {
                ret = dstDate;
            }
        }

        return ret;
    }

    public JSONObject getInfosMailSec(NodeRef dossier) throws JSONException {
        JSONObject ret = new JSONObject();
        S2lowServiceImpl.SecureMailDetail infos = dossierService.getInfosMailSec(dossier);
        ret.put("statut", infos.getStatus());
        ret.put("envoi", infos.getDateEnvoi().getTime());
        JSONArray details = new JSONArray();
        for (S2lowServiceImpl.EmissionDetail detail : infos.getEmis()) {
            JSONObject item = new JSONObject();
            item.put("confirmed", detail.confirmed);
            item.put("confirmationDate", detail.confirmationDate != null ? cvtToGmt(detail.confirmationDate).getTime() : null);
            item.put("email", detail.email);
            details.put(item);
        }

        ret.put("details", details);
        return ret;
    }

    @SuppressWarnings("unused")
    public JSONObject addDocument(NodeRef dossierRef, String name, String content, String nameVisu, String contentVisu, boolean isDocumentPrincipal, boolean reloadMainDocument) throws Exception {
        JSONObject ret = new JSONObject();

        Document document = new Document();
        document.setFileName(name);
        document.setBase64Content(content);

        NodeRef documentRef;

        Document documentVisu = null;
        if (!nameVisu.equals("") && !contentVisu.equals("")) {
            documentVisu = new Document();
            documentVisu.setFileName(nameVisu);
            documentVisu.setBase64Content(contentVisu);
        }
        try {

            if (isDocumentPrincipal && !reloadMainDocument) {
                documentRef = dossierService.addDocumentPrincipal(dossierRef, document, documentVisu);
                //Réorganisation des index des documents : les pièces principales en 1er, puis les annexes
                dossierService.reorderDocuments(dossierRef);
            } else if (isDocumentPrincipal) {
                documentRef = dossierService.replaceDocumentPrincipal(dossierRef, document, documentVisu);
            } else {
                documentRef = dossierService.addAnnexe(dossierRef, document, documentVisu);
                dossierService.reorderDocuments(dossierRef);
            }

            if (documentRef != null) {

                ret.put("success", documentRef.getId());
                ret.put("isLocked", nodeService.hasAspect(documentRef, ParapheurModel.ASPECT_PENDING));
                ret.put("isProtected", dossierService.isPdfProtected(document));
                String nodeRefUrl = documentRef.toString().replace("://", "/");
                ret.put("downloadUrl", "/api/node/" + nodeRefUrl + "/content/" + name);
                if (documentVisu != null) {
                    ret.put("visuelPdfUrl", "/api/node/content%3bph%3avisuel-pdf/" + nodeRefUrl + "/" + documentVisu.getFileName());
                }

                ret.put("statusCode", 200);
            } else {
                ret = JsonUtils.createHttpStatusJsonObject(400, ParapheurErrorCode.INTERNAL_SERVER_ERROR);
            }
        } catch (ParapheurException ex) {
            ret = JsonUtils.createHttpStatusJsonObject(400, ex.getInternalCode());
        } catch (Exception ex) {
            ret = JsonUtils.createHttpStatusJsonObject(400, ex.getMessage());
        }

        return ret;
    }

    public void setCircuit(NodeRef dossierRef, NodeRef bureauCourant, String type, String sousType) {
        if (dossierService.canEdit(dossierRef, bureauCourant)) {
            dossierService.setCircuit(dossierRef, type, sousType);
        }
    }

    @SuppressWarnings("unused")
    @Deprecated
    public void mergeProperties(String dossierRefStr, String bureauCourant, NativeObject object) {
        NodeRef dossier = new NodeRef(dossierRefStr);
        NodeRef bureau = new NodeRef(bureauCourant);
        if (getActions(dossier, bureau).contains(DossierService.ACTION_DOSSIER.ARCHIVAGE.toString())) {

            Map<String, Serializable> properties = NativeUtils.nativeToProperties(object);
            Map<QName, Serializable> newProperties = CollectionsUtils.createQNameProperties(properties, dictionaryService, namespaceService);
            Map<QName, Serializable> originalProperties = nodeService.getProperties(dossier);

            // Update with given parameters

            for (Map.Entry<QName, Serializable> entry : newProperties.entrySet()) {
                if (entry.getValue() == null) {
                    originalProperties.remove(entry.getKey());
                } else {
                    originalProperties.put(entry.getKey(), entry.getValue());
                }
            }

            // Write data

            dossierService.setDossierQNameProperties(dossier, originalProperties);
        }
    }

    public void chainCircuit(String dossierRefStr, String bureauCourant, String type, String sousType) {
        NodeRef dossier = new NodeRef(dossierRefStr);
        NodeRef bureau = new NodeRef(bureauCourant);
        if (getActions(dossier, bureau).contains(DossierService.ACTION_DOSSIER.ARCHIVAGE.toString())) {
            SavedWorkflow savedWorkflow = workflowService.getSavedWorkflow(dossierService.getCircuitRef(dossier, type, sousType));
            parapheurService.appendCircuit(dossier, savedWorkflow.getCircuit());
            parapheurService.restartCircuit(dossier, type, sousType, "subtype");
            String signatureFormat = typesService.getSignatureFormat(type);
            // We define a new signature format ONLY IF IT IS NOT AUTO !!
            if(signatureFormat != null && !signatureFormat.equals("AUTO")) {
                nodeService.setProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT, signatureFormat);
            }
        }
    }

    public NodeRef getCircuitRef(NodeRef dossier, String type, String sousType) {
        return dossierService.getCircuitRef(dossier, type, sousType);
    }

    @SuppressWarnings("unused")
    public @NotNull JSONObject finalizeCreateDossier(NodeRef nodeRef, NodeRef bureauCourant) {

        if (dossierService.canEdit(nodeRef, bureauCourant)) {
            String messageResult = dossierService.finalizeCreateDossier(nodeRef);
            return createHttpResult(messageResult);
        } else {
            return JsonUtils.createHttpStatusJsonObject(403, "Erreur : Vous n'avez pas les droits d'édition de dossier sur le bureau courant");
        }
    }

    public JSONObject deleteNodes(NativeArray nodes) throws JSONException {
        List<NodeRef> nodesToDelete = nativeArrayToNodeRefList(nodes);
        JSONObject json = new JSONObject();
        json.put("result", dossierService.deleteNodes(nodesToDelete));
        return json;
    }

    public void deleteDossier(NodeRef dossier) {
        dossierService.deleteDossier(dossier, true);
    }

    public void deleteDossierAdmin(String id) {
        dossierService.deleteDossierAdmin(new NodeRef("workspace://SpacesStore/" + id));
    }

    public void deleteNode(String node) {
        List<NodeRef> nodesToDelete = new ArrayList<NodeRef>();
        nodesToDelete.add(new NodeRef(node));
        dossierService.deleteNodes(nodesToDelete);
    }

    public boolean canDelete(String dossier) {
        return dossierService.canDelete(new NodeRef(dossier));
    }

    public JSONObject remorseDossier(NativeArray dossiers, String bureauCourant) throws JSONException {
        List<NodeRef> filesToRemorse = nativeArrayToNodeRefList(dossiers);
        JSONObject json = new JSONObject();
        json.put("result", dossierService.remorseDossier(filesToRemorse, new NodeRef(bureauCourant)));
        return json;
    }

    public boolean canRemorse(String dossier, String bureauCourant) {
        return dossierService.canRemorse(new NodeRef(dossier), new NodeRef(bureauCourant));
    }

    public JSONObject RAZDossier(NativeArray dossiers, String bureauCourant) throws JSONException {
        List<NodeRef> filesToRAZ = nativeArrayToNodeRefList(dossiers);
        JSONObject json = new JSONObject();
        json.put("result", dossierService.RAZDossier(filesToRAZ, new NodeRef(bureauCourant)));
        return json;
    }

    public List<String> getActeursVariables(NodeRef dossier) {
        List<String>  ret = new ArrayList<String> ();
        List<NodeRef> acteursVariables = (List<NodeRef>) nodeService.getProperty(dossier, ParapheurModel.PROP_ACTEURS_VARIABLES);
        if(acteursVariables != null) {
            for(NodeRef acteur : acteursVariables) {
                ret.add(acteur.getId());
            }
        }
        return ret;
    }

    public boolean getForbidedSameSig(NodeRef dossier) {
        List<NodeRef> circuit = dossierService.getCircuit(dossier);
        List<String> isAlreadySignedBy = new ArrayList<>();
        boolean forbidSameSig = parapheurService.isForbidSameSig(dossier);
        if (circuit != null && forbidSameSig) {
            circuit.forEach(etape -> {
                String actionDemandeTmp = (String) nodeService.getProperty(etape, ParapheurModel.PROP_ACTION_DEMANDEE);
                if(actionDemandeTmp.equals(EtapeCircuit.ETAPE_SIGNATURE)) {
                    Object effectueProp = nodeService.getProperty(etape, ParapheurModel.PROP_EFFECTUEE);
                    boolean stepeffectue = effectueProp != null && (Boolean) effectueProp;

                    // Folder is already signed by user
                    if(stepeffectue) {
                        isAlreadySignedBy.add((String) nodeService.getProperty(etape, ParapheurModel.PROP_VALIDATOR));
                    }
                }
            });
        }
        return isAlreadySignedBy.contains(AuthenticationUtil.getRunAsUser());
    }

    public JSONObject archiveDossier(NativeArray dossiers, NativeArray nomArchives, String bureauCourant, NativeArray annexes) throws JSONException {
        List<NodeRef> filesToArchive = nativeArrayToNodeRefList(dossiers);
        List<String> noms = nativeArrayToString(nomArchives);
        JSONObject json = new JSONObject();
        if (filesToArchive.size() != 1) {
            json.put("result", new JSONObject(dossierService.archiveDossier(
                    filesToArchive,
                    noms,
                    new NodeRef(bureauCourant),
                    nativeArrayToBoolean(annexes))));
        } else {
            json.put("result", new JSONObject().
                    put(noms.get(0), dossierService.archiveDossier(
                            filesToArchive.get(0),
                            noms.get(0),
                            nativeArrayToNodeRefList(annexes))));
        }
        return json;
    }

    public JSONObject sendMail(NativeArray dossiers, NativeArray _destinataires, String objet, String message, NativeArray _attachments, String _annexesIncluded) {
        JSONObject json = new JSONObject();
        List<NodeRef> filesToMail = nativeArrayToNodeRefList(dossiers);
        List<NodeRef> attachments = nativeArrayToNodeRefList(_attachments);
        List<String> destinataires = nativeArrayToString(_destinataires);
        boolean annexesIncluded = Boolean.parseBoolean(_annexesIncluded);
        dossierService.sendMail(filesToMail, destinataires, objet, message, attachments, annexesIncluded, true);
        return json;
    }

    public List<String> getCorbeillesParent(NodeRef dossier, NodeRef bureauCourant) {
        return dossierService.getCorbeillesParent(dossier, bureauCourant);
    }

    public JSONObject sendMailSec(String _bureauCourant, NativeArray _dossiers, NativeArray _destinataires, NativeArray _destinatairesCC,
                                  NativeArray _destinatairesCCI, String objet, String message, String password, String _showPass, NativeArray _attachments, String _annexesIncluded) {
        JSONObject json = new JSONObject();
        NodeRef bureauCourant = new NodeRef(_bureauCourant);
        List<NodeRef> dossiers = nativeArrayToNodeRefList(_dossiers);
        List<String> destinataires = nativeArrayToString(_destinataires);
        List<String> destinatairesCC = nativeArrayToString(_destinatairesCC);
        List<String> destinatairesCCI = nativeArrayToString(_destinatairesCCI);
        List<NodeRef> attachments = nativeArrayToNodeRefList(_attachments);
        boolean showPass = Boolean.parseBoolean(_showPass);
        boolean annexesIncluded = Boolean.parseBoolean(_annexesIncluded);
        dossierService.sendMailSec(bureauCourant, dossiers, destinataires, destinatairesCC, destinatairesCCI, objet, message, password, showPass, attachments, annexesIncluded);
        return json;
    }

    public JSONObject print(String dossierRef, NativeArray _attachments, boolean includeFirstPage) throws Exception {
        JSONObject json = new JSONObject();
        NodeRef dossier = new NodeRef(dossierRef);
        List<NodeRef> attachments = null;
        if (_attachments != null) {
            attachments = nativeArrayToNodeRefList(_attachments);
        }
        json.put("file", dossierService.print(dossier, attachments, includeFirstPage));
        return json;
    }

    public JSONObject signerDossier(NativeArray dossiers, String annotPub, String annotPriv, String bureauCourant, NativeArray _signatures) throws Exception {
        List<NodeRef> filesToSign = nativeArrayToNodeRefList(dossiers);
        List<String> signatures = nativeArrayToString(_signatures);
        JSONObject json = new JSONObject();
        json.put("result", dossierService.signerDossier(filesToSign, annotPub, annotPriv, new NodeRef(bureauCourant), signatures));
        return json;
    }

    public JSONObject secretariat(NativeArray dossiers, String annotPub, String annotPriv, String bureauCourant) throws JSONException {
        List<NodeRef> filesToSec = nativeArrayToNodeRefList(dossiers);
        JSONObject json = new JSONObject();
        json.put("result", dossierService.secretariat(filesToSec, annotPub, annotPriv, new NodeRef(bureauCourant)));
        return json;
    }

    public boolean canSecretariat(String dossier, String bureauCourant) {
        return dossierService.canSecretariat(new NodeRef(dossier), new NodeRef(bureauCourant));
    }

    public boolean canRAZ(String dossier, String bureauCourant) {
        return dossierService.canRAZ(new NodeRef(dossier), new NodeRef(bureauCourant));
    }

    public boolean canRemoveDoc(NodeRef document, NodeRef dossier, NodeRef bureauCourant) {
        return dossierService.canRemoveDoc(document, dossier, bureauCourant);
    }

    public JSONObject rejectDossier(NativeArray dossiers, String pub, String priv, String bureauCourant) throws JSONException {
        List<NodeRef> filesToReject = nativeArrayToNodeRefList(dossiers);
        JSONObject json = new JSONObject();
        json.put("result", dossierService.rejectDossier(filesToReject, pub, priv, new NodeRef(bureauCourant)));
        return json;
    }

    public boolean canReject(String dossier, String bureauCourant) {
        return dossierService.canReject(new NodeRef(dossier), new NodeRef(bureauCourant));
    }

    @SuppressWarnings("unused")
    public JSONObject visaDossier(NativeArray dossiers, String pub, String priv, String bureauCourant, boolean consecutiveSteps) throws JSONException {
        List<NodeRef> filesToVisa = nativeArrayToNodeRefList(dossiers);
        JSONObject json = new JSONObject();
        json.put("result", dossierService.visaDossier(filesToVisa, pub, priv, new NodeRef(bureauCourant), consecutiveSteps));
        return json;
    }

    public List<NodeRef> getDossiers(NodeRef bureauRef, String parentName, int count, int index, NativeObject _filters, String propSort, String asc, int numPage, int pendingFiles) throws JSONException {
        List<Predicate<?, ?>> filters = null;

        if (_filters != null) {
            filters = nativeObjectPredicate(_filters);
        }
        return dossierService.getDossiers(bureauRef, parentName, count, index, filters, propSort, Boolean.parseBoolean(asc), numPage, pendingFiles);
    }

    public JSONObject getAnnexes(NodeRef dossier) throws JSONException {
        JSONObject annexes = new JSONObject();
        JSONArray json = new JSONArray();
        List<NodeRef> attachments = parapheurService.getAttachments(dossier);
        for (NodeRef nodeRef : attachments) {
            String name = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_NAME);
            String extension = "";
            int i = name.lastIndexOf('.');
            if(i > 0) extension = name.substring(i + 1);
            if(extension.equalsIgnoreCase("zip")) {
                // ignore this annex file
                continue;
            }

            JSONObject tmp = new JSONObject();
            tmp.put("name", nodeService.getProperty(nodeRef, ContentModel.PROP_NAME));
            tmp.put("id", nodeRef.getId());
            tmp.put("selected", parapheurService.areAttachmentsIncluded(dossier));
            json.put(tmp);
        }
        annexes.put("annexes", json);
        return annexes;
    }

    public List<NodeRef> getArchives(int count, int index, NativeObject _filters, String propSort, String asc, int numPage) throws JSONException {
        List<Predicate<?, ?>> filters = null;

        if (_filters != null) {
            filters = nativeObjectPredicate(_filters);
        }
        return dossierService.getArchives(count, index, filters, propSort, Boolean.parseBoolean(asc));
    }

    public List<NodeRef> searchFolder(NativeObject _filters, boolean toList) throws JSONException {
        List<Predicate<?, ?>> filters = null;

        if (_filters != null) {
            filters = nativeObjectPredicate(_filters);
        }
        return dossierService.searchFolder(filters);
    }

    public JSONObject searchFolder(NativeObject _filters) throws JSONException {
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();

        List<Predicate<?, ?>> filters = null;

        if (_filters != null) {
            filters = nativeObjectPredicate(_filters);
        }
        List<NodeRef> result = dossierService.searchFolder(filters);
        if (result != null) {
            for (NodeRef nodeRef : result) {
                JSONObject obj = new JSONObject();
                obj.put("nodeRef", nodeRef.getId());
                Map<QName, Serializable> s = nodeService.getProperties(nodeRef);
                if (s.get(ContentModel.PROP_TITLE) != null) {
                    obj.put("name", s.get(ContentModel.PROP_TITLE));
                } else {
                    obj.put("name", s.get(ContentModel.PROP_NAME));
                }
                array.put(obj);
            }
        } else {
            //En cas de résultat null
            result = new ArrayList<NodeRef>();
        }
        json.put("result", array);
        return json;
    }

    @SuppressWarnings("unused")
    public JSONObject getSignatureInformations(NodeRef dossier, NodeRef bureauCourant) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("signatureInformations", dossierService.getSignatureInformations(dossier, bureauCourant));
        return json;
    }

    public boolean canSign(String dossier, String bureauCourant) {
        return dossierService.canSign(new NodeRef(dossier), new NodeRef(bureauCourant));
    }

    public boolean hasReadDossier(NodeRef dossier) {
        return dossierService.hasReadDossier(dossier, AuthenticationUtil.getRunAsUser());
    }

    public boolean canEdit(String dossier, String bureauCourant) {
        return dossierService.canEdit(new NodeRef(dossier), new NodeRef(bureauCourant));
    }

    public boolean canArchive(String dossier, String bureauCourant) {
        return dossierService.canArchive(new NodeRef(dossier), new NodeRef(bureauCourant));
    }

    /*
    
     public void test(ScriptNode node) {
     NativeArray t = (NativeArray) node.getChildren();
     node = (ScriptNode)t.get(3, null);
     boolean b = node.getIsDocument();
     String test = "test";
        
     }
     */
    public List<LogDossier> getLogsDossier(NodeRef dossierRef) {
        return dossierService.getLogsDossier(dossierRef);
    }

    /**
     * @param nativeObject
     * @return
     */
    private List<Predicate<?, ?>> nativeObjectPredicate(NativeObject nativeObject) {
        Object[] propIds = nativeObject.getIds();
        List<Predicate<?, ?>> predicates = new ArrayList<Predicate<?, ?>>();
        // normalement on a une seule paire ici sauf dans le cas "compatible"
        for (Object propId : propIds) {
            Object value = nativeObject.get(propId.toString(), nativeObject);

            if (value instanceof JSONArray) {
                JSONArray array = (JSONArray) value;
                List<Predicate<?, ?>> predicatesList = jsonArrayToPredicates(array);

                if (predicatesList != null && predicatesList.size() > 0) {
                    Predicate<String, List<Predicate<?, ?>>> predicate = new Predicate<String, List<Predicate<?, ?>>>(propId.toString(), predicatesList);
                    predicates.add(predicate);
                }

            } else if (value instanceof String) {
                String stringValue = propId.toString();
                String[] qNameParts = stringValue.split(":");

                QName filterQName = QName.createQName(qNameParts[0], qNameParts[1], namespaceService);

                Predicate predicate = new Predicate<QName, String>(filterQName, (String) value);
                predicates.add(predicate);

            } else {
                throw new RuntimeException("Malformed request values types should be either String or Array.");
            }

        }


        if (predicates.size() == 0) {
            predicates = null;
        }

        return predicates;
    }

    /**
     * @param array
     * @return
     */
    private List<Predicate<?, ?>> jsonArrayToPredicates(JSONArray array) {
        List<Predicate<?, ?>> predicates = new ArrayList<Predicate<?, ?>>();

        for (int i = 0; i < array.length(); i++) {

            try {
                JSONObject obj = array.getJSONObject(i);
                predicates.addAll(jsonObjectToPredicates(obj));
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return predicates;
    }

    /**
     * @param arr
     * @return
     */
    private List<Predicate<?, ?>> nativeArrayToPredicates(NativeArray arr) {
        List<Predicate<?, ?>> predicates = new ArrayList<Predicate<?, ?>>();

        for (Object o : arr.getIds()) {
            int index = (Integer) o;
            NativeObject obj = (NativeObject) arr.get(index, null);
            predicates.addAll(nativeObjectToPredicates(obj));
        }

        return predicates;
    }

    /**
     * @param object
     * @return
     */
    private List<Predicate<?, ?>> jsonObjectToPredicates(JSONObject object) {

        Iterator<String> itKeys = object.keys();
        List<Predicate<?, ?>> predicates = new ArrayList<Predicate<?, ?>>();

        while (itKeys.hasNext()) {
            String key = itKeys.next();
            Object value = null;
            try {
                value = object.get(key);
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            if (value instanceof JSONArray) {
                JSONArray array = (JSONArray) value;
                List<Predicate<?, ?>> predicatesList = jsonArrayToPredicates(array);
                if (predicatesList != null && predicatesList.size() > 0) {
                    Predicate<String, List<Predicate<?, ?>>> predicate = new Predicate<String, List<Predicate<?, ?>>>(key, predicatesList);
                    predicates.add(predicate);
                }
            } else {

                String[] qNameParts = key.split(":");

                QName filterQName = QName.createQName(qNameParts[0], qNameParts[1], namespaceService);

                Predicate<QName, String> predicate = new Predicate<QName, String>(filterQName, (String) value);
                predicates.add(predicate);
            }
        }

        return predicates;
    }

    /**
     * @param object
     * @return
     */
    private List<Predicate<?, ?>> nativeObjectToPredicates(NativeObject object) {

        List<Predicate<?, ?>> predicates = new ArrayList<Predicate<?, ?>>();

        Object[] propIds = NativeObject.getPropertyIds(object);
        for (Object propId : propIds) {
            String key = propId.toString();
            Object value = NativeObject.getProperty(object, key);

            if (value instanceof NativeArray) {
                NativeArray array = (NativeArray) value;
                List<Predicate<?, ?>> predicatesList = nativeArrayToPredicates(array);
                if (predicatesList != null && predicatesList.size() > 0) {
                    Predicate<String, List<Predicate<?, ?>>> predicate = new Predicate<String, List<Predicate<?, ?>>>(key, predicatesList);
                    predicates.add(predicate);
                }
            } else {

                String[] qNameParts = key.split(":");

                QName filterQName = QName.createQName(qNameParts[0], qNameParts[1], namespaceService);

                Predicate<QName, String> predicate = new Predicate<QName, String>(filterQName, (String) value);
                predicates.add(predicate);
            }
        }

        return predicates;
    }

    public JSONObject getListeNotification(NodeRef dossier) throws JSONException {
        JSONObject result = new JSONObject();

        Set<NodeRef> bureaux = parapheurService.getCurrentEtapeCircuit(dossier).getListeNotification();
        Set<NodeRef> baseNotif = parapheurService.getListeNotificationOriginale(dossier);
        HashMap<String, Boolean> notifications = new HashMap<String, Boolean>();

        if (baseNotif != null) {
            for (NodeRef nodeRef : baseNotif) {
                notifications.put(nodeRef.getId(), true);
            }
        }
        if (bureaux != null) {
            for (NodeRef bureau : bureaux) {
                if (!notifications.containsKey(bureau.getId())) {
                    notifications.put(bureau.getId(), false);
                }
            }
        }
        JSONArray not = new JSONArray();
        for (Map.Entry<String, Boolean> e : notifications.entrySet()) {
            JSONObject notif = new JSONObject();
            notif.put("id", e.getKey());
            notif.put("mandatory", e.getValue());
            not.put(notif);
        }
        result.put("notifications", not);
        return result;
    }

    public void setListeNotification(NodeRef dossier, NativeArray b) {

        List<NodeRef> bureaux = nativeArrayToNodeRefList(b);
        Set<NodeRef> setBureaux = new HashSet<NodeRef>(bureaux);
        EtapeCircuit current = parapheurService.getCurrentEtapeCircuit(dossier);

        for (NodeRef object : parapheurService.getListeNotificationOriginale(dossier))
            if (!setBureaux.contains(object))
                setBureaux.add(object);

        NodeRef parapheur = parapheurService.getParentParapheur(dossier);
        String user = AuthenticationUtil.getRunAsUser();

        Assert.isTrue(parapheurService.isParapheurOwnerOrDelegate(parapheur, user), "L'utilisateur ne fait pas partie des proprietaires ou délégués du bureau.");

        nodeService.setProperty(current.getNodeRef(), ParapheurModel.PROP_LISTE_NOTIFICATION, new ArrayList<NodeRef>(setBureaux));
        parapheurService.ajouterActeursExternes(dossier, new ArrayList<NodeRef>(setBureaux));
    }

    public String getPesPreviewUrl(NodeRef fileNode) {
        return dossierService.getPesPreviewUrl(fileNode);
    }

    public String getPesPreviewUrlFromArchive(NodeRef archive) {
        return dossierService.getPesPreviewUrlFromArchive(archive);
    }

    public boolean isPesViewEnabled(NodeRef dossierRef) {
        return dossierService.isPesViewEnabled(dossierRef);
    }

    public boolean isPesViewEnabled() {
        return dossierService.isPesViewEnabled();
    }

    /**
     * @deprecated les envois au tdt doivent être faits dans le worker. Cette fonction ne fonctionnera pas pour Helios.
     */
    @Deprecated
    public JSONObject sendTdt(String bureauCourant, String dossier, String nature, String classification, String numero, String date,
                              String objet, String annotPub, String annotPriv) throws Exception {
        JSONObject result = new JSONObject();
        Date dateActes;
        try {
            dateActes = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception e) {
            dateActes = null;
        }
        String name = (String) nodeService.getProperty(new NodeRef(dossier), ContentModel.PROP_TITLE);
        try {
            dossierService.sendTdtActes(new NodeRef(bureauCourant), new NodeRef(dossier), nature, classification, numero, dateActes, objet, annotPub, annotPriv);
            result.put(name, "success");
        } catch (Exception e) {
            result.put(name, e.getMessage());
        }
        return result;
    }

    public JSONObject getS2lowActesInfo() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("nature", new TreeMap<Integer, String>(s2lowService.getS2lowActesNatures()));
        result.put("classification", new TreeMap<String, String>(s2lowService.getS2lowActesClassifications()));
        return result;
    }

    public JSONObject getS2lowActesStatus(NodeRef dossier) throws IOException, JSONException {
        JSONObject result = new JSONObject();
        result.put("status", s2lowService.statutS2lowToString(s2lowService.getInfosS2low(dossier)));
        return result;
    }

    public boolean isSendTdt(NodeRef dossier) {
        return nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW);
    }

    public boolean isSendMailSec(String dossier) {
        return nodeService.getProperty(new NodeRef(dossier), ParapheurModel.PROP_MAILSEC_MAIL_ID) != null;
    }

    public JSONObject getConsecutiveSteps(NodeRef dossier, String username) throws JSONException {
        JSONObject json = new JSONObject();
        JSONArray jsonSteps = new JSONArray();
        List<EtapeCircuit> steps = dossierService.getConsecutiveSteps(dossier, username);
        for (EtapeCircuit step : steps) {
            JSONObject jsonstep = new JSONObject();
            jsonstep.put("titulaire", step.getParapheurName());
            jsonstep.put("metadatas", step.getListeMetadatas());
            jsonSteps.put(jsonstep);
        }
        json.put("steps", jsonSteps);
        return json;
    }

    public boolean isDossierLu(NodeRef dossier) {
        return dossierService.isDossierLu(dossier);
    }

    public JSONObject getImageForDocument(String dossier, String document, int numPage) throws JSONException {
        JSONObject json = new JSONObject();
        try {
            NodeRef image = dossierService.getImageForDocument(
                    new NodeRef(dossier),
                    new NodeRef(document),
                    numPage);
            if (image != null) {
                json.put("" + numPage, "/api/node/" + image.toString().replace("://", "/") + "/content");
            } else {
                throw new Exception("Aucun aperçu disponible");
            }
        } catch (Exception e) {
            if (e instanceof UndeclaredThrowableException) {
                json.put("exception", ((UndeclaredThrowableException) e).getUndeclaredThrowable().getMessage());
            } else {
                json.put("exception", e.getMessage());
            }
        }

        return json;
    }

    public int getPageCountForDocument(NodeRef document) {
        return dossierService.getPageCountForDocument(document);
    }

    public int getPageCountForDossier(NodeRef dossier) {
        int pageCount = 0;
        List<NodeRef> mainDocuments = parapheurService.getMainDocuments(dossier);
        if (!mainDocuments.isEmpty()) {
            pageCount = dossierService.getPageCountForDocument(mainDocuments.get(0));
        }
        return pageCount;
    }

    public JSONObject getImagesForDocument(NodeRef dossier, NodeRef document) throws JSONException {
        JSONObject path = new JSONObject();
        for (FileInfo file : fileFolderService.listFiles(dossierService.getDocumentImagesFolder(dossier, document, true))) {
            String fileName = file.getName();
            path.put(fileName.substring(0, fileName.indexOf(".")),
                    "/api/node/" + file.getNodeRef().toString().replace("://", "/") + "/content");
        }
        return path;
    }

    /**
     * Renvoie la liste des images générées pour le document passé en paramètre.
     *
     * @param dossier
     * @return la liste des images générées.
     */
    public JSONObject getGeneratedImagesForDossier(String dossier) throws JSONException {
        JSONObject res = new JSONObject();
        NodeRef dossierRef = new NodeRef(dossier);
        List<NodeRef> mainDocuments = parapheurService.getMainDocuments(dossierRef);

        for (NodeRef document : mainDocuments) {

            int pageCount = dossierService.getPageCountForDocument(document);

            res.put("name", (String) nodeService.getProperty(document, ContentModel.PROP_NAME));
            res.put("nodeRef", document);
            res.put("pageCount", pageCount);

            JSONObject images = new JSONObject();
            for (FileInfo file : fileFolderService.listFiles(dossierService.getDocumentImagesFolder(dossierRef, document, true))) {
                String fileName = file.getName();
                images.put(fileName.substring(0, fileName.indexOf(".")),
                        "/api/node/" + file.getNodeRef().toString().replace("://", "/") + "/content");
            }
            res.put("images", images);
        }
        return res;
    }

    /**
     * @see com.atolcd.parapheur.repo.DossierService#getActions(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    public List<String> getActions(NodeRef dossier, NodeRef bureau) {
        List<DossierService.ACTION_DOSSIER> actions = dossierService.getActions(dossier, bureau);
        List<String> actionsString = new ArrayList<String>(actions.size());
        for (DossierService.ACTION_DOSSIER action : actions) {
            actionsString.add(action.toString());
        }
        return actionsString;
    }

    /**
     * @see com.atolcd.parapheur.repo.ParapheurService#moveDossier(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    public void moveDossier(NodeRef dossier, NodeRef bureau) {
        parapheurService.moveDossier(dossier, bureau);
    }

    @SuppressWarnings("unused")
    public JSONArray handleListDossiers(String bureauId, String parentName, int count, NativeObject _filters, String propSort, NativeObject meta, String asc, int numPage, int pendingFiles, int index) throws JSONException {

        int newIndex = index;
        NodeRef bureauRef = null;
        if (bureauId != null) {
            bureauRef = new NodeRef("workspace", "SpacesStore", bureauId);
        }

        List<NodeRef> nodeRefList;
        JSONArray searchResult = new JSONArray();

        boolean isXemEnabled = BlexContext.getInstance().getXwvConfig().isServerUsable();

        if (bureauRef != null) {
            int reste = 0;
            do {
                nodeRefList = getDossiers(bureauRef, parentName, count + 1, newIndex, _filters, propSort, asc, numPage, pendingFiles);
                if (nodeRefList != null) {
                    reste = nodeRefList.size();
                    for (NodeRef dossier : nodeRefList) {
                        newIndex++;
                        reste--;
                        if (dossier != null && nodeService.exists(dossier) && !permissionService.hasReadPermission(dossier).toString().equals("DENIED")) {
                            JSONObject dossierResult = new JSONObject(extractDossierData(dossier, bureauRef, meta.has("metas", null) ? ((JSONArray) meta.get("metas", null)) : null));
                            String dossierResultProtocol = dossierResult.optString("protocol", "undefined");
                            dossierResult.put("isXemEnabled", isXemEnabled || StringUtils.equals(dossierResultProtocol, ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS));
                            searchResult.put(dossierResult);
                            if ((Boolean) dossierResult.get("locked")) {
                                pendingFiles++;
                                newIndex--;
                            }
                            if (searchResult.length() == count) {
                                break;
                            }
                        }
                    }
                }
            } while (searchResult.length() < count && nodeRefList != null && nodeRefList.size() > count);

            for (int j = 0; j < searchResult.length(); j++) {
                JSONObject dos = (JSONObject) searchResult.get(j);
                dos.put("skipped", newIndex);
                dos.put("pendingFile", pendingFiles);
                dos.put("total", searchResult.length() + (reste > 0 ? 1 : 0));
            }
        } else {
            nodeRefList = searchFolder(_filters, true);
            if (nodeRefList != null) {
                for (int i = 0; i < nodeRefList.size() && i < count; i++) {
                    NodeRef dossier = nodeRefList.get(i);
                    if (dossier != null && nodeService.exists(dossier) && !permissionService.hasReadPermission(dossier).toString().equals("DENIED")) {
                        JSONObject dossierResult = new JSONObject(extractSearchData(dossier));
                        searchResult.put(dossierResult);
                    }
                }
            }
        }

        return searchResult;
    }

    public JSONArray handleListArchives(int count, NativeObject _filters, String propSort, NativeObject meta, String asc, int numPage, int index) throws JSONException {

        int newIndex = index;

        List<NodeRef> nodeRefList;
        JSONArray searchResult = new JSONArray();

        boolean isXemEnabled = BlexContext.getInstance().getXwvConfig().isServerUsable();

        int reste = 0;
        do {
            nodeRefList = getArchives(count + 1, newIndex, _filters, propSort, asc, numPage);
            if (nodeRefList != null) {
                reste = nodeRefList.size();
                for (NodeRef dossier : nodeRefList) {
                    newIndex++;
                    reste--;
                    if (dossier != null && !permissionService.hasReadPermission(dossier).toString().equals("DENIED")) {
                        JSONObject dossierResult = new JSONObject(extractArchiveData(dossier, meta.has("metas", null) ? ((JSONArray) meta.get("metas", null)) : null));
                        String dossierResultProtocol = dossierResult.optString("protocol", "undefined");
                        dossierResult.put("isXemEnabled", isXemEnabled && StringUtils.equals(dossierResultProtocol, ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS));
                        searchResult.put(dossierResult);
                        if (searchResult.length() == count) {
                            break;
                        }
                    }
                }
            }
        } while (searchResult.length() < count && nodeRefList != null && nodeRefList.size() > count);

        for (int j = 0; j < searchResult.length(); j++) {
            JSONObject dos = (JSONObject) searchResult.get(j);
            dos.put("skipped", newIndex);
            dos.put("total", searchResult.length() + (reste > 0 ? 1 : 0));
        }

        return searchResult;
    }

    public JSONArray handleListDossiersAsAdmin(String bureauId, String type, String sousType, String title, String showOnlyCurrent, String showOnlyLate, String beforeEmit, String staticSince) {
        JSONArray searchResult = new JSONArray();

        String username = AuthenticationUtil.getRunAsUser();

        List<NodeRef> bureauxList = new ArrayList<NodeRef>();
        if (bureauId != null && !bureauId.isEmpty()) {
            bureauxList.add(new NodeRef("workspace", "SpacesStore", bureauId));
        }

        List<NodeRef> administres = parapheurService.getAllManagedParapheursByOpAdmin(username);

        if (parapheurService.isAdministrateur(username) || !administres.isEmpty()) {
            if (!administres.isEmpty() && bureauxList.isEmpty()) {
                bureauxList.addAll(administres);
            }

            String path;
            if (!bureauxList.isEmpty()) {
                path = "(";
                for (int i = 0; i < bureauxList.size(); i++) {
                    if (i > 0) {
                        path += " OR ";
                    }
                    String realName = nodeService.getPaths(bureauxList.get(i), false).get(0).last().getPrefixedString(namespaceService);
                    path += "PATH: \"" + "/" + this.companyHomeChildname + "/" + this.parapheursChildname + "/" + realName + "/*/*\"";
                }
                path += ")";
            } else {
                path = "PATH:\"/" + this.companyHomeChildname + "/" + this.parapheursChildname + "/*/*/*\"";
            }


            if (type != null && !type.isEmpty()) {
                path += " AND =ph:typeMetier:" + CakeFilter.luceneEscapeString(type);
            }
            if (sousType != null && !sousType.isEmpty()) {
                path += " AND =ph:soustypeMetier:" + CakeFilter.luceneEscapeString(sousType);
            }
            if (title != null && !title.isEmpty()) {
                path += " AND (cm:name:'" + CakeFilter.luceneEscapeString(title) +"' OR cm:title:'" + CakeFilter.luceneEscapeString(title) + "' OR cm:name:'" + CakeFilter.luceneEscapeString(String.format("*%s*", title)) +"' OR cm:title:'" + CakeFilter.luceneEscapeString(String.format("*%s*", title)) + "')";
            }
            if(Boolean.valueOf(showOnlyCurrent)) {
                path += " AND ph:termine:false";
            }
            if(staticSince != null) {
                SimpleDateFormat sf = new SimpleDateFormat("yyyy\\-MM\\-dd");
                Date date = new Date(Long.parseLong(staticSince));
                path += " AND cm:modified:[MIN TO " + sf.format(date) + "]";
            }
            if(Boolean.valueOf(showOnlyLate)) {
                path += " AND ASPECT:'ph:en-retard'";
            }
            SearchParameters searchParameters = new SearchParameters();
            searchParameters.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
            searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
            searchParameters.setQuery(path);
            /* permission check ? eager search */
            searchParameters.setMaxPermissionChecks(0);

            ResultSet resultSet = null;
            List<NodeRef> refs;
            try {
                resultSet = searchService.query(searchParameters);
                refs = resultSet.getNodeRefs();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                refs = new ArrayList<>();
            } finally {
                if (resultSet != null) {
                    resultSet.close();
                }
            }

            for (NodeRef ref : refs) {
                if (parapheurService.isDossier(ref)) {
                    Map map = extractDossierDataAsAdmin(ref);
                    boolean hasToAdd = true;
                    if(Boolean.valueOf(showOnlyCurrent)) {
                        hasToAdd = map.get("dateEmission") != null;
                    }
                    if(beforeEmit != null) {
                        hasToAdd = hasToAdd && (map.get("dateEmission") != null && (Long) map.get("dateEmission") < Long.parseLong(beforeEmit));
                    }
                    if(hasToAdd) {
                        searchResult.put(map);
                    }
                }
            }
        }

        return searchResult;
    }

    public Map extractDossierDataAsAdmin(NodeRef dossier) {
        HashMap<String, Object> result = new HashMap<String, Object>();

        NodeRef etape = parapheurService.getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);

        result.put("id", dossier.getId());
        result.put("title", nodeService.getProperty(dossier, ContentModel.PROP_TITLE));
        result.put("type", nodeService.getProperty(dossier, ParapheurModel.PROP_TYPE_METIER));
        result.put("sousType", nodeService.getProperty(dossier, ParapheurModel.PROP_SOUSTYPE_METIER));
        Date limite = (Date) nodeService.getProperty(dossier, ParapheurModel.PROP_DATE_LIMITE);
        result.put("dateLimite", limite != null ? limite.getTime() : null);
        if(etape != null) {
            Serializable dateEmission = nodeService.getProperty(etape, ParapheurModel.PROP_DATE_VALIDATION);
            if(dateEmission != null) {
                result.put("dateEmission", ((Date) dateEmission).getTime());
            }
        }

        NodeRef corbeille = nodeService.getPrimaryParent(dossier).getParentRef();
        NodeRef parentBureau = nodeService.getPrimaryParent(corbeille).getParentRef();
        result.put("parent", parentBureau.getId());
        result.put("banetteName", nodeService.getProperty(corbeille, ContentModel.PROP_TITLE));
        result.put("locked", nodeService.getProperty(dossier, ParapheurModel.PROP_LOCKED) != null ? nodeService.getProperty(dossier, ParapheurModel.PROP_LOCKED) : false);
        result.put("modified", ((Date)nodeService.getProperty(dossier, ContentModel.PROP_MODIFIED)).getTime());

        EtapeCircuit currentEtape = parapheurService.getCurrentOrRejectedEtapeCircuit(dossier);
        String actionDemandee = currentEtape != null ? currentEtape.getActionDemandee() : null;

        result.put("actionDemandee", actionDemandee);

        if ("TDT".equals(actionDemandee)) {
            result.put("isSent", nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW));
        } else if ("MAILSEC".equals(actionDemandee) || "MAILSECPASTELL".equals(actionDemandee)) {
            result.put("isSent", nodeService.getProperty(dossier, ParapheurModel.PROP_MAILSEC_MAIL_ID) != null);
        } else {
            result.put("isSent", false);
        }

        return result;
    }

    public Map extractSearchData(NodeRef dossier) {
        HashMap<String, Object> result = new HashMap<String, Object>();

        result.put("title", nodeService.getProperty(dossier, ContentModel.PROP_TITLE));
        result.put("id", dossier.getId());

        return result;
    }

    public String isEmitBy(NodeRef dossier) {
        String bureauTitle = "";
        NodeRef etape = parapheurService.getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        if(etape != null) {
            NodeRef bureau = (NodeRef) nodeService.getProperty(etape, ParapheurModel.PROP_PASSE_PAR);
            bureauTitle = (String) nodeService.getProperty(bureau, ContentModel.PROP_TITLE);
        }
        return bureauTitle;
    }

    public Map<String, Object> extractDossierData(NodeRef dossier, NodeRef bureau, JSONArray metaMap) throws JSONException {

        HashMap<String, Object> result = new HashMap<String, Object>();

        Map<QName, Serializable> props = nodeService.getProperties(dossier);

        String username = AuthenticationUtil.getRunAsUser();

        result.put("id", dossier.getId());
        result.put("title", props.get(ContentModel.PROP_TITLE));
        result.put("creator", parapheurService.getPrenomNomFromLogin((String) props.get(ContentModel.PROP_CREATOR)));
        result.put("emitby", isEmitBy(dossier));
        result.put("type", props.get(ParapheurModel.PROP_TYPE_METIER));
        result.put("sousType", props.get(ParapheurModel.PROP_SOUSTYPE_METIER));
        Date limite = (Date) props.get(ParapheurModel.PROP_DATE_LIMITE);
        if (limite != null) {
            result.put("dateLimite", limite.getTime());
        }
        result.put("dateEmission", ((Date) props.get(ContentModel.PROP_CREATED)).getTime());

        result.put("locked", props.get(ParapheurModel.PROP_LOCKED) != null ? props.get(ParapheurModel.PROP_LOCKED) : false);
        result.put("isSignPapier", props.get(ParapheurModel.PROP_SIGNATURE_PAPIER));
        result.put("readingMandatory", props.get(ParapheurModel.PROP_READING_MANDATORY));
        result.put("includeAnnexes", props.get(ParapheurModel.PROP_INCLUDE_ATTACHMENTS));
        result.put("protocol", props.get(ParapheurModel.PROP_TDT_PROTOCOLE) != null ? props.get(ParapheurModel.PROP_TDT_PROTOCOLE) : "");

        NodeRef corbeille = nodeService.getPrimaryParent(dossier).getParentRef();
        NodeRef parentBureau = nodeService.getPrimaryParent(corbeille).getParentRef();

        result.put("bureauName", nodeService.getProperty(parentBureau, ContentModel.PROP_TITLE));
        result.put("banetteName", nodeService.getProperty(corbeille, ContentModel.PROP_TITLE));

        result.put("hasRead", dossierService.hasReadDossier(dossier, username));
        result.put("isRead", isDossierLu(dossier));

        result.put("actions", getActions(dossier, bureau));

        String actionDemandee = dossierService.getActionDemandee(dossier);
        result.put("actionDemandee", actionDemandee);

        List<NodeRef> circuit = dossierService.getCircuit(dossier);

        NodeRef current = null;
        boolean isRejete = parapheurService.isRejete(dossier);
        for (NodeRef etape : circuit) {
            Boolean approved = (Boolean) nodeService.getProperty(etape, ParapheurModel.PROP_EFFECTUEE);
            if (approved != null && approved) {
                current = etape;
            } else {
                if (!isRejete) {
                    current = etape;
                    break;
                }
            }
        }

        List<String> tListeMetas = (List<String>) nodeService.getProperty(current, ParapheurModel.PROP_LISTE_METADONNEES);

        result.put("listeMetadatas", tListeMetas);

        List<String> tListeMetasRefus = (List<String>) nodeService.getProperty(current, ParapheurModel.PROP_LISTE_METADONNEES_REFUS);

        result.put("listeMetadatasRefus", tListeMetasRefus);


        if ("TDT".equals(actionDemandee)) {
            result.put("isSent", nodeService.hasAspect(dossier, ParapheurModel.ASPECT_S2LOW));
        } else if ("MAILSEC".equals(actionDemandee)) {
            result.put("isSent", props.get(ParapheurModel.PROP_MAILSEC_MAIL_ID) != null);
        } else if ("MAILSECPASTELL".equals(actionDemandee)) {
            result.put("isSent", props.get(ParapheurModel.PROP_STATUS_METIER).equals(StatusMetier.STATUS_EN_COURS_MAILSEC_PASTELL));
        } else {
            result.put("isSent", false);
        }

        SimpleDateFormat classicDateFormat = null;

        if (metaMap != null && metaMap.length() > 0) {
            for (int i = 0; i < metaMap.length(); i++) {
                Object property = props.get(QName.createQName((String) metaMap.get(i), namespaceService));
                if (property instanceof Date) {
                    if (classicDateFormat == null) {
                        classicDateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy");
                    }
                    result.put((String) metaMap.get(i), classicDateFormat.format((Date) property));
                } else if (property instanceof Boolean) {
                    result.put((String) metaMap.get(i), (Boolean) property ? "Oui" : "Non");
                } else {
                    result.put((String) metaMap.get(i), property);
                }
            }
        }

        List<NodeRef> documents = parapheurService.getMainDocuments(dossier);
        if (!documents.isEmpty()) {
            JSONArray docs = new JSONArray();
            for(NodeRef document : documents) {
                HashMap<String, Object> docPrincipal = new HashMap<String, Object>();
                docPrincipal.put("name", nodeService.getProperty(document, ContentModel.PROP_NAME));
                docPrincipal.put("id", document.getId());
                docs.put(docPrincipal);

            }
            result.put("documentsPrincipaux", docs);
        }

        return result;
    }

    public Map<String, Object> extractArchiveData(NodeRef dossier, JSONArray metaMap) throws JSONException {

        HashMap<String, Object> result = new HashMap<>();

        Map<QName, Serializable> props = nodeService.getProperties(dossier);

        result.put("id", dossier.getId());
        result.put("creator", parapheurService.getPrenomNomFromLogin((String) props.get(ContentModel.PROP_CREATOR)));
        result.put("created", ((Date) props.get(ContentModel.PROP_CREATED)).getTime());
        result.put("title", props.get(ContentModel.PROP_TITLE));
        result.put("original", props.get(ParapheurModel.PROP_ORIGINAL_NAME) != null ? "true" : "false");
        result.put("originalName", props.get(ParapheurModel.PROP_ORIGINAL_NAME));
        result.put("sig", props.get(ParapheurModel.PROP_SIG) != null ? "true" : "false");
        result.put("type", props.get(ParapheurModel.PROP_TYPE_METIER));
        result.put("sousType", props.get(ParapheurModel.PROP_SOUSTYPE_METIER));
        result.put("protocol", props.get(ParapheurModel.PROP_TDT_PROTOCOLE) != null ? props.get(ParapheurModel.PROP_TDT_PROTOCOLE) : "");

        SimpleDateFormat classicDateFormat = null;

        if (metaMap != null && metaMap.length() > 0) {
            for (int i = 0; i < metaMap.length(); i++) {
                Object property = props.get(QName.createQName((String) metaMap.get(i), namespaceService));
                if (property instanceof Date) {
                    if (classicDateFormat == null) {
                        classicDateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy");
                    }
                    result.put((String) metaMap.get(i), classicDateFormat.format((Date) property));
                } else if (property instanceof Boolean) {
                    result.put((String) metaMap.get(i), (boolean) property ? "Oui" : "Non");
                } else {
                    result.put((String) metaMap.get(i), property);
                }
            }
        }

        return result;
    }

    public JSONObject getProperties(NodeRef nodeRef) throws JSONException {
        JSONObject result = new JSONObject();
        Map<QName, Serializable> props = nodeService.getProperties(nodeRef);
        for (Map.Entry<QName, Serializable> prop : props.entrySet()) {
            result.put(prop.getKey().toString(), prop.getValue());
        }
        return result;
    }

    public void unlock(NodeRef nodeRef) {
        nodeService.removeAspect(nodeRef, ParapheurModel.ASPECT_PENDING);
    }

    public void reorderDocuments(NodeRef dossier, String documentsStr) throws JSONException {
        if(!dossierService.isEmis(dossier)) {
            JSONArray documents = new JSONArray(documentsStr);

            // Construction de l'objet pour la fonction
            List<Map<String, Serializable>> docsList = new ArrayList<Map<String, Serializable>>();

            for(int i = 0; i < documents.length(); i++) {
                JSONObject obj = (JSONObject) documents.get(i);
                System.out.print(obj);
                Map<String, Serializable> map = new HashMap<String, Serializable>();
                map.put("id", obj.getString("id"));
                if(obj.has("isMainDocument")) {
                    map.put("isMainDocument", obj.getBoolean("isMainDocument"));
                } else {
                    map.put("isMainDocument", false);
                }
                docsList.add(map);
            }

            dossierService.setOrderDocuments(dossier, docsList);
        }
    }

    public void changeDossiersSubtype(String typeID, String oldID, String newID) {
        String username = AuthenticationUtil.getRunAsUser();

        if (parapheurService.isAdministrateur(username)) {

            String path = "PATH:\"/" + this.companyHomeChildname + "/" + this.parapheursChildname + "/*/*/*\"";

            path += " AND @" + CakeFilter.luceneEscapeString(QName.createQName("ph:typeMetier", namespaceService).toString()) + ":\"" + CakeFilter.luceneEscapeString(typeID) + "\"";
            path += " AND @" + CakeFilter.luceneEscapeString(QName.createQName("ph:soustypeMetier", namespaceService).toString()) + ":\"" + CakeFilter.luceneEscapeString(oldID) + "\"";

            SearchParameters searchParameters = new SearchParameters();
            searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
            searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
            searchParameters.setQuery(path);
            /* permission check ? eager search */
            searchParameters.setMaxPermissionChecks(0);

            ResultSet resultSet = null;
            List<NodeRef> refs = null;
            try {
                resultSet = searchService.query(searchParameters);
                refs = resultSet.getNodeRefs();
            } catch (IllegalArgumentException e) {
                //e.printStackTrace();
                refs = new ArrayList<NodeRef>();
            } finally {
                if (resultSet != null) {
                    resultSet.close();
                }
            }

            for (NodeRef ref : refs) {
                if (parapheurService.isDossier(ref) &&
                        nodeService.getProperty(ref, ParapheurModel.PROP_SOUSTYPE_METIER).equals(oldID) &&
                        nodeService.getProperty(ref, ParapheurModel.PROP_TYPE_METIER).equals(typeID)) {
                    nodeService.setProperty(ref, ParapheurModel.PROP_SOUSTYPE_METIER, newID);
                }
            }
        }
    }

    public void changeDossiersType(String oldID, String newID) {
        String username = AuthenticationUtil.getRunAsUser();

        if (parapheurService.isAdministrateur(username)) {

            String path = "PATH:\"/" + this.companyHomeChildname + "/" + this.parapheursChildname + "/*/*/*\"";


            path += " AND =ph:typeMetier:" + CakeFilter.luceneEscapeString(oldID) + "";

            SearchParameters searchParameters = new SearchParameters();
            searchParameters.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
            searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
            searchParameters.setQuery(path);
            /* permission check ? eager search */
            searchParameters.setMaxPermissionChecks(0);

            ResultSet resultSet = null;
            List<NodeRef> refs = null;
            try {
                resultSet = searchService.query(searchParameters);
                refs = resultSet.getNodeRefs();
            } catch (IllegalArgumentException e) {
                //e.printStackTrace();
                refs = new ArrayList<NodeRef>();
            } finally {
                if (resultSet != null) {
                    resultSet.close();
                }
            }

            for (NodeRef ref : refs) {
                if (parapheurService.isDossier(ref)) {
                    nodeService.setProperty(ref, ParapheurModel.PROP_TYPE_METIER, newID);
                }
            }
        }
    }

    public void addCustomSignatureToDocument(NodeRef dossier, NodeRef document, int x, int y, int width, int height, int page) {
        dossierService.addCustomSignatureToDocument(dossier, document, x, y, width, height, page);
    }

    public void deleteCustomSignatureFromDocument(NodeRef dossier, NodeRef document) {
        dossierService.deleteCustomSignatureFromDocument(dossier, document);
    }

    public String getCustomSignatures(NodeRef dossier, boolean cachet) {
        return dossierService.getCustomSignaturesJsonPosition(dossier, cachet);
    }

}
