package org.adullact.iparapheur.repo.runtime;

import org.alfresco.repo.processor.BaseProcessorExtension;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.net.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by lhameury on 29/02/16.
 */
public class RuntimeServiceScriptable extends BaseProcessorExtension {

    @Autowired
    private RuntimeService runtimeService;


    /**
     * Restart the xemelios Java application
     *
     * @throws IOException
     */
    public void restartXemelios() throws IOException {
        try {
            runtimeService.execCmdAt(runtimeService.getXemCmd() + " restart");
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to restart xemelios", e);
        }
    }

    /**
     * Get status of the xemelios Java application
     *
     * @throws IOException
     */
    public String statusXemelios() throws IOException {
        try {
            return runtimeService.execCmd(runtimeService.getXemCmd() + " status");
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to get xemelios status", e);
        }
    }

    /**
     * Get uptime for NginX
     *
     * @throws IOException
     */
    public String uptimeNginx() throws IOException {
        try {
            String pid = "`ps aux | grep -v grep | grep \"nginx\" | awk '{print $2}' | head -1`";
            return uptimeProcess(pid);
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to get xemelios status", e);
        }
    }

    /**
     * Get uptime for process
     *
     * @throws IOException
     */
    private String uptimeProcess(String pid) throws IOException {
        try {
            String uptimeCmd = "ps -p " + pid + " -oetime= | tr '-' ':' | awk -F: '{ total=0; m=1; } { for (i=0; i < NF; i++) {total += $(NF-i)*m; m *= i >= 2 ? 24 : 60 }} {print total}'";
            return runtimeService.execCmd(uptimeCmd);
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to get xemelios status", e);
        }
    }

    /**
     * Get status of the office service
     *
     * @throws IOException
     */
    public boolean statusOffice() throws IOException {
        try {
            return runtimeService.statusOffice();
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to get xemelios status", e);
        }
    }

    /**
     * Get status of the redis service
     *
     * @throws IOException
     */
    public boolean statusRedis() throws IOException {
        try {
            return runtimeService.statusRedis();
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to get redis status", e);
        }
    }


    /**
     * Get uptime for Office
     *
     * @throws IOException
     */
    public String uptimeOffice() throws IOException {
        try {
            String pid = "`ps aux | grep -v grep | grep \"soffice\" | awk '{print $2}' | head -1`";
            return uptimeProcess(pid);
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to get xemelios status", e);
        }
    }

    /**
     * Get disk details
     *
     * @throws IOException
     */
    public String diskDetails() throws IOException {
        try {
            return runtimeService.execCmd("df -h");
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to get disk details", e);
        }
    }

    /**
     * Get memory details
     *
     * @throws IOException
     */
    public String memoryDetails() throws IOException {
        try {
            return runtimeService.execCmd("free -m");
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to get memory details", e);
        }
    }

    /**
     * Count folders
     *
     * @throws IOException
     */
    public String countDossiers() throws IOException {
        String countRequest = "SELECT count(*) FROM alf_node AS n, alf_qname AS q, alf_store AS s WHERE n.type_qname_id=q.id AND n.store_id=s.id AND q.local_name='dossier' AND s.protocol='workspace' AND n.node_deleted=0;";
        try {
            return runtimeService.execSQL(countRequest);
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to count folders", e);
        }
    }

    /**
     * Count archives
     *
     * @throws IOException
     */
    public String countArchives() throws IOException {
        String countRequest = "SELECT count(*) FROM alf_node AS n, alf_qname AS q, alf_store AS s WHERE n.type_qname_id=q.id AND n.store_id=s.id AND q.local_name='archive' AND s.protocol='workspace' AND n.node_deleted=0;";
        try {
            return runtimeService.execSQL(countRequest);
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to count archives", e);
        }
    }

    /**
     * Find orphan nodes
     *
     * @throws IOException
     */
    public String findOrphans() throws IOException {
        String orphanRequest = "SELECT COUNT(*) FROM alf_node WHERE node_deleted=0 AND id NOT IN(SELECT root_node_id FROM alf_store) AND id NOT IN(SELECT child_node_id FROM alf_child_assoc);";
        try {
            return runtimeService.execSQL(orphanRequest);
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to find orphans", e);
        }
    }

    /**
     * Get current server certificates informations
     */
    public JSONObject getCertChain()
            throws InvalidNameException, JSONException, KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException,
            EncoderException {

        List<Certificate> servercert = Arrays.asList(runtimeService.getCertChain());

        Base64 encoder = new Base64(64);

        JSONObject result = new JSONObject();
        JSONArray chain = new JSONArray();

        String cert_begin = "-----BEGIN CERTIFICATE-----\n";
        String end_cert = "-----END CERTIFICATE-----";

        StringBuilder cacert = new StringBuilder();

        StringBuilder trust = new StringBuilder();

        KeyStore trustStore = KeyStore.getInstance("JKS");
        String trustStorePasswd = "truststore";
        trustStore.load(null);

        boolean isFirst = true;

        for (Certificate cert : servercert) {
            if (cert instanceof X509Certificate) {
                X509Certificate x = (X509Certificate) cert;
                String dn = x.getSubjectDN().getName();
                LdapName ldapDN = new LdapName(dn);
                String CN = "";
                for (Rdn rdn : ldapDN.getRdns()) {
                    if (rdn.getType().equals("CN")) {
                        CN = (String) rdn.getValue();
                    }
                }
                if (servercert.indexOf(cert) != 0) {
                    if (isFirst) {
                        isFirst = false;
                    } else {
                        cacert.append("\r\n");
                    }
                    cacert.append(cert_begin);
                    cacert.append(new String(encoder.encode(cert.getEncoded())));
                    cacert.append(end_cert);

                    trustStore.setCertificateEntry(CN, cert);
                } else {
                    trust.append(cert_begin);
                    trust.append(new String(encoder.encode(cert.getEncoded())));
                    trust.append(end_cert);
                }
            }
        }

        // Do this in reverse ! Get the root cert first...
        Collections.reverse(servercert);

        for (Certificate cert : servercert) {
            if (cert instanceof X509Certificate) {
                JSONObject obj = new JSONObject();
                X509Certificate x = (X509Certificate) cert;
                String dn = x.getSubjectDN().getName();
                LdapName ldapDN = new LdapName(dn);
                for (Rdn rdn : ldapDN.getRdns()) {
                    obj.put(rdn.getType(), rdn.getValue());
                }
                obj.put("notBefore", x.getNotBefore().getTime());
                obj.put("notAfter", x.getNotAfter().getTime());
                chain.put(obj);
            }
        }


        ByteArrayOutputStream writeStream = new ByteArrayOutputStream();
        trustStore.store(writeStream, trustStorePasswd.toCharArray());

        result.put("chain", chain);
        result.put("cacert", new String(encoder.encode(cacert.toString().getBytes())));
        result.put("trust", new String(encoder.encode(trust.toString().getBytes())));
        result.put("truststore", new String(encoder.encode(writeStream.toByteArray())));

        return result;
    }
}
