package org.adullact.iparapheur.repo.template;

import org.alfresco.repo.jscript.BaseScopableProcessorExtension;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 27/08/12
 * Time: 15:08
 * To change this template use File | Settings | File Templates.
 */
public class JSON extends BaseScopableProcessorExtension {
    public String quote(String s) {
        return JSONObject.quote(s);
    }
}
