package org.adullact.iparapheur.repo.notification.socket;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.corundumstudio.socketio.annotation.OnEvent;

/**
 * Created by lhameury on 06/01/14.
 */
public class SocketListener {

    @OnEvent("binding")
    public void onBindingHandler(SocketIOClient client, SocketUserEvent data, AckRequest ackRequest) {
        SocketServer.bind(data.getUserName(), client);
    }

    @OnDisconnect
    public void onDisconnectHandler(SocketIOClient client) {
        SocketServer.unbind(client);
    }
}
