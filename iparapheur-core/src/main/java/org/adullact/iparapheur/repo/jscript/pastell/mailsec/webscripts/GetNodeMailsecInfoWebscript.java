package org.adullact.iparapheur.repo.jscript.pastell.mailsec.webscripts;

import org.adullact.iparapheur.repo.jscript.pastell.mailsec.MailsecPastellService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;

/**
 * Créé par lhameury le 21/05/18.
 */
public class GetNodeMailsecInfoWebscript extends AbstractWebScript {

    @Autowired
    private MailsecPastellService mailsecPastellService;

    public GetNodeMailsecInfoWebscript() {
    }

    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String nodeId = webScriptRequest.getServiceMatch().getTemplateVars().get("id");

        try {
            webScriptResponse.setStatus(200);
            webScriptResponse.setContentEncoding("UTF-8");
            webScriptResponse.setContentType(WebScriptResponse.JSON_FORMAT);

            NodeRef node = new NodeRef(
                    StoreRef.STORE_REF_WORKSPACE_SPACESSTORE.getProtocol(),
                    StoreRef.STORE_REF_WORKSPACE_SPACESSTORE.getIdentifier(),
                    nodeId);

            JSONObject object = new JSONObject();
            object.put("serverId", mailsecPastellService.getServerId(node));
            object.put("mailId", mailsecPastellService.getMailsecId(node));

            webScriptResponse.getWriter().write(object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            webScriptResponse.setStatus(500);
            webScriptResponse.getWriter().write(e.getLocalizedMessage());
        }
    }
}
