package org.adullact.iparapheur.repo.notification;

import org.alfresco.service.cmr.repository.NodeRef;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

/**
 * Created by lhameury on 14/05/14.
 * @author jmaire
 */

/**
 * Cette interface de créer et de diffuser les notifications aux utilisateurs concernés par rapport à une action
 * sur un dossier.
 * On va notifier suivant l'état du dossier et l'action faite dessus tout ou partie de ses acteurs (créateur, valideur,
 * liste notification, délégués, secrétaires et émetteur WebService).
 * C'est le NotificationCenter qui va choisir si ces notifications seront envoyées directement ou enregistées pour un
 * envoi ultérieur.
 * @see com.atolcd.parapheur.repo.NotificationCenter
 * **/
public interface NotificationService {

    /**
     * Diffusion des notifications aux utilisateurs concernés par une validation (action passant par la fonction
     * ApproveV4 et qui fait avancer le dossier, ex. visa, signature).
     * @param dossier le dossier sur lequel l'action vient d'être faite.
     * @param bureauCourant le bureau avec lequel on a fait l'action.
     */
    void notifierPourValidation(NodeRef dossier, NodeRef bureauCourant);

    /**
     * Diffusion des notifications aux utilisateurs concernés par un rejet sur un dossier.
     * @param dossier le dossier rejeté.
     */
    void notifierPourRejet(NodeRef dossier);

    /**
     * Diffusion des notifications aux utilisateurs concernés par une suppression par un admin.
     * @param dossier le dossier supprimé
     * @param statusTdt Le statut Tdt s'il y en a un (pour générer le bordereau)
     * @param ackDate Le date de Tdt s'il y en a une (pour générer le bordereau)
     */
    void notifierPourSuppressionAdmin(NodeRef dossier, String statusTdt, String ackDate);

    void notifierPourSuppression(NodeRef emetteur, String dossierTitre, boolean isInSecretariat, NodeRef parentCorbeille, String id);

    /**
     * Diffusion des notifications aux utilisateurs concernés après un droit de remord.
     * @param dossier le dossier supprimé
     */
    void notifierPourRemord(NodeRef dossier);

    /**
     * Diffusion des notifications aux utilisateurs concernés après un envoi ou un retour du secrétariat.
     * @param dossier le dossier
     */
    void notifierPourSecretariat(NodeRef dossier);

    /**
     * Diffusion des notifications aux utilisateurs concernés par un archivage.
     * Cette fonction est appelée avant l'archivage pour avoir accès aux infos du dossier.
     * @param dossier
     * @param nomArchive le nom que l'utilisateur a choisi pour l'archive
     */
    void notifierPourArchivage(NodeRef dossier, String nomArchive);

    void sendPreparedNotificationsForDossier(String id);

    /**
     * Diffusion des notifications après qu'un administrateur ait déplacé un dossier (transfert de bureau)
     * On ne va notifier que les propriétaire et délégués du bureau sur lequel est arrivé le dossier.
     * @param dossier
     * @param banette nom de la banette sur laquelle se trouve le dossier (c'est le même avant et après le move)
     * @param bureauPrecedent bureau sur lequel se trouvait le dossier avant le move
     */
    void notifierPourDeplacement(NodeRef dossier, String banette, NodeRef bureauPrecedent);

    /**
     * Diffusion des notifications après la création d'un dossier.
     * On ne va notifier que les utilisateurs concernés par web socket (pour mise à jour de l'interface).
     * @param dossier le dossier créé
     * @param bureau le bureau sur lequel a été créé le dossier
     */
    void notifierPourCreation(NodeRef dossier, NodeRef bureau);

    /**
     * Diffusion des notifications après l'envoi au TDT.
     * On ne va notifier que les utilisateurs concernés par web socket (pour mise à jour de l'interface).
     * @param dossier le dossier envoyé
     * @param bureau le bureau depuis lequel a été envoyé le dossier
     */
    void notifierPourTdT(NodeRef dossier, NodeRef bureau);

    /**
     * Diffusion des notifications après le passage en signature papier d'un dossier.
     * On ne va notifier que les utilisateurs concernés par web socket (pour mise à jour de l'interface).
     * @param dossier le dossier créé
     */
    void notifierPourPassageSignaturePapier(NodeRef dossier);

    /**
     * Diffusion des notifications après la génération du SignInfo d'un dossier.
     * On ne va notifier que les utilisateurs concernés par web socket (pour mise à jour de l'interface).
     * @param dossier le dossier créé
     */
    void notifierPourSignInfo(@NotNull NodeRef dossier, @NotNull  HashMap<String, String> info);

    /**
     * Diffusion des notifications après l'édition d'un dossier.
     * On ne va notifier que les utilisateurs concernés par web socket (pour mise à jour de l'interface).
     * @param dossier le dossier créé
     */
    void notifierPourEdition(NodeRef dossier);

    /**
     * Diffuse des notfications après une mise à jour de banette en retard
     * @param dossier le dossier en retard
     * @param corbeille la banette en retard qui contient le dossier (virtuelle)
     * @param alertByMail si vrai, un email sera envoyé, sinon on envoie seulement des message sur les websockets
     */
    void notifierPourRetard(NodeRef dossier, NodeRef corbeille, boolean alertByMail);

    void notifierPourRetard(NodeRef dossier, NodeRef corbeille, boolean alertByMail, boolean doDigest);

    void notifierPourRetardSingleUser(NodeRef bureau, NodeRef dossier, String user);

    /**
     * Diffuse les notifications lors d'un changement de délégation
     * @param suppleant bureau du délégué
     * @param created vrai sir la délégation a été créée, faux si supprimée
     */
    void notifierPourDelegation(NodeRef titulaire, NodeRef suppleant, boolean created);

    /**
     * Diffuse une notification sur les websockets pour indiquer une erreur de traitement
     * @param dossier le dossier dont le traitement est revenu en erreur
     * @param msg le message d'erreur
     */
    void notifierApresErreur(NodeRef dossier, String msg);

    /**
     * Diffuse une notification sur les websockets pour indiquer une mise à jour de corbeille virtuelle
     * @param dossier Le dossier impliqué par la notification
     * @param corbeille La corbeille impliquée
     * @param corbeilleName Le nom de la corbeille
     * @param toAdd Notification d'ajout ou de suppression
     */
    void notifierPourCorbeilleVirtuelle(NodeRef dossier, NodeRef corbeille, String corbeilleName, boolean toAdd);

    /**
     * Diffuse une notification sur les websockets lors d'une correction et réémission d'un dossier
     * @param dossier le dossier traité
     */
    void notifierPourRAZ(NodeRef dossier);

    /**
     * Diffuse une notification sur les websockets lors d'un enchainement de circuit
     * @param dossier le dossier traité
     */
    void notifierPourEnchainementCircuit(NodeRef dossier);
}
