/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.repo.EtapeCircuit;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import com.atolcd.parapheur.repo.annotations.Annotation;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;

/**
 * Classe de binding webscript pour la console de gestion des Circuits
 * 
 * NB: Ainsi, seul le constructeur est intéressant dans ce contexte, les autres
 *   méthodes sont "bouchees".
 * 
 * @author Vivien Barousse - ADULLACT Projet
 */
public class ScriptEtapeCircuitImpl implements EtapeCircuit {

    private String transition;

    private NodeRef parapheur;
    
    private Set<NodeRef> listeNotification;

    private String actionDemandee;

    public ScriptEtapeCircuitImpl(String transition, NodeRef parapheur, String actionDemandee, Set<NodeRef> listeNotification) {
        this.transition = transition;
        this.parapheur = parapheur;
        this.actionDemandee = actionDemandee;
        this.listeNotification = listeNotification;
    }

    @Override
    public NodeRef getParapheur() {
        return parapheur;
    }

    /**
     * ne sert à rien dans ce contexte
     * @return null
     */
    @Override
    public String getParapheurName() {
        return null;
    }

    @Override
    public boolean isApproved() {
        return false;
    }

    @Override
    public String getTransition() {
        return transition;
    }

    @Override
    public String getActionDemandee() {
        return actionDemandee;
    }

    @Override
    public String getAnnotation() {
        return "";
    }

    @Override
    public String getAnnotationPrivee() {
        return "";
    }

    @Override
    public String getSignataire() {
        return null;
    }

    @Override
    public java.util.Map<String,Object> getSignataireCertInfos() {
        return null;
    }

    @Override
    public Date getDateValidation() {
        return null;
    }

    @Override
    public String getSignature() {
        return null;
    }

    @Override
    public String getSignatureEtape() {
        return null;
    }

    @Override
    public Set<NodeRef> getListeNotification() {
        return listeNotification;
    }

    @Override
    public void setAndRecordListeDiffusion(NodeService nodeService, Set<NodeRef> liste) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getNotificationsExternes() {
        return null;
    }

    @Override
    public NodeRef getDelegateur() {
        return null;
    }

    @Override
    public String getDelegateurName() {
        return null;
    }
    
    @Override
    public NodeRef getDelegue() {
        return null;
    }

    @Override
    public String getDelegueName() {
        return null;
    }

    /**
     *  FIXME: trop de methodes "vides" verifier l'usage de cette classe.
     */
    @Override
    public String getValidator() {
        return null;
    }

    @Override
    public void setValidator(String username) {

    }


    @Override
    public HashMap<String, ArrayList<Annotation>> getGraphicalAnnotations() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public NodeRef getNodeRef() {
        return null;
    }

    @Override
    public Set<String> getListeMetadatas() {
        return null;
    }

    @Override
    public Set<String> getListeMetadatasRefus() {
        return null;
    }

    @Override
    public boolean isSigned() { return false; }
}
