package org.adullact.iparapheur.repo.notification.socket;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationEvent;
import org.springframework.extensions.surf.util.AbstractLifecycleBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lhameury on 06/01/14.
 */

public class SocketServer extends AbstractLifecycleBean {

    private final String subsystem = "i-Parapheur --- Socket.IO";

    private SocketIOServer server;

    public static Map<String, List<SocketIOClient>> sockets = new HashMap<String, List<SocketIOClient>>();

    private static Logger logger = Logger.getLogger(SocketServer.class);
    private String port;
    private String host;

    @Override
    protected void onBootstrap(ApplicationEvent applicationEvent) {
        logger.setLevel(Level.INFO);
        logger.info("Starting '" + subsystem + "' subsystem");

        Configuration config = new Configuration();
        config.setPort(Integer.valueOf(port));
        config.setHostname(host);

        server = new SocketIOServer(config);
        server.addListeners(new SocketListener());
        server.start();

        logger.info("Startup of '" + subsystem + "' subsystem complete");
    }

    @Override
    protected void onShutdown(ApplicationEvent applicationEvent) {
        logger.info("Stopping '" + subsystem + "' subsystem");
        for(List<SocketIOClient> clients: sockets.values()) {
            for(SocketIOClient client: clients) {
                client.disconnect();
            }
        }
        server.stop();
        logger.info("Stopped '" + subsystem + "' subsystem");
    }

    public void notify(SocketMessage socketMessage) {
        // On notifie tous les utilisateurs ?
        if (socketMessage.getUsers() == null) {
            server.getBroadcastOperations().sendEvent("message", socketMessage.getMsg().toString());
            logger.debug("***********WebSocket : sending message : " + socketMessage.getMsg().toString());
        }
        else {
            for (String user : socketMessage.getUsers()) {
                if (sockets.containsKey(user)) {
                    for(SocketIOClient client : sockets.get(user)) {
                        client.sendEvent("message", socketMessage.getMsg().toString());
                    }
                    logger.debug("***********WebSocket : sending message : " + socketMessage.getMsg().toString());
                }
                else {
                    logger.debug("***********WebSocket : user not connected for message : " + socketMessage.getMsg().toString());
                }
            }
        }
    }

    public static void bind(String username, SocketIOClient client) {
        if(!sockets.containsKey(username)) {
            List<SocketIOClient> clients = new ArrayList<SocketIOClient>();
            clients.add(client);
            sockets.put(username, clients);
        } else {
            sockets.get(username).add(client);
        }
    }

    public static void unbind(SocketIOClient client) {
        for(Map.Entry<String, List<SocketIOClient>> entry : sockets.entrySet()) {
            for(SocketIOClient cl: entry.getValue()) {
                if(cl.getSessionId().equals(client.getSessionId())) {
                    sockets.get(entry.getKey()).remove(cl);
                    return;
                }
            }
        }
    }

    public static List<String> getUsers() {
        return new ArrayList<String>(sockets.keySet());
    }

    public void setPort(String port) {
        this.port = port;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
