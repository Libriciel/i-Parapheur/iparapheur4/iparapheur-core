package org.adullact.iparapheur.repo.runtime;

import com.atolcd.parapheur.model.ParapheurModel;
import org.apache.log4j.Logger;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.sql.*;
import java.util.Properties;

import static com.oracle.net.Sdp.openSocket;

/**
 *
 * Created by lhameury on 29/02/16.
 */
public class RuntimeServiceImpl implements RuntimeService {

    private static Logger logger = Logger.getLogger(RuntimeService.class);
    private Properties configuration;
    private String xemCmd = null;


    private String redisHost;
    private String redisPort;

    private String pastellConnectorHost;
    private String pastellConnectorPort;

    private String pdfStampHost;
    private String pdfStampPort;


    public String getRedisHost() {
        return redisHost;
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }

    public String getRedisPort() {
        return redisPort;
    }

    public void setRedisPort(String redisPort) {
        this.redisPort = redisPort;
    }

    public String getPastellConnectorHost() {
        return pastellConnectorHost;
    }

    public void setPastellConnectorHost(String pastellConnectorHost) {
        this.pastellConnectorHost = pastellConnectorHost;
    }

    public String getPastellConnectorPort() {
        return pastellConnectorPort;
    }

    public void setPastellConnectorPort(String pastellConnectorPort) {
        this.pastellConnectorPort = pastellConnectorPort;
    }

    public String getPdfStampHost() {
        return pdfStampHost;
    }

    public void setPdfStampHost(String pdfStampHost) {
        this.pdfStampHost = pdfStampHost;
    }

    public String getPdfStampPort() {
        return pdfStampPort;
    }

    public void setPdfStampPort(String pdfStampPort) {
        this.pdfStampPort = pdfStampPort;
    }

    public boolean serverListening(String host, String port)
    {
        Socket s = null;
        try
        {
            s = new Socket(host, Integer.parseInt(port));
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
        finally
        {
            if(s != null)
                try {s.close();}
                catch(Exception e){}
        }
    }

    @Override
    public Properties getConfiguration() {
        return configuration;
    }

    /**
     * @param properties The configuration to set.
     */
    public void setConfiguration(Properties properties) {
        configuration = properties;
    }


    @Override
    public String getXemCmd() {
        if (xemCmd == null || "${parapheur.exploit.xemelios.command}".equals(xemCmd)) {
            xemCmd = this.configuration.getProperty(ParapheurModel.PARAPHEUR_XEM_CMD_KEY);
            if (xemCmd == null  || "${parapheur.exploit.xemelios.command}".equals(xemCmd)) {
                logger.warn("Propriete " + ParapheurModel.PARAPHEUR_XEM_CMD_KEY + " non trouvee, valeur utilisee par defaut = " + ParapheurModel.PARAPHEUR_XEM_CMD_KEY_DEFAULT);
                xemCmd = ParapheurModel.PARAPHEUR_XEM_CMD_KEY_DEFAULT;
            } else {
                logger.info("Propriete " + ParapheurModel.PARAPHEUR_XEM_CMD_KEY + " trouvee = " + xemCmd);
            }
        }
        return xemCmd;
    }

    @Override
    public String execCmd(String cmd) throws IOException {
        String ret = "";
        try {
            Process proc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", cmd}, new String[]{"LANG=fr_FR.UTF-8"});
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

            // read the output from the command
            String s;
            boolean firstLine = true;
            while ((s = stdInput.readLine()) != null) {
                if(firstLine) {
                    firstLine = false;
                } else {
                    ret += "\n";
                }
                ret += s;
            }
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to launch command " + cmd, e);
        }
        return ret;
    }

    @Override
    public void execCmdAt(String cmd) throws IOException {
        try {
            // Execute cmd now, without mailing the user
            execCmd("echo " + cmd + " | at -M now");
        } catch (Exception e) {
            // something went wrong
            throw new IOException("Error while trying to launch at command " + cmd, e);
        }
    }

    @Override
    public String execSQL(String request) throws IOException, ClassNotFoundException, SQLException {
        String dbUrl = this.configuration.getProperty("db.url");
        String dbUser = this.configuration.getProperty("db.username");
        String dbPwd = this.configuration.getProperty("db.password");
        String driver = this.configuration.getProperty("db.driver");

        Class.forName(driver);
        Connection conn = DriverManager.getConnection(dbUrl, dbUser, dbPwd);

        // create the java statement
        Statement st = conn.createStatement();

        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(request);

        if(rs.next()) {
            return rs.getString(1);
        } else {
            return "";
        }
    }

    @Override
    public boolean statusOffice() {
        boolean isOOoEnable = true;
        try {
            (new Socket("127.0.0.1", Integer.parseInt(this.configuration.getProperty("ooo.port")))).close();
        } catch (UnknownHostException e) {
            // unknown host
            isOOoEnable = false;
        } catch (IOException e) {
            // io exception, service probably not running
            isOOoEnable = false;
        }
        return isOOoEnable;
    }

    @Override
    public boolean statusRedis() {
        return serverListening(redisHost, redisPort);
    }

    @Override
    public Certificate[] getCertChain() {
        try {
            // create custom trust manager to ignore trust paths
            TrustManager trm = new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            };

            String hostname = this.configuration.getProperty("parapheur.hostname");

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[]{trm}, null);
            SSLSocketFactory factory = sc.getSocketFactory();
            SSLSocket socket = (SSLSocket) factory.createSocket();

            socket.setSoTimeout(2000);
            socket.connect(new InetSocketAddress(hostname, 443), 2000);
            socket.startHandshake();
            SSLSession session = socket.getSession();
            Certificate[] servercert = session.getPeerCertificates();
            socket.close();

            return servercert;
        } catch (Exception e) {
            logger.error("Impossible de récupérer les certificats serveur. Exception :", e);
        }
        return new Certificate[] {};
    }

}
