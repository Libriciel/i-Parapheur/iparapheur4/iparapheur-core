/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.adullact.iparapheur.repo.jscript.pastell.mailsec.webscripts;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.adullact.iparapheur.repo.jscript.pastell.mailsec.MailsecPastellConnector;
import org.adullact.iparapheur.repo.jscript.pastell.mailsec.MailsecPastellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;

/**
 * Créé par lhameury le 4/14/17.
 */
public class CreatePastellMailsecConnectorWebscript extends AbstractWebScript {

    @Autowired
    private MailsecPastellService mailsecPastellService;

    public CreatePastellMailsecConnectorWebscript() {
    }

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {

        final GsonBuilder builder = new GsonBuilder();
        final Gson gson = builder.create();

        try {

            MailsecPastellConnector connector = mailsecPastellService.create(
                    gson.fromJson(webScriptRequest.getContent().getReader(), MailsecPastellConnector.class)
            );

            if (connector != null) {
                webScriptResponse.setStatus(200);
                webScriptResponse.setContentEncoding("UTF-8");
                webScriptResponse.getWriter().write(gson.toJson(connector));
            } else {
                webScriptResponse.setStatus(409);
            }
        } catch (IllegalArgumentException e) {
            webScriptResponse.setStatus(400);
        }
    }
}
