package org.adullact.iparapheur.repo.notification;
/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2012, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 * contact@atolcd.com
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.NotificationCenter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.*;

/**
 * Notification. Modèle contenant toutes les informations utiles pour notifier des utilisateurs suite à une action
 * (ex. après une signature, un rejet, ...).
 *
 * @author Jason Maire <j.maire@adullact-projet.coop>
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 13/02/12
 *         Time: 14:30
 */
public class Notification implements Serializable {

    public enum Target {
        owner,
        current,
        diff,
        delegues,
        tiers,
        actors,
        secretariat,
        next,
        previous,
        batch_complete;
    }

    private static final long serialVersionUID = -2L;

    /**
     * Clef du champ Type dans le payload de la notification
     */
    public static final String KEY_TYPE = "type";
    /**
     * Qui on doit notifier (utilisateur courant, liste de diffusion, délégués, ...)
     */
    public static final String KEY_TARGET = "target";
    /**
     * Clef du champ NodeRef dans le payload de la notification
     */
    public static final String KEY_NODEREF = "nodeRef";
    /**
     * Clef du champ id dans le payload de la notification (dans le cas d'un dossier n'existant plus)
     */
    public static final String KEY_ID = "id";
    /**
     * Clé du champ bureauNotifie.
     * NodeRef du bureau que l'on notifie
     */
    private static final String KEY_BUREAU_NOTIFIE = "bureauNotifie";
    /**
     * clef du champ nomParapheurCourant dans le payload.
     * Role des utilisateurs que l'on notifie (nom du bureau avec indication de délégation/secrétariat)
     */
    private static final String KEY_ROLE_NOTIFIE = "nomParapheurCourant";
    /**
     * Clef du champ ArchiveNodeRef qui contient le nodeRef du dossier archivé.
     */
    public static final String KEY_ARCHIVE_NODEREF = "archiveNodeRef";
    /**
     * Clef du champ Annotation Publique dans le payload de la notification
     */
    public static final String KEY_ANNOTATION_PUBLIQUE = "annotationPublique";
    public static final String KEY_DOSSIER_NAME = "dossierName";
    /**
     * le nom de l'utilisateur ayant fait l'action
     */
    private static final String KEY_NOM_VALIDEUR = "nomValideur";
    /**
     * Le rôle du valideur (nom du bureau sur lequel le valideur a fait l'action)
     */
    private static final String KEY_ROLE_VALIDEUR = "nomParapheurValideur";
    /**
     * Clef du champ Reason dans le payload de la notification
     */

    private static final String KEY_USER_VALIDEUR = "userValideur";
    public static final String KEY_REASON = "reason";
    private static final String KEY_ACTION_DEMANDEE = "actionDemandee";
    private static final String KEY_ACTION_EFFECTUEE = "actionEffectuee";
    private static final String KEY_BANETTE = "banetteCourante";

    private UUID uuid;
    private String username;
    private Boolean read;
    private Long timestamp;
    private Long modifiedStamp;
    private HashMap<String, Serializable> payload;

    public List<String> getUsersTarget() {
        return usersTarget;
    }

    public void setUsersTarget(List<String> usersTarget) {
        this.usersTarget = usersTarget;
    }

    private List<String> usersTarget;


    public Notification() {
        // auto set the timestamp in the constructor (broadcasted notification share the same timestamp)
        setTimestamp(System.currentTimeMillis());
        setModifiedTimestamp(-1L);
    }

    public Notification(String target, NotificationCenter.Reason reason, NodeRef bureauNotifie, String roleNotifie, String nomValideur,
                        String userValideur, String roleValideur, String type, Object dossier, String dossierTitle, String actionDemandee,
                        String actionEffectuee, String annotPub, String banette) {

        this.username = userValideur;

        putStringInPayload(KEY_TARGET, target);
        putStringInPayload(KEY_REASON, reason.name());
        putSerializableInPayload(KEY_BUREAU_NOTIFIE, bureauNotifie);
        putStringInPayload(KEY_ROLE_NOTIFIE, roleNotifie);

        putStringInPayload(KEY_NOM_VALIDEUR, nomValideur);
        putStringInPayload(KEY_USER_VALIDEUR, userValideur);
        putStringInPayload(KEY_ROLE_VALIDEUR, roleValideur);

        putStringInPayload(KEY_TYPE, type);
        if (NotificationCenter.VALUE_TYPE_DOSSIER.equals(type)) {
            if(dossier instanceof NodeRef) {
                putSerializableInPayload(KEY_NODEREF, (NodeRef) dossier);
            } else if(dossier instanceof String) {
                putStringInPayload(KEY_ID, (String) dossier);
            }
            putStringInPayload(KEY_DOSSIER_NAME, dossierTitle);
            putOptStringInPayload(KEY_ANNOTATION_PUBLIQUE, annotPub);
            putOptStringInPayload(KEY_ACTION_DEMANDEE, actionDemandee);
            putOptStringInPayload(KEY_ACTION_EFFECTUEE, actionEffectuee);

        }
        putOptStringInPayload(KEY_BANETTE, banette);
    }

    public Notification(Notification notification) {
        this.payload = new HashMap<String, Serializable>(notification.getPayload());
        this.uuid = notification.uuid;
        this.username = notification.username;
        this.read = notification.read;
        this.timestamp = notification.timestamp;
        this.modifiedStamp = notification.modifiedStamp;
    }

    public Notification getNotificationForUser(String username) {
        this.uuid = UUID.randomUUID();
        this.username = username;
        this.timestamp = System.currentTimeMillis();
        this.modifiedStamp = -1L;
        this.read = false;
        return this;
    }

    public Map<String, Serializable> getPayload() {
        return payload;
    }

    public void setPayload(HashMap<String, Serializable> payload) {
        this.payload = payload;
    }

    protected String getStringFromPayload(String key) {
        return (String) getObjectFromPayload(key);
    }

    protected String getOptStringFromPayload(String key, String valueIfNull) {
        String value = (String) getObjectFromPayload(key);
        return (value == null)? valueIfNull : value;
    }

    protected void putStringInPayload(String key, String value) {
        putSerializableInPayload(key, value);
    }

    protected void putOptStringInPayload(String key, String value) {
        if (value != null) {
            putSerializableInPayload(key, value);
        }
    }

    protected void putSerializableInPayload(String key, Serializable value) {
        if (this.payload == null) {
            // initialize payload with empty map
            this.payload = new HashMap<String, Serializable>();
        }
        this.payload.put(key, value);
    }

    protected Serializable getObjectFromPayload(String key) {
        return this.payload.get(key);
    }

    public NotificationCenter.Reason getReason() {
        String reasonName = getStringFromPayload(KEY_REASON);

        return (reasonName != null)?
                NotificationCenter.Reason.valueOf(reasonName) :
                NotificationCenter.Reason.approve;
    }

    public NodeRef getDossier() {
        return (NodeRef) getObjectFromPayload(KEY_NODEREF);
    }

    public String getId() { return getOptStringFromPayload(KEY_ID, "inconnu"); }

    public void setId(String id) { putOptStringInPayload(KEY_ID, id); }

    public String getDossierName() {
        return getOptStringFromPayload(KEY_DOSSIER_NAME, "inconnu");
    }

    public NodeRef getBureauNotifie() {
        return (NodeRef) getObjectFromPayload(KEY_BUREAU_NOTIFIE);
    }

    public void setBureauNotifie(NodeRef bureauNotifie) {
        putSerializableInPayload(KEY_BUREAU_NOTIFIE, bureauNotifie);
    }

    public String getRoleNotifie() {
        return getOptStringFromPayload(KEY_ROLE_NOTIFIE, "inconnu");
    }

    public void setRoleNotifie(String roleNotifie) {
        putOptStringInPayload(KEY_ROLE_NOTIFIE, roleNotifie);
    }

    public String getNomValideur() {
        return getOptStringFromPayload(KEY_NOM_VALIDEUR, "inconnu");
    }

    public String getUserValideur() {
        return getOptStringFromPayload(KEY_USER_VALIDEUR, "inconnu");
    }

    public String getActionDemandee() {
        return getOptStringFromPayload(KEY_ACTION_DEMANDEE, "Action");
    }

    public String getActionEffectuee() {
        // Nouvelle propriété, pour les anciennes
        return getOptStringFromPayload(KEY_ACTION_EFFECTUEE, "Action");
    }

    public Target getTarget() {
        return Target.valueOf(getStringFromPayload(KEY_TARGET));
    }

    public void setTarget(Target target) {
        putOptStringInPayload(KEY_TARGET, target.name());
    }

    public String getType() {
        return getOptStringFromPayload(KEY_TYPE, "inconnu");
    }

    public String getAnnotation() {
        return getStringFromPayload(KEY_ANNOTATION_PUBLIQUE);
    }

    public String getBanette() {
        return getOptStringFromPayload(KEY_BANETTE, CorbeillesService.AUCUNE);
    }

    public void setBanette(String nomBanette) {
        putOptStringInPayload(KEY_BANETTE, nomBanette);
    }

    public UUID getUUID() {
        return uuid;
    }

    public void setUuidString(String uuidString) {
        uuid = UUID.fromString(uuidString);
    }

    public String getUuidString() {
        return uuid.toString();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getRead() {
        return read;
    }

    public Boolean isRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public long getModifiedTimestamp() {
        return modifiedStamp;
    }

    public void setModifiedTimestamp(Long modifiedStamp) {
        this.modifiedStamp = modifiedStamp;
    }

    public String toString() {
        String retval = new String();

        retval = "UUID=" + this.getUUID();
        retval += "|Username=" + this.getUsername();

        retval += "|Read=" + this.isRead();

        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(this.getTimestamp());

        retval += "|created=" + calendar.getTime();

        if (payload != null) {
            for (String key : payload.keySet()) {
                retval += "\n" + key + ":" + payload.get(key);
            }
        }

        return retval;
    }

    private boolean isEqualSafe(Object o1, Object o2) {
        if (o1 != null && o2 != null) {
            return o1.equals(o2);
        } else {
            if (o1 != o2) { // au moins un des deux est nul
                return false;
            }
            else if (o1 == o2) { // la tout le monde est nul
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        Notification n = (Notification) o;
        boolean retval = true;

        //FIXME: beware of NULLs !
        retval = this.uuid.equals(n.getUUID());
        retval = retval && isEqualSafe(this.username, n.getUsername());
        retval = retval && this.getTimestamp() == n.getTimestamp();
        retval = retval && this.isRead() == n.isRead();

        if (this.getPayload() != null) {
            if (n.getPayload() != null) {
                retval = retval && this.getPayload().equals(n.getPayload());
            }
            else {
                retval = retval && false;
            }
        }
        else if (n.getPayload() != null) {
            if (this.getPayload() != null) {
                retval = retval && this.getPayload().equals(n.getPayload());
            }
            else {
                retval = retval && false;
            }
        }
        return retval;
    }

    public JSONObject toJSONObject() {
        JSONObject json = new JSONObject();
        //FIXME: beware of NULLs !
        try {
            json.put("uuid", getUUID().toString());
            json.put("timestamp", getTimestamp());
            json.put("read", isRead());
            json.put("username", getUsername());
        } catch (JSONException e) {

        }

        return json;
    }
}
