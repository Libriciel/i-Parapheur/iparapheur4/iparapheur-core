/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.adullact.iparapheur.repo.jscript.seals;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.adullact.iparapheur.repo.jscript.seals.exceptions.MalformedNodeRefException;
import org.adullact.iparapheur.repo.jscript.seals.exceptions.NodeRefIsNotSealException;
import org.adullact.iparapheur.util.EncryptUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.extensions.surf.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Utilitaire pour gestion de certificat cachet
 * <p>
 * Créé par lhameury le 4/14/17.
 */
public class SealUtils {

    private static Logger log = Logger.getLogger(SealUtils.class);


    static boolean isComplete(@Nullable SealCertificate seal) {
        return (seal != null)
                && (seal.getTitle() != null)
                && (seal.getCertificate() != null)
                && (seal.getDeserializedPassword() != null)
                && (seal.getDescription() != null);
    }

    static @NotNull NodeRef getCertificatesNodeRef(@NotNull ServiceRegistry serviceRegistry) {

        NodeService nodeService = serviceRegistry.getNodeService();
        NamespaceService namespaceService = serviceRegistry.getNamespaceService();
        SearchService searchService = serviceRegistry.getSearchService();

        NodeRef certificatesCachetHome;
        String sealsPath = SealModel.TYPE_SEAL_CERTIFICATES.getPrefixedQName(namespaceService).getPrefixString();
        String policiesPath = "/app:company_home/app:dictionary/" + sealsPath;
        NodeRef rootNode = nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);

        List<NodeRef> policiesHomes = searchService.selectNodes(
                rootNode,
                policiesPath,
                null,
                namespaceService,
                false
        );

        if (policiesHomes.size() > 0) {
            certificatesCachetHome = policiesHomes.get(0);
        } else {
            NodeRef dictionary = searchService.selectNodes(
                    rootNode,
                    "/app:company_home/app:dictionary",
                    null,
                    namespaceService,
                    false
            ).get(0);

            certificatesCachetHome = nodeService.createNode(
                    dictionary,
                    ContentModel.ASSOC_CONTAINS,
                    SealModel.TYPE_SEAL_CERTIFICATES,
                    SealModel.TYPE_SEAL_CERTIFICATES
            ).getChildRef();
        }

        return certificatesCachetHome;
    }

    static @Nullable String getBase64Content(@Nullable ContentReader contentReader) {

        String result = null;

        if (contentReader != null) {
            // We have an image to show...
            try {
                byte[] imageBytes = IOUtils.toByteArray(contentReader.getContentInputStream());
                result = Base64.encodeBytes(imageBytes);
            } catch (IOException e) {
                log.error(e);
            } catch(ContentIOException e) {
                log.error(e);
            }
        }

        return result;

    }

    public static @Nullable SealCertificate createSealCertificate(@NotNull SealCertificate sealCertificate,
                                                                  @NotNull String certificateKey,
                                                                  @NotNull ServiceRegistry serviceRegistry) {
        final Gson gson = new GsonBuilder().create();
        NodeService nodeService = serviceRegistry.getNodeService();
        NamespaceService namespaceService = serviceRegistry.getNamespaceService();
        ContentService contentService = serviceRegistry.getContentService();

        NodeRef certsHome = SealUtils.getCertificatesNodeRef(serviceRegistry);

        // Verify that all required fields are here...
        if (!SealUtils.isComplete(sealCertificate)) {
            throw new IllegalArgumentException("parameter sealCertificate incomplete");
        }

        // We check for same name before save it
        List<ChildAssociationRef> existingList = nodeService.getChildAssocsByPropertyValue(certsHome, ContentModel.PROP_TITLE, sealCertificate.getTitle());
        if (existingList.isEmpty()) {
            String uuid = UUID.randomUUID().toString();

            Map<QName, Serializable> properties = new HashMap<>();
            properties.put(ContentModel.PROP_NAME, uuid);
            properties.put(ContentModel.PROP_TITLE, sealCertificate.getTitle());
            properties.put(ContentModel.PROP_DESCRIPTION, gson.toJson(sealCertificate.getDescription()));
            properties.put(SealModel.PROP_CERTIFICATE_NAME, sealCertificate.getOriginalName());
            properties.put(ContentModel.PROP_PASSWORD, EncryptUtils.encrypt(sealCertificate.getDeserializedPassword(), certificateKey));
            properties.put(SealModel.PROP_ADDITIONAL_TEXT, sealCertificate.getAdditionalText());

            NodeRef newCert = nodeService.createNode(certsHome,
                    ContentModel.ASSOC_CONTAINS,
                    QName.createQName("ph:certificate" + uuid, namespaceService),
                    SealModel.TYPE_SEAL_CERTIFICATE, properties).getChildRef();

            // We create the content for certificate
            ContentWriter writer = contentService.getWriter(newCert, ContentModel.PROP_CONTENT, true);
            writer.setMimetype("application/x-pkcs12");
            writer.putContent(new ByteArrayInputStream(Base64.decode(sealCertificate.getCertificate())));

            // We create the content for image
            if (sealCertificate.getImage() != null) {
                ContentWriter imgWriter = contentService.getWriter(newCert, SealModel.PROP_SEAL_IMAGE, true);
                imgWriter.putContent(new ByteArrayInputStream(Base64.decode(sealCertificate.getImage())));
            }

            sealCertificate.setId(newCert.getId());

            return sealCertificate;
        } else {
            return null;
        }
    }

    public static @NotNull SealCertificate updateSealCertificate(@NotNull SealCertificate sealCertificate,
                                                                 @NotNull String certificateKey,
                                                                 @NotNull ServiceRegistry serviceRegistry) {
        final Gson gson = new GsonBuilder().create();

        // Find the node and properties

        NodeRef certRef = new NodeRef("workspace://SpacesStore/" + sealCertificate.getId());
        NodeService nodeService = serviceRegistry.getNodeService();

        // CertificateName

        if (sealCertificate.getOriginalName() != null) {
            nodeService.setProperty(certRef, SealModel.PROP_CERTIFICATE_NAME, sealCertificate.getOriginalName());
        }

        // Password

        if (sealCertificate.getDeserializedPassword() != null) {
            String encryptedPassword = EncryptUtils.encrypt(sealCertificate.getDeserializedPassword(), certificateKey);
            nodeService.setProperty(certRef, ContentModel.PROP_PASSWORD, encryptedPassword);
        }

        // Description, should be changed if the certificate is changed too.

        if ((sealCertificate.getDescription() != null) || (sealCertificate.getCertificate() != null))
            nodeService.setProperty(certRef, ContentModel.PROP_DESCRIPTION, gson.toJson(sealCertificate.getDescription()));

        // Certificate

        if (sealCertificate.getCertificate() != null) {
            ContentWriter writer = serviceRegistry.getContentService().getWriter(certRef, ContentModel.PROP_CONTENT, true);
            writer.setMimetype("application/x-pkcs12");
            writer.putContent(new ByteArrayInputStream(Base64.decode(sealCertificate.getCertificate())));
        }

        // Image

        if (sealCertificate.getImage() != null) {
            ContentWriter imgWriter = serviceRegistry.getContentService().getWriter(certRef, SealModel.PROP_SEAL_IMAGE, true);
            imgWriter.putContent(new ByteArrayInputStream(Base64.decode(sealCertificate.getImage())));
        }

        // additional text
        nodeService.setProperty(certRef, SealModel.PROP_ADDITIONAL_TEXT, sealCertificate.getAdditionalText());

        return sealCertificate;
    }

    public static void deleteSealCertificate(String id, @NotNull ServiceRegistry serviceRegistry) {

        NodeService nodeService = serviceRegistry.getNodeService();

        // we have to check that the node is child of cachetCertificates folder

        NodeRef certsHome = SealUtils.getCertificatesNodeRef(serviceRegistry);

        // We check that this ID is good
        Pattern p = Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f‌​]{4}-[0-9a-f]{12}$");
        if (!p.matcher(id).matches()) {
            // Bad id...
            throw new MalformedNodeRefException("Bad id");
        }

        NodeRef certRef = new NodeRef("workspace://SpacesStore/" + id);

        // we check that node is child of certsHome
        if (nodeService.getPrimaryParent(certRef).getParentRef().equals(certsHome)) {
            nodeService.deleteNode(certRef);
        } else {
            // This is not a seal node !
            throw new NodeRefIsNotSealException();
        }
    }

    public static @NotNull List<SealCertificate> listSealCertificates(@NotNull ServiceRegistry serviceRegistry) {
        final Gson gson = new GsonBuilder().create();

        NodeService nodeService = serviceRegistry.getNodeService();
        ContentService contentService = serviceRegistry.getContentService();

        NodeRef certsHome = SealUtils.getCertificatesNodeRef(serviceRegistry);
        List<SealCertificate> certificatesList = new ArrayList<SealCertificate>();

        List<ChildAssociationRef> children = nodeService.getChildAssocs(certsHome);
        for (ChildAssociationRef child : children) {

            NodeRef childRef = child.getChildRef();
            SealCertificate certificate = new SealCertificate();

            certificate.setTitle((String) nodeService.getProperty(childRef, ContentModel.PROP_TITLE));
            certificate.setId(childRef.getId());
            certificate.setOriginalName((String) nodeService.getProperty(childRef, SealModel.PROP_CERTIFICATE_NAME));

            String descJson = (String) nodeService.getProperty(childRef, ContentModel.PROP_DESCRIPTION);
            certificate.setDescription(gson.fromJson(descJson, SealCertificate.CachetDescription.class));

            ContentReader imageContentReader = contentService.getReader(childRef, SealModel.PROP_SEAL_IMAGE);
            String imageBase64 = getBase64Content(imageContentReader);
            certificate.setImage(imageBase64);

            certificate.setAdditionalText((String) nodeService.getProperty(childRef, SealModel.PROP_ADDITIONAL_TEXT));

            certificatesList.add(certificate);
        }

        return certificatesList;
    }

    public static @Nullable String getMailForWarnMail(@NotNull ServiceRegistry serviceRegistry) {
        return (String) serviceRegistry.getNodeService().getProperty(
                SealUtils.getCertificatesNodeRef(serviceRegistry),
                SealModel.PROP_WARN_MAIL
        );
    }

    public static void setMailForWarnMail(@Nullable String mailToWarn, @NotNull ServiceRegistry serviceRegistry) {
        serviceRegistry.getNodeService().setProperty(
                SealUtils.getCertificatesNodeRef(serviceRegistry),
                SealModel.PROP_WARN_MAIL, mailToWarn
        );
    }
}
