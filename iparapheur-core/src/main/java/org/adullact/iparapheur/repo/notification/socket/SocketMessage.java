package org.adullact.iparapheur.repo.notification.socket;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lhameury on 14/01/14.
 */
public class SocketMessage {

    private static Logger logger = Logger.getLogger(SocketMessage.class);

    private List<String> banettes;

    public enum State {
        NEW, // utilisé quand un dossier arrice dans une banette
        END, // utilisé quand un dossier quitte une banette
        CREATED, // utilisé quand un dossier est créé
        REJECTED, // utilisé quand un dossier est rejeté
        EMITTED, // utilisé quand un dossier est emis
        ERROR, // utilisé lorsqu'une erreur est intervenue lors de l'action demandée sur le dossier
        SENT, // utilisé quand un dossier est envoyé (tdt, mailsec)
        EDITED // utilisé quand un dossier est édité
    }

    private List<String> users = new ArrayList<String>();
    private JSONObject msg = new JSONObject();
    private State state;

    public SocketMessage() {

    }

    public SocketMessage(List<String> users) {
        this.users = users;
    }

    public SocketMessage(List<String> users, JSONObject msg) {
        this.users = users;
        this.msg = msg;
    }

    /**
     * Création d'un objet de notification
     * @param users utilisateurs
     * @param message message
     * @param id id
     * @param action action
     * @param titre titre
     * @param state state
     */
    public SocketMessage(List<String> users, String bureauId, String message, String id, String action, String titre, State state, List<String> banettes) {
        this.users = users;
        try {
            msg.put("message", message);
            msg.put("id", id);
            msg.put("bureauId", bureauId);
            msg.put("action", action);
            msg.put("titre", titre);
            if(state != null) {
                msg.put("state", state.name());
            }
            msg.put("banettes", banettes);
        } catch (JSONException e) {
            logger.warn("Impossible de créer un objet SocketMessage", e);
        }
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public JSONObject getMsg() {
        return msg;
    }

    public void setMsg(JSONObject msg) {
        this.msg = msg;
    }

    public void setState(State state) {
        if(state != null) {
            try {
                msg.put("state", state.name());
            } catch (JSONException e) {
                logger.warn("Impossible de définir l'état de la SocketMessage", e);
            }
        }
    }

    public void setBanettes(List<String> banettes) {
        this.banettes = banettes;
    }
}
