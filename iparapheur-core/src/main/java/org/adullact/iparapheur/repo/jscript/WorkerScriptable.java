package org.adullact.iparapheur.repo.jscript;

import org.adullact.iparapheur.repo.amq.MessagesSender;
import org.adullact.iparapheur.repo.worker.SchedulerService;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.mozilla.javascript.NativeArray;
import org.springframework.beans.factory.annotation.Autowired;

public class WorkerScriptable extends BaseProcessorExtension {

    @Autowired
    private MessagesSender messagesSender;

    @Autowired
    private SchedulerService schedulerService;

    @SuppressWarnings("UnusedDeclaration") //Used from API
    public void sendWorker(String objString, String id) {
        messagesSender.sendWorker(objString, id);
    }

    @SuppressWarnings("UnusedDeclaration") //Used from API
    public void sendWorker(String objString) {
        messagesSender.sendWorker(objString);
    }

    public NativeArray getCurrentActions() {
        return new NativeArray(schedulerService.getCurrentActions().values().toArray());
    }

    public boolean cancelWorker(String id) {
        return schedulerService.cancelWorker(id);
    }
}
