package org.adullact.iparapheur.repo.jscript;

import org.adullact.iparapheur.repo.attest.AttestService;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;

/**
 * Created by lhameury on 21/09/15.
 *
 */
public class AttestServiceScriptable extends BaseProcessorExtension {

    @Autowired
    private AttestService attestService;

    public HashMap<String, String> getParameters() {
        return (HashMap<String, String>) attestService.getProperties();
    }

    public void setParameters(HashMap<String, String> params) {
        attestService.updateConfiguration(Boolean.valueOf(params.get("enabled")), params.get("host"), Integer.parseInt(params.get("port")), params.get("username"), params.get("password"));
    }
}
