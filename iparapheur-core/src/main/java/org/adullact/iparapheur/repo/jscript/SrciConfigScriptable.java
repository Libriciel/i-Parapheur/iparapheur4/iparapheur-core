/*
 * Version 3.3
 * CeCILL Copyright (c) 2011, Berger Levrault
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package org.adullact.iparapheur.repo.jscript;

import fr.bl.gbox.bean.BBean;
import fr.bl.gbox.bean.io.JsonReader;
import fr.bl.iparapheur.srci.TenantSrciProperties;
import fr.bl.tdtproxysrci.TPS;
import fr.bl.iparapheur.srci.SrciConfig;
import fr.bl.gbox.rcc.RccMessage;
import fr.bl.gbox.rcc.camel.Sender;
import fr.bl.iparapheur.web.BlexContext;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.junit.Assert;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.BaseElement;

/**
 * Configuration SRCI
 *
 * Expose sous le nom "srciConfig" dans les .js d'edition de configuration
 *
 * @author BLEX
 * @since 2011-05-17
 */
public class SrciConfigScriptable extends BaseProcessorExtension {

    private static final Logger log = Logger.getLogger(SrciConfigScriptable.class);
    private NodeService nodeService;

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }
    private NamespaceService namespaceService;

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }
    private SearchService searchService;

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }
    private ContentService contentService;

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public static interface K {

        public static interface config {

        }
    }

    public Map<String, String> getActesParameters() {
        return getParameters(TenantSrciProperties.K.xpath_actes);
    }

    public void setActesParameters(Map<String, String> params) {
        setParameters(TenantSrciProperties.K.xpath_actes, params);
    }

    /**
     * lecture des parametres HELIOS SRCI
     * - utilise par srci_helios.get.js
     * @return une map de proprietes
     */
    public Map<String, String> getHeliosParameters() {
        Map<String, String> map = getParameters(TenantSrciProperties.K.xpath_helios);
        return map;
    }

    public void setHeliosParameters(Map<String, String> params) {
        setParameters(TenantSrciProperties.K.xpath_helios, params);
    }

    /**
     * Lecture de parametres dans le fichier XML de configuration
     *
     * @param blexConfigPath
     * @return une map de proprietes
     */
    protected Map<String, String> getParameters(String configPath) {
        Document doc = readConfigDocument();

        List<Node> configNodes = doc.selectNodes(configPath);

        Assert.assertEquals("recuperation des parametres SRCI : la requete XPath ne retourne pas de resultats", 1, configNodes.size());
        Assert.assertTrue("recuperation des parametres SRCI : requete XPath invalide", configNodes.get(0) instanceof Element);

        Element configNode = (Element) configNodes.get(0);

        List<Element> configElements = configNode.elements();

        Map<String, String> configParams = new HashMap<String, String>();
        // valeur par defaut (pas dans le fichier de configuration initial)
        configParams.put("proxy_account_message", "");

        for (Element element : configElements) {
            configParams.put(element.getName(), element.getTextTrim());
        }

        return configParams;
    }

    /**
     * Ecriture de la configuration, apres editions utilisateur
     *
     * @param blexConfigPath
     * @param params
     */
    protected void setParameters(String configPath, Map<String, String> params) {
        Document doc = readConfigDocument();

        List<Node> configNodes = doc.selectNodes(configPath);

        Assert.assertEquals("ecriture des parametres SRCI : la requete XPath ne retourne pas de resultats", 1, configNodes.size());
        Assert.assertTrue("ecriture des parametres SRCI : requete XPath invalide", configNodes.get(0) instanceof Element);

        Element configNode = (Element) configNodes.get(0);

        // Remove all previous elements
        for (Element e : ((List<Element>) configNode.elements())) {
            configNode.remove(e);
        }

        for (Entry<String, String> param : params.entrySet()) {
            Element e = new BaseElement(param.getKey());
            e.setText(param.getValue());
            configNode.add(e);
        }

        {   // 
            // message relarif a la validite du compte
            //
            // connexion au proxy SRCI et verification de l'existence du compte
            // puis verification de la VALIDITE du compte
            //
            String proxy_account_message = "[" + new SimpleDateFormat("dd/MM/yy HH:mm").format(new Date()) + "] ";

            try {
                HttpClient httpclient = new HttpClient();

                SrciConfig scriConfig=BlexContext.getInstance().getSrciConfig();
                httpclient.getHostConfiguration().setHost(scriConfig.getServerUrlHost(), scriConfig.getServerUrlPort());

                //
                // Récupération du compte sur le proxy
                //
                RccMessage rccMessage=new RccMessage()
                        .setPayload(
                            TPS.GetAccount.name,
                            new BBean().set(
                                TPS.GetAccount.in.account, params.get(TenantSrciProperties.K.helios.proxy_account)
                            )
                        );

                GetMethod get = Sender.def().buildGetMethod(
                        rccMessage,
                        TPS.httpDirect.executeRelativeUri);

                httpclient.executeMethod(get);
                // {"account":{"targetURI":"xxx","srciCodeUtilisateur":"xxxx"}}
                // {"account":null}
                BBean acco=
                        JsonReader.def().readBean(get.getResponseBodyAsString())
                        .get(TPS.GetAccount.out.account, BBean.class);

                if (acco==null) {
                    throw new Exception("COMPTE NON DECLARE sur le proxy");
                }

                //
                // Tentative de connexion effective au serveur SRCI
                //
                {
                    rccMessage = new RccMessage().setPayload(
                            "checkConnection",
                            new BBean().set(
                            TPS.GetAccount.in.account, params.get(TenantSrciProperties.K.helios.proxy_account)));
                    get = Sender.def().buildGetMethod(rccMessage, TPS.httpDirect.executeRelativeUri);
                    httpclient.executeMethod(get); 
                    BBean response=JsonReader.def().readBean(get.getResponseBodyAsString());
                    if (! response.isEmpty(RccMessage.K.rccExtraParams.error)) {
                        // {"rcc_error":{"message":"erreur retournee (...)", (...) }}
                        BBean error=response.get(RccMessage.K.rccExtraParams.error, BBean.class);
                        throw new Exception(
                                error.get(RccMessage.K.rccExtraParams.error_.message, String.class));
                    }
                }

                proxy_account_message +=
                        "compte valide : "
                        +"["+acco.get(TPS.GetAccount.out.account_description, String.class)+"]"
                        +", cible : ["+acco.get(TPS.GetAccount.out.account_targetURI, String.class)+"]"
                        +", code utilisateur SRCI : ["+acco.get(TPS.GetAccount.out.account_srciCodeUtilisateur, String.class)+"]"
                        
                        +" : Connexion SRCI confirmee."
                        ;


            } catch (Exception any) {
                log.error(any.getMessage(), any);
                proxy_account_message +=
                        "IMPOSSIBLE DE VALIDER CE COMPTE : " + any.toString();
            }

            {
                Element e = new BaseElement("proxy_account_message");
                e.setText(proxy_account_message);
                configNode.add(e);
            }
        }


        setConfigDocument(doc);
    }

    /**
     * Lecture du fichier de configuration SRCI du tenant, sous forme de document
     * XML
     * @return un document XML
     */
    protected Document readConfigDocument() {
        NodeRef configNodeRef = getConfigNode();

        ContentReader reader = contentService.getReader(configNodeRef, ContentModel.PROP_CONTENT);
        SAXReader saxReader = new SAXReader();
        try {
            Document doc = saxReader.read(reader.getContentInputStream());
            return doc;
        } catch (DocumentException ex) {
            log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    protected void setConfigDocument(Document doc) {
        NodeRef configNode = getConfigNode();
        ContentWriter writer = contentService.getWriter(configNode, ContentModel.PROP_CONTENT, true);
        writer.putContent(doc.asXML());
    }

    /**
     * Recuperation du noeud correspondant au fichier de configuration SRCI
     * du tenant
     *
     * @return un noeud
     *
     * @throws RuntimeException
     *         si le fichier de proprietes n'est pas retrouve
     */
    protected NodeRef getConfigNode() {
        List<NodeRef> nodes = searchService.selectNodes(
                nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                TenantSrciProperties.K.configNodePath,
                null,
                namespaceService,
                false);

        if (nodes.size() != 1) {
            throw new RuntimeException("impossible de retrouver le fichier de configuration du TDT SRCI");
        }

        NodeRef configNodeRef = nodes.get(0);

        return configNodeRef;
    }
}
