/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, Netheos, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by Netheos
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.repo.jscript;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.junit.Assert;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.BaseElement;

/**
 *
 * @author Stephane Vast
 */
public class TimestampConfigScriptable extends BaseProcessorExtension {

    private static final Logger log = Logger.getLogger(TimestampConfigScriptable.class);

    private NodeService nodeService;

    private NamespaceService namespaceService;

    private SearchService searchService;

    private ContentService contentService;

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public Map<String, String> getHorodatageParameters() {
        return getParameters("/horodate");
    }

    public void setHorodatageParameters(Map<String, String> params) {
        setParameters("/horodate", params);
    }

    protected Map<String, String> getParameters(String configPath) {
        Document doc = getConfigDocument();

        List<Node> configNodes = doc.selectNodes(configPath);

        Assert.assertEquals("XPath query returned no results", 1, configNodes.size());
        Assert.assertTrue("Invalid XPath query", configNodes.get(0) instanceof Element);

        Element configNode = (Element) configNodes.get(0);

        List<Element> configElements = configNode.elements();

        Map<String, String> configParams = new HashMap<String, String>();

        for (Element element : configElements) {
            configParams.put(element.getName(), element.getTextTrim());
        }

        return configParams;
    }

    protected void setParameters(String configPath, Map<String, String> params) {
        Document doc = getConfigDocument();

        List<Node> configNodes = doc.selectNodes(configPath);

        Assert.assertEquals("XPath query returned no results", 1, configNodes.size());
        Assert.assertTrue("Invalid XPath query", configNodes.get(0) instanceof Element);

        Element configNode = (Element) configNodes.get(0);

        // Remove all previous elements
        for (Element e : ((List<Element>) configNode.elements())) {
            configNode.remove(e);
        }

        for (Entry<String, String> param : params.entrySet()) {
            Element e = new BaseElement(param.getKey());
            e.setText(param.getValue());

            configNode.add(e);
        }

        setConfigDocument(doc);
    }

    protected Document getConfigDocument() {
        NodeRef configNodeRef = getConfigNode();

        ContentReader reader = contentService.getReader(configNodeRef, ContentModel.PROP_CONTENT);
        SAXReader saxReader = new SAXReader();
        try {
            Document doc = saxReader.read(reader.getContentInputStream());

            return doc;
        } catch (DocumentException ex) {
            log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    protected void setConfigDocument(Document doc) {
        NodeRef configNode = getConfigNode();

        ContentWriter writer = contentService.getWriter(configNode, ContentModel.PROP_CONTENT, true);

        writer.putContent(doc.asXML());
    }

    protected NodeRef getConfigNode() {
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                "/app:company_home/app:dictionary/ph:certificats/cm:timestamper_configuration.xml",
                null,
                namespaceService,
                false);

        if (nodes.size() != 1) {
            throw new RuntimeException("Can't find TimeStamp configuration file");
        }

        NodeRef configNodeRef = nodes.get(0);

        return configNodeRef;
    }

}
