/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Initial Developpment by AtolCD, maintained by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package org.adullact.iparapheur.repo.ged;

import com.atolcd.archiland.ws._1.ArchilandService;
import com.atolcd.archiland.ws._1.ArchilandServiceSoapPort;
import com.atolcd.archiland.wsmodel._1.ActeDescription;
import com.atolcd.archiland.wsmodel._1.Document;
import com.atolcd.archiland.wsmodel._1.TypeDocument;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.webservice.authentication.AuthenticationResult;
import org.alfresco.webservice.authentication.AuthenticationServiceLocator;
import org.alfresco.webservice.authentication.AuthenticationServiceSoapPort;
import org.apache.log4j.Logger;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import javax.faces.model.SelectItem;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ArchilandConnector implements InitializingBean {
    private static Logger logger = Logger.getLogger(ArchilandConnector.class);

    private XMLGregorianCalendar startTime;
    private XMLGregorianCalendar expiredTime;

    private String _ticket;

    private AuthenticationServiceSoapPort _svc;

    private boolean autoCloseSession;

    private ParapheurService parapheurService;
    private ContentService contentService;
    private NodeService nodeService;
    private SearchService searchService;
    private NamespaceService namespaceService;
    private List<String> classification;

    private Map<String, Object> archilandConfig;
    private String store;

    public ContentService getContentService() {
        return contentService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public NodeService getNodeService() {
        return nodeService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public ParapheurService getParapheurService() {
        return parapheurService;
    }

    public void setParapheurService(ParapheurService parapheurService) {
        this.parapheurService = parapheurService;
    }

    public SearchService getSearchService() {
        return searchService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }
    
    public boolean isAutoCloseSession() {
        return autoCloseSession;
    }

    public void setAutoCloseSession(boolean autoCloseSession) {
        this.autoCloseSession = autoCloseSession;
    }

    public String getHost() {
        return (String)getArchilandConfig().get("host");
    }

    public void setHost(String host) {
        getArchilandConfig().put("host", host);
    }
    
    public String getUser() {
        return (String)getArchilandConfig().get("user");
    }

    public void setPassword(String host) {
        getArchilandConfig().put("password", host);
    }

    public NamespaceService getNamespaceService() {
        return namespaceService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public ArchilandConnector() {
        startTime = null;
        expiredTime = null;
        archilandConfig = null;
        this.autoCloseSession = true;
        classification = null;
    }

    public ArchilandConnector(String host) {
        this();

    }

    
    private void readIntitules(List<org.dom4j.Node> nodes) {
        for (org.dom4j.Node node : nodes) {
            
            if (node.getName().equals("intitule")) {
                String code = ((Element)node).attributeValue("code");
                Element libelle_elt = ((Element)node).element("libelle");
                this.classification.add(code+" "+libelle_elt.getText());
                Element sous_intitules_elt = ((Element)node).element("sous-intitules");
                
                if (sous_intitules_elt != null) {
                    readIntitules(((Element)sous_intitules_elt).elements());
                }
            }
            
        }
    }
    
    public List getClassification() {
        classification = new ArrayList<String>();

        //Map<String, String> retVal = new HashMap<String, String>();
        String xpath = "app:company_home/app:dictionary/ph:certificats/cm:archiland_classification.xml";
        StoreRef spaces = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");
        NodeRef root = nodeService.getRootNode(spaces);

        List<NodeRef> results = searchService.selectNodes(root,
                xpath,
                null,
                namespaceService,
                false);

        NodeRef config = results.get(0);

        ContentReader tmpReader = this.getContentService().getReader(config, ContentModel.PROP_CONTENT);

        long sizeOfContent = tmpReader.getSize();
        byte[] source = new byte[(int) sizeOfContent];
        InputStream is = tmpReader.getContentInputStream();

        try {
            SAXReader reader = new SAXReader();
            org.dom4j.Document document = reader.read(is);
            List<org.dom4j.Node> nodes = document.getRootElement().elements();
            readIntitules(nodes);

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new AlfrescoRuntimeException(ex.getMessage(), ex);
        }



        return this.classification;
    }
    
    public XMLGregorianCalendar getStartTime() {
        if (this.startTime == null) {
            this.startTime = this.dateToXMLCalendar(new Date());
        }
        return this.startTime;
    }

    public XMLGregorianCalendar getExpiredTime() {
        if (this.expiredTime == null) {
            GregorianCalendar calendar = this.getStartTime().toGregorianCalendar();

            calendar.add(GregorianCalendar.MINUTE, 30);

            this.expiredTime = this.dateToXMLCalendar(calendar.getTime());
        }
        return this.expiredTime;
    }

    public XMLGregorianCalendar dateToXMLCalendar(Date d) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(d);
        XMLGregorianCalendar xmlGregorianCalendar = null;

        try {
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        } catch (DatatypeConfigurationException ex) {
            logger.error(ex.getLocalizedMessage());
        }

        return xmlGregorianCalendar;
    }


    
    public void forceConfigReload() {
        this.archilandConfig = null;
    }

    public Map<String, Object> getArchilandConfig() {
        if (this.archilandConfig == null) {
            archilandConfig = new HashMap<String, Object>();
            
            Map<String, String> retVal = new HashMap<String, String>();
            String xpath = "app:company_home/app:dictionary/ph:certificats/cm:archiland_configuration.xml";
            StoreRef spaces = new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore");
            NodeRef root = nodeService.getRootNode(spaces);

            List<NodeRef> results = searchService.selectNodes(root,
                    xpath,
                    null,
                    namespaceService,
                    false);

            NodeRef config = results.get(0);

            ContentReader tmpReader = this.getContentService().getReader(config, ContentModel.PROP_CONTENT);

            long sizeOfContent = tmpReader.getSize();
            byte[] source = new byte[(int) sizeOfContent];
            InputStream is = tmpReader.getContentInputStream();

            try {
                SAXReader reader = new SAXReader();
                org.dom4j.Document document = reader.read(is);
                List<org.dom4j.Node> nodes = document.getRootElement().elements();
                for (org.dom4j.Node node : nodes) {
                    if (!node.getName().equals("services")) {
                        archilandConfig.put(node.getName(), node.getText());
                    }
                    else {
                        List<org.dom4j.Node> serviceNodes = ((Element)node).elements();
                        List<SelectItem> services = new ArrayList<SelectItem>();
                        for (org.dom4j.Node serviceNode : serviceNodes) {
                            services.add(new SelectItem(((Element)serviceNode).attribute("id").getText(), serviceNode.getText()));
                            
                        }
                        archilandConfig.put("services", services);
                    }
                }

            } catch (Exception ex) {
                //logger.error(ex.getMessage(), ex);
                logger.error(ex.getLocalizedMessage());
                throw new AlfrescoRuntimeException(ex.getMessage(), ex);
            }
        }



        return this.archilandConfig;
    }

    protected AuthenticationServiceSoapPort getAuthenticationServiceStub() throws ServiceException {
        URL alfrescoEndPoint = null;
            try {
                alfrescoEndPoint = new URL(String.format("http://%s:%s/alfresco/api/AuthenticationService?wsdl", getArchilandConfig().get("host"), getArchilandConfig().get("port")));
            } catch (MalformedURLException e) {
                logger.error(e.getLocalizedMessage());
            }

            return (AuthenticationServiceSoapPort) new AuthenticationServiceLocator().getAuthenticationService(alfrescoEndPoint);
    }


    /* alfresco */
    protected String getTicket() throws RemoteException, ServiceException {
        if (this._ticket == null) {
            AuthenticationResult result = getAuthenticationServiceStub().startSession( (String)getArchilandConfig().get("user"), (String)getArchilandConfig().get("password"));
            System.out.println("Auth = "+result+" "+ result.getUsername()+" "+result.getSessionid());

            System.out.println(result.getTicket());
            this._ticket = result.getTicket();

            this.startTime = null;
            this.expiredTime = null;
            this.getStartTime();
            this.getExpiredTime();
            
        }

        return this._ticket;
    }

    protected void releaseTicket() throws RemoteException, ServiceException {
        getAuthenticationServiceStub().endSession(this.getTicket());
        this._ticket = null;
    }

    protected String getMessageForReturn(int response) {

        List<String> messages = new ArrayList<String>();

        messages.add("");
        messages.add("Collectivité ou service inconnu.");
        messages.add("Type d'acte non-géré.");
        messages.add("Aucun conteneur adapté.");
        messages.add("Paramètre manquant ou erroné.");

        return messages.get(response);
    }

    public TypeDocument getTypeDocumentFromS2LowNature(String nature) {
        TypeDocument retVal = null;
        Map<String, TypeDocument> natureMapping = new HashMap<String, TypeDocument>();

        natureMapping.put("3",TypeDocument.ARRETE_INDIVIDUEL);
        natureMapping.put("2",TypeDocument.ARRETE_REGLEMENTAIRE);
        natureMapping.put("6",TypeDocument.AUTRE_ACTE);
        natureMapping.put("4",TypeDocument.CONTRAT_CONVENTION);
        natureMapping.put("1",TypeDocument.DELIBERATION);
        /*
        <actes:NatureActe actes:CodeNatureActe="1" actes:Libelle="Délibérations" actes:TypeAbrege="DE"/>
		<actes:NatureActe actes:CodeNatureActe="2" actes:Libelle="Arrêtés réglementaires" actes:TypeAbrege="AR"/>
		<actes:NatureActe actes:CodeNatureActe="3" actes:Libelle="Arrêtés Individuels" actes:TypeAbrege="AI"/>
		<actes:NatureActe actes:CodeNatureActe="4" actes:Libelle="Contrats et conventions" actes:TypeAbrege="CC"/>
		<actes:NatureActe actes:CodeNatureActe="5" actes:Libelle="Documents budgétaires et financiers" actes:TypeAbrege="BF"/>
		<actes:NatureActe actes:CodeNatureActe="6" actes:Libelle="Autres" actes:TypeAbrege="AU"/>
         *
         */
        return natureMapping.get(nature);

    }

    public class ActesMetaData {
        public String nature;
        public Date date;
        public String objet;
        public String classification;
    }

    public ActesMetaData getActeDataForNodeRef(NodeRef nodeRef) {
        ActesMetaData data = new ActesMetaData();

        data.nature = (String)nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_ACTES_NATURE);

        data.date = null;

        String dateStr = (String)nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_ACTES_DATE);

        if (dateStr != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                data.date =formatter.parse(dateStr);
            }
            catch (ParseException e) {
                logger.error(e.getLocalizedMessage());
            }
        }
        data.classification = (String)nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_ACTES_CLASSIFICATION);
        data.objet = (String)nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_ACTES_OBJET);


        /*
        System.out.println(nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_ACTES_CLASSIFICATION));
        System.out.println((String)nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_ACTES_NATURE));*/

        System.out.println(nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_ACTES_DATE));
        /*
        System.out.println(nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_ACTES_OBJET));
         */
        return data;
    }

    /* */

    public void verserActe(ActeDescription acte) {

        try {
            String ticket = getTicket();

            System.out.println("got ticket = " + ticket);
            URL endpoint = new URL(String.format("http://%s:%s/alfresco/wsdl/archiland-service.wsdl", getArchilandConfig().get("host"), getArchilandConfig().get("port")));

            ArchilandServiceSoapPort service = new ArchilandService(endpoint, new QName("http://www.atolcd.com/archiland/ws/1.0", "ArchilandService")).getArchilandService();

            /* Override endpoint Address */
            Map<String, Object> ctxt = ((BindingProvider)service ).getRequestContext();
            ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, String.format("http://%s:%s/alfresco/api/ArchilandService", getArchilandConfig().get("host"), getArchilandConfig().get("port")));

            /* Adding the ArchilandSecurtityHandler to the handler chain.
             * This security handler adds security headers to the SOAP enveloppe.
             */
            Binding binding = ((BindingProvider) service).getBinding();

            List<Handler> handlerList = binding.getHandlerChain();

            if (handlerList == null) {
                handlerList = new ArrayList<Handler>();
            }

            handlerList.add(new ArchilandSecurityHandler( (String)getArchilandConfig().get("user"), ticket, getStartTime().toString(), getExpiredTime().toString()));
            binding.setHandlerChain(handlerList);

            int response;
            response = service.createActe(acte);

            System.out.println("response = " + response);

            if (response > 0) {
                throw new RuntimeException(getMessageForReturn(response));
            }

            


            /* closing Session */
            if (isAutoCloseSession()) {
                releaseTicket();
            }

        } catch (RemoteException e) {
            logger.error(e.getLocalizedMessage());
            throw new RuntimeException("Impossible de contacter le serveur Archiland, veuillez contacter l'administrateur.");
        } catch (MalformedURLException e) {
            logger.error(e.getLocalizedMessage());
            throw new RuntimeException("Impossible de contacter le serveur Archiland car l'adresse est eronnée, veuillez contacter l'administrateur.");
        } catch (ServiceException e) {
            logger.error(e.getLocalizedMessage());
        } catch (SOAPFaultException sf) {
            SOAPFault f = sf.getFault();
            // FIXME: à gérer au niveau du protocole
            if (f.getFaultString().startsWith("org.alfresco.service.cmr.repository.DuplicateChildNodeNameException")) {
                throw new RuntimeException("Ce nom de dossier est déja utilisé");
            }

            throw sf;
        }


    }

     private TypeDocument typeToAnnexeType(TypeDocument docType) {
        TypeDocument annexeDocType = null;

        String tmp = docType.value();
        Map<String, TypeDocument> translate = new HashMap<String, TypeDocument>();

        translate.put(TypeDocument.ARRETE_INDIVIDUEL.value(),
                TypeDocument.ANNEXE_ARRETE_INDIVIDUEL);

        translate.put(TypeDocument.ARRETE_REGLEMENTAIRE.value(),
                TypeDocument.ANNEXE_ARRETE_REGLEMENTAIRE);

        translate.put(TypeDocument.AUTRE_ACTE.value(),
                TypeDocument.ANNEXE_AUTRE_ACTE);

        translate.put(TypeDocument.CONTRAT_CONVENTION.value(),
                TypeDocument.ANNEXE_CONTRAT_CONVENTION);

        translate.put(TypeDocument.DELIBERATION.value(),
                TypeDocument.ANNEXE_DELIBERATION);

        translate.put(TypeDocument.DOCUMENT_BUDGETAIRE.value(),
                TypeDocument.ANNEXE_DOCUMENT_BUDGETAIRE);

        annexeDocType = translate.get(tmp);

        return annexeDocType;
    }

    private TypeDocument getSignTypeForDocType(TypeDocument docType) {
        TypeDocument annexeDocType = null;

        String tmp = docType.value();
        Map<String, TypeDocument> translate = new HashMap<String, TypeDocument>();

        translate.put(TypeDocument.ARRETE_INDIVIDUEL.value(),
                TypeDocument.SIGNATURE_ARRETE_INDIVIDUEL);

        translate.put(TypeDocument.ARRETE_REGLEMENTAIRE.value(),
                TypeDocument.SIGNATURE_ARRETE_REGLEMENTAIRE);

        translate.put(TypeDocument.DELIBERATION.value(),
                TypeDocument.SIGNATURE_DELIBERATION);


        annexeDocType = translate.get(tmp);

        return annexeDocType;
    }


    private byte[] getNodeContent(NodeRef nr) {
        ContentReader tmpReader = null;
        tmpReader = this.getContentService().getReader(nr, ContentModel.PROP_CONTENT);

        long sizeOfContent = tmpReader.getSize();
        byte[] source = new byte[(int) sizeOfContent];
        InputStream is = tmpReader.getContentInputStream();

        try {
            is.read(source);

        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
        }
        return source;
    }

    private Document buildDocumentForFile(NodeRef file, TypeDocument typeDoc) {
        String contentName = (String) getNodeService().getProperty(file, ContentModel.PROP_NAME);
        byte[] source = getNodeContent(file);

        Document doc = new Document();

        doc.setType(typeDoc);
        doc.setFichier(source);
        doc.setNom(contentName);

        return doc;
    }

    private byte[] getSignaturesAsZipFile(NodeRef dossierRef) {
        ZipOutputStream zout = null;
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        boolean hasSignatures = false;
        try {

            List<EtapeCircuit> etapes = this.parapheurService.getCircuit(dossierRef);

            zout = new ZipOutputStream(bs);
            int ordre = 0;
            for (EtapeCircuit etape : etapes) {
                if (etape.getActionDemandee().trim().equalsIgnoreCase(EtapeCircuit.ETAPE_SIGNATURE)) {
                    byte[] signatureEtape = this.parapheurService.getSignature(etape);
                    if (signatureEtape != null && signatureEtape.length > 0) {
                        //  logger.debug("ARCHIVER - Sauvegarde de la signature Etape " + etapes.indexOf(etape));
                        String extensionSignature = null;
                        String sigFormat = getNodeService().getProperty(dossierRef, ParapheurModel.PROP_SIGNATURE_FORMAT).toString();
                        if (sigFormat.startsWith("XAdES")) {
                          //  sigWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
                            extensionSignature = ".xml";
                        } else if (sigFormat.startsWith("PKCS#7")) {
                           // sigWriter.setMimetype("application/pkcs7-signature");
                            extensionSignature = ".p7s";
                        } else {
                            throw new UnsupportedOperationException("Unknown signature format: " + sigFormat);
                        }
                        ordre++;
                        hasSignatures = true;
                        zout.putNextEntry(new ZipEntry(ordre + "- " + etape.getSignataire() + extensionSignature));
                        zout.write(signatureEtape, 0, signatureEtape.length);
                        zout.closeEntry();
                    }
                }
            }

        } catch (FileNotFoundException ex) {
            logger.error(ex.getLocalizedMessage());
        }
        catch (IOException e) {

        } finally {
            try {
                zout.close();
            } catch (IOException ex) {
                logger.error(ex.getLocalizedMessage());
            }
        }

        if (hasSignatures) {
            return bs.toByteArray();
        }
        else {
            return null;
        }

    }

    /**
     * @deprecated 
     */
    private Document buildBordereauxDocument(NodeRef nodeRef, TypeDocument typeDoc) {
        String contentName = (String) getNodeService().getProperty(nodeRef, ContentModel.PROP_NAME);
        Document retVal = null;

        byte[] bytes = null;

        try {
            java.io.File pdfContent = this.parapheurService.genererDossierPDF(nodeRef, true, true, true);
            FileInputStream fis = new FileInputStream(pdfContent);
            bytes = new byte[(int)pdfContent.length()];
            fis.read(bytes);
        }
        catch (FileNotFoundException e) {

        }
        catch (IOException e) {

        }
        catch (Exception e) {

        }
        if (bytes != null) {
            retVal = new Document();
            retVal.setFichier(bytes);
            retVal.setNom(contentName+"_archive.pdf");
            retVal.setType(typeDoc);
            retVal.setFichier(bytes);
        }
        return retVal;
    }

    /**
     * builds a list of Document from the dossier NodeRef
     * @param nodeRef noderef from foder
     * @param typeActe the TypeActe
     * @return a list of document for soap use.
     */
    public List<Document> getDocumentsForNode(NodeRef nodeRef, String typeActe) {
        List<Document> retVal = new ArrayList<Document>();

        List<NodeRef> files = this.parapheurService.getDocuments(nodeRef);

        TypeDocument typeDoc = TypeDocument.fromValue(typeActe);
        TypeDocument typeSign = null;

        if (typeDoc == TypeDocument.DELIBERATION
                || typeDoc == TypeDocument.ARRETE_INDIVIDUEL
                || typeDoc == TypeDocument.ARRETE_REGLEMENTAIRE) {

            typeSign = getSignTypeForDocType(typeDoc);
        }

        boolean isDocPrincipal = true;

        for (NodeRef file : files) {
            Document doc = buildDocumentForFile(file, typeDoc);

            retVal.add(doc);

            if (isDocPrincipal) {
                typeDoc = typeToAnnexeType(typeDoc);
                isDocPrincipal = false;
            }
        }
        /*
        Document bordereaux = buildBordereauxDocument(nodeRef, typeDoc);

        if (bordereaux != null) {
            retVal.add(bordereaux);
        }
        else {
            throw new RuntimeException("Impossible de générer l'archive PDF.");
        }
*/
        if (typeSign != null) {
            byte[] signature = getSignaturesAsZipFile(nodeRef);

            if (signature != null) {
                String docPrincipalName = retVal.get(0).getNom();

                Document sigDoc = new Document();
                sigDoc.setFichier(signature);
                sigDoc.setType(typeSign);
                sigDoc.setNom(docPrincipalName + ".zip");

                retVal.add(sigDoc);
            }
        }

        return retVal;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(nodeService, "There must be a node service");
        Assert.notNull(searchService, "There must be a search service");
        Assert.notNull(contentService, "There must be a content service");
        Assert.notNull(namespaceService, "There must be a namespace service");
        Assert.notNull(parapheurService, "There must be a parapheur service");
    }
}
