package org.adullact.iparapheur.repo.notification.socket;

/**
 * Classe récupérée lors d'un évenement via SocketIO
 * Created by lhameury on 06/01/14.
 */

public class SocketUserEvent {

    private String userName;

    public SocketUserEvent() {
    }

    public SocketUserEvent(String userName) {
        super();
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

}