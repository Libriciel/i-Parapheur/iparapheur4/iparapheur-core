package org.adullact.iparapheur.repo.jscript.pastell.mailsec.redis;

import org.adullact.iparapheur.repo.jscript.pastell.mailsec.MailsecPastellService;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.extensions.surf.util.AbstractLifecycleBean;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

/**
 * Créé par lhameury le 09/05/18.
 */
public class RedisService extends AbstractLifecycleBean {

    private static Logger logger = Logger.getLogger(RedisService.class);
    private final String subsystem = "i-Parapheur --- Redis";

    private Thread task;

    private String host;
    private int port;
    private String channel;

    @Autowired
    private MailsecPastellService mailsecPastellService;

    @Override
    protected void onBootstrap(ApplicationEvent applicationEvent) {
        logger.setLevel(Level.INFO);
        logger.info("Starting '" + subsystem + "' subsystem");

        final Jedis jedis = new Jedis(host, port);

        //Initialisation du taskExecutor avec les paramètres passés en fichier de configuration
        if (task == null) {

            task = new Thread(() -> {
                System.out.println("Subscribing to Redis channel - " + channel);
                jedis.subscribe(new JedisPubSub() {
                    @Override
                    public void onMessage(String channel, String message) {
                        try {
                            mailsecPastellService.updateTransaction(message);
                        } catch(Throwable r) {
                            r.printStackTrace();
                        }
                    }
                }, channel);
            });

            task.start();
        }
        logger.info("Startup of '" + subsystem + "' subsystem complete");
    }

    @Override
    protected void onShutdown(ApplicationEvent applicationEvent) {
        task.interrupt();
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
