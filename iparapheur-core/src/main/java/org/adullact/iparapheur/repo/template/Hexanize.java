/*
 * Version 3.2.2
 * CeCILL Copyright (c) 2011, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package org.adullact.iparapheur.repo.template;

import freemarker.template.*;
import java.math.BigInteger;
import java.util.List;
import org.alfresco.repo.template.BaseTemplateProcessorExtension;

/**
 * Origine du code: Mark van de Veerdonk.
 *   http://osdir.com/ml/web.freemarker.devel/2004-04/msg00010.html
 *   Just an example of what it does: ${31?hex(4)} results in the string "001F".
 * 
 * Extension au langage FreeMarker pour formatter des nombres en chaine HEXA.
 * Utile pour afficher proprement des "SERIAL NUMBER" de certificat. :-)
 * <p>
 * Methode: un nombre (en string), resolu en equivant hexa (en string)
 * <p>
 * Usages: hexanize(String nombre) ou hexanize(String nombre, int longueurMini)
 * 
 * 
 * @author Stephane Vast - ADULLACT Projet
 */
public class Hexanize extends BaseTemplateProcessorExtension implements TemplateMethodModelEx {

    /**
     * @see freemarker.template.TemplateMethodModel#exec(java.util.List)
     */
    public Object exec(List args) throws TemplateModelException {
        String result = "";

        if (args.isEmpty()) {
            throw new TemplateModelException("hexanize(...) requires at least 1 argument");
        }
        if (args.size() == 1) {
            Object arg0 = args.get(0);
            if (arg0 instanceof TemplateScalarModel) {
                String source = ((TemplateScalarModel)arg0).getAsString();
                BigInteger number = new BigInteger(source);
                result = number.toString(16).toUpperCase();
            } else {
                throw new TemplateModelException("Parameter to hexanize(...) must be a string.");
            }
        }
        if (args.size() == 2) {
            Object arg0 = args.get(0);
            Object arg1 = args.get(1);

            if (!(arg0 instanceof TemplateScalarModel)) {
                throw new TemplateModelException("Parameter #1 to hexanize(...) must be a string.");
            }
            else if (!(arg1 instanceof TemplateNumberModel)) {
                throw new TemplateModelException("Parameter #1 to hexanize(...) must be a number.");
            }
            else {
                String source = ((TemplateScalarModel)arg0).getAsString();
                BigInteger big = new BigInteger(source);
                String strValue = big.toString(16).toUpperCase();
                int minLength = ((TemplateNumberModel) arg1).getAsNumber().intValue();
                int numLeadingZeros = minLength - strValue.length();
                String prependStr = "";
                while (numLeadingZeros > 0) {
                    prependStr += "0";
                    numLeadingZeros--;
                }
                result = prependStr + strValue;
            }
        }
        return new SimpleScalar(result);
    }
}

