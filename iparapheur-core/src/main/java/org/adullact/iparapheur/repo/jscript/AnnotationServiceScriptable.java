package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.repo.AnnotationService;
import com.atolcd.parapheur.repo.annotations.Annotation;
import com.atolcd.parapheur.repo.annotations.Rect;
import org.alfresco.repo.jscript.BaseScopableProcessorExtension;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONStringer;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PersonService;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 21/08/12
 * Time: 15:46
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("unused")
public class AnnotationServiceScriptable extends BaseScopableProcessorExtension {

    public static final String ANNOTATION_JSON_KEY_ID = "id";
    public static final String ANNOTATION_JSON_KEY_SECRETAIRE = "secretaire";
    public static final String ANNOTATION_JSON_KEY_TYPE = "type";
    public static final String ANNOTATION_JSON_KEY_DATE = "date";
    public static final String ANNOTATION_JSON_KEY_AUTHOR = "author";
    public static final String ANNOTATION_JSON_KEY_PEN_COLOR = "penColor";
    public static final String ANNOTATION_JSON_KEY_TEXT = "text";
    public static final String ANNOTATION_JSON_KEY_FILL_COLOR = "fillColor";
    public static final String ANNOTATION_JSON_KEY_FILL_RECT = "rect";
    public static final String ANNOTATION_JSON_KEY_FILL_RECT_TOP_LEFT = "topLeft";
    public static final String ANNOTATION_JSON_KEY_FILL_RECT_BOTTOM_RIGHT = "bottomRight";
    public static final String ANNOTATION_JSON_KEY_FILL_RECT_X = "x";
    public static final String ANNOTATION_JSON_KEY_FILL_RECT_Y = "y";


    private static Log logger = LogFactory.getLog(AnnotationServiceScriptable.class);

    @Autowired
    AnnotationService annotationService;
    
    @Autowired
    PersonService personService;
    
    @Autowired
    NodeService nodeService;

    public UUID addAnnotation(NodeRef dossier, NodeRef document, Annotation a) {
        return annotationService.addAnnotation(dossier, document, a);
    }

    public void removeAnnotation(String dossierRef, String documentRef, Object uuids) {
        if (dossierRef != null) {
            annotationService.removeAnnotation(new NodeRef(dossierRef), new NodeRef(documentRef), UUID.fromString((String) uuids));
        }
    }

    public void updateAnnotation(String dossierRef, String documentRef, Annotation a) {
        if ((dossierRef != null) && (documentRef != null)) {
            annotationService.updateAnnotation(new NodeRef(dossierRef), new NodeRef(documentRef), a);
        }
    }

    public HashMap<Integer, ArrayList<Annotation>> getCurrentAnnotations(NodeRef dossier) {
        return null;
    }

    public @NotNull String handleDocumentAnnotations(@Nullable String dossierRef) {

        List<HashMap<String, HashMap<Integer, ArrayList<Annotation>>>> data = new ArrayList<HashMap<String, HashMap<Integer, ArrayList<Annotation>>>>();

        if ((dossierRef != null)) {
            data = annotationService.getAnnotationsByEtape(new NodeRef(dossierRef));
        }

        // Building response String

        JSONStringer stringer = new JSONStringer();
        try {
            stringer.array();
            for (HashMap<String, HashMap<Integer, ArrayList<Annotation>>> dataEtape : data) {

                stringer.object();
                for (Map.Entry<String, HashMap<Integer, ArrayList<Annotation>>> dataEtapeDocument : dataEtape.entrySet()) {
                    stringer.key(dataEtapeDocument.getKey());

                    stringer.object();
                    for (Map.Entry<Integer, ArrayList<Annotation>> dataEtapeDocumentPages : dataEtapeDocument.getValue().entrySet()) {
                        stringer.key(dataEtapeDocumentPages.getKey().toString());

                        stringer.array();
                        for (Annotation dataEtapeDocumentPageAnnotation : dataEtapeDocumentPages.getValue()) {
                            stringifyAnnotation(stringer, dataEtapeDocumentPageAnnotation);
                        }
                        stringer.endArray();
                    }
                    stringer.endObject();
                }
                stringer.endObject();
            }
            stringer.endArray();
        }
        catch (JSONException jsonException) {
            logger.error("Error building annotation response", jsonException);
        }

        // Response

        return stringer.toString();
    }

    public @NotNull String handleDocumentAnnotations(@Nullable String dossierRef, @Nullable String documentRef) {

        List<HashMap<Integer, ArrayList<Annotation>>> data = new ArrayList<HashMap<Integer, ArrayList<Annotation>>>();

        if ((dossierRef != null) && (documentRef != null)) {
            data = annotationService.getDocumentAnnotationsByEtape(new NodeRef(dossierRef), new NodeRef(documentRef));
        }

        // Building response String

        JSONStringer stringer = new JSONStringer();
        try {
            stringer.array();
            for (HashMap<Integer, ArrayList<Annotation>> dataEtape : data) {

                stringer.object();
                for (Map.Entry<Integer, ArrayList<Annotation>> dataEtapePage : dataEtape.entrySet()) {
                    stringer.key(dataEtapePage.getKey().toString());

                    stringer.array();
                    for (Annotation dataEtapePageAnnotation : dataEtapePage.getValue()) {
                        stringifyAnnotation(stringer, dataEtapePageAnnotation);
                    }
                    stringer.endArray();
                }
                stringer.endObject();
            }
            stringer.endArray();
        }
        catch (JSONException jsonException) {
            logger.error("Error building annotation response", jsonException);
        }

        // Response

        return stringer.toString();
    }

    public Annotation getAnnotationForUUID(NodeRef nodeRef, String uuid) {
       return annotationService.getAnnotationForUUID(nodeRef, UUID.fromString(uuid));
    }

    public Annotation getNewTextAnnotationWith(Rect r, String picto, String text, Integer page) {
        NodeRef n = personService.getPerson(AuthenticationUtil.getRunAsUser());
        String fullname = "" + nodeService.getProperty(n, ContentModel.PROP_FIRSTNAME) + " " + nodeService.getProperty(n, ContentModel.PROP_LASTNAME);
        //Récupérer fullname
        return annotationService.getNewTextAnnotationWith(r, picto, text, page, fullname);
    }

    public Annotation getNewRectAnnotationWith(Rect r, String penColor, String fillColor, String text, Integer page) {
        NodeRef n = personService.getPerson(AuthenticationUtil.getRunAsUser());
        String fullname = "" + nodeService.getProperty(n, ContentModel.PROP_FIRSTNAME) + " " + nodeService.getProperty(n, ContentModel.PROP_LASTNAME);
        //Récupérer fullname
        return annotationService.getNewRectAnnotationWith(r, penColor, fillColor, text, page, fullname);
    }

    public Annotation getNewHighlightAnnotationWith(Rect r, String penColor, String fillColor, String text, Integer page) {
        NodeRef n = personService.getPerson(AuthenticationUtil.getRunAsUser());
        String fullname = "" + nodeService.getProperty(n, ContentModel.PROP_FIRSTNAME) + " " + nodeService.getProperty(n, ContentModel.PROP_LASTNAME);
        //Récupérer fullname
        return annotationService.getNewHighlightAnnotationWith(r, penColor, fillColor, text, page, fullname);
    }

    private static void stringifyAnnotation(@NotNull JSONStringer stringer, @NotNull Annotation annotation) throws JSONException {
        stringer.object();
        {
            stringer.key(ANNOTATION_JSON_KEY_ID).value(annotation.getUUID().toString());
            stringer.key(ANNOTATION_JSON_KEY_SECRETAIRE).value(annotation.isSecretaire());
            stringer.key(ANNOTATION_JSON_KEY_TYPE).value(annotation.getType());
            stringer.key(ANNOTATION_JSON_KEY_DATE).value(getISO8601StringForDate(annotation.getDate()));
            stringer.key(ANNOTATION_JSON_KEY_AUTHOR).value(annotation.getAuthor());
            stringer.key(ANNOTATION_JSON_KEY_PEN_COLOR).value(annotation.getPenColor());
            stringer.key(ANNOTATION_JSON_KEY_TEXT).value(annotation.getText());
            stringer.key(ANNOTATION_JSON_KEY_FILL_COLOR).value(annotation.getFillColor());

            stringer.key(ANNOTATION_JSON_KEY_FILL_RECT);
            stringer.object();
            {
                stringer.key(ANNOTATION_JSON_KEY_FILL_RECT_TOP_LEFT);
                stringer.object();
                stringer.key(ANNOTATION_JSON_KEY_FILL_RECT_X).value(annotation.getRect().getTopLeftCorner().getX());
                stringer.key(ANNOTATION_JSON_KEY_FILL_RECT_Y).value(annotation.getRect().getTopLeftCorner().getY());
                stringer.endObject();

                stringer.key(ANNOTATION_JSON_KEY_FILL_RECT_BOTTOM_RIGHT);
                stringer.object();
                stringer.key(ANNOTATION_JSON_KEY_FILL_RECT_X).value(annotation.getRect().getBottomRightCorner().getX());
                stringer.key(ANNOTATION_JSON_KEY_FILL_RECT_Y).value(annotation.getRect().getBottomRightCorner().getY());
                stringer.endObject();
            }
            stringer.endObject();
        }
        stringer.endObject();
    }

    private static String getISO8601StringForDate(Date date) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(date);
    }

    // <editor-fold desc="Deprecated methods">

    @Deprecated
    public UUID addAnnotation(NodeRef dossier, Annotation a) {
        return annotationService.addAnnotation(dossier, a);
    }

    @Deprecated
    public void updateAnnotation(String nodeRef, Annotation a) {
        annotationService.updateAnnotation(new NodeRef(nodeRef), a);
    }

    @Deprecated
    public void removeAnnotation(NodeRef nodeRef, Object uuids) {
        if(uuids instanceof String) {
            annotationService.removeAnnotation(nodeRef, UUID.fromString((String)uuids));
        } else if(uuids instanceof List) {
            for (String uuid : (List<String>)uuids) {
                annotationService.removeAnnotation(nodeRef, UUID.fromString(uuid));
            }
        }
    }

    @Deprecated
    public HashMap<Integer, ArrayList<Annotation>> getAllAnnotations(NodeRef dossier) {
        return null;
    }

    // </editor-fold desc="Deprecated methods">
}
