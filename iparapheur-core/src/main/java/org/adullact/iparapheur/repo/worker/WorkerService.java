package org.adullact.iparapheur.repo.worker;

import org.alfresco.service.cmr.repository.NodeRef;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jmaire
 * Date: 15/05/2014
 * Time: 14:44
 */
public interface WorkerService {

    String ID = "id";
    String DESTINATAIRES = "destinataires";
    String DESTINATAIRESCC = "destinatairesCC";
    String DESTINATAIRESCCI = "destinatairesCCI";
    String MESSAGE = "message";
    String PASSWORD = "password";
    String SHOWPASS = "showpass";
    String INCLUDEDANNEXES = "includedAnnexes";
    String INCLUDEFIRSTPAGE = "includeFirstPage";
    String ANNOTPRIV = "annotPriv";
    String NOTIFME = "notifMe";
    String BUREAUCIBLE = "bureauCible";
    String ANNOTPUB = "annotPub";
    String CONSECUTIVE = "consecutiveSteps";
    String TITREARCHIVE = "name";
    String ANNEXES = "annexes";
    String ANNEXESINCLUDED = "annexesIncluded";
    String ATTACHMENTS = "attachments";
    String DATEACTES = "dateActe";
    String OBJET = "objet";
    String NATURE = "nature";
    String CLASSIFICATION = "classification";
    String NUMERO = "numero";
    String SIGNATURE = "signature";
    String CERTIFICATE = "certificate";
    String SIGNATUREDATETIME = "signatureDateTime";
    String BUREAUCOURANT = "bureauCourant";
    String TYPE = "type";
    String TYPE_DOSSIER = "dossier";
    String TYPE_DOCUMENT = "document";
    String TYPE_EVENT = "event";
    String TYPE_MAIL = "mail";
    String WORKSPACE = "workspace://SpacesStore/";
    String USERNAME = "username";
    String NEXTACTION = "nextAction";
    String ACTION = "action";
    String DOSSIER_CIBLE = "dossierCible";
    String AUTOMATIC = "auto";
    String EVENT = "event";
    String PARENT = "parent";
    String OLD_PARENT = "old_parent";

    /**
     * Returns a runnable which can handle the request passed in parameter.
     * @param request a map defining the action to execute
     * @return a runnable which can handle the request
     */
    Runnable getHandler(Map request);

    void getAttestByWorker(NodeRef node);
}
