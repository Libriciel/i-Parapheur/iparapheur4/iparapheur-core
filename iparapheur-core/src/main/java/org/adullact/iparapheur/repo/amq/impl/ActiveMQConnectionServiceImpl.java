package org.adullact.iparapheur.repo.amq.impl;

import org.adullact.iparapheur.repo.amq.ActiveMQConnectionService;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.store.kahadaptor.KahaPersistenceAdapter;
import org.apache.activemq.usage.SystemUsage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEvent;
import org.springframework.extensions.surf.util.AbstractLifecycleBean;

import javax.jms.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lhameury on 14/01/14.
 */

public class ActiveMQConnectionServiceImpl extends AbstractLifecycleBean implements ActiveMQConnectionService {

    @Autowired
    @Qualifier("tenantAdminService")
    private TenantAdminService tenantAdminService;

    @Autowired
    private MessagesReceiver messagesReceiver;

    //Connection aux queues
    private QueueConnection connection;

    private Map<String, QueueSession> sessions = new HashMap<String, QueueSession>();
    private Map<String, QueueSender> senders = new HashMap<String, QueueSender>();

    private String amqDir;
    private Long storeLimit;
    private String brokerConnector;
    private String consumerConnector;
    private Boolean internalBroker;

    private final String subsystem = "i-Parapheur --- AMQ";

    private static Logger logger = Logger.getLogger(ActiveMQConnectionService.class);

    BrokerService broker;

    @Override
    public void setAmqDir(String amqDir) {
        this.amqDir = amqDir;
    }

    @Override
    public void setInternalBroker(String internalBroker) {
        this.internalBroker = Boolean.valueOf(internalBroker);
    }

    @Override
    public void setStoreLimit(String storeLimit) {
        this.storeLimit = Long.valueOf(storeLimit);
    }

    @Override
    public void setBrokerConnector(String brokerConnector) {
        this.brokerConnector = brokerConnector;
    }

    @Override
    public void setConsumerConnector(String consumerConnector) {
        this.consumerConnector = consumerConnector;
    }

    @Override
    public void createConsumer(Tenant t) throws Exception {
        String tenantName = t != null ? t.getTenantDomain() : "";
        //Config MQ
        String destination = "queue://MQ-" + tenantName;
        //Creation d'une factory pour les connexions aux queues avec les paramètres passés en fichier de configuration
        //QueueConnectionFactory factory = new ConnectionFactoryImpl(MQHost, MQPort, MQUser, MQPassword);
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(consumerConnector);

        try {
            connection = factory.createQueueConnection();//.createQueueConnection(MQUser, MQPassword);
            connection.start();
            //Listener d'exception
            connection.setExceptionListener(messagesReceiver);
            //Création d'une session avec acknowledge automatique des messages
            QueueSession sessionAuto = connection.createQueueSession(false, Session.CLIENT_ACKNOWLEDGE);
            sessions.put(tenantName, sessionAuto);

            //Création des queues, consumers et producers
            Queue queue = sessionAuto.createQueue(destination);

            QueueReceiver qr = sessionAuto.createReceiver(queue);
            //Listener de message
            qr.setMessageListener(messagesReceiver);

            QueueSender qs = sessionAuto.createSender(queue);
            qs.setDeliveryMode(DeliveryMode.PERSISTENT);
            senders.put(tenantName, qs);
        } catch (JMSException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void launchBroker() {
        try {
            broker = new BrokerService();
            KahaPersistenceAdapter adaptor = new KahaPersistenceAdapter();
            adaptor.setDirectory(new File(amqDir));
            broker.setPersistenceAdapter(adaptor);
            broker.setUseJmx(false);
            broker.setStartAsync(!this.internalBroker);
            broker.setPersistent(true);
            broker.addConnector(brokerConnector);
            SystemUsage systemUsage = broker.getSystemUsage();
            systemUsage.getStoreUsage().setLimit(1024L * 1024 * storeLimit);
            systemUsage.getTempUsage().setLimit(1024L * 1024 * storeLimit);
            broker.start();
        } catch (IOException e) {
            logger.error("Startup of '" + subsystem + "' subsystem exception", e);
        } catch (Exception e) {
            logger.error("Startup of '" + subsystem + "' subsystem exception", e);
        }
    }

    @Override
    public QueueSender getSender() {
        String tenantName = tenantAdminService.getCurrentUserDomain();
        return senders.get(tenantName);
    }

    @Override
    public TextMessage createTextMessage() throws JMSException {
        String tenantName = tenantAdminService.getCurrentUserDomain();
        return sessions.get(tenantName).createTextMessage();
    }

    @Override
    protected void onBootstrap(ApplicationEvent applicationEvent) {
        logger.setLevel(Level.INFO);
        logger.info("Starting '" + subsystem + "' subsystem");
        if (this.internalBroker) {
            launchBroker();
        }
        try {
            //Initialisation des consumers et producers
            List<Tenant> tenants = tenantAdminService.getAllTenants();
            if (!tenants.isEmpty()) {
                //Si parapheur multi-tenant, création des queues par tenant
                for (Tenant t : tenants) {
                    createConsumer(t);
                }
            }
            //création de seulement une queue pour le tenant root
            createConsumer(null);
            logger.info("Startup of '" + subsystem + "' subsystem complete");
        } catch (Exception e) {
            logger.error("Startup of '" + subsystem + "' subsystem exception", e);
        }
    }

    @Override
    protected void onShutdown(ApplicationEvent applicationEvent) {
        logger.info("Stopping '" + subsystem + "' subsystem");
        try {
            //Fermeture des sender
            for (QueueSender sender : senders.values()) {
                sender.close();
            }
            //Fermeture des sessions
            for (QueueSession session : sessions.values()) {
                session.close();
            }
            //Fermeture de la connexion
            connection.close();
            //Fermeture du broker
            broker.stop();

        } catch (Exception e) {
            logger.error("Exception during '" + subsystem + "' subsystem shutdown", e);
        }
        logger.info("Stopped '" + subsystem + "' subsystem");
    }
}
