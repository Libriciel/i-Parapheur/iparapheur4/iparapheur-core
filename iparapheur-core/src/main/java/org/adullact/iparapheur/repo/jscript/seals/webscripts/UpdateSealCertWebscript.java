package org.adullact.iparapheur.repo.jscript.seals.webscripts;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.adullact.iparapheur.repo.jscript.seals.SealCertificate;
import org.adullact.iparapheur.repo.jscript.seals.SealUtils;
import org.alfresco.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;

/**
 * Créé par lhameury le 4/19/17.
 */
public class UpdateSealCertWebscript extends AbstractWebScript {

    private final ServiceRegistry serviceRegistry;
    private String certificatesKey;

    @Autowired
    public UpdateSealCertWebscript(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setCertificatesKey(String certificatesKey) {
        this.certificatesKey = certificatesKey;
    }

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        final GsonBuilder builder = new GsonBuilder();
        final Gson gson = builder.create();

        SealCertificate certificate = SealUtils.updateSealCertificate(
                gson.fromJson(webScriptRequest.getContent().getReader(), SealCertificate.class),
                certificatesKey,
                serviceRegistry);

        webScriptResponse.setStatus(200);
        webScriptResponse.setContentEncoding("UTF-8");
        webScriptResponse.getWriter().write(gson.toJson(certificate));
    }

}
