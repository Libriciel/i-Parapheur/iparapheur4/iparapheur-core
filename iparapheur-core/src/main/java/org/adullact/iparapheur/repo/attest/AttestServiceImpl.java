package org.adullact.iparapheur.repo.attest;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import org.adullact.iparapheur.repo.notification.socket.SocketMessage;
import org.adullact.iparapheur.repo.notification.socket.SocketServer;
import org.adullact.iparapheur.status.StatusMetier;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.Tenant;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.extensions.surf.util.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.transaction.UserTransaction;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by lhameury on 17/09/15.
 *
 *
 */
public class AttestServiceImpl implements AttestService {

    private static Logger logger = Logger.getLogger(AttestService.class);

    @Autowired
    private NodeService nodeService;

    @Autowired
    private ParapheurService parapheurService;

    @Autowired
    private SearchService searchService;

    @Autowired
    private NamespaceService namespaceService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private ContentService contentService;

    @Autowired
    private TenantAdminService tenantAdminService;

    @Autowired
    private SocketServer socketServer;

    @Qualifier("fileFolderService")
    @Autowired
    private FileFolderService fileFolderService;

    private Integer intervalleInteger;

    private String dirRoot;

    private String interval;

    private boolean isEnabled = false;

    public void setDirRoot(String dirRoot) {
        this.dirRoot = dirRoot;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    @Override
    public Map<String, String> getProperties() {
        Map<String, String> properties = new HashMap<String, String>();

        NodeRef configNode = getConfigNode();

        try {
            ContentReader reader = contentService.getReader(configNode, ContentModel.PROP_CONTENT);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(reader.getContentInputStream());

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            Element mainNode = (Element) doc.getElementsByTagName("attest-server").item(0);

            Node enabled = mainNode.getElementsByTagName("enabled").item(0).getFirstChild();
            Node host = mainNode.getElementsByTagName("host").item(0).getFirstChild();
            Node port = mainNode.getElementsByTagName("port").item(0).getFirstChild();
            Node username = mainNode.getElementsByTagName("username").item(0).getFirstChild();
            Node password = mainNode.getElementsByTagName("password").item(0).getFirstChild();

            isEnabled = Boolean.valueOf(enabled.getNodeValue());

            properties.put("enabled", host != null ? enabled.getNodeValue() : "");
            properties.put("host", host != null ? host.getNodeValue() : "");
            properties.put("port", port != null ? port.getNodeValue() : "");
            properties.put("username", username != null ? username.getNodeValue() : "");
            properties.put("password", password != null ? password.getNodeValue() : "");


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return properties;
    }

    @Autowired
    @Qualifier("schedulerFactory")
    private Scheduler scheduler;

    @Override
    public void restartGetAttestJobs() {
        List<Tenant> tenants = tenantAdminService.getAllTenants();
        List<String> admins = new ArrayList<String>();
        admins.add("admin"); // Execution in root tenant
        for (Tenant tenant : tenants) {
            if (tenant.isEnabled()) {
                admins.add(tenantService.getDomainUser("admin", tenant.getTenantDomain()));
            }
        }

        for (String admin : admins) {
            AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork() {
                @Override
                public Object doWork() throws Exception {
                    restartGetAttestJob();
                    return null;
                }

            }, admin);
        }
    }

    @Override
    public void restartGetAttestJob() {
        UserTransaction utx = transactionService.getUserTransaction();
        try {
            utx.begin();

            String dossiersXPath = "//*[hasAspect('ph:attest')]";
            List<NodeRef> documents = searchService.selectNodes(
                    nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                    dossiersXPath,
                    null,
                    namespaceService,
                    false);

            for (NodeRef document : documents) {
                if(nodeService.hasAspect(document, ParapheurModel.ASPECT_ATTEST) && nodeService.getProperty(document, ParapheurModel.PROP_ATTEST_ID) != null) {
                    startGetAttestJob(document);
                }
            }

            utx.commit();
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
                logger.error(ex);
            }
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isServiceEnabled() {
        return isEnabled;
    }

    /**
     * Lancement de la tache planifiee qui interroge le service d'attestation pour savoir ou en
     * est un dossier en particulier
     *
     * @param docRef  le document pour lequel on doit interroger le service d'attestation
     */
    protected void startGetAttestJob(NodeRef docRef) {
        logger.setLevel(Level.ALL);
        try {
            long oneMinute = 30 * 1000;

            // BLEX : relance toutes les X min
            long maxRepeatInterval = getJobIntervalle() * oneMinute;

            // BLEX : delai de lancement au hasard entre dans 1 minute et X minutes
            long delay=(long)(Math.random()* maxRepeatInterval) + oneMinute ;

            String infoMessage=null;
            if (logger.isInfoEnabled()) {
                infoMessage=
                        "dossier :"
                                +" ["+nodeService.getProperty(docRef, ContentModel.PROP_NAME)+"]"
                                + " [" + docRef.getId() + "]";
                int id=(int) (delay/oneMinute);
                int ir=(int) (maxRepeatInterval/oneMinute);
                logger.info (
                        "schedule GetAttestJob : "
                                +infoMessage
                                +", delay : "+id+"mn"
                                + ", repeat : " + ir + "mn");
            }

            // Trigger: Execute in X minutes, and repeat every 'repeatInterval' minutes
            Trigger trigger = new SimpleTrigger(
                    docRef.getId(),
                    "ATTEST",
                    // new Date(System.currentTimeMillis() + (2 * 60 * 1000)), // Now + 2 minutes
                    new Date(System.currentTimeMillis() + delay), // BLEX
                    null,
                    SimpleTrigger.REPEAT_INDEFINITELY,
                    maxRepeatInterval);           // was:   5 * 60 * 1000); // 5 minutes

            JobDetail details = new JobDetail(
                    docRef.getId(),
                    "ATTEST",
                    GetAttestJob.class);

            Map<String, Object> jobDataMap = new HashMap<String, Object>();
            jobDataMap.put("attestService", this);
            jobDataMap.put("transactionService", transactionService);
            jobDataMap.put("document", docRef);
            NodeRef dossierref = nodeService.getPrimaryParent(docRef).getParentRef();
            jobDataMap.put("runAs", parapheurService.getPreviousEtapeCircuit(dossierref).getValidator());

            // BLEX
            jobDataMap.put(GetAttestJob.K.infoMessage, infoMessage);

            details.setJobDataMap(new JobDataMap(jobDataMap));

            scheduler.scheduleJob(details, trigger);
        } catch (Exception e) {
            throw new RuntimeException("Error scheduling job", e);
        }
    }

    /**
     * intervalle de temps pour le job startGetAttestJob
     * @return Integer nombre de minutes
     */
    private Integer getJobIntervalle() {
        if (intervalleInteger == null) {
            if (interval == null) {
                logger.warn("Propriete " + ParapheurModel.PARAPHEUR_ATTEST_JOBINTERVAL_KEY + " non trouvee, valeur utilisee par defaut = " + ParapheurModel.PARAPHEUR_ATTEST_JOBINTERVAL_DEFAULT);
                interval = ParapheurModel.PARAPHEUR_ATTEST_JOBINTERVAL_DEFAULT;
            } else {
                logger.info("Propriete " + ParapheurModel.PARAPHEUR_ATTEST_JOBINTERVAL_KEY + " trouvee = " + interval);
            }
            // La valeur trouvée doit être de type entier (nombre de minutes)
            try {
                intervalleInteger = Integer.parseInt(interval);
            } catch (NumberFormatException ex) {
                intervalleInteger = Integer.parseInt(ParapheurModel.PARAPHEUR_ATTEST_JOBINTERVAL_DEFAULT);
                logger.warn("Propriete " + ParapheurModel.PARAPHEUR_ATTEST_JOBINTERVAL_KEY + " mal typee : doit être un entier (nombre de minutes), valeur utilisee par defaut : " + ParapheurModel.PARAPHEUR_ATTEST_JOBINTERVAL_DEFAULT);
            }
            // La valeur doit être supérieure à 0
            if (intervalleInteger <= 0) {
                logger.warn("Propriete " + ParapheurModel.PARAPHEUR_ATTEST_JOBINTERVAL_KEY + "  doit être supérieure à 0 (nombre de minutes), valeur utilisee par defaut : " + ParapheurModel.PARAPHEUR_TDTS2LOW_STATUTJOBINTERVAL_DEFAUT);
                intervalleInteger = Integer.parseInt(ParapheurModel.PARAPHEUR_ATTEST_JOBINTERVAL_DEFAULT);
            }
        }
        return intervalleInteger;
    }

    private void socketNotify(String state, NodeRef node, DossierService.ACTION_DOSSIER action) {
        SocketMessage message = new SocketMessage();
        message.setUsers(SocketServer.getUsers());
        JSONObject msg = new JSONObject();
        try {
            msg.put("id", node.getId());
            msg.put("action", action.name());
            msg.put("state", state);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        message.setMsg(msg);
        socketServer.notify(message);
    }

    @Override
    public int getAttest(NodeRef docRef) throws IOException {
        boolean hasGetAttest = false;
        boolean hasError = false;

        AttestHttpClient httpclient = new AttestHttpClient();

        NodeRef dossierRef = null;

        try {
            String id = (String) nodeService.getProperty(docRef, ParapheurModel.PROP_ATTEST_ID);
            System.out.println("Getting attest for id : " + id);

            dossierRef = nodeService.getPrimaryParent(docRef).getParentRef();

            ContentWriter pdfwriter = contentService.getWriter(docRef, ParapheurModel.PROP_ATTEST_CONTENT, Boolean.TRUE);
            pdfwriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
            pdfwriter.setEncoding("UTF-8");



            HttpGet getFile = new HttpGet("/api/reports/" + id);
            HttpParams params = getFile.getParams();
            params.setParameter("format", "pdf");
            params.setParameter("type", "simple");
            getFile.setParams(params);
            HttpResponse response = httpclient.execute(getFile);

            if (response.getStatusLine().getStatusCode() == 200) {
                HttpEntity resEntity = response.getEntity();

                pdfwriter.putContent(resEntity.getContent());

                nodeService.removeAspect(docRef, ParapheurModel.ASPECT_ATTEST);

                socketNotify(SocketMessage.State.END.name(), docRef, DossierService.ACTION_DOSSIER.GET_ATTEST);

                EntityUtils.consume(resEntity);
                hasGetAttest = true;
            } else if(response.getStatusLine().getStatusCode() == 404) {
                //Oups, l'attestation demandée n'existe pas, et n'est pas en cours de génération...
                throw new Exception("L'attestation d'id " + id + " n'existe pas");
            }
        } catch(Exception e) {
            if(dossierRef != null) {
                // On supprime l'ID de la transaction pour régénération éventuelle et norification de l'erreur !
                nodeService.removeAspect(docRef, ParapheurModel.ASPECT_ATTEST);
                nodeService.addAspect(docRef, ParapheurModel.ASPECT_ATTEST, null);
            }
            try {
                if (logger.isInfoEnabled() && dossierRef != null) {
                    logger.info (
                            "unschedule GetAttestJob : "
                                    + "document:" + docRef.getId() + " \"" + nodeService.getProperty(docRef, ContentModel.PROP_TITLE) + "\"");
                }
                scheduler.unscheduleJob(
                        docRef.getId(),
                        "ATTEST");
            } catch (SchedulerException ex) {
                logger.error("Exception canceling Job", ex);
            }
            hasError = true;
            socketNotify(SocketMessage.State.ERROR.name(), docRef, DossierService.ACTION_DOSSIER.GET_ATTEST);
            setStatus(docRef, StatusMetier.STATUS_ATTEST_KO, "Impossible de récupérer l'attestation de signature du document " + nodeService.getProperty(docRef, ContentModel.PROP_NAME));
        } finally {
            try { httpclient.getConnectionManager().shutdown(); } catch (Exception ignore) {}

            if(dossierRef != null) {
                if(hasGetAttest) {
                    try {
                        if (logger.isInfoEnabled()) {
                            logger.info (
                                    "unschedule GetAttestJob : "
                                    + "document:" + docRef.getId() + " \"" + nodeService.getProperty(docRef, ContentModel.PROP_TITLE) + "\"");
                        }
                        scheduler.unscheduleJob(
                                docRef.getId(),
                                "ATTEST");
                    } catch (SchedulerException ex) {
                        logger.error("Exception canceling Job", ex);
                    }
                    setStatus(docRef, StatusMetier.STATUS_EN_COURS_ATTEST, "Attestation du document " + nodeService.getProperty(docRef, ContentModel.PROP_NAME) + " récupérée");
                }


                boolean removePending = true;
                for (NodeRef mainDoc : parapheurService.getMainDocuments(dossierRef)) {
                    if (nodeService.hasAspect(mainDoc, ParapheurModel.ASPECT_ATTEST) && nodeService.getProperty(mainDoc, ParapheurModel.PROP_ATTEST_ID) != null) {
                        removePending = false;
                        break;
                    }
                }

                if (removePending) {
                    socketNotify(SocketMessage.State.END.name(), dossierRef, DossierService.ACTION_DOSSIER.GET_ATTEST);
                    nodeService.removeAspect(dossierRef, ParapheurModel.ASPECT_PENDING);
                    if(!hasError) {
                        setStatus(docRef, StatusMetier.STATUS_ATTEST_OK, "Attestations de signature récupérée pour tous les documents");
                        setStatus(docRef, StatusMetier.STATUS_ARCHIVE, "Circuit terminé, dossier archivable");
                    }
                }
            }
        }



        //pdfwriter.putContent(new ByteArrayInputStream(visudoc.getContent()));


        return 0;
    }

    private void setStatus(NodeRef doc, String status, String message) {
        NodeRef dossier = nodeService.getPrimaryParent(doc).getParentRef();

        nodeService.setProperty(dossier, ParapheurModel.PROP_STATUS_METIER, status);

        List<String> listProps = parapheurService.getParapheurOwners(parapheurService.getParentParapheur(dossier));
        if(listProps != null && listProps.size() > 0) {
            message = listProps.get(0) + ": " + message;
        }

        List<Object> list = new ArrayList<Object>();
        list.add(status);
        parapheurService.auditWithNewBackend("ParapheurServiceCompat", message, dossier, list);
    }

    @Override
    public void updateConfiguration(boolean enabled, String host, int port, String username, String password) {
        createXmlConfig(enabled, getConfigNode(), host, port, username, password);
    }

    private class AttestHttpClient extends DefaultHttpClient {

        private HttpHost targetHost;
        private String token;

        public AttestHttpClient() {
            super();
            // Get real authentication from configuration
            Map<String, String> props = getProperties();
            // Get real server from configuration
            targetHost = new HttpHost(props.get("host"), Integer.valueOf(props.get("port")), "http");

            try {
                StringEntity postingString =new StringEntity("{\"email\":\"" + props.get("username") + "\", \"password\":\"" +props.get("password")+ "\"}");
                HttpPost post = new HttpPost("/auth/local");
                post.setEntity(postingString);
                post.setHeader("Content-type", "application/json");
                HttpResponse  response = this.execute(post);
                JSONObject tokenObj = new JSONObject(IOUtils.toString(response.getEntity().getContent()));
                token = (String) tokenObj.get("token");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public HttpResponse execute(HttpRequestBase request) throws IOException {
            //
            StringBuilder requestUrl = new StringBuilder(request.getURI().getPath());

            BasicHttpParams params = (BasicHttpParams) request.getParams();
            params.setParameter("access_token", token);

            boolean isFirst = true;
            for(String name: params.getNames()) {
                if(isFirst) {
                    requestUrl.append("?");
                    isFirst = false;
                } else {
                    requestUrl.append("&");
                }
                requestUrl.append(name);
                requestUrl.append("=");
                requestUrl.append(params.getParameter(name));
            }

            try {
                request.setURI(new URI(requestUrl.toString()));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return super.execute(targetHost, request);
        }
    }

    private void createXmlConfig(boolean s_enabled, NodeRef rootRef, String s_host, int s_port, String s_username, String s_password) {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("attest-server");
            doc.appendChild(rootElement);

            // staff elements
            isEnabled = s_enabled;
            Element enabled = doc.createElement("enabled");
            enabled.appendChild(doc.createTextNode(String.valueOf(s_enabled)));
            rootElement.appendChild(enabled);

            // staff elements
            Element host = doc.createElement("host");
            host.appendChild(doc.createTextNode(s_host));
            rootElement.appendChild(host);

            // staff elements
            Element port = doc.createElement("port");
            port.appendChild(doc.createTextNode(String.valueOf(s_port)));
            rootElement.appendChild(port);

            // staff elements
            Element username = doc.createElement("username");
            username.appendChild(doc.createTextNode(s_username));
            rootElement.appendChild(username);

            // staff elements
            Element password = doc.createElement("password");
            password.appendChild(doc.createTextNode(s_password));
            rootElement.appendChild(password);

            // write the content into xml file
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Source xmlSource = new DOMSource(doc);
            Result outputTarget = new StreamResult(outputStream);
            TransformerFactory.newInstance().newTransformer().transform(xmlSource, outputTarget);

            ContentWriter pdfwriter = contentService.getWriter(rootRef, ContentModel.PROP_CONTENT, Boolean.TRUE);

            pdfwriter.putContent(new ByteArrayInputStream(outputStream.toByteArray()));

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    private NodeRef getConfigNode() {

        NodeRef configNode;
        String serviceXpath = "/app:company_home/app:dictionary/ph:attest_service";

        List<NodeRef> resultSearch = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                serviceXpath,
                null,
                namespaceService,
                false);


        if (resultSearch.size() > 0) {
            configNode = resultSearch.get(0);
        }
        else {
            String dictionaryXpath = "/app:company_home/app:dictionary";
            NodeRef dictionary = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                    dictionaryXpath,
                    null,
                    namespaceService,
                    false).get(0);

            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, "AttestService");
            properties.put(ContentModel.PROP_TITLE, "AttestService Configuration");
            properties.put(ContentModel.PROP_DESCRIPTION, "Configuration for attestation service");
            configNode = nodeService.createNode(dictionary,
                    ContentModel.ASSOC_CONTAINS,
                    QName.createQName("ph:attest_service", namespaceService),
                    ContentModel.TYPE_FOLDER,
                    properties).getChildRef();

            createXmlConfig(false, configNode, "libervalid.dev.adullact.org", 9000, "admin", "admin");
        }
        return configNode;
    }

    /**
     * @see
     * com.atolcd.parapheur.repo.S2lowService#envoiS2lowHelios(org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public boolean envoiGetAttest(NodeRef dossier) throws Exception {

        boolean success = true;
        AttestHttpClient httpclient = new AttestHttpClient();

        try {

            HttpPost uploadFile = new HttpPost("/api/reports");

            //For each main document...
            List<NodeRef> mainsDocuments = parapheurService.getMainDocuments(dossier);
            for(int i = 0; i < mainsDocuments.size(); i++) {
                NodeRef document = mainsDocuments.get(i);
                Map<QName, Serializable> props = null;
                // On check l'état de l'attestation sur le document
                if(nodeService.hasAspect(document, ParapheurModel.ASPECT_ATTEST)) {
                    // Pas d'ID avec l'aspect ? Oups... Une erreur ! On reset !
                    if(nodeService.getProperty(document, ParapheurModel.PROP_ATTEST_ID) == null) {
                        nodeService.removeAspect(document, ParapheurModel.ASPECT_ATTEST);
                    } else {
                        // L'ID est renseigné, donc le dossier est en cours de génération...
                        continue;
                    }
                } else if(nodeService.getProperty(document, ParapheurModel.PROP_ATTEST_CONTENT) != null) {
                    // L'attestation existe déjà...
                    continue;
                }
                try {
                    boolean hasContent = false;
                    FileInfo infos = fileFolderService.getFileInfo(document);
                    String filePath = infos.getContentData().getContentUrl().replace("store://", dirRoot);
                    FileBody bin = new FileBody(new File(filePath), infos.getName(), infos.getContentData().getMimetype(), infos.getContentData().getEncoding());
                    MultipartEntity reqEntity = new MultipartEntity();

                    // Récupération des signatures détachées ssi PKCS#7/single
                    if(ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7.equals(nodeService.getProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT))) {
                        List<EtapeCircuit> etapes = parapheurService.getCircuit(dossier);
                        for(int j = 0; j < etapes.size(); j++) {
                            EtapeCircuit etape = etapes.get(j);
                            if(etape.getActionDemandee().equals("SIGNATURE")) {
                                String sig = etape.getSignatureEtape();
                                ByteArrayBody bab = new ByteArrayBody(new String(Base64.decode(sig)).split(",")[i].getBytes(), "application/pkcs7-signature",  "signature-" + i + "-" + j);
                                reqEntity.addPart("content", bab);
                                hasContent = true;
                                break;
                            }
                        }
                    }

                    reqEntity.addPart(hasContent ? "original" : "content", bin);

                    uploadFile.setEntity(reqEntity);
                    // TODO Get eventually signature file

                    HttpResponse response = httpclient.execute(uploadFile);
                    HttpEntity resEntity = response.getEntity();

                    if (response.getStatusLine().getStatusCode() == 200 && resEntity != null) {
                        JSONObject jsonResponse = new JSONObject(IOUtils.toString(resEntity.getContent()));
                        props = new HashMap<QName, Serializable>();
                        props.put(ParapheurModel.PROP_ATTEST_ID, (String) jsonResponse.get("id"));

                        startGetAttestJob(document);
                        setStatus(document, StatusMetier.STATUS_EN_COURS_ATTEST, "Récupération de l'attestation de signature pour le document " + nodeService.getProperty(document, ContentModel.PROP_NAME) + " en cours");
                    } else {
                        throw new Exception("Error from server : " + response.getStatusLine().getStatusCode());
                    }
                    EntityUtils.consume(resEntity);
                } catch (Exception e) {
                    setStatus(document, StatusMetier.STATUS_ATTEST_KO, "Impossible de récupérer l'attestation de signature du document " + nodeService.getProperty(document, ContentModel.PROP_NAME));
                    e.printStackTrace();
                    success = false;
                } finally {
                    nodeService.addAspect(document, ParapheurModel.ASPECT_ATTEST, props);
                }
            }
        } finally {
            try { httpclient.getConnectionManager().shutdown(); } catch (Exception ignore) {}
        }
        return success;
    }

}
