/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.adullact.iparapheur.repo.jscript.seals.webscripts;

import org.adullact.iparapheur.repo.jscript.seals.SealUtils;
import org.adullact.iparapheur.repo.jscript.seals.exceptions.MalformedNodeRefException;
import org.adullact.iparapheur.repo.jscript.seals.exceptions.NodeRefIsNotSealException;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;

/**
 * Créé par lhameury le 4/14/17.
 */
public class DeleteSealCertWebscript extends AbstractWebScript {

    private final ServiceRegistry serviceRegistry;

    @Autowired
    public DeleteSealCertWebscript(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String id = webScriptRequest.getServiceMatch().getTemplateVars().get("id");

        int status = 200;

        try {
            SealUtils.deleteSealCertificate(
                    id,
                    serviceRegistry);
        } catch (MalformedNodeRefException e) {
            status = 400;
        } catch (InvalidNodeRefException e) {
            status = 404;
        } catch (NodeRefIsNotSealException e) {
            status = 403;
        }

        webScriptResponse.setStatus(status);
    }
}
