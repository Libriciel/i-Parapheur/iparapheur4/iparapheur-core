package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.repo.CorbeillesService;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Créé par lhameury le 15/01/18.
 */
public class CorbeilleServiceScriptable extends BaseProcessorExtension {

    private final CorbeillesService corbeillesService;

    @Autowired
    public CorbeilleServiceScriptable(CorbeillesService corbeillesService) {
        this.corbeillesService = corbeillesService;
    }

    public void regenerateCount() {
        corbeillesService.regenerateAllCorbeilleCount();
    }
}
