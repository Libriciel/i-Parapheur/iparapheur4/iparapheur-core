package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.repo.impl.FastServiceImpl;
import org.adullact.iparapheur.util.X509Util;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.ssl.KeyMaterial;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.BaseElement;
import org.junit.Assert;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author stoulouse
 */
public class FastConfigScriptable extends BaseProcessorExtension {
    private static final Logger log = Logger.getLogger(FastConfigScriptable.class);

    private S2lowService fastService;

    private NodeService nodeService;

    private NamespaceService namespaceService;

    private SearchService searchService;

    private ContentService contentService;

    private FileFolderService fileFolderService;

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setNamespaceService(NamespaceService namespaceService) {
        this.namespaceService = namespaceService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setFastService(S2lowService fastService) {
        this.fastService = fastService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public Map<String, String> getActesParameters() {
        return getParameters("/fast/actes");
    }

    public void setActesParameters(Map<String, String> params) {
        setParameters("/fast/actes", params);
    }

    public Map<String, String> getHeliosParameters() {
        return getParameters("/fast/helios");
    }

    public void setHeliosParameters(Map<String, String> params) {
        setParameters("/fast/helios", params);
    }

    public boolean isEnabled() {
        return fastService.isEnabled();
    }

    protected Map<String, String> getParameters(String configPath) {
        Document doc = getConfigDocument();

        List<Node> configNodes = doc.selectNodes(configPath);

        Assert.assertEquals("XPath query returned no results", 1, configNodes.size());
        Assert.assertTrue("Invalid XPath query", configNodes.get(0) instanceof Element);

        Element configNode = (Element) configNodes.get(0);

        List<Element> configElements = configNode.elements();

        Map<String, String> configParams = new HashMap<String, String>();

        for (Element element : configElements) {
            configParams.put(element.getName(), element.getTextTrim());
        }

         if (configParams.containsKey("name") && configParams.get("name")!=null &&
                configParams.containsKey("password") && configParams.get("password")!=null) {
            String res = isPwdValidForCertificate(configParams.get("name"), configParams.get("password"));
            if (res.startsWith("ok")) {
                configParams.put("isPwdGoodForPkcs", "ok");
                configParams.put("dateLimite", res.substring(2));
            } else {
                configParams.put("isPwdGoodForPkcs", res);
            }
        } else {
            log.error("no valid param found 'name' or 'password'.");
            configParams.put("isPwdGoodForPkcs", "ko");
        }

        return configParams;
    }

    protected void setParameters(String configPath, Map<String, String> params) {
        Document doc = getConfigDocument();

        List<Node> configNodes = doc.selectNodes(configPath);

        Assert.assertEquals("XPath query returned no results", 1, configNodes.size());
        Assert.assertTrue("Invalid XPath query", configNodes.get(0) instanceof Element);

        Element configNode = (Element) configNodes.get(0);

        // Remove all previous elements
        for (Element e : ((List<Element>) configNode.elements())) {
            configNode.remove(e);
        }

        for (Entry<String, String> param : params.entrySet()) {
            System.out.println("Processing: " + param.getKey() + " = " + param.getValue());
            Element e = new BaseElement(param.getKey());
            e.setText(param.getValue());

            configNode.add(e);
        }

        setConfigDocument(doc);

        /* unschedule job and restart with new parameters (check_freq)*/
        ((FastServiceImpl)fastService).unscheduleGetStatusJob();
        fastService.restartGetS2lowStatusJob();
    }

    protected Document getConfigDocument() {
        NodeRef configNodeRef = getConfigNode();

        ContentReader reader = contentService.getReader(configNodeRef, ContentModel.PROP_CONTENT);
        SAXReader saxReader = new SAXReader();
        try {
            Document doc = saxReader.read(reader.getContentInputStream());

            return doc;
        } catch (DocumentException ex) {
            log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    protected void setConfigDocument(Document doc) {
        NodeRef configNode = getConfigNode();

        ContentWriter writer = contentService.getWriter(configNode, ContentModel.PROP_CONTENT, true);

        writer.putContent(doc.asXML());
    }

    protected NodeRef getConfigNode() {
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                "/app:company_home/app:dictionary/ph:certificats_fast/cm:fast_properties.xml",
                null,
                namespaceService,
                false);

        if (nodes.size() != 1) {
            throw new RuntimeException("Can't find Fast configuration file");
        }

        NodeRef configNodeRef = nodes.get(0);

        return configNodeRef;
    }

    private InputStream readFile(String nameFile) throws Exception {
        InputStream inputStream = null;
        List<NodeRef> nodes = searchService.selectNodes(nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                "/app:company_home/app:dictionary/ph:certificats_fast/cm:" + org.alfresco.webservice.util.ISO9075.encode(nameFile),
                null,
                namespaceService,
                false);

        if (nodes.size() != 1) {
            throw new RuntimeException("readFile: Can't find the file '" + nameFile + "', SIZE=" + nodes.size());
        }
        NodeRef fileNodeRef = nodes.get(0);
        ContentReader reader = contentService.getReader(fileNodeRef, ContentModel.PROP_CONTENT);
        inputStream = reader.getContentInputStream();
        return inputStream;
    }

    /**
     * Teste le couple certificat PKCS#12 / pwd donné en paramètre
     * @param name
     * @param pwdString
     * @return true si le mot de passe est correct, false sinon.
     */
    public String isPwdValidForCertificate(String name, String pwdString) {
        String res = "ko";
        InputStream is = null;
        try {
            is = readFile(name);
            if (is == null) {
                log.error("No certificate found, null.");
            } else {
                KeyMaterial km = new KeyMaterial(is, pwdString.toCharArray());
                if (km != null) {
                    if (km.getKeyStore() != null) {
                        List<String> my = java.util.Collections.list(km.getKeyStore().aliases());
                        if (!my.isEmpty()) {
                            res = "ok";
                            for (String alias : my) {
                                X509Certificate xcert = (X509Certificate) km.getKeyStore().getCertificate(alias);
                                xcert.checkValidity();
                                SimpleDateFormat sdf = new SimpleDateFormat("E dd MMM yyyy 'à' HH:mm", java.util.Locale.FRENCH);
                                res += sdf.format(xcert.getNotAfter());
                            }
                            log.info("Cert is good !, sending to display '" + res +"'");
                        } else {
                            log.error("No certificate found, km.getCertificates() for ('" + name + "', '" + pwdString + "') is empty.");
                        }
                    } else {
                        log.error("No certificate found, as KeyMaterial for ('" + name + "', '" + pwdString + "') has no keystore.");
                    }
                } else {
                    log.error("No certificate found, KeyMaterial for ('" + name + "', '" + pwdString + "') is null.");
                }
            }
        } catch (java.io.IOException ex) {
            log.error("Open PKCS#12 impossible, IOException", ex);
        } catch (java.security.cert.CertificateExpiredException ex) {
            log.error("Open PKCS#12 impossible, CertificateExpiredException " + ex.getLocalizedMessage(), ex);
            res = "expire";
        } catch (java.security.cert.CertificateNotYetValidException ex) {
            log.error("Open PKCS#12 impossible, CertificateNotYetValidException " + ex.getLocalizedMessage(), ex);
            res = "expire";
        } catch (java.security.cert.CertificateException ex) {
            log.error("Open PKCS#12 impossible, CertificateException", ex);
        } catch (java.security.KeyStoreException ex) {
            log.error("Open PKCS#12 impossible, KeyStoreException", ex);
        } catch (java.security.NoSuchAlgorithmException ex) {
            log.error("Open PKCS#12 impossible, NoSuchAlgorithmException", ex);
        } catch (java.security.UnrecoverableKeyException ex) {
            log.error("Open PKCS#12 impossible, UnrecoverableKeyException", ex);
        } catch (java.lang.Exception ex) {
            log.error("Open PKCS#12 impossible, GenericException " + ex.getLocalizedMessage(), ex);
        } finally {
            try {
                if (is!=null) {
                    is.close();
                }
            } catch (java.io.IOException ex) {
                log.error("Close PKCS#12 stream impossible, IOException", ex);
            }
        }
        return res;
    }

    /**
     * Teste le couple certificat PKCS#12 / pwd donné en paramètre
     *
     * @param file le fichier certificat PKCS#12
     * @param pwdString le mot de passe du certificat
     * @return ok, ko, ou expire
     */
    public String isPwdValidForFileCertificate(String file, String pwdString) {

        byte[] decoded = Base64.decodeBase64(file.getBytes());
        ByteArrayInputStream is = new ByteArrayInputStream(decoded);
        String result = X509Util.checkPasswordForCertificate(is, pwdString);

        try {
            is.close();
        } catch (java.io.IOException ex) {
            log.error("Close PKCS#12 stream impossible, IOException", ex);
        }

        return result;
    }

    public void createCertificateFile(String filename, String cert) {

        ResultSet result = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_XPATH, "/app:company_home/app:dictionary/ph:certificats");

        if (result.length() > 0) {
            NodeRef confHome = result.getNodeRef(0);
            List<ChildAssociationRef> childs = nodeService.getChildAssocs(confHome);
            NodeRef certRef = null;
            for(ChildAssociationRef child : childs) {
                if(nodeService.getProperty(child.getChildRef(), ContentModel.PROP_NAME).equals(filename)) {
                    certRef = child.getChildRef();
                }
            }

            if(certRef == null) {
                //Création
                FileInfo certInfo = fileFolderService.create(
                        confHome,
                        filename,
                        ContentModel.TYPE_CONTENT);
                certRef = certInfo.getNodeRef();
            }

            ByteArrayInputStream contentInputStream = new ByteArrayInputStream(Base64.decodeBase64(cert.getBytes()));
            // get a writer for the content and put the file
            ContentWriter writer = contentService.getWriter(certRef, ContentModel.PROP_CONTENT, true);
            writer.putContent(contentInputStream);
        }
    }
}
