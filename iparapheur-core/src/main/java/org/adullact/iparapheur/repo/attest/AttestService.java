package org.adullact.iparapheur.repo.attest;

import org.alfresco.service.cmr.repository.NodeRef;

import java.io.IOException;
import java.util.Map;

/**
 * Created by lhameury on 17/09/15.
 */
public interface AttestService {

    Map<String, String> getProperties();

    void restartGetAttestJobs();

    void restartGetAttestJob();

    boolean isServiceEnabled();

    int getAttest(NodeRef dossier) throws IOException;

    void updateConfiguration(boolean enabled, String host, int port, String username, String password);

    boolean envoiGetAttest(NodeRef dossier) throws Exception;
}
