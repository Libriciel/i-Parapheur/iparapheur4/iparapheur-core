/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */

package org.adullact.iparapheur.repo;

//import com.atolcd.parapheur.model.ParapheurModel;
import org.alfresco.service.namespace.QName;

/**
 *
 * @author vbarousse - ADULLACT Projet
 */
public interface MultiCModel {

    public static final String MULTI_C_MODEL_URI = "http://www.atolcd.com/alfresco/model/parapheur-mc/1.0";

    public static final String MULTI_C_MODEL_PREFIX = "phmc";

    public static final QName TYPE_COLLECTIVITE = QName.createQName(MULTI_C_MODEL_URI, "collectivite");

    public static final QName TYPE_PES_INFOS = QName.createQName(MULTI_C_MODEL_URI, "pesInfos");

    public static final QName PROP_TITLE = QName.createQName(MULTI_C_MODEL_URI, "title");

    public static final QName PROP_DESCRIPTION = QName.createQName(MULTI_C_MODEL_URI, "description");

    public static final QName PROP_SIREN = QName.createQName(MULTI_C_MODEL_URI, "siren");

    public static final QName PROP_CITY = QName.createQName(MULTI_C_MODEL_URI, "city");

    public static final QName PROP_POSTAL_CODE = QName.createQName(MULTI_C_MODEL_URI, "postalCode");

    public static final QName PROP_COUNTRY = QName.createQName(MULTI_C_MODEL_URI, "country");

    public static final QName PROP_POLICY_IDENTIFIER_ID = QName.createQName(MULTI_C_MODEL_URI, "policyIdentifierID");

    public static final QName PROP_POLICY_IDENTIFIER_DESCRIPTION = QName.createQName(MULTI_C_MODEL_URI, "policyIdentifierDescription");

    public static final QName PROP_POLICY_DIGEST = QName.createQName(MULTI_C_MODEL_URI, "policyDigest");

    public static final QName PROP_CLAIMED_ROLE = QName.createQName(MULTI_C_MODEL_URI, "claimedRole");

    public static final QName PROP_SPURI = QName.createQName(MULTI_C_MODEL_URI, "spuri");

    public static final QName NAME_COLLECTIVITE_INFOS = QName.createQName(MULTI_C_MODEL_URI, "collectivite_infos");

    public static final QName NAME_COLLECTIVITE_PES_INFOS = QName.createQName(MULTI_C_MODEL_URI, "pes_infos");

}
