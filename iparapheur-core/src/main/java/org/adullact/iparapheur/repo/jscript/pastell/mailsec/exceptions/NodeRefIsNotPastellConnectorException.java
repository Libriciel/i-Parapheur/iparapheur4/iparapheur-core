package org.adullact.iparapheur.repo.jscript.pastell.mailsec.exceptions;

import org.alfresco.error.AlfrescoRuntimeException;

/**
 * Créé par lhameury le 07/05/18.
 */
public class NodeRefIsNotPastellConnectorException extends AlfrescoRuntimeException {
    public NodeRefIsNotPastellConnectorException() { super("NodeRefIsNotPastellConnectorException"); }
}