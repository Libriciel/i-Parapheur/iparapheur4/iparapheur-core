package org.adullact.iparapheur.repo.amq.impl;

import lombok.extern.log4j.Log4j;
import org.adullact.iparapheur.repo.worker.SchedulerService;
import org.adullact.iparapheur.repo.worker.WorkerService;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.*;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by lhameury on 14/01/14.
 */
@Log4j
public class MessagesReceiver implements MessageListener, ExceptionListener {

    @Autowired
    private SchedulerService schedulerService;

    /**
     This method is called asynchronously by JMS when a message arrives
     at the queue. Client applications must not throw any exceptions in
     the onMessage method.
     @param message A JMS message.
     */
    @Override
    public void onMessage(Message message)
    {
        TextMessage m = (TextMessage) message;
        try {
            if(m != null) {
                JSONObject obj = new JSONObject(m.getText());
                HashMap request = new ObjectMapper().readValue(obj.toString(), HashMap.class);
                log.info("Consumer : message received; ACTION : " + WorkerService.ACTION + "; ID : " + WorkerService.ID);
                SelectWorkerRunnable selectWorkerRunnable = new SelectWorkerRunnable(schedulerService, request, m);
                (new Thread(selectWorkerRunnable)).start();
            }
        } catch (JMSException | JSONException | IOException ex) {
            log.error("Error on onMessage : ", ex);
        }
    }

    public class SelectWorkerRunnable implements Runnable {
        private SchedulerService schedulerService;
        private HashMap request;
        private Message message;

        public SelectWorkerRunnable(SchedulerService schedulerService, HashMap request, Message message) {
            this.schedulerService = schedulerService;
            this.request = request;
            this.message = message;
        }

        @Override
        public void run() {
            try {
                schedulerService.selectWorker(request);
                message.acknowledge();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     This method is called asynchronously by JMS when some error occurs.
     When using an asynchronous message listener it is recommended to use
     an exception listener also since JMS have no way to report errors
     otherwise.
     @param exception A JMS exception.
     */
    @Override
    public void onException(JMSException exception) {
        log.error("AMQ : an error occurred", exception);
    }
}
