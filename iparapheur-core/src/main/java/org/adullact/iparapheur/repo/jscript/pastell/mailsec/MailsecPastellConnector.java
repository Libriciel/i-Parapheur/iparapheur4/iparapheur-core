package org.adullact.iparapheur.repo.jscript.pastell.mailsec;

/**
 * Créé par lhameury le 18/04/18.
 */
public class MailsecPastellConnector {
    private String id;
    private String serverId;
    private String title;

    public MailsecPastellConnector() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
