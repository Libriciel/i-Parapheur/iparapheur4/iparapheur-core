package org.adullact.iparapheur.repo.jscript.seals;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

/**
 * Pour sérialisation JSON
 *
 * Créé par lhameury le 4/14/17.
 */
@Getter @Setter @NoArgsConstructor
public class SealCertificate {

    private String title;
    private String id;
    private CachetDescription description;
    private String originalName;
    private String certificate;
    private String password;
    private String image;
    private String additionalText;

    public SealCertificate(String title, String certificate, String password, CachetDescription description) {
        this.title = title;
        this.certificate = certificate;
        this.password = password;
        this.description = description;
    }

    /**
     * The password is deserialized from the web interface,
     * but never retrieved from the database.
     * <p>
     * We won't present a classic "getPassword" method.
     * Thus, this one was renamed for disambiguation.
     *
     * @return the certificate password, if the object was deserialized from a web request ; null if the object is from the DB.
     */
    public @Nullable
    String getDeserializedPassword() {
        return password;
    }

    @Getter @Setter @NoArgsConstructor @AllArgsConstructor
    public static class CachetDescription {

        private String alias;
        private String issuerDN;
        private long notAfter;
        private String subjectDN;
    }

    @Override public String toString() {
        return "{SealCertificate id=" + id + " title=" + title + "}";
    }
}
