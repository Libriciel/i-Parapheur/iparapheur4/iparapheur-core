/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, Netheos, ADULLACT-projet
 * Initiated by ADULLACT-projet S.A.
 * Developed by Netheos
 *
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.repo.jscript;

import org.adullact.iparapheur.repo.Collectivite;
import org.adullact.iparapheur.repo.mc.MultiCService;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;
import java.util.Map;

/**
 *
 * @author vbarousse
 */
public class MultiCServiceScriptable extends BaseProcessorExtension implements MultiCService {

    private MultiCService multiCService;

    public void setMultiCService(MultiCService multiCService) {
        this.multiCService = multiCService;
    }

    public boolean isEnabled() {
        return multiCService.isEnabled();
    }

    @Override
    public NodeRef getRootNodeRef(Collectivite c) {
        return multiCService.getRootNodeRef(c);
    }

    public void createCollectivite(Collectivite c, char[] password) {
        multiCService.createCollectivite(c, password);
    }

    public void changeAdminPassword(Collectivite c, char[] newPassword) {
        multiCService.changeAdminPassword(c, newPassword);
    }

    public Collectivite getCollectivite(String tenantDomain) {
        return multiCService.getCollectivite(tenantDomain);
    }

    public List<Collectivite> getCollectivites() {
        return multiCService.getCollectivites();
    }

    public void enableCollectivite(Collectivite c) {
        multiCService.enableCollectivite(c);
    }

    public void disableCollectivite(Collectivite c) {
        multiCService.disableCollectivite(c);
    }

    public void updateCollectivite(Collectivite c) {
        multiCService.updateCollectivite(c);
    }

    public void deleteCollectivite(Collectivite c) {
        multiCService.deleteCollectivite(c);
    }

    public Map<String, String> getInfosPES(Collectivite c) {
        return multiCService.getInfosPES(c);
    }

    public void setInfosPES(Collectivite c, Map<String, String> infosPES) {
        multiCService.setInfosPES(c, infosPES);
    }

    public Map<String, Object> getStats(Collectivite c) {
        return multiCService.getStats(c);
    }

    public void reloadModelsForTenant(String tenant) {
        multiCService.reloadModelsForTenant(tenant);
    }

}
