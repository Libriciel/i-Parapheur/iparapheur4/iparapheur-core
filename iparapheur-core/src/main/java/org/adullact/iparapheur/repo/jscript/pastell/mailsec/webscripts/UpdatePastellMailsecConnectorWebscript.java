package org.adullact.iparapheur.repo.jscript.pastell.mailsec.webscripts;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.adullact.iparapheur.repo.jscript.pastell.mailsec.MailsecPastellConnector;
import org.adullact.iparapheur.repo.jscript.pastell.mailsec.MailsecPastellService;
import org.adullact.iparapheur.repo.jscript.seals.SealCertificate;
import org.adullact.iparapheur.repo.jscript.seals.SealUtils;
import org.alfresco.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;

/**
 * Créé par lhameury le 4/19/17.
 */
public class UpdatePastellMailsecConnectorWebscript extends AbstractWebScript {

    private final MailsecPastellService mailsecPastellService;

    @Autowired
    public UpdatePastellMailsecConnectorWebscript(MailsecPastellService mailsecPastellService) {
        this.mailsecPastellService = mailsecPastellService;
    }

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        final GsonBuilder builder = new GsonBuilder();
        final Gson gson = builder.create();

        mailsecPastellService.update(
                gson.fromJson(webScriptRequest.getContent().getReader(), MailsecPastellConnector.class)
        );

        webScriptResponse.setStatus(200);
        webScriptResponse.setContentEncoding("UTF-8");
    }

}
