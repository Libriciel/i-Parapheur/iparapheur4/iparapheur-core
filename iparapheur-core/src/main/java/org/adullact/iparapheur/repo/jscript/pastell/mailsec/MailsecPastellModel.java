package org.adullact.iparapheur.repo.jscript.pastell.mailsec;

import org.alfresco.service.namespace.QName;

/**
 * Créé par lhameury le 18/04/18.
 */
public interface MailsecPastellModel {

    String PARAPHEUR_MODEL_URI = "http://www.atolcd.com/alfresco/model/parapheur/1.0";

    // Type d'objet pour connecteur pastell mailsec
    QName TYPE_MAILSEC_PASTELL_CONNECTOR = QName.createQName(PARAPHEUR_MODEL_URI, "mailsec-pastell-connector");
    // Type d'objet pour le dossier contenant les connecteurs pastell mailsec
    QName TYPE_MAILSEC_PASTELL_CONNECTORS = QName.createQName(PARAPHEUR_MODEL_URI, "mailsec-pastell-connectors");
    // Identifiant du connecteur sur 'pastell-connector'
    QName PROP_PASTELL_CONNECTOR_SERVER_ID = QName.createQName(PARAPHEUR_MODEL_URI, "pastell-connector-server-id");

}
