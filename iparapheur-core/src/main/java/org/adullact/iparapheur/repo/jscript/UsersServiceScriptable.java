package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.admin.UsersService;
import org.adullact.iparapheur.domain.CertificatesDAO;
import org.adullact.iparapheur.repo.notification.socket.SocketServer;
import org.adullact.iparapheur.util.NativeUtils;
import org.adullact.iparapheur.util.X509Util;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.jscript.BaseScopableProcessorExtension;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.TicketComponent;
import org.alfresco.repo.security.authority.script.ScriptGroup;
import org.alfresco.repo.security.authority.script.ScriptUser;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.alfresco.web.bean.repository.Repository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.NativeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.json.JSONUtils;
import sun.security.provider.X509Factory;

import javax.faces.context.FacesContext;
import javax.transaction.UserTransaction;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;

import static org.adullact.libersign.util.signature.PKCS7VerUtil.pem2der;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;

/**
 *
 * @author Berger Levrault - Mathieu Passenaud
 * @author jmaire
 * @author lhameury
 */
public class UsersServiceScriptable extends BaseScopableProcessorExtension {
    
    private static final Logger log = Logger.getLogger(UsersServiceScriptable.class);
    private enum ADMIN_ROLE {
        admin,
        adminFonctionnel,
        aucun
    }

    @Autowired
    private UsersService usersService;
    @Autowired
    private NodeService nodeService;
    @Autowired
    private TenantService tenantService;
    @Autowired
    private PersonService personService;
    @Autowired
    private NamespaceService namespaceService;
    @Autowired
    private ParapheurService parapheurService;
    @Autowired
    private CertificatesDAO certificatesDAO;
    @Autowired
    private AuthorityService authorityService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private TicketComponent ticketComponent;

    public String createUser(String userName, String email, String firstName, String lastName, String password) {
        return  usersService.createUser(userName, email, firstName, lastName, password);
    }

    public void deleteUser(String id) {
        NodeRef userRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, id);
        if (nodeService.exists(userRef)) {
            usersService.deleteUser(userRef);
        }
    }

    public void export(){
        usersService.export();
    }

    public String getCurrentUsername() {
        return AuthenticationUtil.getRunAsUser();
    }

    public JSONObject getTicketForUser(String user) throws Exception {
        JSONObject ret = new JSONObject();
        Pair<String, String> result;

        result = usersService.getTicketForUser(user);

        ret.put("username", result.getFirst());
        ret.put("ticket", result.getSecond());

        return ret;
    }

    public JSONObject getUserTicketWithOpenId(String bearerToken) throws IOException, JSONException {
        JSONObject ret = new JSONObject();

        ret.put("ticket", usersService.getUserTicketWithOpenId(bearerToken));

        return ret;
    }

    public JSONObject getTicket(String strCert) throws Exception {

        JSONObject ret = new JSONObject();

        Pair<String, String> result = null;

        X509Certificate[] certs = null;

        strCert = strCert.replace('\t', '\n');
        byte[] derbytes = pem2der(strCert.getBytes(), X509Factory.BEGIN_CERT.getBytes(), X509Factory.END_CERT.getBytes());
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(derbytes);
        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        Collection x509col = fact.generateCertificates(byteArrayInputStream);
        if(x509col != null && x509col.size() > 0) {
            certs = new X509Certificate[x509col.size()];
            x509col.toArray(certs);
        }
        result = usersService.getTicketWithCertificate(certs);

        ret.put("username", result.getFirst());
        ret.put("ticket", result.getSecond());

        return ret;
    }

    /**
     * Récupère tous les utilisateurs. Seules les informations
     * basiques sont renvoyés par souci d'efficacité.
     *
     * @return la liste de tous les utilisateurs.
     */
    public JSONArray getUsers(String search) {
        JSONArray users = new JSONArray();
        if ((search != null) && (!search.trim().isEmpty())) {
            List<NodeRef> usersRef = usersService.searchUser(search.trim());
            for (NodeRef user : usersRef) {
                String username = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);
                List<ChildAssociationRef> assocs = nodeService.getParentAssocs(user);
                boolean isFromLdap = false;
                for(ChildAssociationRef assoc : assocs) {
                    NodeRef parent = assoc.getParentRef();
                    if(((String)nodeService.getProperty(parent, ContentModel.PROP_NAME)).contains("AUTH.EXT")) {
                        isFromLdap = true;
                        break;
                    }
                }
                try {
                    users.put(new JSONObject()
                            .put("id", user.getId())
                            .put("username", username)
                            .putOpt("firstName", nodeService.getProperty(user, ContentModel.PROP_FIRSTNAME))
                            .putOpt("lastName", nodeService.getProperty(user, ContentModel.PROP_LASTNAME))
                            .putOpt("email", nodeService.getProperty(user, ContentModel.PROP_EMAIL))
                            .putOpt("metadata", nodeService.getProperty(user, ContentModel.PROP_ORGID))
                            .putOpt("isAdmin", usersService.isAdministrateur(username))
                            .putOpt("isAdminFonctionnel", usersService.isAdministrateurFonctionnel(username))
                            .putOpt("hasCertificate", usersService.hasCertificat(user))
                            .putOpt("isProprietaire", usersService.isProprietaire(username))
                            .putOpt("isSecretaire", usersService.isSecretaire(username))
                            .putOpt("isFromLdap", isFromLdap)
                    );
                } catch (JSONException e) {
                    log.warn("Unable to build User's JSON", e);
                }
            }
        }
        else {
            UserTransaction tx = null;

            try {
                tx = transactionService.getUserTransaction(true);
                tx.begin();
                List<ChildAssociationRef> childRefs = nodeService.getChildAssocs(personService.getPeopleContainer());
                for (ChildAssociationRef ref: childRefs) {

                    NodeRef nodeRef = ref.getChildRef();
                    if (nodeService.getType(nodeRef).equals(ContentModel.TYPE_PERSON)) {
                        String username = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_USERNAME);
                        if ((!"guest".equals(tenantService.getBaseNameUser(username))) &&
                                (!"System".equals(tenantService.getBaseNameUser(username) ))) { // On enlève "guest" et "System" des résultats

                            List<ChildAssociationRef> assocs = nodeService.getParentAssocs(nodeRef);
                            boolean isFromLdap = false;
                            for(ChildAssociationRef assoc : assocs) {
                                NodeRef parent = assoc.getParentRef();
                                if(((String)nodeService.getProperty(parent, ContentModel.PROP_NAME)).contains("AUTH.EXT")) {
                                    isFromLdap = true;
                                    break;
                                }
                            }
                            users.put(new JSONObject()
                                    .put("id", nodeRef.getId())
                                    .put("username", username)
                                    .putOpt("firstName", nodeService.getProperty(nodeRef, ContentModel.PROP_FIRSTNAME))
                                    .putOpt("lastName", nodeService.getProperty(nodeRef, ContentModel.PROP_LASTNAME))
                                    .putOpt("email", nodeService.getProperty(nodeRef, ContentModel.PROP_EMAIL))
                                    .putOpt("metadata", nodeService.getProperty(nodeRef, ContentModel.PROP_ORGID))
                                    .putOpt("isAdmin", usersService.isAdministrateur(username))
                                    .putOpt("isAdminFonctionnel", usersService.isAdministrateurFonctionnel(username))
                                    .putOpt("hasCertificate", usersService.hasCertificat(nodeRef))
                                    .putOpt("isProprietaire", usersService.isProprietaire(username))
                                    .putOpt("isSecretaire", usersService.isSecretaire(username))
                                    .putOpt("isFromLdap", isFromLdap)
                            );
                        }
                    }
                }
                tx.commit();
            } catch (Exception err) {
                    log.warn("Unable to build User's JSON ", err);
                try { if (tx != null) {tx.rollback();} } catch (Exception tex) {
                    log.warn("Unable to rollback transaction", tex);
                }
            }
        }
        return users;
    }

    public void removeUserFromGroup(String id, String group) {
        NodeRef userRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, id);
        if (nodeService.exists(userRef)) {
            try {
                String username = (String) nodeService.getProperty(userRef, ContentModel.PROP_USERNAME);
                authorityService.removeAuthority("GROUP_" + group, username);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public JSONObject getUser(String id) {
        JSONObject user = new JSONObject();
        NodeRef userRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, id);
        if (nodeService.exists(userRef)) {
            try {
                String username = (String) nodeService.getProperty(userRef, ContentModel.PROP_USERNAME);

                List<ChildAssociationRef> assocs = nodeService.getParentAssocs(userRef);
                boolean isFromLdap = false;
                for(ChildAssociationRef assoc : assocs) {
                    NodeRef parent = assoc.getParentRef();
                    if(((String)nodeService.getProperty(parent, ContentModel.PROP_NAME)).contains("AUTH.EXT")) {
                        isFromLdap = true;
                        break;
                    }
                }

                user.put("id", userRef.getId())
                    .put("username", username)
                    .put("firstName", nodeService.getProperty(userRef, ContentModel.PROP_FIRSTNAME))
                    .put("lastName", nodeService.getProperty(userRef, ContentModel.PROP_LASTNAME))
                    .put("email", nodeService.getProperty(userRef, ContentModel.PROP_EMAIL))
                    .put("metadata", nodeService.getProperty(userRef, ContentModel.PROP_ORGID))
                    .putOpt("isFromLdap", isFromLdap);

                if (usersService.isAdministrateur(username)) {
                    user.put("admin", ADMIN_ROLE.admin.toString());
                }
                else if (usersService.isAdministrateurFonctionnel(username)) {
                    user.put("admin", ADMIN_ROLE.adminFonctionnel.toString());
                }
                else {
                    user.put("admin", ADMIN_ROLE.aucun.toString());
                }

                String signatureId = usersService.getSignature(userRef);
                if (signatureId != null) {
                    user.put("signature", signatureId);
                }

                Set<String> authorities = authorityService.getAuthoritiesForUser(username);
                Set<String> groups = new TreeSet<String>();

                for(String authority : authorities) {
                    if(!authority.contains("ROLE") && !authority.equals("GROUP_EVERYONE")) {
                        ScriptGroup scriptGroup = new ScriptGroup(authority, authorityService);
                        List<ScriptUser> users = new ArrayList<ScriptUser>(Arrays.asList(scriptGroup.getChildUsers()));
                        for(ScriptUser scriptUser : users) {
                            if(scriptUser.getShortName().equals(username)) {
                                groups.add(authority.replace("GROUP_", ""));
                            }
                        }
                    }
                }

                user.put("groups", groups);

                String idCertificat = (String) nodeService.getProperty(userRef, ParapheurModel.PROP_ID_CERTIFICAT);
                if (idCertificat != null) {
                    String x509Certificat = usersService.getCertificat(userRef);
                    X509Certificate cert = X509Util.getX509CertificateFromString(x509Certificat);
                    if (cert != null) {
                        user.put("certificat", new JSONObject(X509Util.getUsefulCertProps(cert))
                                .put("id", idCertificat));
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    /**
     * get user's information from his username
     * @param username
     * @return JSON string containing the user's information
     */

    public HashMap<String, String> getUserFromUserName(String username) {
        HashMap<String, String> result = new HashMap<String, String>();
        NodeRef nodeRef = personService.getPerson(username, false);
        try{
            result.put("id", nodeRef.toString());
            result.put("userName", username);

            result.put("firstName", (String) nodeService.getProperty(nodeRef, ContentModel.PROP_FIRSTNAME));

            result.put("lastName", (String) nodeService.getProperty(nodeRef, ContentModel.PROP_LASTNAME));

            result.put("email", (String) nodeService.getProperty(nodeRef, ContentModel.PROP_EMAIL));
        }catch(Exception ex){
            // l'exception n'arrive que si l'utilisateur n'existe pas, c'est entièrement voulu de renvoyer une exception jusque dans l'API
        }

        return result;
    }

    public JSONArray getBureaux(String userId) {
        JSONArray bureaux = new JSONArray();
        NodeRef userRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, userId);
        if (nodeService.exists(userRef)) {
            String username = (String) nodeService.getProperty(userRef, ContentModel.PROP_USERNAME);
            for (NodeRef bureauRef : usersService.getBureauxProprietaire(username)) {
                try {
                    bureaux.put(new JSONObject()
                            .put("id", bureauRef.getId())
                            .put("title", nodeService.getProperty(bureauRef, ContentModel.PROP_TITLE))
                            .put("isProprietaire", true)
                            .put("isSecretaire", false));
                } catch (JSONException e) {
                    // Ne rien faire, le bureau ne sera pas renvoyé.
                }
            }
            for (NodeRef bureauRef : usersService.getBureauxSecretaire(username)) {
                try {
                    bureaux.put(new JSONObject()
                            .put("id", bureauRef.getId())
                            .put("title", nodeService.getProperty(bureauRef, ContentModel.PROP_TITLE))
                            .put("isProprietaire", false)
                            .put("isSecretaire", true));
                } catch (JSONException e) {
                    // Ne rien faire, le bureau ne sera pas renvoyé.
                }
            }
        }
        return bureaux;
    }

    public JSONArray getBureauxAdministres(String userId) {
        JSONArray bureaux = new JSONArray();
        NodeRef userRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, userId);
        if (nodeService.exists(userRef)) {
            String username = (String) nodeService.getProperty(userRef, ContentModel.PROP_USERNAME);
            for (NodeRef bureauRef : parapheurService.getAllManagedParapheursByOpAdmin(username)) {
                bureaux.put(bureauRef.getId());
            }
        }
        return bureaux;
    }

    public void removeUserFromBureau(String userId, String bureauId, boolean isProprietaire) {
        NodeRef user = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, userId);
        NodeRef bureau = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, bureauId);
        if (nodeService.exists(user) && nodeService.exists(bureau)) {
            usersService.removeUserFromBureau(user, bureau, isProprietaire);
        }
    }

    public void addUserToBureau(String userId, String bureauId, boolean isProprietaire) {
        NodeRef user = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, userId);
        NodeRef bureau = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, bureauId);
        if (nodeService.exists(user) && nodeService.exists(bureau)) {
            usersService.addUserToBureau(user, bureau, isProprietaire);
        }
    }

    public boolean isAdministrateur(String username) {
        return usersService.isAdministrateur(username);
    }

    public void updateUser(String userId, NativeObject json, NativeObject propNative) {

        NodeRef userRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, userId);
        if (nodeService.exists(userRef)) {
            NativeObjectMapAdapter jsonData = new NativeObjectMapAdapter(json);
            NativeObjectMapAdapter properties = new NativeObjectMapAdapter(propNative);

            HashMap<QName, Serializable> propertiesMap = new HashMap<QName, Serializable>();

            propertiesMap.put(ContentModel.PROP_MODIFIED, new Date());

            String adminRole = null;
            String signatureB64 = null;
            boolean deleteSignature = false;
            String certificatB64 = null;
            boolean deleteCertificat = false;
            List<String> bureauxAdministres = null;

            for (Object key : jsonData.keySet()) {

                Object prop = properties.get(key);
                Object value = jsonData.get(key);

                if (prop != null) {
                    propertiesMap.put(QName.createQName((String) prop, namespaceService), (Serializable) value);
                }
                else {
                    if (key.equals("admin")) {
                        adminRole = (String) value;
                    }
                    else if (key.equals("bureauxAdministres")) {
                        NativeArray bureauxAdministresNative = (NativeArray) value;
                        if ((bureauxAdministresNative != null)) {
                            bureauxAdministres = NativeUtils.nativeArrayToString(bureauxAdministresNative);
                        }
                    }
                    else if (key.equals("signatureData")) {
                        signatureB64 = (String) value;
                    }
                    else if (key.equals("signature")) {
                        deleteSignature = ((String) value).isEmpty();
                    }
                    else if (key.equals("certificat")) {
                        NativeObjectMapAdapter certData = new NativeObjectMapAdapter((NativeObject) value);
                        deleteCertificat = certData.isEmpty();
                        if (!deleteCertificat) {
                            if (certData.containsKey("content")) {
                                certificatB64 = (String) certData.get("content");
                            }
                        }
                    }
                }
            }
            usersService.updateUser(userRef, propertiesMap);

            // GESTION DES DROITS D'ADMINISTRATION
            if ((adminRole != null)) {
                String username = (String) nodeService.getProperty(userRef, ContentModel.PROP_USERNAME);
                // Purger les droits pour ne pas faire cohabiter un rôle d'admin et d'admin fonctionnel.
                removeAdminRole(username);

                if (adminRole.equals(ADMIN_ROLE.adminFonctionnel.name())) {
                    if (bureauxAdministres != null) {
                        setAdminFonctionnel(username, bureauxAdministres);
                    }
                } else if (adminRole.equals(ADMIN_ROLE.admin.name())) {
                    addToAdminGroup(username);
                }
            }
            // Si on a juste changé les bureaux à administrer ET qu'on est bien déjà admin fonctionnel
            else {
                String username = (String) nodeService.getProperty(userRef, ContentModel.PROP_USERNAME);
                if ((bureauxAdministres != null) && (usersService.isAdministrateurFonctionnel(username))) {
                    setAdminFonctionnel(username, bureauxAdministres);
                }
            }

            if (signatureB64 != null) {
                usersService.setSignature(userRef, signatureB64);
            } else if (deleteSignature) {
                usersService.deleteSignature(userRef);
            }

            if (deleteCertificat) {
                usersService.deleteCertificat(userRef);
            }
            else if (certificatB64 != null) {
                usersService.setCertificat(userRef, certificatB64);
            }
        }
    }

    private void removeAdminRole(String username) {
        if (usersService.isAdministrateur(username)) {
            usersService.removeFromAdminGroup(username);
        }
        if (usersService.isAdministrateurFonctionnel(username)) {
            // Suppression des droits d'administration fonctionnels en passant une liste de bureaux vide (ou null).
            parapheurService.setPHAdminAuthoritiesForUser(username, null);
        }
    }

    public boolean isAdminFonctionnel(String username) {
        return usersService.isAdministrateurFonctionnel(username);
    }

    public boolean isGestionnaireCircuit(String username) {
        return usersService.isGestionnaireCircuit(username);
    }

    private void addToAdminGroup(String username) {
        usersService.addToAdminGroup(username);
    }

    private void setAdminFonctionnel(String username, List<String> bureauxId) {
        parapheurService.setPHAdminAuthoritiesForUser(username, bureauxId);
    }

    public String getUsername(String id) {
        NodeRef userRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, id);
        String username = null;
        if (nodeService.exists(userRef)) {
            username = (String) nodeService.getProperty(userRef, ContentModel.PROP_USERNAME);
        }
        return username;
    }

    public List<String> getConnectedUsers() {
        return new ArrayList<String>(ticketComponent.getUsersWithTickets(true));
    }

    public int resetPasswordRequest(String username, String email) {
        return usersService.resetPasswordRequest(username, email);
    }

    public int resetPassword(String username, String uuid, String password) {
        return usersService.resetPassword(username, uuid, password);
    }
}
