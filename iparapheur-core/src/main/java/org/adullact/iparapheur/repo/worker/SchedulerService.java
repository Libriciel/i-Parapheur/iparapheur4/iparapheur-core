package org.adullact.iparapheur.repo.worker;

import com.atolcd.parapheur.repo.DossierService;
import org.adullact.iparapheur.repo.worker.impl.WorkerServiceImpl;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.extensions.surf.util.AbstractLifecycleBean;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class SchedulerService extends AbstractLifecycleBean {

    private final String subsystem = "i-Parapheur --- Scheduler";

    @Autowired
    private WorkerServiceImpl workerService;

    //Executeur de tâches, géré par spring
    private ConcurrentTaskExecutor taskExecutor;

    //Id de noeuds en transaction
    private List<String> idInTransaction = new CopyOnWriteArrayList<String>();

    //Actions en cours
    private static ConcurrentMap<String, Map> currentActions = new ConcurrentHashMap<String, Map>();

    private static ConcurrentMap<String, Future> currentFuture = new ConcurrentHashMap<String, Future>();

    //Fichier de conf
    private int threadPool;
    //---

    private Semaphore sem;                  // Sémaphore pour actions générale
    private Semaphore signSem;              // Limitée à 1 pour éviter la signature de PES en simultané (pour ne pas écrouler le serveur)
    private Semaphore tdtSem;               // Limitée à 1 pour éviter l'envoi de PES en simultané (pour ne pas écrouler le serveur)
    private Semaphore transSem;             // Sémaphore pour transformation PDF, limitée à 1 pour éviter d'écrouler le serveur
    private Semaphore signInfoSem;          // Sémaphore pour signInfo, limitée à 1 pour éviter d'écrouler le serveur

    private String idInSignature;           // Id du dossier en cours de signature
    private String idInTdt;                 // Id du dossier en cours de télétransmission
    private String idInTrans;               // Id du document en cours de transformation
    private String idInSignInfo;            // Id du document en cours de transformation

    private static Logger logger = Logger.getLogger(SchedulerService.class);

    private static final String ACTION = "action";

    public ConcurrentMap getCurrentActions() {
        return currentActions;
    }

    /**
     * Fonction appellé à chaque début d'execution de job
     *
     * @param id id du noeud traité
     */
    public void startExecution(String id) {
        //TODO
    }

    /**
     * Fonction appellé à chaque fin d'execution de job
     *
     * @param id id du noeud traité
     */
    public void stopExecution(String id) {
        if (id != null) {
            idInTransaction.remove(id);
            currentActions.remove(id);
            currentFuture.remove(id);

            if (id.equals(idInSignature)) {
                idInSignature = null;
                signSem.release();
            } else if (id.equals(idInTdt)) {
                idInTdt = null;
                tdtSem.release();
            } else if (id.equals(idInTrans)) {
                idInTrans = null;
                transSem.release();
            } else if (id.equals(idInSignInfo)) {
                idInSignInfo = null;
                signInfoSem.release();
            }
        }
        sem.release();
        logger.debug("Semaphore released!");
    }

    public boolean cancelWorker(String id) {
        return !currentFuture.get(id).isDone() && currentFuture.get(id).cancel(true);
    }

    public void selectWorker(Map request) throws JSONException {
        String type = (String) request.get(WorkerService.TYPE);
        DossierService.ACTION_DOSSIER action = DossierService.ACTION_DOSSIER.valueOf((String) request.get(ACTION));
        if(WorkerService.TYPE_EVENT.equals(type)) {
            taskExecutor.submit(workerService.getHandler(request));
        } else if (WorkerService.TYPE_DOSSIER.equals(type) || WorkerService.TYPE_DOCUMENT.equals(type)) {
            String id = (String) request.get(WorkerService.ID);

            // (id == null) peut arriver pour les mails direct par exemple
            if ((id == null) || !idInTransaction.contains(id) || action.equals(DossierService.ACTION_DOSSIER.SIGN_INFO)) {
                if (id != null) {
                    try {
                        //Si il s'agit d'une signature, traitement en série
                        if (action.equals(DossierService.ACTION_DOSSIER.SIGNATURE)) {
                            logger.debug("Attente du semaphore de signature");
                            signSem.acquire();
                            idInSignature = id;
                        } else if (action.equals(DossierService.ACTION_DOSSIER.TDT_ACTES) ||
                                   action.equals(DossierService.ACTION_DOSSIER.TDT_HELIOS)) {
                            logger.debug("Attente du semaphore de Télétransmission");
                            tdtSem.acquire();
                            idInTdt = id;
                        } else if (action.equals(DossierService.ACTION_DOSSIER.TRANSFORM)) {
                            logger.debug("Attente du semaphore de Transformation");
                            transSem.acquire();
                            idInTrans = id;
                        } else if (action.equals(DossierService.ACTION_DOSSIER.SIGN_INFO)) {
                            logger.debug("Attente du semaphore de SignInfo");
                            signInfoSem.acquire();
                            idInSignInfo = id;
                        }
                        logger.debug("Attente du semaphore");
                        sem.acquire();
                        logger.debug("Semaphore ok !");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    idInTransaction.add(id);
                    request.put("date", new Date().getTime());
                    currentActions.putIfAbsent(id, request);
                    currentFuture.putIfAbsent(id, taskExecutor.submit(workerService.getHandler(request)));
                } else {
                    taskExecutor.submit(workerService.getHandler(request));
                }
            } else {
                logger.error("----Consumer : " + "id " + id + " déjà en cours de traitement. CECI NE DEVRAIT PAS ARRIVER !!!");
            }
        }
    }

    public void setThreadPoolString(String threadPool) {
        this.threadPool = 1;
        if (threadPool != null) {
            this.threadPool = Integer.parseInt(threadPool);
        }
    }

    /**
     * Initialisation du Pool de Threads (géré par Spring)
     */
    @Override
    protected void onBootstrap(ApplicationEvent applicationEvent) {
        logger.setLevel(Level.INFO);
        logger.info("Starting '" + subsystem + "' subsystem");
        //Initialisation du taskExecutor avec les paramètres passés en fichier de configuration
        if (taskExecutor == null) {
            /* Low priority ThreadFactory */
            ThreadFactory threadFactory = new ThreadFactory() {
                public String PH_JOB_PREFIX = "IP_Job_Queue";
                private int jobId = 1;

                @Override
                public Thread newThread(@NotNull Runnable runnable) {
                    Thread nt = new Thread(runnable);
                    nt.setPriority(Thread.MIN_PRIORITY);
                    nt.setName(PH_JOB_PREFIX + "-" + jobId);
                    jobId++;
                    nt.setDaemon(true);
                    return nt;
                }
            };

            ThreadPoolExecutor executor = new ThreadPoolExecutor(threadPool,
                                                                 threadPool,
                                                                 0L,
                                                                 TimeUnit.MILLISECONDS,
                                                                 new LinkedBlockingQueue<Runnable>(),
                                                                 threadFactory);
            taskExecutor = new ConcurrentTaskExecutor(executor);
            sem = new Semaphore(threadPool);
            signSem = new Semaphore(1);
            tdtSem = new Semaphore(1);
            transSem = new Semaphore(1);
            signInfoSem = new Semaphore(1);
        }
        logger.info("Startup of '" + subsystem + "' subsystem complete");
    }

    @Override
    protected void onShutdown(ApplicationEvent applicationEvent) {
        try {
            logger.info("Stopping '" + subsystem + "' subsystem");
            logger.info("Requesting '" + subsystem + "' subsystem semaphore");
            sem.acquire(threadPool);
            logger.info("Semaphore for '" + subsystem + "' subsystem locked");
        } catch (InterruptedException e) {
            logger.error("There was task(s) in '" + subsystem + "' subsystem");
            //FIXME: How to Handle that ?
        }
        logger.info("Stopped '" + subsystem + "' subsystem");
    }
}
