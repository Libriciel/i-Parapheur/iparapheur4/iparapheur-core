package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.repo.JobService;
import com.atolcd.parapheur.repo.NotificationCenter;
import com.atolcd.parapheur.repo.ParapheurUserPreferences;
import com.atolcd.parapheur.repo.impl.DigestQuartzJobServiceImpl;
import org.adullact.iparapheur.repo.notification.Notification;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.jscript.BaseScopableProcessorExtension;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.transaction.TransactionService;
import org.mozilla.javascript.NativeObject;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.transaction.UserTransaction;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 18/07/12
 * Time: 11:38
 * To change this template use File | Settings | File Templates.
 */

public class NotificationCenterScriptable extends BaseScopableProcessorExtension {

    @Autowired
    private NotificationCenter notificationCenter;

    @Autowired
    private DigestQuartzJobServiceImpl digestQuartzJobService;

    @Qualifier("tenantService")
    @Autowired
    private TenantService tenantService;

    @Qualifier("transactionService")
    @Autowired
    private TransactionService transactionService;

    @Qualifier("personService")
    @Autowired
    private PersonService personService;

    @Qualifier("nodeService")
    @Autowired
    private NodeService nodeService;

    @Autowired
    private JobService jobService;

    @Autowired
    private ParapheurUserPreferences parapheurUserPreferences;

    public List<Notification> getUnreadNotifications() {
        String username = AuthenticationUtil.getRunAsUser();

        List<Notification> retVal = notificationCenter.getUnreadNotificationsForUser(username);

        return retVal;
    }

    public void clearNotifications() {
        String username = AuthenticationUtil.getRunAsUser();
        notificationCenter.clearNotifications(username);
    }

    public void postNotification(Notification notification) {
        notificationCenter.enableNotificationsForUser(AuthenticationUtil.getRunAsUser());
        notificationCenter.postNotification(AuthenticationUtil.getRunAsUser(),
                notification);
    }

    public void enableNotificationsForUser(String username) {
        notificationCenter.enableNotificationsForUser(username);
    }

    public void disableNotificationsForUser(String username) {
        notificationCenter.disableNotificationsForUser(username);
    }

    /**
     * This method is synchronized to avoid two threads to reschedule at the same time
     */
    public synchronized void rescheduleMailJob() {
        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {

            @Override
            public Object doWork() throws Exception {
                
                UserTransaction utx = transactionService.getNonPropagatingUserTransaction();
                try {
                    utx.begin();


                    Set<NodeRef> people = personService.getAllPeople();
                    List<String> users = new ArrayList<String>();

                    /* collect users */

                    for (NodeRef person : people) {
                        String username = (String) nodeService.getProperty(person, ContentModel.PROP_USERNAME);
                        if (parapheurUserPreferences.isDailyDigestEnabledForUsername(username)) {
                            users.add(username);
                        }
                    }

                    try {
                        digestQuartzJobService.unscheduleCronJobForUsers(users);
                    } catch (SchedulerException e) {
                        e.printStackTrace();
                    }

                    /* Schedule Jobs */
                    digestQuartzJobService.scheduleCronJobForUsers(users);

                    utx.commit();
                } catch (Exception e) {

                    e.printStackTrace();
                    try {
                        utx.rollback();
                    } catch (Exception ex) {
                    }
                }
                return null;
            }


        }, AuthenticationUtil.getAdminUserName());
    }


    public void enableNotifications() {
        notificationCenter.enableNotificationsForUser(AuthenticationUtil.getRunAsUser());
    }

    public void disableNotifications() {
        notificationCenter.disableNotificationsForUser(AuthenticationUtil.getRunAsUser());
    }
    
    public void enableDailyDigest() {
        parapheurUserPreferences.enableDailyDigestForUsername(AuthenticationUtil.getRunAsUser());
    }

    public void disableDailyDigest() {
        parapheurUserPreferences.disableDailyDigestForUsername(AuthenticationUtil.getRunAsUser());
    }

    public void setCronString(String cronString) {
        parapheurUserPreferences.setDigestCronForUsername(AuthenticationUtil.getRunAsUser(), cronString);
    }

    public boolean isDailyDigestEnabled() {
        return parapheurUserPreferences.isDailyDigestEnabledForUsername(AuthenticationUtil.getRunAsUser());
    }
    
    public boolean isNotificationEnabled() {
        return parapheurUserPreferences.isNotificationsEnabledForUsername(AuthenticationUtil.getRunAsUser());
    }
    
    public String getDigestCron() {
        String cron = parapheurUserPreferences.getDigestCronForUsername(AuthenticationUtil.getRunAsUser());
        return (cron == null)? digestQuartzJobService.getDefaultCronString() : cron;
    }

    /**
     * Set the frequency for notifications (i.e DIGEST_CRON_PREFERENCE).
     * The value of the cron string can be, depending of the parameter "mode" :
     *     - "0 0 0/X * * ?" (hourly),
     *     - "0 0 X * * ?" (daily),
     *     - "0 0 7 * * X" (weekly),
     * with X the value of the parameter "value".
     * The mode can also be :
     *     - none if notifications are disabled (never send emails)
     *     - always for direct emails (no digest)
     * @param username
     * @param mode
     * @param value 
     */
    public String setFrequency(String username, String mode, String value) {
        // Ne jamais recevoir d'emails (digest ou pas)
        String cronString = null;
        if (NotificationCenter.VALUE_FREQUENCY_NEVER.equals(mode)) {
            parapheurUserPreferences.disableDailyDigestForUsername(username);
            parapheurUserPreferences.disableNotificationsForUsername(username);
        }
        // Seulement des mails unitaires (pas de digest donc)
        else if (NotificationCenter.VALUE_FREQUENCY_ALWAYS.equals(mode)) {
            parapheurUserPreferences.disableDailyDigestForUsername(username);
            parapheurUserPreferences.enableNotificationsForUsername(username);

        }
        // Notifications groupées
        else {
            cronString = "0 0 8 * * ?"; // Default to 8am each days
            parapheurUserPreferences.enableDailyDigestForUsername(username);
            parapheurUserPreferences.enableNotificationsForUsername(username);

            if (NotificationCenter.VALUE_FREQUENCY_HOURLY.equals(mode)) {
                cronString = "0 0 0/" + value + " * * ?";
            }
            else if (NotificationCenter.VALUE_FREQUENCY_DAILY.equals(mode)) {
                cronString = "0 0 " + value + " * * ?";
            }
            else if (NotificationCenter.VALUE_FREQUENCY_WEEKLY.equals(mode)) {
                cronString = "0 0 8 ? * " + value;
            }
            parapheurUserPreferences.setDigestCronForUsername(username, cronString);
        }
        return cronString;
    }
    
    /**
     * Return the frequency mode of the notification mail for the current user.
     * The value can be either
     * VALUE_FREQUENCY_HOURLY, VALUE_FREQUENCY_DAILY, VALUE_FREQUENCY_WEEKLY
     * (theese values are defined in the NotificationCenter class).
     * @return the frequency mode of the digest mail of the current user.
     */
    public String getFrequencyMode() {
        String cronString = getDigestCron();
        String mode = NotificationCenter.VALUE_FREQUENCY_DAILY;
        if (cronString.endsWith("?")) {
            if (cronString.charAt(5) == '/') {
                mode = NotificationCenter.VALUE_FREQUENCY_HOURLY;
            }
        }
        else {
            mode = NotificationCenter.VALUE_FREQUENCY_WEEKLY;
        }
        return mode;
    }
    
    /**
     * Return the value associated with the frequency mode of the notification
     * mail of the current user.
     * For example, if the frequency mode is "daily", the value returned is the
     * hour at with the mail is sent.
     */
    public int getFrequencyValue() {
        String cronString = getDigestCron();
        int value;
        if (cronString.endsWith("?")) {
            if (cronString.charAt(5) == '/') {
                value = Integer.parseInt(cronString.substring(6, 8).trim());
            }
            else {
                value = Integer.parseInt(cronString.substring(4, 6).trim());
            }
        }
        else {
            value = Integer.parseInt(cronString.substring(10, 11));
        }
        return value;
    }

    public String getNotificationMail() {
        String[] notificationMail = parapheurUserPreferences.getNotificationMailForUsername(AuthenticationUtil.getRunAsUser());
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < notificationMail.length; i++) {
            if (i > 0) {
                result.append(";");
            }
            result.append(notificationMail[i]);
        }

        return result.toString();
    }
    
    private void setNotificationMail(String username, String mail) {
        parapheurUserPreferences.setNotificationMailForUsername(username, mail);
    }

    public void setNotificationsPreferences(String userId, NativeObject json) throws Exception {
        NativeObjectMapAdapter jsonData = new NativeObjectMapAdapter(json);
        final String email = (String) jsonData.get("mail");
        final String mode = (String) jsonData.get("mode");
        final String frequency = (String) jsonData.get("frequency");
        NodeRef user = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, userId);

        if (nodeService.exists(user)) {
            final String username = (String) nodeService.getProperty(user, ContentModel.PROP_USERNAME);

            AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {

                @Override
                public Object doWork() throws Exception {
                    UserTransaction tx = transactionService.getUserTransaction();
                    try {
                        tx.begin();
                        if (email != null) {
                            setNotificationMail(username, email);
                        }

                        // On arrete le job pour cet utilisateur dans tous les cas.
                        digestQuartzJobService.unscheduleCronJobForUser(username);

                        // Si on repasse en mail unitaire, on envoie un mail groupé contenant les notifications non lues
                        // doit être fait avant de changer les préférences utilisateurs (vérification dans le job).
                        // On ne les envoie pas si on a désactiver les notifications.
                        if (NotificationCenter.VALUE_FREQUENCY_ALWAYS.equals(mode)) {
                            digestQuartzJobService.sendDigestMailToUser(username);
                        }

                        String cronString = setFrequency(username, mode, frequency);

                        // On fait repartir le job si on a enregistré un cron
                        if (cronString != null) {
                            digestQuartzJobService.scheduleCronJobForUser(username, cronString);
                        }
                        tx.commit();
                    } catch (Exception e) {
                        try {
                            if (tx != null) {
                                tx.rollback();
                            }
                        } catch (Exception tex) {
                        }
                        throw new RuntimeException(e);
                    }
                    return null;
                }
            },AuthenticationUtil.getAdminUserName());
        }
    }
}
