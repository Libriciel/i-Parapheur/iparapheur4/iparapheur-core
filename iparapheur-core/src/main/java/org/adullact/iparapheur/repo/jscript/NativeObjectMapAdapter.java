package org.adullact.iparapheur.repo.jscript;

import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.Undefined;
import org.mozilla.javascript.Wrapper;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 31/08/12
 * Time: 10:42
 * To change this template use File | Settings | File Templates.
 */
public class NativeObjectMapAdapter implements Map {
    private NativeObject internalObject;

    public NativeObjectMapAdapter(NativeObject object) {
        internalObject = object;
    }

    @Override
    public int size() {
        Object[] ids = internalObject.getIds();
        return ids.length;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean containsKey(Object key) {
        if (key instanceof String) {
            return internalObject.has((String) key, internalObject);
        } else if (key instanceof Number) {
            return internalObject.has(((Number) key).intValue(), internalObject);
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        for (Object obj : values()) {
            if (value == obj ||
                    value != null && value.equals(obj)) {
                return true;
            }
        }
        return false;    }

    @Override
    public Object get(Object key) {
        Object value = null;
        if (key instanceof String) {
            value = internalObject.get((String) key, internalObject);
        } else if (key instanceof Number) {
            value = internalObject.get(((Number) key).intValue(), internalObject);
        }
        if (value == Scriptable.NOT_FOUND || value == Undefined.instance) {
            return null;
        } else if (value instanceof Wrapper) {
            return ((Wrapper) value).unwrap();
        } else {
            return value;
        }
    }

    @Override
    public Object put(Object o, Object o1) {
        throw new UnsupportedOperationException("put");
    }

    @Override
    public Object remove(Object key) {
        Object value = get(key);
        if (key instanceof String) {
            internalObject.delete((String) key);
        } else if (key instanceof Number) {
            internalObject.delete(((Number) key).intValue());
        }
        return value;
    }

    @Override
    public void putAll(Map map) {
        throw new UnsupportedOperationException("putAll");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("clear");
    }

    @Override
    public Set keySet() {
        return new KeySet();
    }

    @Override
    public Collection values() {
        return new ValueCollection();
    }

    @Override
    public Set entrySet() {
        return new EntrySet();
    }

    class KeySet extends AbstractSet<Object> {

        @Override
        public boolean contains(Object key) {
            return containsKey(key);
        }

        @Override
        public Iterator<Object> iterator() {
            return new Iterator<Object>() {
                Object[] ids = internalObject.getIds();
                Object key;
                int index = 0;

                public boolean hasNext() {
                    return index < ids.length;
                }

                public Object next() {
                    try {
                        return (key = ids[index++]);
                    } catch(ArrayIndexOutOfBoundsException e) {
                        key = null;
                        throw new NoSuchElementException();
                    }
                }

                public void remove() {
                    if (key == null) {
                        throw new IllegalStateException();
                    }
                    NativeObjectMapAdapter.this.remove(key);
                    key = null;
                }
            };
        }

        @Override
        public int size() {
            return NativeObjectMapAdapter.this.size();
        }
    }

    class ValueCollection extends AbstractCollection<Object> {

        @Override
        public Iterator<Object> iterator() {
            return new Iterator<Object>() {
                Object[] ids = internalObject.getIds();
                Object key;
                int index = 0;

                public boolean hasNext() {
                    return index < ids.length;
                }

                public Object next() {
                    return get((key = ids[index++]));
                }

                public void remove() {
                    if (key == null) {
                        throw new IllegalStateException();
                    }
                    NativeObjectMapAdapter.this.remove(key);
                    key = null;
                }
            };
        }

        @Override
        public int size() {
            return NativeObjectMapAdapter.this.size();
        }
    }

    class EntrySet extends AbstractSet<Entry<Object, Object>> {
        @Override
        public Iterator<Entry<Object, Object>> iterator() {
            return new Iterator<Map.Entry<Object, Object>>() {
                Object[] ids = internalObject.getIds();
                Object key = null;
                int index = 0;

                public boolean hasNext() {
                    return index < ids.length;
                }

                public Map.Entry<Object, Object> next() {
                    final Object ekey = key = ids[index++];
                    final Object value = get(key);
                    return new Map.Entry<Object, Object>() {
                        public Object getKey() {
                            return ekey;
                        }

                        public Object getValue() {
                            return value;
                        }

                        public Object setValue(Object value) {
                            throw new UnsupportedOperationException();
                        }

                        public boolean equals(Object other) {
                            if (!(other instanceof Map.Entry)) {
                                return false;
                            }
                            Map.Entry e = (Map.Entry) other;
                            return (ekey == null ? e.getKey() == null : ekey.equals(e.getKey()))
                                    && (value == null ? e.getValue() == null : value.equals(e.getValue()));
                        }

                        public int hashCode() {
                            return (ekey == null ? 0 : ekey.hashCode()) ^
                                    (value == null ? 0 : value.hashCode());
                        }

                        public String toString() {
                            return ekey + "=" + value;
                        }
                    };
                }

                public void remove() {
                    if (key == null) {
                        throw new IllegalStateException();
                    }
                    NativeObjectMapAdapter.this.remove(key);
                    key = null;
                }
            };
        }

        @Override
        public int size() {
            return NativeObjectMapAdapter.this.size();
        }
    }
}
