/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.repo.attest;

import javax.transaction.UserTransaction;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.util.Assert;

/**
 *
 * @author Lukas Hameury
 */
public class GetAttestJob implements Job {

    public static interface K { // BLEX
        String infoMessage="infoMessage";
    }
    private static Logger logger = Logger.getLogger(GetAttestJob.class);

    @Override
    public void execute(final JobExecutionContext ctx) throws JobExecutionException {
        JobDataMap dataMap = ctx.getJobDetail().getJobDataMap();

        final AttestService attestService = (AttestService) dataMap.get("attestService");
        final TransactionService transactionService = (TransactionService) dataMap.get("transactionService");
        final NodeRef document = (NodeRef) dataMap.get("document");
        String runAs = (String) dataMap.get("runAs");
        runAs = runAs != null ? runAs : "system";

        Assert.notNull(attestService, "s2lowService is mandatory");
        Assert.notNull(transactionService, "transactionService is mandatory");
        Assert.notNull(document, "document is mandatory");
        // BLEX
        if (dataMap.get(K.infoMessage)!=null) { logger.info (dataMap.get(K.infoMessage)); }

        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {

            @Override
            public Object doWork() throws Exception {
                UserTransaction utx = transactionService.getNonPropagatingUserTransaction();
                try {
                    utx.begin();

                    attestService.getAttest(document);

                    utx.commit();
                } catch (Exception ex) {
                    logger.warn("Error retrieving attest status. Retrying in 5 minutes", ex);
                    // Déjà affichée avec le warning
                    // ex.printStackTrace();
                    utx.rollback();
                }
                return null;
            }

        }, runAs);
    }

}
