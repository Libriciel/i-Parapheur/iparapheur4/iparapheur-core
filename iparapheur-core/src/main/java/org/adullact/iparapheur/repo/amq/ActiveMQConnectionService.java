package org.adullact.iparapheur.repo.amq;

import org.alfresco.repo.tenant.Tenant;

import javax.jms.JMSException;
import javax.jms.QueueSender;
import javax.jms.TextMessage;

/**
 * Created by lhameury on 14/05/14.
 */
public interface ActiveMQConnectionService {

    /**
     * Flag définissant si le broker activeMQ est embarqué ou non dans l'application I-Parapheur
     * @param internalBroker
     */
    void setInternalBroker(String internalBroker);

    /**
     * Défini le connecteur du broker sous la forme protocol://hostname
     * @param brokerConnector Connecteur du broker
     */
    void setBrokerConnector(String brokerConnector);

    /**
     * Défini le connecteur du consommateur sous la forme protocol://hostname
     * @param consumerConnector Connecteur du consommateur
     */
    void setConsumerConnector(String consumerConnector);

    /**
     * Défini le répertoire où sont stockés les messages de la queue
     * Voir la définition du bean pour le chemin utilisé
     * @param amqDir Le chemin du répertoire
     */
    void setAmqDir(String amqDir);

    /**
     * Défini la taille maximal du storage de la message queue
     * Renseigné dans la propriété parapheur.jobs.mq.store de alfresco-global.properties
     * @param storeLimit la Limite du store en MM
     */
    void setStoreLimit(String storeLimit);

    /**
     * Création d'un consumer sur une queue en amqp
     *
     * @param t Le tenant sur lequel créer le consumer
     */
    void createConsumer(Tenant t) throws Exception;

    /**
     * Permet le lancement du broker amq permettant la gestion des MQ avec persistance KahaDB dans alf_data/activemq (défini dans la définition du bean)
     */
    void launchBroker();

    /**
     * Récupération d'un sender de message
     * @return Le sender
     */
    QueueSender getSender();

    /**
     * Création d'un message pour envoi ultérieur dans la queue concernée (dépend du tenant de l'utilisateur)
     * @return Un message
     * @throws JMSException
     */
    TextMessage createTextMessage() throws JMSException;
}
