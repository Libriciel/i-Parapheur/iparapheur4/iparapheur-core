/*
 * Version 3.3
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 * 
 * contact@atolcd.com
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */
package org.adullact.iparapheur.repo.jscript;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import org.adullact.iparapheur.rules.bean.CustomProperty;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.dom4j.Element;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author eperalta
 */
public class TypesServiceScriptable extends BaseProcessorExtension {
    private static final Logger log = Logger.getLogger(TypesServiceScriptable.class);

    private TypesService typesService;
    
    //Pour getMetaDonneesApi
    @Autowired
    private MetadataService metadataService;
    
    @Autowired
    private NamespaceService namespaceService;
    
    @Autowired
    private NodeService nodeService;

    @Autowired
    private ParapheurService parapheurService;

    @Autowired
    private WorkflowService workflowService;

    public void setTypesService(TypesService typesService) {
        this.typesService = typesService;
    }
    
    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }
    
    public JSONObject getMetaDonneesDossierApi(String type, String sstype, NodeRef dossier) {
        //Liste des définitions de metadonnées
        List<CustomMetadataDef> list = metadataService.getMetadataDefs();
        Map<String, Map<String, String>> mds = null;
        Map<QName, Serializable> properties = nodeService.getProperties(dossier);
        
        Map<String, Map<String, Serializable>> toReturn = new HashMap<String, Map<String, Serializable>>(); 
        Map<String, Serializable> child;
        
        String realName = null;
        String keyName = null;
        String keyValue = null;
        
         if (type != null && sstype != null && !type.isEmpty() && !sstype.isEmpty()) {
            try {
                //Récupération des metadonnées pour les type/sous-type donnés
                mds = typesService.getMetadatasMap(type, sstype);
                List<CustomProperty<?>> definedProperties = new ArrayList<>();
                for(String key : mds.keySet()) {
                    QName tmpQname = QName.createQName(key);
                    if(properties.containsKey(tmpQname)) {
                        String localName = tmpQname.getLocalName();
                        Serializable value = properties.get(tmpQname);
                        definedProperties.add(new CustomProperty<>(localName, value));
                    }

                }

                NodeRef workflow = getWorkflow(type, sstype, definedProperties);

                if(workflow != null) {
                    SavedWorkflow savedOne = workflowService.getSavedWorkflow(workflow);
                    for(EtapeCircuit e : savedOne.getCircuit()) {
                        for(String meta : e.getListeMetadatas()) {
                            if(!mds.containsKey(meta)) {
                                Map<String, String> props = new HashMap<>();
                                props.put("default", "");
                                props.put("editable", "false");
                                props.put("mandatory", "false");

                                mds.put("{http://www.adullact.org/parapheur/metadata/1.0}" + meta, props);
                            }
                        }

                        for(String meta : e.getListeMetadatasRefus()) {
                            if(!mds.containsKey(meta)) {
                                Map<String, String> props = new HashMap<>();
                                props.put("default", "");
                                props.put("editable", "false");
                                props.put("mandatory", "false");

                                mds.put("{http://www.adullact.org/parapheur/metadata/1.0}" + meta, props);
                            }
                        }
                    }
                }

            }catch(Exception e){
                e.printStackTrace();
            }
            if (mds != null) {
                for (CustomMetadataDef customMetadataDef : list) {
                    //Récupération de la valeur de la clé complète
                    keyValue = customMetadataDef.getName().toString();
                    keyName = customMetadataDef.getName().toPrefixString(namespaceService);
                    if(mds.containsKey(keyValue)) {
                        child = new HashMap<String, Serializable>();
                        if(customMetadataDef.getType() == CustomMetadataType.DATE) {
                            if(mds.get(keyValue).containsKey("default") && !mds.get(keyValue).get("default").isEmpty()) {
                                try {
                                    mds.get(keyValue).put("default", String.valueOf(new SimpleDateFormat("yyyy-MM-dd").parse(mds.get(keyValue).get("default")).getTime()));
                                }catch(Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        child.putAll(mds.get(keyValue));
                        child.put("realName", customMetadataDef.getTitle());
                        child.put("type", customMetadataDef.getType().toString());
                        if(properties.containsKey(customMetadataDef.getName())) {
                            if(properties.get(customMetadataDef.getName()) != null) {
                                if(customMetadataDef.getType().toString().equals("DATE")) {
                                    child.put("value", String.valueOf(((Date) properties.get(customMetadataDef.getName())).getTime()));
                                } else {
                                    child.put("value", (properties.get(customMetadataDef.getName())).toString());
                                }
                            }
                        }
                        else{
                            child.put("value", ""); 
                        }
                        if(customMetadataDef.getEnumValues() != null) {
                            child.put("values", (Serializable) customMetadataDef.getEnumValues());
                        }
                        toReturn.put(keyName, child);
                    }
                }
            }
        }
        JSONObject json = new JSONObject(toReturn);
        return json;
    }

    @SuppressWarnings("unused")
    public boolean hasSelectionScript(String type, String sousType) {
        return typesService.hasSelectionScript(type, sousType);
    }

    /**
     * Renvoie Les metadonnées pour le type/sous-type donné sous forme de string JSON
     * @param type Type du dossier
     * @param sstype Sous-Type du dossier
     * @return JSON sous forme de String
     */
    public String getMetaDonneesApi(String type, String sstype) {
        //Liste des définitions de metadonnées
        List<CustomMetadataDef> list = metadataService.getMetadataDefs();
        
        
        JSONArray mainJson = new JSONArray();
        
        Map<String, Map<String, String>> mds = null;
        
        String realName;
        String keyName;
        
        try {
            //Récupération des metadonnées pour les type/sous-type donnés
            mds = typesService.getMetadatasMap(type, sstype);
        }catch(Exception e){
            mds = null;
        } finally {
            if (mds != null) {
                for (CustomMetadataDef customMetadataDef : list) {
                    //Récupération de la valeur de la clé complète
                    String keyValue = customMetadataDef.getName().toString();
                    if(mds.containsKey(keyValue)) {
                        //Ajout de la metadonnée dans l'objet Json et des propriétés supplémentaires
                        try {
                            //Récupération du nom réel et du nom de la clé
                            realName = customMetadataDef.getTitle();
                            keyName = customMetadataDef.getName().toPrefixString(namespaceService);
                            //Mise a jour de l'objet Json
                            JSONObject obj = new JSONObject();

                            if(customMetadataDef.getType() == CustomMetadataType.DATE) {
                                if(mds.get(keyValue).containsKey("default") && !mds.get(keyValue).get("default").isEmpty()) {
                                    obj.put("default", new SimpleDateFormat("yyyy-MM-dd").parse(mds.get(keyValue).get("default")).getTime());
                                }
                            } else if(mds.get(keyValue).containsKey("default") && !mds.get(keyValue).get("default").isEmpty()) {
                                obj.put("default", mds.get(keyValue).get("default"));
                            }

                            obj.put("id", keyName);
                            obj.put("editable", mds.get(keyValue).get("editable"));
                            obj.put("isAlphaOrdered", customMetadataDef.isAlphaOrdered());
                            obj.put("mandatory", mds.get(keyValue).get("mandatory"));
                            obj.put("realName", realName);
                            obj.put("type", customMetadataDef.getType().toString());
                            if(customMetadataDef.getEnumValues() != null) {
                                obj.put("values", customMetadataDef.getEnumValues());
                            }
                            mainJson.put(obj);
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else if((type == null || type.isEmpty()) && (sstype == null || sstype.isEmpty())) {
                String username = AuthenticationUtil.getRunAsUser();
                List<NodeRef> parapheurs = parapheurService.getOwnedParapheurs(username);
                parapheurs.addAll(parapheurService.getOwnedBySecretariatParapheurs(username));
                for (CustomMetadataDef customMetadataDef : list) {
                    //Récupération de la valeur de la clé complète
                    String keyValue = customMetadataDef.getName().toString();
                    //Ajout de la metadonnée dans l'objet Json et des propriétés supplémentaires
                    try {
                        //Récupération du nom réel et du nom de la clé
                        realName = customMetadataDef.getTitle();
                        keyName = customMetadataDef.getName().toPrefixString(namespaceService);


                        boolean addMeta = false;
                        for(NodeRef parapheur: parapheurs) {
                            ArrayList<String> visibilite = (ArrayList<String>) nodeService.getProperty(parapheur, ParapheurModel.PROP_VISIBILITE_METADONNE);
                            if(visibilite != null) {
                                addMeta = visibilite.contains(keyName.replace("cu:", ""));
                            }
                            if(addMeta) {
                                break;
                            }
                        }
                        if(addMeta) {
                            //Mise a jour de l'objet Json
                            JSONObject obj = new JSONObject();
                            obj.put("id", keyName);
                            obj.put("name", realName);
                            obj.put("type", customMetadataDef.getType().toString());
                            if(customMetadataDef.getEnumValues() != null) {
                                obj.put("values", customMetadataDef.getEnumValues());
                            }
                            mainJson.put(obj);
                        }
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            } 
        }
        return mainJson.toString();
    }

    public boolean isReadingMandatory(String typeName, String subtypeName) {
        return typesService.isReadingMandatory(typeName, subtypeName);
    }

    public boolean isCircuitHierarchiqueVisible(String typeName, String subtypeName) {
        return typesService.isCircuitHierarchiqueVisible(typeName, subtypeName);
    }

    public List<NodeRef> getListeCalques(String type, String subtype) {
        return typesService.getListeCalques(type, subtype);
    }

    public List<String> getMandatoryMetadatas(String type, String subtype) {
        return typesService.getMandatoryMetadatas(type, subtype);
    }

    public List<String> getEditableMetadatas(String type, String subtype) {
        return typesService.getEditableMetadatas(type, subtype);
    }

    public NodeRef getWorkflow(String type, String subtype) {
        return typesService.getWorkflow(type, subtype);
    }

    public NodeRef getWorkflow(String type, String subtype, List<CustomProperty<?>> customProperties) {
        return typesService.getWorkflow(null, type, subtype, customProperties);
    }

    public Map<String, Map<String, String>> getMetadatasMap(String typeName, String subtypeName) {
        return typesService.getMetadatasMap(typeName, subtypeName);
    }

    public boolean isDigitalSignatureMandatory(String typeName, String subtypeName) {
        return typesService.isDigitalSignatureMandatory(typeName, subtypeName);
    }

    @SuppressWarnings("unused")
    public boolean isMultiDocument(String typeName, String subtypeName) {
        return typesService.isMultiDocument(typeName, subtypeName);
    }
    
    public boolean areAttachmentsIncluded(String typeName, String subtypeName) {
        return typesService.areAttachmentsIncluded(typeName, subtypeName);
    }

    @SuppressWarnings("unused")
    public JSONArray getTypologyAsAdmin(String search) {
        JSONArray ret = new JSONArray();
        try {
            Map<String, List<Element>> typesDefinitions = typesService.optimizedGetSavedTypeAsAdmin();
            List<Element> types = typesService.getTypesElements();
            Map<String, JSONObject> typoMap = new HashMap<String,JSONObject>();
            int total = types.size();

            for (Element type : types) {

                JSONObject typology = new JSONObject();
                Map<String, JSONObject> subtypoMap = new HashMap<String,JSONObject>();

                String id = type.element("ID").getTextTrim();

                String override = "false";
                if (type.element("TdT") != null) {
                    if (type.element("TdT").element("override") != null) {
                        override = type.element("TdT").element("override").getTextTrim();
                    }
                }

                boolean show = id.toLowerCase().contains(search.toLowerCase()) || type.element("Desc").getTextTrim().toLowerCase().contains(search.toLowerCase());

                typology.put("id", id);
                typology.put("total", total);
                typology.put("desc", type.element("Desc").getTextTrim());
                typology.put("tdtNom", type.element(TypesService.TYPE_KEY_TDT).element("Nom").getTextTrim());
                typology.put("tdtProtocole", type.element(TypesService.TYPE_KEY_TDT).element(TypesService.TYPE_KEY_PROTOCOLE).getTextTrim());
                typology.put("tdtOverride", override);
                typology.put("sigFormat", type.element(TypesService.TYPE_KEY_SIGNATURE_FORMAT).getTextTrim());
                typology.put("sigLocation", type.element(TypesService.TYPE_KEY_SIGNATURE_LOCATION).getTextTrim());
                typology.put("sigPostalCode", type.element(TypesService.TYPE_KEY_SIGNATURE_POSTALCODE).getTextTrim());

                JSONArray subtypesArray = new JSONArray();

                if(typesDefinitions.containsKey(id)) {
                    for(Element subtypeElement : typesDefinitions.get(id)) {
                        JSONObject subtypeDesc = new JSONObject();
                        if(subtypeElement.element("ID").getTextTrim().toLowerCase().contains(search.toLowerCase())) {
                            show = true;
                        }
                        subtypeDesc.put("id", subtypeElement.element("ID").getTextTrim());
                        subtypeDesc.put("desc", subtypeElement.element("Desc").getTextTrim());
                        subtypeDesc.put("parent", id);
                        subtypoMap.put(subtypeDesc.getString("id").toLowerCase(), subtypeDesc);
                    }
                }

                for(JSONObject obj : subtypoMap.values()) {
                    subtypesArray.put(obj);
                }

                typology.put("sousTypes", subtypesArray);

                if(show) {
                    typoMap.put(id.toLowerCase(), typology);
                }
            }

            Map<String, JSONObject> treeMap = new TreeMap<String, JSONObject>(typoMap);

            for(JSONObject obj : treeMap.values()) {
                ret.put(obj);
            }
        } catch (JSONException e) {
            throw new WebScriptException(Status.STATUS_INTERNAL_SERVER_ERROR, "Unable to retrieve typology");
        }

        return ret;
    }

    public JSONArray getTypologyWithBureau(String b) {
        NodeRef bureau = new NodeRef("workspace://SpacesStore/" + b);
        JSONArray ret = new JSONArray();
        try {
            Map<String, List<String>> typesDefinitions = typesService.optimizedGetSavedType(false, bureau);

            for (Map.Entry<String, List<String>> definition : typesDefinitions.entrySet()) {
                JSONObject typology = new JSONObject();
                typology.put("id", definition.getKey());
                typology.put("sousTypes", definition.getValue());
                ret.put(typology);
            }
        } catch (JSONException e) {
            throw new WebScriptException(Status.STATUS_INTERNAL_SERVER_ERROR, "Unable to retrieve typology");
        }

        return ret;
    }

    public JSONArray getAllTypology() {
        JSONArray ret = new JSONArray();
        try {
            Map<String, List<String>> typesDefinitions = typesService.optimizedGetSavedType(true, null);

            for (Map.Entry<String, List<String>> definition : typesDefinitions.entrySet()) {
                JSONObject typology = new JSONObject();
                typology.put("id", definition.getKey());
                typology.put("sousTypes", definition.getValue());
                ret.put(typology);
            }
        } catch (JSONException e) {
            throw new WebScriptException(Status.STATUS_INTERNAL_SERVER_ERROR, "Unable to retrieve typology");
        }

        return ret;
    }

    @SuppressWarnings("unused")
    public boolean isCompatibleWithMultidoc(@Nullable String typeId) {

        Assert.notNull(typeId);
        return typesService.isCompatibleWithMultidoc(typeId);
    }

    @SuppressWarnings("unused")
    public void updateSubtypesMultidocState(@Nullable String typeId) {

        Assert.notNull(typeId);
        boolean areSubtypesMultidoc = typesService.isCompatibleWithMultidoc(typeId);

        if (!areSubtypesMultidoc) {
            typesService.setValueForSousType(typeId, "*", TypesService.SOUS_TYPE_KEY_MULTI_DOCUMENT, "false");
        }
    }

    @SuppressWarnings("unused")
    public String getProtocol(@Nullable String typeId) {

        Assert.notNull(typeId);
        return typesService.getProtocol(typeId);
    }
}
