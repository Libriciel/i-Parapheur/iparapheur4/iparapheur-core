package org.adullact.iparapheur.repo.allo;

import com.atolcd.parapheur.repo.ParapheurVersionProperties;
import org.alfresco.repo.processor.BaseProcessorExtension;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by lhameury on 18/11/14.
 */
public class AlloClient extends BaseProcessorExtension {

    @Autowired
    private ParapheurVersionProperties parapheurVersionProperties;

    private String refClient;

    public String getRefClient() {
        return refClient;
    }

    public void setRefClient(String refClient) {
        this.refClient = refClient;
    }

    public String getVersion() {

        return parapheurVersionProperties.getVersionNumber();
    }
}
