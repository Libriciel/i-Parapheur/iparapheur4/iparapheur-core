/*
 * Version 3.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

package org.adullact.iparapheur.repo.jscript;

import org.adullact.iparapheur.domain.CertificatesDAO;
import org.adullact.iparapheur.domain.CertificatesEntity;
import org.adullact.iparapheur.domain.hibernate.CertificatesEntityImpl;
import org.adullact.iparapheur.util.X509Util;
import org.alfresco.repo.jscript.BaseScopableProcessorExtension;
import org.alfresco.util.InputStreamContent;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.surf.util.Base64;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.servlet.FormData;
import org.springframework.extensions.webscripts.servlet.FormData.FormField;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Gestion des certificats X509 des comptes utilisateurs via webscript
 * 
 * @author Stephane Vast - ADULLACT Projet
 */
public class JsKeyMaterial extends BaseScopableProcessorExtension {

    private static Logger logger = Logger.getLogger(JsKeyMaterial.class);

    private CertificatesDAO certificatesDAO;

    public static String beginString = "-----BEGIN CERTIFICATE-----";
    public static String endString   = "-----END CERTIFICATE-----";

    public void setCertificatesDAO(CertificatesDAO certificatesDAO) {
        this.certificatesDAO = certificatesDAO;
    }

    public void addCertificate(String username, String certificateId) {
        CertificatesEntity entity = new CertificatesEntityImpl();
        entity.setUsername(username);
        entity.setCertificateId(certificateId);
        certificatesDAO.createCertificates(entity);
    }

    public boolean hasMultipleCertificates(String username) {
        List<CertificatesEntity> certs = certificatesDAO.getCertificateListByUsername(username);

        return certs.size() > 1;
    }

    public List<CertificatesEntity> getCertificateListByUsername(String username) {
        return certificatesDAO.getCertificateListByUsername(username);
    }
    public List<String> getUsersWithMultipleCertificate() {
        return certificatesDAO.getUsersWithMultipleCertificate();
    }

    public CertificatesEntity getCertificatesByUsername(String username) {
        return certificatesDAO.getCertificatesByUsername(username);
    }

    public void deleteCertificate(CertificatesEntity e) {
        certificatesDAO.deleteCertificates(e);
    }

    public CertificatesEntity getCertificatesById(String certificateId) {
        return certificatesDAO.getCertificatesById(certificateId);
    }

    /**
     * Récupération de l'identifiant du certificat telecharge par 
     * l'administrateur pour l'associer à un utilisateur                       
     * ( voir certif-user.post.js, certif_user_html.ftl )                       <br/><br/>
     * 
     * Le formulaire contient un champ "file" contenant le certificat choisis 
     * par  l'administrateur.                                                   <br/>
     * Si ce champ est vide, et si l'utilisateur possède déjà un certificat,
     * l'association entre l'utilisateur et son certificat existant est supprimée
     * 
     * @return 
     * Un identifiant du certificat, construit d'apres le numero de série,      <br/>
     * ou  "EMPTY" si l'administrateur n'a pas sélectionné de fichier           <br/>
     * ou null si le fichier téléchargé n'est pas reconnu comme un certificat.  <br/>
     * 
     * 
     */
    public String getIdCertificat(FormData formData) {

        try {
            Content contenu = null;

            for (FormField field : formData.getFields()) {
                if ("file".equals(field.getName()) && field.getIsFile()) {
                    contenu = (Content) field.getContent();
                    break;
                }
            }
            
            if (contenu==null) {
                throw new Exception("champ \"file\" non trouve");
            }

            if (logger.isDebugEnabled()) {
                logger.debug("form content : getSize()="+ contenu.getSize()+", getContent()=\"" + contenu.getContent() + "\"");
            }

            if (contenu.getContent()==null || contenu.getContent().trim().length()==0) {
                return "EMPTY";
            }

            Collection<? extends Certificate> certs = null;
            CertificateFactory factory = CertificateFactory.getInstance("X.509");

            if (contenu.getContent().contains(beginString)) {
                /**
                 * seance nettoyage des eventuelles infos en clair.
                 * Les infos intéressantes se situent entre BEGIN et END
                 */
                String cleancontent = contenu.getContent();
                int beginIndex = cleancontent.indexOf(beginString);
                int endIndex = cleancontent.indexOf(endString);
                cleancontent = cleancontent.substring(beginIndex, endIndex + endString.length());
                if (logger.isDebugEnabled()) {
                    logger.debug("Cleaned CONTENT: \t" + cleancontent);
                }
                certs = factory.generateCertificates(new ByteArrayInputStream(cleancontent.getBytes()));
            } else {
                /* on tente quand même: peut etre encodé binaire */
                certs = factory.generateCertificates(contenu.getInputStream());
            }

            X509Certificate[] xcerts = new X509Certificate[certs.size()];
            for (int i = 0; i < xcerts.length; i++) {
                xcerts[i] = (X509Certificate) certs.toArray()[i];
            }

            return getUniqueId(xcerts);

        } catch (Exception e) {
            logger.warn("JsKeyMaterial::getIdCertificat", e);
            return null;
        }
    }

    public Map<String, String> getCertPropertiesMap(String x509cert) {
        Map<String, String> res = new HashMap<String, String>();
        try {
            if (x509cert != null && !x509cert.trim().isEmpty()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("1-getCertPropertiesMap :\n" + x509cert);
                }
                //byte[] cert = x509cert.getBytes() // Base64.decode(x509cert);
                CertificateFactory factory = CertificateFactory.getInstance("X.509");
                Collection<? extends Certificate> certs = null;
                if (x509cert.contains(beginString)) {
                    String cleancontent = x509cert;
                    int beginIndex = cleancontent.indexOf(beginString);
                    int endIndex = cleancontent.indexOf(endString);
                    cleancontent = cleancontent.substring(beginIndex, endIndex + endString.length());
                    certs = factory.generateCertificates(new ByteArrayInputStream(cleancontent.getBytes()));
                } else {
                    certs = factory.generateCertificates(new ByteArrayInputStream(x509cert.getBytes()));
                }
                X509Certificate mycert = (X509Certificate) certs.toArray()[0];
                SimpleDateFormat sdf = new SimpleDateFormat("E dd MMM yyyy 'à' HH:mm", Locale.FRENCH);
                res.put("from", sdf.format(mycert.getNotBefore()));
                res.put("to", sdf.format(mycert.getNotAfter())); //mycert.getSubjectDN()
                // email de la forme EMAILADDRESS=toto@mail.org,
                res.put("email", extractEmail(mycert.getSubjectX500Principal().toString()));
                String x500Principal = mycert.getSubjectX500Principal().getName(); // rfc2253 par défaut
                res.put("name", extractCN(x500Principal));
                res.put("organization", extractO(x500Principal));
            }
        } catch (Exception e) {
            logger.warn("JsKeyMaterial::getCertPropertiesMap" + e.getLocalizedMessage());
            if (logger.isDebugEnabled()) {
                logger.debug(e);
            }
        }
        return res;
    }

    private static String extractCN(String dn) {
        if (dn == null || dn.length() < 4) {
            return "<inconnu>";
        }
        int i = dn.indexOf("CN=");
        if (i < 0) {
            return "<inconnu>";
        }
        i += 3;
        int j = dn.indexOf(",", i);
        if (j - 1 < 0) {
            return "<inconnu>";
        }
        else if (j == -1) {
            // 'CN=' is in last position, no ',' behind !
            return dn.substring(i).trim();
        }
        return dn.substring(i, j).trim();
    }

    private static String extractO(String dn) {
        if (dn == null || dn.length() < 4) {
            return "<inconnu>";
        }
        int i = dn.indexOf("O=");
        if (i < 0) {
            return "<inconnu>";
        }
        i += 2;
        int j = dn.indexOf(",", i);
        if (j - 1 < 0) {
            return "<inconnu>";
        }
        else if (j == -1) {
            // 'O=' is in last position, no ',' behind !
            return dn.substring(i).trim();
        }
        return dn.substring(i, j).trim();
    }

    private static String extractEmail(String dn) {
        if (dn == null || dn.length() < 4) {
            return "<inconnu>";
        }
        int i = dn.indexOf("EMAILADDRESS=");
        if (i < 0) {
            return "<inconnu>";
        }
        i += 13;
        int j = dn.indexOf(",", i);
        if (j - 1 < 0) {
            return "<inconnu>";
        }
        else if (j == -1) {
            // 'O=' is in last position, no ',' behind !
            return dn.substring(i).trim();
        }
        return dn.substring(i, j).trim();
    }

    public String getCertificatBase64(InputStreamContent content) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int readed;
        InputStream in = content.getInputStream();
        while ((readed = in.read(buffer)) >= 0) {
            out.write(buffer, 0, readed);
        }

        byte[] data = out.toByteArray();
        return Base64.encodeBytes(data);
    }

    public String getCertificatBase64(org.springframework.extensions.surf.util.InputStreamContent content) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int readed;
        InputStream in = content.getInputStream();
        while ((readed = in.read(buffer)) >= 0) {
            out.write(buffer, 0, readed);
        }

        byte[] data = out.toByteArray();
        return Base64.encodeBytes(data);
    }

    public org.springframework.extensions.surf.util.InputStreamContent getCertificatContent(String base64) {
        byte[] data = Base64.decode(base64);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        return new org.springframework.extensions.surf.util.InputStreamContent(in, "application/octet-stream", "UTF-8");
    }

    public String getUserOfCertificat(String certB64) {
        byte[] decoded = Base64.decode(certB64);
        if (decoded != null) {
            String certString = new String(decoded);
            X509Certificate x509Cert = X509Util.getX509CertificateFromString(certString);
            if (x509Cert != null) {
                String id = X509Util.getUniqueId(x509Cert);
                if (id != null) {
                    CertificatesEntity certEntity = certificatesDAO.getCertificatesById(id);
                    return (certEntity != null) ? certEntity.getUsername() : null;
                }
            }
        }
        return null;
    }

    public Map<String, String> getUsefulProps(String certB64) {
        byte[] decoded = Base64.decode(certB64);
        if (decoded != null) {
            String certString = new String(decoded);
            X509Certificate x509Cert = X509Util.getX509CertificateFromString(certString);
            if (x509Cert != null) {
                return X509Util.getUsefulCertProps(x509Cert);
            }
        }
        return new HashMap<String, String>();
    }

    public static String getUniqueId(X509Certificate[] certs) {
        String identifier = null;

        if (logger.isDebugEnabled()) {
            logger.debug("getUniqueId: " + certs[0]);
        }

        identifier = certs[0].getSerialNumber().toString() + "/" + certs[0].getIssuerDN().toString();
//        for (X509Certificate cert : certs) {
//            if (identifier == null) {
//                identifier = cert.getSerialNumber().toString();
//            } else {
//                identifier += "/" + cert.getSerialNumber().toString();
//            }
//
//        }

        return identifier;
    }

    public JSONObject getCertificatDetails(String certB64) {
        JSONObject details = new JSONObject();
        String id = null;
        String usedBy = null;
        byte[] decoded = Base64.decode(certB64);
        if (decoded != null) {
            String certString = new String(decoded);
            X509Certificate x509Cert = X509Util.getX509CertificateFromString(certString);
            if (x509Cert != null) {
                id = X509Util.getUniqueId(x509Cert);
                if (id != null) {
                    CertificatesEntity certEntity = certificatesDAO.getCertificatesById(id);
                    usedBy = (certEntity != null) ? certEntity.getUsername() : null;
                }
                try {
                    details.putOpt("usedBy", usedBy);
                    JSONObject cert = new JSONObject(X509Util.getUsefulCertProps(x509Cert)).putOpt("id", id);
                    details.putOpt("certificat", cert);
                } catch (JSONException e) {}
            }
        }
        return details;
    }
}
