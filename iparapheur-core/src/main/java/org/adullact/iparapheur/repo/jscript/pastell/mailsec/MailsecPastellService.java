package org.adullact.iparapheur.repo.jscript.pastell.mailsec;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.TypesService;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import com.atolcd.parapheur.repo.impl.S2lowServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.adullact.iparapheur.repo.jscript.pastell.mailsec.exceptions.NodeRefIsNotPastellConnectorException;
import org.adullact.iparapheur.repo.jscript.pastell.mailsec.model.Journal;
import org.adullact.iparapheur.repo.jscript.seals.exceptions.MalformedNodeRefException;
import org.adullact.iparapheur.status.StatusMetier;
import org.adullact.utils.StringUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import static com.atolcd.parapheur.repo.impl.S2lowServiceImpl.STATUS_MAILSEC_NOT_CONFIRMED;

/**
 * Créé par lhameury le 18/04/18.
 */
public class MailsecPastellService {

    @Autowired
    private SearchService searchService;
    @Autowired
    private NodeService nodeService;
    @Autowired
    private NamespaceService namespaceService;
    @Autowired
    private ParapheurService parapheurService;
    @Autowired
    private ContentService contentService;
    @Autowired
    private TypesService typesService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private CorbeillesService corbeillesService;

    private String connectorHost;
    private String connectorPort;
    private String connectorPath;

    private static final String SERVER_ID_PAYLOAD = "serverId";
    private static final String MAIL_ID_PAYLOAD = "id";
    private static final String USER_PAYLOAD = "user";
    private static final String PAYLOAD_KEY = "payload";
    private static final String EVENTS_KEY = "events";
    private static final String OBJECT_KEY = "object";
    private static final String MESSAGE_KEY = "message";
    private static final String DOCUMENT_ID_KEY = "documentId";

    private static final String PASTELL_URL_POST_DOCUMENT = "http://%s:%s%s/api/server/%s/document";
    private static final String PASTELL_URL_DELETE_DOCUMENT = "http://%s:%s%s/api/server/%s/document/%s";
    private static final String PASTELL_URL_GET_JOURNAL = "http://%s:%s%s/api/server/%s/document/%s/journal";


    public MailsecPastellService() {
    }

    public String getConnectorHost() {
        return connectorHost;
    }

    public void setConnectorHost(String connectorHost) {
        this.connectorHost = connectorHost;
    }

    public String getConnectorPort() {
        return connectorPort;
    }

    public void setConnectorPort(String connectorPort) {
        this.connectorPort = connectorPort;
    }

    public String getConnectorPath() {
        return connectorPath;
    }

    public void setConnectorPath(String connectorPath) {
        this.connectorPath = connectorPath;
    }

    private NodeRef getPastellConnectorsNode() {

        NodeRef pastellConnectorsHome;
        String sealsPath = MailsecPastellModel.TYPE_MAILSEC_PASTELL_CONNECTORS.getPrefixedQName(namespaceService).getPrefixString();
        String policiesPath = "/app:company_home/app:dictionary/" + sealsPath;
        NodeRef rootNode = nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);

        List<NodeRef> policiesHomes = searchService.selectNodes(
                rootNode,
                policiesPath,
                null,
                namespaceService,
                false
        );

        if (policiesHomes.size() > 0) {
            pastellConnectorsHome = policiesHomes.get(0);
        } else {
            NodeRef dictionary = searchService.selectNodes(
                    rootNode,
                    "/app:company_home/app:dictionary",
                    null,
                    namespaceService,
                    false
            ).get(0);

            pastellConnectorsHome = nodeService.createNode(
                    dictionary,
                    ContentModel.ASSOC_CONTAINS,
                    MailsecPastellModel.TYPE_MAILSEC_PASTELL_CONNECTORS,
                    MailsecPastellModel.TYPE_MAILSEC_PASTELL_CONNECTORS
            ).getChildRef();
        }

        return pastellConnectorsHome;
    }

    public List<MailsecPastellConnector> list() {
        NodeRef certsHome = this.getPastellConnectorsNode();
        List<MailsecPastellConnector> connectorList = new ArrayList<>();

        List<ChildAssociationRef> children = nodeService.getChildAssocs(certsHome);
        for (ChildAssociationRef child : children) {

            NodeRef childRef = child.getChildRef();
            MailsecPastellConnector connector = new MailsecPastellConnector();

            connector.setTitle((String) nodeService.getProperty(childRef, ContentModel.PROP_TITLE));

            connector.setId(childRef.getId());
            connector.setServerId((String) nodeService.getProperty(childRef, MailsecPastellModel.PROP_PASTELL_CONNECTOR_SERVER_ID));

            connectorList.add(connector);
        }

        return connectorList;
    }


    public MailsecPastellConnector create(MailsecPastellConnector pastellConnector) {

        NodeRef connectorsNode = this.getPastellConnectorsNode();

        // We check for same name before save it
        List<ChildAssociationRef> existingList = nodeService.getChildAssocsByPropertyValue(connectorsNode, ContentModel.PROP_TITLE, pastellConnector.getTitle());
        if (existingList.isEmpty()) {
            String uuid = UUID.randomUUID().toString();

            Map<QName, Serializable> properties = new HashMap<>();
            properties.put(ContentModel.PROP_NAME, uuid);
            properties.put(ContentModel.PROP_TITLE, pastellConnector.getTitle());
            properties.put(MailsecPastellModel.PROP_PASTELL_CONNECTOR_SERVER_ID, pastellConnector.getServerId());

            NodeRef newConnector = nodeService.createNode(connectorsNode,
                    ContentModel.ASSOC_CONTAINS,
                    QName.createQName("ph:mailsec-pastell-connector" + uuid, namespaceService),
                    MailsecPastellModel.TYPE_MAILSEC_PASTELL_CONNECTOR, properties).getChildRef();

            pastellConnector.setId(newConnector.getId());

            return pastellConnector;
        } else {
            return null;
        }
    }

    public void delete(String id) {
        // we have to check that the node is child of pastell connector folder

        NodeRef connectorsHome = this.getPastellConnectorsNode();

        // We check that this ID is good
        Pattern p = Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f‌​]{4}-[0-9a-f]{12}$");
        if (!p.matcher(id).matches()) {
            // Bad id...
            throw new MalformedNodeRefException("Bad id");
        }

        NodeRef certRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, id);

        // we check that node is child of connectorsHome
        if (nodeService.getPrimaryParent(certRef).getParentRef().equals(connectorsHome)) {
            nodeService.deleteNode(certRef);
        } else {
            // This is not a connector node !
            throw new NodeRefIsNotPastellConnectorException();
        }
    }

    public void update(MailsecPastellConnector mailsecPastellConnector) {
        NodeRef mailsecConnectorNode = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, mailsecPastellConnector.getId());

        nodeService.setProperty(mailsecConnectorNode, ContentModel.PROP_TITLE, mailsecPastellConnector.getTitle());
    }

    public void sendMailsec(List<String> destinataires, List<String> destinatairesCC, List<String> destinatairesCCI, String subject, String body, NodeRef node, List<NodeRef> includedAnnexes, boolean includeFirstPage) throws Exception {

        String serverId = getServerId(node);

        HttpEntity entity = buildEntity(
                destinataires,
                destinatairesCC,
                destinatairesCCI,
                subject,
                body,
                node,
                includedAnnexes,
                includeFirstPage);

        HttpPost request = new HttpPost(String.format(PASTELL_URL_POST_DOCUMENT, connectorHost, connectorPort, connectorPath, serverId));
        request.setEntity(entity);

        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(request);

        JSONObject object = new JSONObject(EntityUtils.toString(response.getEntity()));

        markNodeAsHandled(node, object);
    }

    private void markNodeAsHandled(NodeRef node, JSONObject object) throws JSONException {
        // storing the mail id in the folder properties
        nodeService.setProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_ID, object.getString(MAIL_ID_PAYLOAD));
        nodeService.setProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_STATUS, STATUS_MAILSEC_NOT_CONFIRMED);
        nodeService.setProperty(node, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_EN_COURS_MAILSEC_PASTELL);

        // This action can be cancelled
        nodeService.setProperty(node, ParapheurModel.PROP_RECUPERABLE, Boolean.TRUE);

        List<Object> list = new ArrayList<>();
        list.add(StatusMetier.STATUS_EN_COURS_MAILSEC_PASTELL);
        parapheurService.auditWithNewBackend("ParapheurServiceCompat", "Envoyé par mail sécurisé via Pastell.", node, list);

        /*
         * Positionnement du nom utilisateur dans l'étape courante pour
         * recuperation par le trigger.
         */
        parapheurService.setCurrentValidator(node, AuthenticationUtil.getRunAsUser());
    }

    public String getServerId(NodeRef node) {
        String typeMetier = (String) nodeService.getProperty(node, ParapheurModel.PROP_TYPE_METIER);
        String sousTypeMetier = (String) nodeService.getProperty(node, ParapheurModel.PROP_SOUSTYPE_METIER);

        // On récupère l'id du serveur Pastell
        return typesService.getPastellServerId(typeMetier, sousTypeMetier);
    }

    public String getMailsecId(NodeRef node) {
        return (String) nodeService.getProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_ID);
    }

    private HttpEntity buildEntity(List<String> destinataires, List<String> destinatairesCC, List<String> destinatairesCCI, String subject, String body, NodeRef node, List<NodeRef> includedAnnexes, boolean includeFirstPage) throws Exception {
        final MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
        multipartEntityBuilder.setMode(HttpMultipartMode.RFC6532);

        destinataires.forEach(s -> multipartEntityBuilder.addTextBody("to", s));
        destinatairesCC.forEach(s -> multipartEntityBuilder.addTextBody("cc", s));
        destinatairesCCI.forEach(s -> multipartEntityBuilder.addTextBody("bcc", s));

        List<NodeRef> mainDocuments = parapheurService.getMainDocuments(node);
        /*
          First Attachment is the recap doc, a.k.a "PDF d'impression", without annexes.
         */
        File myPDFile = parapheurService.genererDossierPDF(node, false, null, includeFirstPage);

        String title = (String) nodeService.getProperty(node, ContentModel.PROP_TITLE);

        multipartEntityBuilder.addBinaryBody(
                "files",
                myPDFile,
                ContentType.create("application/pdf"),
                title + ".pdf");

        mainDocuments.addAll(includedAnnexes);
        /*
          builds parts from docRef list.
         */
        String mainDocName = null;
        for (NodeRef docRef : mainDocuments) {
            String docname = (String) nodeService.getProperty(docRef, ContentModel.PROP_NAME);
            if(mainDocName == null) mainDocName = docname;

            ContentReader tmpReader = contentService.getReader(docRef, ContentModel.PROP_CONTENT);
            String mimetype = tmpReader.getContentData().getMimetype();

            multipartEntityBuilder.addBinaryBody(
                    "files",
                    tmpReader.getContentInputStream(),
                    ContentType.create(mimetype),
                    docname);
        }

        /*
        Handle ZIP with signatures
         */
        String signatureFormat = (String) nodeService.getProperty(node, ParapheurModel.PROP_SIGNATURE_FORMAT);
        if (signatureFormat != null && !StringUtils.startsWithIgnoreCase(signatureFormat, "PAdES")) {
            File zipWithSigs = parapheurService.produceZipSignatures(mainDocName, node);
            if(zipWithSigs != null) {
                multipartEntityBuilder.addBinaryBody(
                        "files",
                        new FileInputStream(zipWithSigs),
                        ContentType.create("application/zip"),
                        title + "_SIGs.zip");
            }
        }

        JSONObject payload = new JSONObject();
        payload.put(MAIL_ID_PAYLOAD, node.toString());
        payload.put(USER_PAYLOAD, authenticationService.getCurrentUserName());
        payload.put(SERVER_ID_PAYLOAD, getServerId(node));

        multipartEntityBuilder
                .addBinaryBody(OBJECT_KEY, subject.getBytes("UTF-8"))
                .addBinaryBody(MESSAGE_KEY, body.getBytes("UTF-8"))
                .addTextBody(PAYLOAD_KEY, payload.toString());

        return multipartEntityBuilder.build();
    }

    public void updateTransaction(String received) {
        try {
            JSONObject object = new JSONObject(received);
            ObjectMapper mapper = new ObjectMapper();

            List<Journal> events = mapper.readValue(object.getString(EVENTS_KEY), new TypeReference<List<Journal>>() {});

            String annotation = getAnnotationFromEvents(events);

            JSONObject payload = new JSONObject(object.getString(PAYLOAD_KEY));

            // Payload is NodeRef
            NodeRef node = new NodeRef(payload.getString(MAIL_ID_PAYLOAD));
            String user = payload.getString(USER_PAYLOAD);
            String serverId = payload.getString(SERVER_ID_PAYLOAD);
            String mailId = object.getString(DOCUMENT_ID_KEY);

            NodeRef parentCourant;
            if(nodeService.exists(node)) {
                parentCourant = nodeService.getPrimaryParent(node).getParentRef();
            } else {
                parentCourant = null;
            }

            AuthenticationUtil.runAs(() -> transactionService.getRetryingTransactionHelper().doInTransaction(() -> approveMailsec(annotation, node, user, serverId, mailId)), user);

            if(parentCourant != null) {
                AuthenticationUtil.runAs(() -> transactionService.getRetryingTransactionHelper().doInTransaction(() -> {
                    corbeillesService.moveDossier(node, parentCourant, nodeService.getPrimaryParent(node).getParentRef());
                    return null;
                }), user);
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    private Object approveMailsec(String annotation, NodeRef node, String user, String serverId, String mailId) throws IOException {
        if(nodeService.exists(node)) {
            parapheurService.setAnnotationPublique(node, annotation);
            parapheurService.setCurrentValidator(node, user);

            nodeService.setProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_STATUS, S2lowServiceImpl.STATUS_MAILSEC_FULLY_CONFIRMED);

            List<EtapeCircuit> circuit = parapheurService.getCircuit(node);

            // Récupération de la prochaine étape
            EtapeCircuitImpl etapeCourante = null;
            for (EtapeCircuit etape : circuit) {
                if (!etape.isApproved()) {
                    etapeCourante = (EtapeCircuitImpl) etape;
                    break;
                }
            }

            String actionDemandee = (etapeCourante != null ? etapeCourante.getActionDemandee() : null);
            if(actionDemandee != null && actionDemandee.equals("MAILSECPASTELL")) {
                parapheurService.approveV4(node, null);
            }

            nodeService.removeProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_ID);
        }

        removeDocument(serverId, mailId);

        return null;
    }

    public List<Journal> getDossierEvents(NodeRef node) throws IOException {
        String mailId = (String) nodeService.getProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_ID);
        String serverId = getServerId(node);

        HttpGet request = new HttpGet(String.format(PASTELL_URL_GET_JOURNAL, connectorHost, connectorPort, connectorPath, serverId, mailId));

        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(request);

        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(EntityUtils.toString(response.getEntity()), new TypeReference<List<Journal>>() {
        });
    }

    public String getAnnotationFromEvents(List<Journal> events) {

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        List<String> sended = new ArrayList<>();
        Map<String, String> received = new HashMap<>();

        final String[] sentDate = new String[1];

        // 1 - Get all mail and Look for mail confirmation
        events.forEach(ev -> {
            if (ev.getMessage().equals("Le document a été envoyé")) {
                sentDate[0] = df.format(ev.getDate());
            }
            if (ev.getAction().equals("envoi") && ev.getType().equals("5")) {
                sended.add(ev.getMessage().substring(23));
            } else if (ev.getAction().equals("Consulté") && ev.getType().equals("5")) {
                received.put(ev.getMessage().split(" ")[0], df.format(ev.getDate()));
            }
        });

        StringBuilder builder = new StringBuilder().append("Envoyé le ").append(sentDate[0]);
        sended.forEach(mail -> {
            if (received.containsKey(mail)) {
                builder.append("\n").append(mail).append(" reçu le ").append(received.get(mail));
            } else {
                builder.append("\nNon confirmé par ").append(mail);
            }
        });

        return builder.toString();
    }

    private int removeDocument(String serverId, String mailId) throws IOException {
        HttpDelete request = new HttpDelete(String.format(PASTELL_URL_DELETE_DOCUMENT, connectorHost, connectorPort, connectorPath, serverId, mailId));

        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(request);

        return response.getStatusLine().getStatusCode();
    }

    public void cancel(NodeRef node) throws IOException {
        String mailId = (String) nodeService.getProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_ID);
        String serverId = getServerId(node);

        removeDocument(serverId, mailId);
    }

    public void revert(NodeRef node) throws IOException {

        String mailId = (String) nodeService.getProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_ID);
        String serverId = getServerId(node);

        if (removeDocument(serverId, mailId) == 204) {
            nodeService.removeProperty(node, ParapheurModel.PROP_MAILSEC_MAIL_ID);
            nodeService.setProperty(node, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_PRET_MAILSEC_PASTELL);


            parapheurService.setSignataire(node, null);

            List<Object> list = new ArrayList<>();
            list.add(StatusMetier.STATUS_PRET_MAILSEC_PASTELL);
            parapheurService.auditWithNewBackend("ParapheurServiceCompat", "Envoi par mail sécurisé via Pastell annulé", node, list);
        }
    }

}
