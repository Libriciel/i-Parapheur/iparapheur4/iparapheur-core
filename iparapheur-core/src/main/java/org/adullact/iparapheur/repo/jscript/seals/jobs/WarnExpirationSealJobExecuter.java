/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.adullact.iparapheur.repo.jscript.seals.jobs;

import org.adullact.iparapheur.repo.jscript.seals.SealCertificate;
import org.adullact.iparapheur.repo.jscript.seals.SealUtils;
import org.adullact.iparapheur.repo.template.TemplateUtils;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.*;

/**
 * Créé par lhameury le 5/10/17.
 */
public class WarnExpirationSealJobExecuter {

    private static final String TEMPLATE_NAME = "parapheur-seals-warn.ftl";
    private static final String SUBJECT = "[i-Parapheur] Alerte d'expiration de certificat cachet serveur";

    private static final Logger logger = Logger.getLogger(WarnExpirationSealJobExecuter.class);

    /**
     * Public API access
     */
    private ServiceRegistry serviceRegistry;
    private int daysUntilExpiration;

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    /**
     * Executer implementation
     */
    public void execute() {
        // Here we need a retrying transaction because of nodeservice usage
        serviceRegistry.getRetryingTransactionHelper().doInTransaction(
                new RetryingTransactionHelper.RetryingTransactionCallback<Object>() {
                    @Override public Object execute() throws Throwable {

                        List<SealCertificate> listSeals = SealUtils.listSealCertificates(serviceRegistry);
                        // We check that we have at least one seal before sending mail
                        Map<String, Object> model = createEmailTemplateModel(listSeals);

                        String templateStr = TemplateUtils.getTemplateFromName(TEMPLATE_NAME, serviceRegistry);
                        String mailForWarn = SealUtils.getMailForWarnMail(serviceRegistry);

                        if (model != null && mailForWarn != null) {
                            sendMail(model, templateStr, Arrays.asList(mailForWarn.split("(,)|(;)")));
                        }

                        return null;
                    }
                });
    }

    private void sendMail(@NotNull Map<String, Object> model, @NotNull String templateStr, @NotNull List<String> recipients) {

        String mailToSend = serviceRegistry.getTemplateService().processTemplate("freemarker", templateStr, model);

        Map<String, Serializable> aParams = new HashMap<String, Serializable>();
        //aParams.put("to_many", (Serializable) recipients);
        aParams.put("subject", SUBJECT);
        aParams.put("text", mailToSend);

        // FIXED IN parapheur v5.0
        // Contournement moche pour limitation alfresco... Voir :
        // https://community.alfresco.com/thread/178024-mailactionexecuterparamtomany-parameter-not-working
        for(String recipient : recipients) {
            aParams.put("to", recipient);

            Action a = serviceRegistry.getActionService().createAction("mail", aParams);
            if (a != null) {
                serviceRegistry.getActionService().executeAction(a, null);
            } else {
                throw new RuntimeException("Could not create mail action");
            }
        }
    }

    @Nullable Map<String, Object> createEmailTemplateModel(@NotNull List<SealCertificate> seals) {
        Map<String, Object> model = new HashMap<>();

        // -1 property is none !
        if (daysUntilExpiration == -1) return null;

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, daysUntilExpiration);
        long daysUntilExpirationDate = cal.getTime().getTime();

        List<SealCertificate> modelSeals = new ArrayList<>();

        logger.debug("Filtering seals with " + daysUntilExpiration + " days = " + daysUntilExpirationDate);

        // PATCH moche pour freemarker 2.3.16, qui ne supporte pas la fonction "number_to_date"
        Map<String, Date> datemap = new HashMap<>();

        // We keep seals that have its expiry date between now and 'daysUntilExpiration' days
        for (SealCertificate sealCertificate : seals) {

            logger.debug("Cert : " + sealCertificate.getTitle() + ", date : " + sealCertificate.getDescription().getNotAfter());

            if (sealCertificate.getDescription().getNotAfter() < daysUntilExpirationDate || daysUntilExpiration == 0) {
                // We have an expired certificate...
                modelSeals.add(sealCertificate);
                datemap.put(sealCertificate.getTitle(), new Date(sealCertificate.getDescription().getNotAfter()));
            }
        }

        if (modelSeals.isEmpty()) return null;

        model.put("seals", modelSeals);
        model.put("datemap", datemap);

        if (daysUntilExpiration > 0) {
            model.put("daysUntilExpiration", daysUntilExpiration);
        }

        return model;
    }

    public void setDaysUntilExpiration(int daysUntilExpiration) {
        this.daysUntilExpiration = daysUntilExpiration;
    }
}
