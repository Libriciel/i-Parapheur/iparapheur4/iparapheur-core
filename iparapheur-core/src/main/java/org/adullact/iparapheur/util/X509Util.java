/*
 * Version 3.0
 * CeCILL Copyright (c) 2006-2014, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by ADULLACT-projet S.A.
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.util;

import org.apache.commons.ssl.KeyMaterial;
import org.apache.log4j.Logger;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.cert.X509CertificateHolder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Utilitaires de manipulation de certificats X509v3
 * 
 * @author Vivien Barousse
 * @author Stephane Vast  - Adullact Projet
 */
public class X509Util {

    public static final String beginString = "-----BEGIN CERTIFICATE-----";
    public static final String endString   = "-----END CERTIFICATE-----";

    private static final Logger logger = Logger.getLogger(X509Util.class);

    public static final String CERT_POLICY_EXTENSION_OID = "2.5.29.32";
    public static final String MIMETYPE_X509_CA_CERT = "application/x-x509-ca-cert";

    /**
     * Construit une map<String, String> contenant les elements de cert policy
     * contenues dans le certificat passé en paramètre
     * 
     * @param cert le certificat X509 à analyser
     * @return en key de map: pPolicyIdentifierID, pSPURI, pPolicyidentifierDescription
     */
    public static Map<String, String> getPolicyProperties(X509Certificate cert) {
        Map<String, String> properties = new HashMap<String, String>();
        if (cert == null) {
            return properties;
        }

        byte[] extensionValue = cert.getExtensionValue(CERT_POLICY_EXTENSION_OID);

        if (extensionValue == null) {
            return properties;
        }

        try {
            byte[] bValue = ((ASN1OctetString) ASN1Object.fromByteArray(extensionValue)).getOctets();

            ASN1Sequence pSeq = (ASN1Sequence) ASN1Sequence.fromByteArray(bValue);

            for (int i = 0, len = pSeq.size(); i < len; i++) {
                PolicyInformation pi = PolicyInformation.getInstance(pSeq.getObjectAt(i));
                String piId = pi.getPolicyIdentifier().getId();

                properties.put("pPolicyIdentifierID", piId);

                ASN1Sequence pQuals;
                if ((pQuals = pi.getPolicyQualifiers()) != null) {

                    for (int j = 0, plen = pQuals.size(); j < plen; j++) {
                        ASN1Sequence pqi = (ASN1Sequence) pQuals.getObjectAt(j);
                        String pqId = ((DERObjectIdentifier) pqi.getObjectAt(0)).getId();

                        DEREncodable d = pqi.getObjectAt(1);
                        if (pqId.equals("1.3.6.1.5.5.7.2.1")) {
                            String sUri = ((DERString) d).getString();
                            properties.put("pSPURI", sUri);
                        } else if (pqId.equals("1.3.6.1.5.5.7.2.2")) {
                            ASN1Sequence un = (ASN1Sequence) d;

                            for (int k = 0, dlen = un.size(); k < dlen; k++) {
                                DEREncodable de = un.getObjectAt(k);
                                properties.put("pPolicyIdentifierDescription", ((DERString) de).getString());
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Error getting policy properties from certificate", e);
        }

        return properties;
    }

    /**
     * Construit une map avec des propriétés usuelles en clair contenues dans
     * un certificat X509. La liste comprend:
     * <ul><li>subject_name</li>
     *     <li>issuer_name</li>
     *     <li>email (si dispo, "_inconnu_" sinon)</li>
     *     <li>organization (si dispo, "_inconnu_" sinon)</li>
     *     <li>title (si dispo, "_inconnu_" sinon)</li>
     *     <li>certificate_valid_from</li>
     *     <li>certificate_valid_to</li>
     * </ul>
     * 
     * @param certHolder le X509CertificateHolder à lire
     * @return les propriétés en clair
     */
    public static Map<String, String> getUsefulCertProps(X509CertificateHolder certHolder) {
        Map<String, String> props = new HashMap<String, String>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy 'à' HH:mm", Locale.FRENCH);

        props.put("issuer_name", extractCNIssuer(certHolder.getIssuer().toString()));
        String subjectDN = certHolder.getSubject().toString();
        if (logger.isDebugEnabled()) {
            logger.debug("X509Util::getUsefulCertProps subjectDN="+ subjectDN);
        }
        props.put("subject_name", extractCN(subjectDN));
        String email = extractE(subjectDN);
        if("_inconnu_".equals(email)) {
            props.put("email", extractEmail(subjectDN));
        } else {
            props.put("email", email);
        }
        // props.put("email", extractE(subjectDN));
        props.put("organization", extractO(subjectDN));
        props.put("title", extractT(subjectDN));
        props.put("certificate_valid_from", sdf.format(certHolder.getNotBefore()));
        props.put("certificate_valid_to", sdf.format(certHolder.getNotAfter()));
        return props;
    }

    /**
     * Construit une map avec des propriétés usuelles en clair contenues dans
     * un certificat X509.
     * <ul><li>subject_name</li>
     *     <li>issuer_name</li>
     *     <li>email (si dispo, "_inconnu_" sinon)</li>
     *     <li>organization (si dispo, "_inconnu_" sinon)</li>
     *     <li>title (si dispo, "_inconnu_" sinon)</li>
     *     <li>certificate_valid_from</li>
     *     <li>certificate_valid_to</li>
     * </ul>
     * 
     * @param cert le X509Certificate à lire
     * @return les propriétés en clair
     */
    public static Map<String, String> getUsefulCertProps(X509Certificate cert) {
        Map<String, String> props = new HashMap<String, String>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy 'à' HH:mm", Locale.FRENCH);

        props.put("issuer_name", extractCNIssuer(cert.getIssuerX500Principal().getName()));
        String subjectDN = cert.getSubjectX500Principal().toString();
        if (logger.isDebugEnabled()) {
            logger.debug("X509Util::getUsefulCertProps subjectDN="+ subjectDN);
        }
        HashMap<String, String> subjectHashMap = dnTokenizer(subjectDN);
        if (subjectHashMap.containsKey("CN")) {
            props.put("subject_name", subjectHashMap.get("CN"));
        } else {
            props.put("subject_name", extractCN(subjectDN));
        }
        if (subjectHashMap.containsKey("EMAILADDRESS")) {
            props.put("email", subjectHashMap.get("EMAILADDRESS"));
        } else {
            props.put("email", extractEmail(subjectDN));
        }
        if (subjectHashMap.containsKey("O")) {
            props.put("organization", subjectHashMap.get("O"));
        } else {
            props.put("organization", extractO(subjectDN));
        }
        if (subjectHashMap.containsKey("T")) {
            props.put("title",subjectHashMap.get("T"));
        } else {
            props.put("title", extractT(subjectDN));
        }
        props.put("certificate_valid_from", sdf.format(cert.getNotBefore()));
        props.put("certificate_valid_to", sdf.format(cert.getNotAfter()));
        return props;
    }

    /**
     * Fournit une représentation Clé, Valeur d'une ligne
     * SUBJECT ou ISSUER d'un certificat.
     * @param in la chaine concernée
     * @return la hashmap K,V
     */
    private static HashMap<String, String> dnTokenizer(String in) {
        HashMap<String, String> retVal = new HashMap<String, String>();
        int idx = 0;

        String buffer = "";
        String[] currentPair = new String[2];
        boolean ignoreComma = false;

        while (idx < in.length()) {
            if (in.charAt(idx) == '=') {
                currentPair[0] = buffer;
                buffer = "";
                idx++;
                continue;
            }
            if (in.charAt(idx) == '"') {
                ignoreComma = !ignoreComma;
                idx++;
                continue;
            }
            /*
             * Gestion de la seconde manière d'échapper les virgules
             */
            if (idx + 1 < in.length() && in.charAt(idx) == '\\' && in.charAt(idx + 1) == ',') {
                idx += 2;
                buffer = buffer + ",";
                continue;
            }
            if (!ignoreComma && in.charAt(idx) == ',') {

                currentPair[1] = buffer;
                retVal.put(currentPair[0], currentPair[1]);

                currentPair = new String[2];
                buffer = "";
                idx++;

                // On ignore les espaces en debut de clef
                while (in.charAt(idx) == ' ') {
                    idx++;
                }
                continue;
            }

            buffer = buffer + in.charAt(idx);
            idx++;
        }

        /* Si le buffer n'est pas vide c'est qu'il nous reste la derniere valeur
         * à récuperer.
         */
        if (buffer.length() > 0) {
            currentPair[1] = buffer;
            retVal.put(currentPair[0], currentPair[1]);
        }

        return retVal;
    }

    /**
     * Extrait le champ CN d'un libellé DN (distinguished name)
     * 
     * @param dn le distinguished name (champ de certificat Issuer ou Subject)
     * @return la valeur du sous-champ 'CN'
     */
    public static String extractCN(String dn) {
        if (logger.isDebugEnabled()) {
            logger.debug("extractCN: [" + dn + "], length=" + dn.length());
        }
        if (dn == null || dn.length() < 4) {
            return "_inconnu_";
        }
        int i = dn.indexOf("CN=");
        if (i < 0) {
            return "_inconnu_";
        }
        i += 3;
        int j = dn.indexOf(",", i);
        if (j == -1) {
            // 'CN=' is in last position, no ',' behind !
            return dn.substring(i).trim();
        } else if (j - 1 < 0) {
            return "_inconnu_";
        }
        return dn.substring(i, j).trim();
    }

    /**
     * Extrait le champ CN d'un libellé DN (distinguished name)
     *   warn: ça ressemble vachement à "extractCN" ==> doublon??
     * 
     * @param dn le distinguished name (champ de certificat Issuer ou Subject)
     * @return la valeur du sous-champ 'CN'
     */
    public static String extractCNIssuer(String dn) {
        if (dn == null || dn.length() < 4) {
            return "_inconnu_";
        }
        int i = dn.indexOf("CN=");
        if (i < 0) {
            return "_inconnu_";
        }
        i += 3;
        int j = dn.indexOf(",", i);
        // System.out.print("j=" + j);
        if (j == -1) {
            // 'CN=' is in last position, no ',' behind !
            //System.out.print("substring = [" + dn.substring(i) + "]");
            return dn.substring(i).trim();
        } else if (j - 1 < 0) {
            return "_inconnu_";
        }
        return dn.substring(i, j).trim();
    }

    public static String extractO(String dn) {
        if (dn == null || dn.length() < 3) {
            return "_inconnu_";
        }
        int i = dn.indexOf("O=");
        if (i < 0) {
            return "_inconnu_";
        }
        i += 2;
        int j = dn.indexOf(",", i);
        if (j == -1) {
            // 'O=' is in last position, no ',' behind !
            return dn.substring(i).trim();
        } else if (j - 1 < 0) {
            return "_inconnu_";
        }
        return dn.substring(i, j).trim();
    }

    public static String extractT(String dn) {
        if (logger.isDebugEnabled()) {
            logger.debug("extractT: [" + dn + "], length=" + dn.length());
        }
        if (dn == null || dn.length() < 3) {
            return "_inconnu_";
        }
        int i = dn.indexOf("T=");
        if (i < 0) {
            return "_inconnu_";
        }
        i += 2;
        int j = dn.indexOf(",", i);
        if (j == -1) {
            // 'T=' is in last position, no ',' behind !
            return dn.substring(i).trim();
        } else if (j - 1 < 0) {
            return "_inconnu_";
        }
        return dn.substring(i, j).trim();
    }

    public static String extractE(String dn) {
        if (logger.isDebugEnabled()) {
            logger.debug("extractE: [" + dn + "], length=" + dn.length());
        }
        if (dn == null || dn.length() < 3) {
            return "_inconnu_";
        }
        int i = dn.indexOf("E=");
        if (i < 0) {
            return "_inconnu_";
        }
        i += 2;
        int j = dn.indexOf(",", i);
        if (j == -1) {
            // 'E=' is in last position, no ',' behind !
            return dn.substring(i).trim();
        } else if (j - 1 < 0) {
            return "_inconnu_";
        }
        return dn.substring(i, j).trim();
    }

    public static String extractEmail(String dn) {
        if (logger.isDebugEnabled()) {
            logger.debug("extractEmail: [" + dn + "], length=" + dn.length());
        }
        if (dn == null || dn.length() < 4) {
            return "_inconnu_";
        }
        int i = dn.indexOf("EMAILADDRESS=");
        if (i < 0) {
            return "_inconnu_";
        }
        i += 13;
        int j = dn.indexOf(",", i);
        if (j == -1) {
            // 'EMAILADDRESS=' is in last position, no ',' behind !
            return dn.substring(i).trim();
        } else if (j - 1 < 0) {
            return "_inconnu_";
        }
        return dn.substring(i, j).trim();
    }

    public static X509Certificate getX509CertificateFromString(String certString) {
        X509Certificate cert = null;
        if (certString != null && !certString.trim().isEmpty()) {

            try {
                CertificateFactory factory = CertificateFactory.getInstance("X.509");
                String cleancontent;
                if (certString.contains(beginString)) {
                    int beginIndex = certString.indexOf(beginString);
                    int endIndex = certString.indexOf(endString);
                    cleancontent = certString.substring(beginIndex, endIndex + endString.length());
                } else {
                    cleancontent = certString;
                }
                Collection<? extends Certificate> certs = 
                        factory.generateCertificates(new ByteArrayInputStream(cleancontent.getBytes()));
                if ((certs != null) && (certs.size() > 0)) {
                    cert = (X509Certificate) certs.toArray()[0];
                }
            } catch (CertificateException e) { /* No need */ }
        }
        return cert;
    }

    public static String getUniqueId(X509Certificate cert) {
        if (cert != null) {
            return cert.getSerialNumber().toString() + "/" + cert.getIssuerDN().toString();
        }
        return null;
    }

    /**
     * Tests PKCS#12 / pwd couple.
     *
     * @param is PKCS#12 certificate, this method does not close it.
     * @param pwdString certificate password
     * @return ok, ko, ou expire
     */
    public static @NotNull String checkPasswordForCertificate(@Nullable InputStream is, @NotNull String pwdString) {
        String res = "ko";

        // Default case

        if (is == null) {
            logger.error("No certificate found");
            return res;
        }

        // Tests

        try {
            KeyMaterial km = new KeyMaterial(is, pwdString.toCharArray());
            //noinspection ConstantConditions
            if (km != null) {
                if (km.getKeyStore() != null) {
                    List<String> my = java.util.Collections.list(km.getKeyStore().aliases());
                    if (!my.isEmpty()) {
                        res = "ok";
                        for (String alias : my) {
                            X509Certificate xcert = (X509Certificate) km.getKeyStore().getCertificate(alias);
                            try {
                                xcert.checkValidity();
                            } catch (java.security.cert.CertificateExpiredException ex) {
                                logger.error("Open PKCS#12 impossible, CertificateExpiredException " + ex.getLocalizedMessage());
                                res = "ex";
                            } catch (java.security.cert.CertificateNotYetValidException ex) {
                                logger.error("Open PKCS#12 impossible, CertificateNotYetValidException " + ex.getLocalizedMessage());
                                res = "ex";
                            }
                            SimpleDateFormat sdf = new SimpleDateFormat("E dd MMM yyyy 'à' HH:mm", java.util.Locale.FRENCH);
                            res += sdf.format(xcert.getNotAfter());
                        }
                        logger.info("Cert is good !, sending to display '" + res + "'");
                    } else {
                        logger.error("No certificate found, km.getCertificates() for ('" + pwdString + "') is empty.");
                    }
                } else {
                    logger.error("No certificate found, as KeyMaterial for ('" + pwdString + "') has no keystore.");
                }
            } else {
                logger.error("No certificate found, KeyMaterial for certificate', '" + pwdString + "') is null.");
            }
        } catch (java.io.IOException ex) {
            logger.error("Open PKCS#12 impossible, IOException", ex);
        } catch (java.security.cert.CertificateExpiredException ex) {
            logger.error("Open PKCS#12 impossible, CertificateExpiredException " + ex.getLocalizedMessage());
            res = "expire";
        } catch (java.security.cert.CertificateNotYetValidException ex) {
            logger.error("Open PKCS#12 impossible, CertificateNotYetValidException " + ex.getLocalizedMessage());
            res = "expire";
        } catch (java.security.cert.CertificateException ex) {
            logger.error("Open PKCS#12 impossible, CertificateException");
        } catch (java.security.KeyStoreException ex) {
            logger.error("Open PKCS#12 impossible, KeyStoreException");
        } catch (java.security.NoSuchAlgorithmException ex) {
            logger.error("Open PKCS#12 impossible, NoSuchAlgorithmException");
        } catch (java.security.UnrecoverableKeyException ex) {
            logger.error("Open PKCS#12 impossible, UnrecoverableKeyException");
        } catch (java.lang.Exception ex) {
            logger.error("Open PKCS#12 impossible, GenericException " + ex.getLocalizedMessage(), ex);
        }

        return res;
    }
}
