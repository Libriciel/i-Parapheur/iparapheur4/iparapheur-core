/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.adullact.iparapheur.util;

import com.atolcd.parapheur.repo.CustomMetadataDef;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.axis.utils.StringUtils;
import org.apache.commons.collections.Predicate;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains a set of utility methods for the collection framework.
 * 
 * @author Vivien Barousse
 */
public class CollectionsUtils {

    public static <T> boolean containsOneOf(@NotNull Collection<T> coll, @NotNull Collection<T> oneOf) {
        for (T t : oneOf) {
            if (coll.contains(t)) {
                return true;
            }
        }
        
        return false;
    }

    public static <K, V> void filter(@Nullable Map<K, V> map, @Nullable Predicate predicate) {

        if ((map == null) || (predicate == null)) {
            return;
        }

        for (Iterator<Map.Entry<K, V>> i = map.entrySet().iterator(); i.hasNext();) {
            Map.Entry<K, V> entry = i.next();
            if (!predicate.evaluate(entry)) {
                i.remove();
            }
        }
    }

    /**
     * Null-safe check if the specified map is empty.
     * <p/>
     * Null returns true.
     *
     * @param map the map to check, may be null
     * @return true if empty or null
     * @since Commons Collections 3.2
     */
    public static boolean isEmpty(@Nullable Map map) {
        return (map == null || map.isEmpty());
    }

    /**
     * Gets a String from a Map in a null-safe manner.
     * <p/>
     * The String is obtained via <code>toString</code>.
     *
     * @param map the map to use
     * @param key the key to look up
     * @return the value in the Map as a String, <code>null</code> if null map input
     */
    public static String getString(@Nullable final Map map, @NotNull final Object key) {
        if (map != null) {
            Object answer = map.get(key);
            if (answer != null) {
                return answer.toString();
            }
        }
        return null;
    }

    /**
     * Looks up the given key in the given map, converting the result into
     * a string, using the default value if the the conversion fails.
     *
     * @param map          the map whose value to look up
     * @param key          the key of the value to look up in that map
     * @param defaultValue what to return if the value is null or if the
     *                     conversion fails
     * @return the value in the map as a string, or defaultValue if the
     * original value is null, the map is null or the string conversion
     * fails
     */
    public static String getString(@Nullable Map map, @NotNull Object key, @Nullable String defaultValue) {
        String answer = getString(map, key);
        if (answer == null) {
            answer = defaultValue;
        }
        return answer;
    }

    public static @NotNull Map<QName, Serializable> createQNameProperties(@NotNull Map<String, Serializable> newProperties,
                                                                          @NotNull DictionaryService dictionaryService,
                                                                          @NotNull NamespaceService namespaceService) {

        Map<QName, Serializable> resultProperties = new HashMap<QName, Serializable>();

        DataTypeDefinition def;
        QName typeQ;
        PropertyDefinition defProperty;
        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);

        for (String key : newProperties.keySet()) {
            defProperty = dictionaryService.getProperty(QName.createQName(key, namespaceService));
            def = defProperty.getDataType();

            if (def != null) {
                typeQ = def.getName();

                if (typeQ.isMatch(DataTypeDefinition.TEXT) || typeQ.isMatch(DataTypeDefinition.MLTEXT)) {
                    resultProperties.put(QName.createQName(key, namespaceService), newProperties.get(key));
                } else if (typeQ.isMatch(DataTypeDefinition.INT)) {
                    try {
                        resultProperties.put(QName.createQName(key, namespaceService), format.parse((String) newProperties.get(key)).intValue());
                    } catch (Exception ex) {
                        resultProperties.put(QName.createQName(key, namespaceService), null);
                    }
                } else if (typeQ.isMatch(DataTypeDefinition.DOUBLE)) {
                    try {
                        resultProperties.put(QName.createQName(key, namespaceService),
                                          format.parse(((String) newProperties.get(key)).replace(".", ",")).doubleValue());
                    } catch (Exception ex) {
                        resultProperties.put(QName.createQName(key, namespaceService), null);
                    }
                } else if (typeQ.isMatch(DataTypeDefinition.DATE)) {
                    try {
                        Date d = new Date();
                        d.setTime(Long.parseLong((String) newProperties.get(key)));
                        resultProperties.put(QName.createQName(key, namespaceService), d);
                    } catch (Exception e) {
                        //logger.warn("Can't parse date for " + QName.createQName(key, namespaceService) + ", reason : ", e);
                    }
                } else if (typeQ.isMatch(DataTypeDefinition.BOOLEAN)) {
                    try {
                        if (!StringUtils.isEmpty((String) newProperties.get(key))) {
                            resultProperties.put(QName.createQName(key, namespaceService), Boolean.valueOf((String) newProperties.get(key)));
                        } else {
                            resultProperties.put(QName.createQName(key, namespaceService), null);
                        }
                    } catch (Exception ex) {
                        resultProperties.put(QName.createQName(key, namespaceService), null);
                    }
                } else if(typeQ.isMatch(DataTypeDefinition.NODE_REF)) {
                    resultProperties.put(QName.createQName(key, namespaceService), newProperties.get(key));
                }else {
                    throw new RuntimeException("Unknown type: " + typeQ);
                }
            } else {
                resultProperties.put(QName.createQName(key, namespaceService), newProperties.get(key));
            }
            // Si c'est une métadonnée custom et qu'elle est vide, on la supprime
            //OUPA pour la sélection de circuit !
            /*if (properties.get(key).isEmpty() && key.contains(MetadataServiceImpl.METADATA_DEF_CUSTOM_PREFIX)) {
                QName propQName = QName.createQName(key, namespaceService);

                oldProperties.remove(propQName);
                resultProperties.remove(propQName);
            }*/
        }

        return resultProperties;
    }

    /**
     * Split <code>KEY1="value 1", KEY2="value 2", KEY3="value 3"</code> into a proper Map
     *
     * @param metadata the String to parse
     * @return Map, may be empty but never null
     */
    public static @NotNull Map<String, String> parseMetadata(@Nullable String metadata) {
        Map<String, String> ret = new HashMap<String, String>();

        if (metadata != null && !metadata.isEmpty()) {

            // Regex : (\w+)\s*=\s*"(.*?)(?<!\\)"
            //         (\w+)       catch a word, at least one char long
            //         \s*=\s*"    followed by any spaces, an =, any spaces, and a quote
            //         (.*?)       catch anything, not greedy
            //         (?<!\\)"    followed by a quote not preceeded by \ (to skip escaped ones)
            Pattern p = Pattern.compile("(\\w+)\\s*=\\s*\"(.*?)(?<!\\\\)\"");
            Matcher m = p.matcher(metadata);

            while (m.find()) {
                ret.put(m.group(1), String.valueOf(m.group(2)).replace("\\\"", "\""));
            }
        }

        return ret;
    }

    public static @NotNull HashMap<QName, CustomMetadataDef> metadataMapFromList(@Nullable List<CustomMetadataDef> metadataDefList) {

        HashMap<QName, CustomMetadataDef> result = new HashMap<QName, CustomMetadataDef>();

        if (metadataDefList != null) {
            for (CustomMetadataDef metadataDef : metadataDefList) {
                result.put(metadataDef.getName(), metadataDef);
            }
        }

        return result;
    }
}
