/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.adullact.iparapheur.util;

import org.apache.log4j.Logger;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.MessageDigest;
import java.util.Arrays;

/**
 * Classe utilisée pour le chiffrement d'informations privées à l'aide d'une clé sous forme de text
 * (placée en fichier de propriété)
 *
 * Créé par lhameury le 4/18/17.
 */
public class EncryptUtils {

    private static Logger log = Logger.getLogger(EncryptUtils.class);

    private static Key generateKeyFromString(final String secKey) throws Exception {
        byte[] key = secKey.getBytes("UTF-8");
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16); // use only first 128 bit

        return new SecretKeySpec(key, "AES");
    }

    public static String encrypt(final String valueEnc, final String secKey) {

        String encryptedVal = null;

        try {
            final Key key = generateKeyFromString(secKey);
            final Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.ENCRYPT_MODE, key);
            final byte[] encValue = c.doFinal(valueEnc.getBytes());
            encryptedVal = new BASE64Encoder().encode(encValue);
        } catch (Exception ex) {
            log.error("EncryptionUtils : encrypt error : " + ex.getMessage());
            ex.printStackTrace();
        }

        return encryptedVal;
    }

    public static String decrypt(final String encryptedValue, final String secretKey) {

        String decryptedValue = null;

        try {

            final Key key = generateKeyFromString(secretKey);
            final Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, key);
            final byte[] decorVal = new BASE64Decoder().decodeBuffer(encryptedValue);
            final byte[] decValue = c.doFinal(decorVal);
            decryptedValue = new String(decValue);
        } catch (Exception ex) {
            log.error("EncryptionUtils : decrypt error : " + ex.getMessage());
            ex.printStackTrace();
        }

        return decryptedValue;
    }
}
