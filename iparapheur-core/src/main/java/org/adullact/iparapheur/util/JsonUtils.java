package org.adullact.iparapheur.util;

import com.atolcd.parapheur.model.ParapheurErrorCode;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtils {

    private static final String HTTP_RETURN_STATUS_CODE_KEY = "statusCode";
    private static final String HTTP_RETURN_ERROR_MESSAGE_KEY = "errorMessage";

    public static @NotNull JSONObject createHttpStatusJsonObject(int statusCode, @NotNull ParapheurErrorCode errorCode) {
        return createHttpStatusJsonObject(statusCode, String.valueOf(errorCode.getCode()));
    }

    public static @NotNull JSONObject createHttpStatusJsonObject(int statusCode, int errorCode) {
        return createHttpStatusJsonObject(statusCode, String.valueOf(errorCode));
    }

    public static @NotNull JSONObject createHttpStatusJsonObject(int statusCode, @NotNull String defaultMessage) {
        JSONObject result = new JSONObject();

        try {
            result.put(HTTP_RETURN_STATUS_CODE_KEY, statusCode);
            result.put(HTTP_RETURN_ERROR_MESSAGE_KEY, defaultMessage);
        }
        catch (JSONException e) { e.printStackTrace(); }

        return result;
    }

}
