package org.adullact.iparapheur.util;

import org.adullact.iparapheur.repo.jscript.NativeObjectMapAdapter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.NativeObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: lhameury
 * Date: 15/10/13
 * Time: 10:04
 */
public class NativeUtils {

    public static Map<String, Serializable> nativeToProperties(NativeObject nativeObject) {
        //noinspection MismatchedQueryAndUpdateOfCollection
        NativeObjectMapAdapter mapAdapter = new NativeObjectMapAdapter(nativeObject);
        Map<String, Serializable> properties = new HashMap<String, Serializable>();

        for (Object key : mapAdapter.keySet()) {
            Serializable prop;
            if(mapAdapter.get(key) instanceof Boolean) {
                prop = mapAdapter.get(key).toString();
            } else if (mapAdapter.get(key) instanceof Double) {
                prop = Long.toString(((Double)mapAdapter.get(key)).longValue());
            } else if (mapAdapter.get(key) instanceof NativeArray) {
                prop = (Serializable) nativeArrayToNodeRef((NativeArray) mapAdapter.get(key));
            } else {
                prop = (String) mapAdapter.get(key);
            }
            properties.put((String) key, prop);
        }
        return properties;
    }

    public static List<Object> nativeArrayToObject(NativeArray array) {
        List<Object> retVal = new ArrayList<Object>();
        for (int i = 0; i < array.getLength(); i++) {
            try {
                retVal.add(array.get(i, null));
            } catch(Exception e) {
                System.out.println("Some NativeArray properties aren't objects");
            }
        }
        return retVal;
    }

    public static List<String> nativeArrayToString(NativeArray array) {
        List<String> retVal = new ArrayList<String>();
        for (int i = 0; i < array.getLength(); i++) {
            try {
                retVal.add((String) array.get(i, null));
            } catch(Exception e) {
                System.out.println("Some NativeArray properties are objects");
            }
        }
        return retVal;
    }

    public static List<NodeRef> nativeArrayToNodeRef(NativeArray array) {
        List<NodeRef> retVal = new ArrayList<NodeRef>();
        for (int i = 0; i < array.getLength(); i++) {
            try {
                retVal.add(new NodeRef("workspace://SpacesStore/"+ array.get(i, null)));
            } catch(Exception e) {
                System.out.println("Some NativeArray properties are objects");
            }
        }
        return retVal;
    }

    public static List<String> NodeRefListToId(Object[] array) {
        List<String> retVal = new ArrayList<String>();
        for (Object anArray : array) {
            retVal.add(((NodeRef) anArray).getId());
        }
        return retVal;
    }
}
