/*
 * iParapheur-Server
 * Copyright (C) 2017 Libriciel
 * Original work (C) Bruno Lowagie ("iText in Action - 2nd Edition")
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.util;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.security.*;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import coop.libriciel.crypto.model.stamp.PdfSignatureStampElement;
import org.alfresco.util.TempFileProvider;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.util.List;

public class PdfSealUtils {

    private static Logger log = Logger.getLogger(PdfSealUtils.class);

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public static String getNewSignatureField(PdfReader reader) {
        String field = "Signature";
        int index = 1;
        while(reader.getAcroFields().doesSignatureFieldExist(field + (index))) { index += 1; }
        return field + index;
    }

    public static File addSignatureField(PdfReader reader, Rectangle position, String field, int page) throws IOException, DocumentException {
        // On doit d'abord générer un champ de formulaire pour y placer la signature.
        // La fonction itext relative ne fonctionne pas on doit donc faire ça à la main
        File tempFile = TempFileProvider.createTempFile("tmpPades", ".pdf");

        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(tempFile), '\0', true);

        PdfFormField signatureField = PdfFormField.createSignature(stamper.getWriter());
        signatureField.setWidget(position, null);
        signatureField.setFlags(PdfAnnotation.FLAGS_PRINT);
        signatureField.setFieldName(field);
        signatureField.setPage(page);
        stamper.addAnnotation(signatureField, page);

        stamper.close();

        return tempFile;
    }

    /**
     * Manipulates a PDF file src with the file dest as result
     *
     * @param src  the original PDF
     * @param dest the resulting PDF
     * @throws GeneralSecurityException
     * @throws IOException
     * @throws DocumentException
     * @throws FileNotFoundException
     * @throws KeyStoreException
     */
    public static void signPdf(@NotNull String src, @NotNull String dest, @NotNull String p12Path,
                               @NotNull String p12Password, @NotNull String p12AliasPassword, boolean certified,
                               boolean graphic, Rectangle position, int page, Image image, String textToAdd) throws GeneralSecurityException, IOException, DocumentException {

        // Private key and certificate
        KeyStore ks = KeyStore.getInstance("PKCS12");

        log.debug(p12Path + " " + p12Password);
        InputStream is = new FileInputStream(p12Path);
        ks.load(is, p12Password.toCharArray());
        String alias = ks.aliases().nextElement();
        PrivateKey pk = (PrivateKey) ks.getKey(alias, p12AliasPassword.toCharArray());
        Certificate[] chain = ks.getCertificateChain(alias);

        // Reader and stamper

        PdfReader reader = new PdfReader(src);
        int numberPages = reader.getNumberOfPages();
        int realPage = page < 1 || page > numberPages ? numberPages : page;

        List<String> sigs = reader.getAcroFields().getSignatureNames();
        boolean append = !sigs.isEmpty();

        PdfStamper stamper;
        PdfSignatureAppearance appearance;

        OutputStream output = new FileOutputStream(dest);

        // Si le document est déjà signé, on rajoute manuellement le "field" de la signature puis on se met en append mode
        if (append) {
            String field = PdfSealUtils.getNewSignatureField(reader);

            // On doit d'abord générer un champ de formulaire pour y placer la signature.
            // La fonction itext relative ne fonctionne pas on doit donc faire ça à la main
            File tempFile = PdfSealUtils.addSignatureField(reader, position, field, realPage);
            // le document "tempFile" est le nouveau document à signer

            // Ici, on crée l'annotation de signature sur le champ de formulaire créé précédement
            reader = new PdfReader(new FileInputStream(tempFile));
            stamper = PdfStamper.createSignature(reader, output, '\0', null, true);
            appearance = stamper.getSignatureAppearance();
            appearance.setVisibleSignature(field);
        } else {
            stamper = PdfStamper.createSignature(reader, output, '\0');
            appearance = stamper.getSignatureAppearance();
            appearance.setVisibleSignature(position, realPage, null);
        }

        // Appearance
        appearance.setSignatureCreator("i-Parapheur");

//        appearance.setVisibleSignature("mySig");
//        appearance.setReason("It's personal.");
//        appearance.setLocation("Foobar");

        if (certified && reader.getCertificationLevel() == PdfSignatureAppearance.NOT_CERTIFIED) {
            appearance.setCertificationLevel(PdfSignatureAppearance.CERTIFIED_FORM_FILLING);
        }

        if (graphic) {
            if(textToAdd != null) {
                appearance.setLayer2Font(new Font(Font.FontFamily.HELVETICA));
                appearance.setLayer2Text(textToAdd);
            } else {
                appearance.setLayer2Text("");
            }
            if (image != null) {
                appearance.setImage(image);
                // -1 pour scale automatique dans le rectangle défini
                appearance.setImageScale(-1);
            }
            if (position != null) {
                appearance.setVisibleSignature(position, realPage, null);
            }
        }

        // Signature

        ExternalSignature es = new PrivateKeySignature(
                pk,
                DigestAlgorithms.SHA256,
                BouncyCastleProvider.PROVIDER_NAME
        );

        ExternalDigest digest = new BouncyCastleDigest();
        MakeSignature.signDetached(
                appearance,
                digest,
                es,
                chain,
                null,
                null,
                null,
                0,
                CryptoStandard.CADES
        );
    }

}