package org.alfresco.repo.search.impl.lucene.analysis;

import org.apache.lucene.analysis.*;

import java.io.Reader;

/**
 * Created by lhameury on 07/07/16.
 *
 */
public class LowerCaseVerbatimAnalyserThatRemoveAccents extends Analyzer {

    public LowerCaseVerbatimAnalyserThatRemoveAccents() {
    }

    public TokenStream tokenStream(String fieldName, Reader reader) {
        TokenStream result = new WhitespaceTokenizer(reader);
        result = new LowerCaseFilter(result);
        return new ISOLatin1AccentFilter(result);
    }
}
