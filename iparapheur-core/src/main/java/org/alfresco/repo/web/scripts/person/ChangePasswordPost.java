//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.alfresco.repo.web.scripts.person;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.alfresco.repo.security.authentication.AuthenticationException;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.MutableAuthenticationService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;

public class ChangePasswordPost extends DeclarativeWebScript {
    private static final String PARAM_NEWPW = "newpw";
    private static final String PARAM_OLDPW = "oldpw";
    private MutableAuthenticationService authenticationService;
    private AuthorityService authorityService;

    public ChangePasswordPost() {
    }

    public void setAuthenticationService(MutableAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status) {
        if(!req.getContentType().equals("application/json")) {
            throw new WebScriptException(401, "Do not have appropriate auth or wrong auth details provided.");
        }
        String userName = req.getExtensionPath();
        Content c = req.getContent();
        if (c == null) {
            throw new WebScriptException(500, "Missing POST body.");
        } else {
            try {
                JSONObject json = new JSONObject(c.getContent());
                String oldPassword = null;
                boolean isAdmin = this.authorityService.hasAdminAuthority();
                if (!isAdmin || userName.equalsIgnoreCase(this.authenticationService.getCurrentUserName())) {
                    if (!json.has("oldpw") || json.getString("oldpw").length() == 0) {
                        throw new WebScriptException(400, "Old password 'oldpw' is a required POST parameter.");
                    }

                    oldPassword = json.getString("oldpw");
                }

                if (!json.has("newpw") || json.getString("newpw").length() == 0) {
                    throw new WebScriptException(400, "New password 'newpw' is a required POST parameter.");
                }

                String newPassword = json.getString("newpw");
                if (isAdmin && !userName.equalsIgnoreCase(this.authenticationService.getCurrentUserName())) {
                    this.authenticationService.setAuthentication(userName, newPassword.toCharArray());
                } else {
                    this.authenticationService.updateAuthentication(userName, oldPassword.toCharArray(), newPassword.toCharArray());
                }
            } catch (AuthenticationException var9) {
                throw new WebScriptException(401, "Do not have appropriate auth or wrong auth details provided.");
            } catch (JSONException var10) {
                throw new WebScriptException(500, "Unable to parse JSON POST body: " + var10.getMessage());
            } catch (IOException var11) {
                throw new WebScriptException(500, "Unable to retrieve POST body: " + var11.getMessage());
            }

            Map<String, Object> model = new HashMap(1, 1.0F);
            model.put("success", Boolean.TRUE);
            return model;
        }
    }
}
