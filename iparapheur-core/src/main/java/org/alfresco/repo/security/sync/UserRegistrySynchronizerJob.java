package org.alfresco.repo.security.sync;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UserRegistrySynchronizerJob implements Job {
    public UserRegistrySynchronizerJob() {
    }

    public void execute(JobExecutionContext executionContext) throws JobExecutionException {
        final UserRegistrySynchronizer userRegistrySynchronizer = (UserRegistrySynchronizer)executionContext.getJobDetail().getJobDataMap().get("userRegistrySynchronizer");
        final String synchronizeChangesOnly = (String)executionContext.getJobDetail().getJobDataMap().get("synchronizeChangesOnly");
        AuthenticationUtil.runAs(new RunAsWork<Object>() {
            public Object doWork() throws Exception {
                // 2nd parameter set to false, disable removing users from app
                userRegistrySynchronizer.synchronize(synchronizeChangesOnly == null || !Boolean.parseBoolean(synchronizeChangesOnly), false, true);
                return null;
            }
        }, AuthenticationUtil.getSystemUserName());
    }
}
