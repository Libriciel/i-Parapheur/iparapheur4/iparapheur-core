package coop.libriciel.pdfstamp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.pdfstamp.model.StampList;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;

import java.io.*;

/**
 * Créé par lhameury le 18/04/18.
 */
public class PdfStampServiceImpl implements PdfStampService {

    private String connectorHost;
    private String connectorPort;
    private String connectorPath;


    public PdfStampServiceImpl() {
    }

    public String getConnectorHost() {
        return connectorHost;
    }

    public void setConnectorHost(String connectorHost) {
        this.connectorHost = connectorHost;
    }

    public String getConnectorPort() {
        return connectorPort;
    }

    public void setConnectorPort(String connectorPort) {
        this.connectorPort = connectorPort;
    }

    public String getConnectorPath() {
        return connectorPath;
    }

    public void setConnectorPath(String connectorPath) {
        this.connectorPath = connectorPath;
    }

    public File doStamp(File doc, StampList stamps) throws StampException {
        HttpClient httpclient = HttpClients.createDefault();

        HttpPost post = new HttpPost("http://" + connectorHost + ":" + connectorPort + connectorPath);

        post.setEntity(buildEntity(doc, stamps));

        //Execute and get the response.
        try {
            HttpResponse response = httpclient.execute(post);
            HttpEntity entity = response.getEntity();

            if (entity != null && response.getStatusLine().getStatusCode() == 200) {
                copyInputStreamToFile(entity.getContent(), doc);
            } else {
                throw new StampException("Cannot stamp document, see error log on pdf-stamp application");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }

    private void copyInputStreamToFile(InputStream inputStream, File file)
            throws IOException {

        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            IOUtils.copy(inputStream, outputStream);
        }
    }

    private HttpEntity buildEntity(File file, StampList stamp) {
        final MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();

        ObjectMapper mapper = new ObjectMapper();
        try {
            String stampStr  = mapper.writeValueAsString(stamp);

            multipartEntityBuilder.addBinaryBody("file", file);
            multipartEntityBuilder.addTextBody("request", stampStr, ContentType.APPLICATION_JSON);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        return multipartEntityBuilder.build();
    }

}
