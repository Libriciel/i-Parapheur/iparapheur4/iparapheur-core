package coop.libriciel.pdfstamp;

import coop.libriciel.pdfstamp.model.StampList;
import org.alfresco.service.PublicService;

import java.io.File;

@PublicService
public interface PdfStampService {
    File doStamp(File doc, StampList stamps) throws StampException;
}
