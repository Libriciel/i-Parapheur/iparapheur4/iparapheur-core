package coop.libriciel.pdfstamp.model;

import java.util.List;

/**
 * Créé par lhameury le 9/13/17.
 */
public class Stamp {

    public Stamp() {
    }

    private List<Row> rows;
    private int fontSize;
    private float opacity;
    private int spacesBetweenLines;
    private Position position;
    private int page = -1;
    private boolean drawRectangle = true;

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public float getOpacity() {
        return opacity;
    }

    public void setOpacity(float opacity) {
        this.opacity = opacity;
    }

    public int getSpacesBetweenLines() {
        return spacesBetweenLines;
    }

    public void setSpacesBetweenLines(int spacesBetweenLines) {
        this.spacesBetweenLines = spacesBetweenLines;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public boolean isDrawRectangle() {
        return drawRectangle;
    }

    public void setDrawRectangle(boolean drawRectangle) {
        this.drawRectangle = drawRectangle;
    }
}
