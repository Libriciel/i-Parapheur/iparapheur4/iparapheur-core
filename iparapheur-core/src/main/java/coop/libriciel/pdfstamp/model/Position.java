package coop.libriciel.pdfstamp.model;

/**
 * Créé par lhameury le 9/14/17.
 */
public class Position {

    public Position() {
    }

    public enum Placement {
        TOP_RIGHT,
        BOTTOM_LEFT
    }

    private int x;
    private int y;
    private int width;
    private int height;
    private int marginLeft = 5;
    private Placement placement = Placement.TOP_RIGHT;
    private String onText;
    private boolean centered = false;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMarginLeft() {
        return marginLeft;
    }

    public void setMarginLeft(int marginLeft) {
        this.marginLeft = marginLeft;
    }

    public Placement getPlacement() {
        return placement;
    }

    public void setPlacement(Placement placement) {
        this.placement = placement;
    }

    public String getOnText() {
        return onText;
    }

    public void setOnText(String onText) {
        this.onText = onText;
    }

    public boolean isCentered() {
        return centered;
    }

    public void setCentered(boolean centered) {
        this.centered = centered;
    }
}
