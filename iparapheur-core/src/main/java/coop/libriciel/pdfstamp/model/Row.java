package coop.libriciel.pdfstamp.model;


/**
 * Créé par lhameury le 9/13/17.
 */
public class Row {

    public Row() {
    }

    private String title;
    private String value;
    private Logo logo;
    private String color = "black";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Logo getLogo() {
        return logo;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
