package coop.libriciel.pdfstamp.model;

/**
 * Créé par lhameury le 9/15/17.
 */
public class Logo {

    public Logo() {
    }

    private String data;
    private int width;
    private int height;
    private int marginRight;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMarginRight() {
        return marginRight;
    }

    public void setMarginRight(int marginRight) {
        this.marginRight = marginRight;
    }
}
