package coop.libriciel.pdfstamp.model;

import java.util.List;

public class StampList {
    public StampList() {
    }

    private List<Stamp> stamps;

    public List<Stamp> getStamps() {
        return stamps;
    }

    public void setStamps(List<Stamp> stamps) {
        this.stamps = stamps;
    }
}
