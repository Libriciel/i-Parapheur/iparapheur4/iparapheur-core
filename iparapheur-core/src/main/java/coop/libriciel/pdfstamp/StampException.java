package coop.libriciel.pdfstamp;

public class StampException extends Exception {
    public StampException(String s) {
        super(s);
    }
}
