package coop.libriciel.utils.poppler;

public class PopplerCallResult {
    public String output = "";
    public String error = "";
    public int resultCode = 1;
}
