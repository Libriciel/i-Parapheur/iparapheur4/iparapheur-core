package coop.libriciel.utils.poppler;

import lombok.extern.log4j.Log4j;
import org.alfresco.util.TempFileProvider;

import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Log4j
public class PopplerUtils {

    public enum CommandType {
        FILE("-f"),
        COUNT("-c"),
        VALID("-v"),
        SIGNED("-s"),
        ENCRYPTED("-e"),
        LOCATE("-l"),
        PAGE("-p"),
        TEXT("-t"),
        UNLOCK("-u"),
        MERGE("-m"),
        IMAGE("-i"),
        RESOLUTION("-r"),
        OUTPUT("-o");

        private final String value;

        CommandType(String v) {
            value = v;
        }

        public String value() {
            return value;
        }
    }

    private static final String EXECPATH = System.getProperty("alfresco.home") + "/common/lib/libriciel-pdf";

    private boolean testPdf(String path, CommandType testType) {
        PopplerCallResult result = doCall(
                testType.value(),
                CommandType.FILE.value(), path
        );
        if (result.resultCode == 0) {
            return true;
        } else {
            log.warn(result.error);
            return false;
        }
    }

    public boolean isValid(String path) {
        return testPdf(path, CommandType.VALID);
    }

    public boolean isSigned(String path) {
        return testPdf(path, CommandType.SIGNED);
    }

    public boolean isEncrypted(String path) {
        return testPdf(path, CommandType.ENCRYPTED);
    }

    public int getPageCount(String path) {
        int pageCount = 0;
        PopplerCallResult result = doCall(
                CommandType.COUNT.value(),
                CommandType.FILE.value(), path
        );
        if (result.resultCode == 0) {
            try {
                pageCount = Integer.parseInt(result.output.trim());
            } catch (Exception e) {
                log.error("Cannot parse getPageCount return", e);
                log.error(result.output);
            }
        } else {
            log.error(result.error);
            log.error(result.output);
        }
        return pageCount;
    }

    public TagSearchResult getTextLocation(String path, String toFind) {
        TagSearchResult tagSearchResult = new TagSearchResult();
        PopplerCallResult result = doCall(
                CommandType.LOCATE.value(), toFind,
                CommandType.FILE.value(), path
        );

        if (result.resultCode == 0) {
            // Top, Right, Bottom, Left
            String[] rectData = result.output.split(",");
            if (rectData.length != 5) {
                log.error(result.output);
                log.error(result.error);
                return null;
            }
            Rectangle rectangle = new Rectangle();
            rectangle.width = Math.abs(Math.round(Float.parseFloat(rectData[1].trim()) - Float.parseFloat(rectData[3].trim())));
            rectangle.height = Math.round(Float.parseFloat(rectData[0].trim()) - Float.parseFloat(rectData[2].trim()));
            rectangle.x = Math.round(Float.parseFloat(rectData[3].trim()));
            rectangle.y = Math.round(Float.parseFloat(rectData[2].trim()) - Math.abs(rectangle.height));
            tagSearchResult.page = Integer.parseInt(rectData[4].trim()) + 1;
            tagSearchResult.rectangle = rectangle;
            return tagSearchResult;
        } else {
            if (!result.error.isEmpty()) {
                log.error(result.error);
            }
        }

        return null;
    }

    public String getText(String path) {
        PopplerCallResult result = doCall(
                CommandType.TEXT.value(),
                CommandType.FILE.value(), path
        );
        if (result.resultCode == 0) {
            return result.output;
        } else {
            log.error(result.error);
            return "";
        }
    }

    public boolean unlock(String path, String dest) {
        PopplerCallResult result = doCall(
                CommandType.UNLOCK.value(),
                CommandType.FILE.value(), path,
                CommandType.OUTPUT.value(), dest
        );
        if (result.resultCode == 0 && (new File(dest)).length() > 0) {
            return true;
        } else {
            log.error(String.format("Cannot unlock document %s to %s", path, dest));
            log.error(result.error);
            return false;
        }
    }

    public boolean merge(String path, String dest, List<String> tomerge) {
        List<File> printedFiles = new ArrayList<>();
        // We have to use "unlock" function to flat forms fields before merge to avoid missing fields values
        for(String f : tomerge) {
            File tmp = TempFileProvider.createTempFile("beforemerge", ".pdf");
            this.unlock(f, tmp.getAbsolutePath());
            printedFiles.add(tmp);
        }

        PopplerCallResult result = doCall(
                CommandType.MERGE.value(), printedFiles.stream().map(File::getAbsolutePath).collect(Collectors.joining(";")),
                CommandType.FILE.value(), path,
                CommandType.OUTPUT.value(), dest
        );

        // After call, remove all temp files
        //noinspection ResultOfMethodCallIgnored
        printedFiles.forEach(File::delete);

        if (result.resultCode == 0 && (new File(dest)).length() > 0) {
            return true;
        } else {
            log.error(String.format("Cannot merge document %s to %s", path, dest));
            log.error(result.error);
            return false;
        }
    }

    public boolean getImage(String path, int page, String dest, int dpi) {
        PopplerCallResult result = doCall(
                CommandType.IMAGE.value(),
                CommandType.PAGE.value(), String.valueOf(page),
                CommandType.RESOLUTION.value(), String.valueOf(dpi),
                CommandType.FILE.value(), path,
                CommandType.OUTPUT.value(), dest
        );
        if (result.resultCode == 0 && (new File(dest)).length() > 0) {
            return true;
        } else {
            log.error(String.format("Cannot get image for page %s on %s to %s", page, path, dest));
            log.error(result.error);
            return false;
        }
    }

    private PopplerCallResult doCall(String... params) {
        List<String> paramsList = new ArrayList<>(Arrays.asList(params));
        paramsList.add(0, EXECPATH);
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.inheritIO().command(paramsList.toArray(new String[]{}));

        processBuilder.redirectOutput(ProcessBuilder.Redirect.PIPE);

        PopplerCallResult result = new PopplerCallResult();
        try {
            Process process = processBuilder.start();

            StringJoiner output = new StringJoiner("\n");
            StringJoiner outputErr = new StringJoiner("\n");

            StreamGobbler outputStreamGobbler = new StreamGobbler(process.getInputStream(), output::add);
            StreamGobbler errorStreamGobbler = new StreamGobbler(process.getErrorStream(), outputErr::add);

            Thread outThread = new Thread(outputStreamGobbler);
            outThread.start();

            Thread errThread = new Thread(errorStreamGobbler);
            errThread.start();

            boolean ended = process.waitFor(5L, TimeUnit.MINUTES);
            if(ended) {
                result.resultCode = process.exitValue();
            } else {
                log.error("Poppler process TIMEOUT - forcing destroy");
                log.error(Arrays.toString(paramsList.toArray()));
                process.destroyForcibly();
            }

            outThread.join();
            errThread.join();

            result.error = outputErr.toString();
            result.output = output.toString();
        } catch (IOException | InterruptedException e) {
            log.error("Exception catched on poppler call", e);
        }

        return result;
    }
}
