package coop.libriciel.utils.poppler;

import java.awt.*;

public class TagSearchResult {
    public Rectangle rectangle;
    public int page;

    TagSearchResult() {}
}
