package coop.libriciel.crypto.webscript;

import com.atolcd.parapheur.model.ParapheurModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.crypto.model.webscript.DataToSignRequestModel;
import coop.libriciel.crypto.model.webscript.DataToSignResponseModel;
import coop.libriciel.crypto.service.SignatureHandlerProvider;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.eclipse.jetty.http.MimeTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
@Controller
public class PostDataToSignWebscript extends AbstractWebScript {

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private SignatureHandlerProvider signatureHandlerProvider;
    @Autowired
    private ServiceRegistry serviceRegistry;

    @SneakyThrows
    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        webScriptResponse.setStatus(200);
        webScriptResponse.setContentEncoding("UTF-8");
        webScriptResponse.setContentType(MimeTypes.TEXT_JSON);

        DataToSignRequestModel model = mapper.readValue(webScriptRequest.getContent().getContent(), DataToSignRequestModel.class);

        NodeRef dossier = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, webScriptRequest.getServiceMatch().getTemplateVars().get("docid"));
        NodeRef desk = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, webScriptRequest.getServiceMatch().getTemplateVars().get("deskid"));

        List<DataToSignResponseModel> response = signatureHandlerProvider
                .getService((String) this.serviceRegistry.getNodeService().getProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT))
                .getDataToSign(dossier, desk, model);

        webScriptResponse.getWriter().write(mapper.writeValueAsString(response));
    }
}
