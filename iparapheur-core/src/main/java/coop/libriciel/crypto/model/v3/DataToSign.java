package coop.libriciel.crypto.model.v3;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
public class DataToSign {

    private String id;
    private @Deprecated String digestBase64;
    private String dataToSignBase64;
    private @JsonAlias("signatureBase64") String signatureValue;

}