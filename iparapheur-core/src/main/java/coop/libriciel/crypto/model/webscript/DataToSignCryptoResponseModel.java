package coop.libriciel.crypto.model.webscript;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.crypto.model.v3.DataToSign;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataToSignCryptoResponseModel {
    List<DataToSign> dataToSignList;
    Long signatureDateTime;
}
