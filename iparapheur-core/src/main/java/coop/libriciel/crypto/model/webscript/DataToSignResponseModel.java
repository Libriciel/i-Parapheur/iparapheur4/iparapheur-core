package coop.libriciel.crypto.model.webscript;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataToSignResponseModel {
    String dataToSignBase64List;
    Long signatureDateTime;
    String format;
    String id;
}
