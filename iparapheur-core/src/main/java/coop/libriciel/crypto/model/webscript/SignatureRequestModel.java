package coop.libriciel.crypto.model.webscript;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignatureRequestModel {
    String certificate;
    List<String> signatureBase64List;
    Long signatureDateTime;
}
