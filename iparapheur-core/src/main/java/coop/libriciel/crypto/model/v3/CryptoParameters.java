package coop.libriciel.crypto.model.v3;


import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.crypto.model.stamp.PdfSignatureStamp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CryptoParameters {

    private @JsonAlias("remoteDocumentList") List<DataToSign> dataToSignList = new ArrayList<>();
    private @Deprecated String signatureFormat;
    private @JsonAlias("publicKeyBase64") String publicCertificateBase64;
    private String digestAlgorithm = "SHA256";
    private Long signatureDateTime = new Date().getTime();
    private Map<String, String> payload = new HashMap<>();

    private List<String> claimedRoles = new ArrayList<>();
    private String country;
    private String city;
    private String zipCode;
    private String signaturePackaging = "DETACHED";

    private PdfSignatureStamp stamp;
    // Permission for pades signature :
    // 0 - NO_CHANGE_PERMITTED
    // 1 - MINIMAL_CHANGES_PERMITTED
    // 2 - CHANGES_PERMITTED
    private Integer certificationPermission = null;

}