package coop.libriciel.crypto.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SignatureResultModel<T> extends SignatureModel<T> {
    Long signatureDateTime;
    List<String> signatureBase64List;
}
