package coop.libriciel.crypto.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RemoteDocument {
    String id;
    String digestBase64;
    String signatureBase64;
}
