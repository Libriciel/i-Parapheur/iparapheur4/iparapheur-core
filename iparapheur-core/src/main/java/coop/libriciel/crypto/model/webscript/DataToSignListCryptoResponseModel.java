package coop.libriciel.crypto.model.webscript;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataToSignListCryptoResponseModel {
    String id;
    String digestBase64;
    String dataToSignBase64;
    String signatureValue;
}
