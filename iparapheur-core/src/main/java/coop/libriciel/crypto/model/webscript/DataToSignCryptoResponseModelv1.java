package coop.libriciel.crypto.model.webscript;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataToSignCryptoResponseModelv1 {
    List<String> dataToSignBase64List;
    Long signatureDateTime;
}
