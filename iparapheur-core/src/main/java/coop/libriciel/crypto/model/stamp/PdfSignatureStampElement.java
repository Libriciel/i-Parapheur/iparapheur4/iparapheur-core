package coop.libriciel.crypto.model.stamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

@Data
public class PdfSignatureStampElement {

    public enum Type {
        TEXT,
        IMAGE
    }

    @Getter
    @AllArgsConstructor
    public enum Font {

        TIMES_ROMAN(PDType1Font.TIMES_ROMAN),
        TIMES_BOLD(PDType1Font.TIMES_BOLD),
        TIMES_ITALIC(PDType1Font.TIMES_ITALIC),
        TIMES_BOLD_ITALIC(PDType1Font.TIMES_BOLD_ITALIC),

        HELVETICA(PDType1Font.HELVETICA),
        HELVETICA_BOLD(PDType1Font.HELVETICA_BOLD),
        HELVETICA_OBLIQUE(PDType1Font.HELVETICA_OBLIQUE),
        HELVETICA_BOLD_OBLIQUE(PDType1Font.HELVETICA_BOLD_OBLIQUE),

        COURIER(PDType1Font.COURIER),
        COURIER_BOLD(PDType1Font.COURIER_BOLD),
        COURIER_OBLIQUE(PDType1Font.COURIER_OBLIQUE),
        COURIER_BOLD_OBLIQUE(PDType1Font.COURIER_BOLD_OBLIQUE);

        private PDType1Font pdType1Font;
    }

    private Type type = Type.TEXT;
    private String value;

    private Font font = Font.HELVETICA;
    private Integer fontSize;
    private String colorCode = "#000000";

    private Float x;
    private Float y;
    private Float width;
    private Float height;
}
