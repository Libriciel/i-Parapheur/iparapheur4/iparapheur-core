package coop.libriciel.crypto.model.stamp;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class PdfSignatureStamp {
    private int x = 0;
    private int y = 0;
    private int width = 0;
    private int height = 0;
    private int page = 1;
    private @JsonAlias("metadatas") Map<String, String> metadata;

    private List<PdfSignatureStampElement> elements = new ArrayList<>();
}
