package coop.libriciel.crypto.model;

import coop.libriciel.crypto.model.stamp.PdfSignatureStamp;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class SignatureModel<T> {
    List<T> dataToSignList = new ArrayList<>();
    String publicKeyBase64;
    // SHA-256 is always default value
    String digestAlgorithm = "SHA256";
    Map<String, String> payload = new HashMap<>();

    // Only for Pades signature, but it's common
    PdfSignatureStamp stamp;
    // Permission for pades signature :
    // 0 - NO_CHANGE_PERMITTED
    // 1 - MINIMAL_CHANGES_PERMITTED
    // 2 - CHANGES_PERMITTED
    Integer certificationPermission = null;
}
