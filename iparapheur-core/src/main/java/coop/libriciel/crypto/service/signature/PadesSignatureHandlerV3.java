package coop.libriciel.crypto.service.signature;


import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.crypto.model.v3.CryptoParameters;
import coop.libriciel.crypto.model.v3.DataToSign;
import coop.libriciel.crypto.model.webscript.*;
import coop.libriciel.crypto.service.PdfStampPositionService;
import coop.libriciel.service.NodeUtilsService;
import lombok.Getter;
import lombok.extern.log4j.Log4j;
import org.adullact.iparapheur.util.X509Util;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.http.HttpEntity;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@Log4j
public class PadesSignatureHandlerV3 extends CommonSignatureHandlerv3 {
    @Autowired
    protected PdfStampPositionService pdfStampPositionService;
    @Autowired
    private NodeUtilsService nodeUtilsService;

    @Getter
    public final String entryName = "pades";

    @Getter
    Integer permission =  null;

    @Getter
    public final String[] format = new String[]{
            ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_PADES_ISO32000_1
    };

    @Override
    protected boolean isFullFileSended() {
        return true;
    }

    private final ConcurrentHashMap<String, String> dataToSignCache = new ConcurrentHashMap<>();

    @Override
    public List<DataToSignResponseModel> getDataToSign(NodeRef dossier, NodeRef desk, DataToSignRequestModel model) throws IOException, NoSuchAlgorithmException {
        List<DataToSignResponseModel> result = new ArrayList<>();
        List<NodeRef> documents = folderUtilsService.getMainDocuments(dossier);

        for (NodeRef document : documents) {
            CryptoParameters sigModel = new CryptoParameters();
            sigModel.setPublicCertificateBase64(model.getCertificate());
            sigModel.setCertificationPermission(this.getPermission());

            Map<String, String> payload = new HashMap<>();

            String username = AuthenticationUtil.getRunAsUser();
            String role = getUserUtilsService().getRoleWithFolderAndDesk(username, dossier, desk);
            String sigLocation = getUserUtilsService().getLocationWithUserAndFolder(username, dossier);

            payload.put("reason", role);
            payload.put("location", sigLocation);

            sigModel.setStamp(pdfStampPositionService.getStampForDocument(document, desk, false));
            sigModel.setPayload(payload);

            FileContentReader toSend = (FileContentReader) this.serviceRegistry.getContentService().getReader(document, ContentModel.PROP_CONTENT);

            DataToSignResponseModel toSign = generateDataToSign(toSend.getFile(), sigModel);

            dataToSignCache.put(document.getId(), toSign.getDataToSignBase64List());
            result.add(toSign);
        }

        return result;
    }

    private DataToSignResponseModel generateDataToSign(File file, CryptoParameters model) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        HttpEntity entity = doPost(file, model, "generateDataToSign");
        if(entity != null) {
            entity.writeTo(baos);
            ObjectMapper mapper = new ObjectMapper();
            String responseStr = new String(baos.toByteArray());
            System.out.println(responseStr);
            DataToSignCryptoResponseModel responseModel = mapper.readValue(baos.toByteArray(), DataToSignCryptoResponseModel.class);
            DataToSignResponseModel response = new DataToSignResponseModel();
            response.setDataToSignBase64List(responseModel.getDataToSignList().stream().map(DataToSign::getDataToSignBase64).collect(Collectors.joining()));
            response.setSignatureDateTime(responseModel.getSignatureDateTime());
            response.setFormat(null);
            return response;
        }
        return null;
    }

    @Override
    public void doSignFolder(NodeRef node, NodeRef bureauCourant, List<String> signatures, List<Long> signatureDateTime, String certificate) throws IOException, NoSuchAlgorithmException, CertificateException, JSONException {
        List<InputStream> signatureResult = new ArrayList<>();
        List<NodeRef> documents = folderUtilsService.getMainDocuments(node);
        for (int i = 0; i < documents.size(); i++) {
            CryptoParameters model = new CryptoParameters();
            model.setPublicCertificateBase64(certificate);
            model.setCertificationPermission(this.getPermission());

            DataToSign dts = new DataToSign();
            NodeRef document = documents.get(i);

            dts.setSignatureValue(signatures.get(i));
            dts.setDataToSignBase64(dataToSignCache.get(document.getId()));

            model.getDataToSignList().add(dts);
            model.setSignatureDateTime(signatureDateTime.get(i));

            signatureResult.add(getSignature(document, bureauCourant, model));
        }
        doSignFolder(node, bureauCourant, signatureResult, certificate);
    }

    public void doSignFolder(NodeRef dossier, NodeRef currentDesk, List<InputStream> signedDocuments, String publicKeyBase64) throws CertificateException, FileNotFoundException {
        // this.serviceRegistry.getNodeService().removeProperty(dossier, ParapheurModel.PROP_SIGN_INFO);

        List<NodeRef> documents = folderUtilsService.getMainDocuments(dossier);
        EtapeCircuit etape = folderUtilsService.getCurrentStep(dossier);

        for (int i = 0; i < documents.size(); i++) {
            InputStream signed = signedDocuments.get(i);
            NodeRef document = documents.get(i);

            handleDocumentSignature(document, dossier, etape, signed);
        }

        CertificateFactory factory = CertificateFactory.getInstance("X509");
        X509Certificate cert = (X509Certificate) factory.generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(publicKeyBase64)));

        String signature = new JSONObject(X509Util.getUsefulCertProps(cert)).toString();
        this.serviceRegistry.getNodeService().setProperty(etape.getNodeRef(), ParapheurModel.PROP_CLE_PUBLIQUE_ETAPE, publicKeyBase64);
        this.serviceRegistry.getNodeService().setProperty(etape.getNodeRef(), ParapheurModel.PROP_SIGNATURE, signature);
        this.serviceRegistry.getNodeService().setProperty(
                etape.getNodeRef(),
                ParapheurModel.PROP_SIGNATAIRE,
                userUtilsService.getFullname(AuthenticationUtil.getRunAsUser()));
    }

    public InputStream getSignature(NodeRef document, NodeRef desk, CryptoParameters model) throws IOException, NoSuchAlgorithmException, JSONException {
        NodeRef dossier = nodeUtilsService.getFirstParentOfType(document, ParapheurModel.TYPE_DOSSIER);
        Map<String, String> payload = new HashMap<>();

        String username = AuthenticationUtil.getRunAsUser();
        String role = getUserUtilsService().getRoleWithFolderAndDesk(username, dossier, desk);
        String sigLocation = getUserUtilsService().getLocationWithUserAndFolder(username, dossier);

        payload.put("reason", role);
        payload.put("location", sigLocation);

        model.setStamp(pdfStampPositionService.getStampForDocument(document, desk, false));
        model.setPayload(payload);

        FileContentReader toSend = (FileContentReader) this.serviceRegistry.getContentService().getReader(document, ContentModel.PROP_CONTENT);
        return this.generateSignature(toSend.getFile(), model);
    }

    public InputStream generateSignature(File file, Object model) throws IOException {
        HttpEntity entity = doPost(file, model, "generateSignature");
        return handleEntity(entity);
    }

    protected void handleDocumentSignature(NodeRef document, NodeRef dossier, EtapeCircuit etape, InputStream signed) {
        // Remove all old properties on document
        getServiceRegistry().getNodeService().removeProperty(document, ParapheurModel.PROP_VISUEL_PDF);
        getServiceRegistry().getNodeService().removeProperty(document, ParapheurModel.PROP_CUSTOM_SIGNATURE_POSITION_RECTANGLE);
        getServiceRegistry().getNodeService().removeProperty(document, ParapheurModel.PROP_CUSTOM_SIGNATURE_PAGE_NUMBER);
        getServiceRegistry().getNodeService().removeProperty(document, ParapheurModel.PROP_SIGNATURE_TAG_POSITION_RECTANGLE);
        getServiceRegistry().getNodeService().removeProperty(document, ParapheurModel.PROP_SIGNATURE_TAG_PAGE_NUMBER);

        getServiceRegistry().getContentService().getWriter(document, ContentModel.PROP_CONTENT, true).putContent(signed);
        getFolderUtilsService().deleteImagesPreviews(document, dossier);

    }

    public InputStream handleEntity(HttpEntity entity) throws IOException {
        // We have to clone the stream to avoid unexpected http entity close
        return clone(entity.getContent());
    }

    public static InputStream clone(final InputStream inputStream) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int readLength = 0;
            while ((readLength = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, readLength);
            }
            outputStream.flush();
            inputStream.close();
            return new ByteArrayInputStream(outputStream.toByteArray());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
