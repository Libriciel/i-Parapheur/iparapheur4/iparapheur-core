package coop.libriciel.crypto.service;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.S2lowService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.crypto.model.stamp.PdfSignatureStamp;
import coop.libriciel.crypto.model.stamp.PdfSignatureStampElement;
import coop.libriciel.service.FolderUtilsService;
import coop.libriciel.service.NodeUtilsService;
import coop.libriciel.service.UserUtilsService;
import coop.libriciel.utils.poppler.PopplerUtils;
import coop.libriciel.utils.poppler.TagSearchResult;
import lombok.extern.log4j.Log4j;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

@Service
@Log4j
public class PdfStampPositionService {

    @Value("${parapheur.libersign.tag.signature.name}")
    private String signatureTag = "#signature#";
    @Value("${parapheur.libersign.tag.signature.name.tenants}")
    private String signatureTenantsTag = "{}";
    private Map<String, String> signatureTagMap = new HashMap<>();
    @Value("${parapheur.libersign.tag.cachet.name}")
    private String cachetTag = "#cachet#";
    @Value("${parapheur.libersign.tag.cachet.name.tenants}")
    private String cachetTenantsTag = "{}";
    private Map<String, String> cachetTagMap = new HashMap<>();
    @Value("${parapheur.libersign.tampon.signature.info}")
    String padesStampInformations;

    @Autowired
    private ServiceRegistry serviceRegistry;
    @Autowired
    private TenantService tenantService;

    @Autowired
    private NodeUtilsService nodeUtilsService;
    @Autowired
    private FolderUtilsService folderUtilsService;
    @Autowired
    private UserUtilsService userUtilsService;
    @Autowired
    @Qualifier("s2lowServiceDecorator")
    private S2lowService s2lowService;


    @PostConstruct
    public void init() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        this.signatureTagMap = mapper.readValue(signatureTenantsTag, new TypeReference<Map<String, String>>() {});
        this.cachetTagMap = mapper.readValue(cachetTenantsTag, new TypeReference<Map<String, String>>() {});
    }

    public PdfSignatureStamp getStampForDocument(NodeRef document, NodeRef currentDesk, boolean cachet) {
        NodeRef dossier = nodeUtilsService.getFirstParentOfType(document, ParapheurModel.TYPE_DOSSIER);
        Properties padesProperties = s2lowService.getPadesSignaturePropertiesWithDossier(dossier);

        PdfSignatureStamp result = new PdfSignatureStamp();
        if (Boolean.parseBoolean(padesProperties.getProperty("showStamp"))) {

            // Step 1 - build general stamp
            long numSignature = 1 + folderUtilsService.getWorkflow(dossier).stream().filter(etapeCircuit -> etapeCircuit.isApproved() && etapeCircuit.getActionDemandee().equalsIgnoreCase("signature")).count();
            setPdfSignatureStampPosition(result, document, cachet, padesProperties, numSignature);

            // Step 2 - add new text element with padesStampInformations on it, if font is not 0 and padesStampInformations is defined
            int fontSize = Integer.parseInt(padesProperties.getProperty("stampFontSize"));
            String username = AuthenticationUtil.getRunAsUser();

            // Step 3 - add user image if defined
            PdfSignatureStampElement imageElement = initializePdfStampImageElement(username);
            if(imageElement != null) {
                imageElement.setX(0F);
                imageElement.setY(0F);
                imageElement.setWidth((float) result.getWidth());
                imageElement.setHeight((float) result.getHeight());

                result.getElements().add(imageElement);
            }

            // Step 4 - add text information on top of image
            if(!padesStampInformations.isEmpty()) {
                String role = userUtilsService.getRoleWithFolderAndDesk(username, dossier, currentDesk);

                PdfSignatureStampElement element = initializePdfStampTextElement(username, fontSize, role);
                element.setX(0F);
                element.setY(0F);
                element.setWidth((float) result.getWidth());
                element.setHeight((float) result.getHeight());

                try {
                    String[] lines = StringUtils.split(element.getValue(), "\n");
                    int realFontSize = calculateOptimalFontSize(result, element, lines);
                    float maxSize = (int) (element.getFont().getPdType1Font().getFontDescriptor().getCapHeight() / 1000 * lines.length) + (1.3f * (lines.length - 1));
                    float currentHeight = realFontSize * maxSize;
                    float yPosition = (result.getHeight() - currentHeight) / 2;
                    element.setY(yPosition);
                }catch(Exception e) {
                    log.error("Cannot center annotation : " + e.getLocalizedMessage(), e);
                    // Ignore this and set default position
                }

                result.getElements().add(element);
            }
        }

        return result;
    }

    private int calculateOptimalFontSize(PdfSignatureStamp signatureStamp, PdfSignatureStampElement element, String[] lines) throws IOException {
        int fontSize = element.getFontSize();

        if (fontSize == 0) {

            // Do the Math

            float maxSize = -1;
            int widthFontSize;
            int heightFontSize;

            // Get max length line

            for (String line : lines) {
                maxSize = Math.max(element.getFont().getPdType1Font().getStringWidth(line) / 1000, maxSize);
            }
            widthFontSize = Math.round(signatureStamp.getWidth() / maxSize) - 1;

            // Get all lines height

            maxSize = (int) (element.getFont().getPdType1Font().getFontDescriptor().getCapHeight() / 1000 * lines.length) + (1.3f * (lines.length - 1));
            heightFontSize = Math.round(signatureStamp.getHeight() / maxSize) - 1;

            fontSize = Math.min(widthFontSize, heightFontSize);
        }

        return fontSize;
    }

    private PdfSignatureStampElement initializePdfStampImageElement(String username) {
        NodeRef personneRef = serviceRegistry.getPersonService().getPerson(username);
        PdfSignatureStampElement image = null;
        if (personneRef != null) {
            ContentReader imageReader = null;
            // On récupère l'image de signature de l'utilisateur
            if (serviceRegistry.getNodeService().hasAspect(personneRef, ParapheurModel.ASPECT_SIGNATURESCAN)) {
                List<ChildAssociationRef> children = serviceRegistry.getNodeService().getChildAssocs(personneRef);
                for (ChildAssociationRef child : children) {
                    if (child.getTypeQName().equals(ParapheurModel.ASSOC_SIGNATURE_SCAN)) {
                        imageReader = serviceRegistry.getContentService().getReader(child.getChildRef(), ContentModel.PROP_CONTENT);
                    }
                }
            }
            if (imageReader != null) {
                try {
                    image = new PdfSignatureStampElement();
                    File tmpFile = File.createTempFile("image", "tmp");
                    imageReader.getContent(tmpFile);
                    image.setValue(Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(tmpFile)));
                    image.setType(PdfSignatureStampElement.Type.IMAGE);
                    Files.delete(tmpFile.toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return image;
    }

    @NotNull
    private PdfSignatureStampElement initializePdfStampTextElement(String username, int fontSize, String role) {
        Calendar signDate = Calendar.getInstance();
        signDate.set(Calendar.SECOND, 0);
        signDate.set(Calendar.MILLISECOND, 0);

        StringBuilder builder = new StringBuilder();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setTimeZone(signDate.getTimeZone());
        String timestampString = dateFormat.format(signDate.getTime());
        builder.append(formatStampInfos(userUtilsService.getFullname(username), timestampString, role));

        PdfSignatureStampElement element = new PdfSignatureStampElement();

        element.setColorCode("#00000");
        element.setFontSize(fontSize);
        element.setFont(PdfSignatureStampElement.Font.HELVETICA);
        element.setType(PdfSignatureStampElement.Type.TEXT);
        element.setValue(builder.toString());
        element.setX(0F);
        element.setY(0F);
        return element;
    }

    private String formatStampInfos(String name, String timestampString, String role) {
        return padesStampInformations.replace("%N", name)
                .replace("%D", timestampString)
                .replace("%Q", role);
    }

    private void setPdfSignatureStampPosition(PdfSignatureStamp result, NodeRef document, boolean cachet, Properties padesProperties, long numSignature) {
        if (!setCustomSignature(result, document, ParapheurModel.PROP_CUSTOM_SIGNATURE_POSITION_RECTANGLE, ParapheurModel.PROP_CUSTOM_SIGNATURE_PAGE_NUMBER)
        && !setCustomSignature(result, document, ParapheurModel.PROP_SIGNATURE_TAG_POSITION_RECTANGLE, ParapheurModel.PROP_SIGNATURE_TAG_PAGE_NUMBER)
        && !retrieveSignatureTagFromPdf(result, document, padesProperties, (int) numSignature, cachet)) {
            result.setX(Integer.parseInt(padesProperties.getProperty("stampCoordX")));
            result.setY(Integer.parseInt(padesProperties.getProperty("stampCoordY")));
        }
    }

    private boolean setCustomSignature(PdfSignatureStamp stamp, NodeRef documentRef, QName position, QName page) {
        boolean retrieved = false;

        // Getting metadata

        Rectangle customCoordinatesRectangle = null;
        Integer customPage = null;
        try {
            customCoordinatesRectangle = (Rectangle) serviceRegistry.getNodeService().getProperty(documentRef, position);
            customPage = (Integer) serviceRegistry.getNodeService().getProperty(documentRef, page);
        } catch (ClassCastException | InvalidNodeRefException ex) {
            log.debug("Invalid PAdES custom signature location : ", ex);
        }

        // Parsing

        if ((customCoordinatesRectangle != null) && (customPage != null)) {
            stamp.setWidth(customCoordinatesRectangle.width);
            stamp.setHeight(customCoordinatesRectangle.height);
            stamp.setX(customCoordinatesRectangle.x);
            stamp.setY(customCoordinatesRectangle.y);
            stamp.setPage(customPage);

            retrieved = true;
            log.debug("PAdES custom signature location found : dossierRef=" + documentRef.getId());
        } else {
            log.debug("PAdES custom signature location not found");
        }

        return retrieved;
    }

    private boolean retrieveSignatureTagFromPdf(PdfSignatureStamp stamp, NodeRef documentRef, Properties padesProperties, int numSignature, boolean cachet) {
        boolean retrieved = false;

        stamp.setWidth(Integer.parseInt(padesProperties.getProperty("stampWidth")));
        stamp.setHeight(Integer.parseInt(padesProperties.getProperty("stampHeight")));

        FileContentReader tmpReader = (FileContentReader) serviceRegistry.getContentService().getReader(documentRef, ContentModel.PROP_CONTENT);
        if (tmpReader == null) {
            return false;
        }

        String path = tmpReader.getFile().getAbsolutePath();

        int page = Integer.parseInt(padesProperties.getProperty("stampPage"));
        if (page < 1) {
            PopplerUtils utils = new PopplerUtils();
            page = utils.getPageCount(path);
        }
        stamp.setPage(page);


        // Position du tag de signature
        TagSearchResult position = scanForTag(path, cachet, numSignature);

        if (position != null) {
            stamp.setPage(position.page);
            stamp.setX(position.rectangle.x - stamp.getWidth() / 2 + position.rectangle.width / 2);
            stamp.setY(position.rectangle.y - stamp.getHeight() / 2 + position.rectangle.height / 2);
            retrieved = true;

            log.debug("PAdES signature tag location found : dossierRef=" + documentRef.getId());
            savePdfSignatureTagToMetadata(stamp, documentRef);
        } else {
            log.debug("PAdES signature tag location not found");
        }

        return retrieved;
    }

    /**
     * Saves already fetched x, y, ... from document #signature# tag to metadata.
     */
    private void savePdfSignatureTagToMetadata(PdfSignatureStamp stamp, NodeRef documentRef) {
        Rectangle rectangle = new Rectangle();
        rectangle.x = stamp.getX();
        rectangle.y = stamp.getY();
        rectangle.width = stamp.getWidth();
        rectangle.height = stamp.getHeight();

        serviceRegistry.getNodeService().setProperty(documentRef, ParapheurModel.PROP_SIGNATURE_TAG_POSITION_RECTANGLE, rectangle);
        serviceRegistry.getNodeService().setProperty(documentRef, ParapheurModel.PROP_SIGNATURE_TAG_PAGE_NUMBER, stamp.getPage());
    }

    public TagSearchResult scanForTag(String path, boolean cachet, int numSignature) {
        // Depuis la 4.3, on peut définir un tag de signature par tenant... On tente de le récupérer ici !
        String sigTag = cachet ? cachetTag : signatureTag;
        Map<String, String> tagMap = cachet ? cachetTagMap : signatureTagMap;
        String domain = tenantService.getCurrentUserDomain();
        if (!domain.isEmpty() && tagMap.containsKey(domain)) {
            sigTag = tagMap.get(domain);
        }

        return new PopplerUtils().getTextLocation(path, sigTag.replace("%n", String.valueOf(numSignature)));
    }
}
