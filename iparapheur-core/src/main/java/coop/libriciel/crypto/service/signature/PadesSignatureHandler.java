package coop.libriciel.crypto.service.signature;


import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import coop.libriciel.crypto.model.SignatureModel;
import coop.libriciel.crypto.model.SignatureResultModel;
import coop.libriciel.crypto.service.PdfStampPositionService;
import coop.libriciel.service.NodeUtilsService;
import lombok.Getter;
import lombok.extern.log4j.Log4j;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.http.HttpEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Log4j
public class PadesSignatureHandler extends CommonSignatureHandler<InputStream> {
    @Autowired
    protected PdfStampPositionService pdfStampPositionService;
    @Autowired
    private NodeUtilsService nodeUtilsService;

    @Getter
    public final boolean isFullFileSended = true;

    @Getter
    public final String entryName = "pades";

    @Getter
    public final String[] format = new String[]{
            ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_PADES_ISO32000_1
    };

    @Override
    public boolean shouldCallCryptoService() {
        return true;
    }

    @Override
    public void specificModelFill(SignatureModel<InputStream> model, NodeRef document, NodeRef currentDesk) {
        NodeRef dossier = nodeUtilsService.getFirstParentOfType(document, ParapheurModel.TYPE_DOSSIER);
        Map<String, String> payload = new HashMap<>();

        String username = AuthenticationUtil.getRunAsUser();
        String role = getUserUtilsService().getRoleWithFolderAndDesk(username, dossier, currentDesk);
        String sigLocation = getUserUtilsService().getLocationWithUserAndFolder(username, dossier);

        payload.put("reason", role);
        payload.put("location", sigLocation);

        model.setStamp(pdfStampPositionService.getStampForDocument(document, currentDesk, false));
        model.setPayload(payload);
    }

    @Override
    protected void handleDocumentSignature(NodeRef document, NodeRef dossier, EtapeCircuit etape, List<InputStream> signed) {
        signed.forEach(sign -> {
            // Remove all old properties on document
            getServiceRegistry().getNodeService().removeProperty(document, ParapheurModel.PROP_VISUEL_PDF);
            getServiceRegistry().getNodeService().removeProperty(document, ParapheurModel.PROP_CUSTOM_SIGNATURE_POSITION_RECTANGLE);
            getServiceRegistry().getNodeService().removeProperty(document, ParapheurModel.PROP_CUSTOM_SIGNATURE_PAGE_NUMBER);
            getServiceRegistry().getNodeService().removeProperty(document, ParapheurModel.PROP_SIGNATURE_TAG_POSITION_RECTANGLE);
            getServiceRegistry().getNodeService().removeProperty(document, ParapheurModel.PROP_SIGNATURE_TAG_PAGE_NUMBER);

            getServiceRegistry().getContentService().getWriter(document, ContentModel.PROP_CONTENT, true).putContent(sign);
            getFolderUtilsService().deleteImagesPreviews(document, dossier);
        });

    }

    @Override
    public List<InputStream> handleEntity(HttpEntity entity, SignatureResultModel<InputStream> model) throws IOException {
        // We have to clone the stream to avoid unexpected http entity close
        return Collections.singletonList(clone(entity.getContent()));
    }

    public static InputStream clone(final InputStream inputStream) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int readLength = 0;
            while ((readLength = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, readLength);
            }
            outputStream.flush();
            inputStream.close();
            return new ByteArrayInputStream(outputStream.toByteArray());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
