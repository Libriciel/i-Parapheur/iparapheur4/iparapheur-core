package coop.libriciel.crypto.service.signature;


import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import coop.libriciel.crypto.model.RemoteDocument;
import coop.libriciel.crypto.model.SignatureModel;
import coop.libriciel.crypto.model.SignatureResultModel;
import coop.libriciel.signature.service.SignatureService;
import lombok.Getter;
import lombok.extern.log4j.Log4j;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.http.HttpEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

@Service
@Log4j
public class CMSSignatureHandler extends CommonSignatureHandler<RemoteDocument> {

    @Autowired
    private SignatureService signatureService;

    @Getter
    public final boolean isFullFileSended = false;

    @Getter
    public final String entryName = "cades";

    @Getter
    public final String[] format = new String[]{
            ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7
    };

    @Override
    public boolean shouldCallCryptoService() {
        return true;
    }

    @Getter
    protected final String sigMimeType = "application/pkcs7-signature";

    @Override
    public void specificModelFill(SignatureModel<RemoteDocument> model, NodeRef document, NodeRef currentDesk) throws IOException, NoSuchAlgorithmException {
        FileContentReader toSend = (FileContentReader) getServiceRegistry().getContentService().getReader(document, ContentModel.PROP_CONTENT);
        RemoteDocument remoteDocument = new RemoteDocument();
        remoteDocument.setDigestBase64(getBytesDigest(toSend.getFile()));
        model.setDataToSignList(Collections.singletonList(remoteDocument));
    }

    @Override
    public List<RemoteDocument> handleEntity(HttpEntity entity, SignatureResultModel<RemoteDocument> model) throws IOException, JSONException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        entity.writeTo(bout);

        JSONObject object = new JSONObject(bout.toString());
        System.out.println(object);
        JSONArray signed = object.getJSONArray("signatureResultBase64List");

        if (signed.length() == model.getDataToSignList().size()) {
            for (int i = 0; i < signed.length(); i++) {
                model.getDataToSignList().get(i).setSignatureBase64(signed.getString(i));
            }
        }
        return model.getDataToSignList();
    }

    @Override
    protected void handleDocumentSignature(NodeRef document, NodeRef dossier, EtapeCircuit etape, List<RemoteDocument> signed) {
        // There should be only one signature here
        signed.forEach(sign -> {
            signatureService.setDetachedSignature(document.getId(), new ByteArrayInputStream(Base64.getDecoder().decode(sign.getSignatureBase64())), getSigMimeType());
            getServiceRegistry().getNodeService().setProperty(etape.getNodeRef(), ParapheurModel.PROP_SIGNATURE_ETAPE, sign.getSignatureBase64());
        });
    }
}
