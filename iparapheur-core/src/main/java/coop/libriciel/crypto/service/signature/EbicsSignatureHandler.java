package coop.libriciel.crypto.service.signature;


import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import coop.libriciel.crypto.model.SignatureModel;
import coop.libriciel.crypto.model.SignatureResultModel;
import coop.libriciel.signature.service.SignatureService;
import lombok.Getter;
import lombok.extern.log4j.Log4j;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

@Service
@Log4j
public class EbicsSignatureHandler extends CommonSignatureHandler<String> {

    @Autowired
    private SignatureService signatureService;

    @Getter
    public final boolean isFullFileSended = false;

    @Getter
    public final String entryName = null;

    @Getter
    public final String[] format = new String[]{
            ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_PKCS1_256
    };

    @Override
    public boolean shouldCallCryptoService() {
        return false;
    }

    @Override
    public void specificModelFill(SignatureModel<String> model, NodeRef document, NodeRef currentDesk) throws IOException {
        FileContentReader toSend = (FileContentReader) getServiceRegistry().getContentService().getReader(document, ContentModel.PROP_CONTENT);
        model.setDataToSignList(Collections.singletonList("pkcs1:" + Base64.getEncoder().encodeToString(getBytesDigest(ebicsC14N(toSend.getFile())))));
    }

    static byte[] ebicsC14N(File file) throws IOException {
        byte[] data = IOUtils.toByteArray(new FileInputStream(file));

        byte[] groomedData = new byte[data.length];
        int bytesCopied = 0;

        for (byte datum : data) {
            switch (datum) {
                case (char) 0x0a:
                case (char) 0x0d:
                case (char) 0x1a:
                    break;
                default:
                    groomedData[bytesCopied++] = datum;
            }
        }

        byte[] packedData = new byte[bytesCopied];

        System.arraycopy(groomedData, 0, packedData, 0, bytesCopied);

        return packedData;
    }

    @Override
    public List<String> handleEntity(HttpEntity entity, SignatureResultModel<String> model) {
        return model.getSignatureBase64List();
    }

    @Override
    protected void handleDocumentSignature(NodeRef document, NodeRef dossier, EtapeCircuit etape, List<String> signed) {
        signed.forEach(sign -> {
            signatureService.setDetachedSignature(document.getId(), new ByteArrayInputStream(Base64.getEncoder().encodeToString(sign.getBytes()).getBytes()), "application/pkcs1");
            getServiceRegistry().getNodeService().setProperty(etape.getNodeRef(), ParapheurModel.PROP_SIGNATURE_ETAPE, sign);
        });
    }
}
