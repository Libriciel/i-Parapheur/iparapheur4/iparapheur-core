package coop.libriciel.crypto.service.signature;


import com.atolcd.parapheur.model.ParapheurModel;
import lombok.Getter;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

@Service
@Log4j
public class LockedPadesSignatureHandlerV3 extends PadesSignatureHandlerV3 {
    @Getter
    public final String[] format = new String[]{
            ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_PADES_ISO32000_1_CERT
    };

    @Getter
    Integer permission = 0;

}
