package coop.libriciel.crypto.service.signature;


import com.atolcd.parapheur.model.ParapheurModel;
import lombok.Getter;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

@Service
@Log4j
public class XadesSignatureHandlerV3 extends CMSSignatureHandlerV3 {

    @Getter
    public final String entryName = "xades";

    @Getter
    public final String[] format = new String[]{
            ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_1_1,
            ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_3_2
    };

    @Getter
    protected final String sigMimeType = "application/xml";
}
