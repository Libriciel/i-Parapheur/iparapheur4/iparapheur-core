package coop.libriciel.crypto.service.signature;


import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.crypto.model.SignatureModel;
import coop.libriciel.crypto.model.SignatureResultModel;
import coop.libriciel.crypto.model.v3.DataToSign;
import coop.libriciel.crypto.model.webscript.*;
import coop.libriciel.service.FolderUtilsService;
import coop.libriciel.service.UserUtilsService;
import lombok.Getter;
import lombok.Setter;
import org.adullact.iparapheur.util.X509Util;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.stream.Collectors;

public abstract class CommonSignatureHandler<T> extends SignatureHandler {


    @Getter
    private FolderUtilsService folderUtilsService;
    @Getter
    private ServiceRegistry serviceRegistry;
    @Getter
    private UserUtilsService userUtilsService;

    @Autowired
    public final void setFolderUtilsService(FolderUtilsService folderUtilsService) {
        this.folderUtilsService = folderUtilsService;
    }
    @Autowired
    public final void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
    @Autowired
    public final void setUserUtilsService(UserUtilsService userUtilsService) {
        this.userUtilsService = userUtilsService;
    }

    public abstract String getEntryName();
    public abstract boolean isFullFileSended();

    /**
     * For some signatures, we don't have to call for crypto service (PKCS#1 for example)
     * @return TRUE if cryptoService should be called
     */
    public abstract boolean shouldCallCryptoService();

    // <editor-fold desc="DataToSign">

    public List<DataToSignResponseModel> getDataToSign(NodeRef dossier, NodeRef desk, DataToSignRequestModel model) throws IOException, NoSuchAlgorithmException {
        SignatureModel<T> sigModel = new SignatureModel<>();
        sigModel.setPublicKeyBase64(model.getCertificate());

        List<DataToSignResponseModel> result = new ArrayList<>();
        List<NodeRef> documents = folderUtilsService.getMainDocuments(dossier);

        for (NodeRef document : documents) {
            specificModelFill(sigModel, document, desk);

            FileContentReader toSend = (FileContentReader) this.serviceRegistry.getContentService().getReader(document, ContentModel.PROP_CONTENT);
            DataToSignResponseModel toSign = generateDataToSign(toSend.getFile(), sigModel);

            result.add(toSign);
        }

        return result;
    }

    public abstract void specificModelFill(SignatureModel<T> model, NodeRef document, NodeRef currentDesk) throws IOException, NoSuchAlgorithmException;

    public DataToSignResponseModel generateDataToSign(File file, SignatureModel<T> model) throws IOException {
        if(shouldCallCryptoService()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            HttpEntity entity = doPost(file, model, "generateDataToSign");
            if(entity != null) {
                entity.writeTo(baos);
                ObjectMapper mapper = new ObjectMapper();
                DataToSignCryptoResponseModelv1 responseModel = mapper.readValue(baos.toByteArray(), DataToSignCryptoResponseModelv1.class);
                DataToSignResponseModel response = new DataToSignResponseModel();
                response.setDataToSignBase64List(String.join(",", responseModel.getDataToSignBase64List()));
                response.setSignatureDateTime(responseModel.getSignatureDateTime());
//                response.setFormat(responseModel.getFormat());
                return response;
            }
            return null;
        } else {
            String toSign = (String) model.getDataToSignList().get(0);
            DataToSignResponseModel responseModel = new DataToSignResponseModel();
            responseModel.setDataToSignBase64List(toSign);
            responseModel.setSignatureDateTime((new Date()).getTime());
            return responseModel;
        }
    }

    public List<T> generateSignature(File file, SignatureResultModel<T> model) throws IOException, JSONException {
        HttpEntity entity = null;
        if(shouldCallCryptoService()) {
            entity = doPost(file, model, "generateSignature");
        }
        return handleEntity(entity, model);
    }

    private HttpEntity doPost(File file, SignatureModel<T> model, String method) {
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost("http://" + getConnectorHost() + ":" + getConnectorPort() + getConnectorPath() + "/api/" + getEntryName() + "/" + method);
        post.setEntity(buildEntity(file, model));

        try {
            HttpResponse response = httpclient.execute(post);
            HttpEntity entity = response.getEntity();

            if (entity != null && response.getStatusLine().getStatusCode() == 200) {
                return entity;
            } else {
                throw new Exception("Cannot stamp document, see error log on crypto application");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected HttpEntity buildEntity(File file, Object model) {
        final MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();

        ObjectMapper mapper = new ObjectMapper();
        try {
            String modelStr = mapper.writeValueAsString(model);

            if(isFullFileSended()) {
                multipartEntityBuilder.addBinaryBody("file", file);
            }

            multipartEntityBuilder.addTextBody("model", modelStr, ContentType.APPLICATION_JSON);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        multipartEntityBuilder.setContentType(ContentType.MULTIPART_FORM_DATA);
        return multipartEntityBuilder.build();
    }

    // </editor-fold>

    // <editor-fold desc="Signature">

    public void doSignFolder(NodeRef node, NodeRef bureauCourant, List<String> signatures, List<Long> signatureDateTime, String certificate) throws IOException, NoSuchAlgorithmException, CertificateException, JSONException {
        SignatureRequestModel model = new SignatureRequestModel();
        model.setCertificate(certificate);

        List<List<T>> signatureResult = new ArrayList<>();
        List<NodeRef> documents = folderUtilsService.getMainDocuments(node);
        for (int i = 0; i < documents.size(); i++) {

            NodeRef document = documents.get(i);
            model.setSignatureBase64List(Collections.singletonList(signatures.get(i)));
            model.setSignatureDateTime(signatureDateTime.get(i));

            signatureResult.add(getSignature(document, bureauCourant, model));
        }
        doSignFolder(node, bureauCourant, signatureResult, certificate);
    }

    public List<T> getSignature(NodeRef document, NodeRef desk, SignatureRequestModel model) throws IOException, NoSuchAlgorithmException, JSONException {
        SignatureResultModel<T> sigModel = new SignatureResultModel<>();
        sigModel.setPublicKeyBase64(model.getCertificate());
        sigModel.setSignatureBase64List(model.getSignatureBase64List());
        sigModel.setSignatureDateTime(model.getSignatureDateTime());

        specificModelFill(sigModel, document, desk);

        FileContentReader toSend = (FileContentReader) this.serviceRegistry.getContentService().getReader(document, ContentModel.PROP_CONTENT);
        return this.generateSignature(toSend.getFile(), sigModel);
    }

    // </editor-fold>

    public void doSignFolder(NodeRef dossier, NodeRef currentDesk, List<List<T>> signedDocuments, String publicKeyBase64) throws CertificateException, FileNotFoundException {
        // this.serviceRegistry.getNodeService().removeProperty(dossier, ParapheurModel.PROP_SIGN_INFO);

        List<NodeRef> documents = folderUtilsService.getMainDocuments(dossier);
        EtapeCircuit etape = folderUtilsService.getCurrentStep(dossier);

        for (int i = 0; i < documents.size(); i++) {
            List<T> signed = signedDocuments.get(i);
            NodeRef document = documents.get(i);

            handleDocumentSignature(document, dossier, etape, signed);
        }

        CertificateFactory factory = CertificateFactory.getInstance("X509");
        X509Certificate cert = (X509Certificate) factory.generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(publicKeyBase64)));

        String signature = new JSONObject(X509Util.getUsefulCertProps(cert)).toString();
        this.serviceRegistry.getNodeService().setProperty(etape.getNodeRef(), ParapheurModel.PROP_CLE_PUBLIQUE_ETAPE, publicKeyBase64);
        this.serviceRegistry.getNodeService().setProperty(etape.getNodeRef(), ParapheurModel.PROP_SIGNATURE, signature);
        this.serviceRegistry.getNodeService().setProperty(
                etape.getNodeRef(),
                ParapheurModel.PROP_SIGNATAIRE,
                userUtilsService.getFullname(AuthenticationUtil.getRunAsUser()));
    }

    protected abstract void handleDocumentSignature(NodeRef document, NodeRef dossier, EtapeCircuit etape, List<T> signed) throws FileNotFoundException;

    public abstract List<T> handleEntity(HttpEntity entity, SignatureResultModel<T> model) throws IOException, JSONException;

    public String getBytesDigest(File test) throws IOException, NoSuchAlgorithmException {
        Security.addProvider(new BouncyCastleProvider());

        byte[] buffer= new byte[8192];
        int count;
        MessageDigest digest = MessageDigest.getInstance("SHA256");
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(test))) {
            while ((count = bis.read(buffer)) > 0) {
                digest.update(buffer, 0, count);
            }
        }

        byte[] hash = digest.digest();
        return Base64.getEncoder().encodeToString(hash);
    }

    public byte[] getBytesDigest(final byte[] bytesToDigest) {
        Security.addProvider(new BouncyCastleProvider());

        MessageDigest digest;
        byte[] hash = null;
        try {
            digest = MessageDigest.getInstance("SHA256", "BC");
            digest.update(bytesToDigest);
            hash = digest.digest();
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            e.printStackTrace();
        }
        return hash;
    }
}
