package coop.libriciel.crypto.service;

import coop.libriciel.crypto.service.signature.SignatureHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SignatureHandlerProvider {
    @Value("${parapheur.crypto.connector.host}")
    private String connectorHost;
    @Value("${parapheur.crypto.connector.port}")
    private String connectorPort;
    @Value("${parapheur.crypto.connector.path}")
    private String connectorPath;
    @Autowired
    private List<SignatureHandler> dataToSignHandlerList;

    private final Map<String, SignatureHandler> dataToSignHandlerCache = new HashMap<>();

    public void init() {
        for(SignatureHandler service : dataToSignHandlerList) {
            // Define connector properties ad put service in HashMap
            service.setConnectorHost(connectorHost);
            service.setConnectorPort(connectorPort);
            service.setConnectorPath(connectorPath);
            for(int i = 0; i < service.getFormat().length; i++){
                dataToSignHandlerCache.put(service.getFormat()[i], service);
            }
        }
    }

    public SignatureHandler getService(String type) {
        SignatureHandler service = dataToSignHandlerCache.get(type);
        if(service == null) throw new RuntimeException("Unknown service type: " + type);
        return service;
    }
}
