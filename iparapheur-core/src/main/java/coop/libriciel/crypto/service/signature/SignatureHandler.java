package coop.libriciel.crypto.service.signature;

import coop.libriciel.crypto.model.webscript.DataToSignRequestModel;
import coop.libriciel.crypto.model.webscript.DataToSignResponseModel;
import lombok.Getter;
import lombok.Setter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.json.JSONException;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;

public abstract class SignatureHandler {
    @Getter @Setter
    private String connectorHost;
    @Getter @Setter
    private String connectorPort;
    @Getter @Setter
    private String connectorPath;

    public abstract String[] getFormat();

    public abstract List<DataToSignResponseModel> getDataToSign(NodeRef dossier, NodeRef desk, DataToSignRequestModel model) throws IOException, NoSuchAlgorithmException;
    public abstract void doSignFolder(NodeRef node, NodeRef bureauCourant, List<String> signatures, List<Long> signatureDateTime, String certificate) throws IOException, NoSuchAlgorithmException, CertificateException, JSONException;

}