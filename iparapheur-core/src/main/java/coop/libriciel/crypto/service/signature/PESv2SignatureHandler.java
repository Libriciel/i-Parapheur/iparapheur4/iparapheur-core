package coop.libriciel.crypto.service.signature;


import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import coop.libriciel.crypto.model.RemoteDocument;
import coop.libriciel.crypto.model.SignatureModel;
import coop.libriciel.crypto.model.SignatureResultModel;
import coop.libriciel.crypto.model.webscript.SignatureRequestModel;
import coop.libriciel.service.NodeUtilsService;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import org.adullact.libersign.util.signature.Xades;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.http.HttpEntity;
import org.bouncycastle.util.encoders.Hex;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Log4j
public class PESv2SignatureHandler extends CommonSignatureHandler<RemoteDocument> {

    @Getter
    public final boolean isFullFileSended = false;
    @Getter
    public final String entryName = "pesv2";
    @Getter
    public final String[] format = new String[]{
            ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_ENV_PESv2
    };
    @Autowired
    private NodeUtilsService nodeUtilsService;

    @Autowired
    private DossierService dossierService;

    @Override
    public boolean shouldCallCryptoService() {
        return true;
    }

    @SneakyThrows
    @Override
    public void specificModelFill(SignatureModel<RemoteDocument> model, NodeRef document, NodeRef currentDesk) {
        NodeRef dossier = nodeUtilsService.getFirstParentOfType(document, ParapheurModel.TYPE_DOSSIER);

        Map<String, String> infos = dossierService.getSignatureInformations(dossier, currentDesk);
        Map<String, String> payload = new HashMap<>();

        String username = AuthenticationUtil.getRunAsUser();
        String role = getUserUtilsService().getRoleWithFolderAndDesk(username, dossier, currentDesk);

        boolean doAddSignature = model instanceof SignatureResultModel;

        if (infos != null) {
            // We have all we need to create model
            List<RemoteDocument> documentModels = new ArrayList<>();
            List<String> pesIds = Arrays.asList(infos.get("pesid").split(","));
            List<String> pesHashes = Arrays.asList(infos.get("hash").split(","));

            for (int i = 0; i < pesIds.size(); i++) {
                documentModels.add(new RemoteDocument(pesIds.get(i), Base64.getEncoder().encodeToString(Hex.decode(pesHashes.get(i))), doAddSignature ? ((SignatureResultModel<RemoteDocument>)model).getSignatureBase64List().get(i) : null));
            }

            payload.put("pesclaimedrole", role);
            payload.put("pespostalcode", infos.get("pespostalcode"));
            payload.put("pescountryname", infos.get("pescountryname"));
            payload.put("pescity", infos.get("pescity"));

            model.setPayload(payload);
            model.setDataToSignList(documentModels);
        }
    }

    @Override
    public List<RemoteDocument> handleEntity(HttpEntity entity, SignatureResultModel<RemoteDocument> model) throws IOException, JSONException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        entity.writeTo(bout);

        JSONObject object = new JSONObject(bout.toString());
        JSONArray signed = object.getJSONArray("signatureResultBase64List");

        if (signed.length() == model.getDataToSignList().size()) {
            for (int i = 0; i < signed.length(); i++) {
                model.getDataToSignList().get(i).setSignatureBase64(signed.getString(i));
            }
        }
        return model.getDataToSignList();
    }

    @Override
    public void doSignFolder(NodeRef node, NodeRef bureauCourant, List<String> signatures, List<Long> signatureDateTime, String certificate) throws IOException, NoSuchAlgorithmException, CertificateException, JSONException {
        SignatureRequestModel model = new SignatureRequestModel();
        model.setCertificate(certificate);

        List<List<RemoteDocument>> signatureResult = new ArrayList<>();

        List<NodeRef> documents = this.getFolderUtilsService().getMainDocuments(node);
        for (int i = 0; i < documents.size(); i++) {
            NodeRef document = documents.get(i);

            model.setSignatureBase64List(Arrays.asList(signatures.get(i).split(",")));
            model.setSignatureDateTime(signatureDateTime.get(i));

            signatureResult.add(getSignature(document, bureauCourant, model));
        }
        doSignFolder(node, bureauCourant, signatureResult, certificate);
    }

    @Override
    protected void handleDocumentSignature(NodeRef document, NodeRef dossier, EtapeCircuit etape, List<RemoteDocument> signed) throws FileNotFoundException {
        FileContentReader toSend = (FileContentReader) getServiceRegistry().getContentService().getReader(document, ContentModel.PROP_CONTENT);
        String xpath = (String) this.getServiceRegistry().getNodeService().getProperty(dossier, ParapheurModel.PROP_XPATH_SIGNATURE);
        String allSigned = signed.stream().map(RemoteDocument::getSignatureBase64).collect(Collectors.joining(","));
        InputStream pesSigne = Xades.injectXadesSigIntoXml(new FileInputStream(toSend.getFile()), allSigned, xpath, toSend.getEncoding());

        ContentWriter writer = getServiceRegistry().getContentService().getWriter(document, ContentModel.PROP_CONTENT, true);
        writer.setEncoding(toSend.getEncoding());
        writer.setMimetype(toSend.getMimetype());
        writer.putContent(pesSigne);
    }
}
