package coop.libriciel.crypto.service.signature;


import com.atolcd.parapheur.model.ParapheurModel;
import coop.libriciel.crypto.model.SignatureModel;
import lombok.extern.log4j.Log4j;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
@Log4j
public class LockedPadesSignatureHandler extends PadesSignatureHandler {
    @Override
    public final String[] getFormat() {
        return new String[]{
                ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_PADES_ISO32000_1_CERT
        };
    }

    @Override
    public void specificModelFill(SignatureModel<InputStream> model, NodeRef document, NodeRef currentDesk) {
        super.specificModelFill(model, document, currentDesk);
        model.setCertificationPermission(0);
    }
}
