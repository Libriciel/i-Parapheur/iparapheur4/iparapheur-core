package coop.libriciel.crypto.service.signature;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.crypto.model.webscript.*;
import coop.libriciel.service.FolderUtilsService;
import coop.libriciel.service.UserUtilsService;
import lombok.Getter;
import lombok.Setter;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.*;

public abstract class CommonSignatureHandlerv3 extends SignatureHandler {

    @Getter
    protected FolderUtilsService folderUtilsService;
    @Getter
    protected ServiceRegistry serviceRegistry;
    @Getter
    protected UserUtilsService userUtilsService;

    @Autowired
    public final void setFolderUtilsService(FolderUtilsService folderUtilsService) {
        this.folderUtilsService = folderUtilsService;
    }
    @Autowired
    public final void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
    @Autowired
    public final void setUserUtilsService(UserUtilsService userUtilsService) {
        this.userUtilsService = userUtilsService;
    }

    protected HttpEntity doPost(File file, Object model, String method) {
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost("http://" + getConnectorHost() + ":" + getConnectorPort() + getConnectorPath() + "/api/v3/" + getEntryName() + "/" + method);
        post.setEntity(buildEntity(file, model));

        try {
            HttpResponse response = httpclient.execute(post);
            HttpEntity entity = response.getEntity();

            if (entity != null && response.getStatusLine().getStatusCode() == 200) {
                return entity;
            } else {
                System.out.println(response.getStatusLine().getStatusCode());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                if(entity != null) {
                    entity.writeTo(baos);
                    String responseStr = new String(baos.toByteArray());
                    System.out.println(responseStr);
                }
                throw new Exception("Cannot stamp document, see error log on crypto application");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected abstract String getEntryName();

    protected HttpEntity buildEntity(File file, Object model) {
        final MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();

        ObjectMapper mapper = new ObjectMapper();
        try {
            String modelStr = mapper.writeValueAsString(model);

            if(isFullFileSended()) {
                multipartEntityBuilder.addBinaryBody("file", file);
            }

            multipartEntityBuilder.addTextBody("model", modelStr, ContentType.APPLICATION_JSON);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        multipartEntityBuilder.setContentType(ContentType.MULTIPART_FORM_DATA);
        return multipartEntityBuilder.build();
    }

    public String getBytesDigest(File test) throws IOException, NoSuchAlgorithmException {
        Security.addProvider(new BouncyCastleProvider());

        byte[] buffer= new byte[8192];
        int count;
        MessageDigest digest = MessageDigest.getInstance("SHA256");
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(test))) {
            while ((count = bis.read(buffer)) > 0) {
                digest.update(buffer, 0, count);
            }
        }

        byte[] hash = digest.digest();
        return Base64.getEncoder().encodeToString(hash);
    }

    public byte[] getBytesDigest(final byte[] bytesToDigest) {
        Security.addProvider(new BouncyCastleProvider());

        MessageDigest digest;
        byte[] hash = null;
        try {
            digest = MessageDigest.getInstance("SHA256", "BC");
            digest.update(bytesToDigest);
            hash = digest.digest();
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            e.printStackTrace();
        }
        return hash;
    }

    protected abstract boolean isFullFileSended();

    public abstract List<DataToSignResponseModel> getDataToSign(NodeRef dossier, NodeRef desk, DataToSignRequestModel model) throws IOException, NoSuchAlgorithmException;
    public abstract void doSignFolder(NodeRef node, NodeRef bureauCourant, List<String> signatures, List<Long> signatureDateTime, String certificate) throws IOException, NoSuchAlgorithmException, CertificateException, JSONException;
}
