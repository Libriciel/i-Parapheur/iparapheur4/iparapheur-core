package coop.libriciel.pesviewer;

import fr.bl.iparapheur.web.BlexContext;
import fr.bl.iparapheur.xwv.XwvConfig;
import org.springframework.beans.factory.annotation.Value;

public class PesViewerInitService {
    @Value("${parapheur.pesviewer.connector.host}")
    private String connectorHost;
    @Value("${parapheur.pesviewer.connector.port}")
    private String connectorPort;
    @Value("${parapheur.pesviewer.connector.path}")
    private String connectorPath;

    public void init() {
        String xwvServerUrl = String.format("http://%s:%s%s", connectorHost, connectorPort, connectorPath);
        // construction du contexte XWV - dans tous les cas
        BlexContext.getInstance().setXwvConfig(new XwvConfig(xwvServerUrl));
        // verification du serveur associé au contexte XWV - le cas echeant
        if (xwvServerUrl != null) {
            BlexContext.getInstance().getXwvConfig().doCheckServer();
        }
    }
}
