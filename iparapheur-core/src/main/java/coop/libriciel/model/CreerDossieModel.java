package coop.libriciel.model;

import lombok.Data;
import org.adullact.iparapheur.rules.bean.CustomProperty;
import org.adullact.spring_ws.iparapheur._1.TypeDocAnnexes;
import org.adullact.spring_ws.iparapheur._1.Visibilite;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Créé par lhameury le 11/12/18.
 */
@Data
public class CreerDossieModel {
    private String loggerPrefix = "creerDossier (%s) : ";
    private String username;
    private String wsUsername;
    private DOCTYPE mainDocType;
    private NodeRef parapheur;
    private String id;
    private String title;
    private String type;
    private NodeRef dossier;
    private NodeRef mainDoc;
    private NodeRef workflow;
    private String dateLimite;
    private Visibilite visibilite;
    private HashMap<String, String> docNames = new HashMap<>();
    private Map<QName, Serializable> typageProps;
    private Map<QName, Serializable> customProperties = new HashMap<>();
    private String sousType;
    private String protocole;
    private String sigFormat;
    private nu.xom.Document docXml;
    private TypeDocAnnexes docSuppLst;
    private List<CustomProperty<?>> scriptCustomProperties = null;
    private List<Map<String, Serializable>> ordreDocuments = null;
    
    public enum DOCTYPE {
        XML,
        PDF,
        BUREAUTIQUE,
        OTHER
    }
}
