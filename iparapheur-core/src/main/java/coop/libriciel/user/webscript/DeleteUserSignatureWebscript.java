package coop.libriciel.user.webscript;

import coop.libriciel.service.UserUtilsService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@NoArgsConstructor
@Controller
public class DeleteUserSignatureWebscript extends AbstractWebScript  {

    @Autowired
    private UserUtilsService userUtilsService;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        this.userUtilsService.removeCurrentUserSignatureImage();
    }
}
