package coop.libriciel.service;

import com.atolcd.parapheur.model.ParapheurModel;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Service
public class DeskUtilsService {

    @Autowired
    private ServiceRegistry serviceRegistry;

    @Autowired
    private NodeUtilsService nodeUtilsService;


    public String getDeskName(NodeRef desk) {
        return this.serviceRegistry.getNodeService().getProperty(desk, ContentModel.PROP_TITLE).toString();
    }

    /**
     * Renvoie la ou les autorité(s) proprietaire(s) d'une parapheur,
     * c'est à dire une liste de nom(s) d'utilisateur(s)
     * @param desk le parapheur
     * @return la liste des noms des autorités propriétaires
     * @throws IllegalArgumentException si le nodeRef est null ou n'existe pas dans le repository
     */
    public List<String> getDeskOwners(NodeRef desk) {
        NodeRef parapheur = nodeUtilsService.getFirstParentOfType(desk, ParapheurModel.TYPE_PARAPHEUR);
        if (parapheur == null) {
            return null;
        }
        //noinspection unchecked
        return (ArrayList<String>) this.serviceRegistry.getNodeService().getProperty(parapheur, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR);
    }

    public List<String> getDeskSecretaries(NodeRef parapheur) {
        List<String> lstSecretaires;
        if (this.serviceRegistry.getNodeService().getProperty(parapheur, ParapheurModel.PROP_SECRETAIRES) != null) {
            //noinspection unchecked
            lstSecretaires = (List<String>) this.serviceRegistry.getNodeService().getProperty(parapheur, ParapheurModel.PROP_SECRETAIRES);
        } else {
            lstSecretaires = Collections.emptyList();
        }

        return lstSecretaires;
    }

    public List<NodeRef> getDesksOnDelegationPath(NodeRef desk) {
        // On parcourt l'arbre de délégation et on ajoute chaque parapheur rencontré
        // à un Set. Si on a déjà rencontré le parapheur, c'est qu'il y a une boucle
        // et on lève donc une exception.
        List<NodeRef> delegues = new ArrayList<>();
        HashSet<NodeRef> arbreDelegations = new HashSet<>();
        NodeRef parapheurCourant = desk;
        NodeRef delegue = getDelegation(parapheurCourant);
        boolean boucle = false;
        while (!boucle && (delegue != null)) {
            if (arbreDelegations.add(parapheurCourant)) {
                delegues.add(delegue);
            } else {
                boucle = true;
                delegues = new ArrayList<>();
            }
            parapheurCourant = delegue;
            delegue = getDelegation(parapheurCourant);
        }
        return delegues;
    }

    public NodeRef getDelegation(NodeRef parapheur) {
        if (parapheur == null) {
            return null;
        }

        List<AssociationRef> listAssoc = this.serviceRegistry.getNodeService().getTargetAssocs(parapheur, ParapheurModel.ASSOC_DELEGATION);
        if (listAssoc != null && listAssoc.size() == 1) {
            return listAssoc.get(0).getTargetRef();
        }

        return null;
    }

    public boolean isUserSecretary(NodeRef desk, String username) {
        //noinspection unchecked
        List<String> lstSecretaires = (List<String>) this.serviceRegistry.getNodeService().getProperty(desk, ParapheurModel.PROP_SECRETAIRES);
        return lstSecretaires != null && lstSecretaires.contains(username);
    }
}
