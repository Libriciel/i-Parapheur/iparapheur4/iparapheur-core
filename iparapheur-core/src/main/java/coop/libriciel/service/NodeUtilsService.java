package coop.libriciel.service;

import lombok.extern.log4j.Log4j;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
@Log4j
public class NodeUtilsService {

    @Autowired
    private ServiceRegistry serviceRegistry;

    public boolean isOfType(NodeRef nodeRef, QName type) {

        Assert.notNull(nodeRef, "Node Ref is mandatory");
        Assert.notNull(this.serviceRegistry.getNodeService(), "NodeService is mandatory");
        Assert.isTrue(this.serviceRegistry.getNodeService().exists(nodeRef), "Node Ref must exist in the repository");
        Assert.notNull(type, "Type is mandatory");

        // find it's type so we can see if it's a node we are interested in
        QName type2 = this.serviceRegistry.getNodeService().getType(nodeRef);

        // make sure the type is defined in the data dictionary
        TypeDefinition typeDef = this.serviceRegistry.getDictionaryService().getType(type2);

        if (typeDef == null) {
            if (log.isEnabledFor(Level.WARN)) {
                log.warn("Found invalid object in database: id = " + nodeRef + ", type = " + type2);
            }

            return false;
        }

        return this.serviceRegistry.getDictionaryService().isSubClass(type2, type);
    }

    public NodeRef getFirstChild(NodeRef nodeRef, QName childAssocType) {
        List<ChildAssociationRef> childAssocs = this.serviceRegistry.getNodeService().getChildAssocs(nodeRef, childAssocType,
                RegexQNamePattern.MATCH_ALL);
        return (childAssocs.size() == 1) ? childAssocs.get(0).getChildRef() : null;
    }

    public NodeRef getFirstParentOfType(NodeRef nodeRef, QName parentType) {
        Assert.notNull(nodeRef, "Node Ref is mandatory");
        Assert.isTrue(this.serviceRegistry.getNodeService().exists(nodeRef), "Node Ref must exist in the repository");
        Assert.notNull(parentType, "Type is mandatory");

        for (NodeRef parent = nodeRef; parent != null && this.serviceRegistry.getNodeService().exists(parent); parent = this.serviceRegistry.getNodeService().getPrimaryParent(parent).getParentRef()) {
            if (isOfType(parent, parentType)) {
                return parent;
            }
        }

        return null;
    }

}
