package coop.libriciel.service;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CustomMetadataDef;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.MetadataService;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import lombok.extern.log4j.Log4j;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.filestore.FileContentWriter;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.alfresco.util.TempFileProvider;
import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Log4j
public class FolderUtilsService {

    @Autowired
    private ServiceRegistry serviceRegistry;

    @Autowired
    private NodeUtilsService nodeUtilsService;

    @Autowired
    private MetadataService metadataService;

    @Value("${spaces.company_home.childname}")
    private String companyHomeName;
    @Value("${spaces.dictionary.childname}")
    private String dictionaryName;
    @Value("${spaces.templates.content.childname}")
    private String templatesName;
    @Value("${templates.signataires.childname}")
    private String sigTemplateName;

    @Value("${spaces.store}")
    private String spacesStore;

    public List<EtapeCircuit> getWorkflow(NodeRef dossier) {
        Assert.isTrue(nodeUtilsService.isOfType(dossier, ParapheurModel.TYPE_DOSSIER), "Node Ref doit être un ph:dossier");

        List<EtapeCircuit> circuit = new ArrayList<>();

        NodeRef emetteur = nodeUtilsService.getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        if (emetteur == null || !this.serviceRegistry.getNodeService().exists(emetteur)) {
            return null;
        }
        circuit.add(new EtapeCircuitImpl(this.serviceRegistry.getNodeService(), emetteur));

        for (NodeRef etape = nodeUtilsService.getFirstChild(emetteur, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
             etape != null && this.serviceRegistry.getNodeService().exists(etape);
             etape = nodeUtilsService.getFirstChild(etape, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE)) {
            circuit.add(new EtapeCircuitImpl(this.serviceRegistry.getNodeService(), etape));
        }

        return circuit;
    }

    public boolean isOver(NodeRef dossier) {
        return (Boolean) this.serviceRegistry.getNodeService().getProperty(dossier, ParapheurModel.PROP_TERMINE);
    }

    public NodeRef getEmetteur(NodeRef dossier) {
        NodeRef emetteur = nodeUtilsService.getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        if (emetteur != null) {
            return (NodeRef) this.serviceRegistry.getNodeService().getProperty(emetteur, ParapheurModel.PROP_PASSE_PAR);
        } else {
            return null;
        }
    }

    public boolean isRejected(NodeRef dossier) {
        String statusMetier = (String) this.serviceRegistry.getNodeService().getProperty(dossier, ParapheurModel.PROP_STATUS_METIER);
        return statusMetier.startsWith("Rejet");
    }

    public EtapeCircuit getCurrentStep(NodeRef dossier) {
        List<EtapeCircuit> circuit = getWorkflow(dossier);
        if (circuit == null) {
            return null;
        }
        if (isOver(dossier) && circuit.get(0).getActionDemandee() == null) {
            return null;
        }
        return getCurrentStep(circuit);
    }

    public EtapeCircuit getCurrentStep(List<EtapeCircuit> circuit) {
        if (circuit == null) {
            return null;
        }

        for (EtapeCircuit etape : circuit) {
            if (!etape.isApproved()) {
                return etape;
            }
        }
        return null;
    }

    public List<EtapeCircuit> getCircuitAvecSignataireInfos(NodeRef dossier) {
        List<EtapeCircuit> circuit = new ArrayList<>();

        NodeRef emetteur = nodeUtilsService.getFirstChild(dossier, ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        if (emetteur == null || !this.serviceRegistry.getNodeService().exists(emetteur)) {
            return null;
        }
        circuit.add(new EtapeCircuitImpl(this.serviceRegistry.getNodeService(), emetteur, true));

        for (NodeRef etape = nodeUtilsService.getFirstChild(emetteur, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);
             etape != null && this.serviceRegistry.getNodeService().exists(etape);
             etape = nodeUtilsService.getFirstChild(etape, ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE)) {
            circuit.add(new EtapeCircuitImpl(this.serviceRegistry.getNodeService(), etape, true));
        }

        return circuit;
    }

    public File generateFrontPage(NodeRef folder, String statutTdt, String ackDate) {
        // Récupération du template de la page de garde
        String xpath = String.format("%s/%s/%s/%s", companyHomeName, dictionaryName, templatesName, sigTemplateName);
        List<NodeRef> results;
        results = this.serviceRegistry.getSearchService().selectNodes(this.serviceRegistry.getNodeService().getRootNode(new StoreRef(spacesStore)), xpath, null, this.serviceRegistry.getNamespaceService(), false);
        if (results != null && results.size() == 1) {  // Génération de la page de garde
            NodeRef templateRef = results.get(0);
            Map<String, Object> model = new HashMap<>();
            List<EtapeCircuit> circuit = this.getCircuitAvecSignataireInfos(folder);

            model.put("etapes", circuit);
            model.put("dossier", folder);
            model.put("space", folder);
            model.put("statut", statutTdt);

            model.put("dateGeneration", new Date());

            String statutMetier = (String) this.serviceRegistry.getNodeService().getProperty(folder, ParapheurModel.PROP_STATUS_METIER);
            boolean rejected = statutMetier.startsWith("Rejet");

            int etapeRejet = -1;

            if (rejected) {
                for (int i = 0; i < circuit.size(); i++) {
                    EtapeCircuit etape = circuit.get(i);
                    if (etape.isApproved()) {
                        etapeRejet = i;
                    }
                }
            }

            model.put("etape_rejet", etapeRejet);

            if (ackDate != null) {
                model.put("ackMIAT", ackDate);
            }

            // Méta-données
            List<CustomMetadataDef> mdliste = metadataService.getMetadataDefs();
            if (!mdliste.isEmpty()) {
                QName metadataQName = null;
                try {
                    metadataQName = QName.createQName("cu:customMetadata", this.serviceRegistry.getNamespaceService());
                } catch (org.alfresco.service.namespace.InvalidQNameException qe) {
                    log.error("Unable to set up metadata QName : " + qe.getLocalizedMessage());
                } catch (org.alfresco.service.namespace.NamespaceException ne) {
                    log.error("Unable to....: " + ne.getLocalizedMessage());
                }
                Map<String, Object> metadonnees = new HashMap<>();
                if (metadataQName != null && this.serviceRegistry.getNodeService().hasAspect(folder, metadataQName)) {
                    for (CustomMetadataDef customMetadataDef : mdliste) {
                        if (this.serviceRegistry.getNodeService().getProperty(folder, customMetadataDef.getName()) != null) {
                            String nomMD = customMetadataDef.getTitle();
                            switch (customMetadataDef.getType()) {
                                case DATE:
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    Date date = (Date) this.serviceRegistry.getNodeService().getProperty(folder, customMetadataDef.getName());
                                    metadonnees.put("DATE_" + nomMD, dateFormat.format(date));
                                    metadonnees.put("DATE_B_" + nomMD, date);
                                    break;
                                case DOUBLE:
                                    Double penetration = (Double) this.serviceRegistry.getNodeService().getProperty(folder, customMetadataDef.getName());
                                    metadonnees.put("DOUBLE_" + nomMD, penetration);
                                    break;
                                case INTEGER:
                                    Integer vinteger = (Integer) this.serviceRegistry.getNodeService().getProperty(folder, customMetadataDef.getName());
                                    metadonnees.put("INTEGER_" + nomMD, vinteger);
                                    break;
                                case BOOLEAN:
                                    Boolean vbool = (Boolean) this.serviceRegistry.getNodeService().getProperty(folder, customMetadataDef.getName());
                                    metadonnees.put("STRING_" + nomMD, Boolean.TRUE.equals(vbool) ? "Oui" : "Non");
                                    break;
                                case STRING:
                                case URL:
                                default:
                                    metadonnees.put("STRING_" + nomMD, this.serviceRegistry.getNodeService().getProperty(folder, customMetadataDef.getName()).toString());
                            }
                        }
                    }
                    if (!metadonnees.isEmpty()) {
                        model.put("metadonnees", metadonnees);
                    }
                }
            }

            // Création de la page de garde
            File tmpHtml = TempFileProvider.createTempFile("tmpconv", ".html");
            ContentWriter tmpWriterHtml = new FileContentWriter(tmpHtml);

            tmpWriterHtml.setMimetype(MimetypeMap.MIMETYPE_HTML);
            tmpWriterHtml.setEncoding("utf-8");
            BufferedWriter writer;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(tmpWriterHtml.getContentOutputStream(), tmpWriterHtml.getEncoding()));
            } catch (UnsupportedEncodingException uee) {  // fallback to default encoding
                if (log.isEnabledFor(Level.WARN)) {
                    log.warn("Unsupported encoding: " + tmpWriterHtml.getEncoding() + ", falling back to default encoding.", uee);
                }
                tmpWriterHtml.setEncoding(null);
                writer = new BufferedWriter(new OutputStreamWriter(tmpWriterHtml.getContentOutputStream()));
            }

            this.serviceRegistry.getTemplateService().processTemplate("freemarker", templateRef.toString(), model, writer);
            try {
                writer.close();
            } catch (IOException ioe) {
                if (log.isEnabledFor(Level.WARN)) {
                    log.warn("Error closing the writer on " + tmpHtml, ioe);
                }
            }

            // Conversion au format PDF
            ContentReader tmpReaderHtml = tmpWriterHtml.getReader();
            if (tmpReaderHtml != null) {
                File tmpPdf = TempFileProvider.createTempFile("tmpconv", ".pdf");

                try {
                    // Tentative de génération du bordereau avec itext pour prise en charge du CSS
                    com.itextpdf.text.Document document = new com.itextpdf.text.Document(PageSize.A4);
                    PdfWriter pdfwriter = PdfWriter.getInstance(document, new FileOutputStream(tmpPdf));
                    document.open();
                    XMLWorkerHelper.getInstance().parseXHtml(pdfwriter, document, tmpReaderHtml.getContentInputStream());
                    document.close();
                } catch (Exception e) {
                    log.warn("Transformation PDF impossible avec itext", e);

                    tmpReaderHtml = tmpWriterHtml.getReader();
                    FileContentWriter tmpWriterPdf = new FileContentWriter(tmpPdf);
                    tmpWriterPdf.setMimetype(MimetypeMap.MIMETYPE_PDF);
                    tmpWriterPdf.setEncoding(tmpReaderHtml.getEncoding());
                    try {
                        this.serviceRegistry.getContentService().transform(tmpReaderHtml, tmpWriterPdf);
                    } catch (NoTransformerException ex) {
                        throw new RuntimeException(ex);
                    }
                }

                return tmpPdf;
            }
        } // Fin generation page de garde
        return null;
    }

    public List<NodeRef> getMainDocuments(NodeRef dossier) {
        List<ChildAssociationRef> childAssocs = this.serviceRegistry.getNodeService().getChildAssocs(dossier, ContentModel.ASSOC_CONTAINS,
                RegexQNamePattern.MATCH_ALL);
        List<NodeRef> res = new ArrayList<>();
        NodeRef first = null;
        if (childAssocs != null) {
            for (ChildAssociationRef child : childAssocs) {
                NodeRef childRef = child.getChildRef();
                QName childType = this.serviceRegistry.getNodeService().getType(childRef);
                // Backward compatibility : Before V4 the main document was the first one.
                if ((first == null) && childType.equals(ContentModel.TYPE_CONTENT)) {
                    first = childRef;
                }

                if (this.serviceRegistry.getNodeService().hasAspect(childRef, ParapheurModel.ASPECT_MAIN_DOCUMENT)) {
                    res.add(childRef);
                }
            }
            if (res.isEmpty() && (first != null)) {
                // We set the first document as the main document
                this.serviceRegistry.getNodeService().addAspect(first, ParapheurModel.ASPECT_MAIN_DOCUMENT, null);
                res.add(first); // Backward compatibility...
            }
        }

        return res;
    }

    public void deleteImagesPreviews(NodeRef document, NodeRef dossier) {
        AuthenticationUtil.runAs(() -> {
            String xpath = "/app:company_home/app:dictionary/ph:apercus_png/ph:dossier" + dossier.getId();
            List<NodeRef> results = this.serviceRegistry.getSearchService().selectNodes(this.serviceRegistry.getNodeService().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                    xpath,
                    null,
                    this.serviceRegistry.getNamespaceService(),
                    false);

            if (!results.isEmpty()) {
                NodeRef dossierImagesFolder = results.get(0);

                NodeRef documentImagesFolder = this.serviceRegistry.getFileFolderService().searchSimple(dossierImagesFolder, document.getId());

                if(documentImagesFolder != null) {
                    try {
                        this.serviceRegistry.getFileFolderService().delete(documentImagesFolder);
                    } catch (Exception e) {
                        log.trace(e);
                    }
                }
            }
            return null;
        }, AuthenticationUtil.getAdminUserName());
    }
}
