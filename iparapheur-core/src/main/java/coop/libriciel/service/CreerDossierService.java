package coop.libriciel.service;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.*;
import coop.libriciel.model.CreerDossieModel;
import coop.libriciel.model.CreerDossieModel.DOCTYPE;
import coop.libriciel.utils.poppler.PopplerUtils;
import coop.libriciel.signature.service.SignatureService;
import lombok.extern.log4j.Log4j;
import nu.xom.ParsingException;
import org.adullact.iparapheur.repo.amq.MessagesSender;
import org.adullact.iparapheur.repo.jscript.ScriptEtapeCircuitImpl;
import org.adullact.iparapheur.repo.worker.WorkerService;
import org.adullact.iparapheur.rules.bean.CustomProperty;
import org.adullact.libersign.util.signature.PKCS7VerUtil;
import org.adullact.spring_ws.iparapheur._1.*;
import org.adullact.utils.StringUtils;
import org.alfresco.jlan.server.auth.InvalidUserException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.filestore.FileContentReader;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.security.NoSuchPersonException;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Level;
import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONStringer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import java.io.*;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Créé par lhameury le 11/12/18.
 */
@Service
@Log4j
public class CreerDossierService {

    @Autowired
    private NodeService nodeService;

    @Autowired
    private FileFolderService fileFolderService;

    @Autowired
    private ParapheurService parapheurService;

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private TypesService typesService;

    @Autowired
    private ContentService contentService;

    @Autowired
    private MetadataService metadataService;

    @Autowired
    private DossierService dossierService;

    @Autowired
    private AuthenticationComponent authenticationComponent;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private WsService wsService;

    @Autowired
    private NamespaceService namespaceService;

    @Autowired
    private MessagesSender messagesSender;

    @Autowired
    private SignatureService signatureService;

    @Value("${parapheur.ws.creerdossier.autoselectsigformat.ifunsigned.pdf:pades}")
    private String sigformatifunsignedpdf;
    @Value("${parapheur.ws.creerdossier.autoselectsigformat.ifunsigned:pkcs7}")
    private String sigformatifunsigned;

    /**
     * Initialisation du Pool de Threads (géré par Spring)
     */
    protected void init() {
        log.setLevel(Level.INFO);
    }

    public CreerDossierResponse doCreateFile(CreerDossierRequest request) {
        MessageRetour msg = new MessageRetour();

        CreerDossierResponse res = new CreerDossierResponse();

        CreerDossieModel model = new CreerDossieModel();

        msg.setCodeRetour("KO");
        msg.setMessage("Requete incomplete.");
        msg.setSeverite("FATAL");

        // Do all verifications in a transaction
        UserTransaction tx = transactionService.getUserTransaction(false);

        try {
            tx.begin();

            // Vérification de la requete
            this.checkAndBuildRequest(request, model);
            // Création du dossier
            this.createDossier(request, model);

            AuthenticationUtil.setRunAsUser(model.getUsername());
            authenticationComponent.setCurrentUser(model.getUsername());

            // Définition de la typologie et document principal
            this.setTypologyAndContents(request, model);

            // Gestion des documents supplémentaires
            if (request.getDocumentsSupplementaires() != null) {
                log.debug(model.getLoggerPrefix() + " - Gestion des documents supplémentaires...");
                this.handleDocsSupp(request, model);
            }
            // Gestion des documents annexes
            if (request.getDocumentsAnnexes() != null) {
                log.debug(model.getLoggerPrefix() + " - Gestion des annexes...");
                this.handleDocsAnnexes(request, model);
            }
            // Envoi du dossier dans le circuit
            log.debug(model.getLoggerPrefix() + " - Envoi du document dans le circuit de validation...");

            // Gestion de l'ordre des documents
            if(model.getOrdreDocuments()!=null && !model.getOrdreDocuments().isEmpty()) {
                dossierService.setOrderDocuments(model.getDossier(), model.getOrdreDocuments());
            }

            tx.commit();
        } catch (Exception e) {
            try {
                tx.rollback();
            } catch (SystemException e1) {
                log.error("Erreur lors du rollback de la vérification de la requête", e1);
            }
            log.error(e.getMessage(), e);
            return this.handleException(e);
        }

        // Envoi du dossier dans le workflow
        this.sendFileInWorkflow(request, model);

        msg.setCodeRetour("OK");
        msg.setMessage("Dossier " + model.getTitle() + " soumis dans le circuit");
        msg.setSeverite("INFO");

        res.setMessageRetour(msg);
        // TODO - IMPORTANT - CE IF DOIT ÊTRE CONSERVÉ !
        // FIXME - Retro-compatibilité - Pour les connecteurs n'utilisant pas les dernières versions du WSDL, comprenant le champ "DossierID"
        // Pour information - Ce champ a été ajouté en 2013
        if(request.getDossierID() == null || request.getDossierID().trim().isEmpty()) {
            res.setDossierID(model.getId());
        }

        return res;
    }

    private void setTypologyAndContents(CreerDossierRequest request, CreerDossieModel model) throws IOException {
        log.debug(model.getLoggerPrefix() + " - Préparation du circuit...");
        // This defines the final workflow to the folder
        typesService.getWorkflow(model.getDossier(), model.getType(), model.getSousType(), model.getScriptCustomProperties());

        log.debug(model.getLoggerPrefix() + " - Préparation multidoc...");
        this.prepareMultidoc(request, model);

        log.debug(model.getLoggerPrefix() + " - Définition du circuit...");
        this.setWorkflow(request, model);

        log.debug(model.getLoggerPrefix() + " - Gestion des aspects de typologie...");
        if (model.getProtocole() != null && model.getProtocole().equalsIgnoreCase("HELIOS")) {
            if (typesService.isTdtAuto(model.getType(), model.getSousType())) {
                nodeService.addAspect(model.getDossier(), ParapheurModel.ASPECT_ETAPE_TDT_AUTO, null);
            }
        }
        if (request.getXPathPourSignatureXML() != null) {
            model.getTypageProps().put(ParapheurModel.PROP_XPATH_SIGNATURE, request.getXPathPourSignatureXML());
        }
        nodeService.addAspect(model.getDossier(), ParapheurModel.ASPECT_TYPAGE_METIER, model.getTypageProps());

        log.debug(model.getLoggerPrefix() + " - Métadonnées personalisées...");
        if (!model.getCustomProperties().isEmpty()) {
            nodeService.addAspect(model.getDossier(), QName.createQName("cu:customMetadata", namespaceService), model.getCustomProperties());
        }

        log.debug(model.getLoggerPrefix() + " - Définition des annotations...");
        // ------------------- Annotations (publique + privee)
        if (request.getAnnotationPublique() != null) {
            parapheurService.setAnnotationPublique(model.getDossier(), request.getAnnotationPublique());
        }
        if (request.getAnnotationPrivee() != null) {
            parapheurService.setAnnotationPrivee(model.getDossier(), request.getAnnotationPrivee());
        }

        log.debug(model.getLoggerPrefix() + " - Définition du contenu...");
        this.setContent(request, model);
        this.setVisuel(request, model);

        log.debug(model.getLoggerPrefix() + " - Définition de signature détachée...");
        this.setSignatureDetachee(request, model);

        log.debug(model.getLoggerPrefix() + " - Finalisation de la création...");
        //redéfinition de la propriété, obligatoire sinon non pris en compte
        nodeService.setProperty(model.getDossier(), ParapheurModel.PROP_RECUPERABLE, Boolean.TRUE);

        // Cachet serveur automatique ?
        if (typesService.isCachetAuto(model.getType(), model.getSousType())) {
            nodeService.addAspect(model.getDossier(), ParapheurModel.ASPECT_ETAPE_CACHET_AUTO, null);
        }
    }

    private void createDossier(CreerDossierRequest request, CreerDossieModel model) throws ParseException {
        Map<QName, Serializable> properties = this.defineProperties(model);

        log.debug(model.getLoggerPrefix() + " - Creation de dossier...");
        // ------------------------------------- Création dossier
        model.setDossier(dossierService.createDossier(model.getParapheur(), properties));
        dossierService.setDossierPropertiesWithAuditTrail(model.getDossier(), model.getParapheur(), properties, true);
        model.setId((String) nodeService.getProperty(model.getDossier(), ContentModel.PROP_NAME));
        nodeService.addAspect(model.getDossier(), ParapheurModel.ASPECT_DOSSIER_ORIGINE_WS, null);

        log.debug(model.getLoggerPrefix() + " - Creation de dossier OK");
    }

    private void checkAndBuildRequest(CreerDossierRequest request, CreerDossieModel model) throws Exception {

        model.setUsername(this.getValidUser());

        // Spécificité BLEX
        request.setTypeTechnique(this.handleBlexSpecificity(model.getUsername(), request.getTypeTechnique()));

        if (log.isInfoEnabled()) {
            log.info(model.getLoggerPrefix() + "START");
        }

        if(request.getNomDocPrincipal() != null) {
            request.setNomDocPrincipal(Normalizer.normalize(request.getNomDocPrincipal(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""));
        }

        // Vérifications basiques sur l'intégrité de la requête
        this.checkBasicRequest(request);

        // Récupération du type de document principal
        model.setMainDocType(this.getDocType(request.getDocumentPrincipal(), request.getNomDocPrincipal()));

        // Vérifications basiques sur l'intégrité du document de la requête
        this.checkRequestWithDocument(request, model.getMainDocType());

        // Vérifications basiques sur l'intégrité de la typologie de la requête
        this.checkRequestWithTypology(request);

        // Vérifications de l'utilisateur
        model.setParapheur(parapheurService.getUniqueParapheurForUser(model.getUsername()));
        if (model.getParapheur() == null) {
            throw new InvalidUserException("Requete incorrecte, '" + model.getUsername() + "' n'a pas de parapheur.");
        }

        // Vérification de l'utilisateur spécifique WS
        model.setWsUsername(this.getWSUsername(request));

        /*
         * Determiner dossierID ('name') et Titre ('title')
         */
        if (request.getDossierTitre() != null && !request.getDossierTitre().trim().isEmpty()) {
            model.setTitle(request.getDossierTitre().trim());
            model.setId((request.getDossierID() != null && !request.getDossierID().trim().isEmpty()) ? request.getDossierID().trim() : null);
        } else {
            model.setId(request.getDossierID().trim());
            model.setTitle(model.getId());
        }

        /*
         * Determiner Type et Sous-type
         */
        model.setType(request.getTypeTechnique().trim());
        model.setSousType(request.getSousType().trim());
        model.setDateLimite(request.getDateLimite());
        model.setVisibilite(request.getVisibilite());

        this.setWorkflowInModel(request, model);

        this.buildXmlInModel(request, model);

        try {
            this.checkProtectedPdf(request.getDocumentPrincipal(), request.getNomDocPrincipal());
        } catch(IOException e) {
            throw new Exception("Requete incorrecte, le document principal n'est pas un PDF valide", e);
        }

        this.checkVisuelPdf(request);

        this.checkDocSupp(request);

        this.checkDocAnnexes(request);

        this.setSigFormatAndProtocol(request, model);

        this.checkProtocolLimit(request, model);

        this.checkDocumentWithProtocol(request, model);

        this.checkHeliosProperties(request, model);
    }

    private void sendFileInWorkflow(CreerDossierRequest request, CreerDossieModel model) {
        String USERNAME = "username";
        String ACTION = "action";

        JSONStringer jsonStringer = new JSONStringer();
        try {
            jsonStringer.object()
                    .key(WorkerService.ID).value(model.getDossier().getId())
                    .key(WorkerService.BUREAUCOURANT).value(model.getParapheur().getId())
                    .key(WorkerService.TYPE).value(WorkerService.TYPE_DOSSIER)
                    .key(ACTION).value(DossierService.ACTION_DOSSIER.VISA)
                    .key(USERNAME).value(model.getUsername())
                    .key(WorkerService.ANNOTPUB).value(request.getAnnotationPublique() != null ? request.getAnnotationPublique() : "")
                    .key(WorkerService.ANNOTPRIV).value(request.getAnnotationPrivee() != null ? request.getAnnotationPrivee() : "")
                    .endObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String msgVisa = jsonStringer.toString();

        messagesSender.sendWorker(msgVisa);
    }

    private void handleDocAnnexes(DocAnnexe leDoc, CreerDossieModel model) {
        try {
            FileInfo fiAnnexe = fileFolderService.create(model.getDossier(), leDoc.getNom(), ContentModel.TYPE_CONTENT);
            NodeRef docAnnexeRef = fiAnnexe.getNodeRef();
            
            Map<String, Serializable> docAnnexe = new HashMap<String, Serializable>();
            docAnnexe.put("id", docAnnexeRef.getId());
            docAnnexe.put("isMainDocument", Boolean.FALSE);
            if(model.getOrdreDocuments()==null) {
            	model.setOrdreDocuments(new ArrayList<>());
            }
            model.getOrdreDocuments().add(docAnnexe);
            
            ContentWriter cwAnnexe = contentService.getWriter(docAnnexeRef, ContentModel.PROP_CONTENT, Boolean.TRUE);
            cwAnnexe.setMimetype(leDoc.getMimetype().trim());
            cwAnnexe.setEncoding(leDoc.getEncoding().trim());

            InputStream cisDoc = new ByteArrayInputStream(leDoc.getFichier().getValue());
            cwAnnexe.putContent(cisDoc);
        } catch(Throwable e) {
            e.printStackTrace();
        }

    }

    private void handleDocsAnnexes(CreerDossierRequest request, CreerDossieModel model) {
        TypeDocAnnexes docs = request.getDocumentsAnnexes();
        int increment = 1;
        for (DocAnnexe leDoc : docs.getDocAnnexe()) {
            // Ajout du document leDoc, s'il est valide
            if (leDoc != null && leDoc.getEncoding() != null && !leDoc.getEncoding().trim().isEmpty()
                    && leDoc.getMimetype() != null && !leDoc.getMimetype().trim().isEmpty()
                    && (wsService.checkAcceptableMimeTypeBureautique(leDoc.getMimetype().trim())
                        || wsService.checkAcceptableMimeTypeXML(leDoc.getMimetype().trim())
                        || wsService.checkAcceptableMimeTypeCompressed(leDoc.getMimetype().trim()))
                    && leDoc.getNom() != null && !leDoc.getNom().trim().isEmpty()
                    && leDoc.getFichier() != null && leDoc.getFichier().getValue() != null) {
                /*
                 * Pour éviter les problèmes de nommage, les
                 * annexes sont prefixees par un numero d'ordre
                 * si necessaire.
                 */
                String nom = leDoc.getNom().trim();
                nom = StringEscapeUtils.unescapeHtml(nom).replaceAll("[\\p{M}]", "");
                if (model.getDocNames().containsKey(nom)) {
                    nom = "Annexe" + increment + "- " + nom;
                    leDoc.setNom(nom);
                    increment++;
                }
                leDoc.setNom(Normalizer.normalize(nom, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""));
                model.getDocNames().put(nom, nom);

                this.handleDocAnnexes(leDoc, model);
            } else {
                if (log.isEnabledFor(Level.ERROR)) {
                    log.error(model.getLoggerPrefix() + "doc annexe invalide, donc non ajouté au dossier.");
                }
            }
        }
    }
    
    private void checkDocAnnexes(CreerDossierRequest request) throws DocumentException {
        if (request.getDocumentsAnnexes() != null) {
            this.checkListTypeAnnexe(request.getDocumentsAnnexes(), false);
        }
    }

    private void handleDocSupp(DocAnnexe leDoc, CreerDossieModel model) {
        try {
            leDoc.setNom(Normalizer.normalize(leDoc.getNom(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""));
            FileInfo fiDocSupp = fileFolderService.create(model.getDossier(), leDoc.getNom(), ContentModel.TYPE_CONTENT);
            NodeRef docSuppRef = fiDocSupp.getNodeRef();

            // Mémorisation doc supplementaire
            Map<String, Serializable> docSuppl = new HashMap<String, Serializable>();
            docSuppl.put("id", docSuppRef.getId());
            docSuppl.put("isMainDocument", Boolean.TRUE);
            if(model.getOrdreDocuments()==null) {
            	model.setOrdreDocuments(new ArrayList<>());
            }
            model.getOrdreDocuments().add(docSuppl);
            
            nodeService.addAspect(docSuppRef, ParapheurModel.ASPECT_MAIN_DOCUMENT, null);

            ContentWriter cwdocSupp = contentService.getWriter(docSuppRef, ContentModel.PROP_CONTENT, Boolean.TRUE);
            String lContentTypeSup = leDoc.getMimetype().trim();

            cwdocSupp.setMimetype(lContentTypeSup);
            cwdocSupp.setEncoding(leDoc.getEncoding().trim());

            InputStream cisDoc = new ByteArrayInputStream(leDoc.getFichier().getValue());
            cwdocSupp.putContent(cisDoc);

            // ------------------- Visuel PDF en Propriété du document principal
            if (!lContentTypeSup.equals(MimetypeMap.MIMETYPE_PDF)) {
                ContentWriter pdfwriter = contentService.getWriter(docSuppRef, ParapheurModel.PROP_VISUEL_PDF, Boolean.TRUE);
                pdfwriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
                ContentReader reader = contentService.getReader(docSuppRef, ContentModel.PROP_CONTENT);
                contentService.transform(reader, pdfwriter);

                // BLEX : Information pour statistiques d'usage
                log.info("Creation dossier : origine=WS; idDossier=" + model.getId() + "; document=PDF; size=" + pdfwriter.getSize() + ";");
            } else {
                FileContentReader reader = (FileContentReader) cwdocSupp.getReader();
                PopplerUtils utils = new PopplerUtils();
                if(utils.isSigned(reader.getFile().getAbsolutePath())) {
                    nodeService.addAspect(docSuppRef, ParapheurModel.ASPECT_SIGNED, null);
                }
            }

            // Handle detached signature
            setSignatureDetacheeForDocument(docSuppRef, leDoc.getSignature(), model);
        } catch(Throwable e) {
            e.printStackTrace();
        }
    }

    private void handleDocsSupp(CreerDossierRequest request, CreerDossieModel model) {
        int increment = 1;
        for (DocAnnexe leDoc : request.getDocumentsSupplementaires().getDocAnnexe()) {

            // Ajout du document leDoc
            /*
             * Pour éviter les problèmes de nommage, les
             * docs sont prefixes par un numero d'ordre
             * si necessaire.
             */
            String nom = leDoc.getNom().trim();
            nom = StringEscapeUtils.unescapeHtml(nom).replaceAll("[\\p{M}]", "");
            if (model.getDocNames().containsKey(nom)) {
                nom = "Doc" + increment + "- " + nom;
                leDoc.setNom(nom);
                increment++;
            }
            model.getDocNames().put(nom, nom);

            this.handleDocSupp(leDoc, model);

            if (leDoc.getSignature() != null) {   // signature, optionnel
                String leSigType = leDoc.getSignature().getContentType();
                if (leSigType != null && (leSigType.equalsIgnoreCase("application/x-pkcs7-signature") || leSigType.equalsIgnoreCase(MimetypeMap.MIMETYPE_ZIP))) {
                    // TODO: propriété signature sur étape1 , NON GERE
                    log.warn(model.getLoggerPrefix() + "signature detachee pour '" + leDoc.getNom() + "' detectee dans la requete, MAIS ignoree.");
                }
            }
        }
    }

    private void setSignatureDetachee(CreerDossierRequest request, CreerDossieModel model) {
        // ------------------- Signature doc Principal (optionnel)
        if (request.getSignatureDocPrincipal() != null) {
            String leSigType = request.getSignatureDocPrincipal().getContentType();
            if (leSigType.equalsIgnoreCase("application/pkcs7-signature")
                    || leSigType.equalsIgnoreCase("application/x-pkcs7-signature")) {

                // BeginPatch - LIMOUSIN
                byte[] signedBytes = request.getSignatureDocPrincipal().getValue();
                if (model.getSigFormat().startsWith("PKCS#7")) {
                    int stI = (signedBytes.length > 10 ? 10 : signedBytes.length);
                    String sigStart = new String(signedBytes, 0, stI);
                    if (!sigStart.equals("-----BEGIN")) {
                        /*
                         * On stocke la signature format PEM, pas DER
                         */
                        signedBytes = PKCS7VerUtil.der2pem(signedBytes);
                    }
                }
                // FinPatch


                // TODO  check signature (au moins la structure, mais pas la validité c'est trop tôt)

                parapheurService.setSignature(model.getDossier(), signedBytes, "PKCS#7/single");
                parapheurService.setSignature(model.getDossier(), parapheurService.getCurrentEtapeCircuit(model.getDossier()), signedBytes, "PKCS#7/single");
            } else if ((leSigType.equalsIgnoreCase("application/xml") || leSigType.equalsIgnoreCase("text/xml")) && model.getSigFormat().equalsIgnoreCase("xades/detached")) {

                byte[] signedBytes = request.getSignatureDocPrincipal().getValue();

                parapheurService.setSignature(model.getDossier(), signedBytes, "XAdES/detached");
                parapheurService.setSignature(model.getDossier(), parapheurService.getCurrentEtapeCircuit(model.getDossier()), signedBytes, "XAdES/detached");

            } else {
                if (log.isEnabledFor(Level.ERROR)) {
                    log.error(model.getLoggerPrefix() + "champ signature doc principal non supporte, donc ignore");
                }
            }

            signatureService.setDetachedSignature(model.getMainDoc().getId(), new ByteArrayInputStream(request.getSignatureDocPrincipal().getValue()), leSigType);
        }
    }

    private void setSignatureDetacheeForDocument(NodeRef document, TypeDoc signature, CreerDossieModel model) {
        // ------------------- Signature doc Principal (optionnel)
        if (signature != null) {
            String leSigType = signature.getContentType();
            byte[] signedBytes = signature.getValue();
            if (leSigType.equalsIgnoreCase("application/pkcs7-signature")
                    || leSigType.equalsIgnoreCase("application/x-pkcs7-signature")) {

                // BeginPatch - LIMOUSIN
                if (model.getSigFormat().startsWith("PKCS#7")) {
                    int stI = (signedBytes.length > 10 ? 10 : signedBytes.length);
                    String sigStart = new String(signedBytes, 0, stI);
                    if (!sigStart.equals("-----BEGIN")) {
                        /*
                         * On stocke la signature format PEM, pas DER
                         */
                        signedBytes = PKCS7VerUtil.der2pem(signedBytes);
                    }
                }
            }
            signatureService.setDetachedSignature(document.getId(), new ByteArrayInputStream(signedBytes), leSigType);
        }
    }

    private void addCustomProperties(CreerDossieModel model) throws DocumentException {
        List<String> mandatory = typesService.getMandatoryMetadatas(model.getType(), model.getSousType());
        Map<String, Map<String, String>> metadataMap = typesService.getMetadatasMap(model.getType(), model.getSousType());

        if(model.getScriptCustomProperties() == null) {
            model.setScriptCustomProperties(new ArrayList<>());
        }

        if (metadataMap != null && !metadataMap.isEmpty()) {
            for(String key: metadataMap.keySet()) {
                QName propertyQName = QName.createQName(key);

                if(!model.getCustomProperties().keySet().contains(propertyQName)) {
                    String defaultValue = metadataMap.get(key).get("default");

                    if (defaultValue != null && !defaultValue.isEmpty()) {
                        model.getCustomProperties().put(propertyQName, defaultValue);
                        model.getScriptCustomProperties().add(new CustomProperty<>(propertyQName.getLocalName(), defaultValue));
                    } else if(mandatory.contains(key)) {
                        String errorMessage = String.format("La métadonnée '%s' doit être renseignée.", propertyQName.getLocalName());
                        throw new DocumentException(errorMessage);
                    }
                }
            }
        }
    }

    private void buildXmlInModel(CreerDossierRequest request, CreerDossieModel model) throws ParsingException, IOException {
        byte[] docContent = request.getDocumentPrincipal().getValue();
        nu.xom.Builder builder = new nu.xom.Builder();
        if (model.getMainDocType().equals(DOCTYPE.XML)) {
            model.setDocXml(builder.build(new ByteArrayInputStream(docContent)));
        }
    }

    private void checkHeliosProperties(CreerDossierRequest request, CreerDossieModel model) throws DocumentException {
        if (model.getProtocole() != null) {
            if (model.getProtocole().equalsIgnoreCase("HELIOS")) {

                if ((request.getXPathPourSignatureXML() == null
                        || // BLEX
                        request.getXPathPourSignatureXML().trim().length() == 0)) {
                    throw new DocumentException("HELIOS: XPathPourSignatureXML absent.");
                } else {
                    nu.xom.Nodes nodeList = model.getDocXml().query(request.getXPathPourSignatureXML());

                    if (nodeList.size() == 0) {
                        throw new DocumentException("Requete incorrecte, l'élement à signer ne possède pas d'élément avec xPath " + request.getXPathPourSignatureXML());
                    }

                    for (int i = 0; i < nodeList.size(); i++) {
                        nu.xom.Node tmpN = nodeList.get(i);
                        nu.xom.Element n;
                        if (tmpN instanceof nu.xom.Document) {
                            n = tmpN.getDocument().getRootElement();
                        } else {
                            n = (nu.xom.Element) tmpN;
                        }

                        if (n != null) {
                            String id = n.getAttributeValue("Id");
                            if (id != null) {
                                if ("".equals(id) || id.contains(" ")) {
                                    throw new DocumentException("Requete incorrecte, l'id de l'élement " + n.getLocalName() + " à signer est invalide");
                                }
                            } else {
                                throw new DocumentException("Requete incorrecte, l'élement " + n.getLocalName() + " à signer ne possède pas d'attribut Id");
                            }
                        }
                    }
                }
            }
        }
    }

    private void checkDocumentWithProtocol(CreerDossierRequest request, CreerDossieModel model) throws DocumentException {
        // Verif si format PAdES: PDF-only for documents
        if (model.getSigFormat() != null && model.getSigFormat().equalsIgnoreCase(ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_PADES_ISO32000_1)) {
            // tous les docs principaux doivent être PDF
            if (!model.getMainDocType().equals(DOCTYPE.PDF)) {
                String msgNotValidDocument = "Requete invalide, la typologie choisie est supposee ne recevoir que des documents signables format PDF.";
                throw new DocumentException(msgNotValidDocument);
            }
            // verif autres docs supp
            if (typesService.isMultiDocument(model.getType(), model.getSousType()) &&
                    request.getDocumentsSupplementaires() != null &&
                    !request.getDocumentsSupplementaires().getDocAnnexe().isEmpty()) {
                List<DocAnnexe> docSuppLst = request.getDocumentsSupplementaires().getDocAnnexe();
                for (DocAnnexe leDoc : docSuppLst) {
                    if (leDoc != null && leDoc.getEncoding() != null
                            && leDoc.getMimetype() != null && leDoc.getNom() != null
                            && leDoc.getFichier() != null && leDoc.getFichier().getValue() != null) {

                        String ppMimeType = leDoc.getMimetype().trim();
                        boolean isOk = false;

                        /*
                         * PDF file may be in "application/octet-stream" mime type...
                         * check it out based on file name extension!
                         */
                        if (ppMimeType.equalsIgnoreCase(MimetypeMap.MIMETYPE_BINARY)) {
                            if(!leDoc.getNom().toLowerCase().trim().endsWith(".pdf")) {
                                String msgNotValidDocument = "Requete invalide, L'extension du document supplémentaire " + leDoc.getNom() + " est manquante ou invalide (pdf attendu).";
                                throw new DocumentException(msgNotValidDocument);
                            } else {
                                isOk = true;
                            }
                        }

                        if(ppMimeType.equalsIgnoreCase(MimetypeMap.MIMETYPE_PDF)) {
                            isOk = true;
                            if(!leDoc.getNom().toLowerCase().trim().endsWith(".pdf")) {
                                leDoc.setNom(leDoc.getNom() + ".pdf");
                            }
                        }

                        if (!isOk) {
                            String msgNotValidDocument = "Requete invalide, la typologie choisie est supposee ne recevoir que des documents signables format PDF.";
                            throw new DocumentException(msgNotValidDocument);
                        }
                    }

                }
            }
            // verif annexes: pas de XML
            if (request.getDocumentsAnnexes() != null && !request.getDocumentsAnnexes().getDocAnnexe().isEmpty()) {
                List<DocAnnexe> annexes = request.getDocumentsAnnexes().getDocAnnexe();
                for (DocAnnexe leDoc : annexes) {
                    if (leDoc != null && leDoc.getEncoding() != null
                            && leDoc.getMimetype() != null && leDoc.getNom() != null
                            && leDoc.getFichier() != null && leDoc.getFichier().getValue() != null) {
                        if (wsService.checkAcceptableMimeTypeXML(leDoc.getMimetype().trim())) {
                            String msgShallNotHaveXmlAnnexes = "Requete invalide, la typologie choisie n'est pas supposee recevoir de document annexe XML.";
                            throw new DocumentException(msgShallNotHaveXmlAnnexes);
                        }
                    }
                }

            }
        }
    }

    private void checkProtocolLimit(CreerDossierRequest request, CreerDossieModel model) throws DocumentException {
        /*
         * Document formats paranoia: extra sanity checks.
         * Parce que les systèmes tiers tentent vraiment n'importe quoi...
         *  Si protocole Helios: 1 doc principal XML, et  ZERO annexe
         *  Si protocole Actes : 1 doc principal non XML (sera de toute façon transformé PDF), annexes PDF/PNG/JPG
         *  Si signature PAdES : n docs principaux PDF, annexes non XML
         */
        if (model.getProtocole() != null) {
            if (model.getProtocole().equalsIgnoreCase(ParapheurModel.PROP_TDT_PROTOCOLE_VAL_HELIOS)) {
                if (!model.getMainDocType().equals(DOCTYPE.XML)) {
                    String msgNotXmlDocument = "Requete invalide, typologie Helios mais document fourni non XML.";
                    throw new DocumentException(msgNotXmlDocument);
                }
                // verif autres docs supp: pas besoin de contrôler, ce sera ignoré.
                // verif annexes
                if (request.getDocumentsAnnexes() != null && !request.getDocumentsAnnexes().getDocAnnexe().isEmpty()) {
                    String msgShallNotHaveAnnexes = "Requete invalide, la typologie Helios n'est pas supposee recevoir de document annexe";
                    throw new DocumentException(msgShallNotHaveAnnexes);
                }
            } else if (model.getProtocole().equalsIgnoreCase(ParapheurModel.PROP_TDT_PROTOCOLE_VAL_ACTES)) {
                //if (!finalIsDocPrincipalBureautique) {
                if (!model.getMainDocType().equals(DOCTYPE.BUREAUTIQUE) && !model.getMainDocType().equals(DOCTYPE.PDF)) {
                    String msgNotValidDocument = "Requete invalide, la typologie choisie est supposee recevoir un document 'bureautique' valide.";
                    throw new DocumentException(msgNotValidDocument);
                }
                // verif autres docs supp
                if (typesService.isMultiDocument(model.getType(), model.getSousType()) &&
                        request.getDocumentsSupplementaires() != null &&
                        !request.getDocumentsSupplementaires().getDocAnnexe().isEmpty()) {
                    List<DocAnnexe> docSuppLst = request.getDocumentsSupplementaires().getDocAnnexe();
                    for (DocAnnexe leDoc : docSuppLst) {
                        if (leDoc != null && leDoc.getEncoding() != null
                                && leDoc.getMimetype() != null && leDoc.getNom() != null
                                && leDoc.getFichier() != null && leDoc.getFichier().getValue() != null) {

                            String ppMimeType = leDoc.getMimetype().trim();
                            boolean isOk = wsService.checkAcceptableMimeTypeBureautique(ppMimeType);

                            /*
                             * PDF file may be in "application/octet-stream" mime type...
                             * check it out based on file name extension!
                             */
                            if (!isOk && ppMimeType.equalsIgnoreCase(MimetypeMap.MIMETYPE_BINARY)
                                    && leDoc.getNom().toLowerCase().trim().endsWith(".pdf")) {
                                isOk = true;
                            }

                            if (!isOk) {
                                String msgNotValidDocument = "Requete invalide, la typologie choisie est supposee recevoir un document 'bureautique' valide.";
                                throw new DocumentException(msgNotValidDocument);
                            }
                        }

                    }
                }
                // verif annexes: PDF, JPG, ou PNG
                if (request.getDocumentsAnnexes() != null &&
                        !request.getDocumentsAnnexes().getDocAnnexe().isEmpty()) {
                    List<DocAnnexe> annexes = request.getDocumentsAnnexes().getDocAnnexe();
                    for (DocAnnexe leDoc : annexes) {
                        if (leDoc != null && leDoc.getEncoding() != null
                                && leDoc.getMimetype() != null && leDoc.getNom() != null
                                && leDoc.getFichier() != null && leDoc.getFichier().getValue() != null) {
                            String ppMimeType = leDoc.getMimetype().trim();
                            boolean isOk = false;
                            if (ppMimeType.equalsIgnoreCase(MimetypeMap.MIMETYPE_IMAGE_JPEG)
                                    || ppMimeType.equalsIgnoreCase(MimetypeMap.MIMETYPE_IMAGE_PNG)
                                    || ppMimeType.equalsIgnoreCase(MimetypeMap.MIMETYPE_PDF)) {
                                isOk = true;
                            }
                            /*
                             * PDF file may be in "application/octet-stream" mime type...
                             * check it out based on file name extension!
                             */
                            if (!isOk && ppMimeType.equalsIgnoreCase(MimetypeMap.MIMETYPE_BINARY)
                                    && leDoc.getNom().toLowerCase().trim().endsWith(".pdf")) {
                                isOk = true;
                            }

                            if (!isOk) {
                                String msgNotValidDocument = "Requete invalide, la typologie choisie est supposee recevoir un document annexe 'bureautique' valide (JPG PNG PDF).";
                                throw new DocumentException(msgNotValidDocument);
                            }
                        }
                    }
                }
            }
        }
    }

    private void setSigFormatAndProtocol(CreerDossierRequest request, CreerDossieModel model) {
        // ------------------- Typage Metier: le type technique renseigne certains champs
        Map<QName, Serializable> typageProps = parapheurService.getTypeMetierProperties(model.getType());

        Serializable sigFormat = typageProps.get(ParapheurModel.PROP_SIGNATURE_FORMAT);
        model.setProtocole((String) typageProps.get(ParapheurModel.PROP_TDT_PROTOCOLE));
        //model.setSigFormat((String) typageProps.get(ParapheurModel.PROP_SIGNATURE_FORMAT));

        if (sigFormat != null && ((String) sigFormat).equalsIgnoreCase("auto")) {
            boolean foundFormat = false;
            if (request.getSignatureDocPrincipal() != null) {
                String leSigType = request.getSignatureDocPrincipal().getContentType();
                if (leSigType.equalsIgnoreCase("application/pkcs7-signature")
                        || leSigType.equalsIgnoreCase("application/x-pkcs7-signature")) {
                    sigFormat = ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7; // PKCS7
                    foundFormat = true;
                } else if (leSigType.equalsIgnoreCase("application/xml") || leSigType.equalsIgnoreCase("text/xml")) {
                    foundFormat = true;
                    sigFormat = ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_1_1; // Xades Detaché
                }
            }
            if(!foundFormat
                    && typesService.isMultiDocument(model.getType(), model.getSousType())
                    && request.getDocumentsSupplementaires() != null
                    && request.getDocumentsSupplementaires().getDocAnnexe() != null) {
                // Here, we have to check
                for(DocAnnexe docSupp : request.getDocumentsSupplementaires().getDocAnnexe()) {
                    if(docSupp.getSignature() == null ) continue;
                    String contentType = docSupp.getSignature().getContentType();

                    if (contentType.equalsIgnoreCase("application/pkcs7-signature")
                            || contentType.equalsIgnoreCase("application/x-pkcs7-signature")) {
                        sigFormat = ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7; // PKCS7
                        foundFormat = true;
                        break;
                    } else if (contentType.equalsIgnoreCase("application/xml") || contentType.equalsIgnoreCase("text/xml")) {
                        sigFormat = ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_1_1; // Xades Detaché
                        foundFormat = true;
                        break;
                    }
                }
            }
            if (!foundFormat) {
                boolean allPdf = true;
                if (model.getMainDocType().equals(DOCTYPE.PDF)) {
                    if(typesService.isMultiDocument(model.getType(), model.getSousType()) && request.getDocumentsSupplementaires() != null) {
                        for(DocAnnexe docSupp : request.getDocumentsSupplementaires().getDocAnnexe()) {
                            allPdf = allPdf && getDocType(docSupp.getFichier(), docSupp.getNom()).equals(DOCTYPE.PDF);
                        }
                    }
                } else {
                    allPdf = false;
                }
                if (allPdf) {
                    switch(sigformatifunsignedpdf) {
                        case "pades":
                            sigFormat = ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_PADES_ISO32000_1; // PADES
                            break;
                        case "xades":
                            sigFormat = ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_1_1; // Xades Detaché
                            break;
                        default:
                            sigFormat = ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7;
                    }

                } else {
                    if ("xades".equals(sigformatifunsigned)) {
                        sigFormat = ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_1_1; // Xades Detaché
                    } else {
                        sigFormat = ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7;
                    }
                }
            }
        }
        typageProps.put(ParapheurModel.PROP_SIGNATURE_FORMAT, sigFormat);
        model.setTypageProps(typageProps);
        model.setSigFormat((String) sigFormat);
    }

    private NodeRef circuitObligatoire2circuit(String nom, TypeCircuit typeCircuit) {
        List<org.adullact.spring_ws.iparapheur._1.EtapeCircuit> listeEtapes = typeCircuit.getEtapeCircuit();
        List<com.atolcd.parapheur.repo.EtapeCircuit> res = new ArrayList<>();
        String actionDemandee;

        for (org.adullact.spring_ws.iparapheur._1.EtapeCircuit etapeCircuit : listeEtapes) {
            // Remplir les étapes
            if (etapeCircuit.getParapheur() == null || etapeCircuit.getParapheur().trim().isEmpty()
                    || etapeCircuit.getRole() == null || etapeCircuit.getRole().trim().isEmpty()) {
                if (log.isEnabledFor(Level.ERROR)) {
                    log.error("Etape de circuit dynamique invalide");
                }
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug(nom + typeCircuit + ": etape de " + etapeCircuit.getRole() + " pour [" + etapeCircuit.getParapheur() + "]");
            }
            List<NodeRef> parapheurs = parapheurService.getParapheursFromName(etapeCircuit.getParapheur());

            /*
             * Role: VISEUR SIGNATAIRE ARCHIVAGE TDT, "ENVOI PAR MAIL SECURISE"
             */
            if ("VISEUR".equals(etapeCircuit.getRole())) {
                actionDemandee = com.atolcd.parapheur.repo.EtapeCircuit.ETAPE_VISA;
            } else if ("SIGNATAIRE".equals(etapeCircuit.getRole())) {
                actionDemandee = com.atolcd.parapheur.repo.EtapeCircuit.ETAPE_SIGNATURE;
            } else if ("ARCHIVAGE".equals(etapeCircuit.getRole())) {
                actionDemandee = com.atolcd.parapheur.repo.EtapeCircuit.ETAPE_ARCHIVAGE;
            } else if ("TDT".equals(etapeCircuit.getRole())) {
                actionDemandee = com.atolcd.parapheur.repo.EtapeCircuit.ETAPE_TDT;
            } else if ("ENVOI PAR MAIL SECURISE".equals(etapeCircuit.getRole())) {
                actionDemandee = com.atolcd.parapheur.repo.EtapeCircuit.ETAPE_MAILSEC;
            } else {
                /* etape par defaut */
                log.warn("Should never happen: etape VISA pour '" + etapeCircuit.getRole() + "'");
                actionDemandee = com.atolcd.parapheur.repo.EtapeCircuit.ETAPE_VISA;
            }

            if(parapheurs != null && !parapheurs.isEmpty()) {
                ScriptEtapeCircuitImpl scriptEtapeCircuitImpl = new ScriptEtapeCircuitImpl("PARAPHEUR", parapheurs.get(0), actionDemandee, null);
                res.add(scriptEtapeCircuitImpl);
            } else {
                return null;
            }
        }
        return parapheurService.saveWorkflow(nom, res, null, null, true);
    }

    private void setWorkflow(CreerDossierRequest request, CreerDossieModel model) {
        // ------------------- Circuit
        List<com.atolcd.parapheur.repo.EtapeCircuit> etapesList = null;
        if (request.getCircuitObligatoire() != null) {
            TypeCircuit typeCircuit = request.getCircuitObligatoire();
            if (typeCircuit.getEtapeCircuit() != null) {
                // 1- Enregistrer un circuit public temporaire
                NodeRef tempCircuitRef = circuitObligatoire2circuit(model.getUsername() + model.getId(), typeCircuit);
                if (tempCircuitRef != null) {
                    // 2- Charger ce circuit
                    SavedWorkflow circuit = workflowService.getSavedWorkflow(tempCircuitRef);
                    etapesList = circuit.getCircuit();
                    nodeService.deleteNode(tempCircuitRef);
                }
            }
        }
        if (etapesList == null) {
            SavedWorkflow circuit = workflowService.getSavedWorkflow(model.getWorkflow(), true, model.getParapheur(), model.getDossier());
            etapesList = circuit.getCircuit();
        }
        parapheurService.setCircuit(model.getDossier(), etapesList);
    }

    private void prepareMultidoc(CreerDossierRequest request, CreerDossieModel model) {
        if (typesService.isMultiDocument(model.getType(), model.getSousType())) {
            // check on 'docsupp' stuff
            if (request.getDocumentsSupplementaires() != null) {
                model.setDocSuppLst(request.getDocumentsSupplementaires());

                // do it with sequence number if available
                // so let's detect it
                boolean hasSequence = true;
                for (DocAnnexe leDoc : model.getDocSuppLst().getDocAnnexe()) {
                    if (leDoc == null || leDoc.getSequence() == null) {
                        hasSequence = false;
                        break;
                    }
                }
                if (hasSequence) {
                    //noinspection unchecked
                    Collections.sort(model.getDocSuppLst().getDocAnnexe());
                }
            }
        } else {
            if (request.getDocumentsSupplementaires() != null) {
                // TANT PIS!!
                if (log.isEnabledFor(Level.WARN)) {
                    log.warn(model.getLoggerPrefix() + "typologie mono-document principal: les documents supplementaires presents sont IGNORES.");
                }
            }
        }
    }

    private void checkDocSupp(CreerDossierRequest request) throws DocumentException {
        if (request.getDocumentsSupplementaires() != null) {
            TypeDocAnnexes docsSupp = request.getDocumentsSupplementaires();

            // We have to check max main documents property too...
            int maxMainDocuments = dossierService.getMaxMainDocuments();
            if (docsSupp.getDocAnnexe().size() > maxMainDocuments) {
                String msgTooManyDocs = "Trop de documents principaux, requete rejetee. Limite fixée à " + maxMainDocuments;
                throw new DocumentException(msgTooManyDocs);
            }

            // Check every document for protected PDF
            this.checkListTypeAnnexe(docsSupp, true);
        }
    }

    private void checkListTypeAnnexe(TypeDocAnnexes docsSupp, boolean throwExceptionIfInvalid) throws DocumentException {
        try {
            for (DocAnnexe leDoc : docsSupp.getDocAnnexe()) {
                if (leDoc != null && leDoc.getEncoding() != null && !leDoc.getEncoding().trim().isEmpty()
                        && leDoc.getMimetype() != null && !leDoc.getMimetype().trim().isEmpty()
                        && (wsService.checkAcceptableMimeTypeBureautique(leDoc.getMimetype().trim())
                            || wsService.checkAcceptableMimeTypeXML(leDoc.getMimetype().trim())
                            || wsService.checkAcceptableMimeTypeCompressed(leDoc.getMimetype().trim()))
                        && leDoc.getNom() != null && !leDoc.getNom().trim().isEmpty()
                        && leDoc.getFichier() != null && leDoc.getFichier().getValue() != null) {

                    try {
                        this.checkProtectedPdf(leDoc.getFichier(), leDoc.getNom());
                    } catch(IOException e) {
                        throw new Exception("Requete incorrecte, le document annexe "+ leDoc.getNom() +" n'est pas un PDF valide");
                    }

                } else {
                    if (throwExceptionIfInvalid) {
                        throw new DocumentException("Requete incorrecte, document suppplementaire invalide.");
                    } else {
                        log.warn("Document annexe invalid -> ignoré");
                    }
                }
            }
        } catch (Exception e) {
            throw new DocumentException(e.getMessage());
        }
    }

    private void setVisuel(CreerDossierRequest request, CreerDossieModel model) {
        // ------------------- Visuel PDF en Propriété du document principal
        // contrôle sur le type MIME en amont, mais Pas de contrôle sur le format PDF
        if (!request.getDocumentPrincipal().getContentType().equalsIgnoreCase(MimetypeMap.MIMETYPE_PDF)
                && request.getVisuelPDF() != null) {
            byte[] pdfContent = request.getVisuelPDF().getValue();

            ContentWriter cw2 = contentService.getWriter(model.getMainDoc(), ParapheurModel.PROP_VISUEL_PDF, Boolean.TRUE);
            cw2.setMimetype(MimetypeMap.MIMETYPE_PDF);
            cw2.setEncoding("UTF-8");

            // BLEX : Information pour statistiques d'usage
            log.info("Creation dossier : origine=WS; idDossier=" + model.getId() + "; document=PDF; size=" + pdfContent.length + ";");

            InputStream cis2 = new ByteArrayInputStream(pdfContent);
            cw2.putContent(cis2);
        } else if(!request.getDocumentPrincipal().getContentType().equalsIgnoreCase(MimetypeMap.MIMETYPE_PDF)) {
            ContentWriter pdfwriter = contentService.getWriter(model.getMainDoc(), ParapheurModel.PROP_VISUEL_PDF, Boolean.TRUE);
            pdfwriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
            ContentReader reader = contentService.getReader(model.getMainDoc(), ContentModel.PROP_CONTENT);
            contentService.transform(reader, pdfwriter);

            // BLEX : Information pour statistiques d'usage
            log.info("Creation dossier : origine=WS; idDossier=" + model.getId() + "; document=PDF; size=" + pdfwriter.getSize() + ";");
        }
    }

    private void checkVisuelPdf(CreerDossierRequest request) throws DocumentException {
        if (!request.getDocumentPrincipal().getContentType().equalsIgnoreCase(MimetypeMap.MIMETYPE_PDF)
                && request.getVisuelPDF() != null) {
            byte[] pdfContent = request.getVisuelPDF().getValue();

            // si PDF protege, on s'arrete
            try {
                PopplerUtils utils = new PopplerUtils();
                File tmpFile = TempFileProvider.createTempFile("checkVisuelPdf", ".pdf");
                FileOutputStream out = new FileOutputStream(tmpFile);
                out.write(pdfContent);
                out.close();

                if (utils.isEncrypted(tmpFile.getAbsolutePath())) {
                    throw new DocumentException("Requete incorrecte, visuel PDF protégé.");
                }

                if(!tmpFile.delete()) {
                    log.warn("Cannot delete tmp file on method CreerDossierService::checkVisuelPdf");
                }
            } catch (IOException ioe) {
                log.warn("IO Exception sur VisuelPDF: " + ioe.getLocalizedMessage());
                // mais on laisse continuer (cas du PDF à 1 octet de GFI, on laisse passer par compatibilité ascendante)
            }
        }
    }

    private void checkProtectedPdf(TypeDoc document, String docname) throws DocumentException, IOException {
        byte[] docContent = document.getValue();

        if (document.getContentType().equals(MimetypeMap.MIMETYPE_PDF)) {
            PopplerUtils utils = new PopplerUtils();
            File tmpFile = TempFileProvider.createTempFile("checkProtectedPdf", ".pdf");
            FileOutputStream out = new FileOutputStream(tmpFile);
            out.write(docContent);
            out.close();

            if(!utils.isValid(tmpFile.getAbsolutePath())) {
                throw new IOException("Document " + docname + " is not a valid PDF");
            }
            if (utils.isEncrypted(tmpFile.getAbsolutePath())) {
                if (dossierService.hasToAcceptLockedPdf()) {
                    File outTmpFile = TempFileProvider.createTempFile("checkProtectedPdf", ".pdf");
                    boolean result = utils.unlock(tmpFile.getAbsolutePath(), outTmpFile.getAbsolutePath());
                    if(!result) {
                        throw new DocumentException("Requete incorrecte, document " + docname + " = PDF protégé impossible à débloquer.");
                    }

                    document.setValue(IOUtils.toByteArray(new FileInputStream(outTmpFile)));

                    if(!outTmpFile.delete()) {
                        log.warn("Cannot delete tmp file on method CreerDossierService::checkProtectedPdf");
                    }
                } else {
                    throw new DocumentException("Requete incorrecte, document " + docname + " = PDF protégé.");
                }
            }
            if(!tmpFile.delete()) {
                log.warn("Cannot delete tmp file on method CreerDossierService::checkProtectedPdf");
            }
        }
    }

    private void setContent(CreerDossierRequest request, CreerDossieModel model) throws IOException {
        /*
         * Pour éviter les doublons de nommage, on va se souvenir
         * des noms de documents déjà fournis et renommer les
         * nouveaux.
         */
        String nomDocPrincipal;
        String lContentType = request.getDocumentPrincipal().getContentType().trim().toLowerCase();

        if (request.getNomDocPrincipal() != null && !request.getNomDocPrincipal().trim().isEmpty()) {
            nomDocPrincipal = formatDocName(request.getNomDocPrincipal().trim(), lContentType, model.getMainDocType().equals(DOCTYPE.XML));
        } else {
            nomDocPrincipal = formatDocName(model.getId(), lContentType, model.getMainDocType().equals(DOCTYPE.XML));
        }
        nomDocPrincipal = StringEscapeUtils.unescapeHtml(nomDocPrincipal).replaceAll("[\\p{M}]", "");
        if (log.isDebugEnabled()) {
            log.debug(model.getLoggerPrefix() + "nom DocPrincipal retenu = " + nomDocPrincipal);
        }

        NodeRef found = fileFolderService.searchSimple(model.getDossier(), nomDocPrincipal);
        if (found != null) fileFolderService.delete(found);

        FileInfo fileInfo = fileFolderService.create(model.getDossier(), nomDocPrincipal, ContentModel.TYPE_CONTENT);
        model.setMainDoc(fileInfo.getNodeRef());
        
        // Mémorisation document principal
        Map<String, Serializable> docPrincipal = new HashMap<String, Serializable>();
        docPrincipal.put("id", fileInfo.getNodeRef().getId());
        docPrincipal.put("isMainDocument", Boolean.TRUE);
        if(model.getOrdreDocuments()==null) {
        	model.setOrdreDocuments(new ArrayList<>());
        }
        model.getOrdreDocuments().add(docPrincipal);
                
        nodeService.addAspect(model.getMainDoc(), ParapheurModel.ASPECT_MAIN_DOCUMENT, null);
        ContentWriter cw = contentService.getWriter(model.getMainDoc(), ContentModel.PROP_CONTENT, Boolean.TRUE);
        // Fix bad RTF Mimetypes
        cw.setMimetype(lContentType.replace("text/rtf", "application/rtf").replace("application/x-rtf", "application/rtf"));
        model.getDocNames().put(nomDocPrincipal, nomDocPrincipal);

        // Default charset for files
        String encoding = "UTF-8";

        byte[] docContent = request.getDocumentPrincipal().getValue();
        if (model.getMainDocType().equals(DOCTYPE.XML)) {
            InputStream is = new ByteArrayInputStream(docContent);
            // Read first bytes
            byte[] firstBytes = new byte[100];
            //noinspection ResultOfMethodCallIgnored
            is.read(firstBytes, 0, 100);
            is.close();
            String content = new String(firstBytes);
            Pattern encodingPattern = Pattern.compile("^<\\?([^>]*)encoding=(?:[\"'])([a-zA-Z0-9_-]+)(?:[\"'])([^>]*)\\?>");
            Matcher matcher = encodingPattern.matcher(content);
            if (matcher.find()) {
                encoding = matcher.group(2);
            }
        }

        // BLEX : Information pour statistiques d'usage
        log.info("Creation dossier : origine=WS; idDossier=" + model.getId() + "; document=Principal; size=" + docContent.length + ";");

        if (log.isDebugEnabled()) {
            log.debug(model.getLoggerPrefix() + "doc Type=" + lContentType + ", enc=" + encoding + ", size=" + docContent.length);
        }
        cw.setEncoding(encoding);

        InputStream cis = new ByteArrayInputStream(docContent);
        cw.putContent(cis);

        if(model.getMainDocType().equals(DOCTYPE.PDF)) {
            FileContentReader reader = (FileContentReader) contentService.getReader(model.getMainDoc(), ContentModel.PROP_CONTENT);
            PopplerUtils utils = new PopplerUtils();
            if(utils.isSigned(reader.getFile().getAbsolutePath())) {
                nodeService.addAspect(model.getMainDoc(), ParapheurModel.ASPECT_SIGNED, null);
            }
        }
    }

    /**
     * Yet no support for the Microsoft OpenXML crap.
     */
    private String formatDocName(String docNameString, String mimeTypeString, boolean isXml) {
        String src = docNameString.trim().toLowerCase();
        String res;
        if (mimeTypeString == null || mimeTypeString.trim().isEmpty()) {
            if (log.isEnabledFor(Level.WARN)) {
                log.warn("formatDocName: no mimetype?? ignoring...");
            }
            return docNameString.trim();
        } else {
            /*
             * Cas specifique XML
             */
            if (isXml && !src.endsWith(".xml")) {
                res = docNameString + ".xml";
            } else {
                /*
                 * Autres cas: bureautiques...
                 */
                if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_PDF)
                        && !src.endsWith(".pdf")) {
                    res = docNameString.trim().concat(".pdf");
                } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_TEXT_PLAIN)
                        && !src.endsWith(".txt")) {
                    res = docNameString.trim().concat(".txt");
                } else if ((mimeTypeString.equalsIgnoreCase("text/rtf") || mimeTypeString.equalsIgnoreCase("application/x-rtf") || mimeTypeString.equalsIgnoreCase("application/rtf"))
                        && !src.endsWith(".rtf")) {
                    res = docNameString.trim().concat(".rtf");
                } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_OPENDOCUMENT_TEXT)
                        && !src.endsWith(".odt")) {
                    res = docNameString.trim().concat(".odt");
                } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_OPENDOCUMENT_SPREADSHEET)
                        && !src.endsWith(".ods")) {
                    res = docNameString.trim().concat(".ods");
                } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_OPENDOCUMENT_PRESENTATION)
                        && !src.endsWith(".odp")) {
                    res = docNameString.trim().concat(".odp");
                } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_WORD)
                        && !src.endsWith(".doc")) {
                    res = docNameString.trim().concat(".doc");
                } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_EXCEL)
                        && !src.endsWith(".xls")) {
                    res = docNameString.trim().concat(".xls");
                } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_PPT)
                        && !src.endsWith(".ppt")) {
                    res = docNameString.trim().concat(".ppt");
                } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_IMAGE_JPEG)
                        && !src.endsWith(".jpg")) {
                    res = docNameString.trim().concat(".jpg");
                } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_IMAGE_PNG)
                        && !src.endsWith(".png")) {
                    res = docNameString.trim().concat(".png");
                } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_IMAGE_GIF)
                        && !src.endsWith(".gif")) {
                    res = docNameString.trim().concat(".gif");
                } else {
                    if (log.isEnabledFor(Level.WARN)) {
                        log.warn("formatDocName: failed to set up extension name, ignoring...");
                    }
                    res = docNameString.trim();
                }
            }
        }
        return res;
    }

    private List<CustomProperty<?>> buildCustomProperties(TypeMetaDonnees metaDonnees, Map<QName, Serializable> propsMap) {
        if (metaDonnees.getMetaDonnee().isEmpty() || propsMap == null) {
            if (log.isEnabledFor(Level.WARN)) {
                log.warn("pas de metadonnee.");
            }
            return null;
        }

        ArrayList<CustomProperty<?>> theCustomPropertys = new ArrayList<>();

        for (MetaDonnee md : metaDonnees.getMetaDonnee()) {
            // on récupère toute meta-donnée disponible pour enrichir le dossier.
            if (md != null && md.getNom() != null && !md.getNom().trim().isEmpty()
                    && md.getValeur() != null) { // on autorise la valeur de chaine vide
                String nom = md.getNom().trim();
                String valeur = md.getValeur();
                if (log.isDebugEnabled()) {
                    log.debug("\t Metadonnée " + nom + "=[" + valeur + "]");
                }
                // Vérifie l'existence de la définition de la meta-donnee
                List<CustomMetadataDef> cmdliste = metadataService.getMetadataDefs();
                if (!cmdliste.isEmpty()) {
                    boolean hasFound = false;
                    for (CustomMetadataDef customMetadataDef : cmdliste) {
                        if (customMetadataDef.getName().getLocalName().equals(nom)) {
                            switch (customMetadataDef.getType()) {
                                case DATE:
                                    try {
                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                        Date vDate = dateFormat.parse(valeur);
                                        theCustomPropertys.add(new CustomProperty<>(nom, vDate));
                                        propsMap.put(customMetadataDef.getName(), vDate);
                                    } catch (ParseException pe) {
                                        if (log.isEnabledFor(Level.ERROR)) {
                                            log.error("meta-donnee " + nom + "=[" + valeur + "] : DATE yyyy-mm-dd attendu, meta-donnee ignoree.");
                                        }
                                        if (log.isDebugEnabled()) {
                                            log.debug("", pe);
                                        }
                                    }
                                    break;
                                case DOUBLE:
                                    try {
                                        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(Locale.FRENCH);
                                        Double vDouble;// = Double.valueOf(valeur);
                                        if (',' == formatter.getDecimalFormatSymbols().getDecimalSeparator()) {
                                            if (valeur.contains(".")) {
                                                valeur = valeur.replace('.', ',');
                                            }
                                        } else if ('.' == formatter.getDecimalFormatSymbols().getDecimalSeparator()) {
                                            if (valeur.contains(",")) {
                                                valeur = valeur.replace(',', '.');
                                            }
                                        }

                                        vDouble = formatter.parse(valeur.trim()).doubleValue();
                                        theCustomPropertys.add(new CustomProperty<>(nom, vDouble));
                                        propsMap.put(customMetadataDef.getName(), vDouble);
                                    } catch (NumberFormatException nfe) {
                                        if (log.isEnabledFor(Level.ERROR)) {
                                            log.error("meta-donnee " + nom + "=[" + valeur + "] : DOUBLE attendu, meta-donnee ignoree.");
                                        }
                                        if (log.isDebugEnabled()) {
                                            log.debug("", nfe);
                                        }
                                    } catch (ParseException pe) {
                                        if (log.isEnabledFor(Level.ERROR)) {
                                            log.error("meta-donnee " + nom + "=[" + valeur + "] : DOUBLE attendu, meta-donnee ignoree.");
                                        }
                                        if (log.isDebugEnabled()) {
                                            log.debug(pe.getLocalizedMessage());
                                        }
                                    }
                                    break;
                                case INTEGER:
                                    try {
                                        Integer vInteger = Integer.valueOf(valeur);
                                        theCustomPropertys.add(new CustomProperty<>(nom, vInteger));
                                        propsMap.put(customMetadataDef.getName(), vInteger);
                                    } catch (NumberFormatException nfe) {
                                        if (log.isEnabledFor(Level.ERROR)) {
                                            log.error("meta-donnee " + nom + "=[" + valeur + "] : INT attendu, meta-donnee ignoree.");
                                        }
                                        if (log.isDebugEnabled()) {
                                            log.debug("", nfe);
                                        }
                                    }
                                    break;
                                case BOOLEAN:
                                    try {
                                        Boolean vBoolean;
                                        if (valeur != null && valeur.trim().equalsIgnoreCase("vrai")) {
                                            vBoolean = true;
                                        } else if (valeur != null && valeur.trim().equalsIgnoreCase("faux")) {
                                            vBoolean = false;
                                        } else { // legacy parsing of true/false
                                            vBoolean = Boolean.valueOf(valeur);
                                        }
                                        theCustomPropertys.add(new CustomProperty<>(nom, vBoolean));
                                        propsMap.put(customMetadataDef.getName(), vBoolean);
                                    } catch (NumberFormatException nfe) {
                                        if (log.isEnabledFor(Level.ERROR)) {
                                            log.error("meta-donnee " + nom + "=[" + valeur + "] : BOOLEAN attendu, meta-donnee ignoree.");
                                        }
                                        if (log.isDebugEnabled()) {
                                            log.debug("", nfe);
                                        }
                                    }
                                    break;
                                case URL:
                                    try {
                                        if ((!StringUtils.isEmpty(valeur)) && (!StringUtils.isUrlRFC3986Valid(valeur))) {
                                            throw new IllegalArgumentException("URL invalide : " + valeur);
                                        }
                                        theCustomPropertys.add(new CustomProperty<>(nom, valeur));
                                        propsMap.put(customMetadataDef.getName(), valeur);
                                    } catch (IllegalArgumentException ex) {
                                        if (log.isEnabledFor(Level.ERROR)) {
                                            log.error("meta-donnee " + nom + "=[" + valeur + "] : URL attendue, meta-donnee ignoree.");
                                        }
                                        if (log.isDebugEnabled()) {
                                            log.debug("", ex);
                                        }
                                    }
                                    break;
                                case STRING:
                                    theCustomPropertys.add(new CustomProperty<>(nom, valeur));
                                    propsMap.put(customMetadataDef.getName(), valeur);
                                    break;
                                default:
                                    if (log.isEnabledFor(Level.ERROR)) {
                                        log.error("meta-donnee " + nom + "=[" + valeur + "] : type inconnu ?!");
                                    }
                            }
                            hasFound = true;
                            break;
                        }
                    }
                    if (!hasFound) {
                        log.error("La meta-donnee " + nom + "=[" + valeur + "] est inconnue de i-parapheur, ignoree");
                    }
                } else {
                    if (log.isEnabledFor(Level.ERROR)) {
                        log.error("Pas de dictionnaire de méta-donnée défini dans l'entrepot.");
                    }
                }
            } else {
                if (log.isEnabledFor(Level.ERROR)) {
                    log.error("meta-donnee invalide, donc non prise en compte.");
                }
            }
        }
        return theCustomPropertys;
    }

    private void setWorkflowInModel(CreerDossierRequest request, CreerDossieModel model) throws DocumentException {
        HashMap<QName, Serializable> customProperties = new HashMap<>();
        NodeRef workflow;

        // ------------------- Meta Donnees
        List<CustomMetadataDef> mdliste = metadataService.getMetadataDefs();
        if (!mdliste.isEmpty()
                && request.getMetaData() != null
                && !request.getMetaData().getMetaDonnee().isEmpty()) {
            TypeMetaDonnees metaDonnees = request.getMetaData();
            /*
             * l'objet properties est passé par référence.
             * Du coup la méthode buildCustomProperties en profite
             * pour enrichir la map de 'properties' (pas joli-joli).
             */
            if (log.isDebugEnabled()) {
                log.debug(model.getLoggerPrefix() + " avant buildCustomProperties, size=" + customProperties.size());
            }
            model.setScriptCustomProperties(buildCustomProperties(metaDonnees, customProperties));
            if (log.isDebugEnabled()) {
                log.debug(model.getLoggerPrefix() + " apres buildCustomProperties, size=" + customProperties.size());
            }
            model.setCustomProperties(customProperties);
        }

        this.addCustomProperties(model);

        // ------------------- Check du circuit
        try {
            workflow = typesService.getWorkflowFromWs(null, request.getTypeTechnique(), request.getSousType(), model.getScriptCustomProperties());
        } catch (RuntimeException e) {
            if (log.isEnabledFor(Level.WARN)) {
                log.warn(model.getLoggerPrefix() + "typesService.getWorkflow loupe : " + e.getMessage() + "\n" + e.toString());
            }
            throw new DocumentException("circuit INCONNU pour ["
                    + request.getTypeTechnique() + "][" + request.getSousType()
                    + "] et les meta-donnees fournies");
        }
        if (workflow == null) {
            throw new DocumentException("circuit INCONNU pour ["
                    + request.getTypeTechnique() + "]["
                    + request.getSousType() + "] et les méta-donnees fournies");
        }
        model.setWorkflow(workflow);
    }

    private Map<QName, Serializable> defineProperties(CreerDossieModel model) throws ParseException {
        Map<QName, Serializable> properties = new HashMap<>();

        /*
         * Propriétés basiques du dossier
         */
        // nom Dossier
        if (model.getId() != null) {
            properties.put(ContentModel.PROP_NAME, model.getId());
        }
        if (model.getWsUsername() != null) {
            properties.put(ParapheurModel.PROP_WS_EMETTEUR, model.getWsUsername());
        }
        properties.put(ContentModel.PROP_TITLE, model.getTitle());
        properties.put(ParapheurModel.PROP_DIGITAL_SIGNATURE_MANDATORY,
                typesService.isDigitalSignatureMandatory(model.getType(), model.getSousType()));
        properties.put(ParapheurModel.PROP_READING_MANDATORY,
                typesService.isReadingMandatory(model.getType(), model.getSousType()));
        properties.put(ParapheurModel.PROP_INCLUDE_ATTACHMENTS,
                typesService.areAttachmentsIncluded(model.getType(), model.getSousType()));
        // date Limite
        if (model.getDateLimite() != null && !model.getDateLimite().trim().isEmpty()) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            properties.put(ParapheurModel.PROP_DATE_LIMITE, dateFormat.parse(model.getDateLimite().trim()));
        }
        // Visibilite
        if (model.getVisibilite() != null && !model.getVisibilite().toString().isEmpty()) {
            if (model.getVisibilite().toString().equalsIgnoreCase("CONFIDENTIEL")) {
                properties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.TRUE);
                properties.put(ParapheurModel.PROP_PUBLIC, Boolean.FALSE);
            } else if (model.getVisibilite().toString().equalsIgnoreCase("PUBLIC")) {
                properties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.FALSE);
                properties.put(ParapheurModel.PROP_PUBLIC, Boolean.TRUE);
            } else {
                properties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.FALSE);
                properties.put(ParapheurModel.PROP_PUBLIC, Boolean.FALSE);
            }
        }
        // Type et Sous-type
        properties.put(ParapheurModel.PROP_TYPE_METIER, model.getType());
        properties.put(ParapheurModel.PROP_SOUSTYPE_METIER, model.getSousType());

        return properties;
    }

    private String getWSUsername(CreerDossierRequest request) throws Exception {
        String l_username = null;
        // Email Emetteur: pas grave si champ vide ou null, mais KO si email renseigné est inconnu
        if (request.getEmailEmetteur() != null && !request.getEmailEmetteur().trim().isEmpty()) {
            String l_email = request.getEmailEmetteur().trim();
            try {
                NodeRef emetteurRef = parapheurService.findUserByEmail(l_email);
                if (emetteurRef != null) {
                    l_username = (String) nodeService.getProperty(emetteurRef, ContentModel.PROP_USERNAME);
                    if (log.isDebugEnabled()) {
                        log.debug(" emailEmetteur(" + l_email + ") trouve = " + l_username);
                    }
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("propriete emailEmetteur invalide, aucun Utilisateur correspondant.");
                    }
                    throw new Exception("Requete incorrecte, 'EmailEmetteur'='" + l_email + "' est inconnu de i-Parapheur");
                }
            } catch (NoSuchPersonException nspe) {
                if (log.isDebugEnabled()) {
                    log.debug("catch : propriete emailEmetteur invalide, aucun Utilisateur correspondant.");
                }
                throw new Exception("Requete incorrecte, 'EmailEmetteur'='" + l_email + "' est inconnu de i-Parapheur");
            }
        }
        return l_username;
    }

    private void checkRequestWithTypology(CreerDossierRequest request) throws Exception {
        RetryingTransactionHelper rtxType = transactionService.getRetryingTransactionHelper();
        // Peut gérer plus de 10 créations simultanées
        rtxType.setMaxRetries(10);
        rtxType.setReadOnly(true);
        Map<String, List<String>> typologie = rtxType.doInTransaction(() -> typesService.optimizedGetSavedType(false, null), true, false);

        if (!(typologie.containsKey(request.getTypeTechnique()) &&
                typologie.get(request.getTypeTechnique()).contains(request.getSousType()))) {
            throw new Exception(String.format("Le sous-type %s n'existe pas pour le type '%s'", request.getSousType(), request.getTypeTechnique()));
        }
    }

    private void checkRequestWithDocument(CreerDossierRequest request, DOCTYPE doc_type) throws Exception {
        // PDF = BUREAUTIQUE + PDF
        if (doc_type.equals(DOCTYPE.OTHER)) {
            throw new Exception("Requete incomplete, le type MIME '" + request.getDocumentPrincipal().getContentType() + "' de 'DocumentPrincipal' n'est pas accepte.");
        } else if (doc_type.equals(DOCTYPE.XML)
                && (request.getVisuelPDF() == null || request.getVisuelPDF().getValue() == null
                || request.getVisuelPDF().getValue().length == 0)) {
            throw new Exception("Requete incomplete, champ obligatoire 'VisuelPDF' manquant ou vide.");
        } else if (doc_type.equals(DOCTYPE.XML)
                && (request.getVisuelPDF().getContentType() == null
                || !request.getVisuelPDF().getContentType().equalsIgnoreCase(MimetypeMap.MIMETYPE_PDF))) {
            throw new Exception("Requete incorrecte, VisuelPDF doit avoir le type MIME:" + MimetypeMap.MIMETYPE_PDF);
        }
    }

    private DOCTYPE getDocType(TypeDoc documentPrincipal, String docname) {
        String contentType = documentPrincipal.getContentType().toLowerCase().trim();

        DOCTYPE docType =
                // PDF ?
                contentType.equals(MimetypeMap.MIMETYPE_PDF) ? DOCTYPE.PDF :
                        // XML ?
                        wsService.checkAcceptableMimeTypeXML(contentType) ? DOCTYPE.XML :
                                // BUREAUTIQUE ?
                                wsService.checkAcceptableMimeTypeBureautique(contentType) ? DOCTYPE.BUREAUTIQUE :
                                        // Other...
                                        DOCTYPE.OTHER;
        /*
         * PDF file may be in "application/octet-stream" mime type...
         * check it out based on NomDocPrincipal file extension!
         */
        if (!docType.equals(DOCTYPE.BUREAUTIQUE) && !docType.equals(DOCTYPE.XML)
                && contentType.equalsIgnoreCase(MimetypeMap.MIMETYPE_BINARY)
                && docname != null
                && !docname.trim().isEmpty()
                && docname.toLowerCase().trim().endsWith(".pdf")) {
            docType = DOCTYPE.PDF;
        }

        return docType;
    }

    private void checkBasicRequest(CreerDossierRequest request) throws Exception {
        if (request == null) {
            throw new Exception("Requete incomplete.");
        } else if (request.getTypeTechnique() == null || request.getTypeTechnique().trim().isEmpty()) {
            throw new Exception("Requete incomplete, champ obligatoire 'TypeTechnique' manquant ou vide.");
        } else if (request.getSousType() == null || request.getSousType().trim().isEmpty()) {
            throw new Exception("Requete incomplete, champ obligatoire 'SousType' manquant ou vide.");
        } else if ((request.getDossierID() == null || request.getDossierID().trim().isEmpty()) &&
                ((request.getDossierTitre() == null) || request.getDossierTitre().trim().isEmpty())) {
            throw new Exception("Requete incomplete, champ obligatoire 'DossierID' manquant ou vide alors que le champ DossierTitre est manquant.");
        } else if (request.getDocumentPrincipal() == null
                || request.getDocumentPrincipal().getValue() == null
                || request.getDocumentPrincipal().getValue().length == 0) {
            throw new Exception("Requete incomplete, champ obligatoire 'DocumentPrincipal' manquant ou vide.");
        } else if (request.getDocumentPrincipal().getContentType() == null
                || request.getDocumentPrincipal().getContentType().length() == 0) {
            throw new Exception("Requete incomplete, champ obligatoire 'DocumentPrincipal': type MIME manquant ou vide.");
        } else if(request.getNomDocPrincipal() != null && !wsService.isNomDossierValide(request.getNomDocPrincipal().trim())) {
            throw new Exception("Le champ NomDocPrincipal ('" + request.getNomDocPrincipal() + "') a une syntaxe incorrecte.");
        } else if (request.getDateLimite() != null
                && !request.getDateLimite().trim().isEmpty()
                && !request.getDateLimite().trim().matches("[0-3][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]")) {
            throw new Exception("La DateLimite doit être au format AAAA-MM-JJ, au lieu de: " + request.getDateLimite().trim());
        } else if (request.getVisibilite() == null) {
            throw new Exception("Requete incomplete, champ obligatoire 'Visibilite' manquant ou invalide.");
        } else if (request.getDossierID() != null && !request.getDossierID().trim().isEmpty() && !wsService.isNomDossierValide(request.getDossierID().trim())) {
            throw new Exception("Le champ dossierID ('" + request.getDossierID() + "') a une syntaxe incorrecte.");
        } else if (request.getDossierID() != null && !request.getDossierID().isEmpty() && parapheurService.isNomDossierAlreadyExists(request.getDossierID().trim())) {
            throw new Exception("Le nom de dossier est déjà présent dans le Parapheur: dossierID = " + request.getDossierID().trim());
        }
//        if(request.getDocumentsSupplementaires() != null && request.getDocumentsSupplementaires().getDocAnnexe() != null) {
//            for(DocAnnexe doc : request.getDocumentsSupplementaires().getDocAnnexe()) {
//                if(doc.getNom() != null && !wsService.isNomDossierValide(doc.getNom().trim())) {
//                    throw new Exception("Le champ Nom du document supplémentaire ('" + doc.getNom() + "') a une syntaxe incorrecte.");
//                }
//            }
//        }
//        if(request.getDocumentsAnnexes() != null && request.getDocumentsAnnexes().getDocAnnexe() != null) {
//            for(DocAnnexe doc : request.getDocumentsAnnexes().getDocAnnexe()) {
//                if(doc.getNom() != null && !wsService.isNomDossierValide(doc.getNom().trim())) {
//                    throw new Exception("Le champ Nom du document annexe ('" + doc.getNom() + "') a une syntaxe incorrecte.");
//                }
//            }
//        }
    }

    /**
     * Fonction spécifique BL, permettant de remplacer le nom du type en fonction du nom d'utilisateur
     *
     * @param username      Le nom de l'utilisateur
     * @param typeTechnique Le type
     * @return Le nouveau type
     */
    private String handleBlexSpecificity(String username, String typeTechnique) {
        if ("blex".equals(parapheurService.getHabillage())) {
            // Dans le cas du TMC, si nous demandons un type PES nous devons
            // chercher l'existance d'un type <idCollec>.PES en priorité
            String idCollectivite = "";
            if (username.contains(".")) {
                idCollectivite = username.substring(0, username.indexOf(".")) + ".";
            }
            List<String> sousTypes = null;
            try {
                sousTypes = parapheurService.getSavedSousTypes(typeTechnique, parapheurService.getUniqueParapheurForUser(username));
            } catch (Exception ex) {
                log.debug("type does not exists, continue with given typename");
            }
            if (sousTypes != null) {
                typeTechnique = idCollectivite + typeTechnique;
            }
            return typeTechnique;
        } else {
            return typeTechnique;
        }
    }

    /**
     * Renvoi une réponse webservice en fonction de l'exception reçue
     *
     * @param e L'exception reçue
     * @return Réponse webservice
     */
    private CreerDossierResponse handleException(Exception e) {
        CreerDossierResponse response = new CreerDossierResponse();
        MessageRetour msg = new MessageRetour();

        //  check optionnel sur le contenu de la requete : EmailEmetteur, XPathPourSignaturePES, Annotations
        msg.setCodeRetour("KO");
        msg.setMessage(e.getMessage());
        msg.setSeverite("ERROR");

        response.setMessageRetour(msg);

        return response;
    }

    /**
     * Récupération de l'utilisateur authentifié
     *
     * @return L'utilisateur
     * @throws InvalidUserException En cas d'utilisateur introuvable ou invalide
     */
    private String getValidUser() throws InvalidUserException {
        String lUserName = authenticationComponent.getCurrentUserName();

        if (lUserName == null) {
            throw new InvalidUserException("Authentification invalide.");
        } else if (lUserName.equalsIgnoreCase("admin")) {
            throw new InvalidUserException("Utilisateur invalide ou session expirée.");
        } else {
            return lUserName;
        }
    }
}
