package coop.libriciel.service;

import com.atolcd.parapheur.repo.DossierService;
import lombok.extern.log4j.Log4j;
import org.alfresco.repo.content.MimetypeMap;
import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Créé par lhameury le 11/12/18.
 */
@Service
@Log4j
public class WsService {

    @Autowired
    private DossierService dossierService;

    /**
     * Only check for standard doc types.
     * The Microsoft OpenXML crap is supported if the configuration says so.
     *
     * @param mimeTypeString
     * @return true if supported, false elsewise
     */
    public boolean checkAcceptableMimeTypeBureautique(String mimeTypeString) {
        List<String> authorizedMimeTypes = dossierService.getAuthorizedMimeTypeList();
        if (mimeTypeString == null || mimeTypeString.trim().isEmpty()) {
            if (log.isEnabledFor(Level.WARN)) {
                log.warn("checkAcceptableMimeTypeBur: no mimetype??");
            }
            return false;
        } else if (checkAcceptableMimeTypeXML(mimeTypeString)) {
            // we eliminate the XML stuff, considered as non-desktop types.
            return false;
        } else if (authorizedMimeTypes != null && authorizedMimeTypes.contains(mimeTypeString.trim().toLowerCase())) {
            if (log.isEnabledFor(Level.DEBUG)) {
                log.debug("checkAcceptableMimeTypeBur: mimetype '" + mimeTypeString + "' accepted");
            }
            return true;
        }
        if (log.isEnabledFor(Level.DEBUG)) {
            log.debug("checkAcceptableMimeTypeBur: mimetype '" + mimeTypeString + "' is not accepted");
        }
        return false;
    }

    /**
     * is MIME type one of: text/xml , application/xml , readerpesv2 ?
     *
     * @param mimeTypeString
     * @return true if is acceptable XML mimetype, false elsewise
     */
    public boolean checkAcceptableMimeTypeXML(String mimeTypeString) {
        if (mimeTypeString == null || mimeTypeString.trim().isEmpty()) {
            if (log.isEnabledFor(Level.WARN)) {
                log.warn("checkAcceptableMimeTypeXML: no mimetype??");
            }
            return false;
        } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_XML)
                || mimeTypeString.equalsIgnoreCase("application/xml")
                || mimeTypeString.toLowerCase().contains("readerpesv2")) {
            if (log.isEnabledFor(Level.DEBUG)) {
                log.debug("checkAcceptableMimeTypeXML: mimetype '" + mimeTypeString + "' accepted");
            }
            return true;
        }
        if (log.isEnabledFor(Level.DEBUG)) {
            log.debug("checkAcceptableMimeTypeXML: mimetype '" + mimeTypeString + "' is not accepted as XML");
        }
        return false;
    }

    /**
     * is MIME type one of: application/zip , application/x-zip-compressed
     *
     * @param mimeTypeString
     * @return true if is acceptable ZIP mimetype, false elsewise
     */
    public boolean checkAcceptableMimeTypeCompressed(String mimeTypeString) {
        if (mimeTypeString == null || mimeTypeString.trim().isEmpty()) {
            if (log.isEnabledFor(Level.WARN)) {
                log.warn("checkAcceptableMimeTypeCompressed: no mimetype??");
            }
            return false;
        } else if (mimeTypeString.equalsIgnoreCase(MimetypeMap.MIMETYPE_ZIP)
                || mimeTypeString.equalsIgnoreCase("application/x-zip-compressed")) {
            if (log.isEnabledFor(Level.DEBUG)) {
                log.debug("checkAcceptableMimeTypeCompressed: mimetype '" + mimeTypeString + "' accepted");
            }
            return true;
        }
        if (log.isEnabledFor(Level.DEBUG)) {
            log.debug("checkAcceptableMimeTypeCompressed: mimetype '" + mimeTypeString + "' is not accepted as ZIP");
        }
        return false;
    }

    /**
     * Controle syntaxique sur le dossierID passé en paramètre.
     * On n'accepte pas n'importe quoi...
     *
     * @param nom chaine de caracteres candidate
     * @return true si syntaxe correcte, false sinon.
     */
    public boolean isNomDossierValide(String nom) {
        if (nom == null || nom.trim().isEmpty()) {
            log.warn("nom de dossier null ou vide!");
            return false;
        }
        // Vérifier la syntaxe du dossierID ("\w([\w@\.\(\)\{\}\-_éèêàùçëïü]){0,200}") ?
        // longueur >1, <=200
        // caractères interdits Alfresco       : & " £  * / <  > ? %  | + ;
        return !(nom.length() < 2 || nom.length() > 200 || nom.contains(":")
                || nom.contains("&") || nom.contains("\"") || nom.contains("£")
                || nom.contains("*") || nom.contains("/") || nom.contains("<")
                || nom.contains(">") || nom.contains("?") || nom.contains("%")
                || nom.contains("|") || nom.contains("+") || nom.contains(";"));
    }
}
