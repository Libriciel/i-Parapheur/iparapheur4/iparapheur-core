package coop.libriciel.service;

import com.atolcd.parapheur.model.ParapheurModel;
import org.adullact.iparapheur.util.CollectionsUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserUtilsService {

    @Value("${parapheur.claimedRole.userMetadataBased}")
    boolean roleMetadataBased;

    @Value("${parapheur.suppleance.text}")
    String suppleanceText;

    @Autowired
    private ServiceRegistry serviceRegistry;

    @Autowired
    private NodeUtilsService nodeUtilsService;

    @Autowired
    private DeskUtilsService deskUtilsService;

    public String getFullname(String username) {
        NodeRef personneRef = this.serviceRegistry.getPersonService().getPerson(username);
        String firstName = (String) this.serviceRegistry.getNodeService().getProperty(personneRef, ContentModel.PROP_FIRSTNAME);
        String lastName = (String) this.serviceRegistry.getNodeService().getProperty(personneRef, ContentModel.PROP_LASTNAME);
        return firstName + (lastName != null ? (" " + lastName) : "");
    }

    public String getLocationWithUserAndFolder(String username, NodeRef folder) {
        NodeRef owner = this.serviceRegistry.getPersonService().getPerson(username);
        String metadatas = (String) this.serviceRegistry.getNodeService().getProperty(owner, ContentModel.PROP_ORGID);
        Map<String, String> metas = CollectionsUtils.parseMetadata(metadatas);
        return CollectionsUtils.getString(metas, "VILLE", (String) this.serviceRegistry.getNodeService().getProperty(folder, ParapheurModel.PROP_SIGNATURE_LOCATION));
    }

    public String getRoleWithFolderAndDesk(String username, NodeRef folder, NodeRef currentDesk) {
        NodeRef parentDesk = nodeUtilsService.getFirstParentOfType(folder, ParapheurModel.TYPE_PARAPHEUR);
        String delegRole = "", ownerRole;
        // L'action est-elle faite en délégation ?
        boolean isDelegate = (currentDesk != null && !parentDesk.equals(currentDesk));
        String nomCourant = this.serviceRegistry.getNodeService().getProperty(currentDesk != null ? currentDesk : parentDesk, ContentModel.PROP_TITLE).toString();

        if (roleMetadataBased) {
            if (isDelegate) {
                List<String> owners = deskUtilsService.getDeskOwners(parentDesk);
                if (owners.size() != 1) {
                    delegRole = this.serviceRegistry.getNodeService().getProperty(parentDesk, ContentModel.PROP_TITLE).toString();
                } else {
                    NodeRef deleg = this.serviceRegistry.getPersonService().getPerson(owners.get(0));
                    String metadatas = (String) this.serviceRegistry.getNodeService().getProperty(deleg, ContentModel.PROP_ORGID);
                    Map<String, String> metas = CollectionsUtils.parseMetadata(metadatas);
                    delegRole = metas.containsKey("TITRE") ?
                            metas.get("TITRE") :
                            this.serviceRegistry.getNodeService().getProperty(parentDesk, ContentModel.PROP_TITLE).toString();
                }
            }

            NodeRef owner = this.serviceRegistry.getPersonService().getPerson(username);
            String metadatas = (String) this.serviceRegistry.getNodeService().getProperty(owner, ContentModel.PROP_ORGID);
            Map<String, String> metas = CollectionsUtils.parseMetadata(metadatas);
            ownerRole = CollectionsUtils.getString(metas, "TITRE", nomCourant);
        } else {
            if (isDelegate) {
                delegRole = this.serviceRegistry.getNodeService().getProperty(parentDesk, ContentModel.PROP_TITLE).toString();
            }
            ownerRole = nomCourant;
        }
        return isDelegate ? String.format(suppleanceText, ownerRole, delegRole) : ownerRole;
    }

    public boolean removeCurrentUserSignatureImage() {
        String username = this.serviceRegistry.getAuthenticationService().getCurrentUserName();
        if(username != null) {
            NodeRef user = this.serviceRegistry.getPersonService().getPerson(username);
            if (user != null) {
                ChildAssociationRef childAssociationRef = null;

                /* Création du nœud fils avec pour type d'association ASSOC_SIGNATURE_SCAN */
                for(ChildAssociationRef c : this.serviceRegistry.getNodeService().getChildAssocs(user)) {
                    if(c.getQName().getLocalName().equals(QName.createQName("cm:signature").getLocalName())) {
                        childAssociationRef = c;
                    }
                }

                if(childAssociationRef != null) {
                    this.serviceRegistry.getNodeService().deleteNode(childAssociationRef.getChildRef());
                }
            }
        }
        return true;
    }

}
