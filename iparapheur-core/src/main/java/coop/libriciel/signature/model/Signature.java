package coop.libriciel.signature.model;

import lombok.Data;
import org.springframework.http.MediaType;

import java.io.InputStream;

@Data
public class Signature {
    long length;
    String name;
    MediaType mediaType;
    InputStream stream;

    public String getExtension() {
        switch(mediaType.getSubtype()) {
            case "pkcs7":
            case "pkcs7-signature":
                return "p7s";
            case "pkcs1":
                return "p1s";
            default:
                return mediaType.getSubtype();
        }
    }
}
