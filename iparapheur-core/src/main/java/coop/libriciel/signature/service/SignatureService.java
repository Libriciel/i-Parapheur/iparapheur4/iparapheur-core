package coop.libriciel.signature.service;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import coop.libriciel.signature.model.Signature;
import coop.libriciel.signature.exception.SignatureNotFoundException;
import lombok.extern.log4j.Log4j;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

@Service
@Log4j
public class SignatureService {

    @Autowired
    private ParapheurService parapheurService;

    @Autowired
    private ServiceRegistry serviceRegistry;

    private NodeRef getSignaturesFolderNode() {
        return AuthenticationUtil.runAs(() -> {
            List<NodeRef> dictionnaryNode = this.serviceRegistry.getSearchService().selectNodes(
                    this.serviceRegistry.getNodeService().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                    "/app:company_home/app:dictionary",
                    null,
                    this.serviceRegistry.getNamespaceService(),
                    false);

            NodeRef signaturesFolderNode = null;

            if(!dictionnaryNode.isEmpty()) {
                signaturesFolderNode = this.serviceRegistry.getNodeService().getChildByName(dictionnaryNode.get(0), ContentModel.ASSOC_CONTAINS, "Signatures list");

                if(signaturesFolderNode == null) {
                    FileInfo bureauInfo = this.serviceRegistry.getFileFolderService().create(dictionnaryNode.get(0), "Signatures list", ParapheurModel.TYPE_SIGNATURES_FOLDER);
                    signaturesFolderNode = bureauInfo.getNodeRef();
                }
            }
            return signaturesFolderNode;
        }, AuthenticationUtil.getAdminUserName());
    }

    private String getStepId(String documentId) {
        NodeRef document = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, documentId);
        NodeRef dossier = this.serviceRegistry.getNodeService().getPrimaryParent(document).getParentRef();
        return parapheurService.getCurrentEtapeCircuit(dossier).getNodeRef().getId();
    }

    public Signature getDetachedSignature(String documentId, String etapeId) {
        Signature signature;

        NodeRef document = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, documentId);

        NodeRef signaturesFolderNode = getSignaturesFolderNode();

        NodeRef signatureFolderRef = this.serviceRegistry.getNodeService().getChildByName(signaturesFolderNode, ContentModel.ASSOC_CONTAINS, documentId);

        if(signatureFolderRef == null) {
            /*
            Si le dossier de signature n'est pas trouvé, alors soit :
            1 - Le document n'est pas signé
            2 - Le document a été signé avant la version 4.7.0

            Donc, avant de retourner une erreur, on récupère la signature en mode retro-compatible
            Si la signature n'est pas trouvé, l'exception "SignatureNotFoundException" est lancée
             */
            NodeRef etape = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, etapeId);

            signature = getDetachedSignatureBackwardCompatibilityMethod(document, etape);
        } else {
            signature = getDetachedSignatureWithNewMethod(signatureFolderRef, document, etapeId);
        }

        return signature;
    }

    public void deleteDetachedSignature(String documentId) {
        AuthenticationUtil.runAs(() -> {
            NodeRef signaturesFolderNode = getSignaturesFolderNode();
            String stepId = getStepId(documentId);

            NodeRef signatureFolderRef = this.serviceRegistry.getNodeService().getChildByName(signaturesFolderNode, ContentModel.ASSOC_CONTAINS, documentId);
            if(signatureFolderRef == null) {
                return null;
            }

            NodeRef signatureContentRef = this.serviceRegistry.getNodeService().getChildByName(signatureFolderRef, ContentModel.ASSOC_CONTAINS, stepId);
            // This node does not exists
            if(signatureContentRef == null) {
                return null;
            }

            this.serviceRegistry.getNodeService().deleteNode(signatureContentRef);

            if(this.serviceRegistry.getNodeService().getChildAssocs(signatureFolderRef).isEmpty()) {
                this.serviceRegistry.getNodeService().removeProperty(new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, stepId), ParapheurModel.PROP_SIG);
            }
            return null;
        }, AuthenticationUtil.getAdminUserName());
    }

    public void setDetachedSignature(String documentId, InputStream stream, String mimetype) {
        AuthenticationUtil.runAs(() -> {
            NodeRef signaturesFolderNode = getSignaturesFolderNode();
            String stepId = getStepId(documentId);

            NodeRef signatureFolderRef = this.serviceRegistry.getNodeService().getChildByName(signaturesFolderNode, ContentModel.ASSOC_CONTAINS, documentId);

            if(signatureFolderRef == null) {
                // The folder does not exists, we need to create it
                signatureFolderRef = this.serviceRegistry.getFileFolderService().create(signaturesFolderNode, documentId, ContentModel.TYPE_FOLDER).getNodeRef();
            }

            NodeRef signatureContentRef = this.serviceRegistry.getNodeService().getChildByName(signatureFolderRef, ContentModel.ASSOC_CONTAINS, stepId);
            // This node does not exists, create it
            if(signatureContentRef == null) {
                signatureContentRef = this.serviceRegistry.getFileFolderService().create(signatureFolderRef, stepId, ContentModel.TYPE_CONTENT).getNodeRef();
            }

            ContentWriter writer = this.serviceRegistry.getFileFolderService().getWriter(signatureContentRef);
            writer.setMimetype(mimetype);
            writer.putContent(stream);

            this.serviceRegistry.getNodeService().setProperty(new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, stepId), ParapheurModel.PROP_SIG, "external");

            return null;
        }, AuthenticationUtil.getAdminUserName());
    }

    private Signature getDetachedSignatureWithNewMethod(NodeRef signatureFolderRef, NodeRef document, String etapeId) {
        NodeRef stepSignatureNodeRef = this.serviceRegistry.getNodeService().getChildByName(signatureFolderRef, ContentModel.ASSOC_CONTAINS, etapeId);

        if(stepSignatureNodeRef == null) {
            throw new SignatureNotFoundException("Signature not found on document " + document.getId() + " and step " + etapeId);
        }

        Signature signature = new Signature();

        FileInfo contentInfo = this.serviceRegistry.getFileFolderService().getFileInfo(stepSignatureNodeRef);

        signature.setLength(contentInfo.getContentData().getSize());
        signature.setStream(this.serviceRegistry.getContentService().getReader(stepSignatureNodeRef, ContentModel.PROP_CONTENT).getContentInputStream());

        setSignatureNameAndMedia(document, signature, contentInfo.getContentData().getMimetype());

        return signature;
    }

    private Signature getDetachedSignatureBackwardCompatibilityMethod(NodeRef document, NodeRef etape) {

        // Only works for 1st main document
        NodeRef dossier = this.serviceRegistry.getNodeService().getPrimaryParent(document).getParentRef();
        List<ChildAssociationRef> childAssocs = this.serviceRegistry.getNodeService().getChildAssocs(dossier, ContentModel.ASSOC_CONTAINS,
                RegexQNamePattern.MATCH_ALL);
        if (childAssocs != null) {
            for (ChildAssociationRef child : childAssocs) {
                NodeRef childRef = child.getChildRef();
                if (this.serviceRegistry.getNodeService().hasAspect(childRef, ParapheurModel.ASPECT_MAIN_DOCUMENT)) {
                    if(childRef.getId().equals(document.getId())) {
                        break;
                    } else {
                        return null;
                    }
                }
            }
        }

        Signature signature = new Signature();

        String base64Signature = (String) this.serviceRegistry.getNodeService().getProperty(etape, ParapheurModel.PROP_SIGNATURE_ETAPE);

        if (base64Signature == null) {
            throw new SignatureNotFoundException("Signature not found on document " + document.getId() + " and step " + etape.getId());
        }

        byte[] signatureBytes = Base64.decode(base64Signature);

        signature.setLength(signatureBytes.length);
        signature.setStream(new ByteArrayInputStream(signatureBytes));

        setSignatureNameAndMedia(document, signature, null);

        return signature;
    }

    private void setSignatureNameAndMedia(NodeRef document, Signature signature, String mimetype) {
        String name = this.serviceRegistry.getNodeService().getProperty(document, ContentModel.PROP_NAME) + ".";

        if(mimetype != null) {
            String[] types = mimetype.split("/");
            if(types.length == 2) {
                signature.setMediaType(new MediaType(types[0], types[1]));
            }
        } else {
            NodeRef dossier = this.serviceRegistry.getNodeService().getPrimaryParent(document).getParentRef();
            String sigFormat = (String) this.serviceRegistry.getNodeService().getProperty(dossier, ParapheurModel.PROP_SIGNATURE_FORMAT);

            switch(sigFormat) {
                case ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_CMS_PKCS7:
                    signature.setMediaType(new MediaType("application", "pkcs7"));
                    name += "p7s";
                    break;
                case ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_3_2:
                case ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_XADES_EPES_DET_1_1_1:
                    signature.setMediaType(new MediaType("application", "xml"));
                    name += "xml";
                    break;
                case ParapheurModel.PROP_SIGNATURE_FORMAT_VAL_PKCS1_256:
                    signature.setMediaType(new MediaType("application", "pkcs1"));
                    name += "p1s";
                    break;
                default:
                    signature.setMediaType(new MediaType("application", "unknown"));
                    name += "unknown";
            }
        }

        signature.setName(name);
    }

    public void deleteAllDetachedSignature(String documentId) {
        AuthenticationUtil.runAs(() -> {
            NodeRef signaturesFolderNode = getSignaturesFolderNode();

            NodeRef signatureFolderRef = this.serviceRegistry.getNodeService().getChildByName(signaturesFolderNode, ContentModel.ASSOC_CONTAINS, documentId);
            if(signatureFolderRef == null) {
                return null;
            }

            this.serviceRegistry.getNodeService().deleteNode(signatureFolderRef);

            return null;
        }, AuthenticationUtil.getAdminUserName());
    }
}
