package coop.libriciel.signature.service;

import lombok.Data;

@Data
public class CreateSignatureModel {
    String signature;
    String mimetype;
}
