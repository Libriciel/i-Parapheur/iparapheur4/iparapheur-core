package coop.libriciel.signature.exception;

import org.alfresco.error.AlfrescoRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class SignatureNotFoundException extends AlfrescoRuntimeException {
    public SignatureNotFoundException(String msgId) {
        super(msgId);
    }
}
