package coop.libriciel.signature.webscript;

import coop.libriciel.signature.service.SignatureService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@NoArgsConstructor
@Controller
public class DeleteDetachedSignatureWebscript extends AbstractWebScript {

    @Autowired
    private SignatureService signatureService;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        signatureService.deleteDetachedSignature(webScriptRequest.getServiceMatch().getTemplateVars().get("docid"));
    }
}
