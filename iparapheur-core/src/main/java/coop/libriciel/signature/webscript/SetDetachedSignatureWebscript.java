package coop.libriciel.signature.webscript;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import coop.libriciel.signature.service.CreateSignatureModel;
import coop.libriciel.signature.service.SignatureService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@NoArgsConstructor
@Controller
public class SetDetachedSignatureWebscript extends AbstractWebScript {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private SignatureService signatureService;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {

        String docid = webScriptRequest.getServiceMatch().getTemplateVars().get("docid");

        final GsonBuilder builder = new GsonBuilder();
        final Gson gson = builder.create();

        CreateSignatureModel model = gson.fromJson(webScriptRequest.getContent().getReader(), CreateSignatureModel.class);
        signatureService.setDetachedSignature(
                docid,
                new ByteArrayInputStream(model.getSignature().getBytes()),
                model.getMimetype());
        webScriptResponse.setStatus(HttpStatus.CREATED.value());
    }
}
