package coop.libriciel.signature.webscript;

import coop.libriciel.signature.exception.SignatureNotFoundException;
import coop.libriciel.signature.model.Signature;
import coop.libriciel.signature.service.SignatureService;
import lombok.NoArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Controller;
import org.apache.commons.net.util.Base64;

import java.io.IOException;

@NoArgsConstructor
@Controller
public class GetDetachedSignatureWebscript extends AbstractWebScript {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private SignatureService signatureService;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String docid = webScriptRequest.getServiceMatch().getTemplateVars().get("docid");
        String stepid = webScriptRequest.getServiceMatch().getTemplateVars().get("stepid");

        webScriptResponse.setStatus(200);
        webScriptResponse.setContentEncoding("UTF-8");

        try {
            Signature signature = signatureService.getDetachedSignature(docid, stepid);
            byte[] sigbytes = IOUtils.toByteArray(signature.getStream());
            boolean isB64 = false;
            try {
                isB64 = org.apache.commons.net.util.Base64.isArrayByteBase64(sigbytes) && !(new String(sigbytes)).startsWith("-----"); // This is for PKCS7, which is by default base64 compliant
            } catch(Exception e) {
                // Do nothing
            }
            if(!isB64) {
                sigbytes = Base64.encodeBase64(sigbytes);
            }
            webScriptResponse.getOutputStream().write(sigbytes);
            webScriptResponse.setContentType(signature.getMediaType().toString());
            webScriptResponse.setHeader("Content-Disposition", "attachment; filename=\"" + signature.getName() + "\"");
        } catch(SignatureNotFoundException ex) {
            webScriptResponse.setStatus(404);
        }
    }
}
