package com.atolcd.parapheur.web.bean.wizard.batch;
/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * contact@atolcd.com
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

import com.atolcd.parapheur.model.ParapheurModel;
import junit.framework.TestCase;
import org.alfresco.service.namespace.QName;

import java.util.ArrayList;
import java.util.List;

/**
 * CakeFilterTest
 *
 * @author manz
 *         Date: 11/06/12
 *         Time: 11:35
 */
public class CakeFilterTest extends TestCase {

    public void testGenerateClause() {
        CakeFilter filter = new CakeFilter();

        Predicate<QName, String> predicate = new Predicate<QName, String>(ParapheurModel.PROP_EFFECTUEE, "true");
        Predicate<QName, String> predicate2 = new Predicate<QName, String>(ParapheurModel.PROP_EFFECTUEE, "false");

        Predicate<QName, String> predicate3 = new Predicate<QName, String>(ParapheurModel.PROP_CERTIFICAT, "true");
        Predicate<QName, String> predicate4 = new Predicate<QName, String>(ParapheurModel.PROP_CHILD_COUNT, "4");

        List<Predicate<?, ?>> predicates = new ArrayList<Predicate<?, ?>>();
        predicates.add(predicate);
        predicates.add(predicate2);

        List<Predicate<?, ?>> predicates2 = new ArrayList<Predicate<?, ?>>();
        predicates2.add(predicate3);
        predicates2.add(predicate4);

        Predicate<String , List<Predicate<?,?>>> clause2 = new Predicate<String, List<Predicate<?, ?>>>("and", predicates2);
        predicates.add(clause2);



        Predicate<String , List<Predicate<?,?>>> clause = new Predicate<String, List<Predicate<?, ?>>>("or", predicates);

        filter.setType("ph:dossier");
        filter.setSearchPath("/ph:a-traiter");
        filter.setClause(clause);

        String str = filter.buildFilterQuery();
        System.out.println(str);
    }
}
