/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package fr.starxpert.iparapheur.test.calque.repo.impl;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.repo.impl.ParapheurServiceImpl;
import fr.starxpert.iparapheur.calque.model.CalqueModel;
import fr.starxpert.iparapheur.calque.repo.CalqueService;
import fr.starxpert.iparapheur.calque.repo.impl.CalqueServiceImpl;
import fr.starxpert.iparapheur.calque.repo.impl.ElementCalque;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.junit.Assert;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.mockito.Mockito;

import java.util.List;


public class CalqueServiceTest extends BaseParapheurTest {

    /**
     * CalqueService
     */
    private CalqueService calqueService;
    /**
     * Mocked ActionService
     *
     * @see ActionService
     */
    private ActionService mockedActionService;
    /**
     * Mocked S2lowService
     *
     * @see S2lowService
     */
    private S2lowService mockedS2lowService;
    /**
     * Company home ref
     */
    private NodeRef companyHome;
    private NodeRef archives_configuration;
    /**
     * Parapheurs home ref
     */
    private NodeRef parapheursHome;
    /**
     * Archives home ref
     */
    private NodeRef archivesHome;
    /**
     * Dictionary home ref
     */
    private NodeRef dictionaryHome;
    /**
     * Workflows home ref
     */
    private NodeRef workflowsHome;
    /**
     * Calques home ref
     */
    private NodeRef calquesHome;
    /**
     * Calque 1 ref
     */
    private NodeRef calque1;
    /**
     * Signature Calque 1 ref
     */
    private NodeRef signature1;
    /**
     * Commentaire Calque 1 ref
     */
    private NodeRef commentaire1;
    /**
     * Image Calque 1 ref
     */
    private NodeRef image1;
    /**
     * Metadata Calque 1 ref
     */
    private NodeRef metadata1;


    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        calqueService = (CalqueService) applicationContext.getBean("calqueService");
        ((CalqueServiceImpl) calqueService).setStoreRef(storeRef);


        mockedActionService = Mockito.mock(ActionService.class);
        Mockito.when(mockedActionService.createAction(Mockito.anyString())).thenReturn(actionService.createAction("parapheur-mail"));
        ((ParapheurServiceImpl) parapheurService).setActionService(mockedActionService);

        mockedS2lowService = Mockito.mock(S2lowService.class);
        Mockito.when(mockedS2lowService.isEnabled()).thenReturn(true);
        Mockito.when(
                mockedS2lowService.setS2lowActesArchiveURL(Mockito.any(NodeRef.class))).thenReturn(
                "http://example.com/url-archivage/");
        Mockito.when(
                mockedS2lowService.getInfosS2low(Mockito.any(NodeRef.class))).thenReturn(4);
        ((ParapheurServiceImpl) parapheurService).setS2lowService(mockedS2lowService);

        authenticationService.createAuthentication(tenantService.getDomainUser("proprietaire1", tenantName), "secret".toCharArray());
        authenticationService.createAuthentication(tenantService.getDomainUser("proprietaire2", tenantName), "secret".toCharArray());
        authenticationService.createAuthentication(tenantService.getDomainUser("secretaire2", tenantName), "secret".toCharArray());

        NodeRef prop1 = personService.getPerson(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(prop1, ContentModel.PROP_FIRSTNAME, "Prop");
        nodeService.setProperty(prop1, ContentModel.PROP_LASTNAME, "Ietaire");
        nodeService.setProperty(prop1, ContentModel.PROP_EMAIL, "proprietaire1@example.com");


        companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.company_home.childname"),
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                        namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        archivesHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.archives.childname"),
                        namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();


        permissionService.setInheritParentPermissions(archivesHome, false);
        permissionService.setPermission(archivesHome, "GROUP_EVERYONE", "Contributor", true);

        dictionaryHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.dictionary.childname"),
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        archives_configuration = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("ph:archives_configuration",
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();
        ContentWriter contentWriter = contentService.getWriter(archives_configuration, ContentModel.PROP_CONTENT, true);
        contentWriter.putContent("archive.tamponActes.visible=true\n"
                + "archive.tamponActes.text=Acquitt\u00E9 en PREFECTURE le {0,date,dd/MM/yyyy}");

        workflowsHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.savedworkflows.childname"),
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        calquesHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cal:calques",
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        calque1 = nodeService.createNode(calquesHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("calque1"),
                CalqueModel.TYPE_CALQUE).getChildRef();

        signature1 = nodeService.createNode(calque1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("signature1"),
                CalqueModel.TYPE_SIGNATURE).getChildRef();

        commentaire1 = nodeService.createNode(calque1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("commentaire1"),
                CalqueModel.TYPE_COMMENTAIRE).getChildRef();

        image1 = nodeService.createNode(calque1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("image1"),
                CalqueModel.TYPE_IMAGE).getChildRef();

        metadata1 = nodeService.createNode(calque1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("metadata1"),
                CalqueModel.TYPE_METADATA).getChildRef();

        nodeService.setProperty(signature1, CalqueModel.PROP_PAGE, "1337");
        nodeService.setProperty(commentaire1, CalqueModel.PROP_PAGE, "1337");
        nodeService.setProperty(image1, CalqueModel.PROP_PAGE, "1337");
        nodeService.setProperty(metadata1, CalqueModel.PROP_PAGE, "1337");

    }


    public void testCreerCalque() {
        NodeRef returned = calqueService.creerCalque("calqueTest");
        Assert.assertEquals(nodeService.getProperty(returned,
                CalqueModel.PROP_NOM_CALQUE), "calqueTest");
    }

    /**
     * public void testCopyCalque(){
     * NodeRef returned = calqueService.copyCalque(calque1.getId());
     * Assert.assertEquals(calque1, returned);
     * }*
     */

    public void testAddSignature() {
        NodeRef returned = calqueService.addSignature(calque1.getId(), 0);
        Assert.assertNotNull(returned); // On vérifie que le NodeRef est bien renvoyé
        // On vérifie que la nouvelle signature est bien ajoutée aux signatures de notre calque1
        Assert.assertEquals(calqueService.listSignatures(calque1.getId()).size(), 2);
    }

    public void testSupprimerCalque() {
        NodeRef calqueASupprimer = nodeService.createNode(calquesHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("calqueASupprimer"),
                CalqueModel.TYPE_CALQUE).getChildRef();

        Assert.assertTrue(nodeService.exists(calqueASupprimer));
        // Le calque existe encore
        calqueService.suppressionCalque(calqueASupprimer.getId());
        // Le calque n'existe plus
        Assert.assertFalse(nodeService.exists(calqueASupprimer));
    }

    public void testAddCoordonnees() {
        calqueService.addCoordonnees(calque1, 13, 37, 1337, true);
        Assert.assertEquals(nodeService.getProperty(calque1, CalqueModel.PROP_COORDONNEE_X), 13);
        Assert.assertEquals(nodeService.getProperty(calque1, CalqueModel.PROP_COORDONNEE_Y), 37);
        Assert.assertEquals(nodeService.getProperty(calque1, CalqueModel.PROP_PAGE), 1337);
    }

    public void testAddCoordonneesDefault() {
        calqueService.addCoordonnees(calque1, 0, 0, 1337, true);
        // Les coordonnées (0, 0) sont remplacées par (12, 24)
        Assert.assertEquals(nodeService.getProperty(calque1, CalqueModel.PROP_COORDONNEE_X), 12);
        Assert.assertEquals(nodeService.getProperty(calque1, CalqueModel.PROP_COORDONNEE_Y), 24);
        Assert.assertEquals(nodeService.getProperty(calque1, CalqueModel.PROP_PAGE), 1337);
    }

    public void testAddCommentaireDefault() {
        NodeRef calqueDefault = nodeService.createNode(calquesHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("calqueDefault"),
                CalqueModel.TYPE_CALQUE).getChildRef();
        NodeRef returned = calqueService.addCommentaire(calqueDefault.getId(), "Commentaiste", 1337, "noige");
        // Toute autre valeur que "rouge" est changée pour "noir"
        Assert.assertEquals(nodeService.getProperty(returned, CalqueModel.PROP_TEXTE), "Commentaiste");
        Assert.assertEquals(nodeService.getProperty(returned, CalqueModel.PROP_TAILLE_POLICE), 1337);
        Assert.assertEquals(nodeService.getProperty(returned, CalqueModel.PROP_COULEUR_TEXTE), "noir");
        Assert.assertEquals(calqueService.listCommentaires(calqueDefault.getId()).size(), 1);
    }

    public void testAddCommentaireRouge() {
        NodeRef calqueRouge = nodeService.createNode(calquesHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("calqueRouge"),
                CalqueModel.TYPE_CALQUE).getChildRef();
        NodeRef returned = calqueService.addCommentaire(calqueRouge.getId(), "Commentaiste", 1337, "rouge");
        // Toute autre valeur que "rouge" est changée pour "noir"
        Assert.assertEquals(nodeService.getProperty(returned, CalqueModel.PROP_TEXTE), "Commentaiste");
        Assert.assertEquals(nodeService.getProperty(returned, CalqueModel.PROP_TAILLE_POLICE), 1337);
        Assert.assertEquals(nodeService.getProperty(returned, CalqueModel.PROP_COULEUR_TEXTE), "rouge");
        Assert.assertEquals(calqueService.listCommentaires(calqueRouge.getId()).size(), 1);
    }

    /**
     * public void testAddImage(){
     * new  InputStreamContent(null, tenantName, tenantName);
     * }*
     */

    public void testAddMetadata() {

        ChildAssociationRef metadataRef = nodeService.createNode(
                calquesHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("calqueMedatada"),
                CalqueModel.TYPE_CALQUE
        );

        NodeRef calqueMedatada = metadataRef.getChildRef();
        QName nomMaMetadata = QName.createQName("maMetadata");
        int fontSize = 8;
        NodeRef returned = calqueService.addMetadata(calqueMedatada.getId(), "maMetadata", fontSize);

        Assert.assertEquals(nodeService.getProperty(returned, CalqueModel.PROP_QNAME_MD), nomMaMetadata);
        Assert.assertEquals(nodeService.getProperty(returned, CalqueModel.PROP_TAILLE_POLICE), fontSize);
        Assert.assertTrue(calqueService.listMetadata(calqueMedatada.getId()).get(0).equals(returned));
    }

    public void testListCommentaires() {
        List<NodeRef> returned = calqueService.listCommentaires(calque1.getId());
        Assert.assertEquals(commentaire1, returned.get(0));
    }

    public void testListSignatures() {
        List<NodeRef> returned = calqueService.listSignatures(calque1.getId());
        Assert.assertEquals(signature1, returned.get(0));
    }

    public void testListImages() {
        List<NodeRef> returned = calqueService.listImages(calque1.getId());
        Assert.assertEquals(image1, returned.get(0));
    }

    public void testListMetadata() {
        List<NodeRef> returned = calqueService.listMetadata(calque1.getId());
        Assert.assertEquals(metadata1, returned.get(0));
    }

    public void testGetElementsForCalque() {
        List<ElementCalque> returned = calqueService.getElementsForCalque(calque1);
        Assert.assertEquals(4, returned.size());
        Assert.assertEquals(returned.get(0).getNodeRef(), commentaire1);
        Assert.assertEquals(returned.get(1).getNodeRef(), image1);
        Assert.assertEquals(returned.get(2).getNodeRef(), signature1);
        Assert.assertEquals(returned.get(3).getNodeRef(), metadata1);
    }

    public void testSuppressionSignature() {
        NodeRef signatureASupprimer = nodeService.createNode(calque1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("signatureASupprimer"),
                CalqueModel.TYPE_SIGNATURE).getChildRef();

        Assert.assertTrue(nodeService.exists(signatureASupprimer));
        // Existe encore
        calqueService.suppressionSignature(signatureASupprimer.getId());
        // N'existe plus
        Assert.assertFalse(nodeService.exists(signatureASupprimer));
    }


    public void testSuppressionCommentaire() {
        NodeRef commentaireASupprimer = nodeService.createNode(calque1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("commentaireASupprimer"),
                CalqueModel.TYPE_COMMENTAIRE).getChildRef();

        Assert.assertTrue(nodeService.exists(commentaireASupprimer));
        // Existe encore
        calqueService.suppressionCommentaire(commentaireASupprimer.getId());
        // N'existe plus
        Assert.assertFalse(nodeService.exists(commentaireASupprimer));
    }

    public void testSuppressionImage() {
        NodeRef imageASupprimer = nodeService.createNode(calque1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("imageASupprimer"),
                CalqueModel.TYPE_IMAGE).getChildRef();

        Assert.assertTrue(nodeService.exists(imageASupprimer));
        // Existe encore
        calqueService.suppressionImage(imageASupprimer.getId());
        // N'existe plus
        Assert.assertFalse(nodeService.exists(imageASupprimer));
    }

    public void testSuppressionMetadata() {
        NodeRef metadataASupprimer = nodeService.createNode(calque1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("metadataASupprimer"),
                CalqueModel.TYPE_METADATA).getChildRef();

        Assert.assertTrue(nodeService.exists(metadataASupprimer));
        // Existe encore
        calqueService.suppressionMetadata(metadataASupprimer.getId());
        // N'existe plus
        Assert.assertFalse(nodeService.exists(metadataASupprimer));
    }

}
