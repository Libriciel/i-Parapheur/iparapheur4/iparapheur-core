/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adullact.iparapheur.util;

import java.util.Arrays;
import java.util.Collection;
import junit.framework.TestCase;

/**
 *
 * @author viv
 */
public class CollectionsUtilsTest extends TestCase {

    public void testContainsOneOf() {
        Collection<String> coll = Arrays.asList("string", "string1", "string2");
        Collection<String> oneOf = Arrays.asList("string", "anotherString");

        boolean contains = CollectionsUtils.containsOneOf(coll, oneOf);

        assertTrue(contains);
    }

    public void testDoesntContainsOneOf() {
        Collection<String> coll = Arrays.asList("string0", "string1", "string2");
        Collection<String> oneOf = Arrays.asList("string", "anotherString");

        CollectionsUtils mesoutils = new CollectionsUtils();
        boolean contains = CollectionsUtils.containsOneOf(coll, oneOf);

        assertFalse(contains);
    }

}
