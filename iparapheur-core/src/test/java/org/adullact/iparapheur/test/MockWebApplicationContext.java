/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.test;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Locale;
import java.util.Map;
import javax.servlet.ServletContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.io.Resource;
import org.springframework.web.context.WebApplicationContext;

/**
 * Used to mock Web application context needed by some Spring beans.
 *
 * This mock is constructed around an existing application context, and
 * delegates most of method calls to it.
 *
 * Mock web application context also need a ServletContext, which will be
 * returned when asked.
 *
 * @author Vivien Barousse
 */
public class MockWebApplicationContext implements WebApplicationContext, ApplicationContext {

    /**
     * The original application context.
     * 
     * Used to delegate most of the methods calls.
     */
    private ApplicationContext applicationContext;

    /**
     * Servlet context to use.
     */
    private ServletContext servletContext;

    public MockWebApplicationContext(ApplicationContext applicationContext, ServletContext servletContext) {
        this.applicationContext = applicationContext;
        this.servletContext = servletContext;
    }

    @Override
    public boolean containsBean(String arg0) {
        return applicationContext.containsBean(arg0);
    }

    @Override
    public boolean containsBeanDefinition(String arg0) {
        return applicationContext.containsBeanDefinition(arg0);
    }

    @Override
    public boolean containsLocalBean(String arg0) {
        return applicationContext.containsLocalBean(arg0);
    }

    @Override
    public String[] getAliases(String arg0) {
        return applicationContext.getAliases(arg0);
    }

    @Override
    public AutowireCapableBeanFactory getAutowireCapableBeanFactory()
            throws IllegalStateException {
        return applicationContext.getAutowireCapableBeanFactory();
    }

    @Override
    public Object getBean(String arg0, Class arg1) throws BeansException {
        return applicationContext.getBean(arg0, arg1);
    }

    @Override
    public Object getBean(String arg0) throws BeansException {
        return applicationContext.getBean(arg0);
    }

    @Override
    public int getBeanDefinitionCount() {
        return applicationContext.getBeanDefinitionCount();
    }

    @Override
    public String[] getBeanDefinitionNames() {
        return applicationContext.getBeanDefinitionNames();
    }

    @Override
    public String[] getBeanNamesForType(Class arg0, boolean arg1, boolean arg2) {
        return applicationContext.getBeanNamesForType(arg0, arg1, arg2);
    }

    @Override
    public String[] getBeanNamesForType(Class arg0) {
        return applicationContext.getBeanNamesForType(arg0);
    }

    @Override
    public Map getBeansOfType(Class arg0, boolean arg1, boolean arg2)
            throws BeansException {
        return applicationContext.getBeansOfType(arg0, arg1, arg2);
    }

    @Override
    public Map getBeansOfType(Class arg0) throws BeansException {
        return applicationContext.getBeansOfType(arg0);
    }

    @Override
    public ClassLoader getClassLoader() {
        return applicationContext.getClassLoader();
    }

    @Override
    public String getDisplayName() {
        return applicationContext.getDisplayName();
    }

    @Override
    public String getId() {
        return applicationContext.getId();
    }

    @Override
    public String getMessage(MessageSourceResolvable arg0, Locale arg1)
            throws NoSuchMessageException {
        return applicationContext.getMessage(arg0, arg1);
    }

    @Override
    public String getMessage(String arg0, Object[] arg1, Locale arg2)
            throws NoSuchMessageException {
        return applicationContext.getMessage(arg0, arg1, arg2);
    }

    @Override
    public String getMessage(String arg0, Object[] arg1, String arg2,
            Locale arg3) {
        return applicationContext.getMessage(arg0, arg1, arg2, arg3);
    }

    @Override
    public ApplicationContext getParent() {
        return applicationContext.getParent();
    }

    @Override
    public BeanFactory getParentBeanFactory() {
        return applicationContext.getParentBeanFactory();
    }

    @Override
    public Resource getResource(String arg0) {
        return applicationContext.getResource(arg0);
    }

    @Override
    public Resource[] getResources(String arg0) throws IOException {
        return applicationContext.getResources(arg0);
    }

    @Override
    public long getStartupDate() {
        return applicationContext.getStartupDate();
    }

    @Override
    public Class getType(String arg0) throws NoSuchBeanDefinitionException {
        return applicationContext.getType(arg0);
    }

    @Override
    public boolean isSingleton(String arg0)
            throws NoSuchBeanDefinitionException {
        return applicationContext.isSingleton(arg0);
    }

    @Override
    public boolean isTypeMatch(String arg0, Class arg1)
            throws NoSuchBeanDefinitionException {
        return applicationContext.isTypeMatch(arg0, arg1);
    }

    @Override
    public void publishEvent(ApplicationEvent arg0) {
        applicationContext.publishEvent(arg0);
    }

    @Override
    public ServletContext getServletContext() {
        return servletContext;
    }

    @Override
    public boolean isPrototype(String arg0) throws NoSuchBeanDefinitionException {
        return applicationContext.isPrototype(arg0);
    }

    @Override
    public Map<String, Object> getBeansWithAnnotation(Class<? extends Annotation> type) throws BeansException {
        return applicationContext.getBeansWithAnnotation(type);
    }

    @Override
    public <A extends Annotation> A findAnnotationOnBean(String string, Class<A> type) {
        return applicationContext.findAnnotationOnBean(string, type);
    }

    @Override
    public <T> T getBean(Class<T> type) throws BeansException {
        return applicationContext.getBean(type);
    }

    @Override
    public Object getBean(String string, Object... os) throws BeansException {
        return applicationContext.getBean(string, os);
    }

}
