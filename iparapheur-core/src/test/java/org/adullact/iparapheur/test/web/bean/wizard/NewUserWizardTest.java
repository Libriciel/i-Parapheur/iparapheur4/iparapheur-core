package org.adullact.iparapheur.test.web.bean.wizard;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.QName;
import org.alfresco.model.ContentModel;
import com.atolcd.parapheur.model.ParapheurModel;

import com.atolcd.parapheur.web.bean.wizard.NewUserWizard;
import org.alfresco.service.cmr.repository.NodeRef;

public class NewUserWizardTest extends WebBeanBaseTest {

    private NewUserWizard wizard;

      private NodeRef companyHome;
    /**
     * Parapheurs home ref
     */
    private NodeRef parapheursHome;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();


         companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.company_home.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        setUpManagedBean("PersonService", personService);
        setUpManagedBean("NamespaceService", namespaceService);

        wizard = new NewUserWizard();
        wizard.setAuthenticationService(authenticationService);
        wizard.setNodeService(nodeService);
        wizard.setSearchService(searchService);
        wizard.setPersonService(personService);
        wizard.setPermissionService((PermissionService) applicationContext.getBean("permissionService"));
        wizard.setNamespaceService(namespaceService);

        authenticate("admin");
    }

    public void testFinish() {
        wizard.init();

        wizard.setUserName(tenantService.getDomainUser("username", tenantName));
        wizard.setPassword("password");
        wizard.setConfirm("password");
        wizard.setEmail("email@example.com");

        wizard.finish();

        Assert.assertTrue(personService.personExists(tenantService.getDomainUser("username", tenantName)));
    }

    public void testFinishUserAlreadyExists() {
        wizard.init();

        wizard.setUserName(tenantService.getDomainUser("user", tenantName));
        wizard.setPassword("password");
        wizard.setConfirm("password");
        wizard.setEmail("email@example.com");

        wizard.finish();
        Assert.assertTrue(facesContext.getMessages().hasNext());
    }

    public void testGetSummary() {
        String returned = wizard.getSummary();
        Assert.assertNotNull(returned);
    }

    public void testFinish2() {

        nodeService.deleteNode(parapheursHome);
        
        wizard.init();

        wizard.setUserName(tenantService.getDomainUser("username", tenantName));
        wizard.setPassword("password");
        wizard.setConfirm("password");
        wizard.setEmail("email@example.com");

        wizard.finish();

        Assert.assertTrue(personService.personExists(tenantService.getDomainUser("username", tenantName)));

    }
}
