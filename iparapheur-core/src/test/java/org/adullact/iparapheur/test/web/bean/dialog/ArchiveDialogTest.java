package org.adullact.iparapheur.test.web.bean.dialog;

import java.io.Serializable;
import java.util.*;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.NavigationBean;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.web.bean.dialog.ArchiveDialog;
import org.adullact.iparapheur.repo.ged.ArchilandConnector;
import org.adullact.iparapheur.repo.jscript.ScriptEtapeCircuitImpl;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.web.bean.repository.Node;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class ArchiveDialogTest extends WebBeanBaseTest {

    private NodeRef parapheurRef;
    private NodeRef dossierRef;
    private NodeRef documentRef;
    private ArchiveDialog archiveDialog;
    private NavigationBean navigator;
    private NodeRef archives_configuration;
    private ArchilandConnector archilandConnector;

/*    public ArchilandConnector getArchilandConnector() {
        return archilandConnector;
    }

    public void setArchilandConnector(ArchilandConnector archilandConnector) {
        this.archilandConnector = archilandConnector;
    }
*/
    
    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();
        archilandConnector = (ArchilandConnector) applicationContext.getBean("ArchilandConnector");

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "parapheurRef");
        properties.put(ContentModel.PROP_TITLE, "parapheurRef");
        ArrayList<String> ownersPh = new ArrayList<String>();
               ownersPh.add(tenantService.getDomainUser("proprietaire", tenantName));

        properties.put(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR,
                ownersPh);
        parapheurRef = parapheurService.createParapheur(properties);

        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "dossierRef");
        properties.put(ContentModel.PROP_TITLE, "dossierRef");
        properties.put(ParapheurModel.PROP_TERMINE, true);
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        properties.put(ParapheurModel.PROP_TYPE_METIER, "lala");
        properties.put(ParapheurModel.PROP_SOUSTYPE_METIER, "lalala");
        dossierRef = dossierService.createDossier(parapheurRef, properties);

        documentRef = nodeService.createNode(dossierRef,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}documentRef"),
                ParapheurModel.TYPE_DOCUMENT).getChildRef();
        ContentWriter writer = contentService.getWriter(documentRef, ContentModel.PROP_CONTENT, true);
        writer.setMimetype(MimetypeMap.MIMETYPE_PDF);
        writer.putContent(this.getClass().getResourceAsStream("/samples/document.pdf"));

        parapheurService.setCircuit(dossierRef, Arrays.<EtapeCircuit>asList(new ScriptEtapeCircuitImpl("PARAPHEUR", parapheurRef, "SIGNATURE", new HashSet<NodeRef>())));

        BrowseBean browseBean = Mockito.mock(BrowseBean.class);

        navigator = Mockito.mock(NavigationBean.class);


        archives_configuration = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("ph:archives_configuration",
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();
        ContentWriter contentWriter = contentService.getWriter(archives_configuration, ContentModel.PROP_CONTENT, true);
        contentWriter.putContent("archive.tamponActes.visible=true\n"
                + "archive.tamponActes.text=Acquitt\u00E9 en PREFECTURE le {0,date,dd/MM/yyyy}");


        archiveDialog = new ArchiveDialog();
        archiveDialog.setBrowseBean(browseBean);
        archiveDialog.setDictionaryService((DictionaryService) applicationContext.getBean("dictionaryService"));
        archiveDialog.setFileFolderService(fileFolderService);
        archiveDialog.setNamespaceService(namespaceService);
        archiveDialog.setNavigator(navigator);
        archiveDialog.setNodeService(nodeService);
        archiveDialog.setParapheurService(parapheurService);
        archiveDialog.setSearchService(searchService);
        archiveDialog.setArchilandConnector(archilandConnector);
    }

    public void testFinish() throws Exception {
        nodeService.setProperty(dossierRef, ParapheurModel.PROP_READING_MANDATORY, false);
        authenticate("proprietaire");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("id", dossierRef.getId());
        archiveDialog.init(parameters);
        parapheurService.approve(dossierRef);
        parapheurService.markAsRead(dossierRef);
        parapheurService.approve(dossierRef);

        if (!transactionStatus.isRollbackOnly()) {
            transactionManager.commit(transactionStatus);
        }
        transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
        archiveDialog.finish();
        // TODO: How to assert dossier has been archived
    }

    public void testGetArchiveName() {
        archiveDialog.setArchiveName("nom de l'archive");

        String returned = archiveDialog.getArchiveName();
        Assert.assertEquals("nom de l'archive", returned);
    }

    public void testSetArchiveName() {
        archiveDialog.setArchiveName("nom de l'archive");

        String returned = archiveDialog.getArchiveName();
        Assert.assertEquals("nom de l'archive", returned);
    }

    public void testSetDossierRef() {
        archiveDialog.setDossierRef(dossierRef);

        NodeRef returned = archiveDialog.getDossierRef();
        Assert.assertNotNull(returned.getId());
//        Assert.assertEquals("id", returned.getId());
    }

    public void testGetDossierRef() {
        archiveDialog.setDossierRef(dossierRef);


        NodeRef returned = archiveDialog.getDossierRef();
        Assert.assertNotNull(returned.getId());
        Assert.assertEquals(dossierRef.getId(), returned.getId());
    }

    public void testInitPasId() {

        authenticate("proprietaire");

        Mockito.when(navigator.getCurrentNode()).thenReturn(new Node(parapheursHome));

        Map<String, String> parameters = new HashMap<String, String>();
        try {
            archiveDialog.init(parameters);
            Assert.fail("ArchiveDialog called on a non-dossier element");
        } catch (AlfrescoRuntimeException ex) {
        }


    }

    public void testInitPasUnDossier() {

        authenticate("proprietaire");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("id", parapheursHome.getId());
        try {
            archiveDialog.init(parameters);
            Assert.fail("ArchiveDialog called on a non-dossier element");
        } catch (AlfrescoRuntimeException ex) {
        }
    }

    public void testGetDossierName() {

        Map<String, String> properties = new HashMap<String, String>();
        properties.put("id", dossierRef.getId());
        archiveDialog.init(properties);
        archiveDialog.setDossierRef(dossierRef);

        String returned = archiveDialog.getDossierName();
        Assert.assertNotNull(returned);
        Assert.assertEquals(returned, "dossierRef");
    }

    public void testGetFinishButtonDisabled() {
        Boolean returned = archiveDialog.getFinishButtonDisabled();
        Assert.assertFalse(returned);
    }
}
