package org.adullact.iparapheur.test.web.action.evaluator;

import java.util.Arrays;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.DeleteDocumentEvaluator;

public class DeleteDocumentEvaluatorTest extends WebUIBaseTest {

    private DeleteDocumentEvaluator evaluator;
    private NodeRef premierDocRef;
    private NodeRef nodeRef;
    private NodeRef dossierRef;
    private NodeRef parapheurRef;
    private NodeRef corbeilleRef;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
        premierDocRef = new NodeRef("workspace://SpacesStore/premierDocRef");
        dossierRef = new NodeRef("workspace://SpacesStore/dossierRef");
        parapheurRef = new NodeRef("workspace://SpacesStore/parapheurRef");
        corbeilleRef = new NodeRef("workspace://SpacesStore/corbeilleRef");

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);

        Mockito.when(parapheurService.getParentCorbeille(dossierRef)).thenReturn(
                corbeilleRef);
        
        Mockito.when(parapheurService.getCorbeille(parapheurRef, ParapheurModel.NAME_EN_PREPARATION)).thenReturn(
                corbeilleRef);

        Mockito.when(parapheurService.getParentDossier(nodeRef)).thenReturn(
                dossierRef);
        Mockito.when(parapheurService.getParentDossier(premierDocRef)).thenReturn(dossierRef);
        Mockito.when(parapheurService.isEmis(dossierRef)).thenReturn(false);
        Mockito.when(parapheurService.getParentParapheur(nodeRef)).thenReturn(
                parapheurRef);
        Mockito.when(parapheurService.getParentParapheur(premierDocRef)).thenReturn(parapheurRef);
       Mockito.when(parapheurService.getParentParapheur(dossierRef)).thenReturn(
                parapheurRef);

        Mockito.when(
                parapheurService.isParapheurSecretaire(parapheurRef,
                tenantService.getDomainUser("secretaire", tenantName))).thenReturn(true);
        Mockito.when(
                parapheurService.isParapheurOwner(parapheurRef,
                tenantService.getDomainUser("proprietaire", tenantName))).thenReturn(true);

        Mockito.when(
                nodeService.getChildAssocs(dossierRef,
                ContentModel.ASSOC_CONTAINS,
                RegexQNamePattern.MATCH_ALL)).thenReturn(
                Arrays.asList(new ChildAssociationRef(
                ContentModel.ASSOC_CONTAINS, dossierRef, QName.createQName("{test}premierDocRef"),
                premierDocRef), new ChildAssociationRef(
                ContentModel.ASSOC_CONTAINS, dossierRef, QName.createQName("{test}nodeRef"), nodeRef)));
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(false);

        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);
        setUpManagedBean("AuthenticationComponent", authenticationComponent);

        evaluator = new DeleteDocumentEvaluator();
    }

    
    public void testEvaluateDossierEmis() {

        authenticate("proprietaire");
        Mockito.when(parapheurService.isEmis(dossierRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(premierDocRef));
        Assert.assertFalse(returned);

    }

    public void testEvaluatePremierDocument() {
        authenticate("proprietaire");
        boolean returned = evaluator.evaluate(new Node(premierDocRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateSecretariat() {
        Mockito.when(
                nodeService.hasAspect(dossierRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);
        authenticate("secretaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateSecretariatNonSecretaire() {
        Mockito.when(
                nodeService.hasAspect(dossierRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);
        authenticate("user");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateNonProprietaire() {
        authenticate("user");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateProprietaire() {
        authenticate("proprietaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateProprietaireObject() {
        authenticate("proprietaire");

        Object monNode = new Node(nodeRef);
        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }

    public void testEvaluateNull() {
        authenticate("proprietaire");

        Mockito.when(parapheurService.getParentDossier(nodeRef)).thenReturn(
                null);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }
}
