package org.adullact.iparapheur.test.web.bean;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.bean.LoginBean;
import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.app.Application;
import org.alfresco.web.bean.LoginOutcomeBean;
import org.alfresco.web.bean.NavigationBean;
import org.alfresco.web.bean.users.UserPreferencesBean;
import org.junit.Assert;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class LoginBeanTest extends WebUIBaseTest {

    private LoginBean bean;

	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();

        List<NodeRef> parapheurs = new ArrayList<NodeRef>();
        NodeRef parapheur = new NodeRef("workspace://SpacesStore/nodeRef");
        parapheurs.add(parapheur);

		parapheurService = Mockito.mock(ParapheurService.class);
		Mockito.when(parapheurService.getOwnedParapheurs(tenantService.getDomainUser(TEST_ACCOUNT, tenantName))).thenReturn(parapheurs);
        Mockito.when(parapheurService.getOwnedParapheur(tenantService.getDomainUser(TEST_ACCOUNT, tenantName))).thenReturn(parapheur);
		facesContext.getApplication().createValueBinding("#{LoginOutcomeBean}").setValue(facesContext, new LoginOutcomeBean());

		bean = new LoginBean();
		bean.setAuthenticationService(authenticationService);
		bean.setPersonService(personService);
		bean.setNodeService(nodeService);
		bean.setParapheurService(parapheurService);
		bean.setSearchService(searchService);
		bean.setNamespaceService(namespaceService);
		bean.setNavigator(new NavigationBean());
		bean.setUserPreferencesBean(new UserPreferencesBean());
    }

	public void testLogin() {
		bean.setUsername(tenantService.getDomainUser(TEST_ACCOUNT, tenantName));
		bean.setPassword(TEST_PASSWORD);
		bean.login();

		Assert.assertNotNull(Application.getCurrentUser(facesContext));
		Assert.assertEquals(tenantService.getDomainUser(TEST_ACCOUNT, tenantName),
							Application.getCurrentUser(facesContext).getUserName());
	}

	public void testLoginBadPassword() {
		bean.setUsername(TEST_ACCOUNT);
		bean.setPassword("badpassword");
		bean.login();

		Assert.assertNull(Application.getCurrentUser(facesContext));
	}

	public void testLoginBadLogin() {
		bean.setUsername("badlogin");
		bean.setPassword("badpassword");
        bean.login();

		Assert.assertNull(Application.getCurrentUser(facesContext));
	}
}
