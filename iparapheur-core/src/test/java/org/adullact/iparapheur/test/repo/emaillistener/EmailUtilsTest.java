package org.adullact.iparapheur.test.repo.emaillistener;

import java.util.Date;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;

import org.junit.Assert;
import junit.framework.TestCase;

import org.mockito.Mockito;

import com.atolcd.parapheur.repo.emailListener.EmailMessageHeaders;
import com.atolcd.parapheur.repo.emailListener.EmailUtils;

public class EmailUtilsTest extends TestCase {

	private Message message;

	@Override
	protected void setUp() throws Exception {
		message = Mockito.mock(Message.class);

		Mockito.when(message.getFrom()).thenReturn(
				new Address[] { new InternetAddress("from1@example.com") });
		Mockito.when(message.getRecipients(Message.RecipientType.TO))
				.thenReturn(
						new Address[] { new InternetAddress("to1@example.com"),
								new InternetAddress("to2@example.com"),
								new InternetAddress("to3@example.com") });
		Mockito.when(message.getRecipients(Message.RecipientType.CC))
				.thenReturn(
						new Address[] { new InternetAddress("cc1@example.com"),
								new InternetAddress("cc2@example.com"),
								new InternetAddress("cc3@example.com") });
		Mockito.when(message.getRecipients(Message.RecipientType.BCC))
				.thenReturn(
						new Address[] {
								new InternetAddress("bcc1@example.com"),
								new InternetAddress("bcc2@example.com"),
								new InternetAddress("bcc3@example.com") });
		Mockito.when(message.getSubject()).thenReturn("subject");
		Mockito.when(message.getSentDate()).thenReturn(new Date());
		Mockito.when(message.getReceivedDate()).thenReturn(new Date());
	}

	public void testGetHeaders() throws Exception {
		EmailMessageHeaders headers = EmailUtils.extractHeaders(message);

		Assert.assertTrue(headers.getFrom().contains("from1@example.com"));

		Assert.assertTrue(headers.getTo().contains("to1@example.com"));
		Assert.assertTrue(headers.getTo().contains("to2@example.com"));
		Assert.assertTrue(headers.getTo().contains("to3@example.com"));

		Assert.assertTrue(headers.getCc().contains("cc1@example.com"));
		Assert.assertTrue(headers.getCc().contains("cc2@example.com"));
		Assert.assertTrue(headers.getCc().contains("cc3@example.com"));

		Assert.assertTrue(headers.getBcc().contains("bcc1@example.com"));
		Assert.assertTrue(headers.getBcc().contains("bcc2@example.com"));
		Assert.assertTrue(headers.getBcc().contains("bcc3@example.com"));

		Assert.assertEquals(message.getSubject(), headers.getSubject());

		Assert.assertEquals(message.getSentDate(), headers.getSentDate());

		Assert.assertEquals(message.getReceivedDate(), headers
				.getReceivedDate());
	}
}
