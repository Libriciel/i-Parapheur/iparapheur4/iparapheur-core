package org.adullact.iparapheur.test.repo.security.permissions.dynamic;

import org.junit.Assert;

import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.mockito.Mockito;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.security.permissions.dynamic.ActeurCourantDynamicAuthority;

public class ActeurCourantDynamicAuthorityTest extends BaseParapheurTest {
	
	private NodeRef documentNodeRef;
	
	private NodeRef dossierNodeRef;
	
	private ActeurCourantDynamicAuthority authority;
	
	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();
		
		dossierNodeRef = new NodeRef("workspace://StoreRef/dossier");
		documentNodeRef = new NodeRef("workspace://StoreRef/document");
		
		parapheurService = Mockito.mock(ParapheurService.class);
		Mockito.when(parapheurService.getParentDossier(documentNodeRef)).thenReturn(dossierNodeRef);
		Mockito.when(parapheurService.isActeurCourant(dossierNodeRef, "proprietaire")).thenReturn(true);
		Mockito.when(parapheurService.isActeurCourant(dossierNodeRef, "nonProprietaire")).thenReturn(false);
		
		authority = new ActeurCourantDynamicAuthority();
		authority.setParapheurService(parapheurService);
		authority.afterPropertiesSet();
	}
	
	public void testHasAuthority() {
		boolean returned = authority.hasAuthority(documentNodeRef, "proprietaire");
		Assert.assertTrue(returned);
	}
	
	public void testHasNotAuthority() {
		boolean returned = authority.hasAuthority(dossierNodeRef, "nonProprietaire");
		Assert.assertFalse(returned);
	}

        public void testGetParapheurService() {
            ParapheurService monParapheurService = authority.getParapheurService();
            assertNotNull(monParapheurService);
        }

}
