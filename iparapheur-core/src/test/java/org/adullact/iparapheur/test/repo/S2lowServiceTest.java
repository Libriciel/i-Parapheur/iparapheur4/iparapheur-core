/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.test.repo;

import com.atolcd.parapheur.model.ParapheurException;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.impl.S2lowServiceImpl;
import com.atolcd.parapheur.repo.impl.exceptions.UnknownMailsecTransactionException;
import org.junit.Assert;
import org.adullact.iparapheur.tdt.s2low.TransactionStatus;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Ce test est chargé de vérifier le bon fonctionnement de la classe
 * S2lowServiceImpl.
 * 
 * Le comportement de cette classe est défini par l'interface S2lowService.
 *
 * This test creates a whole content hierarchy, as follow:
 
 * - Company home
 *     - Parapheurs home
 *         - Circuit parapheur 1
 *             - Corbeille en préparation 1
 *                 - Dossier 1
 *                     - Content 1
 *             - Corbeille retournes 1
 *             - Corbeille a traiter 1
 *                   - dossierATeletransmettre
 *                      - contentATeletransmettre
 *                   - dossierNonTermineNode
 *             - Corbeille a archiver 1
 *                 - dossierAArchiver
 *                     - Content a archiver
 *             - Corbeille secretariat 1
 *             - Corbeille virtuelle
 *         - Circuit parapheur 2
 *             - Corbeille en preparation 2
 *             - Corbeille a traiter 2
 *                 - Dossier 2
 *                     - Content 2
 *             - Corbeille secretariat 2
 *                 - Dossier secretariat 2
 *
 * 
 * @see com.atolcd.parapheur.repo.S2lowService
 * @see com.atolcd.parapheur.repo.impl.S2lowServiceImpl
 * @author Vivien Barousse
 */
public class S2lowServiceTest extends BaseParapheurTest {

    private static final DateFormat s2lowDateFormat = new SimpleDateFormat(
            "yyyy-MM-dd");

    private NodeRef folderNode;

    /**
     * Parapheurs home ref
     */
    private NodeRef parapheursHome;

    /**
     * Parapheur 1 ref
     */
    private NodeRef parapheur1;

    /**
     * Parapheur 2 ref
     */
    private NodeRef parapheur2;

    /**
     * Corbeille secrétariat 2 ref
     */
    private NodeRef corbeilleSecretariat2;

    /**
     * Corbeille en préparation 1 ref
     */
    private NodeRef corbeilleEnPreparation1;

    /**
     * Corbeille retournés 1 ref
     */
    private NodeRef corbeilleRetournes1;

     /**
     * Corbeille à traiter 1 ref
     */
    private NodeRef corbeilleATraiter1;

    /**
     * Corbeille secretatiat 1 ref
     */
    private NodeRef corbeilleSecretariat1;

    /**
     * Corbeille a archiver 1 ref
     */
    private NodeRef corbeilleAArchiver1;

    /**
     * Corbeille en préparation 2 ref
     */
    private NodeRef corbeilleEnPreparation2;

    /**
     * Corbeille à traiter 2 ref
     */
    private NodeRef corbeilleATraiter2;

    /**
     * Corbeille virtuelle ref
     */
    private NodeRef corbeilleVirtuelle;

    /**
     * Etape 1 du circuit suivi par le dossier à archiver ref
     */
    private NodeRef etape1DossierAArchiver;

    /**
     * Etape 2 du circuit suivi par le dossier à archiver ref
     */
    private NodeRef etape2DossierAArchiver;

    /**
     * Etape 1 du circuit suivi par le dossier à télétransmettre ref
     */
    private NodeRef etape1DossierATeletransmettre;

    /**
     * Etape 1 du circuit suivi par le dossier à télétransmettre ref
     */
    private NodeRef etape2DossierATeletransmettre;

    /**
     * Dossier non terminé ref
     */
    private NodeRef dossierNonTermineNode;

    /**
     * Dossier à télétransmettre ref
     */
    private NodeRef dossierATeletransmettre;

    /**
     * Dossier à archiver ref
     */
    private NodeRef dossierAArchiver;


    
    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        // Insère un répertoire classique
        folderNode = nodeService.createNode(rootNodeRef, ContentModel.ASSOC_CHILDREN,
                QName.createQName("{test}foldernode"),
                ContentModel.TYPE_FOLDER).getChildRef();

        // Creates S2low configuration file
        NodeRef companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("app:company_home", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        NodeRef dictionaryHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("app:dictionary", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
        // Les parapheurs
        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        parapheur1 = nodeService.createNode(parapheursHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}parapheur1"),
                ParapheurModel.TYPE_PARAPHEUR).getChildRef();

        ArrayList<String> ownersPh1 = new ArrayList<String>();
               ownersPh1.add(tenantService.getDomainUser("proprietaire1", tenantName));

        nodeService.setProperty(parapheur1,
                ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPh1);
        nodeService.setProperty(parapheur1, ContentModel.PROP_TITLE,
                "parapheur1");
        nodeService.setProperty(parapheur1, ContentModel.PROP_NAME,
                "parapheur1");
        parapheur2 = nodeService.createNode(parapheursHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}parapheur2"),
                ParapheurModel.TYPE_PARAPHEUR).getChildRef();

        ArrayList<String> ownersPh2 = new ArrayList<String>();
               ownersPh1.add(tenantService.getDomainUser("proprietaire2", tenantName));

        nodeService.setProperty(parapheur2,
                ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPh2);
        nodeService.setProperty(parapheur2,
                ParapheurModel.PROP_SECRETAIRES, (Serializable) Collections.singletonList(tenantService.getDomainUser("secretaire2", tenantName)));
        nodeService.createAssociation(parapheur2, parapheur1,
                ParapheurModel.ASSOC_HIERARCHIE);

        nodeService.createAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_DELEGATION);

        nodeService.createAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_OLD_DELEGATION);

        // Les corbeilles de mes parapheurs
        corbeilleEnPreparation1 = nodeService.createNode(
                parapheur1, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_EN_PREPARATION,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleRetournes1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_RETOURNES,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleATraiter1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_TRAITER,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleAArchiver1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_ARCHIVER,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleSecretariat1 = nodeService.createNode(
                parapheur1, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_SECRETARIAT, ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleEnPreparation2 = nodeService.createNode(
                parapheur2, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_EN_PREPARATION,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleATraiter2 = nodeService.createNode(parapheur2,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_TRAITER,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleSecretariat2 = nodeService.createNode(
                parapheur2, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_SECRETARIAT, ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleVirtuelle = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}corbeillevirtuellenode"),
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        NodeRef certificats = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("ph:certificats", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        NodeRef configuration = nodeService.createNode(certificats,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:s2low_properties.xml", namespaceService),
                ContentModel.TYPE_CONTENT).getChildRef();

        nodeService.setProperty(configuration, ContentModel.PROP_NAME, "s2low_configuration.xml");

        ContentWriter confWriter = contentService.getWriter(configuration, ContentModel.PROP_CONTENT, true);
        confWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        confWriter.putContent(this.getClass().getResourceAsStream("/alfresco/module/parapheur/s2low/s2low-configuration.xml"));

        NodeRef classifications = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("ph:slow", namespaceService),
                ContentModel.TYPE_CONTENT).getChildRef();

        nodeService.setProperty(classifications, ContentModel.PROP_NAME, "parapheur-s2low-classification.xml");

        ContentWriter classificationsWriter = contentService.getWriter(classifications, ContentModel.PROP_CONTENT, true);
        classificationsWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        classificationsWriter.putContent(this.getClass().getResourceAsStream("/alfresco/module/parapheur/s2low/s2low-classification.xml"));

        NodeRef certificat = nodeService.createNode(certificats,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:antisocial@trust.fr.p12", namespaceService),
                ContentModel.TYPE_CONTENT).getChildRef();

        nodeService.setProperty(certificat, ContentModel.PROP_NAME, "antisocial@trust.fr.p12");
        ContentWriter certWriter = contentService.getWriter(certificat, ContentModel.PROP_CONTENT, true);
        certWriter.setMimetype(MimetypeMap.MIMETYPE_BINARY);
        certWriter.putContent(this.getClass().getResourceAsStream("/alfresco/module/parapheur/s2low/antisocial@trust.fr.p12"));

        // Commit is nedded since S2lowServiceImpl starts some transactions itself
        setComplete();
        endTransaction();
        startNewTransaction();

        // Insère un dossier prêt à être télétransmis
        dossierATeletransmettre = nodeService.createNode(corbeilleATraiter1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}dossiernode"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossierATeletransmettre, ParapheurModel.PROP_TERMINE, true);
        NodeRef contentATeletransmettre = nodeService.createNode(dossierATeletransmettre,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}dossiernodecontent"),
                ContentModel.TYPE_CONTENT).getChildRef();
        ContentWriter writer = contentService.getWriter(contentATeletransmettre,
                ContentModel.PROP_CONTENT, true);
        writer.setMimetype(MimetypeMap.MIMETYPE_PDF);
        writer.putContent(this.getClass().getResourceAsStream(
                "/samples/document.pdf"));

        //Le circuit du dossier à télétransmettre
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, true);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);
        etape1DossierATeletransmettre = nodeService.createNode(dossierATeletransmettre,
                ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_TDT);
        etape2DossierATeletransmettre = nodeService.createNode(etape1DossierATeletransmettre,
                ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        // Insère un dossier déjà télétransmis dans Alfresco
        dossierAArchiver = nodeService.createNode(corbeilleAArchiver1,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("{test}archivenode"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_TERMINE, true);
        nodeService.addAspect(dossierAArchiver, ParapheurModel.ASPECT_S2LOW,
                Collections.<QName, Serializable>emptyMap());
        nodeService.setProperty(dossierAArchiver,
                ParapheurModel.PROP_TRANSACTION_ID, 7921);

        //Le circuit du dossier à archiver
        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, true);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);
        etape1DossierAArchiver = nodeService.createNode(dossierAArchiver,
                ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_ARCHIVAGE);
        etape2DossierAArchiver = nodeService.createNode(etape1DossierAArchiver,
                ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();


        // Insère un dossier non terminé
        dossierNonTermineNode = nodeService.createNode(corbeilleATraiter1,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("{test}dossiernontermine"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossierNonTermineNode,
                ParapheurModel.PROP_TERMINE, false);
    }

    /**
     * Teste si la méthode getXadesSignatureProperties renvoie correctement les
     * propriétés minimum
     */
    public void testGetXadesSignatureProperties() {
        Properties prop = s2lowService.getXadesSignatureProperties(null);
        Assert.assertNotNull(prop.getProperty("pPolicyIdentifierID"));
        Assert.assertNotNull(prop.getProperty("pPolicyIdentifierDescription"));
        Assert.assertNotNull(prop.getProperty("pPolicyDigest"));
        Assert.assertNotNull(prop.getProperty("pSPURI"));
        Assert.assertNotNull(prop.getProperty("pCity"));
        Assert.assertNotNull(prop.getProperty("pPostalCode"));
        Assert.assertNotNull(prop.getProperty("pCountryName"));
        Assert.assertNotNull(prop.getProperty("pClaimedRole"));
    }

    public void testGetS2lowActesClassificationNodeRef() {
        NodeRef returned = s2lowService.getS2lowActesClassificationNodeRef();
        Assert.assertNotNull(returned);
    }

    /**
     * Vérifie que la méthode getS2lowActesClassificationNodeRef() renvoie bien
     * null si jamais le fichier de configuration n'existe pas
     */
    public void testGetS2lowActesClassificationNodeRefNoConf() {
        NodeRef returned = s2lowService.getS2lowActesClassificationNodeRef();
        Assert.assertNotNull(returned);
        nodeService.deleteNode(returned);
        Assert.assertNull(s2lowService.getS2lowActesClassificationNodeRef());
    }

    /**
     * Teste si la méthode getS2lowActesNatures renvoie correctement la liste
     * des natures d'actes en fonction d'un fichier XML d'exemple.
     */
    public void testGetS2lowActesNatures() throws IOException {
        Map<Integer, String> returned = s2lowService.getS2lowActesNatures();
        Assert.assertNotNull(returned);
        Assert.assertTrue(returned.size() > 0);
    }

    /**
     * Teste si la méthode getS2lowActesClassifications renvoie correctement la
     * liste des classifications en fonction d'un fichier XML d'exemple.
     */
    public void testGetS2lowActesClassifications() throws IOException {
        Map<String, String> returned = s2lowService.getS2lowActesClassifications();
        Assert.assertNotNull(returned);
        Assert.assertTrue(returned.size() > 0);
    }

    /**
     * Teste l'envoi d'un dossier à la plateforme S2low.
     *
     * Les vérifications suivantes sont faites :
     * <ul>
     * <li>Envoi du dossier à S2low sans exceptions</li>
     * <li>Ajout de l'aspect "S2low" sur le dossier</li>
     * <li>Ajout de la propriété "Transaction ID" sur le dossier</li>
     * </ul>
     */
    public void testEnvoiS2lowActes() throws IOException, ParapheurException {
        s2lowService.envoiS2lowActes(dossierATeletransmettre, "6", "8-2", "UT" + System.currentTimeMillis(), "Test unitaire", s2lowDateFormat.format(new Date()));


        String currentValidator = parapheurService.getCurrentValidator(dossierATeletransmettre);

        boolean hasAspect = nodeService.hasAspect(dossierATeletransmettre,
                ParapheurModel.ASPECT_S2LOW);
        Assert.assertTrue(hasAspect);

        Assert.assertEquals("admin@unit-tests-tenant", currentValidator);
        Integer transId = (Integer) nodeService.getProperty(dossierATeletransmettre,
                ParapheurModel.PROP_TRANSACTION_ID);
        Assert.assertNotNull(transId);
    }

    /**
     * Teste si l'envoi d'une archive à la plateforme S2low lève une exception
     * comme attendu.
     *
     * L'envoi à la plateforme S2low d'un fichier déjà télétransmit doit lever
     * une exception de type IllegalArgumentException.
     */
    public void testEnvoiArchiveS2lowActes() throws IOException, ParapheurException {
        try {
            s2lowService.envoiS2lowActes(dossierAArchiver, "6", "8-2", "UT" + System.currentTimeMillis(), "Test unitaire",
                    s2lowDateFormat.format(new Date()));
            Assert.fail();
        } catch (IllegalArgumentException e) {
            // Une exceptoion doit être levée !
        }
    }

    /**
     * Envoie un dossier de type différent de ParapheurModel.TYPE_DOSSIER, afin
     * de vérifier que le service lève correctement une exception
     */
    public void testEnvoiFolderS2lowActes() throws IOException, ParapheurException {
        try {
            s2lowService.envoiS2lowActes(folderNode, "6", "8-2", "UT" + System.currentTimeMillis(), "Test unitaire",
                    s2lowDateFormat.format(new Date()));
            Assert.fail();
        } catch (IllegalArgumentException e) {
            // Une exception doit être levée !
        }
    }

    /**
     * Essaye de récupérer des informations sur l'état d'un dossier auprès de
     * S2low
     */
    public void testGetInfosS2low() throws IOException, ParapheurException {
        
        boolean hasS2LOWAspect = nodeService.hasAspect(dossierATeletransmettre, ParapheurModel.ASPECT_S2LOW);
        
        // Si le dossier n'a pas encore été télétransmis on l'envoie à S2LOW.
        if (!hasS2LOWAspect) {
            s2lowService.envoiS2lowActes(dossierATeletransmettre, "6", "8-2", "UT" + System.currentTimeMillis(), "Test unitaire",
                    s2lowDateFormat.format(new Date()));
        }

        int result = s2lowService.getInfosS2low(dossierATeletransmettre);
        // Ce dossier est connu, l'acquittement a été reçu

        /* EPA: On ne peut pas s'assurer que le dossier a bien recu un
         * STATUS_ACK (~2minutes sur la plateforme de test) on ne peut que
         * s'assurer que s2low ne retourne pas une erreur.
         *
         */
        Assert.assertTrue(result != TransactionStatus.STATUS_ERROR);
    }

    /**
     * Essaye de récupérer des informations sur l'état d'un dossier non terminé
     * (et donc, non télétransmis) auprès de S2low.
     *
     * Ce test doit échouer.
     */
    public void testGetInfosS2lowDossierNonTermine() throws IOException {
        try {
            s2lowService.getInfosS2low(dossierNonTermineNode);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye de récupérer des informations sur l'état d'un dossier non
     * télétransmis.
     *
     * Ce test doit échouer
     */
    public void testGetInfosS2lowDossierNonTeletransmis() throws IOException {
        try {
            s2lowService.getInfosS2low(dossierATeletransmettre);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Met à jour l'URL d'archivage d'un dossier aurpsè de la plateforme S2low
     */
    public void testSetS2lowActesArchiveURL() throws IOException, ParapheurException {
        boolean hasS2LOWAspect = nodeService.hasAspect(dossierATeletransmettre, ParapheurModel.ASPECT_S2LOW);
          // Si le dossier n'a pas encore été télétransmis on l'envoie à S2LOW.
        if (!hasS2LOWAspect) {
            s2lowService.envoiS2lowActes(dossierATeletransmettre, "6", "8-2", "UT" + System.currentTimeMillis(), "Test unitaire",
                    s2lowDateFormat.format(new Date()));
        }

        String result = s2lowService.setS2lowActesArchiveURL(dossierATeletransmettre);
        Assert.assertNotNull(result);
    }

    /**
     * Essaye de mettre à jour l'URL d'archivage d'un dossier non télétransmis.
     *
     * Ce test doit lever une exception de type IllegalArgumentException
     */
    public void testSetS2lowActesArchiveURLDossierNonTeletransmis()
            throws IOException {
        try {
            s2lowService.setS2lowActesArchiveURL(dossierATeletransmettre);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye de mettre à jour l'URL d'archivage d'un dossier non terminé (et
     * donc non télétransmis).
     *
     * Ce test doit lever une exception de type IllegalArgumentException
     */
    public void testSetS2lowActesArchiveURLDossierNonTermine()
            throws IOException {
        try {
            s2lowService.setS2lowActesArchiveURL(dossierNonTermineNode);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    public void testGetSecureMailVersion() {
        String version = null;
        try {
            version = s2lowService.getSecureMailVersion();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull("Impossible de recuperer la version de s2low mail.", version);
        Assert.assertEquals("1.2", version);
    }

    public void testSendSecureMail() {
        List<String> mailto = new ArrayList<String>();
        mailto.add("e.peralta@adullact-projet.coop");
        mailto.add("emmanuel.peralta@ringum.net");
        int id_mail = -2;

        try {
            id_mail = s2lowService.sendSecureMail(mailto, "~ Objet ~", "Message");
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }

        assert(id_mail > 0);

    }

    public void testGetSecureMailCount() {

        int count = 0;
        try {
            count = s2lowService.getSecureMailCount();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert (count > 0);

    }

    public void testGetDetailMail() {
        S2lowServiceImpl.SecureMailDetail detail = null;
        try {
            Map<Integer, S2lowServiceImpl.SecureMailDetail> mails = s2lowService.getSecureMailList(1, 1);
            int mail_id = mails.keySet().iterator().next();

            detail = s2lowService.getSecureMailDetail(mail_id);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnknownMailsecTransactionException e) {
            e.printStackTrace();
        }
        assert(detail != null);
    }

    public void testDeleteMail() {
        boolean isDeleted = false;
        try {

            Map<Integer, S2lowServiceImpl.SecureMailDetail> mails = s2lowService.getSecureMailList(1, 1);

            int mail_id = mails.keySet().iterator().next();
            isDeleted = s2lowService.deleteSecureMail(mail_id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert(isDeleted);
    }

}
