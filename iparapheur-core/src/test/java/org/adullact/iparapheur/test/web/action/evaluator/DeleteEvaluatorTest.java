package org.adullact.iparapheur.test.web.action.evaluator;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.DeleteEvaluator;

public class DeleteEvaluatorTest extends WebUIBaseTest {

    private NodeRef nodeRef;
    private NodeRef corbeilleRef;
    private NodeRef parapheurRef;
    private DeleteEvaluator evaluator;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
        corbeilleRef = new NodeRef("workspace://SpacesStore/corbeilleRef");
        parapheurRef = new NodeRef("workspace://SpacesStore/parapheurRef");

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);

        Mockito.when(parapheurService.isEmis(nodeRef)).thenReturn(false);
        Mockito.when(parapheurService.getParentCorbeille(nodeRef)).thenReturn(
                corbeilleRef);
        Mockito.when(nodeService.getPrimaryParent(corbeilleRef)).thenReturn(
                new ChildAssociationRef(ContentModel.ASSOC_CONTAINS,
                parapheurRef, ParapheurModel.NAME_RETOURNES,
                corbeilleRef));
        
        Mockito.when(nodeService.exists(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(true);

        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);

        evaluator = new DeleteEvaluator();
    }

    public void testEvaluateNull() {
        boolean returned = evaluator.evaluate(null);
        Assert.assertFalse(returned);
    }
    
    public void testEvaluateNonExistentNodeRef() {
        boolean returned = evaluator.evaluate(new Node(new NodeRef("workspace://SpacesStore/notInRepoRef")));
        Assert.assertFalse(returned);
    }
    
    public void testEvaluateExistingNodeNotDossier() {
        NodeRef myNodeRef = new NodeRef("workspace://SpacesStore/notInRepoRef");
        
        Mockito.when(nodeService.exists(myNodeRef)).thenReturn(true);
        
        boolean returned = evaluator.evaluate(new Node(myNodeRef));
        Assert.assertFalse(returned);
    }
    
    public void testEvaluate() {
        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateEmis() {
        Mockito.when(parapheurService.isEmis(nodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateEmisEtRetourne() {
        Mockito.when(parapheurService.isEmis(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateSecretariat() {
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateObject() {

        Object monNode = new Node(nodeRef);
        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }
}
