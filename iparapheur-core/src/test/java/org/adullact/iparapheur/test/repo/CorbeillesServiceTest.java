/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 *
 * @author niko
 */
package org.adullact.iparapheur.test.repo;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.repo.impl.ParapheurServiceImpl;
import org.junit.Assert;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationException;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.mockito.Mockito;

import java.io.Serializable;
import java.util.*;

/**
 * Test class for the ParapheurServiceImpl class.
 *
 * This test creates a whole content hierarchy, as follow:
 * - Company home
 *     - Parapheurs home
 *         - Circuit parapheur 1
 *             - Corbeille en préparation 1
 *                 - Dossier 1
 *                     - Content 1
 *             - Corbeille retournes 1
 *             - Corbeille a traiter 1
 *             - Corbeille a archiver 1
 *                 - Dossier a archiver
 *                     - Content a archiver
 *             - Corbeille secretariat 1
 *             - Corbeille virtuelle
 *                 - Dossier 1
 *         - Circuit parapheur 2
 *             - Corbeille en preparation 2
 *             - Corbeille a traiter 2
 *                 - Dossier 2
 *                     - Content 2
 *             - Corbeille secretariat 2
 *                 - Dossier secretariat 2
 *     - Archives home
 *     - Dictionary home
 *         - Workflows home
 *             - Public workflow
 *             - Workflow proprietaire 1
 *             - Worflow proprietaire 2
 *
 * S2lowService is mocked, so calls to the S2low platform aren't actually
 * performed.
 *
 * ActionService is also mocked.
 *
 * Mocks are created using the Mockito mocking framework. For more information
 * on how to use Mockito, please refer to the official
 * <a href="http://mockito.org/">Mockito</a> documentation.
 *
 * @author Vivien Barousse
 * @see Mockito
 */
public class CorbeillesServiceTest extends BaseParapheurTest {

    private CorbeillesService corbeillesService;
    
    /**
     * Mocked ActionService
     * @see ActionService
     */
    private ActionService mockedActionService;
    /**
     * Mocked S2lowService
     * @see S2lowService
     */
    private S2lowService mockedS2lowService;
    /**
     * Company home ref
     */
    private NodeRef companyHome;
    private NodeRef archives_configuration;
    /**
     * Parapheurs home ref
     */
    private NodeRef parapheursHome;
    /**
     * Archives home ref
     */
    private NodeRef archivesHome;
    /**
     * Dictionary home ref
     */
    private NodeRef dictionaryHome;
    /**
     * Workflows home ref
     */
    private NodeRef workflowsHome;
    /**
     * Parapheur 1 ref
     */
    private NodeRef parapheur1;
    /**
     * Parapheur 2 ref
     */
    private NodeRef parapheur2;
    /**
     * Corbeille en préparation 1 ref
     */
    private NodeRef corbeilleEnPreparation1;
    /**
     * Corbeille retournes 1 ref
     */
    private NodeRef corbeilleRetournes1;
    /**
     * Corbeille a traiter 1 ref
     */
    private NodeRef corbeilleATraiter1;
    /**
     * Corbeille a archiver 1 ref
     */
    private NodeRef corbeilleAArchiver1;
    /**
     * Corbeille secretariat 1 ref
     */
    private NodeRef corbeilleSecretariat1;
    /**
     * Corbeille en preparation 2 ref
     */
    private NodeRef corbeilleEnPreparation2;
    /**
     * Corbeille a traiter 2 ref
     */
    private NodeRef corbeilleATraiter2;
    /**
     * Corbeille Secretariat 2 ref
     */
    private NodeRef corbeilleSecretariat2;
    /**
     * Corbeille virtuelle ref
     */
    private NodeRef corbeilleVirtuelle;
    private NodeRef corbeilleEnCours1;
    private NodeRef corbeilleAVenir1;
    private NodeRef corbeilleDossiersDelegues1;
    private NodeRef corbeilleAImprimer1;
    private NodeRef corbeilleEnRetard1;
    private NodeRef corbeilleRecuperable1;
    private NodeRef corbeilleEnCours2;
    private NodeRef corbeilleAVenir2;
    private NodeRef corbeilleDossiersDelegues2;
    private NodeRef corbeilleAImprimer2;
    private NodeRef corbeilleEnRetard2;
    private NodeRef corbeilleRecuperable2;
    /**
    /**
     * Dossier 1 ref
     */
    private NodeRef dossier1;
    /**
     * Dossier 2 ref
     */
    private NodeRef dossier2;
    /**
     * Dossier secretariat 2 ref
     */
    private NodeRef dossierSecretariat2;
    /**
     * Dossier a archiver ref
     */
    private NodeRef dossierAArchiver;
    
    private NodeRef dossierVirtuel;
    /**
     * Content 1 ref
     */
    private NodeRef content1;
    private NodeRef contentSecretariat;
    /**
     * Content 2 ref
     */
    private NodeRef content2;
    /**
     * Content a archiver ref
     */
    private NodeRef contentAArchiver;
    private NodeRef contentVirtuel;    
    private NodeRef circuitEtape1;
    private NodeRef circuitEtape2;
    private NodeRef circuitEtape3;
    private NodeRef circuitEtape4;
    private NodeRef etape1DossierSecretariat;
    private NodeRef etape2DossierSecretariat;
    private NodeRef etape1DossierAArchiver;
    private NodeRef etape2DossierAArchiver;
    /**
     * Public workflow ref
     */
    private NodeRef publicWorkflow;
    /**
     * Workflow proprietaire 1 ref
     */
    private NodeRef workflowProprietaire1;
    /**
     * Workflow proprietaire 2 ref
     */
    private NodeRef workflowProprietaire2;

    /**
     * Initializes the hierarchy described in class header. Mock S2low service.
     *
     * @throws Exception
     */
    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();
        
        
        corbeillesService = (CorbeillesService) applicationContext.getBean("corbeillesService");        

        mockedActionService = Mockito.mock(ActionService.class);
        Mockito.when(mockedActionService.createAction(Mockito.anyString())).thenReturn(actionService.createAction("parapheur-mail"));
        ((ParapheurServiceImpl) parapheurService).setActionService(mockedActionService);

        mockedS2lowService = Mockito.mock(S2lowService.class);
        Mockito.when(mockedS2lowService.isEnabled()).thenReturn(true);
        Mockito.when(
                mockedS2lowService.setS2lowActesArchiveURL(Mockito.any(NodeRef.class))).thenReturn(
                "http://example.com/url-archivage/");
        Mockito.when(
                mockedS2lowService.getInfosS2low(Mockito.any(NodeRef.class))).thenReturn(4);
        ((ParapheurServiceImpl) parapheurService).setS2lowService(mockedS2lowService);
        try {
            authenticationService.createAuthentication(tenantService.getDomainUser("proprietaire1", tenantName), "secret".toCharArray());
            authenticationService.createAuthentication(tenantService.getDomainUser("proprietaire2", tenantName), "secret".toCharArray());
            authenticationService.createAuthentication(tenantService.getDomainUser("secretaire2", tenantName), "secret".toCharArray());
        } catch (AuthenticationException e) {

        }

        NodeRef prop1 = personService.getPerson(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(prop1, ContentModel.PROP_FIRSTNAME, "Prop");
        nodeService.setProperty(prop1, ContentModel.PROP_LASTNAME, "Ietaire");
        nodeService.setProperty(prop1, ContentModel.PROP_EMAIL, "proprietaire1@example.com");







        companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.company_home.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        archivesHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.archives.childname"),
                namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();


        permissionService.setInheritParentPermissions(archivesHome, false);
        permissionService.setPermission(archivesHome, "GROUP_EVERYONE", "Contributor", true);

        dictionaryHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.dictionary.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        archives_configuration = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("ph:archives_configuration",
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();
        ContentWriter contentWriter = contentService.getWriter(archives_configuration, ContentModel.PROP_CONTENT, true);
        contentWriter.putContent("archive.tamponActes.visible=true\n"
                + "archive.tamponActes.text=Acquitt\u00E9 en PREFECTURE le {0,date,dd/MM/yyyy}");

        workflowsHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.savedworkflows.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        parapheur1 = nodeService.createNode(parapheursHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}parapheur1"),
                ParapheurModel.TYPE_PARAPHEUR).getChildRef();

        ArrayList<String> ownersPh1 = new ArrayList<String>();
        ownersPh1.add(tenantService.getDomainUser("proprietaire1", tenantName));
        
        String ownerPh1 = tenantService.getDomainUser("proprietaire1", tenantName);
        
        nodeService.setProperty(parapheur1,
                ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPh1);
        nodeService.setProperty(parapheur1,
                ParapheurModel.PROP_PROPRIETAIRE_PARAPHEUR, ownerPh1);
        
        nodeService.setProperty(parapheur1, ContentModel.PROP_TITLE,
                "parapheur1");
        nodeService.setProperty(parapheur1, ContentModel.PROP_NAME,
                "parapheur1");
        parapheur2 = nodeService.createNode(parapheursHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}parapheur2"),
                ParapheurModel.TYPE_PARAPHEUR).getChildRef();

        ArrayList<String> ownersPh2 = new ArrayList<String>();
        ownersPh2.add(tenantService.getDomainUser("proprietaire2", tenantName));
        nodeService.setProperty(parapheur2, ContentModel.PROP_TITLE, "parapheur 2");
        
        nodeService.setProperty(parapheur2,
                ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPh2);
        nodeService.setProperty(parapheur2,
                ParapheurModel.PROP_SECRETAIRES, (Serializable) Collections.singletonList(tenantService.getDomainUser("secretaire2", tenantName)));
        nodeService.createAssociation(parapheur2, parapheur1,
                ParapheurModel.ASSOC_HIERARCHIE);

        nodeService.createAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_DELEGATION);

        nodeService.createAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_OLD_DELEGATION);

        corbeilleEnPreparation1 = nodeService.createNode(
                parapheur1, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_EN_PREPARATION,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleRetournes1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_RETOURNES,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleATraiter1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_TRAITER,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleAArchiver1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_ARCHIVER,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleSecretariat1 = nodeService.createNode(
                parapheur1, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_SECRETARIAT, ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleEnPreparation2 = nodeService.createNode(
                parapheur2, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_EN_PREPARATION,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleATraiter2 = nodeService.createNode(parapheur2,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_TRAITER,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleSecretariat2 = nodeService.createNode(
                parapheur2, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_SECRETARIAT, ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleVirtuelle = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}corbeillevirtuellenode"),
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleEnCours1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_EN_COURS,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleAVenir1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_VENIR,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();
        
        corbeilleDossiersDelegues1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_DOSSIERS_DELEGUES,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleAImprimer1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_IMPRIMER,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleEnRetard1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_EN_RETARD,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleRecuperable1 = nodeService.createNode(parapheur1,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_RECUPERABLES,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleEnCours2 = nodeService.createNode(parapheur2,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_EN_COURS,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleAVenir2 = nodeService.createNode(parapheur2,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_VENIR,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();
        
        corbeilleDossiersDelegues2 = nodeService.createNode(parapheur2,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_DOSSIERS_DELEGUES,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleAImprimer2 = nodeService.createNode(parapheur2,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_IMPRIMER,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleEnRetard2 = nodeService.createNode(parapheur2,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_EN_RETARD,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleRecuperable2 = nodeService.createNode(parapheur2,
                ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_RECUPERABLES,
                ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        dossier1 = nodeService.createNode(
                corbeilleEnPreparation1, ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}dossier1"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossier1, ParapheurModel.PROP_TERMINE,
                false);
        nodeService.setProperty(dossier1, ContentModel.PROP_NAME,
                "dossier1");
        nodeService.setProperty(dossier1,
                ParapheurModel.PROP_LISTE_DIFFUSION, (Serializable) Arrays.asList(parapheur1, parapheur2));
        nodeService.setProperty(dossier1, ParapheurModel.PROP_SIGNATURE_FORMAT,
                "PKCS#7/single");
        nodeService.setProperty(dossier1,
                ParapheurModel.PROP_SIGNATURE_PAPIER, false);
        nodeService.setProperty(dossier1,
                ParapheurModel.PROP_RECUPERABLE, false);
        dossier2 = nodeService.createNode(corbeilleATraiter2,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}dossier2"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossier2, ParapheurModel.PROP_TERMINE,
                false);
        nodeService.setProperty(dossier2,
                ParapheurModel.PROP_SIGNATURE_PAPIER, false);
        nodeService.setProperty(dossier2,
                ParapheurModel.PROP_RECUPERABLE, true);

        dossierSecretariat2 = nodeService.createNode(
                corbeilleSecretariat2, ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}dossierSecretariat2"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.addAspect(dossierSecretariat2,
                ParapheurModel.ASPECT_SECRETARIAT, Collections.<QName, Serializable>emptyMap());
        nodeService.setProperty(dossierSecretariat2,
                ParapheurModel.PROP_TERMINE, false);

        contentSecretariat = nodeService.createNode(dossierSecretariat2,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}contentSecretariat"), ContentModel.TYPE_CONTENT).getChildRef();
        nodeService.addAspect(contentSecretariat, ParapheurModel.ASPECT_LU,
                Collections.<QName, Serializable>emptyMap());

        dossierAArchiver = nodeService.createNode(corbeilleAArchiver1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}dossierAArchiver"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_TERMINE,
                true);
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_TYPE_METIER,
                "metier");
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_SOUSTYPE_METIER,
                "defaut1");

        content1 = nodeService.createNode(dossier1,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}content1"), ContentModel.TYPE_CONTENT).getChildRef();
        nodeService.addAspect(content1, ParapheurModel.ASPECT_LU,
                Collections.<QName, Serializable>emptyMap());
        content2 = nodeService.createNode(dossier2,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}content2"), ContentModel.TYPE_CONTENT).getChildRef();
        nodeService.addAspect(content2, ParapheurModel.ASPECT_LU,
                Collections.<QName, Serializable>emptyMap());

        contentAArchiver = nodeService.createNode(dossierAArchiver,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}contentAArchiver"),
                ContentModel.TYPE_CONTENT).getChildRef();
        ContentWriter writer = contentService.getWriter(contentAArchiver,
                ContentModel.PROP_CONTENT, true);
        writer.setMimetype(MimetypeMap.MIMETYPE_PDF);
        writer.putContent(this.getClass().getResourceAsStream(
                "/samples/document.pdf"));
        
        dossierVirtuel = nodeService.createNode(corbeilleVirtuelle,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}dossierVirtuel"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossierVirtuel, ParapheurModel.PROP_TERMINE,
                true);
        nodeService.setProperty(dossierVirtuel, ParapheurModel.PROP_TYPE_METIER,
                "metier");
        nodeService.setProperty(dossierVirtuel, ParapheurModel.PROP_SOUSTYPE_METIER,
                "defaut1");
        
        contentVirtuel = nodeService.createNode(dossierVirtuel,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}contentVirtuel"),
                ContentModel.TYPE_CONTENT).getChildRef();
    
        
        //Dossier 1
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);
        circuitEtape1 = nodeService.createNode(dossier1,
                ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();
        nodeService.setProperty(circuitEtape1,
                ParapheurModel.PROP_ANNOTATION_PRIVEE, "annotationprivee1");
        nodeService.setProperty(circuitEtape1,
                ParapheurModel.PROP_LISTE_DIFFUSION, (Serializable) Arrays.asList(parapheur1, parapheur2));        

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_SIGNATURE);
        circuitEtape2 = nodeService.createNode(circuitEtape1,
                ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();
        nodeService.setProperty(circuitEtape2, ParapheurModel.PROP_ANNOTATION,
                "annotationpublique");
        nodeService.setProperty(circuitEtape2,
                ParapheurModel.PROP_LISTE_DIFFUSION, (Serializable) Arrays.asList(parapheur1, parapheur2));        

        //Dossier 2
        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, true);
        properties.put(ParapheurModel.PROP_ANNOTATION_PRIVEE,
                "annotationpriveeprecedente");
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);
        circuitEtape3 = nodeService.createNode(dossier2,
                ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ANNOTATION, "annotationpublique");
        properties.put(ParapheurModel.PROP_ANNOTATION_PRIVEE,
                "annotationprivee");
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_SIGNATURE);
        circuitEtape4 = nodeService.createNode(circuitEtape3,
                ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, true);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);
        etape1DossierSecretariat = nodeService.createNode(
                dossierSecretariat2,
                ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_SIGNATURE);
        etape2DossierSecretariat = nodeService.createNode(
                etape1DossierSecretariat,
                ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, true);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);
        etape1DossierAArchiver = nodeService.createNode(dossierAArchiver,
                ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_ARCHIVAGE);
        etape2DossierAArchiver = nodeService.createNode(etape1DossierAArchiver,
                ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        publicWorkflow = nodeService.createNode(workflowsHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}publicWorkflow"),
                ParapheurModel.TYPE_SAVED_WORKFLOW).getChildRef();
        nodeService.setProperty(publicWorkflow, ContentModel.PROP_NAME,
                "workflowPublic");

        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        workflowProprietaire1 = nodeService.createNode(workflowsHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}proprietaire1workflow"),
                ParapheurModel.TYPE_SAVED_WORKFLOW).getChildRef();
        nodeService.setProperty(workflowProprietaire1, ContentModel.PROP_NAME,
                "workflowProp1");
        nodeService.setProperty(workflowProprietaire1, ContentModel.PROP_CREATOR,
                tenantService.getDomainUser("proprietaire1", tenantName));

        ContentWriter proprietaire1WorkflowWriter = contentService.getWriter(
                workflowProprietaire1, ContentModel.PROP_CONTENT, true);
        proprietaire1WorkflowWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        proprietaire1WorkflowWriter.putContent("<circuit>" + "<validation>" + "<etape>" + parapheur1.toString() + "</etape>" + "<etape>" + parapheur2.toString() + "</etape>" + "</validation>" + "<notification>" + "<etape>" + parapheur2.toString() + "</etape>" + "</notification>" + "</circuit>");
        nodeService.addAspect(workflowProprietaire1, ParapheurModel.ASPECT_PRIVATE_WORKFLOW, null);
        nodeService.setProperty(workflowProprietaire1, ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS, (Serializable) Collections.singletonList(parapheur1.toString()));
        nodeService.setProperty(workflowProprietaire1, ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_GROUPS, (Serializable) Collections.emptyList());

        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire2", tenantName));
        workflowProprietaire2 = nodeService.createNode(workflowsHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}proprietaire2workflow"),
                ParapheurModel.TYPE_SAVED_WORKFLOW).getChildRef();
        nodeService.setProperty(workflowProprietaire2, ContentModel.PROP_NAME,
                "workflowProp2");
        nodeService.setProperty(workflowProprietaire2, ContentModel.PROP_CREATOR,
                tenantService.getDomainUser("proprietaire2", tenantName));

        nodeService.addAspect(workflowProprietaire2, ParapheurModel.ASPECT_PRIVATE_WORKFLOW, null);
        nodeService.setProperty(workflowProprietaire2, ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS, (Serializable) Collections.singletonList(parapheur2.toString()));
        nodeService.setProperty(workflowProprietaire2, ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_GROUPS, (Serializable) Collections.emptyList());


        authenticationComponent.setCurrentUser(tenantService.getDomainUser("admin", tenantName));
        NodeRef typesFolderNode = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metiertype", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
        NodeRef typesNode = nodeService.createNode(typesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:TypesMetier.xml"),
                ContentModel.TYPE_CONTENT).getChildRef();
        ContentWriter typesNodeWriter = contentService.getWriter(typesNode,
                ContentModel.PROP_CONTENT, true);
        typesNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        typesNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/TypesMetier.xml"));

        NodeRef sousTypesFolderNode = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metiersoustype", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
        NodeRef sousTypesNode = nodeService.createNode(sousTypesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metier.xml", namespaceService), ContentModel.TYPE_CONTENT).getChildRef();
        nodeService.setProperty(sousTypesNode, ContentModel.PROP_NAME,
                "metier.xml");
        ContentWriter sousTypesNodeWriter = contentService.getWriter(
                sousTypesNode, ContentModel.PROP_CONTENT, true);
        sousTypesNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/SousTypesMetier.xml"));

        NodeRef sousTypesDefautNode = nodeService.createNode(sousTypesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:defaut.xml", namespaceService), ContentModel.TYPE_CONTENT).getChildRef();
        nodeService.setProperty(sousTypesDefautNode, ContentModel.PROP_NAME,
                "defaut.xml");
        ContentWriter sousTypesNodeDefautWriter = contentService.getWriter(
                sousTypesDefautNode, ContentModel.PROP_CONTENT, true);
        sousTypesNodeDefautWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesNodeDefautWriter.putContent(this.getClass().getResourceAsStream("/samples/defaut.xml"));
    }
    
    public void testGetDossiersFromCorbeilleVirtuelle(){
        nodeService.createAssociation(
                corbeilleVirtuelle,
                dossier1,
                ParapheurModel.ASSOC_VIRTUALLY_CONTAINS); 
        List<NodeRef> dossiers =  corbeillesService.getDossiersFromCorbeilleVirtuelle(parapheur1, QName.createQName("{test}corbeillevirtuellenode"));       
        Assert.assertEquals(1, dossiers.size());
    }
    
    public void testInsertIntoCorbeilleAImprimer(){
        nodeService.setProperty(dossier1, ParapheurModel.PROP_TERMINE, false);
        nodeService.setProperty(circuitEtape1, ParapheurModel.PROP_EFFECTUEE, true);
        nodeService.setProperty(circuitEtape2, ParapheurModel.PROP_ACTION_DEMANDEE , EtapeCircuit.ETAPE_SIGNATURE);
        nodeService.setProperty(dossier1, ParapheurModel.PROP_SIGNATURE_PAPIER, true);
        Boolean result = false;
        corbeillesService.insertIntoCorbeilleAImprimer(dossier1);
        List<AssociationRef> assocs =  nodeService.getTargetAssocs(corbeilleAImprimer2, RegexQNamePattern.MATCH_ALL);
        for (AssociationRef assoc : assocs) {
            if (assoc.getTypeQName().equals(ParapheurModel.ASSOC_VIRTUALLY_CONTAINS)) {
                result = true;
            }
        }
        Assert.assertTrue(result);        
    }
    
    
}


