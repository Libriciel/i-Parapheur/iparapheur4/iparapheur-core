package org.adullact.iparapheur.test.web.action.evaluator;

import org.junit.Assert;
import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.web.action.evaluator.InfosS2lowEvaluator;
import java.util.Collections;
import java.util.List;
import com.atolcd.parapheur.repo.EtapeCircuit;
import org.adullact.iparapheur.repo.jscript.ScriptEtapeCircuitImpl;

public class InfosS2lowEvaluatorTest extends WebUIBaseTest {

    private NodeRef nodeRef;
    private InfosS2lowEvaluator evaluator;
    private EtapeCircuit proprietaireEtape;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        List<EtapeCircuit> circuit = null;

        proprietaireEtape = Mockito.mock(EtapeCircuit.class);

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);
        s2lowService = Mockito.mock(S2lowService.class);

        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(circuit);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit)).thenReturn(proprietaireEtape);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_TDT);


        Mockito.when(s2lowService.isEnabled()).thenReturn(true);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(nodeRef)).thenReturn(
                new ScriptEtapeCircuitImpl("PARAPHEUR", nodeRef, "TDT", Collections.<NodeRef>emptySet()));
        Mockito.when(nodeService.hasAspect(nodeRef, ParapheurModel.ASPECT_S2LOW)).thenReturn(true);

        setUpManagedBean("S2lowService", s2lowService);
        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);

        evaluator = new InfosS2lowEvaluator();
    }

    public void testEvaluate() {
        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateS2lowServiceDisabled() {
        Mockito.when(s2lowService.isEnabled()).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateDossierNonTdt() {
        List<EtapeCircuit> circuit = null;
        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(circuit);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit)).thenReturn(proprietaireEtape);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_ARCHIVAGE);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(nodeRef)).thenReturn(
                new ScriptEtapeCircuitImpl("PARAPHEUR", nodeRef, "ARCHIVAGE", Collections.<NodeRef>emptySet()));


        boolean returned = evaluator.evaluate(new Node(nodeRef));

        Assert.assertFalse(returned);
    }

    public void testEvaluateDossierNonTeletransmis() {
        Mockito.when(nodeService.hasAspect(nodeRef, ParapheurModel.ASPECT_S2LOW)).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateObject() {

        Object monNode = new Node(nodeRef);
        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }
}
