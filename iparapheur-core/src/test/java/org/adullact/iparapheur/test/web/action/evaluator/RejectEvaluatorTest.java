package org.adullact.iparapheur.test.web.action.evaluator;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;
import java.util.List;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.RejectEvaluator;

public class RejectEvaluatorTest extends WebUIBaseTest {

    private NodeRef nodeRef;
    private NodeRef parapheurRef;
    private RejectEvaluator evaluator;
     private EtapeCircuit proprietaireEtape;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        proprietaireEtape = Mockito.mock(EtapeCircuit.class);

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
        parapheurRef = new NodeRef("workspace://SpacesStore/parapheurRef");

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);

        Mockito.when(parapheurService.getParentParapheur(nodeRef)).thenReturn(
                parapheurRef);
        Mockito.when(
                parapheurService.isParapheurOwner(parapheurRef,
                tenantService.getDomainUser("proprietaire", tenantName))).thenReturn(true);
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);
        Mockito.when(parapheurService.isEmis(nodeRef)).thenReturn(true);

        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(false);
        
        Mockito.when(nodeService.exists(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(true);

        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);

        evaluator = new RejectEvaluator();
    }
    
    public void testEvaluateNull() {
        boolean returned = evaluator.evaluate(null);
        Assert.assertFalse(returned);
    }

    public void testEvaluateNonExistentNodeRef() {
        boolean returned = evaluator.evaluate(new Node(new NodeRef("workspace://SpacesStore/notInRepoRef")));
        Assert.assertFalse(returned);
    }

    public void testEvaluateExistingNodeNotDossier() {
        NodeRef myNodeRef = new NodeRef("workspace://SpacesStore/notInRepoRef");

        Mockito.when(nodeService.exists(myNodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(myNodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluate() {
        authenticate("proprietaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateUser() {
        authenticate("user");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateDossierTermine() {
        authenticate("proprietaire");
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateDossierNonEmis() {
        authenticate("proprietaire");
        Mockito.when(parapheurService.isEmis(nodeRef)).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateDossierSecretariat() {
        authenticate("proprietaire");
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateSecretaireDossierSecretariat() {
        authenticate("secretaire");
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluate2() {

        List<EtapeCircuit> circuit = null;
        authenticate("proprietaire");

        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(circuit);
        Mockito.when(nodeService.hasAspect(nodeRef, ParapheurModel.ASPECT_S2LOW)).thenReturn(true);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit)).thenReturn(proprietaireEtape);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_TDT);
        boolean returned = evaluator.evaluate(new Node(nodeRef));

        Assert.assertFalse(returned);
    }

    public void testEvaluateObject() {
        authenticate("proprietaire");

        Object monNode = new Node(nodeRef);

        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }
}
