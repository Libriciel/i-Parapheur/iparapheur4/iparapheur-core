package org.adullact.iparapheur.test.web.action.evaluator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.SecretaryEvaluator;

public class SecretaryEvaluatorTest extends WebUIBaseTest {

    private NodeRef nodeRef;
    private NodeRef parapheurRef;
    private SecretaryEvaluator evaluator;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
        parapheurRef = new NodeRef("workspace://SpacesStore/parapheurRef");

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);

        Mockito.when(parapheurService.getParentParapheur(nodeRef)).thenReturn(
                parapheurRef);
        Mockito.when(parapheurService.getSecretariatParapheur(parapheurRef)).thenReturn(Arrays.asList(tenantService.getDomainUser("secretaire", tenantName)));
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);
        Mockito.when(
                parapheurService.isParapheurOwner(parapheurRef,
                tenantService.getDomainUser("proprietaire", tenantName))).thenReturn(true);

        List<String> proprietaires = new ArrayList<String> ();
        proprietaires.add(tenantService.getDomainUser("proprietaire", tenantName));
        Mockito.when(parapheurService.getParapheurOwners(parapheurRef)).thenReturn(proprietaires);

        Mockito.when(
                parapheurService.isParapheurSecretaire(parapheurRef,
                tenantService.getDomainUser("secretaire", tenantName))).thenReturn(true);

        List<String> secretaires = new ArrayList<String>();
        secretaires.add(tenantService.getDomainUser("secretaire", tenantName));
        Mockito.when(parapheurService.getSecretariatParapheur(parapheurRef)).thenReturn(secretaires);

        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(false);

        Mockito.when(nodeService.exists(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(true);
        
        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);

        evaluator = new SecretaryEvaluator();
    }

    
    public void testEvaluateNull() {
        boolean returned = evaluator.evaluate(null);
        Assert.assertFalse(returned);
    }

    public void testEvaluateNonExistentNodeRef() {
        boolean returned = evaluator.evaluate(new Node(new NodeRef("workspace://SpacesStore/notInRepoRef")));
        Assert.assertFalse(returned);
    }

    public void testEvaluateExistingNodeNotDossier() {
        NodeRef myNodeRef = new NodeRef("workspace://SpacesStore/notInRepoRef");

        Mockito.when(nodeService.exists(myNodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(myNodeRef));
        Assert.assertFalse(returned);
    }
    
    public void testEvaluate() {
        authenticate("proprietaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateSecretaire() {
        authenticate("secretaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateSecretaireSecretariat() {
        authenticate("secretaire");
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateDossierTermine() {
        authenticate("proprietaire");
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateObject() {
        authenticate("proprietaire");

        Object monNode = new Node(nodeRef);

        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }
}
