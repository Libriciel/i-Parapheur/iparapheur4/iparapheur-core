package org.adullact.iparapheur.test.web.action.evaluator;

import java.util.Arrays;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.GetBackEvaluator;

public class GetBackEvaluatorTest extends WebUIBaseTest {

	private NodeRef nodeRef;

	private NodeRef proprietaireParapheur;

	private NodeRef userParapheur;

	private EtapeCircuit proprietaireEtape;

	private EtapeCircuit userEtape;

	private GetBackEvaluator evaluator;

	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();

		nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
		proprietaireParapheur = new NodeRef(
				"workspace://SpacesStore/proprietaireParapheur");
		userParapheur = new NodeRef("workspace://SpacesStore/userParapheur");

		proprietaireEtape = Mockito.mock(EtapeCircuit.class);
		userEtape = Mockito.mock(EtapeCircuit.class);

		Mockito.when(proprietaireEtape.getParapheur()).thenReturn(
				proprietaireParapheur);
		Mockito.when(proprietaireEtape.isApproved()).thenReturn(true);
		Mockito.when(userEtape.getParapheur()).thenReturn(userParapheur);
		Mockito.when(userEtape.isApproved()).thenReturn(false);

		parapheurService = Mockito.mock(ParapheurService.class);
		nodeService = Mockito.mock(NodeService.class);

		Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(true);
		Mockito.when(parapheurService.isRecuperable(nodeRef)).thenReturn(true);
		Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(
				Arrays.asList(proprietaireEtape, userEtape));
		Mockito.when(parapheurService.getOwnedParapheurs(tenantService.getDomainUser("proprietaire", tenantName)))
				.thenReturn(Arrays.asList(proprietaireParapheur));
		Mockito.when(parapheurService.getOwnedParapheurs(tenantService.getDomainUser("user", tenantName))).thenReturn(
				Arrays.asList(userParapheur));

		Mockito.when(
				nodeService.hasAspect(nodeRef,
						ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(false);

        Mockito.when(nodeService.exists(nodeRef)).thenReturn(true);
         Mockito.when(parapheurService.getParapheurOwners(proprietaireParapheur)).thenReturn(Arrays.asList(tenantService.getDomainUser("proprietaire", tenantName)));

                setUpManagedBean("ParapheurService", parapheurService);
		setUpManagedBean("NodeService", nodeService);

		evaluator = new GetBackEvaluator();
	}

	public void testEvaluate() {
		authenticate("proprietaire");

		boolean returned = evaluator.evaluate(new Node(nodeRef));
		Assert.assertTrue(returned);
	}

	public void testEvaluateAsUser() {
		authenticate("user");

		boolean returned = evaluator.evaluate(new Node(nodeRef));
		Assert.assertFalse(returned);
	}

	public void testEvaluateNonDossier() {
		Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(false);

		boolean returned = evaluator.evaluate(new Node(nodeRef));
		Assert.assertFalse(returned);
	}

	public void testEvaluateDossierNonRecuperable() {
		Mockito.when(parapheurService.isRecuperable(nodeRef)).thenReturn(false);

		boolean returned = evaluator.evaluate(new Node(nodeRef));
		Assert.assertFalse(returned);

	}

	public void testEvaluateSecretariat() {
		Mockito.when(
				nodeService.hasAspect(nodeRef,
						ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);

		boolean returned = evaluator.evaluate(new Node(nodeRef));
		Assert.assertFalse(returned);
	}

        public void testEvaluateObject() {
		authenticate("proprietaire");

        Object monNode = new Node(nodeRef);
		boolean returned = evaluator.evaluate(monNode);
		Assert.assertTrue(returned);
	}

}
