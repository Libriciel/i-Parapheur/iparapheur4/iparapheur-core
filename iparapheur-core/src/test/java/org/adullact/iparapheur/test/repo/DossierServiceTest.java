package org.adullact.iparapheur.test.repo;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.content.Document;
import com.atolcd.parapheur.repo.impl.DossierServiceImpl;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import org.junit.Assert;
import junit.framework.TestCase;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ApplicationModel;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: manz
 * Date: 30/08/12
 * Time: 13:35
 * To change this template use File | Settings | File Templates.
 */
public class DossierServiceTest extends BaseParapheurTest {

    public static String b64P7K = "MIAGCSqGSIb3DQEHAqCAMIACAQExCzAJBgUrDgMCGgUAMIAGCSqGSIb3DQEHAQAAoIAwggThMIID\n" +
            "yaADAgECAgEYMA0GCSqGSIb3DQEBBQUAMIGpMQswCQYDVQQGEwJGUjFMMEoGA1UEChNDTEEgUkVH\n" +
            "SU9OIExJTU9VU0lOIFJFUFJFU0VOVEVFIFBBUiBMRSBQUkVTSURFTlQgRFUgQ09OU0VJTCBSRUdJ\n" +
            "T05BTDFMMEoGA1UEAxNDTEEgUkVHSU9OIExJTU9VU0lOIFJFUFJFU0VOVEVFIFBBUiBMRSBQUkVT\n" +
            "SURFTlQgRFUgQ09OU0VJTCBSRUdJT05BTDAeFw0xMDA5MjAxNTAyMDNaFw0xMDA5MjAxNTA3MDNa\n" +
            "ME4xCzAJBgNVBAYTAkZSMRIwEAYDVQQKEwlBc3NvYyBGR0gxFzAVBgNVBAsTDjEyMzEyMzEyMzEy\n" +
            "MzEyMRIwEAYDVQQDEwlhbm5lIEZHSDIwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAIFrCBOe\n" +
            "snb4WGIT6lFTsWP7UOsFkYA3yf9eZb2g1e6W3spIit2T3WJGVZAFQUlPHLta3R0GdJjJcAHmk2IQ\n" +
            "OkRz8YPoo3GeSiPc4rEgjaJzvZOOUKqJ3IyoUf0pUr+kSCtyiJh/QCH9lT0fe7diu12rWpBA2L+q\n" +
            "Ke3nYXB431T1AgMBAAGjggHwMIIB7DBiBgNVHSAEWzBZMFcGA1UdIDBQME4GCCsGAQUFBwIBFkJo\n" +
            "dHRwOi8vcmVnaW9uLWxpbW91c2luLmZyL2RlbWF0ZXJpYWxpc2F0aW9uL3JhY2luZS8xL2xhdGVz\n" +
            "dGNybC5jcmwwHQYDVR0OBBYEFAruuXRCP2Td2wq0ku29b8TeUe+aMAwGA1UdEwEB/wQCMAAwgdYG\n" +
            "A1UdIwSBzjCBy4AUCu65dEI/ZN3bCrSS7b1vxN5R75qhga+kgawwgakxCzAJBgNVBAYTAkZSMUww\n" +
            "SgYDVQQKE0NMQSBSRUdJT04gTElNT1VTSU4gUkVQUkVTRU5URUUgUEFSIExFIFBSRVNJREVOVCBE\n" +
            "VSBDT05TRUlMIFJFR0lPTkFMMUwwSgYDVQQDE0NMQSBSRUdJT04gTElNT1VTSU4gUkVQUkVTRU5U\n" +
            "RUUgUEFSIExFIFBSRVNJREVOVCBEVSBDT05TRUlMIFJFR0lPTkFMggECMBsGA1UdEQQUMBKGEE1h\n" +
            "ZGFtZSBGR0gyIGFubmUwUwYDVR0fBEwwSjBIoEagRIZCaHR0cDovL3JlZ2lvbi1saW1vdXNpbi5m\n" +
            "ci9kZW1hdGVyaWFsaXNhdGlvbi9yYWNpbmUvMS9sYXRlc3RjcmwuY3JsMA4GA1UdDwEB/wQEAwIG\n" +
            "wDANBgkqhkiG9w0BAQUFAAOCAQEAWfzRweca1bRiFuclsNJ7yS7pISRp+3nTiz6H5/j5sQ0fwpGJ\n" +
            "rF6rc2JWQDAv6dBVjROblL0ix2zWY9Ywj/z6sX3AwXFZK8szNW2j0ti+3xrr+Z6Ab/DBdIEr7fkP\n" +
            "UeBdlnj3kHdhsMZNkJ5vWHDxK4eXneYrQn/w9fcEC1rFwRshiK+aU2zuULvFsrxSnmeQKWbf1RhY\n" +
            "odsV4fkOiJMmtbFItGwL1JsUWsDjFYuTKDtBplpSWSxvUY99OPBtFSjacuxEoUzTQWJnshjNmfsd\n" +
            "zHSzf33tDxyYQ2t10k+IZTnhcWDlI6SDg1eXK4BmkNmRm9ECIRUM0AV4nn/h8jLSqQAAMYIRTTCC\n" +
            "EUkCAQEwga8wgakxCzAJBgNVBAYTAkZSMUwwSgYDVQQKE0NMQSBSRUdJT04gTElNT1VTSU4gUkVQ\n" +
            "UkVTRU5URUUgUEFSIExFIFBSRVNJREVOVCBEVSBDT05TRUlMIFJFR0lPTkFMMUwwSgYDVQQDE0NM\n" +
            "QSBSRUdJT04gTElNT1VTSU4gUkVQUkVTRU5URUUgUEFSIExFIFBSRVNJREVOVCBEVSBDT05TRUlM\n" +
            "IFJFR0lPTkFMAgEYMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqG\n" +
            "SIb3DQEJBTEPFw0xMDA5MjAxNTAyMDNaMCMGCSqGSIb3DQEJBDEWBBR70vKauyWnyTdCBu8JblZ1\n" +
            "GbDRxjANBgkqhkiG9w0BAQEFAASBgHZot80ULvk/EXuC46mfUbAsvSp5uYUx3g3gK1Yarl4P1yPw\n" +
            "V2fvRIfus5DMDCmHimM/SDJHSq8XJnXfGu062ZROD29atEDzrH4WLfCeW3bbnkq18Q5cxPUUkUV1\n" +
            "83oeHrJkkvFpEyj55cC1CO7oEMMDH4B7XBUtx6mBCmz3xmB+oYIPlDCCD5AGCyqGSIb3DQEJEAIO\n" +
            "MYIPfzCCD3sGCSqGSIb3DQEHAqCCD2wwgg9oAgEDMQswCQYFKw4DAhoFADCCB80GCyqGSIb3DQEJ\n" +
            "EAEEoIIHvASCB7gwgge0AgEBBgsqhkiG9w0BCRACDjCCBucwCQYFKw4DAhoFAASCBtgwgAYJKoZI\n" +
            "hvcNAQcCoIAwgAIBATELMAkGBSsOAwIaBQAwgAYJKoZIhvcNAQcBAACggDCCBOEwggPJoAMCAQIC\n" +
            "ARgwDQYJKoZIhvcNAQEFBQAwgakxCzAJBgNVBAYTAkZSMUwwSgYDVQQKE0NMQSBSRUdJT04gTElN\n" +
            "T1VTSU4gUkVQUkVTRU5URUUgUEFSIExFIFBSRVNJREVOVCBEVSBDT05TRUlMIFJFR0lPTkFMMUww\n" +
            "SgYDVQQDE0NMQSBSRUdJT04gTElNT1VTSU4gUkVQUkVTRU5URUUgUEFSIExFIFBSRVNJREVOVCBE\n" +
            "VSBDT05TRUlMIFJFR0lPTkFMMB4XDTEwMDkyMDE1MDIwM1oXDTEwMDkyMDE1MDcwM1owTjELMAkG\n" +
            "A1UEBhMCRlIxEjAQBgNVBAoTCUFzc29jIEZHSDEXMBUGA1UECxMOMTIzMTIzMTIzMTIzMTIxEjAQ\n" +
            "BgNVBAMTCWFubmUgRkdIMjCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAgWsIE56ydvhYYhPq\n" +
            "UVOxY/tQ6wWRgDfJ/15lvaDV7pbeykiK3ZPdYkZVkAVBSU8cu1rdHQZ0mMlwAeaTYhA6RHPxg+ij\n" +
            "cZ5KI9zisSCNonO9k45QqoncjKhR/SlSv6RIK3KImH9AIf2VPR97t2K7XatakEDYv6op7edhcHjf\n" +
            "VPUCAwEAAaOCAfAwggHsMGIGA1UdIARbMFkwVwYDVR0gMFAwTgYIKwYBBQUHAgEWQmh0dHA6Ly9y\n" +
            "ZWdpb24tbGltb3VzaW4uZnIvZGVtYXRlcmlhbGlzYXRpb24vcmFjaW5lLzEvbGF0ZXN0Y3JsLmNy\n" +
            "bDAdBgNVHQ4EFgQUCu65dEI/ZN3bCrSS7b1vxN5R75owDAYDVR0TAQH/BAIwADCB1gYDVR0jBIHO\n" +
            "MIHLgBQK7rl0Qj9k3dsKtJLtvW/E3lHvmqGBr6SBrDCBqTELMAkGA1UEBhMCRlIxTDBKBgNVBAoT\n" +
            "Q0xBIFJFR0lPTiBMSU1PVVNJTiBSRVBSRVNFTlRFRSBQQVIgTEUgUFJFU0lERU5UIERVIENPTlNF\n" +
            "SUwgUkVHSU9OQUwxTDBKBgNVBAMTQ0xBIFJFR0lPTiBMSU1PVVNJTiBSRVBSRVNFTlRFRSBQQVIg\n" +
            "TEUgUFJFU0lERU5UIERVIENPTlNFSUwgUkVHSU9OQUyCAQIwGwYDVR0RBBQwEoYQTWFkYW1lIEZH\n" +
            "SDIgYW5uZTBTBgNVHR8ETDBKMEigRqBEhkJodHRwOi8vcmVnaW9uLWxpbW91c2luLmZyL2RlbWF0\n" +
            "ZXJpYWxpc2F0aW9uL3JhY2luZS8xL2xhdGVzdGNybC5jcmwwDgYDVR0PAQH/BAQDAgbAMA0GCSqG\n" +
            "SIb3DQEBBQUAA4IBAQBZ/NHB5xrVtGIW5yWw0nvJLukhJGn7edOLPofn+PmxDR/CkYmsXqtzYlZA\n" +
            "MC/p0FWNE5uUvSLHbNZj1jCP/PqxfcDBcVkryzM1baPS2L7fGuv5noBv8MF0gSvt+Q9R4F2WePeQ\n" +
            "d2Gwxk2Qnm9YcPErh5ed5itCf/D19wQLWsXBGyGIr5pTbO5Qu8WyvFKeZ5ApZt/VGFih2xXh+Q6I\n" +
            "kya1sUi0bAvUmxRawOMVi5MoO0GmWlJZLG9Rj3048G0VKNpy7EShTNNBYmeyGM2Z+x3MdLN/fe0P\n" +
            "HJhDa3XST4hlOeFxYOUjpIODV5crgGaQ2ZGb0QIhFQzQBXief+HyMtKpAAAxggG1MIIBsQIBATCB\n" +
            "rzCBqTELMAkGA1UEBhMCRlIxTDBKBgNVBAoTQ0xBIFJFR0lPTiBMSU1PVVNJTiBSRVBSRVNFTlRF\n" +
            "RSBQQVIgTEUgUFJFU0lERU5UIERVIENPTlNFSUwgUkVHSU9OQUwxTDBKBgNVBAMTQ0xBIFJFR0lP\n" +
            "TiBMSU1PVVNJTiBSRVBSRVNFTlRFRSBQQVIgTEUgUFJFU0lERU5UIERVIENPTlNFSUwgUkVHSU9O\n" +
            "QUwCARgwCQYFKw4DAhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkF\n" +
            "MQ8XDTEwMDkyMDE1MDIwM1owIwYJKoZIhvcNAQkEMRYEFHvS8pq7JafJN0IG7wluVnUZsNHGMA0G\n" +
            "CSqGSIb3DQEBAQUABIGAdmi3zRQu+T8Re4LjqZ9RsCy9Knm5hTHeDeArVhquXg/XI/BXZ+9Eh+6z\n" +
            "kMwMKYeKYz9IMkdKrxcmdd8a7TrZlE4Pb1q0QPOsfhYt8J5bdtueSrXxDlzE9RSRRXXzeh4esmSS\n" +
            "8WkTKPnlwLUI7ugQwwMfgHtcFS3HqYEKbPfGYH4AAAAAAAACAQIYDzIwMTAwOTIwMTUwMjAzWgIK\n" +
            "ZYYa5gw3tPFI8qCBloaBk0NOPUxBIFJFR0lPTiBMSU1PVVNJTiBSRVBSRVNFTlRFRSBQQVIgTEUg\n" +
            "UFJFU0lERU5UIERVIENPTlNFSUwgUkVHSU9OQUwsIE89TEEgUkVHSU9OIExJTU9VU0lOIFJFUFJF\n" +
            "U0VOVEVFIFBBUiBMRSBQUkVTSURFTlQgRFUgQ09OU0VJTCBSRUdJT05BTCwgQz1GUqCCBRkwggUV\n" +
            "MIID/aADAgECAgECMA0GCSqGSIb3DQEBBQUAMIGpMQswCQYDVQQGEwJGUjFMMEoGA1UEChNDTEEg\n" +
            "UkVHSU9OIExJTU9VU0lOIFJFUFJFU0VOVEVFIFBBUiBMRSBQUkVTSURFTlQgRFUgQ09OU0VJTCBS\n" +
            "RUdJT05BTDFMMEoGA1UEAxNDTEEgUkVHSU9OIExJTU9VU0lOIFJFUFJFU0VOVEVFIFBBUiBMRSBQ\n" +
            "UkVTSURFTlQgRFUgQ09OU0VJTCBSRUdJT05BTDAeFw0xMDA5MTQxNzI0MDVaFw0xMzA5MTQxNzI0\n" +
            "MDVaMIGpMQswCQYDVQQGEwJGUjFMMEoGA1UEChNDTEEgUkVHSU9OIExJTU9VU0lOIFJFUFJFU0VO\n" +
            "VEVFIFBBUiBMRSBQUkVTSURFTlQgRFUgQ09OU0VJTCBSRUdJT05BTDFMMEoGA1UEAxNDTEEgUkVH\n" +
            "SU9OIExJTU9VU0lOIFJFUFJFU0VOVEVFIFBBUiBMRSBQUkVTSURFTlQgRFUgQ09OU0VJTCBSRUdJ\n" +
            "T05BTDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAIFFMzJgLxb2mP79V07/4m4RlF2k\n" +
            "+iQMIFRwAf9DYXdrz23Gg33MJZoopAc3mKFSg6lUmhocwX7oyMi8P4147lP4p9uJMuwlUfpLm+Fo\n" +
            "gl84LvDf15v9coNiAjzjFiYoulJrI1KLQW6mdtACa+pope8F9KSTBc5+AtOoIiUSYKk5fkXv9bCU\n" +
            "2zs2F6VLSoJUNdfzsgXjp1T7MWkOa2d7oLODrwsmK/KXIJK5CFgl9ZH+RvCIXV7LViJczFAaG9ob\n" +
            "F3OKW48VjNa1Uja4VbL98L1TYGs4yj7/EVSFstle+AgnBizwFXuk6sHKBEUlLy6qXoYxUVjsBMj3\n" +
            "7hYlXQkelgkCAwEAAaOCAUQwggFAMHAGA1UdIARpMGcwZQYDVR0gMF4wXAYIKwYBBQUHAgEWUGh0\n" +
            "dHA6Ly93d3cucmVnaW9uLWxpbW91c2luLmZyL2RlbWF0ZXJpYWxpc2F0aW9uL1BDLzEvQ1IgTGlt\n" +
            "b3VzaW4tUEMtMjAxMDA4MzEucGRmMB0GA1UdDgQWBBQK7rl0Qj9k3dsKtJLtvW/E3lHvmjAPBgNV\n" +
            "HRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFAruuXRCP2Td2wq0ku29b8TeUe+aMFMGA1UdHwRMMEow\n" +
            "SKBGoESGQmh0dHA6Ly9yZWdpb24tbGltb3VzaW4uZnIvZGVtYXRlcmlhbGlzYXRpb24vcmFjaW5l\n" +
            "LzEvbGF0ZXN0Y3JsLmNybDAWBgNVHSUBAf8EDDAKBggrBgEFBQcDCDAOBgNVHQ8BAf8EBAMCAQYw\n" +
            "DQYJKoZIhvcNAQEFBQADggEBAHVaZNkrTz4VXg6xZp3EutDh4I93uXNKQo39+2o1ALUYMh/Sk0PN\n" +
            "xE9P/l+qvAN9DhIKlY9qPVZeXXYr9e9MLPlljctiaRtxr8pP9l2YrqAZrTLePGQIncRhPWCaktUt\n" +
            "yBEIXQL9t4s80Wd7x8uFB1VL1bALnyewXUzr7u1E3j3dFsFytPZ5izyoPr7zhcG/66agj1UAzyK5\n" +
            "JRzz5mTPDF3yc/qh93//ZBWIeVQqJYwmDKfiYCPDZpnLVHx08j5id+J2Foa2yttZUpqkNd8Q2A3q\n" +
            "7UcPSutn7v2J1Vn8ezn74Pb4NcPele5O/zl7Wiul58KDTt5hUdg6KC7WU5iMH4MxggJmMIICYgIB\n" +
            "ATCBrzCBqTELMAkGA1UEBhMCRlIxTDBKBgNVBAoTQ0xBIFJFR0lPTiBMSU1PVVNJTiBSRVBSRVNF\n" +
            "TlRFRSBQQVIgTEUgUFJFU0lERU5UIERVIENPTlNFSUwgUkVHSU9OQUwxTDBKBgNVBAMTQ0xBIFJF\n" +
            "R0lPTiBMSU1PVVNJTiBSRVBSRVNFTlRFRSBQQVIgTEUgUFJFU0lERU5UIERVIENPTlNFSUwgUkVH\n" +
            "SU9OQUwCAQIwCQYFKw4DAhoFAKCBjDAaBgkqhkiG9w0BCQMxDQYLKoZIhvcNAQkQAQQwHAYJKoZI\n" +
            "hvcNAQkFMQ8XDTEwMDkyMDE1MDIwM1owIwYJKoZIhvcNAQkEMRYEFHOVoFq/yj3990JZ40mgOz7F\n" +
            "JUdlMCsGCyqGSIb3DQEJEAIMMRwwGjAYMBYEFGbtcgN5yv5JVLBpkIBxOPTL4QAcMA0GCSqGSIb3\n" +
            "DQEBAQUABIIBAH3lT4BGDrO0vt9rtPt+h2UKK9J2uTq9DWXsaQjlwS596i6QSwIf911aa+8Gu1qi\n" +
            "fWOR6N3NfvpZZQru7UuwFdH/sqgfJb/bT7EEmokKpQXi/DD6/60DUUMgoTbL1ClbqjqvcNfkel+3\n" +
            "nLhDcmoUsAQN4QJQHCz581OuyUY1RaGdm4l3WYxTGvwJiHzD2AM5QHp9NiTTBcg9MY2Am9EPUeoX\n" +
            "Z+KW7ezM1iy3jtCKh/drN62p1UR9uveSLXwr3XVz9NRGHl6KmVjEH7VLNAfqZKbuuB8R8QTySQeT\n" +
            "q3C4WRAWWPuDDnyVSQ1lc2QL+I5PDXbsvZGEc1i9csf6eGzOtNEAAAAAAAA=";

    public static String b64PDF = "JVBERi0xLjQKJcOkw7zDtsOfCjIgMCBvYmoKPDwvTGVuZ3RoIDMgMCBSL0ZpbHRlci9GbGF0ZURl\n" +
            "Y29kZT4+CnN0cmVhbQp4nNVQywrCMBC871fsuYdl0yRN8gWCt+rBDyhqESvYS3/fbTbFqtC7hMBM\n" +
            "Zh+TYTI4wRNZjk+eHEZX43jGU4UPYGqQKaLB8QrWOJGDDfI4gE1MtjDnoihNSG/MlgJ2hfk6yBDt\n" +
            "cE0SRWcp7qC2kXxhJrJg7VC8zCosb9EO3b721UEPWrf4XLM79qCOdNfwwWb1UgHnLA67HMokdy/f\n" +
            "v/3N74/QfjnfzuO3fjuhub7FFxEubvwKZW5kc3RyZWFtCmVuZG9iagoKMyAwIG9iagoxODAKZW5k\n" +
            "b2JqCgo1IDAgb2JqCjw8Cj4+CmVuZG9iagoKNiAwIG9iago8PC9Gb250IDUgMCBSCi9Qcm9jU2V0\n" +
            "Wy9QREYvVGV4dF0KPj4KZW5kb2JqCgoxIDAgb2JqCjw8L1R5cGUvUGFnZS9QYXJlbnQgNCAwIFIv\n" +
            "UmVzb3VyY2VzIDYgMCBSL01lZGlhQm94WzAgMCA1OTUgODQyXS9Hcm91cDw8L1MvVHJhbnNwYXJl\n" +
            "bmN5L0NTL0RldmljZVJHQi9JIHRydWU+Pi9Db250ZW50cyAyIDAgUj4+CmVuZG9iagoKNCAwIG9i\n" +
            "ago8PC9UeXBlL1BhZ2VzCi9SZXNvdXJjZXMgNiAwIFIKL01lZGlhQm94WyAwIDAgNTk1IDg0MiBd\n" +
            "Ci9LaWRzWyAxIDAgUiBdCi9Db3VudCAxPj4KZW5kb2JqCgo3IDAgb2JqCjw8L1R5cGUvQ2F0YWxv\n" +
            "Zy9QYWdlcyA0IDAgUgovT3BlbkFjdGlvblsxIDAgUiAvWFlaIG51bGwgbnVsbCAwXQovTGFuZyhm\n" +
            "ci1GUikKPj4KZW5kb2JqCgo4IDAgb2JqCjw8L0F1dGhvcjxGRUZGMDA1MzAwNzQwMEU5MDA3MDAw\n" +
            "NjgwMDYxMDA2RTAwNjUwMDIwMDA1NjAwNjEwMDczMDA3ND4KL0NyZWF0b3I8RkVGRjAwNTcwMDcy\n" +
            "MDA2OTAwNzQwMDY1MDA3Mj4KL1Byb2R1Y2VyPEZFRkYwMDRGMDA3MDAwNjUwMDZFMDA0RjAwNjYw\n" +
            "MDY2MDA2OTAwNjMwMDY1MDAyRTAwNkYwMDcyMDA2NzAwMjAwMDMzMDAyRTAwMzI+Ci9DcmVhdGlv\n" +
            "bkRhdGUoRDoyMDExMDIxMDE4MDYyMyswMScwMCcpPj4KZW5kb2JqCgp4cmVmCjAgOQowMDAwMDAw\n" +
            "MDAwIDY1NTM1IGYgCjAwMDAwMDAzNjUgMDAwMDAgbiAKMDAwMDAwMDAxOSAwMDAwMCBuIAowMDAw\n" +
            "MDAwMjcwIDAwMDAwIG4gCjAwMDAwMDA1MDcgMDAwMDAgbiAKMDAwMDAwMDI5MCAwMDAwMCBuIAow\n" +
            "MDAwMDAwMzEyIDAwMDAwIG4gCjAwMDAwMDA2MDUgMDAwMDAgbiAKMDAwMDAwMDcwMSAwMDAwMCBu\n" +
            "IAp0cmFpbGVyCjw8L1NpemUgOS9Sb290IDcgMCBSCi9JbmZvIDggMCBSCi9JRCBbIDw1QzQ0MEU3\n" +
            "ODYxNDBGQkE2RDUwRTRGRDczOEJDRTc1ND4KPDVDNDQwRTc4NjE0MEZCQTZENTBFNEZENzM4QkNF\n" +
            "NzU0PiBdCi9Eb2NDaGVja3N1bSAvMTg2MTc5QkNDRThGMEExRTZGRjE2Q0IyOEYxRUU4Q0QKPj4K\n" +
            "c3RhcnR4cmVmCjk1MwolJUVPRgo=";

    @Autowired
    private DossierService dossierService;

    NodeRef dossier;

    private NodeRef companyHome;
    private NodeRef archives_configuration;
    /**
     * Parapheurs home ref
     */
    private NodeRef parapheursHome;
    /**
     * Archives home ref
     */
    private NodeRef archivesHome;
    /**
     * Dictionary home ref
     */
    private NodeRef dictionaryHome;
    /**
     * Workflows home ref
     */
    private NodeRef workflowsHome;

    private NodeRef parapheur;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();
        companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.company_home.childname"),
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                        namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        archivesHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.archives.childname"),
                        namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();


        permissionService.setInheritParentPermissions(archivesHome, false);
        permissionService.setPermission(archivesHome, "GROUP_EVERYONE", "Contributor", true);

        dictionaryHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.dictionary.childname"),
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        archives_configuration = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("ph:archives_configuration",
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();
        ContentWriter contentWriter = contentService.getWriter(archives_configuration, ContentModel.PROP_CONTENT, true);
        contentWriter.putContent("archive.tamponActes.visible=true\n"
                + "archive.tamponActes.text=Acquitt\u00E9 en PREFECTURE le {0,date,dd/MM/yyyy}");

        workflowsHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.savedworkflows.childname"),
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();


        authenticationService.createAuthentication(tenantService.getDomainUser("proprietaire1", tenantName), "secret".toCharArray());

        NodeRef prop1 = personService.getPerson(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(prop1, ContentModel.PROP_FIRSTNAME, "Prop");
        nodeService.setProperty(prop1, ContentModel.PROP_LASTNAME, "Ietaire");
        nodeService.setProperty(prop1, ContentModel.PROP_EMAIL, "proprietaire1@example.com");



        ArrayList proprietaires = new ArrayList();
        proprietaires.add(tenantService.getDomainUser("proprietaire1", tenantName));

        Map<QName, Serializable> parapheurProperties = new HashMap<QName, Serializable>();
        parapheurProperties.put(ContentModel.PROP_NAME, "Parapheur test unit");
        parapheurProperties.put(ApplicationModel.PROP_ICON, "space-icon-default");
        parapheurProperties.put(ContentModel.PROP_TITLE, "Parapheur test unit");
        parapheurProperties.put(ContentModel.PROP_DESCRIPTION, "Parapheur test unit");
        parapheurProperties.put(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, (ArrayList) proprietaires);

        parapheur = parapheurService.createParapheur(parapheurProperties);


        Map<QName, Serializable> dossierProperties = new HashMap<QName, Serializable>();
        dossierProperties.put(ContentModel.PROP_TITLE, "dossier Test");
        dossierProperties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.FALSE);
        dossierProperties.put(ParapheurModel.PROP_PUBLIC, Boolean.TRUE);

        dossier = dossierService.createDossier(parapheur, dossierProperties);
        EtapeCircuitImpl etape1 = new EtapeCircuitImpl();

        etape1.setActionDemandee("VISA");
        etape1.setParapheur(parapheur);
        etape1.setTransition("PARAPHEUR");
        etape1.setListeDiffusion(new HashSet<NodeRef>());

        List<EtapeCircuit> etapes = new ArrayList<EtapeCircuit>();

        etapes.add(etape1);
        etapes.add(etape1);

        nodeService.createNode(dossier,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}content1"), ContentModel.TYPE_CONTENT).getChildRef();
        //nodeService.addAspect(content1, ParapheurModel.ASPECT_LU,

        parapheurService.setCircuit(dossier, etapes);
        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Integer>() {
            @Override
            public Integer doWork() throws Exception {
                parapheurService.approve(dossier);
                return 0;
            }
        }, tenantService.getDomainUser("proprietaire1", tenantName));
    }

    public void testSetVisuelDocument() throws Exception {
        NodeRef pendingFile = dossierService.beginCreateDossier(parapheur);

        Document doc = new Document();
        doc.setFileName("testPDFfixture.pdf");
        doc.setBase64Content(b64PDF);

        Document visudoc = new Document();
        visudoc.setFileName("testPDFfixturevisu.pdf");
        visudoc.setBase64Content(b64PDF);

        NodeRef docPrincipal = dossierService.addDocumentPrincipal(pendingFile, doc, null);

        dossierService.setVisuelDocument(pendingFile, docPrincipal, visudoc);
    }

    public void testAddSignature() throws Exception {
        NodeRef dossier = dossierService.beginCreateDossier(parapheur);
        List<NodeRef> listDoss = new ArrayList<NodeRef>();
        listDoss.add(dossier);

        Document doc = new Document();
        doc.setFileName("signature.pk7");
        doc.setBase64Content(b64P7K);

        Document empty = new Document();
        empty.setFileName("empty.pk7");
        empty.setBase64Content("\n");

        //NORMAL CASE
        dossierService.addSignature(dossier, doc);

        //NULL DOC CASE
        try {
            dossierService.addSignature(dossier, null);
            fail("Expected Exception Exception");
        } catch (Exception e) {
            //It's OK, expected
        }

        //EMPTY DOC CASE
        dossierService.addSignature(dossier, empty);

        //PDF CASE
        doc.setBase64Content(b64PDF);
        dossierService.addSignature(dossier, doc);

        //EMIT DOSSIER CASE
        doc.setBase64Content(b64P7K);
        dossierService.visaDossier(listDoss, "", "", parapheur, false);
        dossierService.addSignature(dossier, doc);
    }

    public void testBeginCreateDossier() throws Exception {
        NodeRef pendingFile = dossierService.beginCreateDossier(parapheur);
        Assert.assertTrue((Boolean)nodeService.getProperty(pendingFile, ParapheurModel.PROP_UNCOMPLETE));
        Assert.assertTrue(nodeService.exists(pendingFile));
    }

    public void testFinalizeCreateDossier() throws Exception {
        NodeRef pendingFile = dossierService.beginCreateDossier(parapheur);
        dossierService.finalizeCreateDossier(pendingFile);
        Assert.assertTrue((Boolean)nodeService.getProperty(pendingFile, ParapheurModel.PROP_UNCOMPLETE));
    }

    public void testAddDocumentPrincipal() throws Exception {
        NodeRef pendingFile = dossierService.beginCreateDossier(parapheur);

        Document doc = new Document();
        doc.setFileName("testPDFfixture.pdf");
        doc.setBase64Content(b64PDF);
        
        Document visudoc = new Document();
        visudoc.setFileName("testPDFfixturevisu.pdf");
        visudoc.setBase64Content(b64PDF);

        NodeRef docPrincipal = dossierService.addDocumentPrincipal(pendingFile, doc, visudoc);
        Assert.assertTrue(nodeService.exists(docPrincipal));
        Assert.assertEquals(parapheurService.getDocuments(pendingFile).size(), 1);
    }

    public void testSetDossierProperties() throws Exception {
        NodeRef pendingFile = dossierService.beginCreateDossier(parapheur);
        Assert.assertEquals(nodeService.getProperty(pendingFile, ContentModel.PROP_TITLE), "Nouveau dossier");

        HashMap<String, Serializable> properties = new HashMap<String, Serializable>();
        properties.put("cm:title", "Astérix");

        dossierService.setDossierProperties(pendingFile, properties);

        Assert.assertEquals(nodeService.getProperty(pendingFile, ContentModel.PROP_TITLE), "Astérix");


    }

    public void testAddAnnexe() throws Exception {

    }

    public void testSetCircuit() throws Exception {

    }
}
