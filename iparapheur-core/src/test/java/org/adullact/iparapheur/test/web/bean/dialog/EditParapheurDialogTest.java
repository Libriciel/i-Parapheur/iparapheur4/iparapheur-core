package org.adullact.iparapheur.test.web.bean.dialog;

import java.io.Serializable;
import java.util.*;

import com.atolcd.parapheur.web.bean.ParapheurBean;
import org.junit.Assert;

import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.web.bean.AdminParapheurBean;
import com.atolcd.parapheur.web.bean.dialog.EditParapheurDialog;
import org.alfresco.service.cmr.security.AuthorityType;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.web.bean.repository.MapNode;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class EditParapheurDialogTest extends WebBeanBaseTest {

	private NodeRef parapheurRef;
	
	private EditParapheurDialog dialog;
	
	private AdminParapheurBean adminParapheurBean;
	
	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();
		
		Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
		properties.put(ContentModel.PROP_NAME, "parapheurRef");
		properties.put(ContentModel.PROP_TITLE, "parapheurRef");
		parapheurRef = parapheurService.createParapheur(properties);
		
		adminParapheurBean = Mockito.mock(AdminParapheurBean.class);
		setUpManagedBean("AdminParapheurBean", adminParapheurBean);

		dialog = new EditParapheurDialog();
		dialog.setNodeService(nodeService);
		dialog.setParapheurService(parapheurService);
		dialog.setPersonService(personService);
                dialog.setAuthenticationComponent(authenticationComponent);
                dialog.setTenantService(tenantService);
                dialog.setAuthorityService(authorityService);

                authorityService.createAuthority(AuthorityType.ROLE, "PHOWNER_nom");
                
                /* needed by EditParapheurDialog:
                 * authorityService.addAuthority("ROLE_PHOWNER_" + this.nom,
                        authorityService.getName(AuthorityType.USER, this.proprietaire));
                 */
                personService.getPerson("proprietaire@"+tenantName, true);

//               System.out.println("autorité ::::::::::: " + authorityService.findAuthorities(AuthorityType.USER, null, false, "proprietaire@unit-tests-tenant", null));
//                authorityService.createAuthority(AuthorityType.USER, "proprietaire@unit-tests-tenant");


        }

    public void testFinish() {

        Mockito.when(adminParapheurBean.getParapheurNode()).thenReturn(new Node(parapheurRef));
        dialog.init(Collections.<String, String>emptyMap());

        dialog.setNom("nom");
        dialog.setTitre("titre");
        dialog.setDescription("description");
        dialog.setDelegation(false);
        dialog.setDelegate(null);
        dialog.setHabilitationsEnabled(false);
        ArrayList<Node> owners = new ArrayList<Node>();
        owners.add(new MapNode(personService.getPerson(tenantService.getDomainUser("proprietaire", tenantName))));
        dialog.setOwners(owners);
        ArrayList<Node> secretariatList = new ArrayList<Node>();
        secretariatList.add(new MapNode(personService.getPerson(tenantService.getDomainUser("secretaire", tenantName))));
        dialog.setSecretariat(secretariatList);

        transactionManager.commit(transactionStatus);
        transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());

        dialog.finish();

        Map<QName, Serializable> properties = nodeService.getProperties(parapheurRef);


        Assert.assertEquals("nom", properties.get(ContentModel.PROP_NAME));
        Assert.assertEquals("titre", properties.get(ContentModel.PROP_TITLE));
        Assert.assertEquals("description", properties.get(ContentModel.PROP_DESCRIPTION));
        Assert.assertTrue(((ArrayList<String>) properties.get(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR)).contains(tenantService.getDomainUser("proprietaire", tenantName)));
        Serializable secretariat = properties.get(ParapheurModel.PROP_SECRETAIRES);
        Assert.assertTrue(secretariat instanceof List);
        List<String> secretaires = (List<String>) secretariat;
        Assert.assertEquals(1, secretaires.size());
        Assert.assertEquals(tenantService.getDomainUser("secretaire", tenantName), secretaires.get(0));
    }
	
}
