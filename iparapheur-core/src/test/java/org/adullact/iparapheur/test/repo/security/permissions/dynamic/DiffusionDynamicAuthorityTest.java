package org.adullact.iparapheur.test.repo.security.permissions.dynamic;

import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.security.permissions.dynamic.DiffusionDynamicAuthority;
import java.util.Arrays;
import java.util.HashSet;
import org.junit.Assert;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.mockito.Mockito;

public class DiffusionDynamicAuthorityTest extends BaseParapheurTest {

    private NodeRef document;
    private NodeRef dossier;
    private NodeRef document2;
    private NodeRef dossier2;
    private NodeRef parapheurDiffusion1;
    private NodeRef parapheurDiffusion2;
    private NodeRef parapheurDiffusion3;
    private DiffusionDynamicAuthority authority;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        document = new NodeRef("workspace://SpacesStore/document");
        dossier = new NodeRef("workspace://SpacesStore/dossier");
        document2 = new NodeRef("workspace://SpacesStore/document2");
        dossier2 = new NodeRef("workspace://SpacesStore/dossier2");
        parapheurDiffusion1 = new NodeRef("workspace://SpacesStore/parapheurDiffusion1");
        parapheurDiffusion2 = new NodeRef("workspace://SpacesStore/parapheurDiffusion2");
        parapheurDiffusion3 = new NodeRef("workspace://SpacesStore/parapheurDiffusion3");

        parapheurService = Mockito.mock(ParapheurService.class);
        Mockito.when(parapheurService.getParentDossier(document)).thenReturn(dossier);
        Mockito.when(parapheurService.getParentDossier(document2)).thenReturn(dossier2);
        Mockito.when(parapheurService.isEmis(dossier)).thenReturn(true);
        Mockito.when(parapheurService.isEmis(dossier2)).thenReturn(true);
        EtapeCircuit etapeCircuit = Mockito.mock(EtapeCircuit.class);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(dossier)).thenReturn(etapeCircuit);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(dossier2)).thenReturn(null);
        Mockito.when(etapeCircuit.getListeNotification()).thenReturn(new HashSet<NodeRef>(Arrays.asList(parapheurDiffusion1,
                parapheurDiffusion2, parapheurDiffusion3)));
        Mockito.when(parapheurService.isParapheurOwner(parapheurDiffusion1, "proprietaire1")).thenReturn(true);
        Mockito.when(parapheurService.isParapheurOwner(parapheurDiffusion1, "proprietaire2")).thenReturn(true);
        Mockito.when(parapheurService.isParapheurOwner(parapheurDiffusion1, "proprietaire3")).thenReturn(true);

        authority = new DiffusionDynamicAuthority();
        authority.setParapheurService(parapheurService);
        authority.afterPropertiesSet();
    }

    public void testHasAuthority1() {
        boolean returned = authority.hasAuthority(document, "proprietaire1");
        Assert.assertTrue(returned);
    }

    public void testHasAuthority2() {
        boolean returned = authority.hasAuthority(document, "proprietaire1");
        Assert.assertTrue(returned);
    }

    public void testHasAuthority3() {
        boolean returned = authority.hasAuthority(document, "proprietaire1");
        Assert.assertTrue(returned);
    }

    public void testHasNotAuthority() {
        boolean returned = authority.hasAuthority(document, "nonProprietaire");
        Assert.assertFalse(returned);
    }

    public void testHasAuthorityDossierNonEmis() {
        Mockito.when(parapheurService.isEmis(dossier)).thenReturn(false);

        boolean returned = authority.hasAuthority(document, "nonProprietaire");
        Assert.assertFalse(returned);
    }

    public void testHasAuthority4() {


        boolean returned = authority.hasAuthority(document, "proprietaire1");
        Assert.assertTrue(returned);
    }

    public void testGetAuthority() {
        String returned = authority.getAuthority();
        Assert.assertEquals(returned, "ROLE_PARAPHEUR_NOTIFIE");
    }
}
