package org.adullact.iparapheur.test.repo.content.transformer;

import org.junit.Assert;

import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.pdfbox.cos.COSDocument;
import org.pdfbox.pdfparser.PDFParser;

import com.atolcd.parapheur.repo.content.transform.JpgToPdf;

public class JpgToPdfTest extends BaseParapheurTest {

	private NodeRef jpgContent;

	private NodeRef pdfContent;

	private JpgToPdf converter;

	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();
		
		converter = new JpgToPdf();
		
		jpgContent = nodeService.createNode(rootNodeRef,
				ContentModel.ASSOC_CONTAINS,
				QName.createQName("{test}jpgContent"),
				ContentModel.TYPE_CONTENT).getChildRef();
		pdfContent = nodeService.createNode(rootNodeRef,
				ContentModel.ASSOC_CONTAINS,
				QName.createQName("{test}pdfContent"),
				ContentModel.TYPE_CONTENT).getChildRef();

		ContentWriter jpgWriter = contentService.getWriter(jpgContent,
				ContentModel.PROP_CONTENT, true);
		jpgWriter.setMimetype(MimetypeMap.MIMETYPE_IMAGE_JPEG);
		jpgWriter.putContent(this.getClass().getResourceAsStream(
				"/samples/parapheur.jpg"));
	}

	public void testTransformInternal() {
		ContentReader jpgReader = contentService.getReader(jpgContent,
				ContentModel.PROP_CONTENT);
		ContentWriter pdfWriter = contentService.getWriter(pdfContent,
				ContentModel.PROP_CONTENT, true);
		pdfWriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
		
		converter.transform(jpgReader, pdfWriter);

		ContentReader pdfReader = contentService.getReader(pdfContent, ContentModel.PROP_CONTENT);
		Assert.assertEquals(MimetypeMap.MIMETYPE_PDF, pdfReader.getMimetype());
		
		// Tries to open the file with PDFbox to ensure content is well-formed PDF
		try {
			PDFParser parser = new PDFParser(pdfReader.getContentInputStream());
			parser.parse();
			COSDocument doc = parser.getDocument();
			Assert.assertNotNull(doc);
			doc.close();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	public void testGetReliability() {
		double result = converter.getReliability(
				MimetypeMap.MIMETYPE_IMAGE_JPEG, MimetypeMap.MIMETYPE_PDF);
		Assert.assertEquals(1.0, result);
	}

	public void testGetReliabilityPng() {
		double result = converter.getReliability(
				MimetypeMap.MIMETYPE_IMAGE_PNG, MimetypeMap.MIMETYPE_PDF);
		Assert.assertEquals(0.0, result);
	}

	public void testGetReliabilityDoc() {
		double result = converter.getReliability(
				MimetypeMap.MIMETYPE_IMAGE_JPEG, MimetypeMap.MIMETYPE_WORD);
		Assert.assertEquals(0.0, result);
	}

	public void testGetReliabilityPngDoc() {
		double result = converter.getReliability(
				MimetypeMap.MIMETYPE_IMAGE_PNG, MimetypeMap.MIMETYPE_WORD);
		Assert.assertEquals(0.0, result);
	}

}
