package org.adullact.iparapheur.test.repo.security.permissions.dynamic;

import org.junit.Assert;

import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.security.permissions.dynamic.InitiateurDynamicAuthority;

public class InitiateurDynamicAuthorityTest extends BaseParapheurTest {

    private NodeRef document;
    private NodeRef dossier;
    private NodeRef parapheur;
    private InitiateurDynamicAuthority authority;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        document = new NodeRef("workspace://SpacesStore/document");
        dossier = new NodeRef("workspace://SpacesStore/dossier");
        parapheur = new NodeRef("workspace://SpacesStore/parapheur");

        parapheurService = Mockito.mock(ParapheurService.class);
        Mockito.when(parapheurService.getParentDossier(document)).thenReturn(dossier);
        Mockito.when(parapheurService.getEmetteur(dossier)).thenReturn(parapheur);
        Mockito.when(parapheurService.isParapheurOwner(parapheur, "proprietaire")).thenReturn(true);
        Mockito.when(parapheurService.isParapheurSecretaire(parapheur, "secretaire")).thenReturn(true);

        nodeService = Mockito.mock(NodeService.class);
        Mockito.when(nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(false);

        authority = new InitiateurDynamicAuthority();
        authority.setParapheurService(parapheurService);
        authority.setNodeService(nodeService);
        authority.afterPropertiesSet();
    }

    public void testHasAuthority() {
        boolean returned = authority.hasAuthority(document, "proprietaire");
        Assert.assertTrue(returned);
    }

    public void testHasNotAuthority() {
        boolean returned = authority.hasAuthority(document, "nonProprietaire");
        Assert.assertFalse(returned);
    }

    public void testSecretaireHasNotAuthority() {
        boolean returned = authority.hasAuthority(document, "nonProprietaire");
        Assert.assertFalse(returned);
    }

    public void testSecretaireHasAuthoritySecretariat() {
        Mockito.when(nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);

        boolean returned = authority.hasAuthority(document, "secretaire");
        Assert.assertTrue(returned);
    }

    public void testProprietaireHasNotAuthoritySecretariat() {
        Mockito.when(nodeService.hasAspect(dossier, ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);

        boolean returned = authority.hasAuthority(document, "proprietaire");
        Assert.assertFalse(returned);
    }

    public void testHasNotAuthorityDossierEmis() {
        Mockito.when(parapheurService.isEmis(dossier)).thenReturn(true);

        boolean returned = authority.hasAuthority(document, "proprietaire");
        Assert.assertFalse(returned);
    }

    public void testGetAuthority() {
        String returned = authority.getAuthority();
        Assert.assertEquals(returned, "ROLE_PARAPHEUR_INITIATEUR");
    }

    public void testGetNodeService() {
        NodeService returned = authority.getNodeService();
        Assert.assertNotNull(returned);
    }

     public void testGetParapheurService() {
        ParapheurService returned = authority.getParapheurService();
        Assert.assertNotNull(returned);

     }


}
