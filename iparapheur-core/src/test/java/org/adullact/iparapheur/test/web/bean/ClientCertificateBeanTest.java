/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.iparapheur.test.web.bean;

import com.atolcd.parapheur.web.bean.ClientCertificateBean;
import org.junit.Assert;
import org.adullact.iparapheur.test.WebBeanBaseTest;

/**
 *
 * @author stoulouse
 */
public class ClientCertificateBeanTest extends WebBeanBaseTest {

    ClientCertificateBean monCertif;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();
        monCertif = new ClientCertificateBean();
    }

    public void testGetIssuer() {
        String returned = monCertif.getIssuer();
        Assert.assertEquals("", returned);
    }

    public void testGetCertificateClass() {
        int returned = monCertif.getCertificateClass();
        Assert.assertEquals(-1, returned);
    }
}
