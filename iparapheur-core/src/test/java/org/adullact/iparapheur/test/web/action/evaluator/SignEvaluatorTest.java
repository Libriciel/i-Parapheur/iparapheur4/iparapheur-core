package org.adullact.iparapheur.test.web.action.evaluator;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.SignEvaluator;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class SignEvaluatorTest extends WebUIBaseTest {

    private NodeRef nodeRef;
    private NodeRef parapheurRef;
    private EtapeCircuit userEtape;
    private EtapeCircuit proprietaireEtape;
    private SignEvaluator evaluator;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
        parapheurRef = new NodeRef("workspace://SpacesStore/parapheurRef");

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);

        userEtape = Mockito.mock(EtapeCircuit.class);
        proprietaireEtape = Mockito.mock(EtapeCircuit.class);
        
        Mockito.when(userEtape.isApproved()).thenReturn(true);
        Mockito.when(proprietaireEtape.isApproved()).thenReturn(false);

        Mockito.when(parapheurService.getParentParapheur(nodeRef)).thenReturn(
                parapheurRef);
        Mockito.when(
                parapheurService.isParapheurOwner(parapheurRef,
                tenantService.getDomainUser("proprietaire", tenantName))).thenReturn(true);

      //  Mockito.when( parapheurService.getParentParapheur(nodeRef)).thenReturn(tenantService.getDomainUser("proprietaire", tenantName));
        List<String> proprietaires = new ArrayList<String>();
        proprietaires.add(tenantService.getDomainUser("proprietaire", tenantName));
        Mockito.when(parapheurService.getParapheurOwners(parapheurRef)).thenReturn(proprietaires);

        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);
        List<EtapeCircuit> circuit =new ArrayList<EtapeCircuit>(Arrays.asList(userEtape,
                proprietaireEtape));
        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(circuit);



        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit)).thenReturn(userEtape);
        Mockito.when(userEtape.getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_SIGNATURE);

        Mockito.when(
                nodeService.getProperty(nodeRef,
                ParapheurModel.PROP_SIGNATURE_PAPIER)).thenReturn(false);
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(false);

        Mockito.when(nodeService.exists(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(true);
        
        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);

        evaluator = Mockito.spy(new SignEvaluator());

        Mockito.when(evaluator.isCurrentRequestSecure()).thenReturn(true);
    }
    
    public void testEvaluateNull() {
        boolean returned = evaluator.evaluate(null);
        Assert.assertFalse(returned);
    }

    public void testEvaluateNonExistentNodeRef() {
        boolean returned = evaluator.evaluate(new Node(new NodeRef("workspace://SpacesStore/notInRepoRef")));
        Assert.assertFalse(returned);
    }

    public void testEvaluateExistingNodeNotDossier() {
        NodeRef myNodeRef = new NodeRef("workspace://SpacesStore/notInRepoRef");

        Mockito.when(nodeService.exists(myNodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(myNodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluate() {
        authenticate("proprietaire");
        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateOnNonSecureConnection() {
        Mockito.when(evaluator.isCurrentRequestSecure()).thenReturn(false);
        authenticate("proprietaire");
        boolean returned = evaluator.evaluate(nodeRef);
        Assert.assertFalse(returned);
    }

    public void testEvaluateNotOwner() {
        authenticate("user");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateDerniereEtapeSignPapier() {
        authenticate("proprietaire");
        Mockito.when(
                nodeService.getProperty(nodeRef,
                ParapheurModel.PROP_SIGNATURE_PAPIER)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluatePremiereEtapeSignaturePapier() {
        authenticate("proprietaire");
        Mockito.when(
                nodeService.getProperty(nodeRef,
                ParapheurModel.PROP_SIGNATURE_PAPIER)).thenReturn(true);
        Mockito.when(userEtape.isApproved()).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));

        // signature papier et Signature ne sont plus mutuellement exclusifs
        Assert.assertTrue(returned);
    }

    public void testEvaluateDossierSecretariat() {
        authenticate("proprietaire");
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateDossierTermine() {
        authenticate("proprietaire");
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateSLOW() {


        List<EtapeCircuit> circuit = null;
        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(circuit);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit)).thenReturn(proprietaireEtape);
        Mockito.when(nodeService.hasAspect(nodeRef, ParapheurModel.ASPECT_S2LOW)).thenReturn(true);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_TDT);


        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateObjectInNonSecureRequest() {
        Mockito.when(evaluator.isCurrentRequestSecure()).thenReturn(false);
        authenticate("proprietaire");
        Object monNode = new Node(nodeRef);

        boolean returned = evaluator.evaluate(monNode);
        Assert.assertFalse(returned);
    }

    public void testEvaluateNodeRefInNonSecureRequest() {
        Mockito.when(evaluator.isCurrentRequestSecure()).thenReturn(false);
        authenticate("proprietaire");

        boolean returned = evaluator.evaluate(nodeRef);
        Assert.assertFalse(returned);
    }

    public void testEvaluateObject() {
        authenticate("proprietaire");
        Object monNode = new Node(nodeRef);

        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }
}
