package org.adullact.iparapheur.test.web.bean;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.impl.ParapheurServiceImpl;
import org.junit.Assert;

import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.ui.common.component.UIActionLink;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.web.bean.AdminParapheurBean;
import com.atolcd.parapheur.web.bean.ParapheurBean;

public class AdminParapheurBeanTest extends WebBeanBaseTest {

	private NodeRef parapheurCourant;

	private NodeRef corbeilleCourantAArchiver;

	private NodeRef parapheurEnfant1;

	private NodeRef parapheurEnfant2;

	private NodeRef dossierCourantAArchiver;

	private AdminParapheurBean bean;

	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();

		parapheurCourant = nodeService.createNode(parapheursHome,
				ContentModel.ASSOC_CONTAINS,
				QName.createQName("{test}parapheurCourant"),
				ParapheurModel.TYPE_PARAPHEUR).getChildRef();

		corbeilleCourantAArchiver = nodeService.createNode(parapheurCourant,
				ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_ARCHIVER,
				ParapheurModel.TYPE_CORBEILLE).getChildRef();

		dossierCourantAArchiver = nodeService.createNode(
				corbeilleCourantAArchiver, ContentModel.ASSOC_CONTAINS,
				QName.createQName("{test}dossierCourantAArchiver"),
				ParapheurModel.TYPE_DOSSIER).getChildRef();

		parapheurEnfant1 = nodeService.createNode(parapheursHome,
				ContentModel.ASSOC_CONTAINS,
				QName.createQName("{test}parapheurEnfant1"),
				ParapheurModel.TYPE_PARAPHEUR).getChildRef();
		nodeService.createAssociation(parapheurEnfant1, parapheurCourant,
				ParapheurModel.ASSOC_HIERARCHIE);

		parapheurEnfant2 = nodeService.createNode(parapheursHome,
				ContentModel.ASSOC_CONTAINS,
				QName.createQName("{test}parapheurEnfant2"),
				ParapheurModel.TYPE_PARAPHEUR).getChildRef();
		nodeService.createAssociation(parapheurEnfant2, parapheurCourant,
				ParapheurModel.ASSOC_HIERARCHIE);

		bean = new AdminParapheurBean();
		bean.setAuthenticationService(authenticationService);
		bean.setBrowseBean(Mockito.mock(BrowseBean.class));
		bean.setContentService(contentService);
		bean.setNodeService(nodeService);
		bean.setParapheurBean(Mockito.mock(ParapheurBean.class));


		bean.setParapheurService(parapheurService);

        bean.setS2lowService(s2lowService);
	}

	private void setParapheurCourant(NodeRef parapheurCourant) {
		try {
			Field field = AdminParapheurBean.class
					.getDeclaredField("parapheurCourant");
			field.setAccessible(true);
			field.set(bean, parapheurCourant);
		} catch (Exception e) {
		}
	}

	private void setDossierCourant(NodeRef dossierCourant) {
		try {
			Field field = AdminParapheurBean.class
					.getDeclaredField("dossierCourant");
			field.setAccessible(true);
			field.set(bean, dossierCourant);
		} catch (Exception e) {
		}
	}

	private NodeRef getParapheurCourant() {
		try {
			Field field = AdminParapheurBean.class
					.getDeclaredField("parapheurCourant");
			field.setAccessible(true);
			return (NodeRef) field.get(bean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void testGetParapheurs() {
		setParapheurCourant(parapheurCourant);

		List<Node> returned = bean.getParapheurs();
		Assert.assertEquals(2, returned.size());
	}

	public void testGetParapheursNoParents() {
		setParapheurCourant(null);

		List<Node> returned = bean.getParapheurs();
		Assert.assertEquals(1, returned.size());
	}

	public void testGetParapheursNoChildren() {
		setParapheurCourant(parapheurEnfant1);

		List<Node> returned = bean.getParapheurs();
		Assert.assertEquals(0, returned.size());
	}

	public void testGetDossiers() {
		List<Node> returned = bean.getDossiers();

		Assert.assertEquals(1, returned.size());
		Assert.assertEquals(dossierCourantAArchiver, returned.get(0)
				.getNodeRef());
	}

	public void testClick() {
		UIActionLink link = new UIActionLink();
		link.getParameterMap().put("id", dossierCourantAArchiver.getId());

		bean.clickDossier(new ActionEvent(link));

		Node returned = bean.getDossierNode();
		Assert.assertEquals(dossierCourantAArchiver, returned.getNodeRef());
	}

	public void testClickNoId() {
		UIActionLink link = new UIActionLink();

		bean.clickDossier(new ActionEvent(link));

		Node returned = bean.getDossierNode();
		Assert.assertNull(returned);
	}

	public void testClickParapheur() {
		UIActionLink link = new UIActionLink();
		link.getParameterMap().put("id", parapheurEnfant1.getId());

		bean.clickParapheur(new ActionEvent(link));

		NodeRef returned = getParapheurCourant();
		Assert.assertEquals(parapheurEnfant1, returned);
	}

	public void testClickParapheurNoId() {
		setParapheurCourant(parapheurEnfant1);

		UIActionLink link = new UIActionLink();

		bean.clickParapheur(new ActionEvent(link));

		NodeRef returned = getParapheurCourant();
		Assert.assertEquals(parapheurCourant, returned);
	}
	
	public void testDeleteDossierOK() {
		setDossierCourant(dossierCourantAArchiver);
	    ParapheurService spy = Mockito.mock(ParapheurService.class);
        bean.setParapheurService(spy);
        Mockito.when(spy.getEmetteur(dossierCourantAArchiver)).thenReturn(parapheurCourant);

		bean.deleteDossierOK();
		bean.setParapheurService(parapheurService);
		Assert.assertFalse(nodeService.exists(dossierCourantAArchiver));
	}
	
	public void testDeleteParapheurOK() {
		setParapheurCourant(parapheurCourant);
		
		nodeService.deleteNode(dossierCourantAArchiver);
		
		bean.deleteParapheurOK();
		
		Assert.assertFalse(nodeService.exists(dossierCourantAArchiver));
	}
	
	public void testDeleteParapheurOKNonEmpty() {
		setParapheurCourant(parapheurCourant);
		
		bean.deleteParapheurOK();
		
		Assert.assertTrue(nodeService.exists(dossierCourantAArchiver));
	}

}
