/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.test;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.ApplicationContextHelper;
import org.alfresco.web.app.servlet.AuthenticationHelper;
import org.alfresco.web.bean.repository.Repository;
import org.alfresco.web.bean.repository.User;
import org.apache.shale.test.mock.*;
import org.springframework.web.context.WebApplicationContext;

import javax.faces.FactoryFinder;
import javax.faces.application.ApplicationFactory;
import javax.faces.component.UIViewRoot;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.render.RenderKitFactory;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Initializes a mocked web application context, including all servlet and JSF
 * contexts.
 * <p/>
 * All mock objects are providen by the Shale-test framework.
 *
 * @author Vivien Barousse
 */
public abstract class WebUIBaseTest extends BaseParapheurTest {

    protected static final String TEST_ACCOUNT = "user";
    protected static final String TEST_PASSWORD = "user";

    /**
     * Application mock
     *
     * @see javax.faces.application.Application
     * @see MockApplication
     */
    protected MockApplication application;

    /**
     * ServletConfig mock
     *
     * @see javax.servlet.ServletConfig
     * @see MockServletConfig
     */
    protected MockServletConfig config;

    /**
     * ExternalContext mock
     *
     * @see javax.faces.context.ExternalContext
     * @see MockExternalContext
     */
    protected MockExternalContext externalContext;

    /**
     * FacesContext mock
     *
     * @see javax.faces.context.FacesContext
     * @see MockFacesContext
     */
    protected MockFacesContext facesContext;

    /**
     * FacesContextFactory mock
     *
     * @see javax.faces.context.FacesContextFactory
     * @see MockFacesContextFactory
     */
    protected MockFacesContextFactory facesContextFactory;

    /**
     * Lifecycle mock
     *
     * @see javax.faces.lifecycle.Lifecycle
     * @see MockLifecycle
     */
    protected MockLifecycle lifecycle;

    /**
     * LifecycleFactory mock
     *
     * @see javax.faces.lifecycle.LifecycleFactory
     * @see MockLifecycleFactory
     */
    protected MockLifecycleFactory lifecycleFactory;

    /**
     * RenderKit mock
     *
     * @see javax.faces.render.RenderKit
     * @see MockRenderKit
     */
    protected MockRenderKit renderKit;

    /**
     * HttpServletRequest mock
     *
     * @see javax.servlet.http.HttpServletRequest
     * @see MockHttpServletRequest
     */
    protected MockHttpServletRequest request;

    /**
     * HttpServletResponse mock
     *
     * @see javax.servlet.http.HttpServletResponse
     * @see MockHttpServletResponse
     */
    protected MockHttpServletResponse response;

    /**
     * ServletContext mock
     *
     * @see javax.servlet.ServletContext
     * @see MockServletContext
     */
    protected MockServletContext servletContext;

    /**
     * HttpSession mock
     *
     * @see javax.servlet.http.HttpSession
     * @see MockHttpSession
     */
    protected MockHttpSession session;

    /**
     * Initial context class loader
     * <p/>
     * This class changes the context class loader to mock JSF objects. We store
     * a reference to the old class loader in order to restore it at the end of
     * unit test execution.
     */
    private ClassLoader threadContextClassLoader;

    /**
     * WebApplicationContext mock
     *
     * @see WebApplicationContext
     * @see MockWebApplicationContext
     */
    protected MockWebApplicationContext webApplicationContext;

    /**
     * Mock all needed objects.
     *
     * @throws Exception
     */
    @Override
    protected void onSetUpBeforeTransaction() throws Exception {
        super.onSetUpBeforeTransaction();

        // Set up a new thread context class loader
        threadContextClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(
                new URLClassLoader(new URL[0], this.getClass().getClassLoader()));

        // Set up Servlet API Objects
        servletContext = new MockServletContext();
        config = new MockServletConfig(servletContext);
        session = new MockHttpSession();
        session.setServletContext(servletContext);
        request = new MockHttpServletRequest(session) {

            @Override
            public boolean isSecure() {
                return true;
            }
        };
        request.setServletContext(servletContext);
        response = new MockHttpServletResponse();

        // Set up JSF API Objects
        FactoryFinder.releaseFactories();
        FactoryFinder.setFactory(FactoryFinder.APPLICATION_FACTORY, "org.apache.shale.test.mock.MockApplicationFactory");
        FactoryFinder.setFactory(FactoryFinder.FACES_CONTEXT_FACTORY, "org.apache.shale.test.mock.MockFacesContextFactory");
        FactoryFinder.setFactory(FactoryFinder.LIFECYCLE_FACTORY, "org.apache.shale.test.mock.MockLifecycleFactory");
        FactoryFinder.setFactory(FactoryFinder.RENDER_KIT_FACTORY, "org.apache.shale.test.mock.MockRenderKitFactory");

        externalContext = new MockExternalContext(servletContext, request,
                                                  response);
        lifecycleFactory = (MockLifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
        lifecycle = (MockLifecycle) lifecycleFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);
        facesContextFactory = (MockFacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
        facesContext = (MockFacesContext) facesContextFactory.getFacesContext(
                servletContext, request, response, lifecycle);
        externalContext = (MockExternalContext) facesContext.getExternalContext();
        UIViewRoot root = new UIViewRoot();
        root.setViewId("/viewId");
        root.setRenderKitId(RenderKitFactory.HTML_BASIC_RENDER_KIT);
        facesContext.setViewRoot(root);
        ApplicationFactory applicationFactory = (ApplicationFactory) FactoryFinder.getFactory(FactoryFinder.APPLICATION_FACTORY);
        application = (MockApplication) applicationFactory.getApplication();
        facesContext.setApplication(application);
        RenderKitFactory renderKitFactory = (RenderKitFactory) FactoryFinder.getFactory(FactoryFinder.RENDER_KIT_FACTORY);
        renderKit = new MockRenderKit();
        renderKitFactory.addRenderKit(RenderKitFactory.HTML_BASIC_RENDER_KIT,
                                      renderKit);

        webApplicationContext = new MockWebApplicationContext(
                ApplicationContextHelper.getApplicationContext(),
                servletContext);

        servletContext.setAttribute(
                WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE,
                webApplicationContext);
    }

    /**
     * Create authentications for common users :
     * - user
     * - proprietaire
     * - secretaire
     *
     * @throws Exception
     */
    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        if (!authenticationService.authenticationExists(tenantService.getDomainUser(TEST_ACCOUNT, tenantName))) {
            authenticationService.createAuthentication(tenantService.getDomainUser(TEST_ACCOUNT, tenantName), TEST_PASSWORD.toCharArray());
            authenticationService.createAuthentication(tenantService.getDomainUser("proprietaire", tenantName),
                                                       "proprietaire".toCharArray());
            authenticationService.createAuthentication(tenantService.getDomainUser("secretaire", tenantName), "secretaire".toCharArray());
        }

        Repository.getStoreRef(servletContext);
    }

    /**
     * This method helps a unit test to register a managed bean to the faces
     * context.
     *
     * @param name  The managed bean name
     * @param value The managed bean itself
     */
    protected void setUpManagedBean(String name, Object value) {
        application.createValueBinding(name).setValue(facesContext, value);
    }

    /**
     * This method hepls unit test to authenticate a user on the system.
     * <p/>
     * The new authentication is done at different levels:
     * - On the authentication component, to change authentication in services
     * - On the session map, to change authentication in web beans
     *
     * @param username
     */
    protected void authenticate(String username) {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser(username, tenantName));
        String ticket = authenticationService.getCurrentTicket();
        NodeRef userRef = personService.getPerson(tenantService.getDomainUser(username, tenantName));

        User user = new User(tenantService.getDomainUser(username, tenantName), ticket, userRef);
        user.setCompanyRootId(rootNodeRef.getId());
        user.setCompanyRootId(rootNodeRef.getId());
        externalContext.getSessionMap().put(AuthenticationHelper.AUTHENTICATION_USER, user);
    }

    /**
     * Restore old class loader, and release all JSF mocked objects
     *
     * @throws Exception
     */
    @Override
    protected void onTearDownAfterTransaction() throws Exception {
        super.onTearDownAfterTransaction();

        facesContext.release();
        FactoryFinder.releaseFactories();
        Thread.currentThread().setContextClassLoader(threadContextClassLoader);
    }
}
