/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.test;

import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.repo.impl.S2lowServiceImpl;
import com.itextpdf.text.pdf.BaseFont;
import junit.framework.AssertionFailedError;
import org.alfresco.repo.node.StoreArchiveMap;
import org.alfresco.repo.node.archive.NodeArchiveService;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.repo.tenant.TenantAdminService;
import org.alfresco.repo.tenant.TenantService;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.util.BaseAlfrescoSpringTest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe de base pour tous les tests unitaires relatifs au parapheur.
 *
 * Cette classe modifie le chemins d'accès aux fichiers de contexte Spring, afin
 * de charger :
 * - application-context.xml
 * - web-client-application-context.xml
 *
 * De plus, cette classe crée un tenant "unit-tests-tenant" afin que tous les
 * tests unitaires décendant de cette classe soient exécutés dans un tenant
 * autre que le tenant par défaut.
 *
 * Enfin, des références vers les différents services du parapheur et d'Alfresco
 * sont crées afin de pouvoir réutiliser ces services dans les sous classes.
 *
 * @author Vivien Barousse
 */
public abstract class BaseParapheurTest extends BaseAlfrescoSpringTest {

    // Updates Spring context files.
//    static {
//        try {
//            Class<ApplicationContextHelper> klass = ApplicationContextHelper.class;
//            Field field = klass.getDeclaredField("instance");
//            field.setAccessible(true);
//            field.set(null, new ClassPathXmlApplicationContext(new String[]{
//                        "classpath:alfresco/application-context.xml",
//                        "classpath:alfresco/web-client-application-context.xml"}));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * Parapheur S2low service
     */
    protected S2lowService s2lowService;

    /**
     * Parapheur main service
     */
    protected ParapheurService parapheurService;

    /**
     * Alfresco search service
     * @see SearchService
     */
    protected SearchService searchService;

    /**
     * Alfresco namespace service
     * @see NamespaceService
     */
    protected NamespaceService namespaceService;

    /**
     * Properties manager used to retrieve some properties from Alfresco
     * configuration files.
     * @see PropertiesManager
     */
    protected PropertiesManager propertiesManager;

    /**
     * Alfresco authentication component
     * @see AuthenticationComponent
     */
    protected AuthenticationComponent authenticationComponent;

    /**
     * Alfresco file folder service
     * @see FileFolderService
     */
    protected FileFolderService fileFolderService;

    /**
     * Alfresco person service
     * @see PersonService
     */
    protected PersonService personService;

    /**
     * Alfresco node archive service
     * @see NodeArchiveService
     */
    protected NodeArchiveService nodeArchiveService;

    /**
     * Store containing Alfresco archives
     */
    protected StoreRef archiveStore;

    /**
     * Alfresco tenant admin service
     * @see TenantAdminService
     */
    protected TenantAdminService tenantAdminService;

    /**
     * Alfresco tenant service
     * @see TenantService
     */
    protected TenantService tenantService;

    /**
     * Alfresco authority service
     * @see AuthorityService
     */
    protected AuthorityService authorityService;

    /**
     * Alfresco permission service
     * @see PermissionService
     */
    protected PermissionService permissionService;

    /**
     * Dossier main service
     */
    protected DossierService dossierService;

    /**
     * Current tenant name
     */
    protected String tenantName;


    /**
     * Initializes current test.
     *
     * @throws Exception
     */
    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        searchService = (SearchService) applicationContext.getBean("searchService");
        namespaceService = (NamespaceService) applicationContext.getBean("namespaceService");
        fileFolderService = (FileFolderService) applicationContext.getBean("fileFolderService");
        s2lowService = (S2lowService) applicationContext.getBean("s2lowService");
        parapheurService = (ParapheurService) applicationContext.getBean("parapheurService");
        authenticationComponent = (AuthenticationComponent) applicationContext.getBean("authenticationComponent");
        personService = (PersonService) applicationContext.getBean("personService");
        nodeArchiveService = (NodeArchiveService) applicationContext.getBean("nodeArchiveService");
        tenantAdminService = (TenantAdminService) applicationContext.getBean("tenantAdminService");
        tenantService = (TenantService) applicationContext.getBean("tenantService");
        authorityService = (AuthorityService) applicationContext.getBean("authorityService");
        permissionService = (PermissionService) applicationContext.getBean("permissionService");
        propertiesManager = (PropertiesManager) applicationContext.getBean("propertiesManager");

        tenantName = "unit-tests-tenant";
        if (!tenantAdminService.existsTenant(tenantName)) {
                tenantAdminService.createTenant(tenantName, "admin".toCharArray());
            }

        setComplete();
        endTransaction();
        startNewTransaction();

        authenticationComponent.setCurrentUser(tenantService.getDomainUser("admin", tenantName));

        archiveStore = nodeService.createStore(StoreRef.PROTOCOL_ARCHIVE, tenantService.getName(storeRef).getIdentifier());
        storeRef = nodeService.createStore(StoreRef.PROTOCOL_WORKSPACE, tenantService.getName(storeRef).getIdentifier());
        rootNodeRef = nodeService.getRootNode(storeRef);

        parapheurService.getConfiguration().setProperty("spaces.store",
                storeRef.toString());
        ((S2lowServiceImpl) s2lowService).getConfiguration().setProperty("spaces.store",
                storeRef.toString());

        // Set default font to be used during archive creation
        parapheurService.getConfiguration().setProperty("archive.ttfVerdana.location", BaseFont.HELVETICA_BOLDOBLIQUE);

        StoreArchiveMap archiveMap = (StoreArchiveMap) applicationContext.getBean("storeArchiveMap");
        archiveMap.put(tenantService.getName(authenticationComponent.getCurrentUserName(), storeRef),
                tenantService.getName(authenticationComponent.getCurrentUserName(), archiveStore));
    }

    /**
     * Used to compare two bytes arrays. If arrays are different, an
     * AssertionFailedError is thrown.
     *
     * Two bytes arrays are equals if and only if:
     * - Their size are equals
     * - Each byte at each index are equals
     *
     * @param expected The expected byte array
     * @param actual The actual byte array
     * @throws AssertionFailedError if expected and actual are differents
     */
    public static void assertEquals(byte[] expected, byte[] actual) {
        assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], actual[i]);
        }
    }

    /**
     * Reads the given InputStream and returns all bytes contained in this
     * stream.
     * 
     * @param in The InputStream to read
     * @return stream content
     */
    public static byte[] read(InputStream in) {
        try {
            byte[] buffer = new byte[2048];
            int readed;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            while ((readed = in.read(buffer)) >= 0) {
                out.write(buffer, 0, readed);
            }
            return out.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(BaseParapheurTest.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

}
