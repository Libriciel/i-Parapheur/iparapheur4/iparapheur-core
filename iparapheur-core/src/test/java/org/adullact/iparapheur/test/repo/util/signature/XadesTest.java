package org.adullact.iparapheur.test.repo.util.signature;

import junit.framework.TestCase;
import org.adullact.libersign.util.signature.Xades;
import org.apache.commons.io.IOUtils;
import org.apache.xerces.parsers.DOMParser;
import org.apache.xpath.XPathAPI;
import org.junit.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.springframework.extensions.surf.util.Base64;

import java.io.*;


public class XadesTest extends TestCase {

    public void testInjectXadesSigIntoXml() throws Exception {

        String xml = "<Test xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\">" +
                     "<Sig Id=\"Id\">" +
                     "</Sig>" +
                     "</Test>";

        String sig = "<TestSig><ds:Signature Id=\"Id_SIG_1\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\">" +
                     "<ds:SignedInfo>" +
                     "</ds:SignedInfo>" +
                     "</ds:Signature></TestSig>";

        String base64Sig = Base64.encodeBytes(sig.getBytes());

        String xpath = "/Test/Sig";
        String encoding = "UTF-8";

        // Inject into XML

        InputStream resultInputStream = Xades.injectXadesSigIntoXml(new ByteArrayInputStream(xml.getBytes()), base64Sig, xpath, encoding);
        StringWriter writer = new StringWriter();
        Assert.assertNotNull(resultInputStream);

        try { IOUtils.copy(resultInputStream, writer, null); }
        catch (IOException e) { /* No need to catch */ }

        String returned = writer.toString();
        Assert.assertNotNull(returned);

        // Parse generated XML

        DOMParser domParser = new DOMParser();
        domParser.parse(new InputSource(new StringReader(returned)));
        Document doc = domParser.getDocument();
        NodeList list = XPathAPI.selectNodeList(doc.getDocumentElement(),
                                                "/Test/Sig[@Id='Id']/ds:Signature[@Id='Id_SIG_1']/ds:SignedInfo",
                                                doc.getDocumentElement().getFirstChild().getFirstChild());
        Assert.assertNotNull(list);
        Assert.assertEquals(1, list.getLength());
    }

    public void testGetNodeFromIdAndXpath() throws Exception {
        String xml = "<Test Id=\"Id\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\">" +
                     "<Sig>" +
                     "</Sig>" +
                     "</Test>";
        DOMParser parser = new DOMParser();
        parser.parse(new InputSource(new StringReader(xml)));
        Document doc = parser.getDocument();

        NodeList list = Xades.getNodeFromIdAndXpath(doc, "Id", "Sig");

        Assert.assertNotNull(list);
        Assert.assertEquals(1, list.getLength());
    }

    public void testGetExistingSig() throws Exception {
        DOMParser parser = new DOMParser();
        parser.parse(new InputSource(this.getClass().getResourceAsStream("/samples/xades-signe.xml")));
        Document doc = parser.getDocument();

        NodeList list = Xades.getExistingSig(doc, "N65536");

        Assert.assertNotNull(list);
        Assert.assertEquals(1, list.getLength());

    }

}
