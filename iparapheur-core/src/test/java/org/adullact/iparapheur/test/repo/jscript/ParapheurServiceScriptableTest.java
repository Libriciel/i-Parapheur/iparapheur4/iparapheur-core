package org.adullact.iparapheur.test.repo.jscript;

import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import org.junit.Assert;
import junit.framework.TestCase;
import org.adullact.iparapheur.repo.jscript.ParapheurServiceScriptable;
import org.alfresco.service.cmr.repository.NodeRef;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

public class ParapheurServiceScriptableTest extends TestCase {

	private NodeRef nodeRef;
	
	private NodeRef expectedNodeRef;
	
	private ParapheurService service;

	private ParapheurServiceScriptable scriptable;

	public void setUp() {
		nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
		expectedNodeRef = new NodeRef("workspace://SpacesStore/expectedNodeRef");
		
		service = Mockito.mock(ParapheurService.class);
		
		scriptable = new ParapheurServiceScriptable();
		scriptable.setParapheurService(service);
	}

	public void testIsDossierTrue() {
		Mockito.when(service.isDossier(nodeRef)).thenReturn(true);
		
		Assert.assertEquals(true, scriptable.isDossier(nodeRef));
	}

	public void testIsDossierFalse() {
		Mockito.when(service.isDossier(nodeRef)).thenReturn(false);
		
		Assert.assertEquals(false, scriptable.isDossier(nodeRef));
	}

	public void testGetParentParapheur() {
		Mockito.when(service.getParentParapheur(nodeRef)).thenReturn(expectedNodeRef);
		
		Assert.assertEquals(expectedNodeRef, scriptable.getParentParapheur(nodeRef));
	}

    /*
	public void testGetParapheurOwner() {
		//Mockito.when(service.getParapheurOwner(nodeRef)).thenReturn("owner");

        throw new RuntimeException("FIXME !!!!!!");
		//Assert.assertEquals("owner", scriptable.getParapheurOwner(nodeRef));
	}
	*/

	public void testGetCircuit() {
		List<EtapeCircuit> expectedCircuit = Arrays.asList(Mockito.mock(EtapeCircuit.class));
		Mockito.when(service.getCircuit(nodeRef)).thenReturn(expectedCircuit);
		
		Assert.assertEquals(expectedCircuit, scriptable.getCircuit(nodeRef));
	}

	public void testGetActeurCourant() {
		Mockito.when(service.getActeurCourant(nodeRef)).thenReturn("acteurCourant");
		
		Assert.assertEquals("acteurCourant", scriptable.getActeurCourant(nodeRef));
	}

	public void testIsActeurCourantTrue() {
		Mockito.when(service.isActeurCourant(nodeRef, "acteurCourant")).thenReturn(true);
		
		Assert.assertEquals(true, scriptable.isActeurCourant(nodeRef, "acteurCourant"));
	}

	public void testIsActeurCourantFalse() {
		Mockito.when(service.isActeurCourant(nodeRef, "acteurCourant")).thenReturn(false);
		
		Assert.assertEquals(false, scriptable.isActeurCourant(nodeRef, "acteurCourant"));
	}

	public void testGetAnnotationPublique() {
		Mockito.when(service.getAnnotationPublique(nodeRef)).thenReturn("annotationPublique");
		
		Assert.assertEquals("annotationPublique", scriptable.getAnnotationPublique(nodeRef));
	}

	public void testGetAnnotationPrivee() {
		Mockito.when(service.getAnnotationPrivee(nodeRef)).thenReturn("annotationPrivee");
		
		Assert.assertEquals("annotationPrivee", scriptable.getAnnotationPrivee(nodeRef));
	}

	public void testGetAnnotationPriveePrecedente() {
		Mockito.when(service.getAnnotationPriveePrecedente(nodeRef)).thenReturn("annotationPriveePrecedente");
		
		Assert.assertEquals("annotationPriveePrecedente", scriptable.getAnnotationPriveePrecedente(nodeRef));
	}

}
