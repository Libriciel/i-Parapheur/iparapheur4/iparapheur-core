package org.adullact.iparapheur.test.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;


import java.util.HashMap;
import java.util.Map;
import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.NavigationBean;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.Repository;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.web.bean.DisplayParapheurDetails;

public class DisplayParapheurDetailsTest extends WebBeanBaseTest {

    private NodeRef parapheurCourant;
    private NodeRef dossierCourant;
    private DisplayParapheurDetails bean;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        ArrayList<String> ownersPhC = new ArrayList<String>();
               ownersPhC.add(tenantService.getDomainUser("user", tenantName));

        parapheurCourant = parapheurService.createParapheur(Collections.<QName, Serializable>singletonMap(ContentModel.PROP_NAME,
                "parapheurCourant"));
        nodeService.setProperty(parapheurCourant, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPhC);

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        properties.put(ContentModel.PROP_NAME, "dossierCourant");
        dossierCourant = dossierService.createDossier(parapheurCourant, properties);

        BrowseBean browseBean = Mockito.mock(BrowseBean.class);
        Mockito.when(browseBean.getActionSpace()).thenReturn(new Node(dossierCourant));

        bean = new DisplayParapheurDetails();
        bean.setBrowseBean(browseBean);
        bean.setDictionaryService(Repository.getServiceRegistry(facesContext).getDictionaryService());
        bean.setNavigator(new NavigationBean());
        bean.setNodeService(nodeService);
        bean.setParapheurService(parapheurService);
        bean.setPersonService(personService);
        bean.setSearchService(searchService);
    }

    // Empty test to prevent JUnit to fail
    public void testNothing() {

    }
}
