/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.test.model;

import org.junit.Assert;
import junit.framework.TestCase;

import com.atolcd.parapheur.model.ParapheurModel;

/**
 * Test class for the ParapheurModel interface.
 * 
 * @author Vivien Barousse
 */
public class ParapheurModelTest extends TestCase {

    /**
     * This test asserts that all constants defined in the ParapheurModel
     * interface are set, and that none is left null.
     */
    public void testConstantsNotNull() {
        Assert.assertNotNull(ParapheurModel.ASPECT_EMAIL_LISTENER);
        Assert.assertNotNull(ParapheurModel.ASPECT_LU);
        Assert.assertNotNull(ParapheurModel.ASPECT_S2LOW);
        Assert.assertNotNull(ParapheurModel.ASPECT_SECRETARIAT);
        Assert.assertNotNull(ParapheurModel.ASPECT_SIGNED);
        Assert.assertNotNull(ParapheurModel.ASPECT_TYPAGE_METIER);

        Assert.assertNotNull(ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE);
        Assert.assertNotNull(ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE);

        Assert.assertNotNull(ParapheurModel.NAME_A_ARCHIVER);
        Assert.assertNotNull(ParapheurModel.NAME_A_TRAITER);
        Assert.assertNotNull(ParapheurModel.NAME_EN_COURS);
        Assert.assertNotNull(ParapheurModel.NAME_EN_PREPARATION);
        Assert.assertNotNull(ParapheurModel.NAME_RECUPERABLES);
        Assert.assertNotNull(ParapheurModel.NAME_RETOURNES);
        Assert.assertNotNull(ParapheurModel.NAME_SECRETARIAT);

        Assert.assertNotNull(ParapheurModel.PARAPHEUR_MODEL_PREFIX);
        Assert.assertNotNull(ParapheurModel.PARAPHEUR_MODEL_URI);

        Assert.assertNotNull(ParapheurModel.PROP_ANNOTATION);
        Assert.assertNotNull(ParapheurModel.PROP_ANNOTATION_PRIVEE);
        Assert.assertNotNull(ParapheurModel.PROP_ARACTE_XML);
        Assert.assertNotNull(ParapheurModel.PROP_CONFIDENTIEL);
        Assert.assertNotNull(ParapheurModel.PROP_DATE_LIMITE);
        Assert.assertNotNull(ParapheurModel.PROP_DATE_VALIDATION);
        Assert.assertNotNull(ParapheurModel.PROP_EFFECTUEE);
        Assert.assertNotNull(ParapheurModel.PROP_EMAIL_ENABLED);
        Assert.assertNotNull(ParapheurModel.PROP_EMAILFOLDER);
        Assert.assertNotNull(ParapheurModel.PROP_EMAILPASSWORD);
        Assert.assertNotNull(ParapheurModel.PROP_EMAILPORT);
        Assert.assertNotNull(ParapheurModel.PROP_EMAILPROTOCOL);
        Assert.assertNotNull(ParapheurModel.PROP_EMAILSERVER);
        Assert.assertNotNull(ParapheurModel.PROP_EMAILUSERNAME);
        Assert.assertNotNull(ParapheurModel.PROP_LISTE_DIFFUSION);
        Assert.assertNotNull(ParapheurModel.PROP_ORDERED_CHILDREN);
        Assert.assertNotNull(ParapheurModel.PROP_ORIGINAL);
        Assert.assertNotNull(ParapheurModel.PROP_ORIGINAL_NAME);
        Assert.assertNotNull(ParapheurModel.PROP_PASSE_PAR);
        Assert.assertNotNull(ParapheurModel.PROP_PROPRIETAIRE_PARAPHEUR);
        Assert.assertNotNull(ParapheurModel.PROP_PUBLIC);
        Assert.assertNotNull(ParapheurModel.PROP_RECUPERABLE);
        Assert.assertNotNull(ParapheurModel.PROP_SECRETAIRES);
        Assert.assertNotNull(ParapheurModel.PROP_SIG);
        Assert.assertNotNull(ParapheurModel.PROP_SIGNATAIRE);
        Assert.assertNotNull(ParapheurModel.PROP_SIGNATURE);
        Assert.assertNotNull(ParapheurModel.PROP_SIGNATURE_ELECTRONIQUE);
        Assert.assertNotNull(ParapheurModel.PROP_SIGNATURE_PAPIER);
        Assert.assertNotNull(ParapheurModel.PROP_SOUSTYPE_METIER);
        Assert.assertNotNull(ParapheurModel.PROP_STATUS);
        Assert.assertNotNull(ParapheurModel.PROP_STATUS_METIER);
        Assert.assertNotNull(ParapheurModel.PROP_TDT_FICHIER_CONFIG);
        Assert.assertNotNull(ParapheurModel.PROP_TDT_NOM);
        Assert.assertNotNull(ParapheurModel.PROP_TDT_PROTOCOLE);
        Assert.assertNotNull(ParapheurModel.PROP_TERMINE);
        Assert.assertNotNull(ParapheurModel.PROP_TRANSACTION_ID);
        Assert.assertNotNull(ParapheurModel.PROP_TRANSACTION_ID);
        Assert.assertNotNull(ParapheurModel.PROP_TYPE_METIER);
        Assert.assertNotNull(ParapheurModel.PROP_TYPE_SIGNATURE);
        Assert.assertNotNull(ParapheurModel.PROP_VISUEL_PDF);
        Assert.assertNotNull(ParapheurModel.PROP_WS_EMETTEUR);
        Assert.assertNotNull(ParapheurModel.PROP_XPATH_SIGNATURE);

        Assert.assertNotNull(ParapheurModel.TYPE_CORBEILLE);
        Assert.assertNotNull(ParapheurModel.TYPE_CORBEILLE_VIRTUELLE);
        Assert.assertNotNull(ParapheurModel.TYPE_DOCUMENT);
        Assert.assertNotNull(ParapheurModel.TYPE_DOSSIER);
        Assert.assertNotNull(ParapheurModel.TYPE_ETAPE_CIRCUIT);
        Assert.assertNotNull(ParapheurModel.TYPE_PARAPHEUR);
        Assert.assertNotNull(ParapheurModel.TYPE_PARAPHEURS);
    }

}
