package org.adullact.iparapheur.test.web.action.evaluator;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.MoveEvaluator;

public class MoveEvaluatorTest extends WebUIBaseTest {

    private NodeRef nodeRef;
    private MoveEvaluator evaluator;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");

        parapheurService = Mockito.mock(ParapheurService.class);

        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);

        setUpManagedBean("ParapheurService", parapheurService);

        evaluator = new MoveEvaluator();
    }

    public void testEvaluate() {
        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateFalse() {
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateObject() {

        Object monNode = new Node(nodeRef);
        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }
}
