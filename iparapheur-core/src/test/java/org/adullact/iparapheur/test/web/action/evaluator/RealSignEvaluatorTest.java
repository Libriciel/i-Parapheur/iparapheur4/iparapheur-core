package org.adullact.iparapheur.test.web.action.evaluator;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;
import java.util.List;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.RealSignEvaluator;

public class RealSignEvaluatorTest extends WebUIBaseTest {

    private NodeRef nodeRef;
    private NodeRef proprietaireParapheur;
    private NodeRef userParapheur;
    private EtapeCircuit proprietaireEtape;
    private EtapeCircuit userEtape;
    private RealSignEvaluator evaluator;
     List<EtapeCircuit> circuit ;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

       
        
        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
        proprietaireParapheur = new NodeRef(
                "workspace://SpacesStore/proprietaireParapheur");
        userParapheur = new NodeRef("workspace://SpacesStore/userParapheur");

        proprietaireEtape = Mockito.mock(EtapeCircuit.class);
        userEtape = Mockito.mock(EtapeCircuit.class);
        
         circuit = new ArrayList<EtapeCircuit>(Arrays.asList(userEtape,
                proprietaireEtape));

//        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(circuit);

        Mockito.when(proprietaireEtape.isApproved()).thenReturn(false);
        Mockito.when(userEtape.isApproved()).thenReturn(true);

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);

        Mockito.when(parapheurService.getParentParapheur(nodeRef)).thenReturn(
                proprietaireParapheur);
        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(
                new ArrayList<EtapeCircuit>(Arrays.asList(userEtape,
                proprietaireEtape)));
        Mockito.when(parapheurService.getCurrentEtapeCircuit(nodeRef)).thenReturn(proprietaireEtape);
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);
        Mockito.when(
                parapheurService.isParapheurOwner(proprietaireParapheur,
                tenantService.getDomainUser("proprietaire", tenantName))).thenReturn(true);
        Mockito.when(
                parapheurService.isParapheurSecretaire(proprietaireParapheur,
                tenantService.getDomainUser("secretaire", tenantName))).thenReturn(true);

        Mockito.when(
                nodeService.getProperty(nodeRef,
                ParapheurModel.PROP_SIGNATURE_PAPIER)).thenReturn(true);
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(false);

        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit)).thenReturn(proprietaireEtape);
//        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_SIGNATURE);

        Mockito.when(nodeService.exists(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(true);
        
        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);

        evaluator = new RealSignEvaluator();
    }
    public void testEvaluateNull() {
        boolean returned = evaluator.evaluate(null);
        Assert.assertFalse(returned);
    }

    public void testEvaluateNonExistentNodeRef() {
        boolean returned = evaluator.evaluate(new Node(new NodeRef("workspace://SpacesStore/notInRepoRef")));
        Assert.assertFalse(returned);
    }

    public void testEvaluateExistingNodeNotDossier() {
        NodeRef myNodeRef = new NodeRef("workspace://SpacesStore/notInRepoRef");

        Mockito.when(nodeService.exists(myNodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(myNodeRef));
        Assert.assertFalse(returned);
    }
    
    public void testEvaluate() {
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_SIGNATURE);

        authenticate("proprietaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateSecretaire() {
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_SIGNATURE);

        authenticate("secretaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateUser() {
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_SIGNATURE);

        authenticate("user");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateNonDerniereEtape() {

        
         Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_TDT);

        authenticate("proprietaire");
        Mockito.when(userEtape.isApproved()).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateDossierTermine() {
        authenticate("proprietaire");
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateSignatureNonPapier() {
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_SIGNATURE);

        authenticate("proprietaire");
        Mockito.when(
                nodeService.getProperty(nodeRef,
                ParapheurModel.PROP_SIGNATURE_PAPIER)).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateSecretariat() {
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_SIGNATURE);

        authenticate("secretaire");
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateObject() {
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_SIGNATURE);

        authenticate("proprietaire");

        Object monNode = new Node(nodeRef);
        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }

    public void testEvaluateActionDemandee() {
        authenticate("proprietaire");
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(nodeRef).getActionDemandee()).thenReturn(EtapeCircuit.ETAPE_SIGNATURE);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

     public void testEvaluateActionDemandeeNull() {
        authenticate("proprietaire");
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(nodeRef).getActionDemandee()).thenReturn(null);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }
}
