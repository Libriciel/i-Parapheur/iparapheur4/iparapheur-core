package org.adullact.iparapheur.test.web.action.evaluator;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.AnnotationEvaluator;

public class AnnotationEvaluatorTest extends WebUIBaseTest {

    private AnnotationEvaluator evaluator;
    private NodeRef nodeRef;
    private NodeRef parapheurRef;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
        parapheurRef = new NodeRef("workspace://SpacesStore/parapheurRef");

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);

        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);
        Mockito.when(parapheurService.getParentParapheur(nodeRef)).thenReturn(
                parapheurRef);
        Mockito.when(
                parapheurService.isParapheurOwner(parapheurRef,
                tenantService.getDomainUser("proprietaire", tenantName))).thenReturn(true);
        Mockito.when(
                parapheurService.isParapheurSecretaire(parapheurRef, tenantService.getDomainUser("secretaire", tenantName))).thenReturn(true);

        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);

        evaluator = new AnnotationEvaluator();
    }

    public void testEvaluateProprietaire() {
        authenticate("proprietaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateProprietaireSecretariat() {
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);
        authenticate("proprietaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateSecretaire() {
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);
        authenticate("secretaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateSecretaireNonSecretariat() {
        authenticate("secretaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateUser() {
        authenticate("user");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateObject() {
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);
        authenticate("secretaire");

        Object monNode = new Node(nodeRef);
        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }

//    public void testEvaluateAspectSecretariat() {
//
//        nodeService.addAspect(nodeRef, ParapheurModel.ASPECT_SECRETARIAT, null);
//    }
}
