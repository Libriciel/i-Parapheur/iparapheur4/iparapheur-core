/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adullact.iparapheur.test.web.action.evaluator;

import com.atolcd.parapheur.web.action.evaluator.GestionnaireCircuitsEvaluator;
import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.NavigationBean;
import org.alfresco.web.bean.repository.Node;
import org.junit.Assert;

/**
 *
 * @author stoulouse
 */
public class GestionnaireCircuitsEvaluatorTest extends WebUIBaseTest{

    private GestionnaireCircuitsEvaluator evaluator;
    private NodeRef nodeRef;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        evaluator = new GestionnaireCircuitsEvaluator();

        nodeRef = nodeService.createNode(rootNodeRef,
				ContentModel.ASSOC_CONTAINS,
				QName.createQName("{test}nodeRef"), ContentModel.TYPE_CONTENT)
				.getChildRef();

        setUpManagedBean("NavigationBean", new NavigationBean());
    }

    public void testEvaluate() {
        authenticate("admin");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateObject() {
        authenticate("admin");

        Object monNode = new Node(nodeRef);

        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }

}
