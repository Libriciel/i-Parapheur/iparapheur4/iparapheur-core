package org.adullact.iparapheur.test.web.action.evaluator;

import java.io.IOException;

import com.atolcd.parapheur.repo.ParapheurService;
import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.NavigationBean;
import org.alfresco.web.bean.repository.Node;

import com.atolcd.parapheur.web.action.evaluator.AdminEvaluator;
import org.mockito.Mockito;

public class AdminEvaluatorTest extends WebUIBaseTest {

	private AdminEvaluator evaluator;

	private NodeRef nodeRef;

	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();

		evaluator = new AdminEvaluator();
        parapheurService = Mockito.mock(ParapheurService.class);

		nodeRef = nodeService.createNode(rootNodeRef,
				ContentModel.ASSOC_CONTAINS,
				QName.createQName("{test}nodeRef"), ContentModel.TYPE_CONTENT)
				.getChildRef();
		
		setUpManagedBean("NavigationBean", new NavigationBean());
        setUpManagedBean("ParapheurService", parapheurService);
	}

	public void testEvaluateAdmin() throws IOException {
		authenticate("admin");
		
		boolean returned = evaluator.evaluate(new Node(nodeRef));
		Assert.assertTrue(returned);
	}

	public void testEvaluateNotAdmin() {
		authenticate("user");
        Mockito.when(parapheurService.isAdministrateurFonctionnel("user@unit-tests-tenant")).thenReturn(false);
		boolean returned = evaluator.evaluate(new Node(nodeRef));
		Assert.assertFalse(returned);
	}

    public void testEvaluateAdminFonctionnel() {
        authenticate("user");
        Mockito.when(parapheurService.isAdministrateurFonctionnel("user@unit-tests-tenant")).thenReturn(true);
        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateAdminObject() throws IOException {
		authenticate("admin");
                Object monNode = new Node(nodeRef);

		boolean returned = evaluator.evaluate(monNode);
		Assert.assertTrue(returned);
	}

}
