/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.test.mt;

import com.atolcd.parapheur.model.ParapheurModel;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.adullact.iparapheur.test.MultiTenantBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

/**
 * This test tries to create a new parapheur in a newly created tenant.
 * 
 * This test ensures that multi tenancy works correctly, and that multiple 
 * parapheurs can be created in multiple tenants.
 *
 * @author Vivien Barousse
 */
public class MultiTenantTest extends MultiTenantBaseTest {

     /**
     * Company home ref
     */
    private NodeRef companyHome;

    /**
     * Parapheurs home ref
     */
    private NodeRef parapheursHome;

    
     protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

     companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.company_home.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

     }
    /**
     * Tries to create a new parapheur in a brand new tenant.
     */
    public void testCreateParapheur() {
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "parapheur");
        NodeRef parapheur = parapheurService.createParapheur(properties);

        Assert.assertNotNull(parapheur);
        Assert.assertEquals("parapheur", nodeService.getProperty(parapheur,
                ContentModel.PROP_NAME));
        Assert.assertNotNull(nodeService.getProperty(parapheur,
                ContentModel.PROP_CREATOR));
        Assert.assertNotNull(nodeService.getProperty(parapheur,
                ContentModel.PROP_CREATED));

        List<ChildAssociationRef> childs = nodeService.getChildAssocs(parapheur);
        Assert.assertTrue(childs.size() >= 7);
    }
}
