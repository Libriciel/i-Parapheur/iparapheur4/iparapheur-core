package org.adullact.iparapheur.test.web.bean.dialog;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.component.UISelectOne;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.web.bean.dialog.MoveDossierDialog;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class MoveDossierDialogTest extends WebBeanBaseTest {

	private NodeRef sourceParapheur;

	private NodeRef destinationParapheur;

	private NodeRef dossierRef;

	private MoveDossierDialog dialog;

	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();

		Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
		properties.put(ContentModel.PROP_NAME, "sourceParapheur");
		properties.put(ContentModel.PROP_TITLE, "sourceParapheur");
		sourceParapheur = parapheurService.createParapheur(properties);

		properties = new HashMap<QName, Serializable>();
		properties.put(ContentModel.PROP_NAME, "destinationParapheur");
		properties.put(ContentModel.PROP_TITLE, "destinationParapheur");
		destinationParapheur = parapheurService.createParapheur(properties);

		properties = new HashMap<QName, Serializable>();
		properties.put(ContentModel.PROP_NAME, "dossierRef");
		properties.put(ContentModel.PROP_TITLE, "dossierRef");
                properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
                properties.put(ParapheurModel.PROP_PUBLIC, true);
		dossierRef = dossierService
				.createDossier(sourceParapheur, properties);

		nodeService.createNode(dossierRef, ContentModel.ASSOC_CONTAINS, QName
				.createQName("{test}document"), ParapheurModel.TYPE_DOCUMENT);

		dialog = new MoveDossierDialog();
		dialog.setNodeService(nodeService);
		dialog.setParapheurService(parapheurService);
	}

	public void testFinish() {
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("id", dossierRef.getId());
                nodeService.setProperty(dossierRef, ParapheurModel.PROP_CONFIDENTIEL, false);
		nodeService.setProperty(dossierRef, ParapheurModel.PROP_PUBLIC, true);

                dialog.init(parameters);

		UISelectOne combo = new UISelectOne();
		combo.setValue(destinationParapheur.toString());
		dialog.setComboParapheurs(combo);

                transactionManager.commit(transactionStatus);
                transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());

		dialog.finish();

		Assert.assertEquals(destinationParapheur, parapheurService
				.getParentParapheur(dossierRef));
	}

}
