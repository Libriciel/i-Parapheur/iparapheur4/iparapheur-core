package org.adullact.iparapheur.test.repo.security.permissions.dynamic;

import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.annotations.Annotation;
import com.atolcd.parapheur.repo.security.permissions.dynamic.CircuitDynamicAuthority;
import org.junit.Assert;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.mockito.Mockito;

import java.util.*;

public class CircuitDynamicAuthorityTest extends BaseParapheurTest {

    private NodeRef document;

    private NodeRef dossier;

    private NodeRef parapheurCircuit1;

    private NodeRef parapheurCircuit2;

    private NodeRef parapheurCircuit3;

    private CircuitDynamicAuthority authority;
    private NodeRef dossierSansCircuit;
    private NodeRef documentSansCircuit;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        document = new NodeRef("workspace://SpacesStore/document");
        dossier = new NodeRef("workspace://SpacesStore/dossier");
        parapheurCircuit1 = new NodeRef(
                "workspace://SpacesStore/parapheurCircuit1");
        parapheurCircuit2 = new NodeRef(
                "workspace://SpacesStore/parapheurCircuit2");
        parapheurCircuit3 = new NodeRef(
                "workspace://SpacesStore/parapheurCircuit3");
        documentSansCircuit = new NodeRef("workspace://SpacesStore/document");
        dossierSansCircuit = new NodeRef("workspace://SpacesStore/dossier");

        parapheurService = Mockito.mock(ParapheurService.class);
        Mockito.when(parapheurService.getParentDossier(document)).thenReturn(
                dossier);
        Mockito.when(parapheurService.getParentDossier(documentSansCircuit)).thenReturn(
                dossierSansCircuit);
        Mockito.when(parapheurService.getCircuit(dossier)).thenReturn(
                Arrays.<EtapeCircuit>asList(new MockEtapeCircuit("PARAPHEUR", parapheurCircuit1, "VISA", Collections.EMPTY_SET),
                new MockEtapeCircuit("PARAPHEUR", parapheurCircuit2, "VISA", Collections.EMPTY_SET),
                new MockEtapeCircuit("PARAPHEUR", parapheurCircuit3, "VISA", Collections.EMPTY_SET)));
        Mockito.when(parapheurService.isEmis(dossier)).thenReturn(true);
        Mockito.when(parapheurService.isEmis(dossierSansCircuit)).thenReturn(true);
        Mockito.when(parapheurService.isParapheurOwner(parapheurCircuit1, "proprietaire1")).thenReturn(true);
        Mockito.when(parapheurService.isParapheurOwner(parapheurCircuit2, "proprietaire2")).thenReturn(true);
        Mockito.when(parapheurService.isParapheurOwner(parapheurCircuit3, "proprietaire3")).thenReturn(true);
        Mockito.when(parapheurService.isParapheurSecretaire(parapheurCircuit1, "secretaire1")).thenReturn(true);
        Mockito.when(parapheurService.isParapheurSecretaire(parapheurCircuit2, "secretaire2")).thenReturn(true);
        Mockito.when(parapheurService.isParapheurSecretaire(parapheurCircuit3, "secretaire3")).thenReturn(true);

        authority = new CircuitDynamicAuthority();
        authority.setParapheurService(parapheurService);
        authority.afterPropertiesSet();
    }

    public void testHasAuthority1() {
        boolean returned = authority.hasAuthority(document, "proprietaire1");
        Assert.assertTrue(returned);
    }

    public void testHasAuthority2() {
        boolean returned = authority.hasAuthority(document, "proprietaire1");
        Assert.assertTrue(returned);
    }

    public void testHasAuthority3() {
        boolean returned = authority.hasAuthority(document, "proprietaire1");
        Assert.assertTrue(returned);
    }

    public void testHasAuthoritySecretaire1() {
        boolean returned = authority.hasAuthority(document, "secretaire1");
        Assert.assertTrue(returned);
    }

    public void testHasAuthoritySecretaire2() {
        boolean returned = authority.hasAuthority(document, "secretaire2");
        Assert.assertTrue(returned);
    }

    public void testHasAuthoritySecretaire3() {
        boolean returned = authority.hasAuthority(document, "secretaire3");
        Assert.assertTrue(returned);
    }

    public void testHasNotAuthority() {
        boolean returned = authority.hasAuthority(document, "nonProprietaire");
        Assert.assertFalse(returned);
    }

    public void testHasAuthorityDossierNonEmis() {
        Mockito.when(parapheurService.isEmis(dossier)).thenReturn(false);

        boolean returned = authority.hasAuthority(document, "proprietaire");
        Assert.assertFalse(returned);
    }

    public void testHasAuthorityDossierSansCircuit() {
        Mockito.when(parapheurService.isEmis(dossierSansCircuit)).thenReturn(false);

        boolean returned = authority.hasAuthority(documentSansCircuit, "proprietaire");
        Assert.assertFalse(returned);
    }

    public void testHasAuthorityCircuitNull() {
        Mockito.when(parapheurService.getCircuit(dossierSansCircuit)).thenReturn(null);

        boolean returned = authority.hasAuthority(documentSansCircuit, "proprietaire");
        Assert.assertFalse(returned);
    }


    private static class MockEtapeCircuit implements EtapeCircuit {

        private NodeRef parapheur;

        private String actionDemandee;

        private Set<NodeRef> listeNotification;

        private String transition;

        public MockEtapeCircuit(String transition, NodeRef parapheur, String actionDemandee, Set<NodeRef> listeNotification) {
            this.transition = transition;
            this.parapheur = parapheur;
            this.actionDemandee = actionDemandee;
            this.listeNotification = listeNotification;
        }

        @Override
        public String getAnnotation() {
            return null;
        }

        @Override
        public String getAnnotationPrivee() {
            return null;
        }

        @Override
        public String getParapheurName() {
            return null;
        }

        @Override
        public Date getDateValidation() {
            return null;
        }

        @Override
        public String getNotificationsExternes() {
            return null;
        }

        @Override
        public NodeRef getParapheur() {
            return parapheur;
        }

        @Override
        public String getSignataire() {
            return null;
        }

        @Override
        public String getSignature() {
            return null;
        }

        @Override
        public boolean isApproved() {
            return false;
        }

        public String getTransition() {
            return transition;
        }

        public String getActionDemandee() {
            return actionDemandee;
        }

        public NodeRef getDelegateur() {
            return null;
        }

        public String getDelegateurName() {
            return null;
        }
        
        public NodeRef getDelegue() {
            return null;
        }

        public String getDelegueName() {
            return null;
        }

        public String getSignatureEtape() {
            return null;
        }

        public Set<NodeRef> getListeNotification() {
            return listeNotification;
        }

        public void setAndRecordListeDiffusion(NodeService nodeService, Set<NodeRef> liste) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public String getValidator() {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void setValidator(String username) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public Map<String, Object> getSignataireCertInfos() {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public HashMap<String, ArrayList<Annotation>> getGraphicalAnnotations() {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public NodeRef getNodeRef() {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public Set<String> getListeMetadatas() {
            return null;
        }

        @Override
        public Set<String> getListeMetadatasRefus() {
            return null;
        }

        @Override
        public boolean isSigned() {
            return false;
        }
    }

}
