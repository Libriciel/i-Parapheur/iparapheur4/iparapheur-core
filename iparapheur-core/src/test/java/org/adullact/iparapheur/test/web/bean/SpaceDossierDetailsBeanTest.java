package org.adullact.iparapheur.test.web.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.atolcd.parapheur.model.ParapheurModel;
import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.NavigationBean;
import org.mockito.Mockito;

import com.atolcd.parapheur.web.bean.SpaceDossierDetailsBean;
import org.junit.Assert;
import org.alfresco.web.bean.repository.Node;

public class SpaceDossierDetailsBeanTest extends WebBeanBaseTest {

    private NodeRef parapheurRef;
    private NodeRef dossier1Ref;
    private NodeRef dossier2Ref;
    private NodeRef dossier3Ref;
    private BrowseBean browseBean;
    private NavigationBean navigator;
    private SpaceDossierDetailsBean bean;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "parapheurRef");
        properties.put(ContentModel.PROP_TITLE, "parapheurRef");
        parapheurRef = parapheurService.createParapheur(properties);

        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "dossier1Ref");
        properties.put(ContentModel.PROP_TITLE, "dossier1Ref");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        dossier1Ref = dossierService.createDossier(parapheurRef, properties);

        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "dossier2Ref");
        properties.put(ContentModel.PROP_TITLE, "dossier2Ref");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        dossier2Ref = dossierService.createDossier(parapheurRef, properties);

        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "dossier3Ref");
        properties.put(ContentModel.PROP_TITLE, "dossier3Ref");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        dossier3Ref = dossierService.createDossier(parapheurRef, properties);

        browseBean = Mockito.mock(BrowseBean.class);
        navigator = Mockito.mock(NavigationBean.class);

        bean = new SpaceDossierDetailsBean();
        bean.setBrowseBean(browseBean);
        bean.setNavigator(navigator);
        bean.setNodeService(nodeService);
        bean.setParapheurService(parapheurService);
    }

    public void testGetId() {
        Mockito.when(browseBean.getActionSpace()).thenReturn(new Node(dossier1Ref));
        String returned = bean.getId();
        Assert.assertEquals(dossier1Ref.getId(), returned);
    }

    public void testGetName() {
        Mockito.when(browseBean.getActionSpace()).thenReturn(new Node(dossier1Ref));
        String returned = bean.getName();
        Assert.assertEquals(nodeService.getProperty(dossier1Ref, ContentModel.PROP_NAME), returned);
    }

    public void testGetSpace() {

        Mockito.when(browseBean.getActionSpace()).thenReturn(new Node(dossier1Ref));
        Node returned = bean.getSpace();
        Assert.assertNotNull(returned);
    }
}
