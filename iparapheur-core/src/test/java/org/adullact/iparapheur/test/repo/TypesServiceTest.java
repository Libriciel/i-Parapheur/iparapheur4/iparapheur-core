    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.iparapheur.test.repo;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.TypesService;
import com.atolcd.parapheur.repo.WorkflowService;
import com.atolcd.parapheur.repo.impl.TypesServiceImpl;
import com.atolcd.parapheur.repo.impl.WorkflowServiceImpl;
import org.junit.Assert;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.util.*;


/**
 *
 * @author niko
 */
public class TypesServiceTest extends BaseParapheurTest {

    protected TypesService typesService ;  
    
    private NodeRef companyHome;
    private NodeRef dictionaryHome;
    private NodeRef workflowsHome;
    private NodeRef parapheursHome;    
    private NodeRef archivesHome;    
    private NodeRef parapheur1;
    private NodeRef parapheur2;
    private NodeRef publicWorkflow;
    private NodeRef workflowProprietaire1;
    
    private WorkflowService workflowService;
    

    
    
    
    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        typesService = (TypesService) applicationContext.getBean("typesService");        
        ((TypesServiceImpl)typesService).setStoreRef(storeRef);
        
        workflowService = (WorkflowService) applicationContext.getBean("workflowService");        
        ((WorkflowServiceImpl)workflowService).setStore(storeRef.toString());
        
          
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("admin", tenantName));
        

        companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.company_home.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();        
        
        dictionaryHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.dictionary.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();        

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        archivesHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.archives.childname"),
                namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        permissionService.setInheritParentPermissions(archivesHome, false);
        permissionService.setPermission(archivesHome, "GROUP_EVERYONE", "Contributor", true);   

        workflowsHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.savedworkflows.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();        
        
        parapheur1 = nodeService.createNode(parapheursHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}parapheur1"),
                ParapheurModel.TYPE_PARAPHEUR).getChildRef();

        ArrayList<String> ownersPh1 = new ArrayList<String>();
        ownersPh1.add(tenantService.getDomainUser("admin", tenantName));
        
        String ownerPh1 = tenantService.getDomainUser("admin", tenantName);
        
        nodeService.setProperty(parapheur1,
                ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPh1);
        nodeService.setProperty(parapheur1,
                ParapheurModel.PROP_PROPRIETAIRE_PARAPHEUR, ownerPh1);
        
        nodeService.setProperty(parapheur1, ContentModel.PROP_TITLE,
                "parapheur1");
        nodeService.setProperty(parapheur1, ContentModel.PROP_NAME,
                "parapheur1");
        parapheur2 = nodeService.createNode(parapheursHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}parapheur2"),
                ParapheurModel.TYPE_PARAPHEUR).getChildRef();

        ArrayList<String> ownersPh2 = new ArrayList<String>();
        ownersPh2.add(tenantService.getDomainUser("admin", tenantName));
        nodeService.setProperty(parapheur2, ContentModel.PROP_TITLE, "parapheur 2");
        
        nodeService.setProperty(parapheur2,
                ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPh2);
        nodeService.setProperty(parapheur2,
                ParapheurModel.PROP_SECRETAIRES, (Serializable) Collections.singletonList(tenantService.getDomainUser("secretaire2", tenantName)));
        nodeService.createAssociation(parapheur2, parapheur1,
                ParapheurModel.ASSOC_HIERARCHIE);

        nodeService.createAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_DELEGATION);

        nodeService.createAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_OLD_DELEGATION);        
        

        
        NodeRef typesFolderNode = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metiertype", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
       
        NodeRef typesNode = nodeService.createNode(typesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:TypesMetier.xml"),
                ContentModel.TYPE_CONTENT).getChildRef();
        
        ContentWriter typesNodeWriter = contentService.getWriter(typesNode,
                ContentModel.PROP_CONTENT, true);
        typesNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        typesNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/TypesMetier_TypesService.xml"));

        NodeRef sousTypesFolderNode = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metiersoustype", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
        
        NodeRef sousTypesNode = nodeService.createNode(sousTypesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metier.xml", namespaceService), 
                ContentModel.TYPE_CONTENT).getChildRef();
        
        nodeService.setProperty(sousTypesNode, ContentModel.PROP_NAME,
                "metier.xml");
        ContentWriter sousTypesNodeWriter = contentService.getWriter(
                sousTypesNode, ContentModel.PROP_CONTENT, true);
        sousTypesNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/SousTypesMetier_TypesService.xml"));
                
        NodeRef sousTypesDefautNode = nodeService.createNode(sousTypesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:defaut.xml", namespaceService), 
                ContentModel.TYPE_CONTENT).getChildRef();
        
        nodeService.setProperty(sousTypesDefautNode, ContentModel.PROP_NAME,
                "defaut.xml");
        ContentWriter sousTypesNodeDefautWriter = contentService.getWriter(
                sousTypesDefautNode, ContentModel.PROP_CONTENT, true);
        sousTypesNodeDefautWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesNodeDefautWriter.putContent(this.getClass().getResourceAsStream("/samples/defaut_TypesService.xml"));   
        
        publicWorkflow = nodeService.createNode(workflowsHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}publicWorkflow"),
                ParapheurModel.TYPE_SAVED_WORKFLOW).getChildRef();
        nodeService.setProperty(publicWorkflow, ContentModel.PROP_NAME,
                "workflowPublic");

        authenticationComponent.setCurrentUser(tenantService.getDomainUser("admin", tenantName));
        workflowProprietaire1 = nodeService.createNode(workflowsHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}proprietaire1workflow"),
                ParapheurModel.TYPE_SAVED_WORKFLOW).getChildRef();
        nodeService.setProperty(workflowProprietaire1, ContentModel.PROP_NAME,
                "workflowProp1");
        nodeService.setProperty(workflowProprietaire1, ContentModel.PROP_CREATOR,
                tenantService.getDomainUser("proprietaire1", tenantName));

        ContentWriter proprietaire1WorkflowWriter = contentService.getWriter(
                workflowProprietaire1, ContentModel.PROP_CONTENT, true);
        proprietaire1WorkflowWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        proprietaire1WorkflowWriter.putContent("<circuit>" + "<validation>" + "<etape>" + parapheur1.toString() + "</etape>" + "<etape>" + parapheur2.toString() + "</etape>" + "</validation>" + "<notification>" + "<etape>" + parapheur2.toString() + "</etape>" + "</notification>" + "</circuit>");
        nodeService.addAspect(workflowProprietaire1, ParapheurModel.ASPECT_PRIVATE_WORKFLOW, null);
        nodeService.setProperty(workflowProprietaire1, ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS, (Serializable) Collections.singletonList(parapheur1.toString()));
        nodeService.setProperty(workflowProprietaire1, ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_GROUPS, (Serializable) Collections.emptyList());
    
        
        
        
        NodeRef savedWorkflowsNode = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:savedworkflows", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
        
        NodeRef savedWorkflowsTestNode = nodeService.createNode(savedWorkflowsNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:workflowTest", namespaceService), 
                ContentModel.TYPE_CONTENT).getChildRef();
        
        nodeService.setProperty(savedWorkflowsTestNode, ContentModel.PROP_NAME,
                "workflowTest");
        ContentWriter savedWorkflowsTestWriter = contentService.getWriter(
                savedWorkflowsTestNode, ContentModel.PROP_CONTENT, true);
        savedWorkflowsTestWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        savedWorkflowsTestWriter.putContent(this.getClass().getResourceAsStream("/samples/testSavedWorkflows_TypesService.xml"));           
        
        
    }

    public void testGetSousTypeWithInnerDoubleSpace() {
       Map<String, Map<String, String>> metadataMap = typesService.getMetadatasMap("defaut", "double  espace");
    }

    public void testIsDigitalSignatureMandatoryTrue(){
        Assert.assertTrue(typesService.isDigitalSignatureMandatory("defaut", "TestSignatureTrue"));
    }
    
    public void testIsDigitalSignatureMandatoryFalse(){
        Assert.assertFalse(typesService.isDigitalSignatureMandatory("defaut", "TestSignatureFalse"));
    }    
    
    public void testIsReadingMandatoryTrue(){
        Assert.assertTrue(typesService.isReadingMandatory("defaut", "TestReadingTrue"));
    }
    
    public void testIsReadingMandatoryFalse(){
        Assert.assertFalse(typesService.isReadingMandatory("defaut", "TestReadingFalse"));
    }    
    
    public void testIsCircuitHierarchiqueVisibleTrue(){
        Assert.assertTrue(typesService.isCircuitHierarchiqueVisible("defaut", "TestCircuitHierarchiqueTrue"));
    }

    public void testIsCircuitHierarchiqueVisibleFalse(){
        Assert.assertFalse(typesService.isCircuitHierarchiqueVisible("defaut", "TestCircuitHierarchiqueFalse"));
    }  
    
    public void testGetListeCalques(){
        Assert.assertTrue(typesService.getListeCalques("defaut", "TestCalques").toString().contains("workspace://SpacesStore/test"));
    }   
    
    
    public void testGetWorkflowCircuit(){
        Assert.assertEquals(publicWorkflow, typesService.getWorkflow("defaut", "TestSavedWorkflows"));        
    }
    

    
    
    public void testGetEditableMetadatas(){
        List<String> returned = typesService.getEditableMetadatas("defaut", "TestMetadata");
        Assert.assertTrue(returned.contains("{http://www.adullact.org/parapheur/metadata/1.0}coin"));
    }
    
    public void testGetMandatoryMetadata(){
        List<String> returned = typesService.getMandatoryMetadatas("defaut", "TestMetadata");
        Assert.assertTrue(returned.contains("{http://www.adullact.org/parapheur/metadata/1.0}coin"));
    }
    
    public void testGetMetadataMap(){        
        Map<String, Map<String, String>> returned  = typesService.getMetadatasMap("defaut", "TestMetadata");

        Map<String, String> props = new HashMap<String, String>();
        
        props.put("default", ""); 
        props.put("mandatory", "true");
        props.put("editable", "true"); 

        
        Map<String, Map<String, String>> results = new HashMap<String, Map<String, String>>();
        
        results.put("{http://www.adullact.org/parapheur/metadata/1.0}coin", props);
        
        Assert.assertEquals(results, returned);
    }
    
    
}
