package org.adullact.iparapheur.test.web.action.evaluator;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.web.action.evaluator.EnvoiS2lowEvaluator;

public class EnvoiS2lowEvaluatorTest extends WebUIBaseTest {

    private NodeRef nodeRef;
    private NodeRef corbeilleRef;
    private NodeRef parapheurRef;
    private EnvoiS2lowEvaluator evaluator;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
        corbeilleRef = new NodeRef("workspace://SpacesStore/corbeilleRef");
        parapheurRef = new NodeRef("workspace://SpacesStore/parapheurRef");

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);
        s2lowService = Mockito.mock(S2lowService.class);

        Mockito.when(s2lowService.isEnabled()).thenReturn(true);
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(true);

        Mockito.when(nodeService.getProperty(nodeRef, ParapheurModel.PROP_TDT_NOM)).thenReturn("S2LOW");

        Mockito.when(parapheurService.isParapheurOwner(parapheurRef, "admin@unit-tests-tenant")).thenReturn(true);

        Mockito.when(parapheurService.getParentParapheur(corbeilleRef)).thenReturn(parapheurRef);

        Mockito.when(parapheurService.getParentCorbeille(nodeRef)).thenReturn(corbeilleRef);
        Mockito.when(nodeService.getPrimaryParent(corbeilleRef)).thenReturn(
                new ChildAssociationRef(ContentModel.ASSOC_CONTAINS,
                parapheurRef, ParapheurModel.NAME_A_ARCHIVER,
                corbeilleRef));
        
        Mockito.when(nodeService.exists(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(true);

        authenticate("admin");

        setUpManagedBean("S2lowService", s2lowService);
        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);

        evaluator = new EnvoiS2lowEvaluator();
    }
    
    public void testEvaluateNull() {
        boolean returned = evaluator.evaluate(null);
        Assert.assertFalse(returned);
    }
    
    public void testEvaluateNonExistentNodeRef() {
        boolean returned = evaluator.evaluate(new Node(new NodeRef("workspace://SpacesStore/notInRepoRef")));
        Assert.assertFalse(returned);
    }
    
    public void testEvaluateExistingNodeNotDossier() {
        NodeRef myNodeRef = new NodeRef("workspace://SpacesStore/notInRepoRef");
        
        Mockito.when(nodeService.exists(myNodeRef)).thenReturn(true);
        
        boolean returned = evaluator.evaluate(new Node(myNodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateS2lowServiceNotEnabled() {
        Mockito.when(s2lowService.isEnabled()).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateNonTermine() {
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateDejaTeletransmis() {
        Mockito.when(
                nodeService.hasAspect(nodeRef, ParapheurModel.ASPECT_S2LOW)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateMauvaiseCorbeille() {
        Mockito.when(nodeService.getPrimaryParent(corbeilleRef)).thenReturn(
                new ChildAssociationRef(ContentModel.ASSOC_CONTAINS,
                parapheurRef, ParapheurModel.NAME_EN_PREPARATION,
                corbeilleRef));

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluate() {

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateObject() {
        Object monNode = new Node(nodeRef);
        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }
}
