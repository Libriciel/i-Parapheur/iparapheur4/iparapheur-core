package org.adullact.iparapheur.test.web.action.evaluator;

import java.util.Arrays;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.RetourEvaluator;

public class RetourEvaluatorTest extends WebUIBaseTest {

    private NodeRef nodeRef;
    private NodeRef parapheurRef;
    private RetourEvaluator evaluator;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
        parapheurRef = new NodeRef("workspace://SpacesStore/parapheurRef");

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);

        Mockito.when(parapheurService.getParentParapheur(nodeRef)).thenReturn(parapheurRef);
        Mockito.when(parapheurService.getSecretariatParapheur(parapheurRef)).thenReturn(Arrays.asList("secretaire"));
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);
        Mockito.when(parapheurService.isParapheurOwner(parapheurRef, tenantService.getDomainUser("proprietaire", tenantName))).thenReturn(true);
        Mockito.when(parapheurService.isParapheurSecretaire(parapheurRef, tenantService.getDomainUser("secretaire", tenantName))).thenReturn(true);

        Mockito.when(nodeService.hasAspect(nodeRef, ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(true);

        Mockito.when(nodeService.exists(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(true);
        
        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);

        evaluator = new RetourEvaluator();
    }
    
    public void testEvaluateNull() {
        boolean returned = evaluator.evaluate(null);
        Assert.assertFalse(returned);
    }

    public void testEvaluateNonExistentNodeRef() {
        boolean returned = evaluator.evaluate(new Node(new NodeRef("workspace://SpacesStore/notInRepoRef")));
        Assert.assertFalse(returned);
    }

    public void testEvaluateExistingNodeNotDossier() {
        NodeRef myNodeRef = new NodeRef("workspace://SpacesStore/notInRepoRef");

        Mockito.when(nodeService.exists(myNodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(myNodeRef));
        Assert.assertFalse(returned);
    }
    

    public void testEvaluate() {
        authenticate("secretaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateOwner() {
        authenticate("proprietaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateOwnerNonSecretariat() {
        authenticate("proprietaire");
        Mockito.when(
                nodeService.hasAspect(nodeRef,
                ParapheurModel.ASPECT_SECRETARIAT)).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateDossierTermine() {
        authenticate("secretaire");
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateObject() {
        authenticate("secretaire");

        Object monNode = new Node(nodeRef);

        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }
}
