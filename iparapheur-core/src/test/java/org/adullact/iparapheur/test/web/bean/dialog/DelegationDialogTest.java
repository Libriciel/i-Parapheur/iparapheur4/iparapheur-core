package org.adullact.iparapheur.test.web.bean.dialog;

import com.atolcd.parapheur.web.bean.ParapheurBean;
import com.atolcd.parapheur.web.bean.dialog.DelegationDialog;
import org.junit.Assert;
import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.mockito.Mockito;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DelegationDialogTest extends WebBeanBaseTest {

    private NodeRef parapheurRef;
    private NodeRef delegationRef;
    private DelegationDialog dialog;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "parapheurRef");
        properties.put(ContentModel.PROP_TITLE, "parapheurRef");
        parapheurRef = parapheurService.createParapheur(properties);

        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "delegationRef");
        properties.put(ContentModel.PROP_TITLE, "delegationRef");
        delegationRef = parapheurService.createParapheur(properties);

        ParapheurBean parapheurBean = Mockito.mock(ParapheurBean.class);
        Mockito.when(parapheurBean.getParapheurCourant()).thenReturn(
                parapheurRef);

        setUpManagedBean("ParapheurBean", parapheurBean);

        dialog = new DelegationDialog();
        dialog.setParapheurService(parapheurService);
    }

    // EPA: pourquoi deux fois finish ?
    public void testFinish() {
        dialog.init(null);
        dialog.setActivated(true);
        dialog.setDelegue(delegationRef);

        transactionManager.commit(transactionStatus);
        transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());

        dialog.finish();
        transactionManager.commit(transactionStatus);

        /*
        dialog.setActivated(true);
        transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());

        dialog.finish();
        */

        Assert.assertEquals(delegationRef, parapheurService.getDelegation(parapheurRef));
	}

}
