package org.adullact.iparapheur.test.repo.util.signature;

import org.junit.Assert;
import junit.framework.TestCase;

import org.adullact.libersign.util.signature.DigestComputer;
import org.adullact.libersign.util.signature.PesDigest;

public class DigestComputerTest extends TestCase {

	public static final byte[] EXPECTED_DIGEST = { 17, 31, -79, -47, 56, -98,
			-59, 126, 87, 89, 72, -92, -76, 17, 45, 35, -115, -33, 3, -58 };

	public void testComputeDigest() {
		PesDigest returned = DigestComputer.computeDigest(this.getClass()
				.getResourceAsStream("/samples/digest_computer.xml"));

		Assert.assertNotNull(returned);
		Assert.assertEquals("12345", returned.getId());
		Assert.assertEquals(EXPECTED_DIGEST.length, returned.getDigest().length);
		for (int i = 0; i < EXPECTED_DIGEST.length; i++) {
			Assert.assertEquals(EXPECTED_DIGEST[i], returned.getDigest()[i]);
		}
	}

}
