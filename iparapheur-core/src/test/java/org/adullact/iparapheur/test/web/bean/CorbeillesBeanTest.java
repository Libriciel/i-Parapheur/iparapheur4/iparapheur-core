package org.adullact.iparapheur.test.web.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.NavigationBean;
import org.mockito.Mockito;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.web.bean.CorbeillesBean;
import com.atolcd.parapheur.web.bean.ParapheurBean;

public class CorbeillesBeanTest extends WebBeanBaseTest {

    private NodeRef parapheurCourant;
    private NodeRef corbeilleATraiter;
    private NodeRef dossierAImprimer;
    private ParapheurBean parapheurBean;
    private CorbeillesBean bean;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "parapheurCourant");
        parapheurCourant = parapheurService.createParapheur(properties);

        corbeilleATraiter = parapheurService.getCorbeille(parapheurCourant,
                ParapheurModel.NAME_A_TRAITER);

        dossierAImprimer = nodeService.createNode(corbeilleATraiter,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}dossierAImprimer"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossierAImprimer, ParapheurModel.PROP_SIGNATURE_PAPIER, true);

        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheurCourant);
        properties.put(ParapheurModel.PROP_EFFECTUEE, true);
        NodeRef etape1 = nodeService.createNode(dossierAImprimer,
                ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheurCourant);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        nodeService.createNode(etape1,
                ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        parapheurBean = Mockito.mock(ParapheurBean.class);
        Mockito.when(parapheurBean.getParapheurCourant()).thenReturn(
                parapheurCourant);

        bean = new CorbeillesBean();
        bean.setBrowseBean(Mockito.mock(BrowseBean.class));
        bean.setNavigator(Mockito.mock(NavigationBean.class));
        bean.setNodeService(nodeService);
        bean.setParapheurBean(parapheurBean);
        bean.setParapheurService(parapheurService);
        bean.setSearchService(searchService);
    }

    public void testGetEmmission() {
        List<Map> returned = bean.getEmission();

        Assert.assertEquals(6, returned.size());
    }
	public void testGetReception() {
		List<Map> returned = bean.getReception();

		Assert.assertEquals(2, returned.size());
	}

	public void testGetSecretariat() {
		List<Map> returned = bean.getSecretariat();

		Assert.assertEquals(3, returned.size());
	}
}
