package org.adullact.iparapheur.test.repo.security.permissions.dynamic;

import org.junit.Assert;

import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.mockito.Mockito;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.security.permissions.dynamic.OwnerDynamicAuthority;

public class OwnerDynamicAuthorityTest extends BaseParapheurTest {
	
	private NodeRef document;
	
	private NodeRef parapheur;
	
	private OwnerDynamicAuthority authority;
	
	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();
		
		document = new NodeRef("workspace://SpacesStore/document");
		parapheur = new NodeRef("workspace://SpacesStore/parapheur");
		
		parapheurService = Mockito.mock(ParapheurService.class);
		Mockito.when(parapheurService.getParentParapheur(document)).thenReturn(parapheur);
		Mockito.when(parapheurService.isParapheurOwner(parapheur, "proprietaire")).thenReturn(true);
		
		authority = new OwnerDynamicAuthority();
		authority.setParapheurService(parapheurService);
		authority.afterPropertiesSet();
	}
	
	public void testHasAuthority() {
		boolean returned = authority.hasAuthority(document, "proprietaire");
		Assert.assertTrue(returned);
	}
	
	public void testHasNotAuthority() {
		boolean returned = authority.hasAuthority(document, "nonProprietaire");
		Assert.assertFalse(returned);
	}

        public void testGetParapheurService() {
            ParapheurService monParapheurService = authority.getParapheurService();
            assertNotNull(monParapheurService);
        }
}
