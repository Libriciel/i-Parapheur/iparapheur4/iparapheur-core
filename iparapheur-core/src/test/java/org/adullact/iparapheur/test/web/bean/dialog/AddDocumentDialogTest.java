package org.adullact.iparapheur.test.web.bean.dialog;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.FileUploadBean;
import org.alfresco.web.bean.NavigationBean;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.web.bean.dialog.AddDocumentDialog;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class AddDocumentDialogTest extends WebBeanBaseTest {

    private NodeRef parapheurRef;
    private NodeRef dossierRef;
    private AddDocumentDialog dialog;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "parapheurRef");
        properties.put(ContentModel.PROP_TITLE, "parapheurRef");
        parapheurRef = parapheurService.createParapheur(properties);

        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "dossierRef");
        properties.put(ContentModel.PROP_TITLE, "dossierRef");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        dossierRef = dossierService.createDossier(parapheurRef, properties);

        BrowseBean browseBean = new BrowseBean();

        NavigationBean navigator = new NavigationBean();
        navigator.setCurrentNodeId(dossierRef.getId());

        FileUploadBean fileBean = new FileUploadBean();
        fileBean.setFile(File.createTempFile("parapheur", "unittest"));
        fileBean.setFileName("fileName");
        session.setAttribute(FileUploadBean.FILE_UPLOAD_BEAN_NAME, fileBean);

        dialog = new AddDocumentDialog();
        dialog.setBrowseBean(browseBean);
        dialog.setContentService(contentService);
        dialog.setDictionaryService((DictionaryService) applicationContext.getBean("dictionaryService"));
        dialog.setFileFolderService(fileFolderService);
        dialog.setNamespaceService(namespaceService);
        dialog.setNavigator(navigator);
        dialog.setNodeService(nodeService);
        dialog.setSearchService(searchService);
    }

    public void testFinish() {
        dialog.getFileName();
        dialog.setMimeType(MimetypeMap.MIMETYPE_TEXT_PLAIN);
        dialog.setObjectType(ParapheurModel.TYPE_DOCUMENT.toString());

        transactionManager.commit(transactionStatus);
        transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());

        dialog.finish();

        List<NodeRef> docs = parapheurService.getDocuments(dossierRef);
        Assert.assertEquals(1, docs.size());

        NodeRef doc = docs.get(0);
        ContentReader reader = contentService.getReader(doc, ContentModel.PROP_CONTENT);
        Assert.assertEquals(MimetypeMap.MIMETYPE_TEXT_PLAIN, reader.getMimetype());
    }
}
