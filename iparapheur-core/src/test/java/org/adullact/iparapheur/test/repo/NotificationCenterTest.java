/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2012, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 *
 * contact@adullact-projet.coop
 * contact@atolcd.com
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.test.repo;

import com.atolcd.parapheur.repo.NotificationCenter;
import com.atolcd.parapheur.repo.ParapheurUserPreferences;
import org.adullact.iparapheur.repo.notification.Notification;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationException;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * NotificationCenterTest
 *
 * @author Emmanuel Peralta e.peralta@adullact-projet.coop
 *         Date: 13/02/12
 *         Time: 14:25
 */
public class NotificationCenterTest extends BaseParapheurTest {

    private NotificationCenter notificationCenter;
    private ParapheurUserPreferences parapheurUserPreferences;

    private NodeRef daily_digest_user;
    private String daily_digest_username;
    private NodeRef non_daily_digest_user;
    private String non_daily_digest_username;


    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        /* gets the notification center instance from the applicationContext */
        notificationCenter = (NotificationCenter) applicationContext.getBean("NotificationCenter");

        /* builds the full username */
        daily_digest_username = tenantService.getDomainUser("dailydigestuser", tenantName);

        /* wrapping createAuthentication call in a try-catch construct to catch the AuthenticationException raised
         when an user with the same name already exists.
         */
        try {
            authenticationService.createAuthentication(daily_digest_username, "secret".toCharArray());
        } catch (AuthenticationException e) {

        }

        /* Initialize some person properties */
        daily_digest_user = personService.getPerson(daily_digest_username);
        nodeService.setProperty(daily_digest_user, ContentModel.PROP_FIRSTNAME, "Daily");
        nodeService.setProperty(daily_digest_user, ContentModel.PROP_LASTNAME, "Digest");
        nodeService.setProperty(daily_digest_user, ContentModel.PROP_EMAIL,
                "daily-digest@example.com");

        /* adds aspect and setup data structures */
        notificationCenter.enableNotificationsForUser(daily_digest_username);

        /* same as daily_digest_username */
        non_daily_digest_username = tenantService.getDomainUser("nondailydigestuser", tenantName);

        try {
            authenticationService.createAuthentication(non_daily_digest_username, "secret".toCharArray());
        } catch (AuthenticationException e) {

        }

        non_daily_digest_user = personService.getPerson(non_daily_digest_username);
        nodeService.setProperty(non_daily_digest_user, ContentModel.PROP_FIRSTNAME, "No Daily");
        nodeService.setProperty(non_daily_digest_user, ContentModel.PROP_LASTNAME, "Digest");
        nodeService.setProperty(non_daily_digest_user, ContentModel.PROP_EMAIL,
                "non-daily-digest@example.com");

    }

    @Override
    protected void onTearDownInTransaction() throws Exception {
        super.onTearDownInTransaction();
        /*  */
        if (nodeService.exists(daily_digest_user)) {
            personService.deletePerson(daily_digest_user);
        }
        if (nodeService.exists(non_daily_digest_user)) {
            personService.deletePerson(non_daily_digest_user);
        }
    }

    public void testPostNotification() {
        /*Notification n = notificationCenter.getNewNotification(daily_digest_username);

        notificationCenter.postNotification(daily_digest_username, n);

        List<Notification> notifications = notificationCenter.getNotificationsForUser(daily_digest_username);

        List<Notification> unread = notificationCenter.getUnreadNotificationsForUser(daily_digest_username);

        // on teste que l'on a bien qu'une seule notification.
        Assert.assertEquals(1, notifications.size());
        Assert.assertEquals(1, unread.size());
        Assert.assertEquals(n.getUUID(), unread.get(0).getUUID()); */
    }

    public void testPostNotificationForNormalUser() {
        Notification n = new Notification();

        notificationCenter.postNotification(non_daily_digest_username, n);

        assertEquals(0, notificationCenter.getNotificationsForUser(non_daily_digest_username).size());
    }

    public void testGetNotificationByUUID() {
        /* Notification n = notificationCenter.getNewNotification(daily_digest_username);

        Notification n2 = notificationCenter.getNewNotification(daily_digest_username);

        notificationCenter.postNotification(daily_digest_username, n);
        notificationCenter.postNotification(daily_digest_username, n2);

        Notification fetched = notificationCenter.getNotificationForUUID(daily_digest_username, n.getUUID());

        Assert.assertEquals(fetched.getUUID(), n.getUUID()); */
    }
    /*
    public void testSetNotification() {
        // creation de la notification

        Notification n = notificationCenter.getNewNotification(daily_digest_username);
        n.putStringInPayload(NotificationCenter.FIELD_NOTIFICATION_ANNOTATION_PUBLIQUE, "miaou");

        // enregistrement de la notification
        notificationCenter.postNotification(daily_digest_username, n);

        //n.setTimestamp(1);
        n.putSerializableInPayload(NotificationCenter.FIELD_NOTIFICATION_NODEREF, new NodeRef(storeRef, "Coucou"));
        n.putStringInPayload(NotificationCenter.FIELD_NOTIFICATION_ANNOTATION_PUBLIQUE, "test");

        // enregistrement de la notification
        notificationCenter.setNotificationForUUID(daily_digest_username, n.getUUID(), n);

        // recuperation de la notification
        Notification modified = notificationCenter.getNotificationForUUID(daily_digest_username, n.getUUID());

        // on teste si la notification est bien la version modifée.
        Assert.assertEquals("La modification de la notification à échouée", "test", modified.getStringFromPayload(NotificationCenter.FIELD_NOTIFICATION_ANNOTATION_PUBLIQUE));
    }
    */

    public void testMarkNotificationAsRead() {
       /* // creation de la notification
        // clear notifications to have a correct result

        Notification n = notificationCenter.getNewNotification(daily_digest_username);
        // enregistrement de la notification
        notificationCenter.postNotification(daily_digest_username, n);

        List<Notification> unreadNotifications = notificationCenter.getNotificationsForUser(daily_digest_username);

        Assert.assertEquals("Pas de notifications non lue", 1, unreadNotifications.size());

        Notification unreadNotification = unreadNotifications.get(0);

        notificationCenter.markNotificationAsRead(daily_digest_username, unreadNotification);

        unreadNotifications = notificationCenter.getUnreadNotificationsForUser(daily_digest_username);

        Assert.assertEquals("La notification n'a pas été lue.", 0, unreadNotifications.size());
        Assert.assertEquals("La notification n'a pas été marquée comme lue",
                Boolean.TRUE,
                notificationCenter.getNotificationForUUID(daily_digest_username, unreadNotification.getUUID()).isRead());

    */
    }

    public void testBroadcastNotification() {
        /* List<String> users = new ArrayList<String>();

        users.add(daily_digest_username);
        users.add(non_daily_digest_username);

        notificationCenter.enableNotificationsForUser(non_daily_digest_username);
        notificationCenter.clearNotifications(daily_digest_username);
        notificationCenter.clearNotifications(non_daily_digest_username);

        Notification notification = notificationCenter.getNewNotification(daily_digest_username);
        notification.putStringInPayload("key", "value");

        notificationCenter.broadcastNotification(users, notification);

        List<Notification> unreadNotifications1 = notificationCenter.getUnreadNotificationsForUser(daily_digest_username);
        List<Notification> unreadNotifications2 = notificationCenter.getUnreadNotificationsForUser(non_daily_digest_username);

        Assert.assertEquals("daily_digest_username n'a pas recu la notification.", 1, unreadNotifications1.size());
        Assert.assertEquals("daily_digest_username n'a pas recu la notification.", 1, unreadNotifications2.size());
        Assert.assertEquals("Les notifications sont différentes",
                unreadNotifications1.get(0).getStringFromPayload("key"),
                unreadNotifications2.get(0).getStringFromPayload("key")); */
    }
}
