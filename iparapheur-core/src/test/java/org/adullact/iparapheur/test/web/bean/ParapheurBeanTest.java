package org.adullact.iparapheur.test.web.bean;

import java.io.Serializable;
import java.util.*;

import javax.faces.event.ActionEvent;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebBeanBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.app.Application;
import org.alfresco.web.bean.BrowseBean;
import org.alfresco.web.bean.NavigationBean;
import org.alfresco.web.bean.repository.Node;
import org.alfresco.web.bean.repository.User;
import org.alfresco.web.ui.common.component.UIActionLink;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.web.bean.ParapheurBean;
import org.adullact.iparapheur.repo.jscript.ScriptEtapeCircuitImpl;

public class ParapheurBeanTest extends WebBeanBaseTest {

    private NodeRef parapheurRef;
    private NodeRef dossierRef;
    private NodeRef documentRef;
    private ParapheurBean bean;
    private ParapheurBean mockedBean;
    private BrowseBean browseBean;
    private NavigationBean navigator;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();



        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "parapheurRef");
        properties.put(ContentModel.PROP_TITLE, "parapheurRef");
        ArrayList<String> owners = new ArrayList<String>();
        owners.add(tenantService.getDomainUser("proprietaire", tenantName));
        properties.put(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR,
                owners);
        parapheurRef = parapheurService.createParapheur(properties);

        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "dossierRef");
        properties.put(ContentModel.PROP_TITLE, "dossierRef");
        properties.put(ParapheurModel.PROP_TERMINE, false);
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        properties.put(ParapheurModel.PROP_DATE_LIMITE, new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 7));
        dossierRef = dossierService.createDossier(parapheurRef, properties);

        documentRef = nodeService.createNode(dossierRef,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}documentRef"),
                ParapheurModel.TYPE_DOCUMENT).getChildRef();

        browseBean = Mockito.mock(BrowseBean.class);



        navigator = Mockito.mock(NavigationBean.class);
        Mockito.when(navigator.getCurrentNode()).thenReturn(
                new Node(parapheurRef));
        Mockito.when(navigator.getCurrentUser()).thenAnswer(new Answer<User>() {

            @Override
            public User answer(InvocationOnMock invocation) throws Throwable {
                return Application.getCurrentUser(facesContext);
            }
        });

        bean = new ParapheurBean();
        bean.setBrowseBean(browseBean);
        bean.setContentService(contentService);
        bean.setNamespaceService(namespaceService);
        bean.setNavigator(navigator);
        bean.setNodeService(nodeService);
        bean.setParapheurService(parapheurService);
       // bean.getParapheurCourant();
        bean.setSearchService(searchService);

        CorbeillesService corbeillesService = (CorbeillesService) applicationContext.getBean("corbeillesService");
        bean.setCorbeillesService(corbeillesService);


        mockedBean = Mockito.spy(bean);
        // @see http://mockito.googlecode.com/svn/branches/1.8.5/javadoc/org/mockito/Mockito.html#13
        Mockito.doReturn(parapheurRef).when(mockedBean).getParapheurCourant();
        /*
        Mockito.when(mockedBean.getParapheurCourant()).thenReturn(
                parapheurRef);
         */

        authenticate("proprietaire");
    }

    public void testGetCorbeilleChildNumber() {

        
        authenticate("proprietaire");
        int returned = bean.getCorbeilleChildNumber(parapheurService.getCorbeille(
                parapheurRef, ParapheurModel.NAME_EN_PREPARATION));

        Assert.assertEquals(1, returned);
    }

    public void testGetCorbeilleChildNumber2() {
        authenticate("proprietaire");
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "dossierRef2");
        properties.put(ContentModel.PROP_TITLE, "dossierRef2");
        properties.put(ParapheurModel.PROP_TERMINE, false);
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        properties.put(ParapheurModel.PROP_DATE_LIMITE, new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 7));
        dossierService.createDossier(parapheurRef, properties);

        int returned = bean.getCorbeilleChildNumber(parapheurService.getCorbeille(
                parapheurRef, ParapheurModel.NAME_EN_PREPARATION));

        Assert.assertEquals(2, returned);
    }
    /* Not applicable now
    public void testGetParapheurCourant() {
        authenticate("proprietaire");
        NodeRef returned = bean.getParapheurCourant();
        Assert.assertEquals(parapheurRef, returned);
    }

    public void testGetParapheurCourant2() {
        authenticate("proprietaire");
        Mockito.when(navigator.getCurrentNode()).thenReturn(new Node(dossierRef));

        NodeRef returned = bean.getParapheurCourant();
        Assert.assertEquals(parapheurRef, returned);
    }
    */
    public void testGetCurrentUserOwner() {
        //authenticate("proprietaire");
        authenticate("proprietaire");

        boolean returned = mockedBean.getCurrentUserOwner();

        Assert.assertTrue(returned);
    }

    public void testGetCurrentUserOwner2() {
        authenticate("proprietaire");
        authenticate("user");

        boolean returned = bean.getCurrentUserOwner();

        Assert.assertFalse(returned);
    }

    public void testDeleteDocumentOK() {
        authenticate("proprietaire");
        Mockito.when(browseBean.getDocument()).thenReturn(new Node(documentRef));

        bean.deleteDocumentOK();

        Assert.assertFalse(nodeService.exists(documentRef));
    }

    public void testRaz() {
        authenticate("proprietaire");
        NodeRef dossier = nodeService.createNode(
                parapheurService.getCorbeille(parapheurRef,
                ParapheurModel.NAME_RETOURNES),
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}dossier"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheurRef);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        nodeService.createNode(dossier,
                ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE, QName.createQName("{test}premiereEtape"),
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties);

        nodeService.createNode(dossier, ContentModel.ASSOC_CONTAINS, QName.createQName("{test}dossier"), ParapheurModel.TYPE_DOCUMENT);

        UIActionLink link = new UIActionLink();
        link.getParameterMap().put("id", dossier.getId());
        bean.raz(new ActionEvent(link));

        NodeRef parent = nodeService.getPrimaryParent(dossier).getParentRef();
        Assert.assertEquals(parapheurService.getCorbeille(parapheurRef,
                ParapheurModel.NAME_EN_PREPARATION), parent);
    }

    public void testRecuperer() {
        authenticate("proprietaire");
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "dossier");

        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        NodeRef dossier = dossierService.createDossier(parapheurRef,
                properties);
        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "parapheurUser");
        ArrayList<String> owners = new ArrayList<String>();
        owners.add(tenantService.getDomainUser("user", tenantName));
        properties.put(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, owners);
        NodeRef parapheurUser = parapheurService.createParapheur(properties);

        parapheurService.setCircuit(dossier, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheurRef, "VISA", new HashSet<NodeRef>()),
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheurUser, "SIGNATURE", new HashSet<NodeRef>())));

        nodeService.createNode(dossier, ContentModel.ASSOC_CONTAINS, QName.createQName("{test}dossier"), ParapheurModel.TYPE_DOCUMENT);


        Assert.assertEquals(parapheurService.getDocuments(dossier).size(), 1);

        authenticate("proprietaire");
        parapheurService.approve(dossier);

        UIActionLink link = new UIActionLink();
        link.getParameterMap().put("id", dossier.getId());
        mockedBean.recuperer(new ActionEvent(link));

        NodeRef parent = nodeService.getPrimaryParent(dossier).getParentRef();
        Assert.assertEquals(parapheurService.getCorbeille(parapheurRef,
                ParapheurModel.NAME_EN_PREPARATION), parent);
    }
}
