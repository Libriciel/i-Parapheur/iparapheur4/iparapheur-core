package org.adullact.iparapheur.test.repo;

import com.atolcd.parapheur.repo.JobService;
import com.atolcd.parapheur.repo.impl.JobServiceImpl;
import org.adullact.iparapheur.test.BaseParapheurTest;


public class JobServiceTest extends BaseParapheurTest {

    private JobService jobService;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();
        jobService = (JobService) applicationContext.getBean("JobService");
        ((JobServiceImpl)jobService).setBackgroundWorkEnabledString("false");

    }

    public void testPostJob() {

    }

    public void testUnlockNode() {
    }

}
