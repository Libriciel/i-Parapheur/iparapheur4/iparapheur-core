/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.iparapheur.test.web.bean;

import com.atolcd.parapheur.repo.ParapheurVersionProperties;
import com.atolcd.parapheur.web.bean.ParapheurVersionPropertiesBean;
import org.junit.Assert;
import org.adullact.iparapheur.test.WebBeanBaseTest;

/**
 *
 * @author stoulouse
 */
public class ParapheurVersionPropertiesBeanTest extends WebBeanBaseTest {

    ParapheurVersionProperties mesProperties;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();
        mesProperties = new ParapheurVersionProperties();
    }

    public void testGetProperties() {

        ParapheurVersionPropertiesBean monparaphVersion = new ParapheurVersionPropertiesBean();
        monparaphVersion.setProperties(mesProperties);
        ParapheurVersionProperties returned = monparaphVersion.getProperties();
        Assert.assertNotNull(returned);
    }
}
