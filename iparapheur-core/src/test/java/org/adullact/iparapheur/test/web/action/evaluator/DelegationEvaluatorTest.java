package org.adullact.iparapheur.test.web.action.evaluator;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import com.atolcd.parapheur.web.action.evaluator.DelegationEvaluator;
import com.atolcd.parapheur.web.bean.ParapheurBean;

public class DelegationEvaluatorTest extends WebUIBaseTest {

    protected DelegationEvaluator evaluator;
    protected ParapheurBean parapheurBean;
    protected NodeRef nodeRef;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");

        parapheurBean = Mockito.mock(ParapheurBean.class);
        Mockito.when(parapheurBean.getCurrentUserOwner()).thenReturn(true);

        setUpManagedBean("ParapheurBean", parapheurBean);

        evaluator = new DelegationEvaluator();
    }

    public void testEvaluateTrue() {
        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateFalse() {
        Mockito.when(parapheurBean.getCurrentUserOwner()).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testEvaluateTrueObject() {

        Object monNode = new Node(nodeRef);
        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }
}
