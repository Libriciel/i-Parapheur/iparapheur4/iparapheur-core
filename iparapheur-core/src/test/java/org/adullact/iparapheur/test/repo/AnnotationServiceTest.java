package org.adullact.iparapheur.test.repo;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.AnnotationService;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.annotations.Annotation;
import com.atolcd.parapheur.repo.annotations.Point;
import com.atolcd.parapheur.repo.annotations.Rect;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import org.junit.Assert;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ApplicationModel;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.Serializable;
import java.util.*;

public class AnnotationServiceTest extends BaseParapheurTest {

    @Autowired
    AnnotationService annotationService;
 /*
    @Qualifier("nodeService")
    @Autowired
    NodeService nodeService;

    @Autowired
    ParapheurService parapheurService;
    */
    NodeRef dossier;

    private NodeRef companyHome;
    private NodeRef archives_configuration;
    /**
     * Parapheurs home ref
     */
    private NodeRef parapheursHome;
    /**
     * Archives home ref
     */
    private NodeRef archivesHome;
    /**
     * Dictionary home ref
     */
    private NodeRef dictionaryHome;
    /**
     * Workflows home ref
     */
    private NodeRef workflowsHome;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.company_home.childname"),
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                        namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        archivesHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.archives.childname"),
                        namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();


        permissionService.setInheritParentPermissions(archivesHome, false);
        permissionService.setPermission(archivesHome, "GROUP_EVERYONE", "Contributor", true);

        dictionaryHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.dictionary.childname"),
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        archives_configuration = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("ph:archives_configuration",
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();
        ContentWriter contentWriter = contentService.getWriter(archives_configuration, ContentModel.PROP_CONTENT, true);
        contentWriter.putContent("archive.tamponActes.visible=true\n"
                + "archive.tamponActes.text=Acquitt\u00E9 en PREFECTURE le {0,date,dd/MM/yyyy}");

        workflowsHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.savedworkflows.childname"),
                        namespaceService), ContentModel.TYPE_FOLDER).getChildRef();


        authenticationService.createAuthentication(tenantService.getDomainUser("proprietaire1", tenantName), "secret".toCharArray());

        NodeRef prop1 = personService.getPerson(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(prop1, ContentModel.PROP_FIRSTNAME, "Prop");
        nodeService.setProperty(prop1, ContentModel.PROP_LASTNAME, "Ietaire");
        nodeService.setProperty(prop1, ContentModel.PROP_EMAIL, "proprietaire1@example.com");



        ArrayList proprietaires = new ArrayList();
        proprietaires.add(tenantService.getDomainUser("proprietaire1", tenantName));

        Map<QName, Serializable> parapheurProperties = new HashMap<QName, Serializable>();
        parapheurProperties.put(ContentModel.PROP_NAME, "Parapheur test unit");
        parapheurProperties.put(ApplicationModel.PROP_ICON, "space-icon-default");
        parapheurProperties.put(ContentModel.PROP_TITLE, "Parapheur test unit");
        parapheurProperties.put(ContentModel.PROP_DESCRIPTION, "Parapheur test unit");
        parapheurProperties.put(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, (ArrayList) proprietaires);

        NodeRef parapheur = parapheurService.createParapheur(parapheurProperties);


        Map<QName, Serializable> dossierProperties = new HashMap<QName, Serializable>();
        dossierProperties.put(ContentModel.PROP_NAME, "dossier Test");
        dossierProperties.put(ParapheurModel.PROP_CONFIDENTIEL, Boolean.FALSE);
        dossierProperties.put(ParapheurModel.PROP_PUBLIC, Boolean.TRUE);

        dossier = dossierService.createDossier(parapheur, dossierProperties);
        EtapeCircuitImpl etape1 = new EtapeCircuitImpl();

        etape1.setActionDemandee("VISA");
        etape1.setParapheur(parapheur);
        etape1.setTransition("PARAPHEUR");
        etape1.setListeDiffusion(new HashSet<NodeRef>());

        List<EtapeCircuit> etapes = new ArrayList<EtapeCircuit>();

        etapes.add(etape1);
        etapes.add(etape1);

        nodeService.createNode(dossier,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}content1"), ContentModel.TYPE_CONTENT).getChildRef();
        //nodeService.addAspect(content1, ParapheurModel.ASPECT_LU,

        parapheurService.setCircuit(dossier, etapes);
        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Integer>() {
            @Override
            public Integer doWork() throws Exception {
                parapheurService.approve(dossier);
                return 0;
            }
        }, tenantService.getDomainUser("proprietaire1", tenantName));

    }

    @Override
    protected void onTearDownInTransaction() throws Exception {
        super.onTearDownInTransaction();
    }

    public void testAddAnnotation() throws Exception {


        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Void>() {
            @Override
            public Void doWork() throws Exception {
                Annotation a = annotationService.getNewTextAnnotationWith(
                        new Rect(0.0, 0.0, 10.0, 0.0),
                        "myPicto",
                        "text",
                        0, "");
                annotationService.addAnnotation(dossier, a);
                return null;
            }
        }, tenantService.getDomainUser("proprietaire1", tenantName));

        HashMap<Integer, ArrayList<Annotation>> annotations = annotationService.getCurrentAnnotations(dossier);

        Assert.assertEquals("L'annotation n'a pas été enregistrée", annotations.keySet().size(), 1);
    }

    public void testGetAllAnnotations() throws Exception {

    }

    public void testGetNewTextAnnotationWith() throws Exception {
        Annotation a = annotationService.getNewTextAnnotationWith(
                new Rect(0.0, 0.0, 10.0, 0.0),
                "myPicto",
                "text",
                0, "");
        Assert.assertNotNull("L'annotation doit porter un UUID", a.getUUID());

    }

    public void testGetNewRectAnnotationWith() throws Exception {
        Annotation a = annotationService.getNewRectAnnotationWith(
                new Rect(0.0, 0.0, 10.0, 0.0),
                "pCol",
                "fCol", "test",
                0, "");

        Assert.assertNotNull("L'annotation doit porter un UUID", a.getUUID());
    }

    public void testGetNewHighlightAnnotationWith() throws Exception {
        Annotation a = annotationService.getNewHighlightAnnotationWith(
                new Rect(0.0, 0.0, 10.0, 0.0),
                "pCol",
                "fCol", "test",
                0, "");
        Assert.assertNotNull("L'annotation doit porter un UUID", a.getUUID());

    }

    public void testPointEquals() throws Exception {
        Point a = new Point(10.0, 11.0);
        Point b = new Point(10.0, 11.0);

        Assert.assertEquals("a et b doivent être égaux", a, b);

    }

    public void testPointNotEqual() throws Exception {
        Point a = new Point(10.0, 10.0);
        Point b = new Point(10.0, 11.0);

        Assert.assertFalse("a et b doivent être différents", a.equals(b));
    }
}
