/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.test.repo;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.S2lowService;
import com.atolcd.parapheur.repo.SavedWorkflow;
import com.atolcd.parapheur.repo.impl.EtapeCircuitImpl;
import com.atolcd.parapheur.repo.impl.ParapheurServiceImpl;
import org.adullact.iparapheur.repo.jscript.ScriptEtapeCircuitImpl;
import org.adullact.iparapheur.status.StatusMetier;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.security.NoSuchPersonException;
import org.alfresco.service.namespace.QName;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Assert;
import org.mockito.Mockito;

import javax.security.auth.x500.X500Principal;
import java.io.Serializable;
import java.io.StringReader;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Test class for the ParapheurServiceImpl class.
 * <p/>
 * This test creates a whole content hierarchy, as follow:
 * - Company home
 *     - Parapheurs home
 *         - Circuit parapheur 1
 *             - Corbeille en préparation 1
 *                 - Dossier 1
 *                     - Content 1
 *             - Corbeille retournes 1
 *             - Corbeille a traiter 1
 *             - Corbeille a archiver 1
 *                 - Dossier a archiver
 *                     - Content a archiver
 *             - Corbeille secretariat 1
 *             - Corbeille virtuelle
 *         - Circuit parapheur 2
 *             - Corbeille en preparation 2
 *             - Corbeille a traiter 2
 *                 - Dossier 2
 *                     - Content 2
 *             - Corbeille secretariat 2
 *                 - Dossier secretariat 2
 *     - Archives home
 *     - Dictionary home
 *         - Workflows home
 *             - Public workflow
 *             - Workflow proprietaire 1
 *             - Worflow proprietaire 2
 *
 * S2lowService is mocked, so calls to the S2low platform aren't actually
 * performed.
 * <p/>
 * ActionService is also mocked.
 * <p/>
 * Mocks are created using the Mockito mocking framework. For more information
 * on how to use Mockito, please refer to the official
 * <a href="http://mockito.org/">Mockito</a> documentation.
 *
 * @author Vivien Barousse
 * @see Mockito
 */
public class ParapheurServiceTest extends BaseParapheurTest {

    /**
     * Mocked ActionService
     *
     * @see ActionService
     */
    private ActionService mockedActionService;
    /**
     * Mocked S2lowService
     *
     * @see S2lowService
     */
    private S2lowService mockedS2lowService;
    /**
     * Company home ref
     */
    private NodeRef companyHome;
    private NodeRef archives_configuration;
    /**
     * Parapheurs home ref
     */
    private NodeRef parapheursHome;
    /**
     * Archives home ref
     */
    private NodeRef archivesHome;
    /**
     * Dictionary home ref
     */
    private NodeRef dictionaryHome;
    /**
     * Workflows home ref
     */
    private NodeRef workflowsHome;
    /**
     * Parapheur 1 ref
     */
    private NodeRef parapheur1;
    /**
     * Parapheur 2 ref
     */
    private NodeRef parapheur2;
    /**
     * Corbeille en préparation 1 ref
     */
    private NodeRef corbeilleEnPreparation1;
    /**
     * Corbeille retournes 1 ref
     */
    private NodeRef corbeilleRetournes1;
    /**
     * Corbeille a traiter 1 ref
     */
    private NodeRef corbeilleATraiter1;
    /**
     * Corbeille a archiver 1 ref
     */
    private NodeRef corbeilleAArchiver1;
    /**
     * Corbeille secretariat 1 ref
     */
    private NodeRef corbeilleSecretariat1;
    /**
     * Corbeille en preparation 2 ref
     */
    private NodeRef corbeilleEnPreparation2;
    /**
     * Corbeille a traiter 2 ref
     */
    private NodeRef corbeilleATraiter2;
    /**
     * Corbeille Secretariat 2 ref
     */
    private NodeRef corbeilleSecretariat2;
    /**
     * Corbeille virtuelle ref
     */
    private NodeRef corbeilleVirtuelle;
    private NodeRef corbeilleEnCours1;
    private NodeRef corbeilleAVenir1;
    private NodeRef corbeilleDossiersDelegues1;
    private NodeRef corbeilleAImprimer1;
    private NodeRef corbeilleEnRetard1;
    private NodeRef corbeilleRecuperable1;
    private NodeRef corbeilleEnCours2;
    private NodeRef corbeilleAVenir2;
    private NodeRef corbeilleDossiersDelegues2;
    private NodeRef corbeilleAImprimer2;
    private NodeRef corbeilleEnRetard2;
    private NodeRef corbeilleRecuperable2;
    /**
     * /**
     * Dossier 1 ref
     */
    private NodeRef dossier1;
    /**
     * Dossier 2 ref
     */
    private NodeRef dossier2;
    /**
     * Dossier secretariat 2 ref
     */
    private NodeRef dossierSecretariat2;
    /**
     * Dossier a archiver ref
     */
    private NodeRef dossierAArchiver;
    /**
     * Content 1 ref
     */
    private NodeRef content1;
    private NodeRef contentSecretariat;
    /**
     * Content 2 ref
     */
    private NodeRef content2;
    /**
     * Content a archiver ref
     */
    private NodeRef contentAArchiver;
    private NodeRef circuitEtape1;
    private NodeRef circuitEtape2;
    private NodeRef circuitEtape3;
    private NodeRef circuitEtape4;
    private NodeRef etape1DossierSecretariat;
    private NodeRef etape2DossierSecretariat;
    private NodeRef etape1DossierAArchiver;
    private NodeRef etape2DossierAArchiver;
    /**
     * Public workflow ref
     */
    private NodeRef publicWorkflow;
    /**
     * Workflow proprietaire 1 ref
     */
    private NodeRef workflowProprietaire1;
    /**
     * Workflow proprietaire 2 ref
     */
    private NodeRef workflowProprietaire2;

    /**
     * Initializes the hierarchy described in class header. Mock S2low service.
     *
     * @throws Exception
     */
    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        mockedActionService = Mockito.mock(ActionService.class);
        Mockito.when(mockedActionService.createAction(Mockito.anyString())).thenReturn(actionService.createAction("parapheur-mail"));
        ((ParapheurServiceImpl) parapheurService).setActionService(mockedActionService);

        mockedS2lowService = Mockito.mock(S2lowService.class);
        Mockito.when(mockedS2lowService.isEnabled()).thenReturn(true);
        Mockito.when(
                mockedS2lowService.setS2lowActesArchiveURL(Mockito.any(NodeRef.class))).thenReturn(
                "http://example.com/url-archivage/");
        Mockito.when(
                mockedS2lowService.getInfosS2low(Mockito.any(NodeRef.class))).thenReturn(4);
        ((ParapheurServiceImpl) parapheurService).setS2lowService(mockedS2lowService);

        authenticationService.createAuthentication(tenantService.getDomainUser("proprietaire1", tenantName), "secret".toCharArray());
        authenticationService.createAuthentication(tenantService.getDomainUser("proprietaire2", tenantName), "secret".toCharArray());
        authenticationService.createAuthentication(tenantService.getDomainUser("secretaire2", tenantName), "secret".toCharArray());

        NodeRef prop1 = personService.getPerson(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(prop1, ContentModel.PROP_FIRSTNAME, "Prop");
        nodeService.setProperty(prop1, ContentModel.PROP_LASTNAME, "Ietaire");
        nodeService.setProperty(prop1, ContentModel.PROP_EMAIL, "proprietaire1@example.com");

        companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.company_home.childname"),
                                  namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                                  namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        archivesHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.archives.childname"),
                                  namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();


        permissionService.setInheritParentPermissions(archivesHome, false);
        permissionService.setPermission(archivesHome, "GROUP_EVERYONE", "Contributor", true);

        dictionaryHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.dictionary.childname"),
                                  namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        archives_configuration = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("ph:archives_configuration",
                                  namespaceService), ContentModel.TYPE_FOLDER).getChildRef();
        ContentWriter contentWriter = contentService.getWriter(archives_configuration, ContentModel.PROP_CONTENT, true);
        contentWriter.putContent("archive.tamponActes.visible=true\n"
                                 + "archive.tamponActes.text=Acquitt\u00E9 en PREFECTURE le {0,date,dd/MM/yyyy}");

        workflowsHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.savedworkflows.childname"),
                                  namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        parapheur1 = nodeService.createNode(parapheursHome,
                                            ContentModel.ASSOC_CONTAINS,
                                            QName.createQName("{testgetcircuit}parapheur1"),
                                            ParapheurModel.TYPE_PARAPHEUR).getChildRef();

        ArrayList<String> ownersPh1 = new ArrayList<String>();
        ownersPh1.add(tenantService.getDomainUser("proprietaire1", tenantName));

        String ownerPh1 = tenantService.getDomainUser("proprietaire1", tenantName);

        nodeService.setProperty(parapheur1, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPh1);
        nodeService.setProperty(parapheur1, ParapheurModel.PROP_PROPRIETAIRE_PARAPHEUR, ownerPh1);
        nodeService.setProperty(parapheur1, ContentModel.PROP_TITLE, "parapheur1");
        nodeService.setProperty(parapheur1, ContentModel.PROP_NAME, "parapheur1");
        parapheur2 = nodeService.createNode(parapheursHome,
                                            ContentModel.ASSOC_CONTAINS,
                                            QName.createQName("{testgetcircuit}parapheur2"),
                                            ParapheurModel.TYPE_PARAPHEUR).getChildRef();

        ArrayList<String> ownersPh2 = new ArrayList<String>();
        ownersPh2.add(tenantService.getDomainUser("proprietaire2", tenantName));
        nodeService.setProperty(parapheur2, ContentModel.PROP_TITLE, "parapheur 2");
        nodeService.setProperty(parapheur2, ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPh2);
        nodeService.setProperty(parapheur2,
                                ParapheurModel.PROP_SECRETAIRES,
                                (Serializable) Collections.singletonList(tenantService.getDomainUser("secretaire2", tenantName)));
        nodeService.createAssociation(parapheur2, parapheur1, ParapheurModel.ASSOC_HIERARCHIE);
        nodeService.createAssociation(parapheur1, parapheur2, ParapheurModel.ASSOC_DELEGATION);
        nodeService.createAssociation(parapheur1, parapheur2, ParapheurModel.ASSOC_OLD_DELEGATION);

        corbeilleEnPreparation1 = nodeService.createNode(
                parapheur1, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_EN_PREPARATION,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleRetournes1 = nodeService.createNode(parapheur1,
                                                     ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_RETOURNES,
                                                     ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleATraiter1 = nodeService.createNode(parapheur1,
                                                    ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_TRAITER,
                                                    ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleAArchiver1 = nodeService.createNode(parapheur1,
                                                     ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_ARCHIVER,
                                                     ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleSecretariat1 = nodeService.createNode(
                parapheur1, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_SECRETARIAT, ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleEnPreparation2 = nodeService.createNode(
                parapheur2, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_EN_PREPARATION,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleATraiter2 = nodeService.createNode(parapheur2,
                                                    ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_TRAITER,
                                                    ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleSecretariat2 = nodeService.createNode(
                parapheur2, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_SECRETARIAT, ParapheurModel.TYPE_CORBEILLE).getChildRef();

        corbeilleVirtuelle = nodeService.createNode(parapheur1,
                                                    ContentModel.ASSOC_CONTAINS,
                                                    QName.createQName("{test}corbeillevirtuellenode"),
                                                    ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleEnCours1 = nodeService.createNode(parapheur1,
                                                   ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_EN_COURS,
                                                   ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleAVenir1 = nodeService.createNode(parapheur1,
                                                  ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_VENIR,
                                                  ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleDossiersDelegues1 = nodeService.createNode(parapheur1,
                                                            ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_DOSSIERS_DELEGUES,
                                                            ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleAImprimer1 = nodeService.createNode(parapheur1,
                                                     ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_IMPRIMER,
                                                     ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleEnRetard1 = nodeService.createNode(parapheur1,
                                                    ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_EN_RETARD,
                                                    ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleRecuperable1 = nodeService.createNode(parapheur1,
                                                       ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_RECUPERABLES,
                                                       ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleEnCours2 = nodeService.createNode(parapheur2,
                                                   ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_EN_COURS,
                                                   ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleAVenir2 = nodeService.createNode(parapheur2,
                                                  ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_VENIR,
                                                  ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleDossiersDelegues2 = nodeService.createNode(parapheur2,
                                                            ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_DOSSIERS_DELEGUES,
                                                            ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleAImprimer2 = nodeService.createNode(parapheur2,
                                                     ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_A_IMPRIMER,
                                                     ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleEnRetard2 = nodeService.createNode(parapheur2,
                                                    ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_EN_RETARD,
                                                    ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        corbeilleRecuperable2 = nodeService.createNode(parapheur2,
                                                       ContentModel.ASSOC_CONTAINS, ParapheurModel.NAME_RECUPERABLES,
                                                       ParapheurModel.TYPE_CORBEILLE_VIRTUELLE).getChildRef();

        dossier1 = nodeService.createNode(
                corbeilleEnPreparation1, ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}dossier1"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossier1, ParapheurModel.PROP_TERMINE, false);
        nodeService.setProperty(dossier1, ContentModel.PROP_NAME, "dossier1");
        nodeService.setProperty(dossier1, ParapheurModel.PROP_LISTE_DIFFUSION, (Serializable) Arrays.asList(parapheur1, parapheur2));
        nodeService.setProperty(dossier1, ParapheurModel.PROP_SIGNATURE_FORMAT, "PKCS#7/single");
        nodeService.setProperty(dossier1, ParapheurModel.PROP_SIGNATURE_PAPIER, false);
        nodeService.setProperty(dossier1, ParapheurModel.PROP_RECUPERABLE, false);
        dossier2 = nodeService.createNode(corbeilleATraiter2,
                                          ContentModel.ASSOC_CONTAINS,
                                          QName.createQName("{testgetcircuit}dossier2"),
                                          ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossier2, ParapheurModel.PROP_TERMINE, false);
        nodeService.setProperty(dossier2, ParapheurModel.PROP_SIGNATURE_PAPIER, false);
        nodeService.setProperty(dossier2, ParapheurModel.PROP_RECUPERABLE, true);

        dossierSecretariat2 = nodeService.createNode(
                corbeilleSecretariat2, ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}dossierSecretariat2"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.addAspect(dossierSecretariat2, ParapheurModel.ASPECT_SECRETARIAT, Collections.<QName, Serializable> emptyMap());
        nodeService.setProperty(dossierSecretariat2, ParapheurModel.PROP_TERMINE, false);

        contentSecretariat = nodeService.createNode(dossierSecretariat2,
                                                    ContentModel.ASSOC_CONTAINS,
                                                    QName.createQName("{test}contentSecretariat"), ContentModel.TYPE_CONTENT).getChildRef();
        nodeService.addAspect(contentSecretariat, ParapheurModel.ASPECT_LU, Collections.<QName, Serializable> emptyMap());

        dossierAArchiver = nodeService.createNode(corbeilleAArchiver1,
                                                  ContentModel.ASSOC_CONTAINS,
                                                  QName.createQName("{test}dossierAArchiver"),
                                                  ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_TERMINE, true);
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_TYPE_METIER, "metier");
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_SOUSTYPE_METIER, "defaut1");

        content1 = nodeService.createNode(dossier1,
                                          ContentModel.ASSOC_CONTAINS,
                                          QName.createQName("{test}content1"), ContentModel.TYPE_CONTENT).getChildRef();
        nodeService.addAspect(content1, ParapheurModel.ASPECT_LU, Collections.<QName, Serializable> emptyMap());
        content2 = nodeService.createNode(dossier2,
                                          ContentModel.ASSOC_CONTAINS,
                                          QName.createQName("{test}content2"), ContentModel.TYPE_CONTENT).getChildRef();
        nodeService.addAspect(content2, ParapheurModel.ASPECT_LU, Collections.<QName, Serializable> emptyMap());

        contentAArchiver = nodeService.createNode(dossierAArchiver,
                                                  ContentModel.ASSOC_CONTAINS,
                                                  QName.createQName("{test}contentAArchiver"),
                                                  ContentModel.TYPE_CONTENT).getChildRef();
        ContentWriter writer = contentService.getWriter(contentAArchiver, ContentModel.PROP_CONTENT, true);
        writer.setMimetype(MimetypeMap.MIMETYPE_PDF);
        writer.putContent(this.getClass().getResourceAsStream("/samples/document.pdf"));
        //Dossier 1
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);
        circuitEtape1 = nodeService.createNode(dossier1,
                                               ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                                               ParapheurModel.ASSOC_DOSSIER_ETAPE,
                                               ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();
        nodeService.setProperty(circuitEtape1, ParapheurModel.PROP_ANNOTATION_PRIVEE, "annotationprivee1");
        nodeService.setProperty(circuitEtape1, ParapheurModel.PROP_LISTE_DIFFUSION, (Serializable) Arrays.asList(parapheur1, parapheur2));

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_SIGNATURE);
        circuitEtape2 = nodeService.createNode(circuitEtape1,
                                               ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                                               ParapheurModel.ASSOC_DOSSIER_ETAPE,
                                               ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();
        nodeService.setProperty(circuitEtape2, ParapheurModel.PROP_ANNOTATION, "annotationpublique");
        nodeService.setProperty(circuitEtape2, ParapheurModel.PROP_LISTE_DIFFUSION, (Serializable) Arrays.asList(parapheur1, parapheur2));

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_ARCHIVAGE);
        NodeRef circuitEtape2_a = nodeService.createNode(circuitEtape2,
                                                         ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                                                         ParapheurModel.ASSOC_DOSSIER_ETAPE,
                                                         ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();
        nodeService.setProperty(circuitEtape2_a, ParapheurModel.PROP_ANNOTATION, "annotationpublique");
        nodeService.setProperty(circuitEtape2_a,
                                ParapheurModel.PROP_LISTE_DIFFUSION,
                                (Serializable) Arrays.asList(parapheur1, parapheur2));

        //Dossier 2
        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, true);
        properties.put(ParapheurModel.PROP_ANNOTATION_PRIVEE, "annotationpriveeprecedente");
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);
        circuitEtape3 = nodeService.createNode(dossier2,
                                               ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                                               ParapheurModel.ASSOC_DOSSIER_ETAPE,
                                               ParapheurModel.TYPE_ETAPE_CIRCUIT,
                                               properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ANNOTATION, "annotationpublique");
        properties.put(ParapheurModel.PROP_ANNOTATION_PRIVEE, "annotationprivee");
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_SIGNATURE);
        circuitEtape4 = nodeService.createNode(circuitEtape3,
                                               ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                                               ParapheurModel.ASSOC_DOSSIER_ETAPE,
                                               ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, true);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);
        etape1DossierSecretariat = nodeService.createNode(
                dossierSecretariat2,
                ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_SIGNATURE);
        etape2DossierSecretariat = nodeService.createNode(
                etape1DossierSecretariat,
                ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                ParapheurModel.ASSOC_DOSSIER_ETAPE,
                ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur1);
        properties.put(ParapheurModel.PROP_EFFECTUEE, true);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_VISA);
        etape1DossierAArchiver = nodeService.createNode(dossierAArchiver,
                                                        ParapheurModel.CHILD_ASSOC_PREMIERE_ETAPE,
                                                        ParapheurModel.ASSOC_DOSSIER_ETAPE,
                                                        ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ACTION_DEMANDEE, EtapeCircuit.ETAPE_ARCHIVAGE);
        etape2DossierAArchiver = nodeService.createNode(etape1DossierAArchiver,
                                                        ParapheurModel.CHILD_ASSOC_PROCHAINE_ETAPE,
                                                        ParapheurModel.ASSOC_DOSSIER_ETAPE,
                                                        ParapheurModel.TYPE_ETAPE_CIRCUIT, properties).getChildRef();

        publicWorkflow = nodeService.createNode(workflowsHome,
                                                ContentModel.ASSOC_CONTAINS,
                                                QName.createQName("{test}publicWorkflow"),
                                                ParapheurModel.TYPE_SAVED_WORKFLOW).getChildRef();
        nodeService.setProperty(publicWorkflow, ContentModel.PROP_NAME,
                                "workflowPublic");

        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        workflowProprietaire1 = nodeService.createNode(workflowsHome,
                                                       ContentModel.ASSOC_CONTAINS,
                                                       QName.createQName("{test}proprietaire1workflow"),
                                                       ParapheurModel.TYPE_SAVED_WORKFLOW).getChildRef();
        nodeService.setProperty(workflowProprietaire1, ContentModel.PROP_NAME,
                                "workflowProp1");
        nodeService.setProperty(workflowProprietaire1, ContentModel.PROP_CREATOR,
                                tenantService.getDomainUser("proprietaire1", tenantName));

        ContentWriter proprietaire1WorkflowWriter = contentService.getWriter(
                workflowProprietaire1, ContentModel.PROP_CONTENT, true);
        proprietaire1WorkflowWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        proprietaire1WorkflowWriter.putContent(
                "<circuit>"                                                                                                          +                                                                                                        "<validation>" + "<etape>" +                                                                           parapheur1.toString() + "</etape>" + "<etape>" + parapheur2.toString() +
                "</etape>" + "</validation>" + "<notification>" + "<etape>" + parapheur2.toString() + "</etape>" + "</notification>" +
                "</circuit>");
        nodeService.addAspect(workflowProprietaire1, ParapheurModel.ASPECT_PRIVATE_WORKFLOW, null);
        nodeService.setProperty(workflowProprietaire1,
                                ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS,
                                (Serializable) Collections.singletonList(parapheur1.toString()));
        nodeService.setProperty(workflowProprietaire1,
                                ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_GROUPS,
                                (Serializable) Collections.emptyList());

        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire2", tenantName));
        workflowProprietaire2 = nodeService.createNode(workflowsHome,
                                                       ContentModel.ASSOC_CONTAINS,
                                                       QName.createQName("{test}proprietaire2workflow"),
                                                       ParapheurModel.TYPE_SAVED_WORKFLOW).getChildRef();
        nodeService.setProperty(workflowProprietaire2, ContentModel.PROP_NAME,
                                "workflowProp2");
        nodeService.setProperty(workflowProprietaire2, ContentModel.PROP_CREATOR,
                                tenantService.getDomainUser("proprietaire2", tenantName));

        nodeService.addAspect(workflowProprietaire2, ParapheurModel.ASPECT_PRIVATE_WORKFLOW, null);
        nodeService.setProperty(workflowProprietaire2,
                                ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS,
                                (Serializable) Collections.singletonList(parapheur2.toString()));
        nodeService.setProperty(workflowProprietaire2,
                                ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_GROUPS,
                                (Serializable) Collections.emptyList());


        authenticationComponent.setCurrentUser(tenantService.getDomainUser("admin", tenantName));
        NodeRef typesFolderNode = nodeService.createNode(dictionaryHome,
                                                         ContentModel.ASSOC_CONTAINS,
                                                         QName.createQName("cm:metiertype", namespaceService),
                                                         ContentModel.TYPE_FOLDER).getChildRef();
        NodeRef typesNode = nodeService.createNode(typesFolderNode,
                                                   ContentModel.ASSOC_CONTAINS,
                                                   QName.createQName("cm:TypesMetier.xml"),
                                                   ContentModel.TYPE_CONTENT).getChildRef();
        ContentWriter typesNodeWriter = contentService.getWriter(typesNode,
                                                                 ContentModel.PROP_CONTENT, true);
        typesNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        typesNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/TypesMetier.xml"));

        NodeRef sousTypesFolderNode = nodeService.createNode(dictionaryHome,
                                                             ContentModel.ASSOC_CONTAINS,
                                                             QName.createQName("cm:metiersoustype", namespaceService),
                                                             ContentModel.TYPE_FOLDER).getChildRef();
        NodeRef sousTypesNode = nodeService.createNode(sousTypesFolderNode,
                                                       ContentModel.ASSOC_CONTAINS,
                                                       QName.createQName("cm:metier.xml", namespaceService),
                                                       ContentModel.TYPE_CONTENT).getChildRef();
        nodeService.setProperty(sousTypesNode, ContentModel.PROP_NAME, "metier.xml");
        ContentWriter sousTypesNodeWriter = contentService.getWriter(sousTypesNode, ContentModel.PROP_CONTENT, true);
        sousTypesNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/SousTypesMetier.xml"));

        NodeRef sousTypesDefautNode = nodeService.createNode(sousTypesFolderNode,
                                                             ContentModel.ASSOC_CONTAINS,
                                                             QName.createQName("cm:defaut.xml", namespaceService),
                                                             ContentModel.TYPE_CONTENT).getChildRef();
        nodeService.setProperty(sousTypesDefautNode, ContentModel.PROP_NAME, "defaut.xml");
        ContentWriter sousTypesNodeDefautWriter = contentService.getWriter(sousTypesDefautNode, ContentModel.PROP_CONTENT, true);
        sousTypesNodeDefautWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesNodeDefautWriter.putContent(this.getClass().getResourceAsStream("/samples/defaut.xml"));
    }

    /**
     * Teste la méthode isParapheur en lui passant en paramètre un parapheur/
     * <p/>
     * La méthode doit renvoyer <code>true</code>
     *
     * @see ParapheurServiceImpl#isParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsParapheur() {
        boolean result = parapheurService.isParapheur(parapheur1);
        Assert.assertTrue(result);
    }

    /**
     * Teste la méthode isParapheur en lui passant en paramètre un Folder.
     *
     * La méthode doit renvoyer <code>false</code>
     *
     * @see ParapheurServiceImpl#isParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsParapheurDossier() {
        boolean result = parapheurService.isParapheur(dossier1);
        Assert.assertFalse(result);
    }

    /**
     * Teste la méthode isParapheur en lui passant en paramètre
     * <code>null</code>.
     *
     * La méthode doit lever une exception de type IllegalArgumentException
     *
     * @see ParapheurServiceImpl#isParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsParapheurNull() {
        try {
            parapheurService.isParapheur(null);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode isCorbeille en lui passant en paramètre une corbeille.
     *
     * La méthode doit renvoyer <code>true</code>
     *
     * @see ParapheurServiceImpl#isCorbeille(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsCorbeille() {
        boolean result = parapheurService.isCorbeille(corbeilleEnPreparation1);
        Assert.assertTrue(result);
    }

    /**
     * Teste la méthode isCorbeille en lui passant en paramètre un folder.
     *
     * La méthode doit renvoyer <code>false</code>
     *
     * @see ParapheurServiceImpl#isCorbeille(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsCorbeilleFolder() {
        boolean result = parapheurService.isCorbeille(parapheur1);
        Assert.assertFalse(result);
    }

    /**
     * Teste la méthode isCorbeille en lui passant en paramètre
     * <code>null</code>.
     *
     * La méthode doit lever une exception de type
     * <code>IllegalArgumentException</code>
     *
     * @see ParapheurServiceImpl#isCorbeille(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsCorbeilleNull() {
        try {
            parapheurService.isCorbeille(null);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode isCorbeilleVirtuelle en lui passant en paramètre une
     * corbeille virtuelle.
     *
     * La méthode doit renvoyer <code>true</code>.
     *
     * @see ParapheurServiceImpl#isCorbeilleVirtuelle(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsCorbeilleVirtuelle() {
        boolean result = parapheurService.isCorbeilleVirtuelle(corbeilleVirtuelle);
        Assert.assertTrue(result);
    }

    /**
     * Teste la méthode isCorbeilleVirtuelle en lui passant en paramètre un
     * folder.
     *
     * La méthode doit renvoyer <code>false</code>.
     *
     * @see ParapheurServiceImpl#isCorbeilleVirtuelle(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsCorbeilleVirtuelleWithDossier() {
        boolean result = parapheurService.isCorbeilleVirtuelle(dossier1);
        Assert.assertFalse(result);
    }

    /**
     * Teste la méthode isCorbeilleVirtuelle en lui passant en paramètre
     * <code>null</code>.
     *
     * La méthode doit lever une exception de type IllegalArgumentException.
     *
     * @see ParapheurServiceImpl#isCorbeilleVirtuelle(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsCorbeilleVirtuelleNull() {
        try {
            parapheurService.isCorbeilleVirtuelle(null);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste si la méthode isDossier renvoie une valeur correcte lorsque un
     * dossier lui est passé en paramètre.
     *
     * La méthode doit renvoyer true.
     *
     * @see ParapheurServiceImpl#isDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsDossier() {
        boolean result = parapheurService.isDossier(dossier1);
        Assert.assertTrue(result);
    }

    /**
     * Teste si la méthode isDossier fonctionne correctement lorsque un
     * parapheur lui est passé en paramètre.
     *
     * La méthode doit renvoyer false.
     *
     * @see ParapheurServiceImpl#isDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsDossierFolder() {
        boolean result = parapheurService.isDossier(parapheur1);
        Assert.assertFalse(result);
    }

    /**
     * Teste si la méthode isDossier fonctionne correctement lorsque
     * <code>null</code> lui est passé en paramètre.
     *
     * La méthode doit lever une exception de type IllegalArgumentException.
     *
     * @see ParapheurServiceImpl#isDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsDossierNull() {
        try {
            parapheurService.isDossier(null);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode getParapheurs.
     *
     * La méthode doit renvoyer les deux parapheurs existant.
     *
     * @see ParapheurServiceImpl#getParapheurs()
     */
    public void testGetParapheurs() {
        List<NodeRef> parapheurs = parapheurService.getParapheurs();
        Assert.assertNotNull(parapheurs);
        Assert.assertEquals(2, parapheurs.size());
    }

    /**
     * Teste la méthode getCorbeilles.
     *
     * Cette méthode crée un parapheur, ainsi que deux corbeille et un autre
     * parapheur à l'interieur. La hierarchie obtenue est la suivante :
     * - localparapheur
     *     - parapheurnode1
     *     - corbeille1
     *     - corbeille2
     *
     * Le but est de vérifier que la méthode getCorbeilles() renvoie 2, ainsi,
     * on s'assure que :
     * - Les noeuds corbeille sont correctement comptés
     * - Que les noeuds non-corbeille ne sont pas comptés
     *
     * @see ParapheurServiceImpl#getCorbeilles(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetCorbeilles() {
        NodeRef parapheur = nodeService.createNode(rootNodeRef,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}localparapheur"),
                ParapheurModel.TYPE_PARAPHEUR).getChildRef();
        nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, QName.createQName("{test}parapheurnode1"),
                ParapheurModel.TYPE_PARAPHEUR);
        nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, QName.createQName("{test}corbeille1"),
                ParapheurModel.TYPE_CORBEILLE);
        nodeService.createNode(parapheur, ContentModel.ASSOC_CONTAINS, QName.createQName("{test}corbeille2"),
                ParapheurModel.TYPE_CORBEILLE);
        List<NodeRef> corbeilles = parapheurService.getCorbeilles(parapheur);
        Assert.assertNotNull(corbeilles);
        Assert.assertEquals(2, corbeilles.size());
    }

    /**
     * Teste la méthode getDossiers().
     *
     * On récupère la liste des dossiers présents dans la corbeille "Corbeille
     * en préparation 1".
     *
     * Cette méthode doit renvoyer 1.
     *
     * @see ParapheurServiceImpl#getDossiers(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetDossiers() {
        List<NodeRef> dossiers = parapheurService.getDossiers(corbeilleEnPreparation1);
        Assert.assertNotNull(dossiers);
        Assert.assertEquals(1, dossiers.size());
    }

    /**
     * Teste la méthode getDocuments().
     *
     * On récupère la liste des documents présents dans le dossier "Dossier 1".
     *
     * Cette méthode doit renvoyer une liste de taille 1.
     *
     * @see ParapheurServiceImpl#getDocuments(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetDocuments() {
        List<NodeRef> documents = parapheurService.getDocuments(dossier1);
        Assert.assertNotNull(documents);
        Assert.assertEquals(1, documents.size());
    }

    /**
     * Teste la méthode getCorbeille().
     *
     * On essaye de récupérer la corbeille "En preparation" depuis le parapheur
     * "Parapheur 1".
     *
     * Cette méthode doit renvoyer la corbeille "Corbeille en preparation 1"
     *
     * @see ParapheurServiceImpl#getCorbeille(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.namespace.QName)
     */
    public void testGetCorbeille() {
        NodeRef returned = parapheurService.getCorbeille(parapheur1,
                ParapheurModel.NAME_EN_PREPARATION);
        Assert.assertNotNull(returned);
        Assert.assertEquals(corbeilleEnPreparation1, returned);
    }

    /**
     * Teste la méthode getCorbeille() si jamais la corbeille demandée n'existe
     * pas.
     *
     * On essaye de récupérer la corbeille "A archiver" du parapheur "Parapheur
     * 2". Cette corbeille n'a pas été créé dans le bootstrap initial du test
     * unitaire.
     *
     * Cette méthode doit renvoyer null.
     *
     * @see ParapheurServiceImpl#getCorbeille(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.namespace.QName)
     */
    public void testGetCorbeilleDoesntExists() {
        NodeRef returned = parapheurService.getCorbeille(parapheur2,
                ParapheurModel.NAME_A_ARCHIVER);
        Assert.assertNull(returned);
    }

    /**
     * Teste la méthode getParentDossier().
     *
     * Essaye de récupérer le dossier contenant le document "Content 1".
     *
     * Cette méthode doit renvoyer "Dossier 1"
     *
     * @see ParapheurServiceImpl#getParentDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParentDossier() {
        NodeRef returned = parapheurService.getParentDossier(content1);
        Assert.assertNotNull(returned);
        Assert.assertEquals(dossier1, returned);
    }

    /**
     * Teste la méthode getParentCorbeille().
     *
     * Essaye de récupérer la corbeille contenant le dossier "Dossier 1".
     *
     * Cette méthode doit renvoyer "Corbeille en préparation 1"
     *
     * @see ParapheurServiceImpl#getParentCorbeille(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParentCorbeille() {
        NodeRef returned = parapheurService.getParentCorbeille(dossier1);
        Assert.assertNotNull(returned);
        Assert.assertEquals(corbeilleEnPreparation1, returned);
    }

    /**
     * Teste la méthode getParentCorbeille() avec un document.
     *
     * Essaye de récupérer la corbeille contenant le dossier contenant "Content
     * 1".
     *
     * Cette méthode doit renvoyer "Corbeille en preparation 1"
     *
     * @see ParapheurServiceImpl#getParentCorbeille(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParentCorbeilleFromContent() {
        NodeRef returned = parapheurService.getParentCorbeille(content1);
        Assert.assertNotNull(returned);
        Assert.assertEquals(corbeilleEnPreparation1, returned);
    }

    /**
     * Teste la méthode getParentParapheur().
     *
     * Essaye de récupérer le parapheur contenant la corbeille "Corbeille en
     * préparation 1".
     *
     * Cette méthode doit renvoyer "Parapheur 1".
     *
     * @see ParapheurServiceImpl#getParentParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParentParapheur() {
        NodeRef returned = parapheurService.getParentParapheur(corbeilleEnPreparation1);
        Assert.assertNotNull(returned);
        Assert.assertEquals(parapheur1, returned);
    }

    /**
     * Teste la méthode getParentParapheur() avec un dossier.
     *
     * Essaye de récupérer le parapheur contenant le dossier "Dossier 1".
     *
     * Cette méthode est censé renvoyer "Parapheur 1".
     *
     * @see ParapheurServiceImpl#getParentParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParentParapheurFromDossier() {
        NodeRef returned = parapheurService.getParentParapheur(dossier1);
        Assert.assertNotNull(returned);
        Assert.assertEquals(parapheur1, returned);
    }

    /**
     * Teste la méthode getParentParapheur() avec un document.
     *
     * Essaye de récupérer le parapheur contenant le dossier contenant "Content
     * 1".
     *
     * Cette méthode est censé renvoyer "Parapheur 1"
     *
     * @see ParapheurServiceImpl#getParentParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParentParapheurFromContent() {
        NodeRef returned = parapheurService.getParentParapheur(content1);
        Assert.assertNotNull(returned);
        Assert.assertEquals(parapheur1, returned);
    }

    /**
     * Teste la méthode getParapheurOwner() avec un document.
     *
     * Essaye de récupérer le propriétaire du parapheur contenant "Content 1".
     *
     * Cette méthode doit renvoyer "proprietaire1@tenantname".
     *
     * @see ParapheurServiceImpl#getParapheurOwner(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParapheurOwnerFromContent() {
        List<String> returned = parapheurService.getParapheurOwners(content1);
        Assert.assertNotNull(returned);
        Assert.assertTrue(returned.contains(tenantService.getDomainUser("proprietaire1", tenantName)));
    }

    /**
     * Teste la méthode getParapheurOwner() avec une corbeille.
     *
     * Essaye de récupérer le propriétaire du parapheur contenant la corbeille
     * "Corbeille en preparation 1".
     *
     * Cette méthode doit renvoyer "proprietaire1@tenantname".
     *
     * @see ParapheurServiceImpl#getParapheurOwner(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParapheurOwnerFromCorbeille() {
        List<String> returned = parapheurService.getParapheurOwners(corbeilleEnPreparation1);
        Assert.assertNotNull(returned);
        Assert.assertTrue(returned.contains(tenantService.getDomainUser("proprietaire1", tenantName)));
    }

    /**
     * Teste la méthode isParapheurOwner.
     *
     * Essaye de savoir si "proprietaire1@tenantname" est le proprietaire du
     * parapheur "Parapheur 1".
     *
     * Cette méthode doit renvoyer true.
     *
     * @see ParapheurServiceImpl#isParapheurOwner(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testIsParapheurOwner() {
        boolean returned = parapheurService.isParapheurOwner(parapheur1,
                tenantService.getDomainUser("proprietaire1", tenantName));
        Assert.assertTrue(returned);
    }

    /**
     * Teste la méthode isParapheurOwner() avec une corbeille.
     *
     * Essaye de savoir si "proprietaire1@tenantname" est le proprietaire de la
     * corbeille "Corbeille en préparation 1"
     *
     * Cette méthode doit lever une exception de type IllegalArgumentException.
     *
     * @see ParapheurServiceImpl#isParapheurOwner(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testIsParapheurOwnerFromCorbeille() {
        try {
            parapheurService.isParapheurOwner(corbeilleEnPreparation1,
                    tenantService.getDomainUser("proprietaire1", tenantName));
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode getOwnedParapheur().
     *
     * Essye de récupérer le parapheur appartenant à l'utilisateur
     * "proprietaire1@tenantname".
     *
     * Cette méthode doit renvoyer "Parapheur 1".
     *
     * @see ParapheurServiceImpl#getOwnedParapheur(java.lang.String)
     */
    public void testGetOwnedParapheur() {
        List<NodeRef> returned = parapheurService.getOwnedParapheurs(tenantService.getDomainUser("proprietaire1", tenantName));
        Assert.assertNotNull(returned);
        Assert.assertEquals(parapheur1, returned.get(0));
    }

    /**
     * Teste la méthode getSecretariatParapheurs().
     *
     * Essaye de récupérer les parapheurs pour les quels "secretaire" est
     * secretaire.
     *
     * Pour cela, "secretaire" est ajoutée en tant que secretaire sur les
     * parapheurs "Parapheur 1" et "Parapheur 2".
     *
     * Cette méthode doit renvoyer "Parapheur 1" et "Parapheur 2".
     *
     * @see ParapheurServiceImpl#getSecretariatParapheurs(java.lang.String)
     */
    public void testGetSecretariatParapheurs() {
        nodeService.setProperty(parapheur1,
                ParapheurModel.PROP_SECRETAIRES, (Serializable) Collections.singletonList("secretaire"));
        nodeService.setProperty(parapheur2,
                ParapheurModel.PROP_SECRETAIRES, (Serializable) Collections.singletonList("secretaire"));

        List<NodeRef> returned = parapheurService.getSecretariatParapheurs("secretaire");
        Assert.assertNotNull(returned);
        Assert.assertTrue(returned.size() == 2);
    }

    /**
     * Teste la méthode createDossier().
     *
     * Essaye de créer un dossier ayant pour nom "{dossier}" dans le parapheur
     * "Parapheur 1"
     *
     * Cette méthode doit créer un noeud dans la corbeille "Corbeille en
     * préparation 1". Ce noeud doit avoir la propriété "cm:name" égale à
     * "{dossier}"
     *
     * @see ParapheurServiceImpl#createDossier(org.alfresco.service.cmr.repository.NodeRef, java.util.Map)
     */
    public void testCreateDossier() {
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "{dossier}");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, false);

        NodeRef returned = dossierService.createDossier(parapheur1,
                properties);

        Assert.assertNotNull(returned);
        Assert.assertEquals(ParapheurModel.TYPE_DOSSIER, nodeService.getType(returned));
        Assert.assertEquals("{dossier}", nodeService.getProperty(returned,
                ContentModel.PROP_NAME));
    }

    /**
     * Teste la méthode createDossier() dans un dossier.
     *
     * Essaye de créer un dossier au sein du dossier "Dossier 1".
     *
     * Cette méthode doit lever une exceptio nde type IllegalArgumentException.
     *
     * @see ParapheurServiceImpl#createDossier(org.alfresco.service.cmr.repository.NodeRef, java.util.Map)
     */
    public void testCreateDossierInDossier() {
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "{dossier}");
        try {
            dossierService.createDossier(dossier1, properties);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode createDossier() sans nom.
     *
     * Essaye de créer un dossier sans nom.
     *
     * Cette méthode doit lever une exception de type IllegalArgumentException.
     *
     * @see ParapheurServiceImpl#createDossier(org.alfresco.service.cmr.repository.NodeRef, java.util.Map)
     */
    public void testCreateDossierWithoutName() {
        try {
            dossierService.createDossier(parapheur2, Collections.<QName, Serializable>emptyMap());
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode getEmetteur().
     *
     * Essaye de récupérer l'emetteur du dossier "Dossier 1".
     *
     * Cette méthode doit renvoyer "Parapheur 1"
     *
     * @see ParapheurServiceImpl#getEmetteur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetEmetteur() {
        NodeRef emetteur = parapheurService.getEmetteur(dossier1);
        Assert.assertNotNull(emetteur);
        Assert.assertEquals(parapheur1, emetteur);
    }

    /**
     * Essaye de récupérer l'emetteur d'une corbeille.
     *
     * @see ParapheurServiceImpl#getEmetteur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetEmetteurWithCorbeille() {
        try {
            parapheurService.getEmetteur(corbeilleEnPreparation1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode isEmis().
     *
     * @see ParapheurServiceImpl#isEmis(org.alfresco.service.cmr.repository.NodeRef) e
     */
    public void testIsEmis() {
        boolean returned = parapheurService.isEmis(dossier2);
        Assert.assertTrue(returned);
    }

    /**
     * Teste la méthode isEmis() avec un dossier non emis.
     *
     * @see ParapheurServiceImpl#isEmis(org.alfresco.service.cmr.repository.NodeRef) e
     */
    public void testIsNotEmis() {
        boolean returned = parapheurService.isEmis(dossier1);
        Assert.assertFalse(returned);
    }

    /**
     * Teste la méthode isEmis() avec une corbeille.
     *
     * @see ParapheurServiceImpl#isEmis(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsEmisWithCorbeille() {
        try {
            parapheurService.isEmis(corbeilleEnPreparation1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode isTermine().
     *
     * @see ParapheurServiceImpl#isTermine(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsTermine() {
        nodeService.setProperty(dossier1, ParapheurModel.PROP_TERMINE,
                true);
        boolean returned = parapheurService.isTermine(dossier1);
        Assert.assertTrue(returned);
    }

    /**
     * Teste la méthode isTermine() avec un dossier non terminé.
     *
     * @see ParapheurServiceImpl#isTermine(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsNotTermine() {
        nodeService.setProperty(dossier1, ParapheurModel.PROP_TERMINE,
                false);
        boolean returned = parapheurService.isTermine(dossier1);
        Assert.assertFalse(returned);
    }

    /**
     * Teste la méthode isTermine() avec une corbeille.
     *
     * @see ParapheurServiceImpl#isTermine(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsTermineWithCorbeille() {
        try {
            parapheurService.isTermine(corbeilleEnPreparation1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode getCircuit().
     *
     * @see ParapheurServiceImpl#getCircuit(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetCircuit() {
        List<EtapeCircuit> returned = parapheurService.getCircuit(dossier1);
        Assert.assertNotNull(returned);
        Assert.assertEquals(3, returned.size());
    }

    /**
     * Teste la méthode getCircuit() avec une corbeille
     *
     * @see ParapheurServiceImpl#getCircuit(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetCircuitWithCorbeille() {
        try {
            parapheurService.getCircuit(corbeilleEnPreparation1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode getActeurCourant().
     *
     * @see ParapheurServiceImpl#getActeurCourant(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetActeursCourant() {
        List<String> returned = parapheurService.getActeursCourant(dossier1);
        Assert.assertNotNull(returned);
        Assert.assertTrue(returned.contains(tenantService.getDomainUser("proprietaire1", tenantName)));
    }

    /**
     * Teste la méthode getActeurCourant() avec une corbeille.
     *
     * @see ParapheurServiceImpl#getActeurCourant(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetActeurCourantWithCorbeille() {
        try {
            parapheurService.getActeursCourant(corbeilleEnPreparation1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode isActeurCourant().
     *
     * @see ParapheurServiceImpl#isActeurCourant(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testIsActeurCourant() {
        boolean returned = parapheurService.isActeurCourant(dossier1,
                tenantService.getDomainUser("proprietaire1", tenantName));
        Assert.assertTrue(returned);
    }

    /**
     * Teste la méthode isActeurCourant() avec un propriétaire non-acteur
     * courant du dossier.
     *
     * @see ParapheurServiceImpl#isActeurCourant(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testIsNotActeurCourant() {
        boolean returned = parapheurService.isActeurCourant(dossier1,
                "anotherProprietaire");
        Assert.assertFalse(returned);
    }

    /**
     * Teste la méthode isActeurCourant avec une corbeille.
     *
     * @see ParapheurServiceImpl#isActeurCourant(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testIsActeurCourantWithCorbeille() {
        try {
            parapheurService.isActeurCourant(corbeilleEnPreparation1,
                    "proprietaire");
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode isActeurCourant() sans nom d'utilisateur.
     *
     * @see ParapheurServiceImpl#isActeurCourant(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testIsActeurCourantWithNoUser() {
        try {
            parapheurService.isActeurCourant(dossier1, null);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode isActeurCourant() avec l'utilisateur "".
     *
     * @see ParapheurServiceImpl#isActeurCourant(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testIsActeurCourantWithNoUser2() {
        try {
            parapheurService.isActeurCourant(dossier1, "");
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode getAnnotationPublique().
     *
     * @see ParapheurServiceImpl#getAnnotationPublique(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetAnnotationPublique() {
        String returned = parapheurService.getAnnotationPublique(dossier2);
        Assert.assertNotNull(returned);
        Assert.assertEquals("annotationpublique", returned);
    }

    /**
     * Teste la méthode getAnnotationPublique() avec une corbeille.
     *
     * @see ParapheurServiceImpl#getAnnotationPublique(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetAnnotationPubliqueWithCorbeille() {
        try {
            parapheurService.getAnnotationPublique(corbeilleEnPreparation1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode setAnnotationPublique().
     *
     * @see ParapheurServiceImpl#setAnnotationPublique(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testSetAnnotationPublique() {
        parapheurService.setAnnotationPublique(dossier2,
                "annotationpublique");
        String returned = (String) nodeService.getProperty(circuitEtape4,
                ParapheurModel.PROP_ANNOTATION);
        Assert.assertNotNull(returned);
        Assert.assertEquals("annotationpublique", returned);
    }

    /**
     * Teste la méthode setAnnotationPublique() avec une corbeille.
     *
     * @see ParapheurServiceImpl#setAnnotationPublique(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testSetAnnotationPubliqueWithCorbeille() {
        try {
            parapheurService.setAnnotationPublique(
                    corbeilleEnPreparation1, "annotationpublique");
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode getAnnotationPrivee().
     *
     * @see ParapheurServiceImpl#getAnnotationPrivee(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetAnnotationPrivee() {
        String returned = parapheurService.getAnnotationPrivee(dossier2);
        Assert.assertNotNull(returned);
        Assert.assertEquals("annotationprivee", returned);
    }

    /**
     * Teste la méthode getAnnotationPrivee() avec une corbeille.
     *
     * @see ParapheurServiceImpl#getAnnotationPrivee(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetAnnotationPriveeWithCorbeille() {
        try {
            parapheurService.getAnnotationPrivee(corbeilleEnPreparation1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode getAnnotationPriveePrecedente().
     *
     * @see ParapheurServiceImpl#getAnnotationPriveePrecedente(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetAnnotationPriveePrecedente() {
        String returned = parapheurService.getAnnotationPriveePrecedente(dossier2);
        Assert.assertNotNull(returned);
        Assert.assertEquals("annotationpriveeprecedente", returned);
    }

    /**
     * Teste la méthode getAnnotationPriveePrecedente() avec une corbeille.
     *
     * @see ParapheurServiceImpl#getAnnotationPriveePrecedente(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetAnnotationPriveePrecedenteWithCorbeille() {
        try {
            parapheurService.getAnnotationPriveePrecedente(corbeilleEnPreparation1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode setAnnotationPrivee().
     *
     * @see ParapheurServiceImpl#setAnnotationPrivee(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testSetAnnotationPrivee() {
        parapheurService.setAnnotationPrivee(dossier1,
                "annotationprivee");

        String returned = (String) nodeService.getProperty(circuitEtape1,
                ParapheurModel.PROP_ANNOTATION_PRIVEE);
        Assert.assertNotNull(returned);
        Assert.assertEquals("annotationprivee", returned);
    }

    /**
     * Teste la méthode setAnnotationPrivee() avec une corbeille.
     *
     * @see ParapheurServiceImpl#setAnnotationPrivee(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testSetAnnotationPriveeWithCorbeille() {
        try {
            parapheurService.setAnnotationPrivee(
                    corbeilleEnPreparation1, "annotationprivee");
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode approve().
     *
     * Ce test s'authentifie en tant que "proprietaire1@tenantname" afin de
     * valider le dossier "Dossier 1" qui se situe dans son parapheur.
     *
     * @see ParapheurServiceImpl#approve(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testApprove() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(dossier1, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_NON_LU);
        parapheurService.approve(dossier1);

        boolean etape1Effectuee = (Boolean) nodeService.getProperty(
                circuitEtape1, ParapheurModel.PROP_EFFECTUEE);
        Assert.assertTrue(etape1Effectuee);
        Assert.assertFalse(nodeService.hasAspect(content1,
                ParapheurModel.ASPECT_LU));
        Assert.assertEquals(corbeilleATraiter2, nodeService.getPrimaryParent(dossier1).getParentRef());
        Mockito.verify(mockedActionService, Mockito.atLeastOnce()).executeAction(Mockito.any(Action.class),
                Mockito.eq(dossier1));
    }

    /**
     * Teste la méthode approve() pour la dernière étape du circuit.
     *
     * @see ParapheurServiceImpl#approve(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testApproveDerniereEtape() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire2", tenantName));
        nodeService.setProperty(dossier2, ParapheurModel.PROP_SIGNATURE_PAPIER, false);
        nodeService.setProperty(dossier2, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_LU);

        parapheurService.approve(dossier2);

        boolean etape4Effectuee = (Boolean) nodeService.getProperty(
                circuitEtape4, ParapheurModel.PROP_EFFECTUEE);
        Assert.assertTrue(etape4Effectuee);
        Assert.assertEquals(corbeilleAArchiver1, nodeService.getPrimaryParent(dossier2).getParentRef());
        Mockito.verify(mockedActionService, Mockito.atLeastOnce()).executeAction(Mockito.any(Action.class),
                Mockito.eq(dossier2));
    }

    /**
     * Teste la méthode approve() avec une mauvaise authentification.
     *
     * Ce test essaye de valider le dossier "Dossier 2", contenu dans le
     * parapheur "Parapheur 2", en tant que "proprietaire1@tenantname", qui
     * n'est pas propriétaire du parapheur.
     *
     * @see ParapheurServiceImpl#approve(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testApproveWithNoAuthentication() {
        try {
            authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
            nodeService.setProperty(dossier2, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_NON_LU);
            parapheurService.approve(dossier2);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(corbeilleATraiter2, nodeService.getPrimaryParent(dossier2).getParentRef());
        }
    }

    /**
     * Teste l'approbation d'une étape de signature avec un document non lu.
     *
     * @see ParapheurServiceImpl#approve(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testApproveDerniereEtapeUnreadDossier() {
        try {
            authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire2", tenantName));
            nodeService.setProperty(dossier2, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_NON_LU);
            nodeService.removeAspect(content2, ParapheurModel.ASPECT_LU);
            parapheurService.approve(dossier2);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye d'approuver une corbeille.
     *
     * @see ParapheurServiceImpl#approve(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testApproveWithCorbeille() {
        try {
            parapheurService.approve(corbeilleAArchiver1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode reject().
     *
     * @see ParapheurServiceImpl#reject(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testReject() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire2", tenantName));

        parapheurService.reject(dossier2);

        Assert.assertEquals(corbeilleRetournes1, nodeService.getPrimaryParent(dossier2).getParentRef());
        Mockito.verify(mockedActionService, Mockito.atLeastOnce()).executeAction(Mockito.any(Action.class),
                Mockito.eq(dossier2));
    }

    /**
     * Essaye de rejeter un dossier sans authentification.
     *
     * @see ParapheurServiceImpl#reject(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testRejectWithoutAuthentication() {
        try {
            authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
            parapheurService.reject(dossier2);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(corbeilleATraiter2, nodeService.getPrimaryParent(dossier2).getParentRef());
        }
    }

    /**
     * Essaye de rejeter une corbeille.
     *
     * @see ParapheurServiceImpl#reject(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testRejectWithCorbeille() {
        try {
            parapheurService.reject(corbeilleAArchiver1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye de reprendre un dossier.
     *
     * @see ParapheurServiceImpl#reprendreDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testReprendreDossier() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.removeAspect(dossier2, ParapheurModel.ASPECT_LU);

        parapheurService.reprendreDossier(dossier2);
        Assert.assertEquals(corbeilleEnPreparation1, nodeService.getPrimaryParent(dossier2).getParentRef());
    }

    /**
     * Essaye de reprendre une corbeille.
     *
     * @see ParapheurServiceImpl#reprendreDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testReprendreDossierWithCorbeille() {
        try {
            parapheurService.reprendreDossier(corbeilleAArchiver1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye d'envoyer un dossier au secretariat
     *
     * @see ParapheurServiceImpl#secretariat(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testSecretariatAller() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire2", tenantName));

        parapheurService.secretariat(dossier2);

        Assert.assertTrue(nodeService.hasAspect(dossier2,
                ParapheurModel.ASPECT_SECRETARIAT));
        Assert.assertEquals(corbeilleSecretariat2, nodeService.getPrimaryParent(dossier2).getParentRef());
    }

    /**
     * Essaye de renvoyer un dossier "A traiter" au patron.
     *
     * @see ParapheurServiceImpl#secretariat(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testSecretariatRetourATraiter() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("secretaire2", tenantName));
        parapheurService.secretariat(dossierSecretariat2);
        Assert.assertFalse(nodeService.hasAspect(dossierSecretariat2,
                ParapheurModel.ASPECT_SECRETARIAT));
        Assert.assertEquals(corbeilleATraiter2, nodeService.getPrimaryParent(dossierSecretariat2).getParentRef());
    }

    /**
     * Essaye de renvoyer un dossier "En preparation" au patron.
     *
     * @see ParapheurServiceImpl#approve(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testSecretariatRetourEnPreparation() {
        nodeService.setProperty(etape1DossierSecretariat,
                ParapheurModel.PROP_EFFECTUEE, false);
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("secretaire2", tenantName));
        parapheurService.secretariat(dossierSecretariat2);
        Assert.assertFalse(nodeService.hasAspect(dossierSecretariat2,
                ParapheurModel.ASPECT_SECRETARIAT));
        Assert.assertEquals(corbeilleEnPreparation2, nodeService.getPrimaryParent(dossierSecretariat2).getParentRef());
    }

    /**
     * Essaye de récupérer un dossier.
     *
     * @see ParapheurServiceImpl#recupererDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testRecupererDossier() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        parapheurService.recupererDossier(dossier2);

        Assert.assertEquals(corbeilleEnPreparation1, nodeService.getPrimaryParent(dossier2).getParentRef());
    }

    /**
     * Essaye de récupérer un dossier non récupérable.
     *
     * @see ParapheurServiceImpl#recupererDossier(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testRecupererDossierNonRecuperable() {
        try {
            parapheurService.recupererDossier(dossier1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye d'archiver un dossier.
     *
     * @see ParapheurServiceImpl#archiver(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testArchiver() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_ARCHIVE);

        String returned = null;
        try {
            returned = parapheurService.archiver(dossierAArchiver,
             "dossierAArchiver");
        } catch (Exception ex) {
            Logger.getLogger(ParapheurServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        Assert.assertNull(returned);
        Assert.assertFalse(nodeService.exists(dossierAArchiver));

        List<FileInfo> results = fileFolderService.search(archivesHome,
                "dossierAArchiver", false);
        Assert.assertEquals(1, results.size());

        NodeRef dossierArchive = results.get(0).getNodeRef();
        ContentReader childContent = contentService.getReader(dossierArchive,
                ContentModel.PROP_CONTENT);
        Assert.assertEquals(MimetypeMap.MIMETYPE_PDF, childContent.getMimetype());
    }

    /**
     * Essaye d'archiver un dossier avec l'aspect S2low.
     *
     * @see ParapheurServiceImpl#archiver(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testArchiverS2low() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_ARCHIVE);
        nodeService.addAspect(dossierAArchiver, ParapheurModel.ASPECT_S2LOW,
                null);
        nodeService.setProperty(dossierAArchiver,
                ParapheurModel.PROP_TRANSACTION_ID, 12345);
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_STATUS, 4);

        ContentWriter contentWriter = contentService.getWriter(dossierAArchiver, ParapheurModel.PROP_ARACTE_XML, true);
        contentWriter.putContent(this.getClass().getResourceAsStream("/samples/aracte.xml"));
        try {
            String returned = parapheurService.archiver(dossierAArchiver,
                    "dossierAArchiver");
        } catch (Exception ex) {
            Logger.getLogger(ParapheurServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        Assert.assertFalse(nodeService.exists(dossierAArchiver));

        List<FileInfo> results = fileFolderService.search(archivesHome,
                "dossierAArchiver", false);
        Assert.assertEquals(1, results.size());

        NodeRef dossierArchive = results.get(0).getNodeRef();
        ContentReader childContent = contentService.getReader(dossierArchive,
                ContentModel.PROP_CONTENT);
        Assert.assertEquals(MimetypeMap.MIMETYPE_PDF, childContent.getMimetype());
    }

    /**
     * Essaye d'archiver un dossier sans authentification.
     *
     * @see ParapheurServiceImpl#archiver(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testArchiverWithoutAuthentication() {
        try {
            parapheurService.archiver(dossierAArchiver, "dossierAArchiver");
            Assert.fail();
        } catch (IllegalArgumentException e) {
        } catch (Exception ex) {
            Logger.getLogger(ParapheurServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Essaye d'archiver un dossier non termine.
     *
     * @see ParapheurServiceImpl#archiver(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testArchiverDossierNonTermine() {
        try {
            nodeService.setProperty(dossierAArchiver,
                    ParapheurModel.PROP_TERMINE, false);
            parapheurService.archiver(dossierAArchiver, "dossierAArchiver");
            Assert.fail();
        } catch (IllegalArgumentException e) {
        } catch (Exception ex) {
            Logger.getLogger(ParapheurServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void testArchiverDossierRejete() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_REJET_VISA);
        String returned = null;
        try {
            returned = parapheurService.archiver(dossierAArchiver,
             "dossierAArchiver");
        } catch (Exception ex) {
            Logger.getLogger(ParapheurServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        Assert.assertNull(returned);
        Assert.assertFalse(nodeService.exists(dossierAArchiver));

        List<FileInfo> results = fileFolderService.search(archivesHome,
                "dossierAArchiver", false);
        Assert.assertEquals(1, results.size());

        NodeRef dossierArchive = results.get(0).getNodeRef();
        ContentReader childContent = contentService.getReader(dossierArchive,
                ContentModel.PROP_CONTENT);
        Assert.assertEquals(MimetypeMap.MIMETYPE_PDF, childContent.getMimetype());
    }

    /**
     * Essaye d'archiver un dossier avec visuel PDF invalide.
     *
     * @see ParapheurServiceImpl#archiver(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testArchiverVisuelNull() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(dossierAArchiver, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_ARCHIVE);

        NodeRef doc = nodeService.createNode(dossierAArchiver,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}testArchiverVisuelNull"),
                ContentModel.PROP_CONTENT).getChildRef();
        ContentWriter docWriter = contentService.getWriter(doc, ParapheurModel.PROP_VISUEL_PDF, true);
        docWriter.putContent("null");

        String returned = null;
        try {
            returned = parapheurService.archiver(dossierAArchiver, "dossierAArchiver");
        } catch (Exception ex) {
            Logger.getLogger(ParapheurServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        Assert.assertNull(returned);
        Assert.assertFalse(nodeService.exists(dossierAArchiver));

        List<FileInfo> results = fileFolderService.search(archivesHome, "dossierAArchiver", false);
        Assert.assertEquals(1, results.size());

        NodeRef dossierArchive = results.get(0).getNodeRef();
        ContentReader childContent = contentService.getReader(dossierArchive, ContentModel.PROP_CONTENT);
        Assert.assertEquals(MimetypeMap.MIMETYPE_PDF, childContent.getMimetype());
    }

    /**
     * Récupère la liste des workflows sauvegardés.
     *
     * @see ParapheurServiceImpl#getSavedWorkflows()
     */
    /*

    public void testGetSavedWorkflows() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        Map<String, NodeRef> noderefs = parapheurService.getSavedWorkflows();
        Assert.assertEquals(2, noderefs.size());
        Assert.assertTrue(noderefs.containsKey("workflowPublic"));
        Assert.assertTrue(noderefs.containsKey("workflowProp1"));
        Assert.assertEquals(publicWorkflow, noderefs.get("workflowPublic"));
        Assert.assertEquals(workflowProprietaire1, noderefs.get("workflowProp1"));
    }
    */
    /**
     * Récupère la liste des workflows sauvegardés sans authentification.
     *
     * @see ParapheurServiceImpl#getSavedWorkflows()
     */
    /*
    public void testGetSavedWorkflowsWithoutAuthentication() {
        Map<String, NodeRef> noderefs = parapheurService.getSavedWorkflows();
        Assert.assertEquals(1, noderefs.size());
        Assert.assertTrue(noderefs.containsKey("workflowPublic"));
        Assert.assertEquals(publicWorkflow, noderefs.get("workflowPublic"));
    }
    */

    /**
     * Récupère la liste des workflows publics.
     *
     * @see ParapheurServiceImpl#getSavedWorkflows()
     */
    public void testGetPublicWorkflows() {
        Map<String, NodeRef> noderefs = parapheurService.getPublicWorkflows();
        Assert.assertEquals(1, noderefs.size());
        Assert.assertTrue(noderefs.containsKey("workflowPublic"));
        Assert.assertEquals(publicWorkflow, noderefs.get("workflowPublic"));
    }

    /**
     * Recupère la liste des workflows appartenant à l'utilisateur connecté.
     *
     * @see ParapheurServiceImpl#getOwnedWorkflows()
     */
    public void testGetOwnedWorkflows() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        Map<String, NodeRef> noderefs = parapheurService.getOwnedWorkflows();
        assertEquals(1, noderefs.size());
        assertTrue(noderefs.containsKey("workflowProp1"));
        assertEquals(workflowProprietaire1, noderefs.get("workflowProp1"));
    }

    /**
     * Essaye de charger un workflow sauvegardé.
     *
     * @see ParapheurServiceImpl#loadSavedWorkflow(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testLoadSavedWorkflow() {
        SavedWorkflow workflow = parapheurService.loadSavedWorkflow(workflowProprietaire1);
        Assert.assertNotNull(workflow);
        Assert.assertEquals(2, workflow.getCircuit().size());
        Assert.assertEquals(parapheur1, workflow.getCircuit().get(0).getParapheur());
        Assert.assertEquals(parapheur2, workflow.getCircuit().get(1).getParapheur());
    }

    /**
     * Essaye de sauvegarder un workflow privé.
     *
     * @see ParapheurServiceImpl#saveWorkflow(java.lang.String, java.util.List, java.util.Set, java.util.Set, boolean)
     */
    public void testSavePrivateWorkflow() throws Exception {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        NodeRef workflow = parapheurService.saveWorkflow("savedWorkflow",
                Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "SIGNATURE", Collections.<NodeRef>emptySet())),
                Collections.<NodeRef>emptySet(),
                Collections.<String>emptySet(), false);

        Assert.assertNotNull(workflow);
        Assert.assertTrue(nodeService.exists(workflow));
        Assert.assertEquals(workflowsHome, nodeService.getPrimaryParent(workflow).getParentRef());
        Assert.assertEquals("savedWorkflow", nodeService.getProperty(workflow,
                ContentModel.PROP_NAME));

        ContentReader workflowReader = contentService.getReader(workflow,
                ContentModel.PROP_CONTENT);
        Assert.assertEquals(MimetypeMap.MIMETYPE_XML, workflowReader.getMimetype());
    }

    /**
     * Essaye de sauvegarder un workflow public
     * @see ParapheurServiceImpl#saveWorkflow(java.lang.String, java.util.List, java.util.Set, boolean)
     */
    public void testSavePublicWorkflow() throws Exception {
        NodeRef workflow = parapheurService.saveWorkflow("savedWorkflow",
                Arrays.asList(parapheur1, parapheur2),
                Collections.singleton(parapheur2), true);

        Assert.assertNotNull(workflow);
        Assert.assertTrue(nodeService.exists(workflow));
        Assert.assertEquals(workflowsHome, nodeService.getPrimaryParent(
                workflow).getParentRef());
        Assert.assertEquals("savedWorkflow", nodeService.getProperty(workflow,
                ContentModel.PROP_NAME));

        ContentReader workflowReader = contentService.getReader(workflow,
                ContentModel.PROP_CONTENT);
        Assert.assertEquals(MimetypeMap.MIMETYPE_XML, workflowReader.getMimetype());

        SAXReader saxreader = new SAXReader();
        Document document = saxreader.read(new StringReader(workflowReader.getContentString()));
        Element rootElement = document.getRootElement();
        Element validation = rootElement.element("validation");
        List<Element> etapesValidation = validation.elements("etape");
        Assert.assertEquals(2, etapesValidation.size());
        Assert.assertEquals(parapheur1.toString(), etapesValidation.get(
                0).getTextTrim());
        Assert.assertEquals(parapheur2.toString(), etapesValidation.get(
                1).getTextTrim());
        Element notification = rootElement.element("notification");
        List<Element> etapesNotification = notification.elements("etape");
        Assert.assertEquals(1, etapesNotification.size());
        Assert.assertEquals(parapheur2.toString(), etapesNotification.get(0).getTextTrim());
    }

    /**
     * Teste la méthode isRecuperable().
     *
     * @see ParapheurServiceImpl#isRecuperable(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsRecuperable() {
        boolean returned = parapheurService.isRecuperable(dossier2);
        Assert.assertTrue(returned);
    }

    /**
     * Teste la méthode isRecuperable() avec un dossier non récupérable.
     *
     * @see ParapheurServiceImpl#isRecuperable(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsNotRecuperable() {
        boolean returned = parapheurService.isRecuperable(dossier1);
        Assert.assertFalse(returned);
    }

    /**
     * Teste la méthode isRecuperable() avec une corbeille.
     *
     * @see ParapheurServiceImpl#isRecuperable(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testIsRecuperableWithCorbeille() {
        try {
            parapheurService.isRecuperable(corbeilleAArchiver1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye de récupérer le parapheur responsable d'un autre parapheur
     *
     * @see ParapheurServiceImpl#getParapheurResponsable(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParapheurResponsable() {
        NodeRef returned = parapheurService.getParapheurResponsable(parapheur2);
        Assert.assertNotNull(returned);
        Assert.assertEquals(parapheur1, returned);
    }

    /**
     * Essaye de récupérer le parapheur responsable d'un parapheur sans
     * suppérieur hiérarchique.
     *
     * @see ParapheurServiceImpl#getParapheurResponsable(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParapheurResponsable2() {
        NodeRef returned = parapheurService.getParapheurResponsable(parapheur1);
        Assert.assertNull(returned);
    }

    /**
     * Essaye de récupérer le suppérieur hiérarchique d'une corbeille
     *
     * @see ParapheurServiceImpl#getParapheurResponsable(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetParapheurResponsableWithCorbeille() {
        try {
            parapheurService.getParapheurResponsable(corbeilleAArchiver1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode setSignataire()
     *
     * @see ParapheurServiceImpl#setSignataire(org.alfresco.service.cmr.repository.NodeRef, java.lang.String)
     */
    public void testSetSignataire() {
        parapheurService.setSignataire(dossier2, tenantService.getDomainUser("proprietaire2", tenantName));

        Assert.assertEquals(tenantService.getDomainUser("proprietaire2", tenantName), nodeService.getProperty(
                circuitEtape4, ParapheurModel.PROP_SIGNATAIRE));
        Assert.assertNull(nodeService.getProperty(circuitEtape4,
                ParapheurModel.PROP_SIGNATURE));
    }

    /**
     * Teste la méthode setSignataire() avec une signature.
     *
     * @see ParapheurServiceImpl#setSignataire(org.alfresco.service.cmr.repository.NodeRef, java.lang.String, java.security.cert.X509Certificate[])
     */
    public void testSetSignataireWithSignature() {
        X509Certificate cert = Mockito.mock(X509Certificate.class);
        Mockito.when(cert.getIssuerX500Principal()).thenReturn(
                new X500Principal("CN=issuer,OU=adullact"));
        Mockito.when(cert.getSubjectX500Principal()).thenReturn(
                new X500Principal("CN=subject,OU=adullact"));

        X509Certificate[] certs = new X509Certificate[3];
        Arrays.fill(certs, cert);
        parapheurService.setSignataire(dossier2, tenantService.getDomainUser("proprietaire2", tenantName), certs);

        Assert.assertEquals(tenantService.getDomainUser("proprietaire2", tenantName), nodeService.getProperty(
                circuitEtape4, ParapheurModel.PROP_SIGNATAIRE));
        Assert.assertNotNull(nodeService.getProperty(circuitEtape4,
                ParapheurModel.PROP_SIGNATURE));
        String signature = (String) nodeService.getProperty(circuitEtape4,
                ParapheurModel.PROP_SIGNATURE);
        Assert.assertTrue(signature.contains("issuer"));
        Assert.assertTrue(signature.contains("subject"));
    }

    /**
     * Teste la méthode setSignataire() avec une signature ne contenant pas de
     * CN.
     *
     * @see ParapheurServiceImpl#setSignataire(org.alfresco.service.cmr.repository.NodeRef, java.lang.String, java.security.cert.X509Certificate[])
     */
    public void testSetSignataireWithSignatureWithoutCN() {
        X509Certificate cert = Mockito.mock(X509Certificate.class);
        Mockito.when(cert.getIssuerX500Principal()).thenReturn(
                new X500Principal("O=adullact,C=FR"));
        Mockito.when(cert.getSubjectX500Principal()).thenReturn(
                new X500Principal("CN=subject,O=adullact,C=FR"));

        X509Certificate[] certs = new X509Certificate[3];
        Arrays.fill(certs, cert);
        parapheurService.setSignataire(dossier2, tenantService.getDomainUser("proprietaire2", tenantName), certs);

        Assert.assertEquals(tenantService.getDomainUser("proprietaire2", tenantName), nodeService.getProperty(
                circuitEtape4, ParapheurModel.PROP_SIGNATAIRE));
        Assert.assertNotNull(nodeService.getProperty(circuitEtape4,
                ParapheurModel.PROP_SIGNATURE));
        String signature = (String) nodeService.getProperty(circuitEtape4,
                ParapheurModel.PROP_SIGNATURE);
        Assert.assertTrue(signature.contains("subject"));
    }

    /**
     * Essaye de changer le signataire d'une corbeille.
     *
     * @see ParapheurServiceImpl#setSignataire(org.alfresco.service.cmr.repository.NodeRef, java.lang.String, java.security.cert.X509Certificate[])
     */
    public void testSetSignataireWithCorbeille() {
        try {
            parapheurService.setSignataire(corbeilleEnPreparation1,
                    tenantService.getDomainUser("proprietaire1", tenantName));
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode getNomParapheur().
     *
     * @see ParapheurServiceImpl#getNomParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetNomParapheur() {
        String returned = parapheurService.getNomParapheur(parapheur1);
        Assert.assertEquals("parapheur1", returned);
    }

    /**
     * Teste la méthode getNomParapheur() avec une corbeille.
     *
     * @see ParapheurServiceImpl#getNomParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetNomParapheurWithCorbeille() {
        try {
            parapheurService.getNomParapheur(corbeilleATraiter2);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode getNomProprietaires().
     *
     * @see ParapheurServiceImpl#getNomProprietaires(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetNomProprietaires() {
        List<String> returned = parapheurService.getNomProprietaires(parapheur1);
        Assert.assertEquals("Prop Ietaire", returned.get(0));
    }

    /**
     * Teste la méthode getNomProprietaire().
     *
     * @see ParapheurServiceImpl#getNomProprietaire(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetNomProprietaire() {
        String returned = parapheurService.getNomProprietaire(parapheur1);
        Assert.assertEquals("Prop Ietaire", returned);
    }
    /**
     * Teste la méthode getNomProprietaire() avec une corbeille.
     *
     * @see ParapheurServiceImpl#getNomProprietaire(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetNomProprietaireWithCorbeille() {
        try {
            parapheurService.getNomProprietaire(corbeilleATraiter2);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode createParapheur().
     *
     * @see ParapheurServiceImpl#createParapheur(java.util.Map)
     */
    public void testCreateParapheur() {
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "parapheur3");
        NodeRef parapheur = parapheurService.createParapheur(properties);
        Assert.assertNotNull(parapheur);
        Assert.assertEquals("parapheur3", nodeService.getProperty(parapheur,
                ContentModel.PROP_NAME));
        Assert.assertNotNull(nodeService.getProperty(parapheur,
                ContentModel.PROP_CREATOR));
        Assert.assertNotNull(nodeService.getProperty(parapheur,
                ContentModel.PROP_CREATED));

        List<ChildAssociationRef> childs = nodeService.getChildAssocs(parapheur);
        Assert.assertTrue(childs.size() >= 7);
    }

    /**
     * Essaye de créer un parapheur sans nom.
     *
     * @see ParapheurServiceImpl#createParapheur(java.util.Map)
     */
    public void testCreateParapheurWithoutName() {
        try {
            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            parapheurService.createParapheur(properties);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye de créer un parapheur avec des caractères invalides.
     *
     * Une exception doit être levée, mais elle n'est levée qu'au moment de la
     * validation de la transaction.
     *
     * La transaction courant est donc validée pendant le test unitaire afin de
     * vérifier que cette contrainte est correctement respectée.
     *
     * L'expression régulière servant à valider le nom d'un dossier est la
     * suivante :
     *     (.*[\"\*\\\>\<\?\/\:\|]+.*)|(.*[\.]?.*[\.]+$)|(.*[ ]+$)
     *
     * @see ParapheurServiceImpl#createParapheur(java.util.Map)
     */
    public void testCreateParapheurWithInvalidCharacters() {
        try {
            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, "abc<def");
            parapheurService.createParapheur(properties);

            // Valide la transaction courante
            transactionManager.commit(transactionStatus);
            Assert.fail();
        } catch (Exception e) {
        }
    }

    /**
     * Essaye de créer un parapheur dont le nom ce termine avec un point.
     *
     * Une exception doit être levée, mais elle n'est levée qu'au moment de la
     * validation de la transaction.
     *
     * La transaction courant est donc validée pendant le test unitaire afin de
     * vérifier que cette contrainte est correctement respectée.
     *
     * L'expression régulière servant à valider le nom d'un dossier est la
     * suivante :
     *     (.*[\"\*\\\>\<\?\/\:\|]+.*)|(.*[\.]?.*[\.]+$)|(.*[ ]+$)
     *
     * @see ParapheurServiceImpl#createParapheur(java.util.Map)
     */
    public void testCreateParapheurEndingWithPoint() {
        try {
            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, "abcdef.");
            parapheurService.createParapheur(properties);
            transactionManager.commit(transactionStatus);
            Assert.fail();
        } catch (Exception e) {
        }
    }

    /**
     * Essaye de créer un parapheur dont le nom ce termine avec un espace.
     *
     * Une exception doit être levée, mais elle n'est levée qu'au moment de la
     * validation de la transaction.
     *
     * La transaction courant est donc validée pendant le test unitaire afin de
     * vérifier que cette contrainte est correctement respectée.
     *
     * L'expression régulière servant à valider le nom d'un dossier est la
     * suivante :
     *     (.*[\"\*\\\>\<\?\/\:\|]+.*)|(.*[\.]?.*[\.]+$)|(.*[ ]+$)
     *
     * @see ParapheurServiceImpl#createParapheur(java.util.Map)
     */
    public void testCreateParapheurEndingWithSpace() {
        try {
            Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
            properties.put(ContentModel.PROP_NAME, "abcdef ");
            parapheurService.createParapheur(properties);
            transactionManager.commit(transactionStatus);
            Assert.fail();
        } catch (Exception e) {
        }
    }

    /**
     * Essaye de mettre en place une délégation.
     *
     * @see ParapheurServiceImpl#programmerDelegation(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testSetDelegation() {
        nodeService.removeAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_DELEGATION);
        parapheurService.programmerDelegation(parapheur1, parapheur2, new Date(), null, false);
        List<AssociationRef> refs = nodeService.getTargetAssocs(
                parapheur1, ParapheurModel.ASSOC_DELEGATION);
        Assert.assertEquals(1, refs.size());

        NodeRef child = refs.get(0).getTargetRef();
        Assert.assertEquals(parapheur2, child);

        refs = nodeService.getTargetAssocs(
                parapheur1, ParapheurModel.ASSOC_OLD_DELEGATION);
        Assert.assertEquals(1, refs.size());

        child = refs.get(0).getTargetRef();
        Assert.assertEquals(parapheur2, child);
    }

    /**
     * Essaye de mettre en place une délégation programmée.
     *
     * @see ParapheurServiceImpl#programmerDelegation(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testProgrammerDelegation() {
        nodeService.removeAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_DELEGATION);
        Date date = new Date();
        date.setTime(date.getTime() + 120000);
        parapheurService.programmerDelegation(parapheur1, parapheur2, date, null, false);

        List<AssociationRef> refs = nodeService.getTargetAssocs(
                parapheur1, ParapheurModel.ASSOC_DELEGATION);
        Assert.assertEquals(0, refs.size());

        refs = nodeService.getTargetAssocs(
                parapheur1, ParapheurModel.ASSOC_DELEGATION_PROGRAMMEE);
        Assert.assertEquals(1, refs.size());

        NodeRef child = refs.get(0).getTargetRef();
        Assert.assertEquals(parapheur2, child);

        refs = nodeService.getTargetAssocs(
                parapheur1, ParapheurModel.ASSOC_OLD_DELEGATION);
        Assert.assertEquals(0, refs.size());
    }

    /**
     * Essaye de définir une délégation avec une corbeille.
     *
     * @see ParapheurServiceImpl#programmerDelegation(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testSetDelegationWithCorbeille() {
        try {
            parapheurService.programmerDelegation(corbeilleAArchiver1,
                    parapheur2, new Date(), null, false);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye de définir une délégation avec une corbeille.
     *
     * @see ParapheurServiceImpl#programmerDelegationorg.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testSetDelegationWithCorbeille2() {
        try {
            parapheurService.programmerDelegation(parapheur1,
                    corbeilleAArchiver1, new Date(), null, false);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye de définir une délégation qui induirait une boucle de délégation.
     *
     * @see ParapheurServiceImpl#programmerDelegationorg.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testProgrammerDelegationLoop() {
        try {
            parapheurService.programmerDelegation(parapheur2, parapheur1, new Date(), null, false);
            Assert.fail();
        } catch (RuntimeException e) {
        }
    }

    /**
     * Teste la méthode getDelegation().
     *
     * @see ParapheurServiceImpl#getDelegation(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetDelegation() {
        NodeRef returned = parapheurService.getDelegation(parapheur1);
        Assert.assertNotNull(returned);
        Assert.assertEquals(parapheur2, returned);
    }

    public void testGetOldDelegation() {
        NodeRef returned = parapheurService.getOldDelegation(parapheur1);
        Assert.assertNotNull(returned);
        Assert.assertEquals(parapheur2, returned);
    }

    /**
     * Teste la méthode getDelegation() sur un parapheur n'ayant pas de
     * délégation en place.
     *
     * @see ParapheurServiceImpl#getDelegation(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetDelegationNull() {
        NodeRef returned = parapheurService.getDelegation(parapheur2);
        Assert.assertNull(returned);
    }

    /**
     * Teste la méthode getDelegation() avec une corbeille.
     *
     * @see ParapheurServiceImpl#getDelegation(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetDelegationWithCorbeille() {
        try {
            parapheurService.getDelegation(corbeilleATraiter2);
            Assert.fail();
        } catch (RuntimeException e) {
        }
    }

    /**
     * Teste la méthode getSecretariatParapheur().
     *
     * @see ParapheurServiceImpl#getSecretariatParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetSecretariatParapheur() {
        List<String> secretariat = parapheurService.getSecretariatParapheur(parapheur2);
        Assert.assertNotNull(secretariat);
        Assert.assertEquals(1, secretariat.size());
        Assert.assertEquals(tenantService.getDomainUser("secretaire2", tenantName), secretariat.get(0));
    }

    /**
     * Teste la méthode getSecretariatParapheur() avec une corbeille.
     *
     * @see ParapheurServiceImpl#getSecretariatParapheur(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetSecretariatParapheurWithCorbeille() {
        try {
            parapheurService.getSecretariatParapheur(corbeilleAArchiver1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode moveDossier().
     *
     * @see ParapheurServiceImpl#moveDossier(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testMoveDossier() {
        parapheurService.moveDossier(dossier1, parapheur2);

        Assert.assertEquals(corbeilleEnPreparation2, nodeService.getPrimaryParent(dossier1).getParentRef());
    }

    /**
     * Essaye de déplacer un dossier terminé.
     *
     * @see ParapheurServiceImpl#moveDossier(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testMoveDossierTermine() {
        nodeService.setProperty(dossier2, ParapheurModel.PROP_TERMINE,
                true);

        try {
            parapheurService.moveDossier(dossier2, parapheur1);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye de récupérer un parapheur depuis son nom.
     *
     * @see ParapheurServiceImpl#parapheurFromName(java.lang.String)
     */
    public void testParapheurFromName() {
        NodeRef returned = parapheurService.parapheurFromName("parapheur1");
        Assert.assertEquals(parapheur1, returned);
    }

    /**
     * Essaye de récupérer un parapheur depuis un nom qui n'existe pas.
     *
     * @see ParapheurServiceImpl#parapheurFromName(java.lang.String)
     */
    public void testParapheurFromNameDoesntExists() {
        NodeRef returned = parapheurService.parapheurFromName("parapheurDoesntExists");
        Assert.assertNull(returned);
    }

    /**
     * Essaye de récupérer un parapheur sans spécifier son nom.
     *
     * @see ParapheurServiceImpl#parapheurFromName(java.lang.String)
     */
    public void testParapheurFromNameNull() {
        try {
            parapheurService.parapheurFromName(null);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Essaye de récupérer un parapheur en spécifiant un nom vide.
     *
     * @see ParapheurServiceImpl#parapheurFromName(java.lang.String)
     */
    public void testParapheurFromNameEmpty() {
        try {
            parapheurService.parapheurFromName("");
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Teste la méthode setSignature().
     *
     * @see ParapheurServiceImpl#setSignature(org.alfresco.service.cmr.repository.NodeRef, byte[])
     */
    public void testSetSignature() {

        parapheurService.setSignature(dossier1, "-----BEGIN\n12457896\n-----END".getBytes(), null);

        Assert.assertEquals("LS0tLS1CRUdJTgoxMjQ1Nzg5NgotLS0tLUVORA==", nodeService.getProperty(
                dossier1, ParapheurModel.PROP_SIGNATURE_ELECTRONIQUE));
    }

    /**
     * Teste la méthode getSignature().
     *
     * @see ParapheurServiceImpl#getSignature(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testGetSignature() {
        nodeService.setProperty(dossier1,
                ParapheurModel.PROP_SIGNATURE_ELECTRONIQUE, "c2lnbmF0dXJl");

        byte[] returned = parapheurService.getSignature(dossier1);
        Assert.assertEquals("signature", new String(returned));
    }

    /**
     * Teste la méthode getSignAppletURL().
     *
     * @see ParapheurServiceImpl#getSignAppletURL()
     */
    public void testGetSignAppletURL() {
        String returned = parapheurService.getSignAppletURL();

        Assert.assertEquals(parapheurService.getConfiguration().getProperty(
                "signature.applet.url"), returned);
    }

    /**
     * Teste la méthode getSavedXMLTypes().
     *
     * @see ParapheurServiceImpl#getSavedXMLTypes()
     */
    public void testGetSavedXMLTypes() {
        Assert.assertNotNull(parapheurService.getSavedXMLTypes());
    }

    /**
     * Test la méthode getSavedTypes().
     * qui renvoit les types de métiers stockés dans /samples/TypesMetier.xml
     */
    public void testGetSavedTypes() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        List<String> types = parapheurService.getSavedTypes(parapheur1);
        assertEquals(2, types.size());
        assertTrue(types.contains("metier"));
        assertTrue(types.contains("defaut"));
    }

    /**
     * Teste la méthode getSavedXMLSousTypes().
     *
     * @see ParapheurServiceImpl#getSavedXMLSousTypes(java.lang.String)
     */
    public void testGetSavedXMLSousTypes() {
        Assert.assertNotNull(parapheurService.getSavedXMLSousTypes("metier"));
    }

    /**
     * Test la méthode getSavedSousTypes()
     * qui renvoit la liste des sous types pour le metier "metier"
     * stockée dans /samples/SousTypesMetier.xml (alias de metier.xml)
     */
    public void testGetSavedSousTypes() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        List<String> types = parapheurService.getSavedSousTypes("metier", parapheur1);

        assertEquals(2, types.size());
        assertTrue(types.contains("defaut1"));
        assertTrue(types.contains("defaut2"));
    }

    /**
     * DEPRECATED !!!
     * Teste la méthode getCircuitRef().
     *
     * @see ParapheurServiceImpl#getCircuitRef(java.lang.String, java.lang.String)
     */
    public void testGetCircuitRef() {
        NodeRef returned = parapheurService.getCircuitRef("metier", "defaut1");
        Assert.assertNotNull(returned);
    }

    /**
     * Essaye de récupérer un utilisateur via son adresse mail.
     *
     * @see ParapheurServiceImpl#findUserByEmail(java.lang.String)
     */
    public void testUserByEmail() {
        NodeRef returned = parapheurService.findUserByEmail("proprietaire1@example.com");
        Assert.assertEquals(personService.getPerson(tenantService.getDomainUser("proprietaire1", tenantName)), returned);
    }

    /**
     * Essaye de récupérer un utilisateur via une adresse mail inexistante.
     *
     * @see ParapheurServiceImpl#findUserByEmail(java.lang.String)
     */
    public void testUserByEmailDoesntExists() {
        try {
            parapheurService.findUserByEmail("nobody@example.com");
            Assert.fail();
        } catch (NoSuchPersonException e) {
        }
    }


    /**
     * Recherche un dossier en fonction de son nom.
     *
     * @see ParapheurServiceImpl#rechercheDossier(java.lang.String)
     */
    public void testRechercheDossier() {
        NodeRef returned = parapheurService.rechercheDossier("dossier1");
        Assert.assertEquals(dossier1, returned);
    }

    /**
     * Recherche un dossier dont le nom n'existe pas.
     *
     * @see ParapheurServiceImpl#rechercheDossier(java.lang.String)
     */
    public void testRechercheDossierDoesntExists() {
        NodeRef returned = parapheurService.rechercheDossier("dossierDoesntExists");
        Assert.assertNull(returned);
    }

    /**
     * Teste la méthode isNomDossierAlreadyExists().
     *
     * @see ParapheurServiceImpl#isNomDossierAlreadyExists(java.lang.String)
     */
    public void testIsNomDossierAlreadyExists() {
        boolean returned = parapheurService.isNomDossierAlreadyExists("dossier1");
        Assert.assertTrue(returned);
    }

    /**
     * Teste la méthode isNomDossierAlreadyExists() avec un nom de dossier non
     * existant.
     *
     * @see ParapheurServiceImpl#isNomDossierAlreadyExists(java.lang.String)
     */
    public void testIsNomDossierAlreadyExistsDosentExists() {
        boolean returned = parapheurService.isNomDossierAlreadyExists("dossierDoesntExists");
        Assert.assertFalse(returned);
    }

    /**
     * Ce test vérifie que l'emetteur d'un dossier peut le rejeter plus tard dans
     * le circuit.
     *
     * Voir bug forge #4352.
     *
     * @see ParapheurServiceImpl#reject(org.alfresco.service.cmr.repository.NodeRef)
     */
    public void testRejectByEmetteur() {
        // Supprime la délégation en place
        nodeService.removeAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_DELEGATION);

        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "test-reject-by-emetteur");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        NodeRef dossier = dossierService.createDossier(parapheur1, properties);
        NodeRef doc = nodeService.createNode(dossier,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}test-reject-by-emetteur-doc"),
                ContentModel.TYPE_CONTENT).getChildRef();
        ContentWriter docWriter = contentService.getWriter(doc, ContentModel.PROP_CONTENT, true);
        docWriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
        docWriter.putContent(this.getClass().getResourceAsStream("/samples/document.pdf"));
        parapheurService.setCircuit(dossier, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "VISA", Collections.<NodeRef>emptySet()),
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "SIGNATURE", Collections.<NodeRef>emptySet())));

        // Emission du dossier
        parapheurService.approve(dossier);

        // Tentative de rejet
        parapheurService.reject(dossier);

        assertEquals(corbeilleRetournes1, parapheurService.getParentCorbeille(dossier));
        assertEquals(parapheur1, parapheurService.getParentParapheur(dossier));
    }

    /**
     * Essaye de créer un dossier sans etape d'archivage. La dernière etape
     * devient automatiquement une étape d'archivage.
     */
    public void testSetCircuitSansEtapeArchivage() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        // On crée un dossier
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "test-set-circuit-archivage");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        NodeRef dossier = dossierService.createDossier(parapheur1, properties);

        // On lui ajoute un circuit SANS étape d'ARCHIVAGE
        parapheurService.setCircuit(dossier, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "VISA", Collections.<NodeRef>emptySet()),
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur2, "SIGNATURE", Collections.<NodeRef>emptySet())));

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);
        assertEquals(3, circuit.size());
        // Notre circuit initial est toujours présent :
        assertEquals(parapheur1, circuit.get(0).getParapheur());
        assertEquals("VISA", circuit.get(0).getActionDemandee());
        assertEquals(parapheur2, circuit.get(1).getParapheur());
        assertEquals("SIGNATURE", circuit.get(1).getActionDemandee());
        // On vérifie que l'étape ARCHIVAGE a été ajoutée
        assertEquals(parapheur1, circuit.get(2).getParapheur());
        assertEquals("ARCHIVAGE", circuit.get(2).getActionDemandee());
    }

    /**
     * Essaye de créer un dossier sans etape initiale. Une etape doit être crée
     * afin que l'emetteur puisse emettre le dossier.
     */
    public void testSetCircuitSansEtapeInitiale() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        // On crée un dossier
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "test-set-circuit-initial");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        NodeRef dossier = dossierService.createDossier(parapheur1, properties);

        // On lui ajoute un circuit SANS étape INITIALE (VISA) :
        parapheurService.setCircuit(dossier, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur2, "SIGNATURE", Collections.<NodeRef>emptySet()),
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "ARCHIVAGE", Collections.<NodeRef>emptySet())));

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);
        assertEquals(3, circuit.size());
        // On vérifie l'ajout de l'étape initiale au circuit
        assertEquals(parapheur1, circuit.get(0).getParapheur());
        assertEquals("VISA", circuit.get(0).getActionDemandee());
        assertEquals(parapheur2, circuit.get(1).getParapheur());
        assertEquals("SIGNATURE", circuit.get(1).getActionDemandee());
        assertEquals(parapheur1, circuit.get(2).getParapheur());
        assertEquals("ARCHIVAGE", circuit.get(2).getActionDemandee());
    }

    /**
     * Essaye de récupérer un mail d'un utilisateur.
     *
     * @see ParapheurServiceImpl#findEmailForUsername(java.lang.String)
     * @author nCHATELAIN
     */
    public void testEmailForUsername() {
        NodeRef prop1 = personService.getPerson(tenantService.getDomainUser("proprietaire1", tenantName));
        String prop1FullUsername = tenantService.getDomainUser("proprietaire1", tenantName);
        nodeService.setProperty(prop1, ContentModel.PROP_EMAIL, "proprietaire1@example.com");
        String returned = parapheurService.findEmailForUsername(prop1FullUsername);
        Assert.assertEquals(nodeService.getProperty(prop1, ContentModel.PROP_EMAIL) , returned);
    }


     /**
     * Essaye de récupérer un mail d'un utilisateur inexistant.
     *
     * @see ParapheurServiceImpl#findEmailForUsername(java.lang.String)
     * @author nCHATELAIN
     */
    public void testEmailForUsernameDoesntExists() {

        String returned = parapheurService.findEmailForUsername("UtilisateurInexistant");
        Assert.assertEquals(null , returned);
    }


      /**
     * Vérifie que "isPreview" renvoit "true" lorsque le "preview" est définit à "true".
     *
     * @see ParapheurServiceImpl#isPreviewEnabled()
     * @author nCHATELAIN
     */
    public void testIsPreviewEnableTrue(){
        parapheurService.getConfiguration().setProperty("parapheur.preview.enabled", "true");
        boolean returned = parapheurService.isPreviewEnabled();
        Assert.assertEquals(true, returned);
    }

      /**
     * Vérifie que le "isPreview" renvoit "false" lorsque le preview est définit à "false".
     *
     * @see ParapheurServiceImpl#isPreviewEnabled()
     * @author nCHATELAIN
     */
    public void testIsPreviewEnableFalse(){
        parapheurService.getConfiguration().setProperty("parapheur.preview.enabled", "false");
        boolean returned = parapheurService.isPreviewEnabled();
        Assert.assertEquals(false, returned);
    }


    /**
     * Teste getCurrentEtapeCircuit(List) avec un circuit null
     *
     * @see ParapheurServiceImpl#getCurrentEtapeCircuit(List<EtapeCircuit>)
     * @author nCHATELAIN
     */

     public void testGetCurrentEtapeCircuitNull(){
        EtapeCircuit returned = parapheurService.getCurrentEtapeCircuit((List<EtapeCircuit>)null);
        Assert.assertEquals(null, returned);
     }


     /**
     * Teste getParapheursFromName(String) avec un parapheur existant
     *
     * @see ParapheurServiceImpl#getParapheursFromName(String)
     * @nCHATELAIN
     */
      public void testGetParapheursFromName(){
         List<NodeRef> returned = parapheurService.getParapheursFromName("parapheur1");
         List<NodeRef> expected = new ArrayList<NodeRef>();
         expected.add(parapheur1);
         Assert.assertEquals(expected, returned);
     }

     /**
     * Teste getParapheursFromName(String) avec un parapheur inexistant
     *
     * @see ParapheurServiceImpl#getParapheursFromName(String)
     * @nCHATELAIN
     */
      public void testGetParapheursFromNameDoesntExists(){
         List<NodeRef> returned = parapheurService.getParapheursFromName("parapheur-inexistant");
         Assert.assertNull(returned);
     }


     /**
     * Teste getParapheursHome(NodeRef)
     *
     * @see ParapheurServiceImpl#getParapheursHome(NodeRef)
     * @nCHATELAIN
     */
     public void testGetParapheursHome(){
         NodeRef returned = parapheurService.getParapheursHome();
         Assert.assertEquals(parapheursHome, returned);
     }


     /**
     * Teste getChildParapheurs(NodeRef)
     *
     * @see ParapheurServiceImpl#getChildParapheurs(NodeRef)
     * @nCHATELAIN
     */
     public void testGetChildParapheurs(){
         List<NodeRef> returned = parapheurService.getChildParapheurs(parapheur1);
         List<NodeRef> expected = new ArrayList<NodeRef>();
         expected.add(parapheur2);
         Assert.assertEquals(expected, returned);
     }


     /**
     * Teste getCurrentValidator(NodeRef)
     *
     * @see ParapheurServiceImpl#getCurrentValidator(NodeRef)
     * @nCHATELAIN
     */
     public void testGetCurrentValidator(){
         EtapeCircuitImpl etapeCircuit = (EtapeCircuitImpl)parapheurService.getCurrentEtapeCircuit(dossier1);
         nodeService.setProperty(etapeCircuit.getNodeRef(), ParapheurModel.PROP_VALIDATOR, "taiste");
         String returned = parapheurService.getCurrentValidator(dossier1);
         Assert.assertEquals("taiste", returned);
     }

     /**
     * Teste setCurrentValidator(NodeRef, String)
     *
     * @see ParapheurServiceImpl#setCurrentValidator(NodeRef, String)
     * @nCHATELAIN
     */
     public void testSetCurrentValidator(){
         parapheurService.setCurrentValidator(dossier1, "taiste");
         EtapeCircuitImpl etapeCircuit = (EtapeCircuitImpl)parapheurService.getCurrentEtapeCircuit(dossier1);
         String returned = etapeCircuit.getValidator();
         Assert.assertEquals("taiste", returned);
     }


     public void testGetDateLimite(){
        Date date = new Date();
        nodeService.setProperty(dossier1, ParapheurModel.PROP_DATE_LIMITE, date );
        Date returned = parapheurService.getDateLimite(dossier1);
        Assert.assertEquals(date, returned);
     }


     public void testGetXPathSignature(){
         nodeService.setProperty(dossier1, ParapheurModel.PROP_XPATH_SIGNATURE, "taiste");
         String returned = parapheurService.getXPathSignature(dossier1);
         Assert.assertEquals("taiste",returned);
     }

      public void testGetXPathSignatureDossierVide(){

         NodeRef dossierVide;
         dossierVide = nodeService.createNode(
                corbeilleEnPreparation1, ContentModel.ASSOC_CONTAINS,
                QName.createQName("dossierVide"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
         String returned = parapheurService.getXPathSignature(dossierVide);
         Assert.assertNull(returned);
     }

      public void testGetXPathSignatureDossierNull(){
         String returned = parapheurService.getXPathSignature(null);
         Assert.assertNull(returned);
      }

      public void testIsRead(){
          boolean returned = parapheurService.isRead(dossier1);
          Assert.assertTrue(returned);
      }

      public void testIsDigitalSignatureMandatory(){
          nodeService.setProperty(dossier1, ParapheurModel.PROP_DIGITAL_SIGNATURE_MANDATORY, true);
          boolean returned = parapheurService.isDigitalSignatureMandatory(dossier1);
          Assert.assertTrue(returned);
      }

      public void testIsDigitalSignatureMandatoryNull(){
          boolean returned = parapheurService.isDigitalSignatureMandatory(dossier1);
          Assert.assertFalse(returned);
      }



     public void testIsAdministrateurFonctionnelNull(){
         List<String> paraphs = new ArrayList<String>();
         String prop1 = tenantService.getDomainUser("proprietaire1", tenantName);
         parapheurService.setPHAdminAuthoritiesForUser(prop1, paraphs);
         boolean returned = parapheurService.isAdministrateurFonctionnel(prop1);
         Assert.assertEquals(false, returned);
     }



     public void testIsAdministrateurFonctionnel(){
         List<String> paraphs = new ArrayList<String>();
         paraphs.add(parapheur1.getId());
         String prop1 = tenantService.getDomainUser("proprietaire1", tenantName);
         parapheurService.setPHAdminAuthoritiesForUser(prop1, paraphs);
         boolean returned = parapheurService.isAdministrateurFonctionnel(prop1);
         Assert.assertTrue(returned);
     }


     public void testUpdateProperties(){
        Map<QName, Serializable>  properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_PASSE_PAR, parapheur2);
        properties.put(ParapheurModel.PROP_EFFECTUEE, false);
        properties.put(ParapheurModel.PROP_ANNOTATION, "annotationpublique");
        properties.put(ParapheurModel.PROP_ANNOTATION_PRIVEE, "annotationprivee");

        parapheurService.updateProperties(dossier1, properties);
        Map<QName, Serializable> returned  = nodeService.getProperties(dossier1);
        Assert.assertTrue(returned.containsValue(parapheur2));
        Assert.assertTrue(returned.containsValue(false));
        Assert.assertTrue(returned.containsValue("annotationpublique"));
        Assert.assertTrue(returned.containsValue("annotationprivee"));
     }


     public void testRestartCircuit(){
         parapheurService.restartCircuit(dossier1, "test_type", "test_soustype", "test_workflow");
         String type = nodeService.getProperty(dossier1, ParapheurModel.PROP_TYPE_METIER).toString();
         String subtype = nodeService.getProperty(dossier1, ParapheurModel.PROP_SOUSTYPE_METIER).toString();
         String workflow = nodeService.getProperty(dossier1, ParapheurModel.PROP_WORKFLOW).toString();
         String termine = nodeService.getProperty(dossier1, ParapheurModel.PROP_TERMINE).toString();
         Assert.assertEquals(type, "test_type");
         Assert.assertEquals(subtype, "test_soustype");
         Assert.assertEquals(workflow, "test_workflow");
         Assert.assertEquals(termine, "false");
     }


     public void testGetFirstEtape(){
         EtapeCircuit returned = parapheurService.getFirstEtape(dossier1);
         EtapeCircuit expected = new EtapeCircuitImpl(nodeService, circuitEtape1);
         Assert.assertEquals(expected,returned);
    }

     public void testGetFirstEtapeNull(){
         NodeRef dossier_null = nodeService.createNode(corbeilleEnPreparation1, ContentModel.ASSOC_CONTAINS,
            QName.createQName("{testgetfirstetape}dossier1"), ParapheurModel.TYPE_DOSSIER).getChildRef();

         EtapeCircuit etapeReturned = parapheurService.getFirstEtape(dossier_null);
         Assert.assertNull(etapeReturned);
     }




     public void testAppendCircuit(){
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "test-set-append-circuit");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        NodeRef dossier = dossierService.createDossier(parapheur1, properties);

        parapheurService.setCircuit(dossier, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "VISA", Collections.<NodeRef>emptySet()),
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur2, "SIGNATURE", Collections.<NodeRef>emptySet())));


        List<EtapeCircuit> circuitAdded = Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur2, "VISA", Collections.<NodeRef>emptySet()));

        parapheurService.appendCircuit(dossier, circuitAdded);

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier);

        assertEquals(5, circuit.size());
        assertEquals(parapheur1, circuit.get(0).getParapheur());
        assertEquals("VISA", circuit.get(0).getActionDemandee());
        assertEquals(parapheur2, circuit.get(1).getParapheur());
        assertEquals("SIGNATURE", circuit.get(1).getActionDemandee());
        assertEquals(parapheur1, circuit.get(2).getParapheur());
        assertEquals("VISA", circuit.get(2).getActionDemandee());
        assertEquals(parapheur2, circuit.get(3).getParapheur());
        assertEquals("VISA", circuit.get(3).getActionDemandee());
        assertEquals(parapheur1, circuit.get(4).getParapheur());
        assertEquals("ARCHIVAGE", circuit.get(4).getActionDemandee());
     }


     public void testGetDiffusionToutesEtapesUneSeuleEtape(){
       nodeService.setProperty(circuitEtape1, ParapheurModel.PROP_ACTION_DEMANDEE, null);
         Set<NodeRef> returned = parapheurService.getDiffusionToutesEtapes(dossier1);
         HashSet<NodeRef> expected = new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur1, parapheur2));
         Assert.assertEquals(expected, returned);
     }


     public void testGetDiffusionToutesEtapes(){

         parapheurService.setCircuit(dossier1, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur2, "SIGNATURE", new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur1))),
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "ARCHIVAGE", new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur2)))));
         Set<NodeRef> returned = parapheurService.getDiffusionToutesEtapes(dossier1);
         HashSet<NodeRef> expected = new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur1, parapheur2));
         Assert.assertEquals(expected, returned);
     }


    public void testChangeSignatureToSignaturePapier() {
        nodeService.setProperty(dossier1, ParapheurModel.PROP_SIGNATURE_PAPIER, null);
        parapheurService.changeSignatureToSignaturePapier(dossier1);
        String returned = nodeService.getProperty(dossier1, ParapheurModel.PROP_SIGNATURE_PAPIER).toString();
        Assert.assertEquals("true", returned);
    }
    //DEPRECATED !!!
    public void testGetActeurCourant() {
        String returned = parapheurService.getActeurCourant(dossier1);
        String expected = tenantService.getDomainUser("proprietaire1", tenantName);
        Assert.assertNotNull(returned);
        Assert.assertEquals(expected, returned);
    }

  public void testGetSavedTypesFull() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        List<String> types = parapheurService.getSavedTypesFull();
        assertEquals(2, types.size());
        assertTrue(types.contains("metier"));
        assertTrue(types.contains("defaut"));
    }


    public void testGetCircuitHierarchique(){
        List<EtapeCircuit> returned = parapheurService.getCircuitHierarchique(parapheur2);
        assertEquals(3, returned.size());
        assertEquals(parapheur2, returned.get(0).getParapheur());
        assertEquals("PARAPHEUR", returned.get(0).getTransition());
        assertEquals("VISA", returned.get(0).getActionDemandee());
        assertEquals(parapheur1, returned.get(1).getParapheur());
        assertEquals("PARAPHEUR", returned.get(1).getTransition());
        assertEquals("SIGNATURE", returned.get(1).getActionDemandee());
        assertEquals(parapheur2, returned.get(2).getParapheur());
        assertEquals("PARAPHEUR", returned.get(2).getTransition());
        assertEquals("ARCHIVAGE", returned.get(2).getActionDemandee());
    }

    public void testGetSavedTypesRun(){
        List<String> types = parapheurService.getSavedTypes(tenantService.getDomainUser("proprietaire1", tenantName), parapheur1);
        assertEquals(2, types.size());
        assertTrue(types.contains("metier"));
        assertTrue(types.contains("defaut"));
    }



    public void testGetSignatureEtape(){

        EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossier1);
        nodeService.setProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_SIGNATURE_ETAPE, "c2lnbmF0dXJl");

        byte[] returned = parapheurService.getSignature(etape);
        Assert.assertEquals("signature", new String(returned));

    }

    public void testSetSignatureEtape(){

        EtapeCircuit etape = parapheurService.getCurrentEtapeCircuit(dossier1);
        parapheurService.setSignature(dossier1, etape, "-----BEGIN\n12457896\n-----END".getBytes(), null);
        Assert.assertEquals("LS0tLS1CRUdJTgoxMjQ1Nzg5NgotLS0tLUVORA==", nodeService.getProperty(((EtapeCircuitImpl) etape).getNodeRef(), ParapheurModel.PROP_SIGNATURE_ETAPE));
    }

    public void testGetSavedSousTypesRun() {
        List<String> types = parapheurService.getSavedSousTypes("metier", tenantService.getDomainUser("proprietaire1", tenantName), parapheur1);

        assertEquals(2, types.size());
        assertTrue(types.contains("defaut1"));
        assertTrue(types.contains("defaut2"));
    }


    public void testGetUploadFileSizeLimit(){
        parapheurService.getConfiguration().setProperty(ParapheurModel.PARAPHEUR_UPLOAD_FILE_SIZE_LIMIT_KEY, "1337");
        String returned = parapheurService.getUploadFileSizeLimit();
        Assert.assertEquals("1337", returned);
    }

 /**   public void testGetUploadFileSizeLimitNull(){
        parapheurService.getConfiguration().setProperty(ParapheurModel.PARAPHEUR_UPLOAD_FILE_SIZE_LIMIT_DEFAUT, "1337");
        String returned = parapheurService.getUploadFileSizeLimit();
        //String expected = ParapheurModel.PARAPHEUR_UPLOAD_FILE_SIZE_LIMIT_DEFAUT;
        Assert.assertEquals("1337", returned);
    }
    **/

    public void testGetActeurCourantEtapeNull(){
        NodeRef dossierVide;
        dossierVide = nodeService.createNode(
                corbeilleEnPreparation1, ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testGetActeurCourantEtapeNull}dossierVide"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();

        Assert.assertNull(parapheurService.getActeursCourant(dossierVide));
    }

  public void testSetCircuitChefDeResponsableExists() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        parapheurService.setCircuit(dossier1, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("CHEF_DE", parapheur2, "SIGNATURE", Collections.<NodeRef>emptySet())));

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier1);

        assertEquals(3, circuit.size());
        assertEquals(parapheur1, circuit.get(0).getParapheur());
        assertEquals("VISA", circuit.get(0).getActionDemandee());
        assertEquals(parapheur1, circuit.get(1).getParapheur());
        assertEquals("SIGNATURE", circuit.get(1).getActionDemandee());
        assertEquals(parapheur1, circuit.get(2).getParapheur());
        assertEquals("ARCHIVAGE", circuit.get(2).getActionDemandee());
    }


  public void testSetCircuitChefDeResponsableDoesntExist() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(circuitEtape3, ParapheurModel.PROP_EFFECTUEE, false);
        nodeService.setProperty(circuitEtape4, ParapheurModel.PROP_EFFECTUEE, false);
        nodeService.setProperty(circuitEtape3, ParapheurModel.PROP_PASSE_PAR, parapheur2);

        parapheurService.setCircuit(dossier2, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("CHEF_DE", parapheur2, "SIGNATURE", Collections.<NodeRef>emptySet())));

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier2);

        assertEquals(3, circuit.size());
        assertEquals(parapheur2, circuit.get(0).getParapheur());
        assertEquals("VISA", circuit.get(0).getActionDemandee());
        assertEquals(parapheur1, circuit.get(1).getParapheur());
        assertEquals("SIGNATURE", circuit.get(1).getActionDemandee());
        assertEquals(parapheur2, circuit.get(2).getParapheur());
        assertEquals("ARCHIVAGE", circuit.get(2).getActionDemandee());
    }

  public void testSetCircuitEmetteur() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        parapheurService.setCircuit(dossier1, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("EMETTEUR", parapheur2, "SIGNATURE", Collections.<NodeRef>emptySet())));

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier1);

        assertEquals(3, circuit.size());
        assertEquals(parapheur1, circuit.get(0).getParapheur());
        assertEquals("VISA", circuit.get(0).getActionDemandee());
        assertEquals(parapheur1, circuit.get(1).getParapheur());
        assertEquals("SIGNATURE", circuit.get(1).getActionDemandee());
        assertEquals(parapheur1, circuit.get(2).getParapheur());
        assertEquals("ARCHIVAGE", circuit.get(2).getActionDemandee());
    }


    public void testAppendCircuitChefDeResponsableExists(){
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));


        List<EtapeCircuit> circuitAdded = Arrays.<EtapeCircuit>asList(
                  new ScriptEtapeCircuitImpl("CHEF_DE", parapheur2, "SIGNATURE", Collections.<NodeRef>emptySet()));;

        parapheurService.appendCircuit(dossier1, circuitAdded);

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier1);


        assertEquals(5, circuit.size());
        assertEquals(parapheur1, circuit.get(0).getParapheur());
        assertEquals("VISA", circuit.get(0).getActionDemandee());
        assertEquals(parapheur2, circuit.get(1).getParapheur());
        assertEquals("SIGNATURE", circuit.get(1).getActionDemandee());
        assertEquals(parapheur1, circuit.get(2).getParapheur());
        assertEquals("VISA", circuit.get(2).getActionDemandee());
        assertEquals(parapheur1, circuit.get(3).getParapheur());
        assertEquals("SIGNATURE", circuit.get(3).getActionDemandee());
        assertEquals(parapheur1, circuit.get(4).getParapheur());
        assertEquals("ARCHIVAGE", circuit.get(4).getActionDemandee());
     }

  public void testAppendCircuitChefDeResponsableDoesntExist() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(circuitEtape3, ParapheurModel.PROP_EFFECTUEE, false);
        nodeService.setProperty(circuitEtape4, ParapheurModel.PROP_EFFECTUEE, false);
        nodeService.setProperty(circuitEtape3, ParapheurModel.PROP_PASSE_PAR, parapheur2);


        List<EtapeCircuit> circuitAdded = Arrays.<EtapeCircuit>asList(
                  new ScriptEtapeCircuitImpl("CHEF_DE", parapheur2, "SIGNATURE", Collections.<NodeRef>emptySet()));;

        parapheurService.appendCircuit(dossier2, circuitAdded);

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier2);

        assertEquals(4, circuit.size());
        assertEquals(parapheur2, circuit.get(0).getParapheur());
        assertEquals("VISA", circuit.get(0).getActionDemandee());
        assertEquals(parapheur2, circuit.get(1).getParapheur());
        assertEquals("SIGNATURE", circuit.get(1).getActionDemandee());
        assertEquals(parapheur1, circuit.get(2).getParapheur());
        assertEquals("SIGNATURE", circuit.get(2).getActionDemandee());
        assertEquals(parapheur2, circuit.get(3).getParapheur());
        assertEquals("ARCHIVAGE", circuit.get(3).getActionDemandee());
    }

  public void testAppendCircuitEmetteur() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));

        List<EtapeCircuit> circuitAdded = Arrays.<EtapeCircuit>asList(
                  new ScriptEtapeCircuitImpl("EMETTEUR", parapheur1, "SIGNATURE", Collections.<NodeRef>emptySet()));;

        parapheurService.appendCircuit(dossier1, circuitAdded);

        List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier1);

        assertEquals(5, circuit.size());
        assertEquals(parapheur1, circuit.get(0).getParapheur());
        assertEquals("VISA", circuit.get(0).getActionDemandee());
        assertEquals(parapheur2, circuit.get(1).getParapheur());
        assertEquals("SIGNATURE", circuit.get(1).getActionDemandee());
        assertEquals(parapheur1, circuit.get(2).getParapheur());
        assertEquals("VISA", circuit.get(2).getActionDemandee());
        assertEquals(parapheur1, circuit.get(3).getParapheur());
        assertEquals("SIGNATURE", circuit.get(3).getActionDemandee());
        assertEquals(parapheur1, circuit.get(4).getParapheur());
        assertEquals("ARCHIVAGE", circuit.get(4).getActionDemandee());
    }

  public void testGetEmetteurDossierVide(){

      NodeRef dossierVide = nodeService.createNode(
                corbeilleEnPreparation1, ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetemetteur}dossierVide"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();

      Assert.assertNull(parapheurService.getEmetteur(dossierVide));
  }

  public void testIsEmisDossierVide(){
      NodeRef dossierVide = nodeService.createNode(
                corbeilleEnPreparation1, ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetemetteur}dossierVide"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();

      Assert.assertFalse(parapheurService.isEmis(dossierVide));
  }
  public void testGetDelegationParapheurNull(){
      Assert.assertNull(parapheurService.getDelegation((NodeRef) null ));
  }

     public void testGetRootManagedParapheursByOpAdmin(){
         List<String> paraphs = new ArrayList<String>();
         paraphs.add(parapheur1.getId());
         paraphs.add(parapheur2.getId());
         String prop1 = tenantService.getDomainUser("proprietaire1", tenantName);
         parapheurService.setPHAdminAuthoritiesForUser(prop1, paraphs);
         List<NodeRef> returned = parapheurService.getRootManagedParapheursByOpAdmin(prop1);
         Assert.assertEquals(2,returned.size());
     }

  /**
  *     parapheur1 était le parent de parapheur2, getAllManagementParapheursByOpAdmin renvoit 2
  *     DEPRECATED !!!
  **/
     public void testGetAllManagedParapheursByOpAdmin(){
         List<String> paraphs = new ArrayList<String>();
         paraphs.add(parapheur1.getId());
         String prop1 = tenantService.getDomainUser("proprietaire1", tenantName);
         parapheurService.setPHAdminAuthoritiesForUser(prop1, paraphs);
         List<NodeRef> returned = parapheurService.getAllManagedParapheursByOpAdmin(prop1);
         Assert.assertEquals(2,returned.size());
         Assert.assertEquals(returned.get(0).getId(), parapheur1.getId());
         Assert.assertEquals(returned.get(1).getId(), parapheur2.getId());
     }

/**      DEPRECATED !!!
 *       public void testRemoveFromDiffusionToutesEtapes(){

         Set<NodeRef> liste = new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur1, parapheur2));

         parapheurService.setCircuit(dossier1, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur2, "SIGNATURE",liste),
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "ARCHIVAGE", liste )));


         List<EtapeCircuit> circuit = parapheurService.getCircuit(dossier1);
         for (EtapeCircuit etape : circuit) {
             etape.setAndRecordListeDiffusion(nodeService, liste);
         }


         parapheurService.removeFromDiffusionToutesEtapes(dossier1, parapheur1);
         Set<NodeRef> returned = parapheurService.getDiffusionToutesEtapes(dossier1);
         HashSet<NodeRef> expected = new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur2));
         Assert.assertEquals(expected, returned);
     }**/

     public void testRemoveFromDisffusionToutesEtapesSansActionDemandee(){
        nodeService.setProperty(circuitEtape1, ParapheurModel.PROP_ACTION_DEMANDEE, null);
        parapheurService.setCircuit(dossier1, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur2, "SIGNATURE", new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur1))),
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "ARCHIVAGE", new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur2)))));

         parapheurService.removeFromDiffusionToutesEtapes(dossier1, parapheur1);
         Set<NodeRef> returned = parapheurService.getDiffusionToutesEtapes(dossier1);
         HashSet<NodeRef> expected = new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur2));
         Assert.assertEquals(expected, returned);
     }

     public void testGetAllWorkflows(){
         Map<String, NodeRef> returned = parapheurService.getAllWorkflows();
         Assert.assertEquals(3,returned.size());
         Assert.assertTrue(returned.containsKey("workflowProp1"));
         Assert.assertTrue(returned.containsKey("workflowProp2"));
     }



    public void testGetBatchLockedNodes(){
        nodeService.setProperty(dossier1, ParapheurModel.PROP_IN_BATCH_QUEUE, true);
        List <NodeRef> returned = parapheurService.getBatchLockedNodes();
        Assert.assertTrue(returned.contains(dossier1));
    }

/**    public void testGetHabillage(){
        parapheurService.getConfiguration().setProperty(ParapheurModel.PARAPHEUR_HABILLAGE_KEY, "test");
        String returned = parapheurService.getHabillage();
        Assert.assertEquals("test", returned);
    }**/

    public void testAuditTransmissionTDTActionTDT(){
        nodeService.setProperty(dossier1, ParapheurModel.PROP_TERMINE, false);
        nodeService.setProperty(circuitEtape1, ParapheurModel.PROP_EFFECTUEE, true);
        nodeService.setProperty(circuitEtape2, ParapheurModel.PROP_ACTION_DEMANDEE , EtapeCircuit.ETAPE_TDT);
        parapheurService.auditTransmissionTDT(dossier1, "testStatus", "testMessage");
        Assert.assertEquals("testStatus", nodeService.getProperty(dossier1, ParapheurModel.PROP_STATUS_METIER));
    }

    public void testAuditTransmissionTDTActionAutre(){
        nodeService.setProperty(dossier1, ParapheurModel.PROP_TERMINE, false);
        nodeService.setProperty(circuitEtape1, ParapheurModel.PROP_EFFECTUEE, true);
        parapheurService.auditTransmissionTDT(dossier1, "testStatus", "testMessage");
        Assert.assertNull(nodeService.getProperty(dossier1, ParapheurModel.PROP_STATUS_METIER));
    }

     public void testGetTypeMetierProperties(){
        Map<QName, Serializable> returned = parapheurService.getTypeMetierProperties("metier");
        Assert.assertTrue(returned.containsValue("nomTest"));
        Assert.assertTrue(returned.containsValue("protocoleTest"));
        Assert.assertTrue(returned.containsValue("confTest"));
        Assert.assertTrue(returned.containsValue("XAdES/enveloped"));
        Assert.assertTrue(returned.containsValue("CMS"));
     }

    public void testCheckSignatureNonPKCS (){
        nodeService.setProperty(dossier1, ParapheurModel.PROP_SIGNATURE_FORMAT, "XAdES/enveloped");
        EtapeCircuit etape = new EtapeCircuitImpl(nodeService, dossier1);
        Assert.assertFalse(parapheurService.checkSignature(etape));
    }

/**    public void testGetParapheurWsGetdossierPdfDocNameNull(){
        String returned = parapheurService.getParapheurWsGetdossierPdfDocName();
        String expected  = ParapheurModel.PARAPHEUR_WS_GETDOSSIER_PDF_VALEUR_DEFAUT;
        Assert.assertEquals(expected, returned);
    }**/

    public void testGetParapheurWsGetdossierPdfDocName(){
        parapheurService.getConfiguration().setProperty(ParapheurModel.PARAPHEUR_WS_GETDOSSIER_PDF_KEY, "test");
        String returned = parapheurService.getParapheurWsGetdossierPdfDocName();
        Assert.assertEquals("test", returned);
    }


    public void testIsParapheurWsGetdossierPdfEnabledFalse(){
        ((ParapheurServiceImpl)parapheurService).setParapheurWsGetdossierPdfEnabled(null);
        parapheurService.getConfiguration().setProperty("parapheur.ws.getdossier.pdf.enabled", "" );
        Assert.assertFalse(parapheurService.isParapheurWsGetdossierPdfEnabled());
    }

    public void testIsParapheurWsGetdossierPdfEnabledTrue(){
        ((ParapheurServiceImpl)parapheurService).setParapheurWsGetdossierPdfEnabled(null);
        parapheurService.getConfiguration().setProperty("parapheur.ws.getdossier.pdf.enabled", "true" );
        Assert.assertTrue(parapheurService.isParapheurWsGetdossierPdfEnabled());
    }


    public void testChangeSignataire() {

        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {
            @Override
            public Object doWork() throws Exception {
                nodeService.setProperty(dossier1, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_PRET_VISA);
                parapheurService.approve(dossier1);
                return null;
            }
        }, tenantService.getDomainUser("proprietaire1", tenantName));

        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {
            @Override
            public Object doWork() throws Exception {
                List<EtapeCircuit> circuitOriginal = parapheurService.getCircuit(dossier1);

                parapheurService.changeSignataire(dossier1, parapheur1, "annotpublique", "annotprivee");

                List<EtapeCircuit> circuitModifie = parapheurService.getCircuit(dossier1);

                EtapeCircuit etapeCourante = parapheurService.getCurrentEtapeCircuit(dossier1);
                EtapeCircuit etapePrecedente = parapheurService.getPreviousEtapeCircuit(dossier1);

                Assert.assertEquals(etapePrecedente.getActionDemandee(), EtapeCircuit.ETAPE_VISA);
                Assert.assertEquals(etapeCourante.getActionDemandee(), EtapeCircuit.ETAPE_SIGNATURE);

                Assert.assertEquals(circuitOriginal.size() + 1, circuitModifie.size());
                return null;
            }
        }, tenantService.getDomainUser("proprietaire2", tenantName));



    }

    public void testAvisComplementaire() {

        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {
            @Override
            public Object doWork() throws Exception {
                nodeService.setProperty(dossier1, ParapheurModel.PROP_STATUS_METIER, StatusMetier.STATUS_PRET_VISA);
                parapheurService.approve(dossier1);
                return null;
            }
        }, tenantService.getDomainUser("proprietaire1", tenantName));


        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {
            @Override
            public Object doWork() throws Exception {
                List<EtapeCircuit> circuitOriginal = parapheurService.getCircuit(dossier1);

                parapheurService.avisComplementaire(dossier1, parapheur1, "pub", "priv");

                List<EtapeCircuit> circuitModifie = parapheurService.getCircuit(dossier1);

                Assert.assertEquals(circuitOriginal.size() + 2, circuitModifie.size());
                Assert.assertEquals(parapheurService.getParentParapheur(dossier1), parapheur1);

                return null;
            }
        }, tenantService.getDomainUser("proprietaire2", tenantName));
    }

    /**   public void testCheckSignaturePKCS (){
     nodeService.setProperty(dossier1, ParapheurModel.PROP_SIGNATURE_FORMAT, "PKCS#7");
     EtapeCircuit etape = new EtapeCircuitImpl(nodeService, dossier1);
     ContentWriter contentWriter = contentService.getWriter(content1, ContentModel.PROP_CONTENT, true);
     contentWriter.putContent("LS0tLS1CRUdJTgoxMjQ1Nzg5NgotLS0tLUVORA==");
     byte[] signedBytes = "-----BEGIN\n12457896\n-----END".getBytes();
     parapheurService.setSignature(dossier1, etape, signedBytes);

     Assert.assertTrue(parapheurService.checkSignature(etape));
     }

     public void testCreateDossierPESRetour(){
     NodeRef returned = parapheurService.createDossierPesRetour("dossierTest", "{testgetcircuit}parapheur1", null, null);
     Assert.assertEquals(nodeService.getProperty(returned, ContentModel.PROP_NAME), "dossierTest");
     }**/

}
