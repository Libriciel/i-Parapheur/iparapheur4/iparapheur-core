package org.adullact.iparapheur.test.repo.content.cleanup;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import org.junit.Assert;

import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import com.atolcd.parapheur.repo.content.cleanup.ParapheurCleaner;
import javax.swing.JOptionPane;
import javax.transaction.UserTransaction;

public class ParapheurCleanerTest extends BaseParapheurTest {

    private NodeRef archivesHome;
    private NodeRef archive;
    private NodeRef archiveASupprimer;
    private ParapheurCleaner cleaner;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        archivesHome = nodeService.getRootNode(archiveStore);

        NodeRef archivesCompanyHome = nodeService.createNode(archivesHome,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("ph:home", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();

        properties.put(ContentModel.PROP_ARCHIVED_DATE, new Date());
        archive = nodeService.createNode(archivesCompanyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("ph:archive", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
        nodeService.addAspect(archive, ContentModel.ASPECT_ARCHIVED, properties);

        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_ARCHIVED_DATE, new Date(new Date().getTime() - (4L * 24 * 60 * 60 * 1000)));
        archiveASupprimer = nodeService.createNode(archivesCompanyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("ph:archiveASupprimer", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
        nodeService.addAspect(archiveASupprimer, ContentModel.ASPECT_ARCHIVED,
                properties);

        nodeService.createNode(archivesCompanyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("ph:useless", namespaceService),
                ContentModel.TYPE_FOLDER);

        cleaner = (ParapheurCleaner) applicationContext.getBean("parapheurCleaner");
        cleaner.setArchiveStoreRef(archiveStore);
        cleaner.setProtectedDays(3);
    }

    public void testExecute() throws Exception {
        Assert.assertTrue(nodeService.exists(archive));
        Assert.assertTrue(nodeService.exists(archiveASupprimer));

        transactionManager.commit(transactionStatus);

        cleaner.execute();

        UserTransaction tx = transactionService.getUserTransaction();
        tx.begin();

        Assert.assertTrue(nodeService.exists(archive));
        Assert.assertFalse(nodeService.exists(archiveASupprimer));

        tx.rollback();
    }
}
