/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.iparapheur.test.repo;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CustomMetadataDef;
import com.atolcd.parapheur.repo.CustomMetadataType;
import com.atolcd.parapheur.repo.MetadataService;
import com.atolcd.parapheur.repo.impl.MetadataServiceImpl;
import org.junit.Assert;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.dictionary.DictionaryDAO;
import org.alfresco.repo.dictionary.DictionaryDAOImpl;
import org.alfresco.repo.dictionary.RepositoryLocation;
import org.alfresco.service.cmr.admin.RepoAdminService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceServiceMemoryImpl;
import org.alfresco.service.namespace.QName;

import java.util.List;

/**
 *
 * @author niko
 */
public class MetadataServiceTest extends BaseParapheurTest {

    protected MetadataService metadataService;
    private NodeRef companyHome;
    private NodeRef dictionaryHome;
    private NodeRef modelsHome;
    private NodeRef parapheursHome;
    private NodeRef parapheur1;
    private NodeRef corbeilleEnPreparation1;
    private NodeRef dossier1;
    private QName qName = null;
    private QName qNameTest = null;
    private CustomMetadataDef def;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();
        NamespaceServiceMemoryImpl mynamespaceService = new NamespaceServiceMemoryImpl();
        mynamespaceService.registerNamespace(MetadataServiceImpl.METADATA_DEF_CUSTOM_PREFIX, MetadataServiceImpl.METADATA_DEF_CUSTOM_URI);

        for (String prefix : namespaceService.getPrefixes()) {
            mynamespaceService.registerNamespace(prefix, namespaceService.getNamespaceURI(prefix));
        }
        
        namespaceService = mynamespaceService;
        
        RepoAdminService repoAdminService = (RepoAdminService) applicationContext.getBean("repoAdminService");
        RepositoryLocation repositoryLocation = new RepositoryLocation(storeRef, "/app:company_home/app:dictionary/app:models", "xpath");
         RepositoryLocation repositoryMessagesLocation = new RepositoryLocation(storeRef, "/app:company_home/app:dictionary/app:messages", "xpath");
        ((org.alfresco.repo.admin.RepoAdminServiceImpl)repoAdminService).setRepositoryModelsLocation(repositoryLocation);
        ((org.alfresco.repo.admin.RepoAdminServiceImpl)repoAdminService).setRepositoryMessagesLocation(repositoryMessagesLocation);
        ((org.alfresco.repo.admin.RepoAdminServiceImpl)repoAdminService).setNodeService(nodeService);
        ((org.alfresco.repo.admin.RepoAdminServiceImpl)repoAdminService).setNamespaceService(namespaceService);
        ((org.alfresco.repo.admin.RepoAdminServiceImpl)repoAdminService).setSearchService(searchService);
        DictionaryDAO dictionaryDAO = (DictionaryDAO)applicationContext.getBean("dictionaryDAO");
        ((DictionaryDAOImpl)dictionaryDAO).setTenantService(tenantService);
        ((org.alfresco.repo.admin.RepoAdminServiceImpl)repoAdminService).setDictionaryDAO(dictionaryDAO);              
        
        
        metadataService = (MetadataService) applicationContext.getBean("metadataService");
        ((MetadataServiceImpl) metadataService).setStoreRef(storeRef.toString());
        ((MetadataServiceImpl) metadataService).setRepoAdminService(repoAdminService);
        ((MetadataServiceImpl) metadataService).setNamespaceService(namespaceService);
        ((MetadataServiceImpl) metadataService).setNodeService(nodeService);
        ((MetadataServiceImpl) metadataService).setSearchService(searchService);


        authenticationComponent.setCurrentUser(tenantService.getDomainUser("admin", tenantName));


        companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.company_home.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        dictionaryHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.dictionary.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        modelsHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("app:models",
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();        
        
        parapheur1 = nodeService.createNode(parapheursHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}parapheur1"),
                ParapheurModel.TYPE_PARAPHEUR).getChildRef();        

        corbeilleEnPreparation1 = nodeService.createNode(
                parapheur1, ContentModel.ASSOC_CONTAINS,
                ParapheurModel.NAME_EN_PREPARATION,
                ParapheurModel.TYPE_CORBEILLE).getChildRef();        
        
        
        dossier1 = nodeService.createNode(
                corbeilleEnPreparation1, ContentModel.ASSOC_CONTAINS,
                QName.createQName("{testgetcircuit}dossier1"),
                ParapheurModel.TYPE_DOSSIER).getChildRef();
        nodeService.setProperty(dossier1, ParapheurModel.PROP_TERMINE,
                false);
        nodeService.setProperty(dossier1, ContentModel.PROP_NAME,
                "dossier1");
        nodeService.setProperty(dossier1, ParapheurModel.PROP_SIGNATURE_FORMAT,
                "PKCS#7/single");
        nodeService.setProperty(dossier1,
                ParapheurModel.PROP_SIGNATURE_PAPIER, false);
        nodeService.setProperty(dossier1,
                ParapheurModel.PROP_RECUPERABLE, false);
        
        qNameTest = QName.createQName(MetadataServiceImpl.METADATA_DEF_CUSTOM_PREFIX, "taiste", namespaceService);
        
        nodeService.setProperty(dossier1, qNameTest, "taiste");
        
        /*
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "customMetadata.xml");
        properties.put(ContentModel.PROP_TITLE, "customMetadata.xml");

        NodeRef cuMetadataNode = nodeService.createNode(modelsHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:customMetadata", namespaceService),
                ContentModel.TYPE_CONTENT,
                properties).getChildRef();
        */
        qName = QName.createQName(MetadataServiceImpl.METADATA_DEF_CUSTOM_PREFIX, "coin", namespaceService);
        
        def = new CustomMetadataDef(qName, "Ma Métadonnée", CustomMetadataType.STRING, true);
        
        
        /*
        ContentWriter cuMetadataNodeWriter = contentService.getWriter(
                cuMetadataNode, ContentModel.PROP_CONTENT, true);
        cuMetadataNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        cuMetadataNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/customMetadata.xml"));
        */
        
        repoAdminService.deployModel(this.getClass().getResourceAsStream("/samples/customMetadata.xml"), "customMetadata");
        repoAdminService.activateModel("customMetadata");
        
        
        //List metadataDefs = new ArrayList();
        //metadataDefs.add(def);
        //metadataService.saveMetadataDefs(metadataDefs);


        NodeRef typesFolderNode = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metiertype", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        NodeRef typesNode = nodeService.createNode(typesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:TypesMetier.xml"),
                ContentModel.TYPE_CONTENT).getChildRef();

        ContentWriter typesNodeWriter = contentService.getWriter(typesNode,
                ContentModel.PROP_CONTENT, true);
        typesNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        typesNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/TypesMetier_TypesService.xml"));


        NodeRef sousTypesFolderNode = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metiersoustype", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        NodeRef sousTypesNode = nodeService.createNode(sousTypesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metier.xml", namespaceService),
                ContentModel.TYPE_CONTENT).getChildRef();

        nodeService.setProperty(sousTypesNode, ContentModel.PROP_NAME,
                "metier.xml");
        ContentWriter sousTypesNodeWriter = contentService.getWriter(
                sousTypesNode, ContentModel.PROP_CONTENT, true);
        sousTypesNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/SousTypesMetier_TypesService.xml"));

        NodeRef sousTypesDefautNode = nodeService.createNode(sousTypesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:defaut.xml", namespaceService),
                ContentModel.TYPE_CONTENT).getChildRef();

        nodeService.setProperty(sousTypesDefautNode, ContentModel.PROP_NAME,
                "defaut.xml");
        ContentWriter sousTypesNodeDefautWriter = contentService.getWriter(
                sousTypesDefautNode, ContentModel.PROP_CONTENT, true);
        sousTypesNodeDefautWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesNodeDefautWriter.putContent(this.getClass().getResourceAsStream("/samples/defaut_TypesService.xml"));

    }

    public void testGetSubtypesReferencingMetadata() {        
        List<String> returned = metadataService.getSubtypesReferencingMetadata(qName);
        // On a une référence à la metadata "coin" dans "/samples/defaut_TypesService.xml" 
        Assert.assertTrue(returned.contains("defaut"));
    }

    public void testGetMetadataDefs() {
        List<CustomMetadataDef> returned = metadataService.getMetadataDefs();
        // Le fichier "/samples/customMetadata.xml" contient 3 définitions de metadata
        Assert.assertEquals(3, returned.size());
    }
    
    public void testAddMetadataDef() {
        QName mine = QName.createQName(MetadataServiceImpl.METADATA_DEF_CUSTOM_PREFIX, "jjjjjj", namespaceService);
        CustomMetadataDef customMetadataDef = new CustomMetadataDef(mine, "bleargh", CustomMetadataType.STRING, true);
        metadataService.addMetadataDef(customMetadataDef);
        /* On ajoute une metadata
         * On vérifie que le nombre de metadatas repertoriées dans "/samples/customMetadata.xml" est bien incrémenté de 1
         */        
        Assert.assertEquals(4, metadataService.getMetadataDefs().size());
    }
    
    public void testAddMetadataDefExistant(){
        //On crée un Metadata avec un nom déjà existant dans le customMetadata.xml
        QName mine = QName.createQName(MetadataServiceImpl.METADATA_DEF_CUSTOM_PREFIX, "coin", namespaceService);
        CustomMetadataDef customMetadataDef = new CustomMetadataDef(mine, "coin", CustomMetadataType.STRING, true);
        metadataService.addMetadataDef(customMetadataDef);
        //addMetadataDef supprime l'entrée déjà existante pour coin, on doit donc récuperer seulement 3 metadataDef
        Assert.assertEquals(3, metadataService.getMetadataDefs().size());      
    }
    
    public void testDeleteMetadataDef() {
        QName mine = QName.createQName(MetadataServiceImpl.METADATA_DEF_CUSTOM_PREFIX, "plop", namespaceService);        
        CustomMetadataDef customMetadataDef = new CustomMetadataDef(mine, "bleargh", CustomMetadataType.STRING, true);
        metadataService.addMetadataDef(customMetadataDef);
        // On ajoute une metadata et on passe à un total de 4, que l'on vérifie :
        Assert.assertEquals(4, metadataService.getMetadataDefs().size());
        // Puis on supprime la nouvelle metadata avant de vérifier que le compte est repassé à 3
        metadataService.deleteMetadataDef(mine);
        Assert.assertEquals(3, metadataService.getMetadataDefs().size());        
    }
    /*
    public void testGetUsedValuesForDef(){
        CustomMetadataDef customMetadataDef = new CustomMetadataDef(qNameTest, "taiste", CustomMetadataType.STRING, true);
        metadataService.addMetadataDef(customMetadataDef);        
        List<String> returned = metadataService.getUsedValuesForDef(customMetadataDef);
        Assert.assertTrue(returned.contains("taiste"));
    }*/
}
