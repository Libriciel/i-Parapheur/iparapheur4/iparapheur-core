package org.adullact.iparapheur.test.web.action.evaluator;

import org.junit.Assert;

import org.adullact.iparapheur.test.WebUIBaseTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.web.bean.repository.Node;
import org.mockito.Mockito;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.web.action.evaluator.ArchiveEvaluator;
import java.util.Arrays;
import java.util.HashSet;
import org.adullact.iparapheur.repo.jscript.ScriptEtapeCircuitImpl;

public class ArchiveEvaluatorTest extends WebUIBaseTest {

    private ArchiveEvaluator evaluator;
    private NodeRef nodeRef;
    private NodeRef parapheurRef;
    private NodeRef corbeilleRef;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        nodeRef = new NodeRef("workspace://SpacesStore/nodeRef");
        parapheurRef = new NodeRef("workspace://SpacesStore/parapheurRef");
        corbeilleRef = new NodeRef("workspace://SpacesStore/corbeilleRef");

        parapheurService = Mockito.mock(ParapheurService.class);
        nodeService = Mockito.mock(NodeService.class);

        EtapeCircuit etape = new ScriptEtapeCircuitImpl("PARAPHEUR", parapheurRef, EtapeCircuit.ETAPE_ARCHIVAGE, new HashSet<NodeRef>());
        List<EtapeCircuit> circuit = Arrays.<EtapeCircuit>asList(etape);
        Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(circuit);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(nodeRef)).thenReturn(etape);
        Mockito.when(parapheurService.getCurrentEtapeCircuit(circuit)).thenReturn(etape);
        Mockito.when(parapheurService.getParentCorbeille(nodeRef)).thenReturn(corbeilleRef);
        Mockito.when(parapheurService.getEmetteur(nodeRef)).thenReturn(parapheurRef);
        //Mockito.when(parapheurService.getParapheurOwner(parapheurRef)).thenReturn(tenantService.getDomainUser("proprietaire", tenantName));
        Mockito.when(parapheurService.getParapheurOwners(parapheurRef)).thenReturn(Arrays.asList(tenantService.getDomainUser("proprietaire", tenantName)));
        Mockito.when(parapheurService.isParapheurOwner(parapheurRef, tenantService.getDomainUser("proprietaire", tenantName))).thenReturn(true);
        Mockito.when(parapheurService.getParentParapheur(corbeilleRef)).thenReturn(parapheurRef);
        Mockito.when(nodeService.getPrimaryParent(corbeilleRef)).thenReturn(new ChildAssociationRef(ContentModel.ASSOC_CONTAINS, parapheurRef, ParapheurModel.NAME_A_ARCHIVER, corbeilleRef));
        Mockito.when(parapheurService.getParentParapheur(nodeRef)).thenReturn(parapheurRef);
        Mockito.when(nodeService.exists(nodeRef)).thenReturn(true);
        Mockito.when(parapheurService.isDossier(nodeRef)).thenReturn(true);

        List<String> secretaires = new ArrayList<String>();
        secretaires.add(tenantService.getDomainUser("secretaire", tenantName));
        Mockito.when(parapheurService.getSecretariatParapheur(parapheurRef)).thenReturn(secretaires);

        setUpManagedBean("ParapheurService", parapheurService);
        setUpManagedBean("NodeService", nodeService);

        evaluator = new ArchiveEvaluator();
    }

    public void testEvaluateNull() {
        boolean returned = evaluator.evaluate(null);
        Assert.assertFalse(returned);
    }

    public void testEvaluateNonExistentNodeRef() {
        boolean returned = evaluator.evaluate(new Node(new NodeRef("workspace://SpacesStore/notInRepoRef")));
        Assert.assertFalse(returned);
    }

    public void testEvaluateExistingNodeNotDossier() {
        NodeRef myNodeRef = new NodeRef("workspace://SpacesStore/notInRepoRef");

        Mockito.when(nodeService.exists(myNodeRef)).thenReturn(true);

        boolean returned = evaluator.evaluate(new Node(myNodeRef));
        Assert.assertFalse(returned);
    }
    
    public void testBackwardEvaluateNonEmetteur() {
        EtapeCircuit premiereEtape = new ScriptEtapeCircuitImpl("PARAPHEUR", parapheurRef, null, new HashSet<NodeRef>());
        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(Arrays.asList(premiereEtape));
        authenticate("user");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testBackwardEvaluateNonTermine() {
        EtapeCircuit premiereEtape = new ScriptEtapeCircuitImpl("PARAPHEUR", parapheurRef, null, new HashSet<NodeRef>());
        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(Arrays.asList(premiereEtape));
        authenticate("proprietaire");
        Mockito.when(parapheurService.isTermine(nodeRef)).thenReturn(false);

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testBackwardEvaluateMauvaiseCorbeille() {
        EtapeCircuit premiereEtape = new ScriptEtapeCircuitImpl("PARAPHEUR", parapheurRef, null, new HashSet<NodeRef>());
        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(Arrays.asList(premiereEtape));
        authenticate("proprietaire");
        Mockito.when(nodeService.getPrimaryParent(corbeilleRef)).thenReturn(new ChildAssociationRef(ContentModel.ASSOC_CONTAINS, parapheurRef, ParapheurModel.NAME_A_TRAITER, corbeilleRef));

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertFalse(returned);
    }

    public void testBackwardEvaluate() {
        EtapeCircuit premiereEtape = new ScriptEtapeCircuitImpl("PARAPHEUR", parapheurRef, null, new HashSet<NodeRef>());
        Mockito.when(parapheurService.getCircuit(nodeRef)).thenReturn(Arrays.asList(premiereEtape));
        authenticate("proprietaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluate() {
        authenticate("proprietaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateSecretaire() {
        authenticate("secretaire");

        boolean returned = evaluator.evaluate(new Node(nodeRef));
        Assert.assertTrue(returned);
    }

    public void testEvaluateObject() {
        authenticate("proprietaire");
        Object monNode = new Node(nodeRef);

        boolean returned = evaluator.evaluate(monNode);
        Assert.assertTrue(returned);
    }
}
