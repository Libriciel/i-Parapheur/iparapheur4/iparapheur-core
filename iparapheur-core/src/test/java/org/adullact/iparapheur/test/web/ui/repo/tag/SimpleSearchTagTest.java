/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adullact.iparapheur.test.web.ui.repo.tag;

import com.atolcd.parapheur.web.ui.repo.tag.SimpleSearchTag;
import org.junit.Assert;
import junit.framework.TestCase;

/**
 *
 * @author stoulouse
 */
public class SimpleSearchTagTest extends TestCase {

    //@Ignore("Not Yet Ready")

    public void testgetComponentTypeTest() {
        SimpleSearchTag monTag = new SimpleSearchTag();
        String returned = monTag.getComponentType();
        Assert.assertEquals(returned, "org.atolcd.faces.SimpleSearch");
    }


}
