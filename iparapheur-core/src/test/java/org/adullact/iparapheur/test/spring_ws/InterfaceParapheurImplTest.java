/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.iparapheur.test.spring_ws;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.Principal;
import java.util.*;
import java.util.Map.Entry;
import javax.xml.ws.EndpointReference;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.adullact.iparapheur.repo.jscript.ScriptEtapeCircuitImpl;
import org.adullact.iparapheur.test.WebUIBaseTest;
import org.adullact.spring_ws.iparapheur._1.ArchivageAction;
import org.adullact.spring_ws.iparapheur._1.ArchiverDossierRequest;
import org.adullact.spring_ws.iparapheur._1.ExercerDroitRemordDossierResponse;
import org.adullact.spring_ws.iparapheur._1.GetCircuitRequest;
import org.adullact.spring_ws.iparapheur._1.GetCircuitResponse;
import org.adullact.spring_ws.iparapheur._1.GetDossierResponse;
import org.adullact.spring_ws.iparapheur._1.GetListeSousTypesResponse;
import org.adullact.spring_ws.iparapheur._1.GetListeTypesResponse;
import org.adullact.spring_ws.iparapheur._1.InterfaceParapheurImpl;
import org.adullact.spring_ws.iparapheur._1.RechercherDossiersRequest;
import org.adullact.spring_ws.iparapheur._1.RechercherDossiersResponse;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.w3c.dom.Element;

/**
 *
 * @author Vivien Barousse
 */
public class InterfaceParapheurImplTest extends WebUIBaseTest {

    private InterfaceParapheurImpl service;
    private NodeRef parapheursHome;
    private NodeRef companyHome;
    private NodeRef typesHome;
    private NodeRef typesFile;
    private NodeRef sousTypesHome;
    private NodeRef sousTypesFile;
    private NodeRef dictionaryHome;
    private NodeRef workflowsHome;
    private NodeRef userParapheur;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        authenticationService.createAuthentication(tenantService.getDomainUser("ws-user", tenantName), "secret".toCharArray());

        companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("app:company_home", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("ph:parapheurs", namespaceService),
                ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        dictionaryHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("app:dictionary", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        workflowsHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("ph:savedworkflows", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        typesHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metiertype", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        typesFile = nodeService.createNode(
                typesHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:TypesMetier.xml", namespaceService),
                ContentModel.TYPE_CONTENT).getChildRef();

        nodeService.setProperty(typesFile, ContentModel.PROP_NAME, "TypesMetier.xml");

        ContentWriter typesWriter = contentService.getWriter(typesFile, ContentModel.PROP_CONTENT, true);
        typesWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        typesWriter.putContent(this.getClass().getResourceAsStream("/samples/TypesMetier.xml"));

        sousTypesHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metiersoustype", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        sousTypesFile = nodeService.createNode(
                sousTypesHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:defaut.xml", namespaceService),
                ContentModel.TYPE_CONTENT).getChildRef();

        nodeService.setProperty(sousTypesFile, ContentModel.PROP_NAME, "defaut.xml");

        ContentWriter sousTypesWriter = contentService.getWriter(sousTypesFile, ContentModel.PROP_CONTENT, true);
        sousTypesWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesWriter.putContent(this.getClass().getResourceAsStream("/samples/defaut.xml"));

         NodeRef sousTypesMetierFile = nodeService.createNode(
                sousTypesHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metier.xml", namespaceService),
                ContentModel.TYPE_CONTENT).getChildRef();

        nodeService.setProperty(sousTypesMetierFile, ContentModel.PROP_NAME, "metier.xml");

        ContentWriter sousTypesMetierWriter = contentService.getWriter(sousTypesMetierFile, ContentModel.PROP_CONTENT, true);
        sousTypesMetierWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesMetierWriter.putContent(this.getClass().getResourceAsStream("/samples/SousTypesMetier.xml"));

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "ws-user");
        properties.put(ContentModel.PROP_TITLE, "ws-user");
        properties.put(ContentModel.PROP_DESCRIPTION, "ws-user");

        ArrayList<String> proprietaires = new ArrayList<String>();
        proprietaires.add(tenantService.getDomainUser("ws-user", tenantName));

        properties.put(ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, proprietaires);
        userParapheur = parapheurService.createParapheur(properties);

        service = new InterfaceParapheurImpl();
        service.webServiceContext = new MockWebServiceContext();

        parapheurService.saveWorkflow("workflowPublic",
                Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", userParapheur, "SIGNATURE", Collections.<NodeRef>emptySet())),
                Collections.<NodeRef>emptySet(),
                Collections.<String>emptySet(),
                true);
    }

    public void testArchiverDossier() {
        authenticate("ws-user");

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "ws-dossier");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        NodeRef dossier = dossierService.createDossier(userParapheur, properties);
        NodeRef doc = nodeService.createNode(dossier,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}ws-doc"),
                ContentModel.TYPE_CONTENT).getChildRef();
        ContentWriter docWriter = contentService.getWriter(doc, ContentModel.PROP_CONTENT, true);
        docWriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
        docWriter.putContent(this.getClass().getResourceAsStream("/samples/document.pdf"));
        parapheurService.setCircuit(dossier, Arrays.<EtapeCircuit>asList(new ScriptEtapeCircuitImpl("PARAPHEUR", userParapheur, "ARCHIVAGE", Collections.<NodeRef>emptySet())));
        parapheurService.approve(dossier);
        authenticationComponent.setSystemUserAsCurrentUser();

        authenticate("ws-user");
        ArchiverDossierRequest aRequest = new ArchiverDossierRequest();
        aRequest.setArchivageAction(ArchivageAction.ARCHIVER);
        aRequest.setDossierID("ws-dossier");
    }

    public void testEffacerDossierRejete() {
        authenticate("ws-user");

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "ws-effacer-dossier-rejete");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        NodeRef dossier = dossierService.createDossier(userParapheur, properties);
        NodeRef doc = nodeService.createNode(dossier,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}ws-effacer-dossier-rejete"),
                ContentModel.TYPE_CONTENT).getChildRef();
        ContentWriter docWriter = contentService.getWriter(doc, ContentModel.PROP_CONTENT, true);
        docWriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
        docWriter.putContent(this.getClass().getResourceAsStream("/samples/document.pdf"));
        parapheurService.setCircuit(dossier, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", userParapheur, "SIGNATURE", Collections.<NodeRef>emptySet())));

        authenticate("ws-user");
        parapheurService.approve(dossier);
        assertTrue(parapheurService.isEmis(dossier));
        assertEquals(userParapheur, parapheurService.getEmetteur(dossier));
        parapheurService.reject(dossier);
        authenticationComponent.setSystemUserAsCurrentUser();

        authenticate("ws-user");
        service.effacerDossierRejete("ws-effacer-dossier-rejete");

        authenticate("admin");
        assertFalse(nodeService.exists(dossier));
    }

    public void testExercerDroitRemordDossier() {
        authenticate("ws-user");

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "ws-dossier-remords");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        NodeRef dossier = dossierService.createDossier(userParapheur, properties);

        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "ws-dossier-remords");
        NodeRef doc = nodeService.createNode(dossier,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}ws-dossier-remords"),
                ContentModel.TYPE_CONTENT, properties).getChildRef();
        ContentWriter docWriter = contentService.getWriter(doc, ContentModel.PROP_CONTENT, true);
        docWriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
        docWriter.putContent(this.getClass().getResourceAsStream("/samples/document.pdf"));

        parapheurService.setCircuit(dossier, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", userParapheur, "SIGNATURE", Collections.<NodeRef>emptySet())));

        parapheurService.approve(dossier);

        ExercerDroitRemordDossierResponse res = service.exercerDroitRemordDossier("ws-dossier-remords");

        assertEquals(parapheurService.getCorbeille(userParapheur, ParapheurModel.NAME_EN_PREPARATION),
                parapheurService.getParentCorbeille(dossier));
    }

    public void testGetCircuit() {
        GetCircuitRequest req = new GetCircuitRequest();
        req.setTypeTechnique("defaut");
        req.setSousType("defaut1");

        GetCircuitResponse res = service.getCircuit(req);

        assertEquals(1, res.getEtapeCircuit().size());
        assertEquals("ws-user", res.getEtapeCircuit().get(0).getParapheur());
    }

    public void testGetDossier() {
        authenticate("ws-user");

        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "ws-get-dossier");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        properties.put(ParapheurModel.PROP_TERMINE, true);

        NodeRef dossier = dossierService.createDossier(userParapheur, properties);
        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "ws-get-dossier");


        NodeRef doc = nodeService.createNode(dossier,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}ws-get-dossier"),
                ContentModel.TYPE_CONTENT, properties).getChildRef();

        ContentWriter docWriter = contentService.getWriter(doc, ContentModel.PROP_CONTENT, true);
        docWriter.setMimetype(MimetypeMap.MIMETYPE_PDF);
        docWriter.putContent(this.getClass().getResourceAsStream("/samples/document.pdf"));

        parapheurService.setCircuit(dossier, Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", userParapheur, "ARCHIVAGE", Collections.<NodeRef>emptySet())));
        parapheurService.setAnnotationPublique(dossier, "annotation-publique");
        parapheurService.setAnnotationPrivee(dossier, "annotation-privee");
        parapheurService.approve(dossier);
        GetDossierResponse res = service.getDossier("ws-get-dossier");

        assertEquals("annotation-publique", res.getAnnotationPublique());
        assertEquals("annotation-privee", res.getAnnotationPrivee());
        assertEquals("ws-get-dossier", res.getDossierID());
        assertEquals("ws-get-dossier", res.getNomDocPrincipal());
        assertNull(res.getFichierPES());
    }

    public void testGetListeSousTypes() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("ws-user", tenantName));

        GetListeSousTypesResponse res = service.getListeSousTypes("metier");
        assertEquals(2, res.getSousType().size());
        assertTrue(res.getSousType().contains("defaut1"));
        assertTrue(res.getSousType().contains("defaut2"));
    }

    public void testGetListeTypes() {
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("ws-user", tenantName));
        GetListeTypesResponse res = service.getListeTypes(null);
        assertEquals(2, res.getTypeTechnique().size());
        assertTrue(res.getTypeTechnique().contains("defaut"));
        assertTrue(res.getTypeTechnique().contains("metier"));
    }

    public void testRechercherDossiersWithType() {
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "ws-recherche-type-1");
        properties.put(ParapheurModel.PROP_TYPE_METIER, "type");
        properties.put(ParapheurModel.PROP_SOUSTYPE_METIER, "soustype");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        dossierService.createDossier(userParapheur, properties);
    }

    public void testRechercherDossiersWithId() {
        Map<QName, Serializable> properties = new HashMap<QName, Serializable>();
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        properties.put(ContentModel.PROP_NAME, "ws-recherche-id-1");
        dossierService.createDossier(userParapheur, properties);
        properties = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "ws-recherche-id-2");
        properties.put(ParapheurModel.PROP_CONFIDENTIEL, false);
        properties.put(ParapheurModel.PROP_PUBLIC, true);
        dossierService.createDossier(userParapheur, properties);

        RechercherDossiersRequest req = new RechercherDossiersRequest();
        req.setNombreDossiers(new BigInteger("2"));
        req.getDossierID().add("ws-recherche-id-1");
        req.getDossierID().add("ws-recherche-id-2");
        RechercherDossiersResponse res = service.rechercherDossiers(req);

        assertEquals(2, res.getLogDossier().size());
        assertEquals("ws-recherche-id-1", res.getLogDossier().get(0).getNom());
        assertEquals("ws-recherche-id-2", res.getLogDossier().get(1).getNom());
    }

    private class MockWebServiceContext implements WebServiceContext {

        private class MockMessageContext implements MessageContext {

            public void setScope(String arg0, Scope arg1) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public Scope getScope(String arg0) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public int size() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public boolean isEmpty() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public boolean containsKey(Object arg0) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public boolean containsValue(Object arg0) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public Object get(Object key) {
                if (key.equals(MessageContext.SERVLET_CONTEXT)) {
                    return servletContext;
                }
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public Object put(String arg0, Object arg1) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public Object remove(Object arg0) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void putAll(Map<? extends String, ?> arg0) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void clear() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public Set<String> keySet() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public Collection<Object> values() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public Set<Entry<String, Object>> entrySet() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        }

        public MessageContext getMessageContext() {
            return new MockMessageContext();
        }

        public Principal getUserPrincipal() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isUserInRole(String arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public EndpointReference getEndpointReference(Element... arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public <T extends EndpointReference> T getEndpointReference(Class<T> arg0, Element... arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
