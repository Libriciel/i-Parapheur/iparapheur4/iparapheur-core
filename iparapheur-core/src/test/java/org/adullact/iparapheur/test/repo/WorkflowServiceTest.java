/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.iparapheur.test.repo;

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.EtapeCircuit;
import com.atolcd.parapheur.repo.SavedWorkflow;
import com.atolcd.parapheur.repo.TypesService;
import com.atolcd.parapheur.repo.WorkflowService;
import com.atolcd.parapheur.repo.impl.TypesServiceImpl;
import com.atolcd.parapheur.repo.impl.WorkflowServiceImpl;
import org.junit.Assert;
import org.adullact.iparapheur.repo.jscript.ScriptEtapeCircuitImpl;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.util.*;

/**
 *
 * @author niko
 */
public class WorkflowServiceTest extends BaseParapheurTest {
    
    private TypesService typesService ;  
    private WorkflowService workflowService;
    
    private NodeRef companyHome;
    private NodeRef dictionaryHome;
    private NodeRef workflowsHome;
    private NodeRef parapheursHome;    
    private NodeRef archivesHome;    
    private NodeRef parapheur1;
    private NodeRef parapheur2;
    private NodeRef publicWorkflow;
    private NodeRef workflowProprietaire1;
    private NodeRef savedWorkflowsNode;
    private NodeRef savedWorkflowsTestNode;
    
    
       @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        typesService = (TypesService) applicationContext.getBean("typesService");        
        ((TypesServiceImpl)typesService).setStoreRef(storeRef);
        
        workflowService = (WorkflowService) applicationContext.getBean("workflowService");        
        ((WorkflowServiceImpl)workflowService).setStore(storeRef.toString());
        
        authenticationService.createAuthentication(tenantService.getDomainUser("proprietaire1", tenantName), "secret".toCharArray());
        NodeRef prop1 = personService.getPerson(tenantService.getDomainUser("proprietaire1", tenantName));
        nodeService.setProperty(prop1, ContentModel.PROP_FIRSTNAME, "Prop");
        nodeService.setProperty(prop1, ContentModel.PROP_LASTNAME, "Ietaire");
        nodeService.setProperty(prop1, ContentModel.PROP_EMAIL, "proprietaire1@example.com");        
        
          
        authenticationComponent.setCurrentUser(tenantService.getDomainUser("admin", tenantName));
        

        companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.company_home.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();        
        
        dictionaryHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.dictionary.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();        

        parapheursHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.parapheurs.childname"),
                namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        archivesHome = nodeService.createNode(
                companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.archives.childname"),
                namespaceService), ParapheurModel.TYPE_PARAPHEURS).getChildRef();

        permissionService.setInheritParentPermissions(archivesHome, false);
        permissionService.setPermission(archivesHome, "GROUP_EVERYONE", "Contributor", true);   

        workflowsHome = nodeService.createNode(
                dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(propertiesManager.getProperties().getProperty("spaces.savedworkflows.childname"),
                namespaceService), ContentModel.TYPE_FOLDER).getChildRef();        
        
        parapheur1 = nodeService.createNode(parapheursHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("parapheur1"),
                ParapheurModel.TYPE_PARAPHEUR).getChildRef();

        ArrayList<String> ownersPh1 = new ArrayList<String>();
        ownersPh1.add(tenantService.getDomainUser("admin", tenantName));
        
        String ownerPh1 = tenantService.getDomainUser("admin", tenantName);
        
        nodeService.setProperty(parapheur1,
                ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPh1);
        nodeService.setProperty(parapheur1,
                ParapheurModel.PROP_PROPRIETAIRE_PARAPHEUR, ownerPh1);
        
        nodeService.setProperty(parapheur1, ContentModel.PROP_TITLE,
                "parapheur1");
        nodeService.setProperty(parapheur1, ContentModel.PROP_NAME,
                "parapheur1");
        parapheur2 = nodeService.createNode(parapheursHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("parapheur2"),
                ParapheurModel.TYPE_PARAPHEUR).getChildRef();

        ArrayList<String> ownersPh2 = new ArrayList<String>();
        ownersPh2.add(tenantService.getDomainUser("admin", tenantName));
        nodeService.setProperty(parapheur2, ContentModel.PROP_TITLE, "parapheur 2");
        
        nodeService.setProperty(parapheur2,
                ParapheurModel.PROP_PROPRIETAIRES_PARAPHEUR, ownersPh2);
        nodeService.setProperty(parapheur2,
                ParapheurModel.PROP_SECRETAIRES, (Serializable) Collections.singletonList(tenantService.getDomainUser("secretaire2", tenantName)));
        nodeService.createAssociation(parapheur2, parapheur1,
                ParapheurModel.ASSOC_HIERARCHIE);

        nodeService.createAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_DELEGATION);

        nodeService.createAssociation(parapheur1, parapheur2,
                ParapheurModel.ASSOC_OLD_DELEGATION);        
        

        
        NodeRef typesFolderNode = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metiertype", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
       
        NodeRef typesNode = nodeService.createNode(typesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:TypesMetier.xml"),
                ContentModel.TYPE_CONTENT).getChildRef();
        
        ContentWriter typesNodeWriter = contentService.getWriter(typesNode,
                ContentModel.PROP_CONTENT, true);
        typesNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        typesNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/TypesMetier_TypesService.xml"));

        
        
        
        NodeRef sousTypesFolderNode = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metiersoustype", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
        
        NodeRef sousTypesNode = nodeService.createNode(sousTypesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:metier.xml", namespaceService), 
                ContentModel.TYPE_CONTENT).getChildRef();
        
        nodeService.setProperty(sousTypesNode, ContentModel.PROP_NAME,
                "metier.xml");
        ContentWriter sousTypesNodeWriter = contentService.getWriter(
                sousTypesNode, ContentModel.PROP_CONTENT, true);
        sousTypesNodeWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesNodeWriter.putContent(this.getClass().getResourceAsStream("/samples/SousTypesMetier_TypesService.xml"));
        
        

        NodeRef sousTypesDefautNode = nodeService.createNode(sousTypesFolderNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:defaut.xml", namespaceService), 
                ContentModel.TYPE_CONTENT).getChildRef();
        
        nodeService.setProperty(sousTypesDefautNode, ContentModel.PROP_NAME,
                "defaut.xml");
        ContentWriter sousTypesNodeDefautWriter = contentService.getWriter(
                sousTypesDefautNode, ContentModel.PROP_CONTENT, true);
        sousTypesNodeDefautWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        sousTypesNodeDefautWriter.putContent(this.getClass().getResourceAsStream("/samples/defaut_TypesService.xml"));   
        
        
        

        publicWorkflow = nodeService.createNode(workflowsHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}publicWorkflow"),
                ParapheurModel.TYPE_SAVED_WORKFLOW).getChildRef();
        nodeService.setProperty(publicWorkflow, ContentModel.PROP_NAME,
                "workflowPublic");

        authenticationComponent.setCurrentUser(tenantService.getDomainUser("admin", tenantName));
        workflowProprietaire1 = nodeService.createNode(workflowsHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}proprietaire1workflow"),
                ParapheurModel.TYPE_SAVED_WORKFLOW).getChildRef();
        nodeService.setProperty(workflowProprietaire1, ContentModel.PROP_NAME,
                "workflowProp1");
        nodeService.setProperty(workflowProprietaire1, ContentModel.PROP_CREATOR,
                tenantService.getDomainUser("proprietaire1", tenantName));

        ContentWriter proprietaire1WorkflowWriter = contentService.getWriter(
                workflowProprietaire1, ContentModel.PROP_CONTENT, true);
        proprietaire1WorkflowWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        proprietaire1WorkflowWriter.putContent(
                "<circuit>" +                 
                "<acl>" + 
                "<parapheur>" + parapheur1.toString() + "</parapheur>" +
                "</acl>" +
                "<groupes>" + 
                "<groupe>" + "GROUP_EVERYONE" + "</groupe>" +
                "</groupes>" +
                "<etapes>" +
                "<validation>" + 
                "<etape>" + parapheur1.toString() + 
                "<transition>CHEF_DE</transition>" +
                "<action-demandee>VISA</action-demandee>" +
                "</etape>" + 
                "<etape>" + parapheur2.toString() + "</etape>" + 
                "</validation>" + 
                "<notification>" + 
                "<etape>" + parapheur2.toString() + "</etape>" + 
                "</notification>" + 
                "</etapes>" +
                "</circuit>");
        nodeService.addAspect(workflowProprietaire1, ParapheurModel.ASPECT_PRIVATE_WORKFLOW, null);
        nodeService.setProperty(workflowProprietaire1, ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS, (Serializable) Collections.singletonList(parapheur1.toString()));
        nodeService.setProperty(workflowProprietaire1, ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_GROUPS, (Serializable) Collections.emptyList());

        ContentWriter publicWorkflowWriter = contentService.getWriter(
                publicWorkflow, ContentModel.PROP_CONTENT, true);
        publicWorkflowWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        publicWorkflowWriter.putContent("<circuit>" + "<validation>" + "<etape>" + parapheur1.toString() + "</etape>" + "<etape>" + parapheur2.toString() + "</etape>" + "</validation>" + "<notification>" + "<etape>" + parapheur2.toString() + "</etape>" + "</notification>" + "</circuit>");
        nodeService.addAspect(publicWorkflow, ParapheurModel.ASPECT_PRIVATE_WORKFLOW, null);
        nodeService.setProperty(publicWorkflow, ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_PARAPHEURS, (Serializable) Collections.singletonList(parapheur1.toString()));
        nodeService.setProperty(publicWorkflow, ParapheurModel.PROP_PRIVATE_WORKFLOW_ACL_GROUPS, (Serializable) Collections.emptyList());
    
        
        
        
        savedWorkflowsNode = nodeService.createNode(dictionaryHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:savedworkflows", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();
        
        savedWorkflowsTestNode = nodeService.createNode(savedWorkflowsNode,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("cm:workflowTest", namespaceService), 
                ContentModel.TYPE_CONTENT).getChildRef();
        
        nodeService.setProperty(savedWorkflowsTestNode, ContentModel.PROP_NAME,
                "workflowTest");
        ContentWriter savedWorkflowsTestWriter = contentService.getWriter(
                savedWorkflowsTestNode, ContentModel.PROP_CONTENT, true);
        savedWorkflowsTestWriter.setMimetype(MimetypeMap.MIMETYPE_XML);
        savedWorkflowsTestWriter.putContent(this.getClass().getResourceAsStream("/samples/testSavedWorkflows_TypesService.xml"));           
       }
       
       
       public void testGetSavedWorkflow(){
           SavedWorkflow res =  workflowService.getSavedWorkflow(savedWorkflowsTestNode);
           Assert.assertEquals("workflowTest", res.getName());           
       }
       
       public void testGetReadOnlyWorkflows(){
           permissionService.setPermission(workflowProprietaire1, PermissionService.ALL_AUTHORITIES, PermissionService.CONSUMER, true);
           authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
           List<SavedWorkflow> savedWorkflows = workflowService.getReadOnlyWorkflows();           
           AccessStatus readReturned =  permissionService.hasPermission(savedWorkflows.get(0).getNodeRef(), PermissionService.READ);
           Assert.assertEquals(readReturned, AccessStatus.ALLOWED); 
           AccessStatus writeReturned =  permissionService.hasPermission(savedWorkflows.get(0).getNodeRef(), PermissionService.WRITE);
           Assert.assertEquals(writeReturned, AccessStatus.DENIED);            
       }
       
       public void testGetEditableWorkflows(){
           permissionService.setPermission(workflowProprietaire1, PermissionService.ALL_AUTHORITIES, PermissionService.COORDINATOR, true);
           authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
           List<SavedWorkflow> savedWorkflows = workflowService.getEditableWorkflows();           
           AccessStatus writeReturned =  permissionService.hasPermission(savedWorkflows.get(0).getNodeRef(), PermissionService.WRITE);
           Assert.assertEquals(writeReturned, AccessStatus.ALLOWED);   
           AccessStatus readReturned =  permissionService.hasPermission(savedWorkflows.get(0).getNodeRef(), PermissionService.READ);
           Assert.assertEquals(readReturned, AccessStatus.ALLOWED);              
       }       
             
       public void testGetWorkflows(){
           List<SavedWorkflow> workflows = workflowService.getWorkflows();
           Assert.assertEquals(2, workflows.size());
       }
       
       
       
       public void testGetWorkflowsRunAs(){
           List<SavedWorkflow> workflows = workflowService.getWorkflows(tenantService.getDomainUser("admin", tenantName));
           Assert.assertEquals(2, workflows.size());
       }
              
       
       public void testSaveWorkflowPublic(){
           List<EtapeCircuit> circuit = Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "VISA", Collections.<NodeRef>emptySet()),
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "SIGNATURE", Collections.<NodeRef>emptySet()));

           Set<NodeRef> acl = new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur1, parapheur2));           
           Set<String> groupes = new HashSet<String>((List<String>)Arrays.<String>asList("GROUP_EVERYONE"));
                   
           workflowService.saveWorkflow("workflowTestSave", circuit, acl, groupes, true);
           List<SavedWorkflow> workflows = workflowService.getWorkflows();
           Assert.assertEquals(3, workflows.size());           
           
           SavedWorkflow savedWorkflow = workflowService.getSavedWorkflow(workflowService.getWorkflowByName("workflowTestSave"));
           List<EtapeCircuit> circuitSaved = savedWorkflow.getCircuit();
           Assert.assertEquals(2, circuitSaved.size());
           
/**           Set<NodeRef> aclSaved = savedWorkflow.getAclParapheurs();
           Assert.assertEquals(2, aclSaved.size());       
           Assert.assertTrue(aclSaved.contains(parapheur1));       
           Assert.assertTrue(aclSaved.contains(parapheur2));       

           Set<String> groupesSaved = savedWorkflow.getAclGroupes();
            Assert.assertTrue(groupesSaved.contains("GROUP_EVERYONE"));       
**/          
       }
       
       

       /**
       public void testSaveWorkflowNonPublic(){
           authenticationComponent.setCurrentUser(tenantService.getDomainUser("proprietaire1", tenantName));
           
           List<EtapeCircuit> circuit = Arrays.<EtapeCircuit>asList(
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "VISA", Collections.<NodeRef>emptySet()),
                new ScriptEtapeCircuitImpl("PARAPHEUR", parapheur1, "SIGNATURE", Collections.<NodeRef>emptySet()));

           Set<NodeRef> acl = new HashSet<NodeRef>((List<NodeRef>)Arrays.asList(parapheur1, parapheur2));           
           Set<String> groupes = new HashSet<String>((List<String>)Arrays.<String>asList("GROUP_EVERYONE"));
                   
           workflowService.saveWorkflow("workflowTestSave", circuit, acl, groupes, false);
           List<SavedWorkflow> workflows = workflowService.getWorkflows();
           Assert.assertEquals(3, workflows.size());           
           
           SavedWorkflow savedWorkflow = workflowService.getSavedWorkflow(workflowService.getWorkflowByName("workflowTestSave"));
           List<EtapeCircuit> circuitSaved = savedWorkflow.getCircuit();
           Assert.assertEquals(2, circuitSaved.size());
           
           NodeRef savedWorkflowNode = workflowService.getWorkflowByName("workflowTestSave") ; 
           AccessStatus statusCoordinator = permissionService.hasPermission(savedWorkflowNode, PermissionService.COORDINATOR);
           Assert.assertEquals(AccessStatus.ALLOWED, statusCoordinator);
        
       } 
        
       public void testGetSavedWorkflowHierarchy(){
           SavedWorkflow res =  workflowService.getSavedWorkflow(savedWorkflowsTestNode, true, null);
           Assert.assertEquals("workflowTest", res.getName());        
           Assert.assertTrue(res.getAclGroupes().contains("GROUP_EVERYONE"));
           Assert.assertTrue(res.getAclParapheurs().contains(parapheur1));
          
       }       **/
    
}
