package org.adullact.iparapheur.test.repo.security.permissions.dynamic;

import org.junit.Assert;

import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.mockito.Mockito;

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.security.permissions.dynamic.SecretaireDynamicAuthority;

public class SecretaireDynamicAuthorityTest extends BaseParapheurTest {
	
	private NodeRef document;
	
	private NodeRef parapheur;
	
	private SecretaireDynamicAuthority authority;
	
	@Override
	protected void onSetUpInTransaction() throws Exception {
		super.onSetUpInTransaction();
		
		document = new NodeRef("workspace://SpacesStore/document");
		parapheur = new NodeRef("workspace://SpacesStore/parapheur");
		
		parapheurService = Mockito.mock(ParapheurService.class);
		Mockito.when(parapheurService.getParentParapheur(document)).thenReturn(parapheur);
		Mockito.when(parapheurService.isParapheurSecretaire(parapheur, "secretaire")).thenReturn(true);
		
		authority = new SecretaireDynamicAuthority();
		authority.setParapheurService(parapheurService);
		authority.afterPropertiesSet();
	}
	
	public void testHasAuthority() {
		boolean returned = authority.hasAuthority(document, "secretaire");
		Assert.assertTrue(returned);
	}
	
	public void testHasNotAuthority() {
		boolean returned = authority.hasAuthority(document, "nonSecretaire");
		Assert.assertFalse(returned);
	}

        public void testGetParapheurService() {
		ParapheurService returned = authority.getParapheurService();
		Assert.assertNotNull(returned);
	}

         public void testGetAuthority() {
		String returned = authority.getAuthority();
		Assert.assertEquals(returned, "ROLE_PARAPHEUR_SECRETAIRE");
	}

}