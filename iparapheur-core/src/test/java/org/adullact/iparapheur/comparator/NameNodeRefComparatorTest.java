/*
 * Version 1.1
 * CeCILL Copyright (c) 2006-2007, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developped by AtolCD
 *
 * contact@atolcd.com
 * contact@adullact-projet.coop
 *
 * Ce logiciel est un programme informatique servant à faire circuler des
 * documents au travers d'un circuit de validation, où chaque acteur vise
 * le dossier, jusqu'à l'étape finale de signature.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *
 */
package org.adullact.iparapheur.comparator;

import java.io.Serializable;
import java.util.Collections;
import org.adullact.iparapheur.test.BaseParapheurTest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

/**
 *
 * @author Vivien Barousse
 */
public class NameNodeRefComparatorTest extends BaseParapheurTest {

    private NodeRef companyHome;

    private NameNodeRefComparator comparator;

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.onSetUpInTransaction();

        companyHome = nodeService.createNode(
                rootNodeRef,
                ContentModel.ASSOC_CHILDREN,
                QName.createQName("app:company_home", namespaceService),
                ContentModel.TYPE_FOLDER).getChildRef();

        comparator = new NameNodeRefComparator(nodeService);
    }

    public void testCompare() {
        NodeRef node1 = nodeService.createNode(companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}node1"),
                ContentModel.TYPE_CONTENT,
                Collections.<QName, Serializable>singletonMap(ContentModel.PROP_NAME, "abc")).getChildRef();
        NodeRef node2 = nodeService.createNode(companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}node2"),
                ContentModel.TYPE_CONTENT,
                Collections.<QName, Serializable>singletonMap(ContentModel.PROP_NAME, "def")).getChildRef();

        assertTrue(comparator.compare(node1, node2) < 0);
        assertTrue(comparator.compare(node2, node1) > 0);
    }

    public void testCompareWithEquals() {
        NodeRef node = nodeService.createNode(companyHome,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName("{test}node1"),
                ContentModel.TYPE_CONTENT,
                Collections.<QName, Serializable>singletonMap(ContentModel.PROP_NAME, "abc")).getChildRef();

        assertTrue(comparator.compare(node, node) == 0);
    }
}
