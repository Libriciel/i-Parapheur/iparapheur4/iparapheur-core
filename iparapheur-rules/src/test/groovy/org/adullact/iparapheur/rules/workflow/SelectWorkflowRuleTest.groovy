package org.adullact.iparapheur.rules.workflow

import junit.framework.TestCase
import org.adullact.iparapheur.rules.RuleManager
import org.adullact.iparapheur.rules.bean.CustomProperty

class SelectWorkflowRuleTest extends TestCase {

    void testRun() {
        RuleManager rule = new SelectWorkflowRule()
        rule.setProperties(Collections.singletonList(new CustomProperty<Integer>("ammount", 35000)))
        rule.setScript("""
            selectionCircuit { 
              si(ammount < 40000) { 
                circuit "ok" 
              }
              circuit "nok" 
            }
            """)

        rule.run()

        assertNotNull(rule.getResults().get("workflow"))
        assertEquals("ok", rule.getResults().get("workflow"))
    }

    void testGString() {
        RuleManager rule = new SelectWorkflowRule()
        String circuitName = "ok"
        rule.setProperties(Collections.singletonList(new CustomProperty<GString>("circuit", "$circuitName")))
        rule.setScript("""
            selectionCircuit { 
              circuit "\$circuit"
            }
            """)

        rule.run()

        assertNotNull(rule.getResults().get("workflow"))
        assertEquals("ok", rule.getResults().get("workflow"))
    }

    void testFrenchConditions() {
        RuleManager rule = new SelectWorkflowRule()
        rule.setProperties(Collections.singletonList(new CustomProperty<Integer>("ammount", 35000)))
        rule.setScript("""
            selectionCircuit { 
              si(ammount < 35000) { 
                circuit "nok" 
              }; sinonsi(ammount > 35000) {
                circuit "nok"
              }; sinon {
                circuit "ok"
              }
            }
            """)
        rule.run()

        assertNotNull(rule.getResults().get("workflow"))
        assertEquals("ok", rule.getResults().get("workflow"))
    }

    void testIsBureauRule() {
        IsBureauRule rule = IsBureauRule.class.newInstance()
        rule.setProperties(Collections.singletonList(new CustomProperty<Integer>("ammount", 35000)))
        rule.setBureauString("ammount")
        String realBureauName = rule.run()

        assertEquals("35000", realBureauName)
    }

    void testRunWithParam() {
        RuleManager rule = new SelectWorkflowRule()
        String test = "11"

        rule.setProperties(Collections.singletonList(new CustomProperty<GString>("bureau", "bureau${test}")))
        rule.setScript("""
            selectionCircuit { 
              if(bureau == "bureau11") { 
                circuit "oké", ["bureau1"];
              } else {
                circuit "nok";
              }
            }
            """)

        rule.run()

        assertNotNull(rule.getResults().get("workflow"))
        assertEquals("oké", rule.getResults().get("workflow"))
        assertEquals("bureau1", ((ArrayList)rule.getResults().get("variables")).get(0))
    }

    void testRunInvalidScript() {
        RuleManager rule = new SelectWorkflowRule()
        rule.setProperties(Collections.singletonList(new CustomProperty<Integer>("ammount", 35000)))
        try {
            rule.setScript("foobar")
            rule.run()
            fail()
        } catch (GroovyRuntimeException ignored) {
        }
    }
}
