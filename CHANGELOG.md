Change Log
==========

Toutes les modifications apportées au projet seront documentées dans ce fichier.

Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/)
et adhère aux principes du [Semantic Versioning](http://semver.org/).

## [4.7.25] - 25-01-31

### Corrections

- Le champ "DossierID" de la fonction webservice "CreerDossier" n'est envoyé que si celui-ci n'est pas défini dans la requête 

## [4.7.24] - 25-01-17

### Corrections

- Correction de problème de compteurs suite à une action de mail sécurisé pastell
- Correction de problème de compteurs suite à une action de transfert de dossier depuis l'écran d'administration
- L'envoi aux dossiers à extraire via webservice laissait le dossier en fin de circuit
- La version de libersign n'était pas correctement affichée sur la page d'à propos
- Problème d'affichage du script de sélection de circuit dans l'écran d'administration des types
- Les noms d'utilisateurs de plus de 56 caractères posent problèmes sur alfresco et ont donc été limités
- Certains caractères n'étaient pas correctement échappés dans les URL de téléchargement des dossiers à extraire
- Problème d'accès au bouton de suppression des cachets serveur depuis la page d'administration
- Typo dans les notifications mails lors d'action de mail sécurisé pastell

## [4.7.23] - 24-11-29

### Corrections

- Les dossiers envoyés dans les dossiers à extraire n'étaient pas correctement supprimés

## [4.7.22] - 24-11-12

### Corrections

- Décalage du job alfresco "ftsIndexerTrigger" impliquant des ralentissements importants
- Correction de problème de compteurs négatifs suite aux actions d'enchainement de circuit
- Problèmes "ponctuels" de recherche de dossier suite à une création via webservice

### Amélioration

- Passage en version 3.1.1 de LiberSign

## [4.7.21] - 24-10-17

### Corrections

- Certains dossiers n'étaient plus correctement indéxés

## [4.7.20] - 24-08-19

### Corrections

- Une vérification au niveau des dossiers déclenchait des erreurs non attendues

## [4.7.19] - 24-08-06

### Corrections

- Les compteurs de dossiers n'étaient pas correctement mis à jour en mode "multi-tenant"
- L'affichage des métadonnées en administration pouvait poser problème lors d'un usage intensif

## [4.7.18] - 24-06-11

### Corrections

- Mise à jour du composant crypto en 3.0.14
- Mise à jour du composant pastell-connector en 1.4.0-LEGACY
- Mise à jour du composant pdf-stamp en 2.7.0
- Mise à jour du composant libriciel-pdf en 3.0.6
- Mise à jour du composant libersign en 3.1.2
- Les images de signature sont maintenant re-taillées correctement
- Libersign utilise maintenant une version 8 de OpenJDK.
- Un profil secrétaire ne pouvait plus positionner la signature graphique
- Les mises à jour des compteurs de corbeilles pouvaient se bloquer

### Suppressions

- Le format de signature PKCS#1 n'est plus supporté

## [4.7.17] - 23-11-27

### Corrections

- Certaines annotations publiques pouvaient bloquer le traitement des dossiers
- Mise à jour du composant pes-viewer en version 2.0.9
- Mise à jour du composant crypto en 3.0.11
- Mise à jour du composant pdf-stamp en 2.6.4
- Mise à jour du composant libriciel-pdf en 3.0.3
- Mise à jour du composant libersign en 3.0.3

### Suppressions

- Pour des raisons de performances, la recherche plein texte est maintenant désactivée par défaut

