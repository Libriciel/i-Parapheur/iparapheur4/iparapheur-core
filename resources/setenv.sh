#!/bin/sh

ulimit -Hn 16384 >/dev/null 2>&1
ulimit -Sn 16384

export CATALINA_OPTS="$CATALINA_OPTS -Xms$JVM_MEMORY"
export CATALINA_OPTS="$CATALINA_OPTS -Xmx$JVM_MEMORY"
export CATALINA_OPTS="$CATALINA_OPTS -Xss1024k"
export CATALINA_OPTS="$CATALINA_OPTS -XX:NewSize=256m"
export CATALINA_OPTS="$CATALINA_OPTS -Dfile.encoding=UTF-8"
export CATALINA_OPTS="$CATALINA_OPTS -Dalfresco.home=$CATALINA_HOME"

if [ "${MULTITENANT_ENABLED}" == "true" ]; then
  echo "Enabling multi-tenant"
  ln -s "$CATALINA_HOME/resources/mt" "$CATALINA_HOME/shared/classes/alfresco/extension/mt"
else
  unlink "$CATALINA_HOME/shared/classes/alfresco/extension/mt" >/dev/null 2>&1 | true
fi

export EXT_AUTH_ENABLED=false
if [ "${KEYCLOAK_AUTH_ENABLED}" == "true" ] || [ "${CAS_AUTH_ENABLED}" == "true" ]; then
  export EXT_AUTH_ENABLED=true
fi

if [ "${TRUSTSTORE_URLS}" != "" ]; then
  echo "build truststore"
  IFS=';' read -ra ADDR <<< ${TRUSTSTORE_URLS}
  for i in "${ADDR[@]}"; do
    echo "$i"
    # Ajoute la chaine de certification du serveur renseigné dans un truststore local
    openssl s_client -servername "${i}" -showcerts -connect "${i}" </dev/null 2>/dev/null > /tmp/chain_server.pem
    cat /tmp/chain_server.pem | sed -n -e '/BEGIN\ CERTIFICATE/,/END\ CERTIFICATE/ p' > /tmp/chain_server_good.pem
    keytool -import -trustcacerts -alias "${i}" -file /tmp/chain_server_good.pem -noprompt -keystore $JAVA_HOME/lib/security/cacerts -storepass changeit
    rm -f /tmp/chain_server*.pem
  done
fi

envsubst < "$CATALINA_HOME/shared/classes/alfresco-global.tpl" > "$CATALINA_HOME/shared/classes/alfresco-global.properties"

echo "Parapheur env done..."
