package org.adullact.iparapheur.repo.bean;
/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * contact@atolcd.com
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.TypesService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.alfresco.repo.security.authentication.AuthenticationUtil;

/**
 * GetTypologyScript
 *
 * @author manz
 *         Date: 16/05/12
 *         Time: 09:37
 */
public class GetTypologyScript extends DeclarativeWebScript {
    @Autowired
    private TypesService typesService;

    @Autowired
    private ParapheurService parapheurService;

    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
        // Extract user and password from JSON POST
        //TODO: play with Cache Object

        Content c = req.getContent();

        if (c == null) {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, "Missing POST body.");
        }
        JSONObject typology = new JSONObject();
        // extract ticket from JSON object
        JSONObject json;
        NodeRef bureauRef = null;
        List<NodeRef> bureaux = null;
        boolean getAll = false;

        try {
            json = new JSONObject(c.getContent());

            String bureauRefStr = null;
            if(json.has("bureauRef")) {
                bureauRefStr = json.getString("bureauRef");
            }
            if(json.has("getAll")) {
                getAll = true;
            }

            if(bureauRefStr == null) {
                bureaux = parapheurService.getOwnedParapheurs(AuthenticationUtil.getRunAsUser());
            } else {
                bureauRef = new NodeRef(bureauRefStr);
            }
        } catch (JSONException e) {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST,
                    "Unable to parse JSON POST body: " + e.getMessage());
        } catch (IOException e) {
            throw new WebScriptException(Status.STATUS_INTERNAL_SERVER_ERROR,
                    "Unable to retrieve POST body: " + e.getMessage());
        }

        try {
            List<String> types;
            if(getAll) {
                types = parapheurService.getSavedTypes();
            } else if(bureauRef == null) {
                Set<String> typeSet = new HashSet<String>();
                for (NodeRef bureau : bureaux) {
                    typeSet.addAll(parapheurService.getSavedTypes(bureau));
                }
                types = new ArrayList<String>();
                types.addAll(typeSet);
            } else {
                types = parapheurService.getSavedTypes(bureauRef);
            }

            for (String type : types) {
                List<String> sousTypes;
                if(getAll) {
                    sousTypes = parapheurService.getSavedSousTypes(type);
                } else if(bureauRef == null) {
                    Set<String> sousTypeSet = new HashSet<String>();
                    for (NodeRef bureau : bureaux) {
                        sousTypeSet.addAll(parapheurService.getSavedSousTypes(type, bureau));
                    }
                    sousTypes = new ArrayList<String>();
                    sousTypes.addAll(sousTypeSet);
                } else {
                    sousTypes = parapheurService.getSavedSousTypes(type, bureauRef);
                }
                typology.put(type, sousTypes);
            }


        } catch (JSONException e) {
            throw new WebScriptException(Status.STATUS_INTERNAL_SERVER_ERROR, "Unable to retrieve typology");
        }

        // construct model for ticket
        Map<String, Object> model = new HashMap<String, Object>(1, 1.0f);
        model.put("typology", typology.toString());

        if (status.getCode() != Status.STATUS_OK){
            status.setRedirect(true);
        }
        return model;
    }
}
