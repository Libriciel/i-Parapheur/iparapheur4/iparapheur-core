package org.adullact.iparapheur.repo.bean;
/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * contact@atolcd.com
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

import org.adullact.iparapheur.repo.Collectivite;
import org.adullact.iparapheur.repo.mc.MultiCService;
import org.alfresco.error.AlfrescoRuntimeException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * GetSpacesRootScript
 *
 * @author lhameury
 */
public class GetSpacesRootScript extends DeclarativeWebScript {
    @Autowired
    private MultiCService multiCService;

    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
        List<Collectivite> collectivites = Collections.emptyList();

        try {
            collectivites = multiCService.getCollectivites();
        } catch(AlfrescoRuntimeException exception) {
            status.setCode(401);
            status.setException(exception);
        }

        JSONArray nodes = new JSONArray();

        for(Collectivite col : collectivites) {
            if(col.isEnabled()) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("name", col.getTenantDomain());
                    obj.put("node", multiCService.getRootNodeRef(col).toString());
                    nodes.put(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        // construct model for ticket
        Map<String, Object> model = new HashMap<String, Object>(1, 1.0f);
        model.put("nodes", nodes.toString());

        if (status.getCode() != Status.STATUS_OK){
            status.setRedirect(true);
        }
        return model;
    }
}
