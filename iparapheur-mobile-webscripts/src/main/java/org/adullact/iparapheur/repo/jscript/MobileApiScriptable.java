package org.adullact.iparapheur.repo.jscript;
/*
 * Version 2.1
 * CeCILL Copyright (c) 2006-2008, AtolCD, ADULLACT-projet
 * Initiated by AtolCD S.A. & ADULLACT-projet S.A.
 * Developed by ADULLACT-projet S.A.
 * 
 * contact@adullact-projet.coop
 * contact@atolcd.com
 * 
 * Ce logiciel est un programme informatique servant à faire circuler des 
 * documents au travers d'un circuit de validation, où chaque acteur vise 
 * le dossier, jusqu'à l'étape finale de signature.
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 *  
 */

import com.atolcd.parapheur.model.ParapheurModel;
import com.atolcd.parapheur.repo.CorbeillesService;
import com.atolcd.parapheur.repo.DossierService;
import com.atolcd.parapheur.repo.JobService;
import com.atolcd.parapheur.repo.ParapheurService;
import com.atolcd.parapheur.repo.job.AbstractBatchJob;
import com.atolcd.parapheur.web.action.evaluator.SignEvaluator;
import com.atolcd.parapheur.web.bean.wizard.batch.CakeFilter;
import com.atolcd.parapheur.web.bean.wizard.batch.DossierFilter;
import com.atolcd.parapheur.web.bean.wizard.batch.Predicate;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.jscript.BaseScopableProcessorExtension;
import org.alfresco.repo.jscript.ScriptNode;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.LimitBy;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.NativeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.*;
import org.apache.lucene.queryParser.QueryParser;

/**
 * MobileApiScriptable
 *
 * @author Emmanuel Peralta
 *         Date: 04/05/12
 *         Time: 09:42
 */
public class MobileApiScriptable extends BaseScopableProcessorExtension {

    public static String PDF_EXTENSION = ".pdf";

    @Autowired
    private ParapheurService parapheurService;

    @Autowired
    private DossierService dossierService;

    @Autowired
    private CorbeillesService corbeillesService;
    
    @Autowired
    private JobService jobService;

    @Qualifier("nodeService")
    @Autowired
    private NodeService nodeService;

    @Qualifier("namespaceService")
    @Autowired
    private NamespaceService namespaceService;

    @Qualifier("searchService")
    @Autowired
    private SearchService searchService;

    @Autowired
    private ServiceRegistry serviceRegistry;
    /**
     * handles batch rejection
     *
     * @param _filesToReject
     * @param pub
     * @param priv
     */
    public void reject(JSONArray _filesToReject, String pub, String priv) throws Exception {
        final String fPriv = priv;
        final String fPub = pub;
        List<NodeRef> filesToReject = jsonArrayToNodeRefList(_filesToReject);

        jobService.postJobAndLockNodes(new AbstractBatchJob() {
            @Override
            protected Void doWorkUnitInTransaction(NodeRef dossier) {
                parapheurService.setSignataire(dossier,
                        parapheurService.getPrenomNomFromLogin(
                                AuthenticationUtil.getRunAsUser()));

                parapheurService.setAnnotationPrivee(dossier, fPriv);
                parapheurService.setAnnotationPublique(dossier, fPub);
                parapheurService.reject(dossier);
                if (jobService.isBackgroundWorkEnabled()) {
                    corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurService.getParentParapheur(dossier), ParapheurModel.NAME_A_TRAITER));
                }
                return null;
            }
        }, filesToReject);
        
    }

    /**
     * Handles batch visa
     *
     * @param _filesToVisa
     * @param pub
     * @param priv
     * @throws Exception
     */
    public void visa(JSONArray _filesToVisa, String pub, String priv) throws Exception {
        final String fPriv = priv;
        final String fPub = pub;
        SignEvaluator signEvaluator = new SignEvaluator();
        List<NodeRef> filesToVisa = jsonArrayToNodeRefList(_filesToVisa);
        /*
        for (NodeRef dossier : filesToVisa) {
            if (!signEvaluator.evaluate(new Node(dossier))) {
                throw new Exception(String.format("Le dossier %s n'est pas à viser", dossier.toString()));
            }
        }
        */
        jobService.postJobAndLockNodes(new AbstractBatchJob() {
            @Override
            protected Void doWorkUnitInTransaction(NodeRef dossier) {
                parapheurService.setSignataire(dossier,
                        parapheurService.getPrenomNomFromLogin(
                                AuthenticationUtil.getRunAsUser()));

                parapheurService.setAnnotationPrivee(dossier, fPriv);
                parapheurService.setAnnotationPublique(dossier, fPub);
                parapheurService.approveV4(dossier, parapheurService.getCurrentParapheur());
                if (jobService.isBackgroundWorkEnabled()) {
                    corbeillesService.updateCorbeilleChildCount(parapheurService.getCorbeille(parapheurService.getParentParapheur(dossier), ParapheurModel.NAME_A_TRAITER));
                }
                return null;
            }
        }, filesToVisa);
    }

    //FIXME: implement method.
    public void sign(JSONArray _filesToSign, String base64str, String publique, String privee) {

    }
    /**
     * Handles batch archive jobs.
     *
     * @param filesToArchive
     */
    public void archiver(List<NodeRef> filesToArchive) {
        AbstractBatchJob job = new AbstractBatchJob() {
            @Override
            protected Void doWorkUnitInTransaction(NodeRef dossier) {
                String baseName = (String) nodeService.getProperty(dossier, ContentModel.PROP_NAME);
                try {
                    parapheurService.archiver(dossier, _getUniqueArchiveName(baseName, 0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };

        // pas besoin de deverrouillage après archivage car le dossier n'existe plus.
        job.setShallUnlock(false);

        jobService.postJob(job);
    }

    /**
     * @param searchPath
     * @param count
     * @param index
     * @param _filters
     * @return
     */
    //TODO: add ParentNodeRef si corbeille -> PARENT si corbeilleVirtuelle -> VIRTUALPARENT
    public List<NodeRef> getDossiers(String searchPath, int count, int index, NativeObject _filters) {
        CakeFilter filter = new CakeFilter();

        filter.setType("ph:dossier");
        filter.setSearchPath(searchPath);

        //filter.setParent;

        List<Predicate<?, ?>> filters = null;

        if (_filters != null) {
            filters = nativeObjectPredicate(_filters);
        }

        filter.setClause(new Predicate<String, List<Predicate<?, ?>>>("and", filters));

        String query = filter.buildFilterQuery();
        /* SearchParameters Wizardry */
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        searchParameters.setQuery(query);

        /* permission check ? eager search */
        searchParameters.setMaxPermissionChecks(0);

        /* if count is not equal to 0 then we limit the result set and apply index */
        if (count > 0) {
            searchParameters.setLimitBy(LimitBy.FINAL_SIZE);
            searchParameters.setLimit(count + 1);
            searchParameters.setMaxItems(count + 1);
            searchParameters.setSkipCount(index);
        }

        ResultSet resultSet = null;
        List<NodeRef> refs = null;
        try {
            resultSet = searchService.query(searchParameters);
            refs = resultSet.getNodeRefs();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return refs;
    }
    
    public ScriptNode getPreviewRootForDossier(String dossierStr) {
        ScriptNode previewScriptNode = null;
        NodeRef dossier = new NodeRef(dossierStr);

        List<NodeRef> documents = parapheurService.getMainDocuments(dossier);

        if(documents != null && !documents.isEmpty()) {
            NodeRef documentPrincipal = documents.get(0);
            NodeRef previewRoot = dossierService.getDocumentImagesFolder(dossier, documentPrincipal, true);

            if (previewRoot != null) {
                previewScriptNode = new ScriptNode(previewRoot, serviceRegistry, getScope());
            }
        }
        return previewScriptNode;
    }


    /* private helper methods */

    /**
     * Converts a NativeObject in Map<String, String>
     * warning may crash if the nativeObject isn't a dictionary
     *
     * @param nativeObject
     * @return the hashmap containing key/values pair from nativeObject.
     */
    private Map<String, String> nativeObjectToStringMap(NativeObject nativeObject) {
        Object[] propIds = nativeObject.getIds();
        Map<String, String> retVal = new HashMap<String, String>();

        for (Object propId : propIds) {
            Object value = nativeObject.get(propId.toString(), nativeObject);
            retVal.put(propId.toString(), (String) value);

        }
        return retVal;

    }


    private Object getValueFromArray(NativeArray array, Object[] propIds, int i) {
        return array.get((Integer) propIds[i], null);
    }

    /**
     * @param nativeArray
     * @return
     */
    private List<Predicate<?, ?>> nativeArrayToPredicates(NativeArray nativeArray) {

        Object[] propIds = nativeArray.getIds();
        //List<NodeRef> retVal = new ArrayList<NodeRef>();
        List<Predicate<?, ?>> predicates = new ArrayList<Predicate<?, ?>>();

        for (Object propId : propIds) {
            int index = (Integer) propId;
            Object value = nativeArray.get(index, null);

            if (value instanceof NativeObject) {
                predicates.addAll(nativeObjectPredicate((NativeObject) value));
            }

        }

        return predicates;


    }

    /**
     * @param array
     * @return
     */
    private List<Predicate<?, ?>> jsonArrayToPredicates(JSONArray array) {
        List<Predicate<?, ?>> predicates = new ArrayList<Predicate<?, ?>>();

        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject obj = array.getJSONObject(i);
                predicates.addAll(jsonObjectToPredicates(obj));
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return predicates;
    }

    /**
     * @param object
     * @return
     */
    private List<Predicate<?, ?>> jsonObjectToPredicates(JSONObject object) {

        Iterator<String> itKeys = object.keys();
        List<Predicate<?, ?>> predicates = new ArrayList<Predicate<?, ?>>();

        while (itKeys.hasNext()) {
            String key = itKeys.next();
            Object value = null;
            try {
                value = object.get(key);
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            if (value instanceof JSONArray) {
                JSONArray array = (JSONArray) value;
                List<Predicate<?, ?>> predicatesList = jsonArrayToPredicates(array);
                if (predicatesList != null && predicatesList.size() > 0) {
                    Predicate<String, List<Predicate<?, ?>>> predicate = new Predicate<String, List<Predicate<?, ?>>>(key, predicatesList);
                    predicates.add(predicate);
                }
            } else {

                String[] qNameParts = key.split(":");

                QName filterQName = QName.createQName(qNameParts[0], qNameParts[1], namespaceService);

                Predicate<QName, String> predicate = new Predicate<QName, String>(filterQName, (String) value);
                predicates.add(predicate);
            }
        }

        return predicates;
    }

    /**
     * @param nativeObject
     * @return
     */
    private List<Predicate<?, ?>> nativeObjectPredicate(NativeObject nativeObject) {
        Object[] propIds = nativeObject.getIds();
        List<Predicate<?, ?>> predicates = new ArrayList<Predicate<?, ?>>();
        // normalement on a une seule paire ici sauf dans le cas "compatible"
        for (Object propId : propIds) {
            Object value = nativeObject.get(propId.toString(), nativeObject);

            if (value instanceof JSONArray) {
                JSONArray array = (JSONArray) value;
                List<Predicate<?, ?>> predicatesList = jsonArrayToPredicates(array);

                if (predicatesList != null && predicatesList.size() > 0) {
                    Predicate<String, List<Predicate<?, ?>>> predicate = new Predicate<String, List<Predicate<?, ?>>>(propId.toString(), predicatesList);
                    predicates.add(predicate);
                }

            } else if (value instanceof String) {
                String stringValue = propId.toString();
                String[] qNameParts = stringValue.split(":");

                QName filterQName = QName.createQName(qNameParts[0], qNameParts[1], namespaceService);

                Predicate predicate = new Predicate<QName, String>(filterQName, (String) value);
                predicates.add(predicate);

            } else {
                throw new RuntimeException("Malformed request values types should be either String or Array.");
            }

        }


        if (predicates.size() == 0) {
            predicates = null;
        }

        return predicates;
    }

    /**
     * @param nativeArray
     * @return
     */
    private List<NodeRef> nativeArrayToNodeRefList(NativeArray nativeArray) {
        Object[] propIds = nativeArray.getIds();
        List<NodeRef> retVal = new ArrayList<NodeRef>();

        for (Object propId : propIds) {
            int index = (Integer) propId;
            retVal.add(new NodeRef((String) nativeArray.get(index, null)));
        }

        return retVal;
    }

    /**
     * @param jsonArray
     * @return
     * @throws JSONException
     */
    private List<NodeRef> jsonArrayToNodeRefList(JSONArray jsonArray) throws JSONException {
        List<NodeRef> retVal = new ArrayList<NodeRef>();
        for (int i = 0; i < jsonArray.length(); i++) {
            retVal.add(new NodeRef((String) jsonArray.getString(i)));
        }
        return retVal;
    }

    /**
     * Private Method that takes care of unique archive name computing.
     *
     * @param baseName
     * @param it
     * @return an unique name in the ph:archives space.
     */
    //TODO: Move it somewhere we can access it from the batchworkflow too.
    //Generalize
    private String _getUniqueArchiveName(String baseName, int it) {
        String uniqueName;

        if (it == 0) {
            uniqueName = baseName + PDF_EXTENSION;
        } else {
            uniqueName = baseName + it + PDF_EXTENSION;
        }

        DossierFilter dossierFilter = new DossierFilter();
        dossierFilter.setSearchPath("/app:company_name/ph:archives/*");

        dossierFilter.addFilterElement(ContentModel.PROP_NAME.getPrefixedQName(namespaceService), baseName);

        ResultSet rs = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_LUCENE, dossierFilter.buildFilterQuery());

        List<NodeRef> list = rs.getNodeRefs();

        rs.close();

        if (list.size() == 0) {
            return uniqueName;
        } else {
            return _getUniqueArchiveName(baseName, it + 1);
        }
    }

}
