<#if daysUntilExpiration??>
Attention, un ou plusieurs certificats cachet serveur arrivent à epxiration dans ${daysUntilExpiration} jour(s) ou moins :
<#else>
Résumé des dates d'expirations des certificats cachet serveur :
</#if>

<#list seals as seal>

    - Certificat : ${seal.title}
    - Issuer : ${seal.description.issuerDN}
    - Date d'expiration : ${datemap[seal.title]?string["dd/MM/yyyy"]}

</#list>