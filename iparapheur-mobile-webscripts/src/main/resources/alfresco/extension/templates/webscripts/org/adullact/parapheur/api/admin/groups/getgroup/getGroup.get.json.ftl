{
    "id" : "${idGroup}",
    "groupName" : "${groupName}",
    "userId" : [
    <#assign first = true>
    <#list userId as id>
        <#if first == false>
            ,
        </#if>
        "${id}"
        <#assign first = false>
    </#list>
]
}