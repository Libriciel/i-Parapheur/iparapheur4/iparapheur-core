// Il faut être loggué "admin multi-collectivités"
// Résultat dans workspace://SpacesStore/company_home/<RESULT_DATA_FILE>

importClass(Packages.org.alfresco.repo.security.authentication.AuthenticationUtil);
importClass(Packages.java.lang.RuntimeException);

const RESULT_DATA_FILE = "sharejs_archives_delete.csv";

var runnable = { doWork : deleteArchives };
var logData = "";
var nbOkTot = 0;
var nbNokTot = 0;
var to = "";
var from = "";

function logDataAdd(logdate, userName, dossiersCount) {
  var log = userName + "\t" + dossiersCount;
  logDataLog(logdate, log);
}

function logDataAddHeader() {
  logDataAdd("logdate", "userName", "dossiersCount");
}

function logDataLog(logdate, message) {
  logData += logdate + "\t" + message + "\n";
}

function deleteArchives() {
  var userName = AuthenticationUtil.getRunAsUser();

  // les requêtes renvoient 1000 éléments maximum. On effectue donc la recherche plusieurs fois pour un même compte.
  do {
    var nbOkInLoop = 0;
    //MPAS : requête à étudier
    //var nodes = search.luceneSearch("+PATH:\"/app:company_home/ph:archives/*\"");
    var nodes = search.luceneSearch("+PATH:\"/app:company_home/ph:archives/*\" +TYPE:\"ph:archive\" AND @cm\\:created:[" + from + " TO " + to + "] ");

    for each(var node in nodes) {
      var docName = node.name;
      var docCreated = node.properties['cm:created'];//.toString()
      //var docContentUrl = node.properties['cm:content'].contentUrl;
      var deleteResult = false;
      var deleteExc = null;
      try {
        deleteResult = node.remove();
      } catch(ex) {
        deleteExc = ex;
      }
      logDataAdd(new Date(), userName, docName, deleteResult, deleteExc);
  
      if (deleteResult) {
        nbOkInLoop += 1;
        nbOkTot += 1;
      } else {
        nbNokTot += 1;
      }
    }
  } while (nbOkInLoop > 0); 
}

function runAs(runAsWork, user) {
  var originalFullAuthentication = AuthenticationUtil.getFullAuthentication();
  try {
    AuthenticationUtil.setRunAsUser(user);
    runAsWork.doWork();
  } catch(ex) {
    logDataLog(ex.toString());
  } finally {
    AuthenticationUtil.setFullAuthentication(originalFullAuthentication);
  }
}

function runTenant(tenant, runAsWork) {
  // Accès au scriptable "MultiCServiceScriptable".
  // Il faut être loggué "admin multi-collectivités"
  // Les index sont 0-based.
  nbOkTot = 0;
  nbNokTot = 0;
  
  logDataAddHeader();
  // Boucle sur les tenants   
  var tenantDomain = tenant;
  var tenantLog = "Tenant " + tenant + " : " + tenantDomain;
  var userAdmin = "admin@" + tenantDomain;
  logger.log(tenantLog + ", administrateur : " + userAdmin);
  runAs(runAsWork, userAdmin);
  model.status = "OK";
}

var workspace = companyhome;
var dataFile = workspace.childByNamePath(RESULT_DATA_FILE);
if (dataFile != null) {
  dataFile.remove();
} 


if(args.from != null && args.to != null && args.tenant != null){
    to = args.to;
    from = args.from;
    runTenant(args.tenant, runnable);


    dataFile = workspace.createFile(RESULT_DATA_FILE);
    dataFile.content = logData;


    model.inputstring = from + " - " + to;
    model.outputFile = dataFile.url;
    model.archives = nbOkTot;

}