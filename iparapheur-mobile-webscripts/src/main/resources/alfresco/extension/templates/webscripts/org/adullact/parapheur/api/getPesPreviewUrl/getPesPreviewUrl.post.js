<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var rules = {
    "documentRef": {"typeOf":"string", "isValue":is_valid_nodeRef()}
};


//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.mjson = dossierService.getPesPreviewUrl(jsonData.documentRef);
}