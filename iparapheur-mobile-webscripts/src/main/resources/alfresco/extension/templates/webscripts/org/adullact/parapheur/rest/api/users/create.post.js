<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE

//Rules
var rules = {
    "username" : {
        "typeOf":"string",
        "optional" : false
    },
    "firstName" : {
        "typeOf":"string",
        "optional" : false
    },
    "lastName" : {
        "typeOf":"string",
        "optional" : false
    },
    "email" : {
        "typeOf": "string",
        "optional" : false
    },
    "password" : {
        "typeOf": "string",
        "optional" : false
    }
};


//Récupération du contenu JSON de la requete
var jsonData = JSON.parse(requestbody.content);
//Validation de la requête
if(validate(jsonData, rules)) {
    try {
        var userId = usersService.createUser(jsonData.username, jsonData.email, jsonData.firstName, jsonData.lastName, jsonData.password);
        if(is_valid_uuid(userId)) {
            model.userId = userId;
        }
        else if (userId === "already exists") {
            setStatusMessageCode("L'utilisateur existe déjà : ", 409);
        }
        else {
            setStatusMessageCode("Une erreur est survenue lors de la création de l'utilisateur", 500);
        }
    } catch (e) {
        setStatusMessageCode("Erreur lors de la création de l'utilisateur : " + e, 500);
    }
}
