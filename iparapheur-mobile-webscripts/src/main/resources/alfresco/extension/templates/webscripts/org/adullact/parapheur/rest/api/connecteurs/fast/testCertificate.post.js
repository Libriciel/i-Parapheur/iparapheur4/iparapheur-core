<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "cert": {"typeOf":"string", "optional": true},
    "currentCertName": {"typeOf":"string", "optional": true},
    "password": {"typeOf":"string"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var ret = {};

        if(jsonData.cert !== undefined) {
            ret.isPwdGoodForPkcs = fastConfig.isPwdValidForFileCertificate(jsonData.cert, jsonData.password);
        } else {
            ret.isPwdGoodForPkcs = fastConfig.isPwdValidForCertificate(jsonData.currentCertName, jsonData.password).substring(0, 2);
        }

        model.mjson = jsonUtils.toJSONString(ret);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}