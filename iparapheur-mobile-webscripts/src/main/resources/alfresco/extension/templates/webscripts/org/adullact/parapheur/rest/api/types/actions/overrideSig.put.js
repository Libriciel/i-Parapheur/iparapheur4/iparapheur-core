<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(java.util.HashMap);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "active": {"typeOf": "string"},
    "pPolicyIdentifierID": {"typeOf": "string"},
    "pPolicyIdentifierDescription": {"typeOf": "string"},
    "pPolicyDigest": {"typeOf": "string"},
    "pSPURI": {"typeOf": "string"},
    "pCountryName": {"typeOf": "string"},
    "pClaimedRole": {"typeOf": "string"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Récupération de l'id du type
var type = url.templateArgs['id'];
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var parameters = new HashMap();

        parameters.active = "" + jsonData.active;

        parameters.pPolicyIdentifierID = "" + jsonData.pPolicyIdentifierID;
        parameters.pPolicyIdentifierDescription = "" + jsonData.pPolicyIdentifierDescription;
        parameters.pPolicyDigest = "" + jsonData.pPolicyDigest;
        parameters.pSPURI = "" + jsonData.pSPURI;
        parameters.pCountryName = "" + jsonData.pCountryName;
        parameters.pClaimedRole = "" + jsonData.pClaimedRole;

        s2lowConfig.setSigParametersForType(type, parameters);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
