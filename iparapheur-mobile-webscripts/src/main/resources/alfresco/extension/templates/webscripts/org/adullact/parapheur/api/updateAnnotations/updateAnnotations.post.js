<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.com.atolcd.parapheur.repo.annotations.Rect);
importClass(Packages.com.atolcd.parapheur.repo.annotations.Point);
importClass(Packages.com.atolcd.parapheur.repo.annotations.impl.AnnotationImpl);

var jsonData = JSON.parse(requestbody.content);

var rules = {
    "dossier":{"typeOf":"string", "isValid":is_valid_nodeRef()},
    "annotations":{
        "typeOf":"array",
        "childRules":{
            "uuid":{
                "typeOf":"string",
                "isValid":is_valid_uuid()
            },
            "text":{
                "typeOf":"string",
                "optional":"true"
            },
            "fillColor":{
                "typeOf":"string",
                "optional":"true"
            },
            "penColor":{
                "typeOf":"string",
                "optional":"true"
            },
            "rect":{
                "typeOf":"object",
                "optional":"true",
                "isValid":is_valid_rect()
            }
        }
    }
}


var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    for (var i = 0; i < jsonData.annotations.length; i++) {
        var toUpdate = jsonData.annotations[i];
        var annotation = annotationService.getAnnotationForUUID(jsonData.dossier, toUpdate.uuid);
        
        if (annotation === undefined || annotation === null) {
            setStatusMessage("Annotation inconnue");
            //return false;
        }
        else {

            if (toUpdate.text !== undefined) {
                annotation.setText(toUpdate.text);
            }

            if (toUpdate.rect !== undefined) {
                var r = toUpdate.rect;

                annotation.setRect(new Rect(new Point(r.topLeft.x, r.topLeft.y), new Point(r.bottomRight.x, r.bottomRight.y)));
            }

            if (toUpdate.fillColor !== undefined) {
                //annotation.setFillColor(fillColor);
            }

            if (toUpdate.penColor !== undefined) {
                //annotation.setPenColor(jsonData.annotation.penColor)
            }

            annotationService.updateAnnotation(jsonData.dossier, annotation);
        }
    }

}