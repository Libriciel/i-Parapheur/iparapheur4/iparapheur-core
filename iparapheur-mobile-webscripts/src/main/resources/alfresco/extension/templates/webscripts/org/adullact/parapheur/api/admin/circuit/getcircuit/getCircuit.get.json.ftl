{
    "id" : "${idCircuit}",
    "name" : "${circuit.name}",
   
    "ACLBureau" : 
    [
        <#assign first = true>
        <#list circuit.aclParapheurs as acl>
            <#if first == false>
                ,
            </#if>
            "${acl}"
            <#assign first = false>
        </#list>

    ],
    "ACLGroupes" : 
    [
        <#assign first = true>
        <#list circuit.aclGroupes as acl>
            <#if first == false>
                ,
            </#if>
            "${acl}"
            <#assign first = false>
        </#list>

    ],
     "public" : "${isPublic}",


     "etapes" :
     [
     <#assign first = true>
      <#list circuit.circuit as etape>
        <#if first == false>
                ,
        </#if>
        {
            "objet" : <#if etape.transition == "PARAPHEUR"> "${etape.parapheur.toString()}", <#else> "${etape.transition}",</#if>
            "action" : "${etape.actionDemandee}",
            "notification" : [ 
                                <#assign firstNotif = true>
                                <#list etape.listeNotification as notification>  
                                    <#if firstNotif == false>
                                        ,
                                    </#if>

                                    "${notification}"
                                    <#assign firstNotif = false>
                               </#list>
                            ]
        }
        <#assign first = false>
      </#list>
      ]
}
