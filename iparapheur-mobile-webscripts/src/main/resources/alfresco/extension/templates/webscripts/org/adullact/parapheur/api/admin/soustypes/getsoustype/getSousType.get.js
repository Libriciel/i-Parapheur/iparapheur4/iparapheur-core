var sstypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ args.typeName +".xml']")[0];
if (sstypesnode == null) {
    throw new Error("Les types techniques n'ont pas été initialisés.");
}

model.isNew = true;
model.type = args.typeName;


model.document = sstypesnode;
model.xml = sstypesnode.content;

var rootxml = new XML(sstypesnode.content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "")); // Workaround: Mozilla Bug 336551
model.rootxml = rootxml;



// Valeur par defaut du flag "digitalSignatureMandatory" (pour les nouveaux
// sous types, et lors de la lecture d'un sous type cree dans une version 
// precedente)
model.default_digitalSignatureMandatory=false;

// BL prefere que, par defaut, la signature electronique soit obligatoire
if ("blex".equals(iparapheur.getHabillage())) {
    model.default_digitalSignatureMandatory=true;
}

// Manu ListCustomMDefs 

model.metadatas = {}

function myForEach(list, callback) {
    for (var i=0; i < list.size(); i++) {
        var v = list.get(i);
        callback(v);
    }
}


var mdslist = metadataService.getMetadataDefs();
myForEach(mdslist, function(v) {model.metadatas[v.name.toString()] = v});

model.savedMds = {};

if (args.typeName != null && args.sousTypeName != null) {
    
    model.savedMds = typesService.getMetadatasMap(args.typeName, args.sousTypeName);
    model.mandatory_mds = typesService.getMandatoryMetadatas(args.typeName, args.sousTypeName);
    model.editable_mds = typesService.getEditableMetadatas(args.typeName, args.sousTypeName);  
}


if (model.mandatory_mds == null) {
    model.mandatory_mds = [];
    
}
if (model.editable_mds == null) {
    model.editable_mds = [];
}

if (model.savedMds == null) {
    model.savedMds = {};
}


logger.log(rootxml.toString());

model.ssTypes = new Array();
for (var numero in rootxml.MetierSousType) {
    model.ssTypes.push(rootxml.MetierSousType[numero].ID.toString());
    if (rootxml.MetierSousType[numero].ID.toString() == args.sousTypeName) {
        model.ssType = new Object();
        model.ssType.ID = rootxml.MetierSousType[numero].ID.toString();
        model.ssType.Desc = rootxml.MetierSousType[numero].Desc.toString();
        model.ssType.Circuit = iparapheur.getWorkflowByName(rootxml.MetierSousType[numero].Circuit.toString()).toString();
        model.ssType.Script = rootxml.MetierSousType[numero].Script.toString();
        model.ssType.readingMandatory = rootxml.MetierSousType[numero].readingMandatory.toString();
        // digitalSignatureMandatory
        /*
        BL (CMON) : je ne comprend pas ce test : 
        if (rootxml.MetierSousType[numero].digitalSignatureMandatory) {
        Dans tous les cas (digitalSignatureMandatory existant ou non), ce test vaut TRUE
        IMHO, le test qui permet de reconnaitre les circuits cree avec une 
        ancienne version est le suivant
        */
        if ("" + rootxml.MetierSousType[numero].digitalSignatureMandatory == "") {
            model.ssType.digitalSignatureMandatory = "" + model.default_digitalSignatureMandatory;
        } else {
            model.ssType.digitalSignatureMandatory = rootxml.MetierSousType[numero].digitalSignatureMandatory.toString();
        }
        if (rootxml.MetierSousType[numero].circuitHierarchiqueVisible) {
            model.ssType.circuitHierarchiqueVisible = rootxml.MetierSousType[numero].circuitHierarchiqueVisible.toString();
        } else {
            model.ssType.circuitHierarchiqueVisible = "true";
        }
        if ("" + rootxml.MetierSousType[numero].includeAttachments != "") {
            model.ssType.includeAttachments = rootxml.MetierSousType[numero].includeAttachments.toString();
        } else {
            model.ssType.includeAttachments = "true";
        }
        model.ssType.visibility = rootxml.MetierSousType[numero]["@visibility"].toString();
        model.ssType.parapheurs = new Array();
        var i = 0;
        for (var p in rootxml.MetierSousType[numero].parapheurs.parapheur) {
            model.ssType.parapheurs[i++] = rootxml.MetierSousType[numero].parapheurs.parapheur[p].toString();
        }
        
        model.ssType.visibilityFilter = rootxml.MetierSousType[numero]["@visibilityFilter"].toString();
        model.ssType.visibilityFilter = model.ssType.visibilityFilter != "" ? model.ssType.visibilityFilter : "public";
        model.ssType.parapheursFilters = [];
        j = 0;
        for (var pf in rootxml.MetierSousType[numero].parapheursFilters.parapheur) {
            model.ssType.parapheursFilters[j++] = rootxml.MetierSousType[numero].parapheursFilters.parapheur[pf].toString();
        }
        
        //StarXpert
        model.ssType.calques = new Array();
        i = 0;
        for (var c in rootxml.MetierSousType[numero].calques.calque) {
            model.ssType.calques[i++] = rootxml.MetierSousType[numero].calques.calque[c].toString();
        }
        
        model.ssType.groups = new Array();
        i = 0;
        for (var g in rootxml.MetierSousType[numero].groups.group) {
            model.ssType.groups[i++] = rootxml.MetierSousType[numero].groups.group[g].toString();
        }
        model.ssType.groupsFilters = [];
        j = 0;
        for (var gf in rootxml.MetierSousType[numero].groupsFilters.group) {
            model.ssType.groupsFilters[j++] = rootxml.MetierSousType[numero].groupsFilters.group[gf].toString();
        }

        model.isNew = false;
    }
}