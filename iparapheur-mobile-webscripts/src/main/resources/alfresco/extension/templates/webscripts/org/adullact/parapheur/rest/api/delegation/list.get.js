/**
 * Created by jmaire on 14/08/13.
 */
var toSearch = args.q;

if (toSearch) {
    model.mjson = iparapheur.searchUser(toSearch);
} else {
    model.mjson = usersService.getUsers();
}