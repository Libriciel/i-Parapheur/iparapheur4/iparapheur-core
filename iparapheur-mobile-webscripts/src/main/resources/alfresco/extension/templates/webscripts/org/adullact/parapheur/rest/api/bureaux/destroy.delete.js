//VALID

function is_valid_uuid(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

setStatusMessageCode("Fonctionnalité non implémentée", 501);

/* NON IMPLEMENTEE

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    var id = url.templateArgs['id'];
    var node =  search.findNode("workspace://SpacesStore/" + url.templateArgs['id']);
    //Test de la validité de l'id
    if(is_valid_uuid(id) && node) {
        try {
            //Suppression du bureau
            iparapheur.deleteBureau(node.nodeRef);
        } catch(e) {
            setStatusMessageCode("Erreur lors de la suppression du bureau : " + e, 500);
        }
    } else {
        setStatusMessageCode("Impossible de trouver le bureau d'ID " + id, 403);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}

*/
