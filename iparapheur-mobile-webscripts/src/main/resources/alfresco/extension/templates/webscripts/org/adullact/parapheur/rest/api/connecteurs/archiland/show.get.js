<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var configFiles = companyhome.childrenByXPath("/app:company_home/app:dictionary/ph:certificats/cm:archiland_configuration.xml");

    var ac = new XML(configFiles[0].properties.content.content);

    ret.host = ac.host.toString();
    ret.port = ac.port.toString();
    ret.user = ac.user.toString();
    ret.password = ac.password.toString();
    ret.collectivite = ac.collectivite.toString();

    ret.enabled = ac.enabled != undefined ? ac.enabled.toString() : "false";

    ret.services = [];

    for (var i=0; i<ac.services.service.length(); i++) {
        ret.services[i] = ac.services.service[i].toString()+":"+ac.services.service[i].@id;
    }

    model.mjson = JSON.stringify(ret);

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}