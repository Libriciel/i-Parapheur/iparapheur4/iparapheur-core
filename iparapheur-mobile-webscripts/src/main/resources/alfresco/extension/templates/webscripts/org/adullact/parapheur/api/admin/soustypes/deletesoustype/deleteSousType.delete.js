var sstypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ args.typeName +".xml']")[0];
if (null == sstypesnode) {
    throw new Error("Le type demandé n'existe pas");
}

var rootxml = new XML(sstypesnode.content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "")); // Workaround: Mozilla Bug 336551
var newrootxml = new XML();
newrootxml = <MetierSousTypes></MetierSousTypes>;
for (var numero in rootxml.MetierSousType)
{
    if (rootxml.MetierSousType[numero].ID.toString() != args.sousTypeName)
    {
            newrootxml.MetierSousTypes +=  rootxml.MetierSousType[numero];
    }
}
sstypesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + newrootxml.toXMLString();
sstypesnode.save();
