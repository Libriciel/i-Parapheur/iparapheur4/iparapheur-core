<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var id = url.templateArgs['id'];

var best = null;

var data = {};

if(is_valid_uuid(id)) {
    var username = usersService.getCurrentUsername();
    var dossier = search.findNode("workspace://SpacesStore/" + id);

    var hasSecretaire = false;
    var isSecretaire = false;

    if(dossier !== null) {
        var parentParapheur = iparapheur.getParentParapheur(dossier.nodeRef);

        if (iparapheur.isParapheurOwner(parentParapheur, username)) {
            best = parentParapheur;
            hasSecretaire = iparapheur.hasSecretaire(parentParapheur);
        } else if (iparapheur.isParapheurSecretaire(parentParapheur, username)) {
            best = parentParapheur;
            hasSecretaire = true;
            isSecretaire = true;
        } else {
            var delegateP = iparapheur.getParapheursOnDelegationPath(parentParapheur);
            for (var i = 0; i < delegateP.size(); i++) {
                if (iparapheur.isParapheurOwner(delegateP.get(i), username)) {
                    best = delegateP.get(i);
                    hasSecretaire = iparapheur.hasSecretaire(parentParapheur);
                    break;
                }
            }
            if(best === null) {
                best = iparapheur.getBestBureau(username, dossier.nodeRef);
            }
        }
        if (best !== null) {
            var parapheur = iparapheur.getParapheurAsScriptNode(best);
            logger.log(parapheur.nodeRef);
            var path = parapheur.qnamePath;
            if(!isSecretaire) {
                data = {
                    shortName: parapheur.properties["cm:name"],
                    name: parapheur.properties["cm:title"],
                    id: parapheur.id,
                    nodeRef: parapheur.nodeRef,
                    description: "",
                    image: "",
                    collectivite: "",
                    "a-traiter": companyhome.childrenByXPath(path + "/ph:a-traiter")[0].properties["ph:childCount"],
                    "en-retard": companyhome.childrenByXPath(path + "/ph:en-retard")[0].properties["ph:childCount"],
                    retournes: companyhome.childrenByXPath(path + "/ph:retournes")[0].properties["ph:childCount"],
                    "a-archiver": companyhome.childrenByXPath(path + "/ph:a-archiver")[0].properties["ph:childCount"],
                    "dossiers-delegues": companyhome.childrenByXPath(path + "/ph:dossiers-delegues")[0].properties["ph:childCount"],
                    "en-preparation": companyhome.childrenByXPath(path + "/ph:en-preparation")[0].properties["ph:childCount"],
                    hasSecretaire: hasSecretaire,
                    isSecretaire: false,
                    show_a_venir: parapheur.properties["ph:show-a-venir"],
                    habilitation: {
                        secretariat: parapheur.properties["ph:hab_secretariat"],
                        archivage: parapheur.properties["ph:hab_archivage"],
                        enchainement : parapheur.properties["ph:hab_enchainement"],
                        traiter: parapheur.properties["ph:hab_traiter"],
                        transmettre: parapheur.properties["ph:hab_transmettre"]
                    }
                };
            } else {
                data = {
                    shortName:parapheur.properties["cm:name"],
                    name:parapheur.properties["cm:title"],
                    id:parapheur.id,
                    nodeRef:parapheur.nodeRef,
                    description : "",
                    image : "",
                    collectivite : "",
                    secretariat : companyhome.childrenByXPath(path + "/ph:secretariat")[0].properties["ph:childCount"],
                    "a-imprimer" : companyhome.childrenByXPath(path + "/ph:a-imprimer")[0].properties["ph:childCount"],
                    "a-archiver" : companyhome.childrenByXPath(path + "/ph:a-archiver")[0].properties["ph:childCount"],
                    "dossiers-delegues" : companyhome.childrenByXPath(path + "/ph:dossiers-delegues")[0].properties["ph:childCount"],
                    "en-preparation": companyhome.childrenByXPath(path + "/ph:en-preparation")[0].properties["ph:childCount"],
                    hasSecretaire : hasSecretaire,
                    isSecretaire: true,
                    show_a_venir : parapheur.properties["ph:show-a-venir"],
                    habilitation : {
                        secretariat : parapheur.properties["ph:hab_secretariat"],
                        archivage : parapheur.properties["ph:hab_archivage"],
                        enchainement : parapheur.properties["ph:hab_enchainement"],
                        traiter : parapheur.properties["ph:hab_traiter"],
                        transmettre : parapheur.properties["ph:hab_transmettre"]
                    }
                };
            }
        }
    }
}

model.mjson = jsonUtils.toJSONString(data);