//Pas vraiment de vérifications à faire ici

/* gets the owned desk */

var username = usersService.getCurrentUsername(),
    isSecretaire = false,
    asAdmin = args['asAdmin'],
    data = [];

var user = people.getPerson(username);

var administres = JSON.parse(usersService.getBureauxAdministres(user.id));

if(asAdmin && (usersService.isAdministrateur(username) || administres.length > 0 || usersService.isGestionnaireCircuit(username))) {
    //RECUP TOUT PARAPHEURS
    var parapheurs = iparapheur.getParapheursAsScriptNode();

    for (var j = 0; j < parapheurs.size(); j++) {
        var paraph = parapheurs.get(''+j);
        if(iparapheur.isParapheur(paraph.nodeRef)) {
            var parent = paraph.assocs["ph:hierarchie"];
            var profondeur = iparapheur.getProfondeurHierarchie(paraph.nodeRef);
            var id = "";
            if(parent) {
                id = parent[0].id;
            }

            tmp = {
                name:paraph.properties["cm:name"],
                title:paraph.properties["cm:title"],
                id:paraph.id,
                nodeRef:paraph.nodeRef,
                description : paraph.properties["cm:description"],
                image : "",
                collectivite : "",
                proprietaires: iparapheur.getParapheurOwners(paraph.nodeRef),
                secretaires : iparapheur.getParapheurSecretaires(paraph.nodeRef),
                "metadatas-visibility":  paraph.properties["ph:metadatas-visibility"] || [],
                hierarchie : id,
                profondeur : profondeur,
                "show-a-venir" : paraph.properties["ph:show-a-venir"] !== null ? paraph.properties["ph:show-a-venir"] : true,
                hab_enabled: paraph.hasAspect("ph:habilitations"),
                hab_secretariat : paraph.properties["ph:hab_secretariat"],
                hab_archivage : paraph.properties["ph:hab_archivage"],
                hab_enchainement : paraph.properties["ph:hab_enchainement"],
                hab_traiter : paraph.properties["ph:hab_traiter"],
                hab_transmettre : paraph.properties["ph:hab_transmettre"],
                delegation: iparapheur.getDelegation(paraph.nodeRef)
            };

            data[j] = tmp;
        }
    }
} else {
    /* iterate over the desk list and get the relevant data */
    var handleParapheurs = function(parapheurs, isSec) {
        for (var i=0; i<parapheurs.size(); i++) {
            var parapheur = parapheurs.get(''+i);
            var path = parapheur.qnamePath;
            var tmp;

            var hasSecretaire = iparapheur.hasSecretaire(parapheur.nodeRef);

            if(!isSec) {
                tmp = {
                    shortName:parapheur.properties["cm:name"],
                    name:parapheur.properties["cm:title"],
                    id:parapheur.id,
                    nodeRef:parapheur.nodeRef,
                    description : "",
                    image : "",
                    collectivite : "",
                    "a-traiter" : companyhome.childrenByXPath(path + "/ph:a-traiter")[0].properties["ph:childCount"],
                    "en-retard" : companyhome.childrenByXPath(path + "/ph:en-retard")[0].properties["ph:childCount"],
                    retournes : companyhome.childrenByXPath(path + "/ph:retournes")[0].properties["ph:childCount"],
                    "a-archiver" : companyhome.childrenByXPath(path + "/ph:a-archiver")[0].properties["ph:childCount"],
                    "dossiers-delegues" : iparapheur.hasDelegationActive(parapheur.nodeRef) ? companyhome.childrenByXPath(path + "/ph:dossiers-delegues")[0].properties["ph:childCount"] : 0,
                    "en-preparation": companyhome.childrenByXPath(path + "/ph:en-preparation")[0].properties["ph:childCount"],
                    hasSecretaire : hasSecretaire,
                    isSecretaire: false,
                    show_a_venir : parapheur.properties["ph:show-a-venir"] !== null ? parapheur.properties["ph:show-a-venir"] : true,
                    habilitation : {
                        secretariat : parapheur.properties["ph:hab_secretariat"],
                        archivage : parapheur.properties["ph:hab_archivage"],
                        enchainement : parapheur.properties["ph:hab_enchainement"],
                        traiter : parapheur.properties["ph:hab_traiter"],
                        transmettre : parapheur.properties["ph:hab_transmettre"]
                    }
                };
            } else {
                tmp = {
                    shortName:parapheur.properties["cm:name"],
                    name:parapheur.properties["cm:title"],
                    id:parapheur.id,
                    nodeRef:parapheur.nodeRef,
                    description : "",
                    image : "",
                    collectivite : "",
                    secretariat : companyhome.childrenByXPath(path + "/ph:secretariat")[0].properties["ph:childCount"],
                    "a-imprimer" : companyhome.childrenByXPath(path + "/ph:a-imprimer")[0].properties["ph:childCount"],
                    "a-archiver" : companyhome.childrenByXPath(path + "/ph:a-archiver")[0].properties["ph:childCount"],
                    "dossiers-delegues" : companyhome.childrenByXPath(path + "/ph:dossiers-delegues")[0].properties["ph:childCount"],
                    "en-preparation": companyhome.childrenByXPath(path + "/ph:en-preparation")[0].properties["ph:childCount"],
                    hasSecretaire : hasSecretaire,
                    isSecretaire: true,
                    show_a_venir : parapheur.properties["ph:show-a-venir"] !== null ? parapheur.properties["ph:show-a-venir"] : true,
                    habilitation : {
                        secretariat : parapheur.properties["ph:hab_secretariat"],
                        archivage : parapheur.properties["ph:hab_archivage"],
                        enchainement : parapheur.properties["ph:hab_enchainement"],
                        traiter : parapheur.properties["ph:hab_traiter"],
                        transmettre : parapheur.properties["ph:hab_transmettre"]
                    }
                };
            }
            data[data.length] = tmp;
        }
    };

    data = [];

    handleParapheurs(iparapheur.getOwnedParapheursAsScriptNode(username), false);
    handleParapheurs(iparapheur.getSecretariatParapheurAsScriptNode(username), true);
}

model.mjson = jsonUtils.toJSONString(data);