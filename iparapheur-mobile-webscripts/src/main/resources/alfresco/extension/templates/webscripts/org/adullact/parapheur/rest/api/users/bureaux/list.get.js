<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE
//VALID

/**
 * Created by jmaire on 14/08/13.
 */

function is_valid_uuid(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var administres = args['administres'] === 'true';
var userId = url.templateArgs['userId'];

if(is_valid_uuid(userId)) {
    if (administres) {
        model.mjson = usersService.getBureauxAdministres(userId);
    }
    else {
        model.mjson = usersService.getBureaux(userId);
    }
} else {
    setStatusMessageCode("Des mauvais identifiants ont été renseignés. Utilisateur : " + userId + ", bureau : " + bureauId, 403);
}