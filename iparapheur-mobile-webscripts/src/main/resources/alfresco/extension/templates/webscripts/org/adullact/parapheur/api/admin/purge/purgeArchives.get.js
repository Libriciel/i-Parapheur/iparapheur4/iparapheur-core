// Il faut être loggué "admin multi-collectivités"
// Résultat dans workspace://SpacesStore/company_home/<RESULT_DATA_FILE>

importClass(Packages.org.alfresco.repo.security.authentication.AuthenticationUtil);
importClass(Packages.java.lang.RuntimeException);

const RESULT_DATA_FILE = "sharejs_archives_delete.csv";

var runnable = { doWork : deleteArchives };
var logData = "";
var nbOkTot = 0;
var nbNokTot = 0;

function logDataAdd(logdate, userName, docName, deleteResult, deleteExc) {
  var deleteExcStr = deleteExc == null ? "" : deleteExc.toString();
  var log = userName + "\t" + docName + "\t" + deleteResult + "\t" + deleteExcStr;
  logDataLog(logdate, log);
}

function logDataAddHeader() {
  logDataAdd("logdate", "userName", "docName", "deleteResult", "deleteException");
}

function logDataLog(logdate, message) {
  logData += logdate + "\t" + message + "\n";
}

function deleteArchives() {
  var userName = AuthenticationUtil.getRunAsUser();

  // les requêtes renvoient 1000 éléments maximum. On effectue donc la recherche plusieurs fois pour un même compte.
  do {
    var nbOkInLoop = 0;
    //MPAS : requête à étudier
    var nodes = search.luceneSearch("+PATH:\"/app:company_home/ph:archives/*\"");
    //var nodes = search.luceneSearch("+PATH:\"/app:company_home/ph:archives/*\" +TYPE:\"ph:archive\" AND @cm\\:created:[2010\-04\-19 TO NOW] ");

    for each(var node in nodes) {
      var docName = node.name;
      var docCreated = node.properties['cm:created'];//.toString()
      //var docContentUrl = node.properties['cm:content'].contentUrl;
      var deleteResult = false;
      var deleteExc = null;
      try {
        deleteResult = node.remove();
      } catch(ex) {
        deleteExc = ex;
      }
      logDataAdd(new Date(), userName, docName, deleteResult, deleteExc);
  
      if (deleteResult) {
        nbOkInLoop += 1;
        nbOkTot += 1;
      } else {
        nbNokTot += 1;
      }
    }
  } while (nbOkInLoop > 0); 
}

function runAs(runAsWork, user) {
  var originalFullAuthentication = AuthenticationUtil.getFullAuthentication();
  try {
    AuthenticationUtil.setRunAsUser(user);
    runAsWork.doWork();
  } catch(ex) {
    
    logDataLog(ex.toString());
  } finally {
    AuthenticationUtil.setFullAuthentication(originalFullAuthentication);
  }
}

function runTenants(indexFirst, indexLast, runAsWork) {
  // Accès au scriptable "MultiCServiceScriptable".
  // Il faut être loggué "admin multi-collectivités"
  // Les index sont 0-based.
  nbOkTot = 0;
  nbNokTot = 0;
  
  logDataAddHeader();
  // Boucle sur les tenants
  var tenants = multiCService.collectivites;
  var count = tenants.size();
  if ((indexLast < 0) || (indexLast >= count)) {
    indexLast = count - 1;
  }
  
  for (var i = indexFirst; (i <= indexLast) && (i < count); i++) {
    var tenant = tenants.get(i);    
    var tenantDomain = tenant.tenantDomain;
    var tenantLog = "Tenant " + i + "/" + (count-1) + " : " + tenantDomain;
    if (tenant.isEnabled()) {
      var userAdmin = "admin@" + tenantDomain;
      logger.log(tenantLog + ", administrateur : " + userAdmin);
      runAs(runAsWork, userAdmin);
    } else {
      logger.log(tenantLog + ", inactif");
    }
  }
  model.status = "OK";
}

var workspace = companyhome;
var dataFile = workspace.childByNamePath(RESULT_DATA_FILE);
if (dataFile != null) {
  dataFile.remove();
} 
dataFile = workspace.createFile(RESULT_DATA_FILE);
dataFile.content = logData;

model.outputFile = dataFile.url;
model.archives = nbOkTot;
// Index 0-based; -1 pour le dernier
runTenants(0, -1, runnable);

