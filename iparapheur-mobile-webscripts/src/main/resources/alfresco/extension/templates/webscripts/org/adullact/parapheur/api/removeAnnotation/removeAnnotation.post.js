<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);
importClass(Packages.com.atolcd.parapheur.repo.annotations.Rect);
importClass(Packages.com.atolcd.parapheur.repo.annotations.Point);

var rules = {
    "dossier":{"typeOf":"string", "isValid":is_valid_nodeRef()},
    "page":{"typeOf": "number", "optional":"true"}
}


var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    annotationService.removeAnnotation(jsonData.dossier, jsonData.uuid);
}
