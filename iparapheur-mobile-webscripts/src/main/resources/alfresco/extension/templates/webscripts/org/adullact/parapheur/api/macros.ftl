<#macro serializeCircuit circuit>
[
    <#local first = true>
    <#list circuit as e>
        <#if !first>,<#else><#local first = false></#if>
    {
    <#if e.nodeRef??>
    "nodeRef":"${e.nodeRef}",
</#if>
    "approved" : ${e.approved?string},
        <#if e_index == rejected>
        "rejected" : true,
</#if>
    "actionDemandee" : "${e.actionDemandee}",
        <#--if e.delegateur??>
        "delegateur"   : "${e.delegateur}",
</#if-->
    "dateValidation" : "${xmldate(e.dateValidation)}",
        <#if e.annotation??>
        "annotPub" : ${JSON.quote(e.annotation)},
</#if>
        <#if e.signatureEtape??>
        "signatureEtape" : "${e.signatureEtape}",
</#if>
        <#if hasSigInfo?? && hasSigInfo[e_index]?? && hasSigInfo[e_index]>
            "signatureInfo" : {
                "subject_name": "${signaturesInfo[e_index][0]["subject_name"]}",
                "issuer_name": "${signaturesInfo[e_index][0]["issuer_name"]}",
                "signature_date": "${signaturesInfo[e_index][0]["signature_date"]}",
                "certificate_valid_from": "${signaturesInfo[e_index][0]["certificate_valid_from"]}",
                "certificate_valid_to": "${signaturesInfo[e_index][0]["certificate_valid_to"]}"
            },
</#if>
        <#if e.signataire??>
        "signataire" : "${jsonUtils.encodeJSONString(e.signataire)}",
</#if>
        <#--if e.graphicalAnnotations??>
        "annotations": {

            <#list e.graphicalAnnotations?keys as page>
            "${page}" : [
                <#assign annotations = e.graphicalAnnotations[page]/>
                <#list annotations as annot>
                {
                "uuid" : "${annot.UUID}",
                "secretaire" : "${annot.secretaire?string}",
                "type" : "${annot.type}",
                "date" : "${xmldate(annot.date)}",
                "author" : "${annot.author}",
                "penColor" : "${annot.penColor}",
                "text" : "${jsonUtils.encodeJSONString(annot.text)}",
                "fillColor" : "${annot.fillColor}"

                    <#if annot.rect??>
                    ,
                "rect" : {
                    "topLeft" : {
                        "x" : ${annot.rect.topLeftCorner.x?c},
                        "y" : ${annot.rect.topLeftCorner.y?c} },

                    "bottomRight" :{
                        "x" : ${annot.rect.bottomRightCorner.x?c},
                        "y" : ${annot.rect.bottomRightCorner.y?c} }
                    }
</#if>
                }
                    <#if annot_has_next>,</#if>

</#list>
            ] <#if page_has_next>,</#if>
</#list>
        },


</#if-->
    <#if e.parapheurName??>
    "parapheurName" : "${e.parapheurName}"
    </#if>
    <#if e.delegueName??>
    ,"delegueName" : "${e.delegueName}"
    </#if>
    }
</#list>
]
</#macro>

<#macro serializeAnnotations circuit>
[
    <#local first = true>
    <#list circuit as e>
        <#if !first>,<#else><#local first = false></#if>


        {
        <#if e.graphicalAnnotations??>

            <#list e.graphicalAnnotations?keys as page>
            "${page}" : [
                <#assign annotations = e.graphicalAnnotations[page]/>
                <#list annotations as annot>
                {
                "id" : "${annot.UUID}",
                "secretaire" : "${annot.secretaire?string}",
                "type" : "${annot.type}",
                "date" : "${xmldate(annot.date)}",
                "author" : "${annot.author}",
                <#if annot.penColor?exists>
                    "penColor" : "${annot.penColor}",
</#if>
                "text" : "${jsonUtils.encodeJSONString(annot.text)}"

                <#if annot.fillColor?exists>
                    ,
                    "fillColor" : "${annot.fillColor}"
</#if>

                    <#if annot.rect??>
                    ,
                    "rect" : {
                    "topLeft" : {
                    "x" : ${annot.rect.topLeftCorner.x?c},
                    "y" : ${annot.rect.topLeftCorner.y?c} },

                    "bottomRight" :{
                    "x" : ${annot.rect.bottomRightCorner.x?c},
                    "y" : ${annot.rect.bottomRightCorner.y?c} }
                    }
</#if>
                }
                    <#if annot_has_next>,</#if>

</#list>
            ] <#if page_has_next>,</#if>
</#list>



</#if>

    }
</#list>
]
</#macro>

<#macro serializeSequence sequence>
[
    <#local first = true>
    <#list sequence as e>
        <#if !first>,<#else><#local first = false></#if>
        <#if e?is_date>"${xmldate(e)}"
        <#elseif e?is_boolean>${e?string}
        <#elseif e?is_number>${e?c}
        <#elseif e?is_sequence><@serializeSequence sequence=e/>
        <#elseif e?is_string>"${e}"
        <#elseif e?is_hash_ex><@serializeHash hash=e/>

</#if>
</#list>
]
</#macro>

<#macro serializeHash hash>

    <#if hash?is_hash_ex>

    {
        <#escape x as jsonUtils.encodeJSONString(x)>
            <#local first = true>
            <#list hash?keys as key>

                <#if hash[key]??>
                    <#local val = hash[key]>
                    <#if !first>,<#else><#local first = false></#if>"${key}":
                    <#if val?is_date>"${xmldate(val)}"
                    <#elseif val?is_boolean>${val?string}
                    <#elseif val?is_number>${val?c}
                    <#elseif val?is_sequence><@serializeSequence sequence=val/>
                    <#elseif val?is_string>"${val}"
                    <#elseif val?is_hash_ex><@serializeHash hash=val/>

</#if>
                <#--else>
          <#if !first>,<#else><#local first = false></#if>"${key}": null -->
</#if>
</#list>

</#escape>
    }
</#if>

</#macro>

<#--
<#macro serializeHash hash>
    <#escape x as jsonUtils.encodeJSONString(x)>
        <#local first = true>
        <#list hash?keys as key>
            <#if hash[key]??>
                <#local val = hash[key]>
                <#if !first>,<#else><#local first = false></#if>"${key}":
                <#if isUser && object.isTemplateContent(val)>"${val.content}"
                    <#elseif object.isTemplateContent(val)>"${val.url}"
                    <#elseif object.isTemplateNodeRef(val)>"${val.nodeRef}"
                    <#elseif val?is_date>"${xmldate(val)}"
                    <#elseif val?is_boolean>${val?string}
                    <#elseif val?is_number>${val?c}
                    <#elseif val?is_sequence><@serializeSequence sequence=val/>
                    <#elseif val?is_hash><@serializeHash hash=val/>
                    <#else>"${val}"
</#if>
</#if>
</#list>
</#escape>
</#macro>
-->