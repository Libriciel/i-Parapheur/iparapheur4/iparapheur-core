//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Récupération de l'id du type
var id = url.templateArgs['id'];
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var parameters = s2lowConfig.getSigParametersForType(id);

    ret.active = parameters.active;

    if (ret.active == undefined) {
        ret.active = 'true';
    }

    ret.pPolicyIdentifierID = parameters.pPolicyIdentifierID;
    ret.pPolicyIdentifierDescription = parameters.pPolicyIdentifierDescription;
    ret.pPolicyDigest = parameters.pPolicyDigest;
    ret.pSPURI = parameters.pSPURI;
    ret.pCountryName = parameters.pCountryName;
    ret.pClaimedRole = parameters.pClaimedRole;

    model.mjson = JSON.stringify(ret);

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
