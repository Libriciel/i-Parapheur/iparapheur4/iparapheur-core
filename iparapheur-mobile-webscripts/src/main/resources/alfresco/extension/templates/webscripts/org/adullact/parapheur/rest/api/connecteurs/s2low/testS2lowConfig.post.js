<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "cert": {"typeOf":"string", "optional": true},
    "currentCertName": {"typeOf":"string", "optional": true},
    "userlogin": {"typeOf": "string", "optional": true},
    "userpassword": {"typeOf": "string", "optional": true},
    "password": {"typeOf":"string"},
    "port" : {"typeOf":"string"},
    "server" : {"typeOf":"string"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var ret = {};
        ret.listeLogins = s2lowConfig.getArrayLogin(jsonData.cert, jsonData.password, jsonData.port, jsonData.server, jsonData.currentCertName);

        ret.validCertCnx = (ret.listeLogins !== null) ? "ok" : "ko";

        if(jsonData.userlogin !== undefined) {
            ret.validLoginAndCertCnx = s2lowConfig.testLogedConnexion(jsonData.cert, jsonData.password, jsonData.port, jsonData.server, jsonData.currentCertName, jsonData.userlogin, jsonData.userpassword) ? "ok" : "ko";
        }


        if(jsonData.cert !== undefined) {
            ret.isPwdGoodForPkcs = s2lowConfig.isPwdValidForFileCertificate(jsonData.cert, jsonData.password);
        } else {
            ret.isPwdGoodForPkcs = s2lowConfig.isPwdValidForCertificate(jsonData.currentCertName, jsonData.password).substring(0, 2);
        }

        model.mjson = jsonUtils.toJSONString(ret);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}