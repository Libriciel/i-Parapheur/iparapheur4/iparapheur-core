var jsonData = JSON.parse(requestbody.content);

var action = args.action;
var IDtype = jsonData.typeName;
var newssID = jsonData.name;
var newDesc = jsonData.description;
var newCircuit = jsonData.circuit;
var newScript = "";
var readingMandatory = jsonData.mustRead;
var digitalSignatureMandatory = jsonData.mustSign;
var circuitHierarchiqueVisible = jsonData.circuitVisible;
var includeAttachments = jsonData.includeAttachementsWhenPrintingOrArchive;
var isPublic = jsonData["public"];
var parapheurs = jsonData.bureaux;
var groupes = jsonData.groupes;
var groupsFilters = jsonData.groupsFilters;
var parapheursFilters = jsonData.parapheursFilters;
var isPublicFilters = jsonData.visibilityFilter === "private";

// Optional arg, true if not present 
if(jsonData.visibilityFilter == null){
    // si on a pas de groupe, alors c'est un T1C et on le met public
    if(groupes.length == 0){
        isPublicFilters = true;    
    }
    // si nous avons un groupe, c'est en TMC donc on se met par défaut sur le groupe
    else {
        isPublicFilters = false;
        groupsFilters = jsonData.groupes;
    }
}
var sstypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ IDtype +".xml']")[0];
var rootxml = new XML(sstypesnode.content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "")); // Workaround: Mozilla Bug 336551


// Ajouter le noeud XML dans le fichier
var str = "<MetierSousType visibility=\"" + (isPublic ? "public" : "private") + "\" visibilityFilter=\""+ (isPublicFilters ? "public" : "private") +"\">" +
"<ID>"+newssID+"</ID>" +
"<Desc><![CDATA["+newDesc+"]]></Desc>"+
"<Circuit>"+newCircuit+"</Circuit>" +
"<Script><![CDATA["+newScript+"]]></Script>" +
"<parapheurs>";
for each (parapheur in parapheurs) {
    str += "<parapheur>" + parapheur + "</parapheur>";
}
str += "</parapheurs>" +
       "<parapheursFilters>";
for each (parapheur in parapheursFilters) {
    str += "<parapheur>" + parapheur + "</parapheur>";
}
str += "</parapheursFilters>" +
        
        
"<groups>";
for each (group in groupes) {
    str += "<group>" + group+ "</group>";
}

str += "</groups>" +
"<groupsFilters>";
for each (groupFilter in groupsFilters) {
    str += "<group>" + groupFilter + "</group>";
}
str += "</groupsFilters>" +
        
        
    "<digitalSignatureMandatory>" + (digitalSignatureMandatory ? "true" : "false") + "</digitalSignatureMandatory>" +
    "<readingMandatory>" + (readingMandatory ? "true" : "false") + "</readingMandatory>" +
    "<circuitHierarchiqueVisible>" + (circuitHierarchiqueVisible ? "true" : "false") + "</circuitHierarchiqueVisible>" +
            "<includeAttachments>" + (includeAttachments ? "true" : "false") + "</includeAttachments>" +
    "</MetierSousType>";
var newsoustype = new XML(str);
rootxml.MetierSousTypes += newsoustype;
sstypesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + rootxml.toXMLString();
sstypesnode.save();
