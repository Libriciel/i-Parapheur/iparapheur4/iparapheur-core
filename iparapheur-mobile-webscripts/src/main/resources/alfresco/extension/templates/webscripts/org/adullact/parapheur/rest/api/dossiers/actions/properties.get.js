<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA


var id = url.templateArgs['id'];

if (is_valid_uuid(id)) {
    var dossier = search.findNode("workspace://SpacesStore/" + id);
    model.ret = dossierService.getProperties(dossier.nodeRef);
}