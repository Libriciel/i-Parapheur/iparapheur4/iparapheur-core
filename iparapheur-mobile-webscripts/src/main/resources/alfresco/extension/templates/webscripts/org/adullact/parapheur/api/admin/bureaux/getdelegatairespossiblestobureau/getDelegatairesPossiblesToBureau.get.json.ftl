{
"bureaux" : 
        [
            <#assign first = true>
            <#list bureaux as bureau>
                <#if first == false>
                    ,
                </#if>
                "${bureau.nodeRef}"
                <#assign first = false>
            </#list>
        ]
}