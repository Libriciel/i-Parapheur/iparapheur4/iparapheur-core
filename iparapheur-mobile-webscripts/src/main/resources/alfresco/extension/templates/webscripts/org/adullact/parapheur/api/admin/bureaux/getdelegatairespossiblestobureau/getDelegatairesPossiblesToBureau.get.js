
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);
importPackage(Packages.java.util);

var bureau = search.findNode(args.bureauId).getNodeRef();

var listBureaux = new ArrayList();

listBureaux = iparapheur.getDelegationsPossibles(bureau);
model.bureaux = listBureaux;