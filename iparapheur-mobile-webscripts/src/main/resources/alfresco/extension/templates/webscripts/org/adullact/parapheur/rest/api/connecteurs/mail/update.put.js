<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "username": {"typeOf":"string"},
    "password": {"typeOf":"string"},
    "server": {"typeOf":"string"},
    "protocole" : {"typeOf":"string"},
    "port" : {"typeOf":"string"},
    "folder": {"typeOf": "string"},
    "enabled": {"typeOf": "string"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var configFiles = companyhome.childrenByXPath("/app:company_home/app:dictionary/ph:email_listener/ph:email_listener-configuration");

        var ac = new XML(configFiles[0].properties.content.content);

        ac.username = jsonData.username;
        ac.password = jsonData.password;
        ac.server = jsonData.server;
        ac.protocole = jsonData.protocole;
        ac.port = jsonData.port;
        ac.folder = jsonData.folder;
        ac.enabled = jsonData.enabled;

        configFiles[0].properties.content.content = ac.toXMLString();
        configFiles[0].save();
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
