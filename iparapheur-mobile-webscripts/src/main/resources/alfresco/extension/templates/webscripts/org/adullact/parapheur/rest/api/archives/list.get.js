// Pour un utilisateur, utiliser les filtres
var pageSize= args["pageSize"] != undefined ? +args["pageSize"] : 10;
var propSort = args["sort"] != undefined ? args["sort"] : "cm:title";
var asc = args["asc"] != undefined ? args["asc"] : "true";
var skipped = args["skipped"] !== undefined ? +args["skipped"] : 0;
var page = args["page"] !== undefined ? +args["page"] : 0;
var metas = args["metas"] != undefined ? jsonUtils.toObject(args["metas"]) : {};
var filters = args["filter"] != undefined ? jsonUtils.toObject(args["filter"]) : {};

model.mjson = dossierService.handleListArchives(pageSize, filters, propSort, metas, asc, page, skipped);
