
 <import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);
importClass(Packages.com.atolcd.parapheur.repo.annotations.Rect);
importClass(Packages.com.atolcd.parapheur.repo.annotations.Point);


var rules = {
    "dossier":{"typeOf":"string", "isValid":is_valid_nodeRef()},
    "annotations":{
        "typeOf":"array",
        "childRules":{
            "type":{"typeOf":"string", "isValid":is_valid_annot_type()},

            "page":{"typeOf":"number"},

            "rect":{"typeOf":"object",
                "isValid":is_valid_rect()
            },
            "text":{"typeOf":"string"},
            "fillColor":{"typeOf":"string", "optional":"true"},
            "penColor":{"typeOf":"string", "optional":"true"}
        }
    }
};


//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {

    var dossier = jsonData.dossier;
    var annotations = jsonData.annotations;
    var results = [];

    for (var i = 0; i < annotations.length; i++) {

        var annot = annotations[i];

        logger.log(annot);
        var type = annot.type;
        var page = annot.page;
        var topLeft = annot.rect.topLeft;
        var bottomRight = annot.rect.bottomRight;
        var aRect = new Rect(new Point(topLeft.x, topLeft.y), new Point(bottomRight.x, bottomRight.y));
        var a = null;

        if (type === "rect") {
            a = annotationService.getNewRectAnnotationWith(aRect, annot.penColor, annot.fillColor, annot.text, page);
        }
        else if (type === "highlight") {
            a = annotationService.getNewHighlightAnnotationWith(aRect, annot.penColor, annot.fillColor, annot.text, page);
        }
        else if (type === "text") {
            a = annotationService.getNewTextAnnotationWith(aRect, annot.picto, annot.text, page);
        }

        if (a !== null) {
            results.push(annotationService.addAnnotation(new NodeRef(dossier), a).toString());
        }

    }

    model.uuids = results;

}
