var bureau = args['bureau'],
    asAdmin = args['asAdmin'],
    search = args['search'],
    username = usersService.getCurrentUsername();

if(asAdmin) {
    model.mjson = typesService.getTypologyAsAdmin(search || "");
} else if(bureau) {
    //For Creation
    model.mjson = typesService.getTypologyWithBureau(bureau);
} else {
    //For Filters
    model.mjson = typesService.getAllTypology();
}