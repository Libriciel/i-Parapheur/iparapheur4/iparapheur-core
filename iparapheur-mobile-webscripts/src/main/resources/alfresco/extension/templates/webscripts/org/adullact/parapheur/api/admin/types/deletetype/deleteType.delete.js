var typesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiertype/cm:TypesMetier.xml")[0];

if (null == typesnode) {
    throw new Error("Les types techniques n'ont pas été initialisés.");
}

var soustypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ args.name +".xml']")[0];
if (soustypesnode !=  null) {
    soustypesnode.remove();
}

var content = typesnode.content;

// Workaround: Mozilla Bug 336551
content = content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "");

var rootxml = new XML(content);
var newrootxml = new XML();
newrootxml = <MetierTypes/>;
for (var numero in rootxml.MetierType) {
    if (rootxml.MetierType[numero].ID.toString() != args.name) {
        newrootxml.MetierTypes +=  rootxml.MetierType[numero];
    }
}
typesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + newrootxml.toXMLString();
typesnode.save();
