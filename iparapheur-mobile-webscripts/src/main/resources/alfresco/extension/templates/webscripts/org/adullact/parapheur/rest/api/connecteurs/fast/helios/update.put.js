<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(java.util.HashMap);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "cert": {"typeOf":"string", "optional": true},
    "name": {"typeOf":"string", "optional": true},
    "password": {"typeOf":"string"},
    "port" : {"typeOf":"string"},
    "server" : {"typeOf":"string"},
    "active": {"typeOf": "string"},
    "pPolicyIdentifierID": {"typeOf": "string"},
    "pPolicyIdentifierDescription": {"typeOf": "string"},
    "pPolicyDigest": {"typeOf": "string"},
    "pSPURI": {"typeOf": "string"},
    "pCity": {"typeOf": "string"},
    "pPostalCode": {"typeOf": "string"},
    "pCountryName": {"typeOf": "string"},
    "pClaimedRole": {"typeOf": "string"},
    "collectivite": {"typeOf": "string"},
    "parapheur": {"typeOf": "string"},
    "cinematique": {"typeOf": "string", "optional": true},
    "check_freq": {"typeOf": "string", "optional": true},
    "time_to_consider_file_not_pickable": {"typeOf": "string", "optional": true}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        if(jsonData.cert !== undefined && jsonData.name !== undefined) {
            var content = jsonData.cert;
            var filename = jsonData.name;
        }
        var parameters = new HashMap();
        var cert = null;
        if (filename && filename != "" && content && content != "") {
            fastConfig.createCertificateFile(filename, content);

            parameters.name = "" + filename;
            parameters.password = "" + jsonData.password;
        } else {
            var oldParams = fastConfig.getHeliosParameters();
            parameters.name = "" + oldParams.name;
            parameters.password = "" + oldParams.password;
        }
        parameters.active = "" + jsonData.active;
        parameters.server = "" + jsonData.server;
        parameters.port = "" + jsonData.port;

        parameters.collectivite = "" + jsonData.collectivite;
        parameters.parapheur = "" + jsonData.parapheur;
        parameters.pPolicyIdentifierID = "" + jsonData.pPolicyIdentifierID;
        parameters.pPolicyIdentifierDescription = "" + jsonData.pPolicyIdentifierDescription;
        parameters.pPolicyDigest = "" + jsonData.pPolicyDigest;
        parameters.pSPURI = "" + jsonData.pSPURI;
        parameters.pCity = "" + jsonData.pCity;
        parameters.pPostalCode = "" + jsonData.pPostalCode;
        parameters.pCountryName = "" + jsonData.pCountryName;
        parameters.pClaimedRole = "" + jsonData.pClaimedRole;

        parameters.cinematique = "" + jsonData.cinematique;
        parameters.check_freq = "" + jsonData.check_freq; // minutes
//ret.time_to_consider_file_in_error = parameters.time_to_consider_file_in_error; // jours
        parameters.time_to_consider_file_not_pickable = "" + jsonData.time_to_consider_file_not_pickable; // jours

        fastConfig.setHeliosParameters(parameters);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}