// Il faut être loggué "admin multi-collectivités"
// Résultat dans workspace://SpacesStore/company_home/<RESULT_DATA_FILE>

importClass(Packages.org.alfresco.repo.security.authentication.AuthenticationUtil);
importClass(Packages.java.lang.RuntimeException);

const RESULT_DATA_FILE = "sharejs_archives_count.csv";

var runnable = { doWork : countArchives };
var logData = "";
var nbOkTot = 0;
var nbNokTot = 0;


function logDataAdd(logdate, userName, dossiersCount) {
  var log = userName + "\t" + dossiersCount;
  logDataLog(logdate, log);
}

function logDataAddHeader() {
  logDataAdd("logdate", "userName", "dossiersCount");
}

function logDataLog(logdate, message) {
  logData += logdate + "\t" + message + "\n";
}

function countArchives(){
  //var nodes = search.luceneSearch("+PATH:\"/app:company_home/ph:archives/*\"");
  var nodes = search.luceneSearch("+PATH:\"/app:company_home/ph:archives/*\" +TYPE:\"ph:archive\" AND @cm\\:created:[2010\-04\-19 TO NOW] ");
  var userName = AuthenticationUtil.getRunAsUser();
  var dossiersCount = nodes.length;
  nbOkTot += dossiersCount;
  logDataAdd(new Date(), userName, dossiersCount);
}

function runAs(runAsWork, user) {
  var originalFullAuthentication = AuthenticationUtil.getFullAuthentication();
  try {
    AuthenticationUtil.setRunAsUser(user);
    runAsWork.doWork();
  } catch(ex) {
    logDataLog(ex.toString());
  } finally {
    AuthenticationUtil.setFullAuthentication(originalFullAuthentication);
  }
}

function runTenants(indexFirst, indexLast, runAsWork) {
  // Accès au scriptable "MultiCServiceScriptable".
  // Il faut être loggué "admin multi-collectivités"
  // Les index sont 0-based.
  nbOkTot = 0;
  nbNokTot = 0;
  
  logDataAddHeader();
  // Boucle sur les tenants
  var tenants = multiCService.collectivites;
  var count = tenants.size();
  if ((indexLast < 0) || (indexLast >= count)) {
    indexLast = count - 1;
  }
  logger.log("Tenants (count=" + count + ") : " + indexFirst + " à " + indexLast);
  for (var i = indexFirst; (i <= indexLast) && (i < count); i++) {
    var tenant = tenants.get(i);    
    var tenantDomain = tenant.tenantDomain;
    var tenantLog = "Tenant " + i + "/" + (count-1) + " : " + tenantDomain;
    if (tenant.isEnabled()) {
      var userAdmin = "admin@" + tenantDomain;
      logger.log(tenantLog + ", administrateur : " + userAdmin);
      runAs(runAsWork, userAdmin);
    } else {
      logger.log(tenantLog + ", inactif");
    }
  }
  logger.log("Total = OK=" + nbOkTot + "; NOK=" + nbNokTot + "; total=" + (nbOkTot + nbNokTot));
}

var workspace = companyhome;
var dataFile = workspace.childByNamePath(RESULT_DATA_FILE);
if (dataFile != null) {
  dataFile.remove();
} 

// Index 0-based; -1 pour le dernier

runTenants(0, -1, runnable);


dataFile = workspace.createFile(RESULT_DATA_FILE);
dataFile.content = logData;

model.outputFile = dataFile.url;
model.archives = nbOkTot;
