<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

function is_valid_id(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var rules = {
    "aclGroupes": {"typeOf":"array"},
    "aclParapheurs": {"typeOf":"array","isValid":is_valid_uuid()},
    "etapes" : {
        "typeOf":"array",
        "isValid":is_valid_etape()
    },
    "isPublic" : {"typeOf":"boolean"},
    "name" : {"typeOf":"string"}
};

var ret = {};
var id = url.templateArgs['id'];

model.idWorkflow = "";

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Récupération du contenu JSON de la requete
var jsonData = JSON.parse(requestbody.content);

if ((!usersService.isAdministrateur(username)) && (!usersService.isGestionnaireCircuit(username))) {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
else if (!validate(jsonData, rules)) {
    setStatusMessageCode("Invalid data sent, expecting : \n" + jsonUtils.toJSONString(rules), 400);
}
else if (!is_valid_id(id)) {
    setStatusMessageCode("1005", 400);
}
else {
    var workflow = search.findNode("workspace://SpacesStore/" +id);
    if (workflow) {
        try {
            var result = jsonUtils.toObject(workflowService.updateWorkflow(workflow.nodeRef,
                                                                           jsonData.name,
                                                                           jsonData.etapes,
                                                                           jsonData.aclParapheurs,
                                                                           jsonData.aclGroupes,
                                                                           jsonData.isPublic));
            if (result['statusCode'] == 200) {
                model.idWorkflow = result['errorMessage'];
            } else {
                setStatusMessageCode(result['errorMessage'], result['statusCode']);
            }
        } catch (e) {
            setStatusMessageCode("999", 500);
        }
    } else {
        setStatusMessageCode("1005", 400);
    }
}