importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);
importPackage(Packages.java.util);

var jsonData = JSON.parse(requestbody.content);

var bureau = search.findNode(jsonData.bureauId).getNodeRef();

var listBureaux = new ArrayList();

for (var i = 0; ; i++) {
    if (!jsonData.bureaux[i]) {
        break;
    }
    listBureaux.add(search.findNode(jsonData.bureaux[i]).getNodeRef());
}
iparapheur.setDelegationsPossibles(bureau, listBureaux);
