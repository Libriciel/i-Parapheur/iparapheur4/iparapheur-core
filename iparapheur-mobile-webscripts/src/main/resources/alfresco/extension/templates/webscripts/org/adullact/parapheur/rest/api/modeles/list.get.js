/**
 * Liste des templates e-mail et impression
 */

importClass(Packages.java.util.HashMap);

//var printnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ args.id +".xml']");
var printnodes = companyhome.childrenByXPath("./app:dictionary/app:content_templates/*[like(@cm:name, 'parapheur*')]");
if (printnodes == null) {
    throw new Error("Les templates i-parapheur sont introuvables.");
}


//var emailnodes = companyhome.childrenByXPath("./app:dictionary/app:email_templates/*[@cm:name='parapheur*']");
var emailnodes = companyhome.childrenByXPath("./app:dictionary/app:email_templates/*[like(@cm:name, 'parapheur*')]");
if (emailnodes == null) {
    throw new Error("Les templates email i-parapheur sont introuvables.");
}

var ret = [];

for each(node in printnodes) {
    ret.push({
        type: "bdx",
        name: node.name,
        id: node.id
    });
}

for each(node in emailnodes) {
    ret.push({
        type: "mail",
        name: node.name,
        id: node.id
    });
}

model.mjson = jsonUtils.toJSONString(ret);
