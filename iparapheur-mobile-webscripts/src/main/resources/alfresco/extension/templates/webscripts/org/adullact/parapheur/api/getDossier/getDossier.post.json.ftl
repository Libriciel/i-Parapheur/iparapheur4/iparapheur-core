<#import "*/macros.ftl" as my>

{
<#if object?exists>
    <@serializeDossier object/>
</#if>
}
<#--

-->
<#macro serializeDossier dossier>
"titre": "${object.properties["cm:title"]?string}",
"dossierRef": "${object.nodeRef}",
"recuperable": ${object.properties["ph:recuperable"]?string},
"isRejete":${isRejete?string},
<#if object.properties["ph:xpathSignature"]?exists>
    "xpathSignature": "${object.properties["ph:xpathSignature"]}",
</#if>
<#if object.properties["ph:typeMetier"]?exists>
    "type": "${object.properties["ph:typeMetier"]}",
</#if>
<#if object.properties["ph:soustypeMetier"]?exists>
    "sousType": "${object.properties["ph:soustypeMetier"]}",
</#if>
<#if object.properties["ph:public"]?exists>
    "publik": "${object.properties["ph:public"]?string}",
</#if>
<#if object.properties["ph:confidentiel"]?exists>
    "confidentiel": "${object.properties["ph:confidentiel"]?string}",
</#if>
<#if object.properties["ph:tdtNom"]?exists>
    "nomTdt": "${object.properties["ph:tdtNom"]}",
</#if>
<#if object.properties["ph:tdtProtocole"]?exists>
    "protocoleTdt": "${object.properties["ph:tdtProtocole"]}",
</#if>
<#if object.properties["ph:reading-mandatory"]?exists>
    "lectureObligatoire": ${object.properties["ph:reading-mandatory"]?string},
</#if>
<#if object.properties["ph:digital-signature-mandatory"]?exists>
    "signatureNumeriqueObligatoire": ${object.properties["ph:digital-signature-mandatory"]?string},
</#if>
<#if object.properties["ph:status-metier"]?exists>
"statutMetier" : "${object.properties["ph:status-metier"]}",
</#if>
<#if object.properties["ph:dateLimite"]?exists>
    "dateLimite" : "${xmldate(object.properties["ph:dateLimite"])}",
</#if>
<#if canAdd?exists>
    "canAdd" : true,
</#if>
    "hasRead":${hasRead?string},
    "isRead":${isRead?string},
<#if parent?exists>
    "parent" : "${parent}",
</#if>
"dateCreation" : "${xmldate(object.properties["cm:created"])}",
"actionDemandee": "${actionDemandee}"

    <#if object.children?exists>
    ,
    "documents":
    [
        <#assign first = true>
        <#list object.children as child>
            <#if child.isDocument = true>
                <#if first == false>
                ,
                </#if>
            {
                <@serializeDocument child />

         <#if first>
             <#if pageCount?exists>
                ,"pageCount":"${pageCount?string.computer}" <#-- Pour éviter l'espace si page > 1 000 -->
             </#if>
             <#if images?exists>
                ,"images":${images}
             </#if>
         <#else>

         ,"images": {}
         </#if>
            }

                <#assign first = false>
            </#if>

        </#list>
    ]
    <#else>
    ,
    "documents": []
    </#if>
    <#if meta?exists>
        ,
        "metadonnees": <@my.serializeHash meta />
    </#if>
        ,
        "actions": {
            "remorse":${remorse?string},
            "delete":${del?string},
            "reject":${reject?string},
            "sign":${sign?string},
            "raz":${raz?string},
            "edit":${edit?string},
            "archive":${archive?string},
            "secretary":${secretary?string}
        },
        "acteurCourant":${acteurCourant?string},
        "isPesPreviewEnabled":${hasXemelios?string},
        "isSignPapier":${signPapier?string},
        "isCurrentTdt":${isCurrentTdt?string}

</#macro>

<#macro serializePreviews object>
    <#if object?exists>
        <#assign first = true>
        <#list object.children as child>
            <#if first == false>
            ,
            </#if>
        {"image": "/api/node/${child.nodeRef?replace("://", "/")}/content"}
            <#assign first = false>

        </#list>
    </#if>
</#macro>

<#macro serializeDocument document>
    "name" : "${document.name}",
    "size" : ${document.size?c},
    "nodeRef" : "${document.nodeRef}",
    "downloadUrl" : "/api/node/${document.nodeRef?replace("://", "/")}/content",
    "isLocked" : ${document.isLocked?string},
    <#if canDelete[document.nodeRef]??>
        <#if canDelete[document.nodeRef] = true>
            "canDelete" : true
        <#else>
            "canDelete" : false
        </#if>
    </#if>
    <#if document.properties['ph:visuel-pdf']??>
        ,"visuelPdfUrl" : "/api/node/content%3bph%3avisuel-pdf/${document.nodeRef?replace("://", "/")}/${document.name}.pdf"
    <#--#else>
        "visuelPdfUrl" : ""-->
    </#if>
</#macro>
