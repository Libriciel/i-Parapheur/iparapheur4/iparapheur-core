//VALID

function is_valid_uuid(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var id = url.templateArgs['id'];
var asAdmin = args['asAdmin'];

var data = {};
//Test de la validité de l'id
if(is_valid_uuid(id)) {
    try {
        var parapheur = search.findNode("workspace://SpacesStore/" + url.templateArgs['id']);
        //Creation du bureau et récupération de l'id pour le retour
        if(asAdmin) {
            data["delegations-possibles"] = iparapheur.getBureauxAssociesAsAdmin(parapheur.nodeRef);
        } else {
            data["associes"] = iparapheur.getBureauxAssocies("workspace://SpacesStore/" + id);
        }
    } catch(e) {
        setStatusMessageCode("Erreur lors de la récupération des bureaux associés : " + e, 500);
    }
} else {
    setStatusMessageCode("ID du bureau invalide", 400);
}

model.mjson = jsonUtils.toJSONString(data);