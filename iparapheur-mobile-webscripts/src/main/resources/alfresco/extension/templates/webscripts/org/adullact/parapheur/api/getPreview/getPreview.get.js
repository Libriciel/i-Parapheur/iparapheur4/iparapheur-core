<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var contentNodeRef = new NodeRef("workspace://SpacesStore/" + args.nodeRef);
var contentNode = search.findNode(contentNodeRef);

var previewCandidates = companyhome.childrenByXPath("/app:company_home/app:dictionary/ph:previews/*[@cm:name='" + contentNode.id + ".swf']");

if (previewCandidates.length != 0) {
    model.previewNode = "workspace://SpacesStore/"+previewCandidates[0].nodeRef.id;
    model.code="OK";
}
else {
    model.previewNode = "";
    model.code="KO";
}