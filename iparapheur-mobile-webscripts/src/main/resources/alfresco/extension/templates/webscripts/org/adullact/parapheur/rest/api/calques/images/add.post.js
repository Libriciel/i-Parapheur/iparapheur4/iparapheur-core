<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "fichierImage": {"typeOf":"string"},
    "nomImage": {"typeOf": "string"},
    "coordonneeX": {"typeOf":"string"},
    "coordonneeY": {"typeOf":"string"},
    "page": {"typeOf":"string"},
    "postSignature": {"typeOf":"boolean"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {

        var id = url.templateArgs['id'];

        var noderef = calqueService.addImage(id, jsonData.nomImage, jsonData.fichierImage);
        calqueService.addCoordonnees(noderef, jsonData.coordonneeX, jsonData.coordonneeY, jsonData.page, jsonData.postSignature);


        var nodeCalque = search.findNode(noderef);
        model.idImg = nodeCalque.id;
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}