importClass(java.util.HashMap);

var jsonData = JSON.parse(requestbody.content);

var type = jsonData.type;
var parameters = new HashMap();

parameters.active = "true";


parameters.collectivite = "" + jsonData.collectivite;
parameters.parapheur = "" + jsonData.parapheur;
parameters.pPolicyIdentifierID = "" + jsonData.pPolicyIdentifierID;
parameters.pPolicyIdentifierDescription = "" + jsonData.pPolicyIdentifierDescription;
parameters.pPolicyDigest = "" + jsonData.pPolicyDigest;
parameters.pSPURI = "" + jsonData.pSPURI;
parameters.pCity = "" + jsonData.pCity;
parameters.pPostalCode = "" + jsonData.pPostalCode;
parameters.pCountryName = "" + jsonData.pCountryName;
parameters.pClaimedRole = "" + jsonData.pClaimedRole;
// Impacts 4.7
var sigPostalCode = "" + jsonData.pPostalCode;
var sigLocation = "" + jsonData.pCity;

s2lowConfig.setHeliosParametersForType(type, parameters);

// activer la surcharge au niveau du type
var typesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiertype/cm:TypesMetier.xml")[0];
var rootxml = new XML(typesnode.content);

for (var numero in rootxml.MetierType) {
    if (rootxml.MetierType[numero].ID.toString() == type) {
        // récupère les informations du type
        var newID = rootxml.MetierType[numero].ID.toString();
        var newDesc = rootxml.MetierType[numero].Desc.toString();
        var newTdTNom = rootxml.MetierType[numero].TdT.Nom.toString();
        var newTdTProtocole = rootxml.MetierType[numero].TdT.Protocole.toString();
        var overrideActive = "true";
        var newTdTUriConfiguration =  rootxml.MetierType[numero].TdT.UriConfiguration.toString();
        var sigFormat = rootxml.MetierType[numero].sigFormat.toString();
    }
}


var newrootxml = new XML();
    newrootxml = <MetierTypes></MetierTypes>;
    for (var numero in rootxml.MetierType) {
	if (rootxml.MetierType[numero].ID.toString() != type) {
		newrootxml.MetierTypes +=  rootxml.MetierType[numero];
	} else {
            var str = new String("<MetierType><ID>"+newID+"</ID><Desc><![CDATA["+newDesc+"]]></Desc><TdT><Nom>"+newTdTNom+"</Nom><Protocole>"+newTdTProtocole+"</Protocole><override>"+overrideActive+"</override><UriConfiguration>"+newTdTUriConfiguration+"</UriConfiguration></TdT>" +
                    "<sigFormat>" + sigFormat + "</sigFormat>" +
                    "<sigLocation>" + sigLocation + "</sigLocation>" +
                    "<sigPostalCode>" + sigPostalCode + "</sigPostalCode>" +
                    "</MetierType>");
            var newtype = new XML(str);
            newrootxml.MetierTypes += newtype;
	}
    }
    typesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + newrootxml.toXMLString();
    typesnode.save();
