<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "cert": {"typeOf":"string"},
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

model.ret = "";

if (validate(jsonData, rules)) {
    model.ret = usersService.getTicket(jsonData.cert);
}

