<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "dossiers": {"typeOf":"array","isValid":is_valid_nodeRef()},
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    var dossiers = jsonData.dossiers;

    // dossiers NativeArray
    model.ret = dossierService.getSignatureInformations(dossiers);

    model.code = "OK";
}
else {
    model.code = "KO";
}

