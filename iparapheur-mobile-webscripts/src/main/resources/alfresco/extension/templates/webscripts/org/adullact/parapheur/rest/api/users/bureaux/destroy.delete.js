<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE
//VALID

function is_valid_uuid(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var userId = url.templateArgs['userId'];
var bureauId = url.templateArgs['bureauId'];
var isProprietaire = args['isProprietaire'] === "true";

//Test de la validité des id
if(is_valid_uuid(userId) && is_valid_uuid(bureauId)) {
    try {
        // On enleve l'utilisateur du bureau
        usersService.removeUserFromBureau(userId, bureauId, isProprietaire);
    } catch(e) {
        setStatusMessageCode("Erreur lors de la dissociation de l'utilisateur du bureau : " + e, 500);
    }
} else {
    setStatusMessageCode("Des mauvais identifiants ont été renseignés. Utilisateur : " + userId + ", bureau : " + bureauId, 403);
}
