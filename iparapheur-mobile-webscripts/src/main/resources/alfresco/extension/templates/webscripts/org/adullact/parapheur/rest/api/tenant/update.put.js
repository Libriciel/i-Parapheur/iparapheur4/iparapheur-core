<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

importPackage(Packages.java.util);
importClass(Packages.org.adullact.iparapheur.repo.Collectivite);

//Rules
var rules = {
    "title" : {
        "typeOf":"string"
    },
    "description" : {
        "typeOf":"string"
    }
};

var jsonData = JSON.parse(requestbody.content);
if(validate(jsonData, rules)) {
    var id = url.templateArgs['id'];
    var c = new Collectivite(id);
    c.setTitle(jsonData.title);
    c.setDescription(jsonData.description);
    c.setSiren(jsonData.siren);
    c.setCity(jsonData.city);
    c.setPostalCode(jsonData.postalCode);
    c.setCountry(jsonData.country);

    multiCService.updateCollectivite(c);
}

