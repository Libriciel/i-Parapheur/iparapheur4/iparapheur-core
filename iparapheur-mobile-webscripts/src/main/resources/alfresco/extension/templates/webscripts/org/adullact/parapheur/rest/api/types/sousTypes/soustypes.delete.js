var id = url.templateArgs['id'];
var sousTypeId = url.templateArgs['sousType'];
var username = usersService.getCurrentUsername();

var sstypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ id +".xml']")[0];
if (sstypesnode == null) {
    throw new Error("Les types techniques n'ont pas été initialisés.");
}

var rootxml = new XML(sstypesnode.content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "")); // Workaround: Mozilla Bug 336551
var newrootxml = new XML();
newrootxml = <MetierSousTypes></MetierSousTypes>;

for (var numero in rootxml.MetierSousType) {
    if (rootxml.MetierSousType[numero].ID.toString() !== sousTypeId) {
        newrootxml.MetierSousTypes +=  rootxml.MetierSousType[numero];
    }  else {
        workflowService.isNotUsedBy(rootxml.MetierSousType[numero].Circuit.toString(), id, sousTypeId);
    }
}

sstypesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + newrootxml.toXMLString();
sstypesnode.save();