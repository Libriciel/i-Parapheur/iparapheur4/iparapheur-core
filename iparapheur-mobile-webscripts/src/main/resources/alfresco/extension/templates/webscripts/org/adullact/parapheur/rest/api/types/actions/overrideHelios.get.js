//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Récupération de l'id du type
var id = url.templateArgs['id'];
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var parameters = s2lowConfig.getHeliosParametersForType(id);

    ret.active = parameters.active;

    if (ret.active == undefined) {
        ret.active = 'true';
    }

    ret.name = parameters.name;
    ret.server = parameters.server;
    ret.port = parameters.port;
    ret.password = parameters.password;
    ret.userlogin = (parameters.userlogin == null)? "" : parameters.userlogin;
    ret.userpassword = (parameters.userpassword == null)? "" : parameters.userpassword;
    ret.baseUrlArchivage = parameters.baseUrlArchivage;

    ret.isPwdGoodForPkcs = parameters.isPwdGoodForPkcs;
    if (parameters.isPwdGoodForPkcs == "ok") {
        ret.dateLimite = parameters.dateLimite;
        if (ret.active == 'true') {
            if (iparapheur.isCertificateAbleToConnectToS2low("ACTES", id)) {
                ret.validCertCnx = "ok";

                ret.listeLogins = iparapheur.getArrayLoginForType("ACTES", id);
                if (iparapheur.isConnectionOK("ACTES", id)) {
                    ret.validLoginAndCertCnx = "ok";
                } else {
                    ret.validLoginAndCertCnx = "ko";
                }
            } else {
                ret.validCertCnx = "ko";
            }
        }
    } else {
        ret.dateLimite = "";
    }

    ret.collectivite = parameters.collectivite;
    ret.parapheur = parameters.parapheur;
    ret.pPolicyIdentifierID = parameters.pPolicyIdentifierID;
    ret.pPolicyIdentifierDescription = parameters.pPolicyIdentifierDescription;
    ret.pPolicyDigest = parameters.pPolicyDigest;
    ret.pSPURI = parameters.pSPURI;
    ret.pCountryName = parameters.pCountryName;
    ret.pClaimedRole = parameters.pClaimedRole;
    
    model.mjson = JSON.stringify(ret);

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
