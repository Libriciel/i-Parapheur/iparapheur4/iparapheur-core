importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

/* get the user */
/* Default validation rules */

var id = url.templateArgs['id'];

if(id) {
    model.mjson = usersService.getUser(id);
} else {
    status.code = 404;
    ret = {code : 404, message : "Utilisateur introuvable"};
    model.mjson = jsonUtils.toJSONString(ret);
}
