<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "password" : {
        "typeOf": "string"
    }
};

var jsonData = JSON.parse(requestbody.content);
if(validate(jsonData, rules)) {
    var id = url.templateArgs['id'];
    var c = multiCService.getCollectivite(id);

    multiCService.changeAdminPassword(c, jsonData.password.split(''));
}