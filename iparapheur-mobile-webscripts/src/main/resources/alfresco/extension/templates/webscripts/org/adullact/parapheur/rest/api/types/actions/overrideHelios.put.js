<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/base64.lib.js">

importClass(java.util.HashMap);

var t = "</></>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "cert": {"typeOf":"string", "optional": true},
    "name": {"typeOf":"string", "optional": true},
    "userlogin": {"typeOf":"string", "optional": true},
    "userpassword": {"typeOf":"string", "optional": true},
    "password": {"typeOf":"string"},
    "port" : {"typeOf":"string"},
    "server" : {"typeOf":"string"},
    "active": {"typeOf": "string"},
    "pPolicyIdentifierID": {"typeOf": "string"},
    "pPolicyIdentifierDescription": {"typeOf": "string"},
    "pPolicyDigest": {"typeOf": "string"},
    "pSPURI": {"typeOf": "string"},
    "pCountryName": {"typeOf": "string"},
    "pClaimedRole": {"typeOf": "string"},
    "collectivite": {"typeOf": "string"},
    "parapheur": {"typeOf": "string"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Récupération de l'id du type
var type = url.templateArgs['id'];
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        if(jsonData.cert !== undefined && jsonData.name !== undefined) {
            var content = jsonData.cert;
            var filename = jsonData.name;
        }
        var parameters = new HashMap();
        if (filename && filename != "" && content && content != "") {
            s2lowConfig.createCertificateFile(filename, content);

            parameters.name = "" + filename;
            parameters.password = "" + jsonData.password;
        } else {
            var oldParams = s2lowConfig.getHeliosParametersForType(type);
            parameters.name = "" + oldParams.name;
            parameters.password = "" + oldParams.password;
        }
        parameters.active = "" + jsonData.active;
        parameters.server = "" + jsonData.server;
        parameters.port = "" + jsonData.port;
        parameters.userlogin = "" + jsonData.userlogin;
        parameters.userpassword = "" + jsonData.userpassword;

        parameters.collectivite = "" + jsonData.collectivite;
        parameters.parapheur = "" + jsonData.parapheur;
        parameters.pPolicyIdentifierID = "" + jsonData.pPolicyIdentifierID;
        parameters.pPolicyIdentifierDescription = "" + jsonData.pPolicyIdentifierDescription;
        parameters.pPolicyDigest = "" + jsonData.pPolicyDigest;
        parameters.pSPURI = "" + jsonData.pSPURI;
        parameters.pCountryName = "" + jsonData.pCountryName;
        parameters.pClaimedRole = "" + jsonData.pClaimedRole;

        s2lowConfig.setHeliosParametersForType(type, parameters);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
