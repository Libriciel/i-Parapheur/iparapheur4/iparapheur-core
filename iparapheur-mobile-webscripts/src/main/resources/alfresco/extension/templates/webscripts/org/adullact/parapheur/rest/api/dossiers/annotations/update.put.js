<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.com.atolcd.parapheur.repo.annotations.Rect);
importClass(Packages.com.atolcd.parapheur.repo.annotations.Point);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

//noinspection JSUnresolvedVariable
var id = url.templateArgs['id'];
var documentId = url.templateArgs['documentId'];
var idAnnot = url.templateArgs['idAnnotation'];

var rules = {
    "id":{
        "typeOf":"string",
        "isValid":is_valid_uuid()
    },
    "text":{
        "typeOf":"string",
        "optional":"true"
    },
    "fillColor":{
        "typeOf":"string",
        "optional":"true"
    },
    "penColor":{
        "typeOf":"string",
        "optional":"true"
    },
    "rect":{
        "typeOf":"object",
        "optional":"true",
        "isValid":is_valid_rect()
    }
};


var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {

    var dossier = search.findNode("node", ["workspace", "SpacesStore", id]);
    var document = search.findNode("node", ["workspace", "SpacesStore", documentId]);
    var annotation = annotationService.getAnnotationForUUID(dossier.nodeRef, idAnnot);

    if (annotation === undefined || annotation === null) {
        setStatusMessage("Annotation inconnue");
        //return false;
    }
    else {

        if (jsonData.text !== undefined) {
            annotation.setText(jsonData.text);
        }

        if (jsonData.rect !== undefined) {
            var r = jsonData.rect;

            annotation.setRect(new Rect(new Point(r.topLeft.x, r.topLeft.y), new Point(r.bottomRight.x, r.bottomRight.y)));
        }

        if (jsonData.fillColor !== undefined) {
            //annotation.setFillColor(fillColor);
        }

        if (jsonData.penColor !== undefined) {
            //annotation.setPenColor(jsonData.annotation.penColor)
        }

        annotationService.updateAnnotation(dossier.nodeRef, document.nodeRef, annotation);
    }

}