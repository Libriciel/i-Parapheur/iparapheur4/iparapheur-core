<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(java.util.HashMap);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var parameters = attestService.getParameters();

    if (parameters != null && parameters.host != null) {
        ret.enabled = parameters.enabled;
        ret.host = parameters.host;
        ret.port = parameters.port;
        ret.username = parameters.username;
        ret.password = parameters.password;
    } else {
        ret.enabled = "false";
        ret.host = "";
        ret.port = "";
        ret.username = "";
        ret.password = "";
    }

    model.mjson = JSON.stringify(ret);

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
