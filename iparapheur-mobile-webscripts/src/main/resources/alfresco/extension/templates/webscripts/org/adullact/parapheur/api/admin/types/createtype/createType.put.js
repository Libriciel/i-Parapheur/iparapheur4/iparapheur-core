var jsonData = JSON.parse(requestbody.content);

var oldID = args.oldID;
var newID = jsonData.name;
var newDesc = jsonData.description;
var newTdTNom = jsonData.tdtName;
var newTdTProtocole = jsonData.tdtProtocol;
var overrideActive = args.overrideActive;
var newTdTUriConfiguration = args.newTdTUriConfiguration;
var sigFormat = jsonData.signature;

if (newTdTNom == "S²LOW") {
    if (newTdTProtocole == "ACTES") {
        sigFormat = "PKCS#7/single";
    } else if (newTdTProtocole == "HELIOS") {
        sigFormat = "XAdES/enveloped";
    }
}

if (newTdTNom == "FAST") {
    /*if (newTdTProtocole == "ACTES") {
        sigFormat = "PKCS#7/single";
    } else
    */if (newTdTProtocole == "HELIOS") {
        sigFormat = "XAdES/enveloped";
    }
}
// <BLEX> - voir SrciService.K.tdtName
if (newTdTNom=="SRCI") {
    if (newTdTProtocole == "ACTES") {
        sigFormat = "PKCS#7/single";
    } else if (newTdTProtocole == "HELIOS") {
        sigFormat = "XAdES/enveloped";
    }
}
// </BLEX>
var typesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiertype/cm:TypesMetier.xml")[0];
if (null == typesnode)
{
    throw new Error("Les types techniques n'ont pas été initialisés");
}

// Trim the newID manually as the trim() function is not available in Rhino.
newID = newID.replace(/^\s+/,'').replace(/\s+$/,'');
//if (!newID.match(/^([A-Za-z])([a-zA-Z0-9_\s]{0,30})[a-zA-Z0-9_]$/g)) {
if (!newID.match(/^[A-Za-z][^?\|:;&%\+£\"'\*\\\/<>]{1,31}$/g)) {
    throw new Error("Le champ type contient des caractères incorrects");
}

var rootxml = new XML(typesnode.content);

 if (oldID == null) {
	 // Creation
	 
	 /* Impacts 4.7
	  * - Ajout des attributs sigLocation et sigPostalCode. Comme BL Ready n'envoie pas ces infos à ce moment mais lors de la surcharge
	  *   de la signature, on les crées à vide car il doit y avoir l'attribut dans le fichier de conf.
	  */
	 
    var str = new String("<MetierType><ID>"+newID+"</ID><Desc><![CDATA["+newDesc+"]]></Desc><TdT><Nom>"+newTdTNom+"</Nom><Protocole>"+newTdTProtocole+"</Protocole><override>"+overrideActive+"</override><UriConfiguration>"+newTdTUriConfiguration+"</UriConfiguration></TdT>" +
                "<sigFormat>" + sigFormat + "</sigFormat>" +
                "<sigLocation></sigLocation>" +
                "<sigPostalCode></sigPostalCode>" +
                "</MetierType>");
    var newtype = new XML(str);
    rootxml.MetierTypes += newtype;
    typesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + rootxml.toXMLString();
    typesnode.save();

    // Creer le fichier de sous-types correspondant dans le bon répertoire
    var soustypeFolder = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype")[0];
    var mycontent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
<MetierSousTypes>\n\
</MetierSousTypes>\n";
    var soustypeDoc = soustypeFolder.createFile(newID + ".xml");
    soustypeDoc.content = mycontent;
    soustypeDoc.save();
 } else {
    // Mettre à jour le noeud XML
    var newrootxml = new XML();
    newrootxml = <MetierTypes></MetierTypes>;
    for (var numero in rootxml.MetierType) {
	if (rootxml.MetierType[numero].ID.toString() != oldID) {
		newrootxml.MetierTypes +=  rootxml.MetierType[numero];
	} else {
		// Modification
		
		/* 
		 * Impact 4.7
		 * - On récupère les infos code postal et ville avant de recréer le xml pour ne pas perdre les informations
		 */
		var sigPostalCode = rootxml.MetierType[numero].sigPostalCode.toString();
		var sigLocation = rootxml.MetierType[numero].sigLocation.toString();
		
            var str = new String("<MetierType><ID>"+newID+"</ID><Desc><![CDATA["+newDesc+"]]></Desc><TdT><Nom>"+newTdTNom+"</Nom><Protocole>"+newTdTProtocole+"</Protocole><override>"+overrideActive+"</override><UriConfiguration>"+newTdTUriConfiguration+"</UriConfiguration></TdT>" +
                    "<sigFormat>" + sigFormat + "</sigFormat>" +
                    "<sigLocation>" + sigLocation + "</sigLocation>" +
                    "<sigPostalCode>" + sigPostalCode + "</sigPostalCode>" +
                    "</MetierType>");
            var newtype = new XML(str);
            newrootxml.MetierTypes += newtype;
	}
    }
    typesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + newrootxml.toXMLString();
    typesnode.save();

    // si newID != oldID : ça se complique, il faut aussi modifier:
    //  - le fichier des sous-types!!
    //  - le fichier de surcharge TDT !!
    if (newID != oldID) {
	var soustypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ oldID +".xml']")[0];
	if (soustypesnode != null) {
            soustypesnode.name = newID + ".xml";
            soustypesnode.save();
	} else {
            var soustypeFolder = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype")[0];
            var mycontent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<MetierSousTypes>\n</MetierSousTypes>\n";
            var soustypeDoc = soustypeFolder.createFile(newID + ".xml");
            soustypeDoc.content = mycontent;
            soustypeDoc.save();
	}
        var overridetdt = companyhome.childrenByXPath("./app:dictionary/cm:metiertype/*[@cm:name='"+ oldID +"_s2low_properties.xml']")[0];
        if (overridetdt != null) {
            // do the stuff
            overridetdt.name = newID + "_s2low_properties.xml";
            overridetdt.save();
        }
        // pas de clause "else": si la surcharge n'existe pas on fait rien, nib, que d'alle
    }
 }
 
 model.id=newID;
