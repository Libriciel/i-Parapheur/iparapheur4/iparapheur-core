//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();

var id = url.templateArgs['id'];
var dossier = search.findNode("workspace://SpacesStore/" + id);
//Test si administrateur
if(usersService.isAdministrateur(username) || usersService.isAdminFonctionnel(username)) {
    dossierService.deleteDossierAdmin(id);
}