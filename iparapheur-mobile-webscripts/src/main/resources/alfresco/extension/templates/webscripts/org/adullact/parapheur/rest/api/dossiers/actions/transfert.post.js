<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
var t = "</>;//";

var rules = {
    "bureau": {"typeOf":"string", "isValid":is_valid_uuid()}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();

var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];
var dossier = search.findNode("node", ["workspace", "SpacesStore",  id]);
var bureau = search.findNode("node", ["workspace", "SpacesStore", jsonData.bureau]);
//Test si administrateur
if(usersService.isAdministrateur(username) || usersService.isAdminFonctionnel(username)) {
    iparapheur.moveDossier(dossier.getNodeRef(), bureau.getNodeRef());
}