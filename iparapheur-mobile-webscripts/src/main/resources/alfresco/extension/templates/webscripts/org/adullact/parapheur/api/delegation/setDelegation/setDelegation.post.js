<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var rules = {
    "bureauCourant" : {"typeOf":"string","isValid":is_valid_nodeRef()},
    "bureauCible" : {"typeOf":"string","isValid":is_valid_nodeRef()},
    "dateDebut" : {"typeOf":"string"},
    "dateFin" : {"typeOf":"string"},
    "enCours": {"typeOf":"string"}
};

var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules))
    model.ret = iparapheur.setDelegation(jsonData.bureauCourant, jsonData.bureauCible, 
        jsonData.dateDebut, jsonData.dateFin, jsonData.enCours);