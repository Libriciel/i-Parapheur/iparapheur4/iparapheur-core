var id = url.templateArgs['id'];

var c = multiCService.getCollectivite(id);

var ret = {
    tenantDomain: c.tenantDomain,
    title: c.title,
    description: c.description,
    siren: c.siren,
    city: c.city,
    postalCode: c.postalCode,
    country: c.country
};

model.mjson = jsonUtils.toJSONString(ret);