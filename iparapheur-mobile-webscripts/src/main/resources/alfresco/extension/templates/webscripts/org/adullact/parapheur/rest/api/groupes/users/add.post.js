//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
var user = url.templateArgs['user'];
var id = url.templateArgs['id'];

//Test si administrateur
if(usersService.isAdministrateur(username)) {
    var node = search.findNode("node", ["workspace", "SpacesStore", id]);

    var group = groups.getGroup(node.properties["cm:authorityName"].replace("GROUP_","")); //Little hack for administrators group

    group.addAuthority(user);
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}