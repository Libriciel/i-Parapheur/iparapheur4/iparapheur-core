<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var rules = {
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_uuid()}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];

//Test de la validité de l'id
if(valid_uuid(id) && validate(jsonData, rules)) {
    //jsonData.type = "dossier";
    jsonData.action = "MERGE_PROPERTIES";
    jsonData.id = id;
    jsonData.username = usersService.getCurrentUsername();
    var props = {};
    for(var j in jsonData.metadatas) {
        var val = jsonData.metadatas[j].value;
        if(jsonData.metadatas[j].values) {
            val = val !== "" ? val : null;
        }
        props[j] = val;
    }
    if(jsonData.acteursVariables) {
        props["ph:acteurs-variables"] = jsonData.acteursVariables;
    }
    dossierService.mergeProperties("workspace://SpacesStore/" + id, "workspace://SpacesStore/" + jsonData.bureauCourant, props);
    model.ret = "";
    //Creation du bureau et récupération de l'id pour le retour
} else {
    setStatusMessageCode("ID du dossier invalide", 400);
}