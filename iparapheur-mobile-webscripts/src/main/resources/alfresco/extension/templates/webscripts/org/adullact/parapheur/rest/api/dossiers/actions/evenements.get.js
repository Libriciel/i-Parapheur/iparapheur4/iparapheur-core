<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var id = url.templateArgs['id'];

var dossier = search.findNode("workspace://SpacesStore/" + id);
if (dossier == null) {
    model.status = "KO";
    model.msg = "Vous ne pouvez pas consulter ce dossier";
}
else {
    var logsDossier = dossierService.getLogsDossier(dossier.nodeRef);
    model.status = "OK";
    model.dossierName = dossier.properties['cm:title'];
    model.logsDossier = logsDossier;
    // Displayed only if logsDossier is empty (never happens normally)
    model.msg = "Ce dossier ne contient aucun évènement";
}