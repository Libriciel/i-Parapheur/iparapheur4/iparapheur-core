/**
 * Created by lhameury on 24/03/14
 */

var id = url.templateArgs['id'];
var node = search.findNode("node", ["workspace", "SpacesStore", id]);

var groupFound = groups.getGroup(node.properties["cm:authorityName"].replace("GROUP_","")); //Little hack for administrators group

var response = {};

if(groupFound) {
    response = {
        users: []
    };
    var users = groupFound.getChildUsers();
    for(var i = 0; i < users.length; i++) {
        var p = people.getPerson(users[i].shortName);
        response.users.push({
            fullName : p.properties.firstName + " " + p.properties.lastName,
            shortName : users[i].shortName
        })
    }
}

model.mjson = jsonUtils.toJSONString(response);