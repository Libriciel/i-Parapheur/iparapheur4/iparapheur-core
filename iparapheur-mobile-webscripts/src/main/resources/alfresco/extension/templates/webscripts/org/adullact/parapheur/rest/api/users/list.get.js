<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
/**
 * Created by jmaire on 14/08/13.
 */
var toSearch = args.search;

var username = usersService.getCurrentUsername();
if(usersService.isAdministrateur(username) || usersService.isAdminFonctionnel(username)) {
    model.mjson = usersService.getUsers(toSearch);
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}

