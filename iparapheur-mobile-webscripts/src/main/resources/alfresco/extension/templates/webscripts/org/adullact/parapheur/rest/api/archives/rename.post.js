<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Récupération du contenu JSON de la requete
var jsonData = JSON.parse(requestbody.content);
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    var id = url.templateArgs['id'];
    //Test de la validité de l'id
    if (valid_uuid(id)) {
        var n = search.findNode("workspace://SpacesStore/" + id);
        n.properties["cm:title"] = jsonData.title;
        n.save();
    } else {
        setStatusMessageCode("ID du dossier invalide", 400);
    }
}