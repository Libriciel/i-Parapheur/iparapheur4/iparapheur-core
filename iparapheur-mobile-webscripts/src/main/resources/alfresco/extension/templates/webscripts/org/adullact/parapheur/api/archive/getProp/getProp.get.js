importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

function main() {
    /**
     * Fournir le fichier de signature PKCS#7
     */
    var node = search.findNode("node", ["workspace", "SpacesStore", args.dossier.substring(24)]); // without "workspace://SpacesStore/"

    // 404 si recherche infructueuse
    if (node == null) {
        status.setCode(status.STATUS_NOT_FOUND, "The dossier source node could not be found");
        return;
    } else {
        var property = args.property;
        if (property == null) {
            status.setCode(status.STATUS_NOT_FOUND, "Property argument could not be found");
            return;
        }

        model.contentNode = node;
        model.contentFile = node.properties[property];
        model.base64Property = property;

        var filename = args.filename;

        if (property == "ph:sig") {
            model.filename = filename ? filename + ".zip" : "signature.zip";
        } else if (property == "ph:original") {
            model.filename = node.properties["ph:originalName"];
        } else {
            while (node.properties["ph:sigFormat"] == null) {
                node = node.parent;
            }
            if (node.properties["ph:sigFormat"] === "XAdES/detached") {
                model.filename = filename ? filename + ".xml" : "signature.xml";
            } else {
                model.filename = filename ? filename + ".p7s" : "signature.p7s";
            }
        }
    }
}

main();
