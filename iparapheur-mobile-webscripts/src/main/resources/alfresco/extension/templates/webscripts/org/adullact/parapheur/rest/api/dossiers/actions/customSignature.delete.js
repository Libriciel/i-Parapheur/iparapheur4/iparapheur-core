<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//"; //Closed tag required in JS for validation in IntelliJ IDEA


// Checking data sent

var dossierId = url.templateArgs['dossierId'];
var documentId = url.templateArgs['documentId'];

// Checks and result

if (!valid_uuid(dossierId)) {
    setStatusMessageCode("1002", 400);
}
else if (!valid_uuid(documentId)) {
    setStatusMessageCode("1003", 400);
}
else {
    var dossier = search.findNode("workspace://SpacesStore/" + dossierId);
    var document= search.findNode("workspace://SpacesStore/" + documentId);
    model.ret = dossierService.deleteCustomSignatureFromDocument(dossier.nodeRef, document.nodeRef);
}
