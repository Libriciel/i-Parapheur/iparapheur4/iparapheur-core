<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

importClass(Packages.java.util.HashMap);

var jsonData = JSON.parse(requestbody.content);

var id = url.templateArgs['id'];

var pes = new HashMap();
pes.policyIdentifierID = jsonData.policyIdentifierID;
pes.policyIdentifierDescription = jsonData.policyIdentifierDescription;
pes.policyDigest = jsonData.policyDigest;
pes.spuri = jsonData.spuri;

var c = multiCService.getCollectivite(id);

multiCService.setInfosPES(c, pes);