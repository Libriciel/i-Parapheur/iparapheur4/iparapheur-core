<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "name": {"typeOf":"string"},
    "content" : {"typeOf":"string"},
    "visualname" : {"typeOf":"string"},
    "visualcontent" : {"typeOf":"string"},
    "isMainDocument": {"typeOf": "boolean"},
    "reloadMainDocument" : {"typeOf":"boolean"}
};

var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];


if (!valid_uuid(id)) {
    setStatusMessageCode("1001", 400);
}
else if (!validate(jsonData, rules)) {
    setStatusMessageCode("Invalid data sent, expecting : \n" + jsonUtils.toJSONString(rules), 400);
}
else {
    var dossier = search.findNode("workspace://SpacesStore/" + id);
    var result = jsonUtils.toObject(dossierService.addDocument(dossier.nodeRef, jsonData.name, jsonData.content, jsonData.visualname, jsonData.visualcontent, jsonData.isMainDocument, jsonData.reloadMainDocument));

    if (result['statusCode'] == 200) {
        model.ret = jsonUtils.toJSONString(result);
    }
    else if (result['statusCode'] == 400){
        setStatusMessageCode(result['errorMessage'], result['statusCode']);
    }
    else {
        setStatusMessageCode("999", 400);
    }
}
