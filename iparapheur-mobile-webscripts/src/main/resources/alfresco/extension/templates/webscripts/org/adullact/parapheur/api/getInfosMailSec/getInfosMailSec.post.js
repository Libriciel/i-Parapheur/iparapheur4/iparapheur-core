<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "dossier": {"typeOf":"string","isValid":is_valid_nodeRef()}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.ret = dossierService.getInfosMailSec(jsonData.dossier);
}

