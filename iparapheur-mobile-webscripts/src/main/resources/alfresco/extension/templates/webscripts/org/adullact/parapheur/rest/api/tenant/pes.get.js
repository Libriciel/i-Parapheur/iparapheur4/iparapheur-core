var id = url.templateArgs['id'];

var c = multiCService.getCollectivite(id);

var p = multiCService.getInfosPES(c);

var ret = {
    policyIdentifierID: p.policyIdentifierID,
    policyIdentifierDescription: p.policyIdentifierDescription,
    policyDigest: p.policyDigest,
    spuri: p.spuri
};

model.mjson = jsonUtils.toJSONString(ret);