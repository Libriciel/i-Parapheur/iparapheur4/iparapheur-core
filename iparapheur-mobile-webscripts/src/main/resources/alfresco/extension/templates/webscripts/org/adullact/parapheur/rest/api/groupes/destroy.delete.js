var id = url.templateArgs['id'];
var node = search.findNode("node", ["workspace", "SpacesStore", id]);

var name = node.properties["cm:authorityName"].replace("GROUP_","");

var group = groups.getGroup(name); //Little hack for administrators group

if(group != null && name !== "ALFRESCO_ADMINISTRATORS" && name !== "GESTIONNAIRE_CIRCUITS_IPARAPHEUR") {
    group.deleteGroup();
}