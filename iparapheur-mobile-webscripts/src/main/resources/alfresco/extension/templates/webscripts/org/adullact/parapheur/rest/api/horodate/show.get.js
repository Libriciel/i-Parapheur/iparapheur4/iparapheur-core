<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var parameters = horodateConfig.getHorodatageParameters();

    if (parameters != null && parameters.name!=null) {
        ret.name = parameters.name;
        ret.server = parameters.server;
        ret.port = parameters.port;
        ret.algorithme = parameters.algorithme;
        ret.idclient = parameters.idclient;
    } else {
        ret.name = "_off_";
        ret.server = "";
        ret.port = "";
        ret.algorithme = "SHA1";
        ret.idclient = "";
    }

    model.mjson = JSON.stringify(ret);

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
