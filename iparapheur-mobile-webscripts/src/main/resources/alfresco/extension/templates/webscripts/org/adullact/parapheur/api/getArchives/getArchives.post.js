importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);
importPackage(Packages.java.util);

function extractArchiveData(archive) {
    return {
        "created": archive.properties["cm:created"],
        "creator": archive.properties["cm:creator"],
        "name": archive.properties["cm:title"],
        "nodeRef": archive.nodeRef.getId(),
        "original": (archive.properties["ph:original"] != null && archive.properties["ph:original"].size != 0) ? "true" : "false",
        "sig": (archive.properties["ph:sig"] != null && archive.properties["ph:sig"].size != 0) ? "true" : "false",
        "type": archive.properties["ph:typeMetier"],
        "sousType": archive.properties["ph:soustypeMetier"]
    };
}

//noinspection JSUnresolvedVariable
var jsonData = jsonUtils.toObject(requestbody.content);

var filters = jsonData.filters;

if (filters == undefined) {
    filters = null;
}
else if (filters.length == 0) {
    filters = null
}

var index = 0;
var pageSize = 10;
var currentPage = 0;
var propSort = "cm:title";
var asc = "true";

var skipped = jsonData.skipped !== undefined ? jsonData.skipped : 0;

if (jsonData.pageSize != undefined) {
    pageSize = jsonData.pageSize;
}

if (jsonData.page != undefined) {
    index = pageSize * jsonData.page + skipped;
    currentPage = jsonData.page;
}

if (jsonData.propSort != undefined) {
    propSort = jsonData.propSort;
}

if (jsonData.asc != undefined) {
    asc = jsonData.asc;
}

var nodeRefList;

var searchResult = [];
var reste = 0;
var newPage = 0;

do {
    nodeRefList = dossierService.getArchives(pageSize, index + (newPage * pageSize), filters, propSort, asc, true);
    if (nodeRefList != null) {
        reste = nodeRefList.size();
        var toTest = Math.min(nodeRefList.size(), pageSize);
        for (var i = 0; i < toTest; i++) {
            var archive = search.findNode(nodeRefList.get(i));
            reste--;
            if (archive != null) {
                searchResult.push(extractArchiveData(archive));
                if (searchResult.length === pageSize) {
                    break;
                }
            } else {
                skipped++;
            }
        }
    }
    newPage++;
} while (searchResult.length < pageSize && nodeRefList != null)

model.data = {"total": searchResult.length + (reste > 0 ? 1 : 0), "page": currentPage, "archives": searchResult, "skipped": skipped};
        