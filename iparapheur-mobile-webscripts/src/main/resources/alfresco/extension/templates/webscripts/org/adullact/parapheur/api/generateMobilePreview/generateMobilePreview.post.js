<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "document": {"typeOf":"string","isValid":is_valid_nodeRef()}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);
if (validate(jsonData, rules)) {
    var node=search.findNode(jsonData.document);
    if (node !==null)
        model.ret = iparapheur.generateMobilePreview(node.nodeRef);
    else
        model.ret = false;
}