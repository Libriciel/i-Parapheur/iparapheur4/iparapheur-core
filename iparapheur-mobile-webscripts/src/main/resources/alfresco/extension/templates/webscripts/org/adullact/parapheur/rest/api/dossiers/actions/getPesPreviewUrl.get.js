<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA


var id = url.templateArgs['id'];
var idDocu = url.templateArgs['document'];

if (is_valid_uuid(idDocu)) {
    var document =  search.findNode("node", ["workspace", "SpacesStore", idDocu]);

    model.location = dossierService.getPesPreviewUrl(document.nodeRef);
}