<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_uuid()}
};


var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];


if (!valid_uuid(id)) {
    setStatusMessageCode("Invalid Bureau node", 400);
}
else if (!validate(jsonData, rules)) {
    setStatusMessageCode("Invalid data sent, expecting : \n" + jsonUtils.toJSONString(rules), 400);
}
else {
    var dossier = search.findNode("workspace://SpacesStore/" + id);
    var bureauCourant = search.findNode("workspace://SpacesStore/" + jsonData.bureauCourant);
    var result = jsonUtils.toObject(dossierService.finalizeCreateDossier(dossier.nodeRef, bureauCourant.nodeRef));

    if (result['statusCode'] == 200) {
        model.ret = jsonUtils.toJSONString(result);
    }
    else {
        setStatusMessageCode(result['errorMessage'], result['statusCode']);
    }
}