<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var parameters = s2lowConfig.getActesParameters();

    ret.active = parameters.active;

    if (ret.active == undefined) {
        ret.active = 'true';
    }

    ret.name = parameters.name;
    ret.server = parameters.server;
    ret.port = parameters.port;
    ret.password = parameters.password;
    ret.userlogin = (parameters.userlogin == null)? "" : parameters.userlogin;
    ret.userpassword = (parameters.userpassword == null)? "" : parameters.userpassword;
    ret.baseUrlArchivage = parameters.baseUrlArchivage;

    ret.isPwdGoodForPkcs = parameters.isPwdGoodForPkcs;
    ret.dateLimite = parameters.dateLimite;
    if (parameters.isPwdGoodForPkcs == "ok") {
        if (ret.active == 'true') {
            if (iparapheur.isCertificateAbleToConnectToS2low("ACTES", null)) {
                ret.validCertCnx = "ok";

                ret.listeLogins = iparapheur.getArrayLoginForType("ACTES", null);
                if (iparapheur.isConnectionOK("ACTES", null)) {
                    ret.validLoginAndCertCnx = "ok";
                } else {
                    ret.validLoginAndCertCnx = "ko";
                }
            } else {
                ret.validCertCnx = "ko";
            }
        }
    }
    model.mjson = JSON.stringify(ret);


} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}

