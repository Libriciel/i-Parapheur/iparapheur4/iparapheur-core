// Recensement des emails des utilisateurs
// Résultat dans workspace://SpacesStore/company_home/<RESULT_DATA_FILE>

importClass(Packages.org.alfresco.repo.security.authentication.AuthenticationUtil);
importClass(Packages.java.lang.RuntimeException);

const RESULT_DATA_FILE = "sharejs_users_email.csv";

var runnable = { doWork : tenantUsersEmail };
var logData = "";
var nbOkTot = 0;
var nbNokTot = 0;
var nbTot = 0;
var error = "";


function logDataAdd(user, email) {
  logData += user + "," + email + "\n";
}

function logDataAddHeader() {
  logDataAdd("user", "email");
}

function tenantUsersEmail() {
  //var gens = people.getPeople(null);
  // Source : http://forums.alfresco.com/forum/developer-discussions/web-scripts/hos-get-full-list-users-their-belonging-groups-08052009-1430
  var persons = search.luceneSearch("TYPE:\"{http://www.alfresco.org/model/content/1.0}person\"");
  for (var i = 0; i < persons.length; i++) {
    var userName = persons[i].properties["cm:userName"];
    var userEmail = persons[i].properties["cm:email"]
    logDataAdd(userName, userEmail);
  }
}

function runAs(runAsWork, user) {
  var originalFullAuthentication = AuthenticationUtil.getFullAuthentication();
  try {
    AuthenticationUtil.setRunAsUser(user);
    runAsWork.doWork();
  } catch(ex) {
    error = error + "\n" + ("    " + ex.toString());
  } finally {
    AuthenticationUtil.setFullAuthentication(originalFullAuthentication);
  }
}

function runTenantsRange(indexFirst, indexLast, runAsWork) {
  // Accès au scriptable "MultiCServiceScriptable".
  // Il faut être loggué "admin multi-collectivités"
  // Les index sont 0-based.
  nbOkTot = 0;
  nbNokTot = 0;
  nbTot = 0;
  
  logDataAddHeader();
  // Boucle sur les tenants
  var tenants = multiCService.collectivites;
  var count = tenants.size();
  if ((indexLast < 0) || (indexLast >= count)) {
    indexLast = count - 1;
  }
  logger.log("Tenants (count=" + count + ") : " + indexFirst + " à " + indexLast);
  for (var i = indexFirst; (i <= indexLast) && (i < count); i++) {
    var tenant = tenants.get(i);    
    var tenantDomain = tenant.tenantDomain;
    var tenantLog = "Tenant " + i + "/" + (count-1) + " : " + tenantDomain;
    if (tenant.isEnabled()) {
      var userAdmin = "admin@" + tenantDomain;
      //logger.log(tenantLog + ", administrateur : " + userAdmin);
      runAs(runAsWork, userAdmin);
    } else {
     error = error + "\n" + (tenantLog + ", inactif");
    }
  }
  logger.log("Total = OK=" + nbOkTot + "; NOK=" + nbNokTot + "; total=" + nbTot);
}

function runAllTenants(runAsWork) {
  // Il faut être loggué "admin multi-collectivités"
  runTenantsRange(0, -1, runAsWork)
}

runAllTenants(runnable);
//var tenantMin = 11;
//var tenantMax = 20;
//runTenantsRange(tenantMin, tenantMax, runnable);

var workspace = companyhome;
var dataFile = workspace.childByNamePath(RESULT_DATA_FILE);
if (dataFile != null) {
  dataFile.remove();
} 
dataFile = workspace.createFile(RESULT_DATA_FILE);
dataFile.content = logData;

model.outputFile("Data enregistrées : " + dataFile.url);
model.errors(errors);

