importClass(Packages.org.adullact.iparapheur.repo.Collectivite);
var collectiviteName = args.tenantDomain;

var collectivite = multiCService.getCollectivite(collectiviteName);

model.collectivite = collectivite;