// Il faut être loggué "admin multi-collectivités"
// Résultat dans workspace://SpacesStore/company_home/<RESULT_DATA_FILE>

importClass(Packages.org.alfresco.repo.security.authentication.AuthenticationUtil);
importClass(Packages.java.lang.RuntimeException);

const RESULT_DATA_FILE = "sharejs_archives_count.csv";

var runnable = { doWork : countArchives };
var logData = "";
var nbOkTot = 0;
var nbNokTot = 0;
var to = "";
var from = "";

function logDataAdd(logdate, userName, dossiersCount) {
  var log = userName + "\t" + dossiersCount;
  logDataLog(logdate, log);
}

function logDataAddHeader() {
  logDataAdd("logdate", "userName", "dossiersCount");
}

function logDataLog(logdate, message) {
  logData += logdate + "\t" + message + "\n";
}

function countArchives(){
  //var nodes = search.luceneSearch("+PATH:\"/app:company_home/ph:archives/*\"");
  var nodes = search.luceneSearch("+PATH:\"/app:company_home/ph:archives/*\" +TYPE:\"ph:archive\" AND @cm\\:created:[" + from + " TO " + to + "] ");
  var userName = AuthenticationUtil.getRunAsUser();
  var dossiersCount = nodes.length;
  nbOkTot += dossiersCount;
  logDataAdd(new Date(), userName, dossiersCount);
}

function runAs(runAsWork, user) {
  var originalFullAuthentication = AuthenticationUtil.getFullAuthentication();
  try {
    AuthenticationUtil.setRunAsUser(user);
    runAsWork.doWork();
  } catch(ex) {
    logDataLog(ex.toString());
  } finally {
    AuthenticationUtil.setFullAuthentication(originalFullAuthentication);
  }
}

function runTenant(tenant, runAsWork) {
  // Accès au scriptable "MultiCServiceScriptable".
  // Il faut être loggué "admin multi-collectivités"
  // Les index sont 0-based.
  nbOkTot = 0;
  nbNokTot = 0;
  
  logDataAddHeader();
  // Boucle sur les tenants   
  var tenantDomain = tenant;
  var tenantLog = "Tenant " + tenant + " : " + tenantDomain;
  var userAdmin = "admin@" + tenantDomain;
  logger.log(tenantLog + ", administrateur : " + userAdmin);
  runAs(runAsWork, userAdmin);
  
}

var workspace = companyhome;
var dataFile = workspace.childByNamePath(RESULT_DATA_FILE);
if (dataFile != null) {
  dataFile.remove();
} 

if(args.from != null && args.to != null && args.tenant != null){
    from = args.from;
    to = args.to;

    runTenant(args.tenant, runnable);

    dataFile = workspace.createFile(RESULT_DATA_FILE);
    dataFile.content = logData;

    model.inputString = from + "-" + to;

    model.outputFile = dataFile.url;
    model.archives = nbOkTot;
}