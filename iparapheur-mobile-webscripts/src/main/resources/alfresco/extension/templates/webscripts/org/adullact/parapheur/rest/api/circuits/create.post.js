<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

function is_valid_etape() {
    return function (value) {
        var etapeRules = {
            "etape" : {
                "typeOf":"object",
                "childRules": {
                    "actionDemandee" : {"typeOf": "string"},
                    "transition" : {"typeOf": "string"},
                    "listeNotification" : {"typeOf": "array"},
                    "listeMetadatas": {"typeOf": "array"},
                    "listeMetadatasRefus": {"typeOf": "array"},
                    "parapheur" : {"typeOf": "string", "isValid" : is_valid_uuid(), "optional" : true}
                }
            }
        };

        return validate({"etape":value}, etapeRules);
    };

}

var rules = {
    "aclGroupes": {"typeOf":"array"},
    "aclParapheurs": {"typeOf":"array","isValid":is_valid_uuid()},
    "etapes" : {
        "typeOf":"array",
        "isValid":is_valid_etape()
    },
    "isPublic" : {"typeOf":"boolean"},
    "name" : {"typeOf":"string"}
};

var ret = {};
model.idWorkflow = "";

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username) || usersService.isGestionnaireCircuit(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        try {
            model.idWorkflow = workflowService.createWorkflow(jsonData.name, jsonData.etapes, jsonData.aclParapheurs, jsonData.aclGroupes, jsonData.isPublic);
        } catch (e) {
            setStatusMessageCode("Erreur lors de la création du circuit : " + e, 500);
        }
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
