<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "dossier":{"typeOf":"string", "isValid":is_valid_nodeRef()}
}


var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.nodeRef = jsonData.dossier;

    var node = search.findNode("node", ["workspace", "SpacesStore", jsonData.dossier.substring(24)]);

    if (iparapheur.isDossier(node.nodeRef)) {
        model.noderef = node.nodeRef;
        model.circuit = iparapheur.getCircuit(node.nodeRef);

    }
    else {
        setStatusMessage("Le noeud: "+jsonData.dossier+ " n'est pas un dossier");
    }
}
