<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "dossiers": {"typeOf":"array","isValid":is_valid_nodeRef()},
    "annotPub": {"typeOf":"string"},
    "annotPriv": {"typeOf":"string"},
    "bureauCourant": {"typeOf":"string"}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    var dossiers = jsonData.dossiers;
    var annotPub = jsonData.annotPub;
    var annotPriv = jsonData.annotPriv;
    var bureauCourant = jsonData.bureauCourant;

    // dossiers NativeArray
    model.ret = dossierService.rejectDossier(dossiers, annotPub, annotPriv, bureauCourant);

    model.code = "OK";
}
else
    model.code = "KO";

