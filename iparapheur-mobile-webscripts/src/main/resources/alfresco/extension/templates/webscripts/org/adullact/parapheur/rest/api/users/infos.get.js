

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
var user = people.getPerson(username);

var data = {
    id: user.id,
    username: username,
    isAdmin : usersService.isAdministrateur(username),
    administres: usersService.getBureauxAdministres(user.id),
    isGestionnaire: usersService.isGestionnaireCircuit(username)
};

model.mjson = jsonUtils.toJSONString(data);