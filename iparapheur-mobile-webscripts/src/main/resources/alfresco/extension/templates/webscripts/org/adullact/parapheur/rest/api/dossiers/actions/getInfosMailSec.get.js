function is_valid_uuid(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var id = url.templateArgs['id'];
//Test de la validité de l'id
if(is_valid_uuid(id)) {
    var dossier = search.findNode("workspace://SpacesStore/" + id);
    model.ret = dossierService.getInfosMailSec(dossier.nodeRef);
}

