<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

//noinspection JSUnresolvedVariable
var jsonData = {};
var bureauCourant = args.bureauCourant;
var id = url.templateArgs['id'];

//Test de la validité de l'id
if(valid_uuid(id) && valid_uuid(bureauCourant)) {
    jsonData.type = "dossier";
    jsonData.action = "SUPPRESSION";
    jsonData.id = id;
    jsonData.bureauCourant = bureauCourant;
    jsonData.username = usersService.getCurrentUsername();
    //Creation du bureau et récupération de l'id pour le retour
    model.ret = worker.sendWorker(jsonUtils.toJSONString(jsonData), id);
} else {
    setStatusMessageCode("ID du dossier invalide", 400);
}