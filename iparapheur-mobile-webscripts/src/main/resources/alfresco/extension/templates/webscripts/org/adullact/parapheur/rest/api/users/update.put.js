<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE
//VALID

var properties = {
    firstName : "cm:firstName",
    lastName : "cm:lastName",
    email : "cm:email",
    metadata : "cm:organizationId"
};

//Rules
var rules = {
    "firstName" : {
        "typeOf":"string",
        "optional" : true
    },
    "lastName" : {
        "typeOf":"string",
        "optional" : true
    },
    "email" : {
        "typeOf": "string",
        "optional" : true
    },
    "metadata" : {
        "typeOf": "string",
        "optional" : true
    },
    "admin" : {
        "typeOf": "string",
        "optional" : true
    },
    "bureauxAdministres" : {
        "typeOf": "array",
        "optional" : true
    },
    "certificat" : {
        "typeOf" : "object",
        "optional" : true
    },
    "signature" : {
        "typeOf": "string",
        "optional" : true
    },
    signatureData : {
        "typeOf": "string",
        "optional" : true
    }
};


//Récupération du contenu JSON de la requete
var jsonData = JSON.parse(requestbody.content);

//Validation de la requête
if(validate(jsonData, rules)) {
    var id = url.templateArgs['id'];

    if(is_valid_uuid(id)) {
        try {
            //Mise à jour de l'utilisateur avec les propriétés envoyées
            usersService.updateUser(id, jsonData, properties);
        } catch (e) {
            setStatusMessageCode("Erreur lors de la modification de l'utilisateur : " + e, 400);
        }
    } else {
        setStatusMessageCode("Impossible de trouver l'utilisateur, ID : " + id, 404);
    }
}
