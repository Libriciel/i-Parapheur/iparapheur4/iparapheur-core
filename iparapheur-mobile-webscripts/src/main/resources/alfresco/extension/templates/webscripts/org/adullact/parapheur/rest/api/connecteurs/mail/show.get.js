<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var configFiles = companyhome.childrenByXPath("/app:company_home/app:dictionary/ph:email_listener/ph:email_listener-configuration");

    var ac = new XML(configFiles[0].properties.content.content);

    // model.parameters.enabled = ac.enabled.toString();
    ret.server = ac.server.toString();
    ret.protocole = ac.protocole.toString();
    ret.port = ac.port.toString();
    ret.folder = ac.folder.toString();
    ret.username = ac.username.toString();
    ret.password = ac.password.toString();

    ret.enabled = ac.enabled != undefined ? ac.enabled.toString() : "false";

    model.mjson = JSON.stringify(ret);

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
