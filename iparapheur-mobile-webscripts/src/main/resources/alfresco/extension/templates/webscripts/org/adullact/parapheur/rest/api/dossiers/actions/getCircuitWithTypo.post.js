<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var rules = {
    "type":{"typeOf":"string"},
    "sousType":{"typeOf":"string"},
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_uuid()}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];

//Test de la validité de l'id
if(valid_uuid(id) && validate(jsonData, rules)) {
    var dossier = search.findNode("node", ["workspace", "SpacesStore", id]);
    var etapes = null;

    var type = jsonData.type;
    var sousType = jsonData.sousType;
    var circuitRef;
    try {
        circuitRef = dossierService.getCircuitRef(dossier.nodeRef, type, sousType);
    } catch (e) {
        //OUPS, on ne peut pas, récupération du circuit sans propriétés du dossier
    }

    var circuit =  {
        "sigFormat" : iparapheur.getTypeMetierSigFormat(type),
        "isDigitalSignatureMandatory" : typesService.isDigitalSignatureMandatory(type, sousType),
        "hasSelectionScript" : typesService.hasSelectionScript(type, sousType),
        "etapes" : []
    };

    if(circuitRef !== null) {
        if (jsonData.bureauCourant == null || jsonData.bureauCourant == undefined) {
            etapes = iparapheur.loadSavedWorkflow(circuitRef).getCircuit();
        } else {
            var bureau = search.findNode("workspace://SpacesStore/" + jsonData.bureauCourant);
            try {
                etapes = iparapheur.loadSavedWorkflow(circuitRef, true, bureau.nodeRef, dossier.nodeRef).getCircuit();
            } catch(e) {
                setStatusMessageCode("circuit INCONNU pour ["
                    + type + "][" + sousType
                    + "] et les meta-donnees fournies", 400);
            }
        }
    }

    if (etapes != null) {
        for (var i = 0; i < etapes.size(); i++) {
            var etape = etapes.get(i);
            var obj = {
                "actionDemandee" : etape.actionDemandee,
                "parapheurName" : etape.parapheurName || "Emetteur"
            };
            circuit.etapes.push(obj);
        }
    }

    model.mjson = jsonUtils.toJSONString(circuit);
} else {
    setStatusMessageCode("ID du dossier invalide", 400);
}