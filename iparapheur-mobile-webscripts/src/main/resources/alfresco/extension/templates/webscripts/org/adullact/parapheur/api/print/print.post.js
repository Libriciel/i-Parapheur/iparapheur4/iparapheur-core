<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var id = url.templateArgs['id'];
var rules = {
    "attachments": {"typeOf":"array","isValid":is_valid_uuid(),"optional":"true"}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    var dossier = search.findNode("node", ["workspace", "SpacesStore", id]);
    if(jsonData.attachments) {
        model.ret = dossierService.print(dossier, jsonData.attachments, true);
    } else {
        model.ret = dossierService.print(dossier, null, true);
    }
}

