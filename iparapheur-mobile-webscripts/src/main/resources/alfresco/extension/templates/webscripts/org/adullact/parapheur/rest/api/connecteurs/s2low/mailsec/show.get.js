<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};

    var parameters = s2lowConfig.getSecureMailParameters();

    ret.name = parameters.name;
    ret.server = parameters.server;
    ret.port = parameters.port;
    ret.password = parameters.password;
    ret.userlogin = (parameters.userlogin == null)? "" : parameters.userlogin;
    ret.userpassword = (parameters.userpassword == null)? "" : parameters.userpassword;
    //model.parameters.baseUrlArchivage = parameters.baseUrlArchivage;

    ret.isPwdGoodForPkcs = parameters.isPwdGoodForPkcs;
    if (parameters.isPwdGoodForPkcs == "ok") {
        ret.dateLimite = parameters.dateLimite;
        if (iparapheur.isCertificateAbleToConnectToS2low("MAILSEC", null)) {
            ret.validCertCnx = "ok";

            ret.listeLogins = iparapheur.getArrayLoginForType("MAILSEC", null);
            if (iparapheur.isConnectionOK("MAILSEC", null)) {
                ret.validLoginAndCertCnx = "ok";
            } else {
                ret.validLoginAndCertCnx = "ko";
            }
        } else {
            ret.validCertCnx = "ko";
            }
    } else {
        ret.dateLimite = "";
    }

    model.mjson = JSON.stringify(ret);

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}