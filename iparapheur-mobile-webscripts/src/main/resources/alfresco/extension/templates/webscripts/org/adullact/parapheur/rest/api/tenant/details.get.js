var id = url.templateArgs['id'];

var c = multiCService.getCollectivite(id);
if (c.enabled) {
    var stats = multiCService.getStats(c);
    var ret = {
        people: stats.people,
        bureaux: stats.parapheurs,
        dossiers: stats.dossiers,
        dossiersAArchiver: stats.dossiersAArchiver,
        dossiersAArchiverDiskUsage: stats.dossiersAArchiverDiskUsage,
        diskUsage: stats.diskUsage
    };
}

model.mjson = jsonUtils.toJSONString(ret);