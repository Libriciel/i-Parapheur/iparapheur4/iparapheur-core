<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "bureauCourant":{"typeOf":"string", "isValid":is_valid_uuid()}
};

var ret = {};
model.idDossier = "";

//Récupération du contenu JSON de la requete
var jsonData = JSON.parse(requestbody.content);
if(validate(jsonData, rules)) {
    try {
        model.idDossier = dossierService.beginCreateDossier(jsonData.bureauCourant);
    } catch (e) {
        setStatusMessageCode("Erreur lors de la création du dossier : " + e, 500);
    }
}

