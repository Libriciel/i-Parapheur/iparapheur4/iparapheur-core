<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

function is_valid_uuid(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)  || usersService.isGestionnaireCircuit(username)) {
    var id = url.templateArgs['id'];
    var node =  search.findNode("workspace://SpacesStore/" + url.templateArgs['id']);
    //Test de la validité de l'id
    if(is_valid_uuid(id) && node) {
        try {
            //Suppression du circuit
            workflowService.deleteWorkfow(node.nodeRef);
        } catch(e) {
            setStatusMessageCode("Erreur lors de la suppression du circuit : " + e, 500);
        }
    } else {
        setStatusMessageCode("Impossible de trouver le circuit d'ID " + id, 403);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
