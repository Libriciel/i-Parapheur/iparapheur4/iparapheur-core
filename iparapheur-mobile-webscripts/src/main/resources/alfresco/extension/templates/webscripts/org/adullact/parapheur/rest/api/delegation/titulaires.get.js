//VALID

/**
 * Fonction utilitaire qui positionne un message d'erreur + code d'erreur
 *
 * @param message
 * @param code
 */
function setStatusMessageCode(message, code) {
    status.message = message;
    status.code = code;
    status.redirect = true;
}


function is_valid_uuid(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var id = url.templateArgs['id'];
//Test de la validité de l'id
if(is_valid_uuid(id)) {
    try {
        //Creation du bureau et récupération de l'id pour le retour
        model.ret = iparapheur.getTitulaires("workspace://SpacesStore/" + id);
    } catch(e) {
        setStatusMessageCode("Erreur lors de la récupération des bureaux titulaires : " + e, 500);
    }
} else {
    setStatusMessageCode("ID du bureau invalide", 400);
}