<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.com.atolcd.parapheur.repo.CustomMetadataType);
importClass(Packages.com.atolcd.parapheur.repo.CustomMetadataDef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "id": {"typeOf": "string"},
    "name": {"typeOf": "string"},
    "hasValues": {"typeOf": "boolean"},
    "isAlphaOrdered": {"typeOf": "boolean"},
    "type": {"typeOf": "string"},
    "values": {"typeOf": "array"}
};

var username = usersService.getCurrentUsername();
var jsonData = JSON.parse(requestbody.content);

if (!usersService.isAdministrateur(username)) {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
else if (!validate(jsonData, rules)) {
    setStatusMessageCode("Invalid data sent, expecting : \n" + jsonUtils.toJSONString(rules), 400);
}
else {

    var id = url.templateArgs['id'];
    var name = Packages.org.alfresco.service.namespace.QName.createQName("http://www.adullact.org/parapheur/metadata/1.0", id);
    var type = CustomMetadataType.valueOf(jsonData.type);
    var list = new Packages.java.util.ArrayList();

    for (var i = 0; i < jsonData.values.length; i++) {
        list.add(jsonData.values[i].value);

        if ((type == "URL") && (!metadataService.isUrlValid(jsonData.values[i].value))) {
            setStatusMessageCode("Invalid URL sent : " + jsonData.values[i].value, 400);
        }
    }

    var def;
    if (list.size() > 0 && jsonData.hasValues) {
        def = new CustomMetadataDef(name, jsonData.name, list, type, true, jsonData.isAlphaOrdered);
    } else {
        def = new CustomMetadataDef(name, jsonData.name, type, true);
    }
        
    metadataService.addMetadataDef(def);
}