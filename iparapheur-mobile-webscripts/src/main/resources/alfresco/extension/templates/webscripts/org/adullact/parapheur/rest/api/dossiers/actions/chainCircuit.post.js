<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var rules = {
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_uuid()},
    "type":{"typeOf":"string"},
    "sousType":{"typeOf":"string"}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];

//Test de la validité de l'id
if(valid_uuid(id) && validate(jsonData, rules)) {
    //jsonData.type = "dossier";
    jsonData.action = "ENCHAINER_CIRCUIT";
    jsonData.id = id;
    jsonData.username = usersService.getCurrentUsername();
    dossierService.chainCircuit("workspace://SpacesStore/" + id, "workspace://SpacesStore/" + jsonData.bureauCourant, jsonData.type, jsonData.sousType);
    model.ret = "";
    //Creation du bureau et récupération de l'id pour le retour
} else {
    setStatusMessageCode("ID du dossier invalide", 400);
}