<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "dossiers": {"typeOf":"array","isValid":is_valid_nodeRef()},
    "bureauCourant": {"typeOf":"string"}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules))
    model.ret = dossierService.remorseDossier(jsonData.dossiers, jsonData.bureauCourant);