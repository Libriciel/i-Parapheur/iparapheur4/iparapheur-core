<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

//noinspection JSUnresolvedVariable
var type = url.templateArgs['type'];
var sousType = url.templateArgs['sousType'];

var sigFormat = iparapheur.getTypeMetierSigFormat(type);

var circuit =  {
    "sigFormat" : iparapheur.getTypeMetierSigFormat(type),
    "protocol" : typesService.getProtocol(type),
    "isMultiDocument": typesService.isMultiDocument(type, sousType) && (sigFormat != "PKCS#7/multiple"),
    "isDigitalSignatureMandatory" : typesService.isDigitalSignatureMandatory(type, sousType),
    "hasSelectionScript" : typesService.hasSelectionScript(type, sousType),
    "etapes" : []
};

var etapes = null;
var circuitRef = iparapheur.getCircuitRef(type, sousType);

if(circuitRef !== null) {
    if (args.bureau == null || args.bureau == undefined) {
        etapes = iparapheur.loadSavedWorkflow(iparapheur.getCircuitRef(type, sousType)).getCircuit();
    } else {
        var bureau = search.findNode("workspace://SpacesStore/" + args.bureau);
        try {
            etapes = iparapheur.loadSavedWorkflow(iparapheur.getCircuitRef(type, sousType), true, bureau.nodeRef, null).getCircuit();
        } catch(e) {
            circuit = e;
        }
        
    }
}

if (etapes != null) {
    for (var i = 0; i < etapes.size(); i++) {
        var etape = etapes.get(i);
        var obj = {
            "actionDemandee" : etape.actionDemandee,
            "parapheurName" : etape.parapheurName || "Emetteur",
            "transition": etape.transition,
            "listMetadatas": etape.listeMetadatas,
            "listMetadatasRefus": etape.listeMetadatasRefus
        };
        circuit.etapes.push(obj);
    }
}

model.mjson = jsonUtils.toJSONString(circuit);
