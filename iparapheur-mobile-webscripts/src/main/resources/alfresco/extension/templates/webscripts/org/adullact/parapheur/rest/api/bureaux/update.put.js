<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE
//VALID

var properties = {
    title : "cm:title",
    description : "cm:description",
    proprietaires : "ph:proprietaires",
    secretaires : "ph:secretaires",
    "show-a-venir" : "ph:show-a-venir",
    hab_enabled : "ph:habilitations",
    hab_secretariat : "ph:hab_secretariat",
    hab_archivage : "ph:hab_archivage",
    hab_enchainement: "ph:hab_enchainement",
    hab_traiter : "ph:hab_traiter",
    hab_transmettre : "ph:hab_transmettre",
    "delegations-possibles": "ph:delegations-possibles",
    "metadatas-visibility": "ph:metadatas-visibility"
};

var assocs = {
    hierarchie : "ph:hierarchie"
};

//Rules
var rules = {
    "name" : {
        "typeOf":"string",
        "optional" : true
    },
    "title" : {
        "typeOf":"string",
        "optional" : true
    },
    "description" : {
        "typeOf":"string",
        "optional" : true
    },
    "proprietaires" : {
        "typeOf": "array",
        "optional" : true
    },
    "secretaires" : {
        "typeOf": "array",
        "optional" : true
    },
    "show-a-venir" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "hab_secretariat" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "hab_archivage" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "hab_traiter" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "hab_transmettre" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "hab_enchainement" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "delegations-possibles" : {
        "typeOf": "array",
        "optional" : true
    },
    "metadatas-visibility": {
        "typeOf": "array",
        "optional": true
    }
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username) || usersService.isAdminFonctionnel(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    //Validation de la requête
    if(validate(jsonData, rules)) {
        var id = url.templateArgs['id'];
        //Récupération du bureau
        var parapheur = search.findNode("workspace://SpacesStore/" + id);
        if(is_valid_uuid(id) && parapheur != null) {
            try {
                if(jsonData.proprietaires !== undefined) {
                    var tmp = [];
                    for(var i = 0; i < jsonData.proprietaires.length; i++) {
                        tmp.push(jsonData.proprietaires[i].username);
                    }
                    jsonData.proprietaires = tmp;
                }
                if(jsonData.secretaires !== undefined) {
                    var tmpSec = [];
                    for(var j = 0; j < jsonData.secretaires.length; j++) {
                        tmpSec.push(jsonData.secretaires[j].username);
                    }
                    jsonData.secretaires = tmpSec;
                }
                // Mise à jour du nom du noeud en amont, pour que le changement soit correctement pris en compte
                // dans l'arborescence alfresco
                if(jsonData.name) {
                    parapheur.name = jsonData.name;
                    parapheur.save();
                }
                //Mise à jour du bureau avec les propriétés envoyées
                iparapheur.updateBureau(parapheur.nodeRef, jsonData, properties, assocs);
            } catch (e) {
                setStatusMessageCode("Erreur lors de la définition des propriétés du bureau : " + e, 413);
            }
        } else {
            setStatusMessageCode("Impossible de trouver le bureau d'ID " + id, 404);
        }
    } else {
        setStatusMessageCode("Requete non valide", 400);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}