function is_valid_id(value) {
    return !!value && value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var id = url.templateArgs['id'];
var jsonData = JSON.parse(requestbody.content);

if(is_valid_id(id)) {
    var parapheur = search.findNode("workspace://SpacesStore/" + url.templateArgs['id']);
    if(parapheur != null) {
        var username = usersService.getCurrentUsername();
        if(iparapheur.isParapheurOwner(parapheur.nodeRef, username) || iparapheur.isParapheurSecretaire(parapheur.nodeRef, username) || usersService.isAdministrateur(username) || usersService.isAdminFonctionnel(username)) {
            if(is_valid_id(jsonData.idCible)) {
                iparapheur.setDelegation(
                    "workspace://SpacesStore/" + url.templateArgs['id'],
                    "workspace://SpacesStore/" + jsonData.idCible,
                    jsonData['date-debut-delegation'] ? jsonData['date-debut-delegation']:null,
                    jsonData['date-fin-delegation'] ? jsonData['date-fin-delegation']:null,
                    jsonData['deleguer-presents']
                );
            } else {
                iparapheur.supprimerDelegation("workspace://SpacesStore/" + url.templateArgs['id']);
            }
        } else {
            status.code = 403;
        }
    } else {
        status.code = 404;
    }
} else {
    status.code = 400;
}