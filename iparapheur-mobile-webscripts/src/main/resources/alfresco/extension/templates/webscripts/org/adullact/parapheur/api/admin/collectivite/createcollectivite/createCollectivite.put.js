importClass(Packages.org.adullact.iparapheur.repo.Collectivite);

var jsonData = JSON.parse(requestbody.content);

var tenantDomain = jsonData.tenantDomain;
var collectiviteAdminPassword = jsonData.adminPassword;
var collectiviteTitre = jsonData.titre;
var collectiviteDescription = jsonData.description;
var collectiviteSiren = jsonData.siren;
var collectiviteVille = jsonData.ville;
var collectiviteCodePostal = jsonData.codePostal;
var collectivitePays = jsonData.pays;

var c = new Collectivite(tenantDomain);
c.setTitle(collectiviteTitre);
c.setDescription(collectiviteDescription);
c.setSiren(collectiviteSiren);
c.setCity(collectiviteVille);
c.setPostalCode(collectiviteCodePostal);
c.setCountry(collectivitePays);

multiCService.createCollectivite(c, collectiviteAdminPassword.split(''));