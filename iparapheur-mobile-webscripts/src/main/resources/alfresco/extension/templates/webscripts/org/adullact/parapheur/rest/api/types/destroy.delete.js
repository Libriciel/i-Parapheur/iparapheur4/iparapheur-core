//VALID

var typesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiertype/cm:TypesMetier.xml")[0];

if (null == typesnode) {
    throw new Error("Les types techniques n'ont pas été initialisés.");
}

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    var id = url.templateArgs['id'];

    workflowService.removeTypeForUse(id);

    var soustypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ id +".xml']")[0];
    if (soustypesnode !=  null) {
        soustypesnode.remove();
    }
    //Test de la validité du noeud du type
    if(typesnode) {
        try {
            var content = typesnode.content;

            // Workaround: Mozilla Bug 336551
            content = content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "");

            var rootxml = new XML(content);
            var newrootxml = <MetierTypes/>;
            for (var numero in rootxml.MetierType) {
                if (rootxml.MetierType[numero].ID.toString() != id) {
                    newrootxml.MetierTypes +=  rootxml.MetierType[numero];
                }
            }
            typesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + newrootxml.toXMLString();
            typesnode.save();
        } catch(e) {
            setStatusMessageCode("Erreur lors de la suppression du type : " + e, 500);
        }
    } else {
        setStatusMessageCode("Impossible de trouver le type d'ID " + id, 403);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}