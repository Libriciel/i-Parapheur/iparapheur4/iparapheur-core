importClass(Packages.org.springframework.extensions.surf.util.Base64);

model.username = args.userName;

if (!args.force) {
    var filename = null;
    var content = null;
    var file = null;

    // locate file attributes
    for each (field in formdata.fields)
    {
        if (field.name == "file" && field.isFile)
        {
            filename = field.filename;
            content = field.content;
            file = field;
        }
    }

    // ensure mandatory file attributes have been located
    if (filename == undefined || content == undefined)
    {
        status.code = 400;
        status.message = "Uploaded file cannot be located in request";
        status.redirect = true;
    }
    else
    {
        var person = people.getPerson(args.userName);

        var idCert = km.getIdCertificat(formdata);

        otherCert = km.getCertificatesById(idCert);
        if (otherCert) {
            model.result = "already_used";
            model.newuser = args.userName;
            model.olduser = otherCert.username;
            model.idCert = idCert;
            model.cert = km.getCertificatBase64(content);
        } else if (idCert == "EMPTY") {
            /** 
             * pas de certificat uploade - en attendant mieux, cette
             * situation est "legale" pour permettre de supprimer un
             * certificat
             */
            oldCert = km.getCertificatesByUsername(args.userName);
            if (oldCert) {
                km.deleteCertificate(oldCert);
                person.properties['ph:certificat']="";
                person.properties['ph:idCertificat'] = "";
                person.save();
                km.deleteCertificate(oldCert);
                model.result = "deleted";
            } else {
                model.result = "bad_certif";
                status.code = 500;
            }
            

        } else if (idCert) {
            person.properties['ph:certificat'].write(content);
            person.properties['ph:idCertificat'] = idCert;
            person.save();

            oldCert = km.getCertificatesByUsername(args.userName);
            if (oldCert) {
                km.deleteCertificate(oldCert);
            }

            km.addCertificate(args.userName, idCert);

            model.result = "ok";
            status.code = 200;
        } else {
            model.result = "bad_certif";
            status.code = 500;
        }
    }
} else {
        
        var idCertif = args.id_cert;

        var cert = km.getCertificatesById(idCertif);

        var personn = people.getPerson(args.userName);

        personn.properties['ph:certificat'].write(km.getCertificatContent(args.cert));
        personn.properties['ph:idCertificat'] = idCertif;
        personn.save();

        /* purge de l'ancienne association de certificat */
        var oldpersonn =  people.getPerson(args.olduser);
        oldpersonn.properties['ph:certificat']="";
        oldpersonn.properties['ph:idCertificat'] = "";
        oldpersonn.save();

        km.deleteCertificate(cert);
        km.addCertificate(args.userName, idCertif);

        model.result = "ok";
        status.code = 200;
    }
