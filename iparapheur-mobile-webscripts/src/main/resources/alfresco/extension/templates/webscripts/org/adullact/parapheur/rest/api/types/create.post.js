<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "desc" : {"typeOf" : "string"},
    "tdtOverride" : {"typeOf" : "string"},
    "tdtNom" : {"typeOf" : "string"},
    "tdtProtocole" : {"typeOf" : "string"},
    "sigFormat" : {"typeOf" : "string"},
    "sigLocation": {"typeOf" : "string"},
    "sigPostalCode": {"typeOf" : "string"}
};

// </BLEX>
var typesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiertype/cm:TypesMetier.xml")[0];
if (null == typesnode)
{
    throw new Error("Les types techniques n'ont pas été initialisés");
}
var rootxml = new XML(typesnode.content);

var ret = {};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        try {
            var hasError = false;
            var id = url.templateArgs['id'];
            // Trim the newID manually as the trim() function is not available in Rhino.
            id = id.replace(/^\s+/,'').replace(/\s+$/,'');
            //if (!newID.match(/^([A-Za-z])([a-zA-Z0-9_\s]{0,30})[a-zA-Z0-9_]$/g)) {
            if (!id.match(/^[A-Za-z0-9][^?\|:;&%\+£\"'\*\\\/<>]{1,31}$/g)) {
                throw new Error("Le champ type contient des caractères incorrects");
            }

            var newTdTUriConfiguration = "";

            for (var numero in rootxml.MetierType) {
                if(id === rootxml.MetierType[numero].ID.toString()) {
                    setStatusMessageCode("Le type " + id + " existe déjà", 409);
                    hasError = true;
                }
            }
            if(!hasError) {
                var str = new String("<MetierType><ID>" + id + "</ID><Desc><![CDATA[" + jsonData.desc + "]]></Desc><TdT><Nom>" + jsonData.tdtNom + "</Nom><Protocole>" + jsonData.tdtProtocole + "</Protocole><override>" + jsonData.tdtOverride + "</override><UriConfiguration>" + newTdTUriConfiguration + "</UriConfiguration></TdT>" +
                    "<sigFormat>" + jsonData.sigFormat + "</sigFormat>" +
                    "<sigLocation>" + jsonData.sigLocation + "</sigLocation>" +
                    "<sigPostalCode>" + jsonData.sigPostalCode + "</sigPostalCode>" +
                    "</MetierType>");
                rootxml.MetierTypes += new XML(str);
                typesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + rootxml.toXMLString();
                typesnode.save();

                // Creer le fichier de sous-types correspondant dans le bon répertoire
                var soustypeFolder = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype")[0];
                var mycontent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
    <MetierSousTypes>\n\
    </MetierSousTypes>\n";
                var soustypeDoc = soustypeFolder.createFile(id + ".xml");
                soustypeDoc.content = mycontent;
                soustypeDoc.save();
            }
        } catch (e) {
            setStatusMessageCode("Erreur lors de la création du type : " + e, 408);
        }
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
