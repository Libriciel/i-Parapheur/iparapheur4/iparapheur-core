var result = {
    cancelled : false
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    var id = url.templateArgs['id'];

    result.cancelled = worker.cancelWorker(id);
}


model.mjson = JSON.stringify(result);