<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(java.util.HashMap);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "enabled": {"typeOf": "string"},
    "host": {"typeOf":"string"},
    "port" : {"typeOf":"string"},
    "username" : {"typeOf":"string"},
    "password": {"typeOf": "string"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var parameters = new HashMap();

        parameters.enabled = "" + jsonData.enabled;
        parameters.host = "" + jsonData.host;
        parameters.port = "" + jsonData.port;
        parameters.username = "" + jsonData.username;
        parameters.password = "" + jsonData.password;

        attestService.setParameters(parameters);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
