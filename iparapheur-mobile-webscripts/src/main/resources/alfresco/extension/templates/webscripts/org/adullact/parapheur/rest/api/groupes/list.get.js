/**
 * Created by lhameury on 24/03/14
 */
var groupsList = groups.getAllRootGroups();

var response = [];

var getGroup = function(group, parent) {
    var subGroups = group.getChildGroups();


    var ret = {
        id: people.getGroup(group.getFullName()).id,
        shortName : group.getShortName()
    };

    if(parent) {
        ret.parent = parent;
    }

    response.push(ret);

    for(var i = 0; i < subGroups.length; i++) {
        getGroup(subGroups[i], group.shortName);
    }

    return ret;
};

for(var i = 0; i < groupsList.length; i++) {
    getGroup(groupsList[i]);
}

model.mjson = jsonUtils.toJSONString(response);
