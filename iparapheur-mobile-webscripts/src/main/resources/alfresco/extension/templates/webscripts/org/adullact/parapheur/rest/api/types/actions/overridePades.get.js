//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Récupération de l'id du type
var id = url.templateArgs['id'];
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var parameters = s2lowConfig.getPadesParametersForType(id);

    ret.active = parameters.active;

    if (ret.active == undefined) {
        ret.active = 'true';
    }

    ret.showStamp = parameters.showStamp;
    ret.stampPage = parameters.stampPage;
    ret.stampCoordX = parameters.stampCoordX;
    ret.stampCoordY = parameters.stampCoordY;
    ret.stampHeight = parameters.stampHeight;
    ret.stampWidth = parameters.stampWidth;
    ret.stampFontSize = parameters.stampFontSize;

    model.mjson = JSON.stringify(ret);

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
