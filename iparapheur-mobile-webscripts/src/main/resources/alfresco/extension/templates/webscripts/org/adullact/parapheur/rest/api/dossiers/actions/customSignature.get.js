<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//"; //Closed tag required in JS for validation in IntelliJ IDEA


// Checking data sent

var dossierId = url.templateArgs['dossierId'],
    cachet = args['cachet'];

// Checks and result

if (!valid_uuid(dossierId)) {
    setStatusMessageCode("1001", 400);
}
else {
    var dossier = search.findNode("workspace://SpacesStore/" + dossierId);
    model.ret = dossierService.getCustomSignatures(dossier.nodeRef, cachet === "true");
}