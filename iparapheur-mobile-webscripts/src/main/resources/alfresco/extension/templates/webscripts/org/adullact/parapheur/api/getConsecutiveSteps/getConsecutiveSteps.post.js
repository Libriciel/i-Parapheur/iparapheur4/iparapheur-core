<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var rules = {
    "dossier": {"typeOf":"string","isValid":is_valid_nodeRef()},
    "username": {"typeOf":"string"}
};

var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.ret = dossierService.getConsecutiveSteps(jsonData.dossier, jsonData.username);
}