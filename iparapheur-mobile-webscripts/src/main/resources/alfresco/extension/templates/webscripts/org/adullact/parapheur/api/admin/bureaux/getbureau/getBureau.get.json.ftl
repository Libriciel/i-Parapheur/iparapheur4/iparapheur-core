{
    "id" : "${idBureau}",
    "name" : "${bureau.properties.name}",
    "title" : "${bureau.properties.title}",
    "description" : "${bureau.properties.description}",
    "delegataire" :  "${delegataire}",
    "owners" : 
    [

        <#assign first = true>
        <#list proprietaires as proprietaire>
            <#if first == false>
                ,
            </#if>
            "${proprietaire}"
            <#assign first = false>
        </#list>

    ]
}
