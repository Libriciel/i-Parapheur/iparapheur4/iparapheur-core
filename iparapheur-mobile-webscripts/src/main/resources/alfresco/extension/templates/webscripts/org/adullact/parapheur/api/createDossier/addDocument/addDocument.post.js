<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var jsonData = JSON.parse(requestbody.content);

var rules = {
    "dossier":{"typeOf":"string", "isValid":is_valid_nodeRef()},
    "name": {"typeOf":"string"},
    "content" : {"typeOf":"string"},
    "visualname" : {"typeOf":"string"},
    "visualcontent" : {"typeOf":"string"},
    "reloadMainDocument" : {"typeOf":"boolean"}
};

if (validate(jsonData, rules)) {
    model.ret = dossierService.addMainDocument(jsonData.dossier, jsonData.name, jsonData.content, jsonData.visualname, jsonData.visualcontent, jsonData.reloadMainDocument);
}
