<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);


var rules = {
    "dossierRef":{"typeOf":"string","isValid":is_valid_nodeRef()},
};

var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.res = dossierService.getGeneratedImagesForDossier(jsonData.dossierRef);
}