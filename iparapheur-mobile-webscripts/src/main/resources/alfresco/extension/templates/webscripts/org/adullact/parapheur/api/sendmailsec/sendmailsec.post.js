<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_nodeRef()},
    "dossiers": {"typeOf":"array","isValid":is_valid_nodeRef()},
    "attachments": {"typeOf":"array","isValid":is_valid_nodeRef()},
    "destinataires": {"typeOf":"array"},
    "destinatairesCC": {"typeOf":"array"},
    "destinatairesCCI": {"typeOf":"array"},
    "objet": {"typeOf":"string"},
    "message": {"typeOf":"string"},
    "password":{"typeOf":"string"},
    "showpass":{"typeOf":"string"},
    "annexesIncluded": {"typeOf": "string"}
};
    

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.ret = dossierService.sendMailSec(jsonData.bureauCourant, jsonData.dossiers, jsonData.destinataires, 
    jsonData.destinatairesCC, jsonData.destinatairesCCI, jsonData.objet, jsonData.message, jsonData.password,
    jsonData.showpass, jsonData.attachments, jsonData.annexesIncluded);
}

