<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

//noinspection JSUnresolvedVariable
var id = url.templateArgs['id'];
var documentId = url.templateArgs['documentId'];
var idAnnot = url.templateArgs['idAnnotation'];

//Test de la validité de l'id
if(valid_uuid(id) && valid_uuid(idAnnot)) {

    var dossier = search.findNode("node", ["workspace", "SpacesStore", id]);
    var document = search.findNode("node", ["workspace", "SpacesStore", documentId]);

    annotationService.removeAnnotation(dossier.nodeRef, document.nodeRef, idAnnot);
}