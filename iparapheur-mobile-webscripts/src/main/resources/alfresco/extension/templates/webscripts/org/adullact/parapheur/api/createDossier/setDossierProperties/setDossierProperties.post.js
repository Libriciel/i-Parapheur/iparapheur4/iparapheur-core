<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var jsonData = JSON.parse(requestbody.content);

var rules = {
    "dossier":{"typeOf":"string", "isValid":is_valid_nodeRef()},
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_nodeRef()},
    "properties": {"typeOf":"object"}
};

if (validate(jsonData, rules))
    dossierService.setDossierProperties(jsonData.dossier, jsonData.bureauCourant, jsonData.properties);