{toc}


h2. Index of All Web Scripts
<#macro recursepackage package>
    <#if package.scripts?size &gt; 0>
h3. Package: ${package.path} ${url.serviceContext}/index/package${package.path}
        <#list package.scripts as webscript>
            <#assign desc = webscript.description>
h4. ${desc.shortName}
            <#if desc.description??>
${desc.description}
            </#if>
h5. URI's
|| Method || URI ||
                <#list desc.URIs as uri>
| ${desc.method?html} | {noformat}${url.serviceContext}${uri?html}{noformat} |
                </#list>
h5. Properties
|| Property || Value ||
| Authentication | ${desc.requiredAuthentication} |
| Transaction | ${desc.requiredTransaction} |
| Format Style | ${desc.formatStyle} |
| Default Format | ${desc.defaultFormat!"Determined at run-time"} |
| Lifecycle | ${desc.lifecycle} |
| Id | ${url.serviceContext}/script/${desc.id} |
| Descriptor | ${url.serviceContext}/description/${desc.id} |
| Descriptor Path | ${desc.storePath}/${desc.descPath} |
        </#list>
    </#if>
    <#list package.children as childpath>
    <@recursepackage package=childpath/>
    </#list>
</#macro>

<@recursepackage package=rootpackage/>