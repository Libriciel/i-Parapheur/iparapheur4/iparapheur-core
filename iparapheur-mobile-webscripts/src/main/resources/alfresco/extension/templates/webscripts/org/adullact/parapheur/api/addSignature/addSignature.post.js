<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var jsonData = JSON.parse(requestbody.content);

var rules = {
    "dossier":{"typeOf":"string", "isValid":is_valid_nodeRef()},
    "bureauCourant" : {"typeOf":"string", "isValid":is_valid_nodeRef()},
    "name": {"typeOf":"string"},
    "content" : {"typeOf":"string"}
};

if (validate(jsonData, rules)) {
//    var dossier = search.findNode(jsonData.dossier);
//    var bureauParent = iparapheur.getParentParapheur(dossier);
//    if(bureauParent.nodeRef === jsonData.bureauCourant) {
//        
//    } else {
//        model.ret = "Vous n'avez pas l'autorisation"
//    }
    model.ret = dossierService.addSignature(jsonData.dossier, jsonData.bureauCourant, jsonData.name, jsonData.content);
}
