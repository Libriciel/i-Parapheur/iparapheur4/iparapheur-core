<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var id = url.templateArgs['id'];
model.imageNum = url.templateArgs['page'];
var idDocu = url.templateArgs['document'];

model.document =  search.findNode("node", ["workspace", "SpacesStore", idDocu]);
model.dossier =  search.findNode("node", ["workspace", "SpacesStore", id]);