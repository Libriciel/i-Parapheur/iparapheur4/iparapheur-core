var username = usersService.getCurrentUsername();

var data = [];

if(usersService.isAdministrateur(username)) {

    var users = usersService.getConnectedUsers();
    for(var i = 0; i < users.size(); i++) {
        data.push(users.get(i));
    }
}

model.mjson = jsonUtils.toJSONString(data);