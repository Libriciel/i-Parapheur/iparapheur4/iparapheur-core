<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "dossiers": {"typeOf":"array","isValid":is_valid_nodeRef()},
    "annotPub": {"typeOf":"string"},
    "annotPriv": {"typeOf":"string"},
    "bureauCourant": {"typeOf":"string"},
    "consecutiveSteps" : {"typeOf":"boolean", "optional":"true"}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    var dossiers = jsonData.dossiers;
    var annotPub = jsonData.annotPub;
    var annotPriv = jsonData.annotPriv;
    var bureauCourant = jsonData.bureauCourant;

    var consecutiveSteps = jsonData.consecutiveSteps ? jsonData.consecutiveSteps : false;
    

    model.ret = dossierService.visaDossier(dossiers, annotPub, annotPriv, bureauCourant, consecutiveSteps);

    model.code = "OK";
}
else
    model.code = "KO";

