<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);


var rules = {
    "dossiers": {"typeOf":"array","isValid":is_valid_nodeRef()},
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_nodeRef()},
    "nomArchives" : {"typeOf":"array"},
    "annexes" : {"typeOf":"array"}
};


//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.ret = dossierService.archiveDossier(jsonData.dossiers,
        jsonData.nomArchives,
        jsonData.bureauCourant,
        jsonData.annexes);
}