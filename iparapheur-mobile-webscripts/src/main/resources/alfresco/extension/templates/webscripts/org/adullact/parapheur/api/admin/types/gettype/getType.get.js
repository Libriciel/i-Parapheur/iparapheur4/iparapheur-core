var typesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiertype/cm:TypesMetier.xml")[0];
if (typesnode == null){
    throw new Error("Les types techniques n'ont pas été initialisés");
}

model.isNew = true;
model.id = args.name;
model.xml = typesnode.content;
/* create data model */
var rootxml = new XML(typesnode.content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "")); // Workaround: Mozilla Bug 336551
model.rootxml = rootxml;

model.types = new Array();
for (var numero in rootxml.MetierType) {
    model.types.push(rootxml.MetierType[numero].ID.toString());
    if (rootxml.MetierType[numero].ID.toString() == args.name) {
        model.type = new Object();
        model.type.ID = rootxml.MetierType[numero].ID.toString();
        model.type.Desc = rootxml.MetierType[numero].Desc.toString();
        model.type.TdTNom = rootxml.MetierType[numero].TdT.Nom.toString();
        model.type.TdTProtocole = rootxml.MetierType[numero].TdT.Protocole.toString();
        model.type.TdTOverride = rootxml.MetierType[numero].TdT.override.toString();
        if (model.type.TdTOverride != null && model.type.TdTOverride != "true") {
            model.type.TdTOverride = "false";
        }
        model.type.sigFormat = rootxml.MetierType[numero].sigFormat.toString();
        model.isNew = false;
    }
}
/* <BLEX> */
var tdtNames = [];
tdtNames.push("pas de TdT");
tdtNames.push("S²LOW");
tdtNames.push("SRCI"); /* voir SrciService.K.tdtName */
tdtNames.push("FAST");

model.tdtNames=tdtNames;
/* </BLEX> */