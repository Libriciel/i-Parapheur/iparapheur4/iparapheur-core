<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(java.util.HashMap);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "active": {"typeOf": "string"},
    "showStamp": {"typeOf": "string"},
    "stampPage": {"typeOf": "string"},
    "stampCoordX": {"typeOf": "string"},
    "stampCoordY": {"typeOf": "string"},
    "stampHeight": {"typeOf": "string"},
    "stampWidth": {"typeOf": "string"},
    "stampFontSize": {"typeOf": "string"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Récupération de l'id du type
var type = url.templateArgs['id'];
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var parameters = new HashMap();

        parameters.active = "" + jsonData.active;

        parameters.showStamp = "" + jsonData.showStamp;
        parameters.stampPage = "" + jsonData.stampPage;
        parameters.stampCoordX = "" + jsonData.stampCoordX;
        parameters.stampCoordY = "" + jsonData.stampCoordY;
        parameters.stampHeight = "" + jsonData.stampHeight;
        parameters.stampWidth = "" + jsonData.stampWidth;
        parameters.stampFontSize = "" + jsonData.stampFontSize;

        s2lowConfig.setPadesParametersForType(type, parameters);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
