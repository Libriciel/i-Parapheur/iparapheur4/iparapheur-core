<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var jsonData = JSON.parse(requestbody.content);

var rules = {
    "dossier":{"typeOf":"string", "isValid":is_valid_nodeRef()},
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_nodeRef()}
};

if (validate(jsonData, rules))
    model.ret =  dossierService.finalizeCreateDossier(jsonData.dossier, jsonData.bureauCourant);