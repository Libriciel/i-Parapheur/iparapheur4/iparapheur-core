<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "enabled" : {
        "typeOf":"boolean"
    }
};

var jsonData = JSON.parse(requestbody.content);
if(validate(jsonData, rules)) {
    var id = url.templateArgs['id'];
    var c = multiCService.getCollectivite(id);
    if(jsonData.enabled) {
        multiCService.enableCollectivite(c);
    } else {
        multiCService.disableCollectivite(c);
    }
}