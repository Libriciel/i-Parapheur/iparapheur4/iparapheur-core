<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var parameters = s2lowConfig.getHeliosParameters();

    ret.active = parameters.active;

    if (ret.active == undefined) {
        ret.active = 'true';
    }

    ret.name = parameters.name;
    ret.server = parameters.server;
    ret.port = parameters.port;
    ret.password = parameters.password;
    ret.userlogin = (parameters.userlogin == null)? "" : parameters.userlogin;
    ret.userpassword = (parameters.userpassword == null)? "" : parameters.userpassword;
    ret.baseUrlArchivage = parameters.baseUrlArchivage;

    ret.isPwdGoodForPkcs = parameters.isPwdGoodForPkcs;
    ret.dateLimite = parameters.dateLimite;
    if (parameters.isPwdGoodForPkcs == "ok") {
        if (ret.active == 'true') {
            if (iparapheur.isCertificateAbleToConnectToS2low("HELIOS", null)) {
                ret.validCertCnx = "ok";

                ret.listeLogins = iparapheur.getArrayLoginForType("HELIOS", null);
                if (iparapheur.isConnectionOK("HELIOS", null)) {
                    ret.validLoginAndCertCnx = "ok";
                } else {
                    ret.validLoginAndCertCnx = "ko";
                }
            } else {
                ret.validCertCnx = "ko";
            }
        }
    }

    ret.collectivite = parameters.collectivite;
    ret.parapheur = parameters.parapheur;
    ret.pPolicyIdentifierID = parameters.pPolicyIdentifierID;
    ret.pPolicyIdentifierDescription = parameters.pPolicyIdentifierDescription;
    ret.pPolicyDigest = parameters.pPolicyDigest;
    ret.pSPURI = parameters.pSPURI;
    ret.pCity = parameters.pCity;
    ret.pPostalCode = parameters.pPostalCode;
    ret.pCountryName = parameters.pCountryName;
    ret.pClaimedRole = parameters.pClaimedRole;

    model.mjson = JSON.stringify(ret);

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
