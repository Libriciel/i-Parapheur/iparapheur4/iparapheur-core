//StarXpert
var nodes = null;
var noderef_calques = calqueService.getEspaceCalques();
if (noderef_calques != null) {
    // Recherche de tous les calques
    var node_calques = search.findNode(noderef_calques);
    nodes = node_calques.children;
}

var ret = [];
var j = 0;
for(var i = 0; i < nodes.length; i++) {
    var calque = nodes[i];
    var name = calque.properties["cal:nomCalque"];
    if(name !== null) {
        ret[j++] = {
            id: calque.id,
            name: calque.properties["cal:nomCalque"]
        }
    }
}

model.mjson = jsonUtils.toJSONString(ret);