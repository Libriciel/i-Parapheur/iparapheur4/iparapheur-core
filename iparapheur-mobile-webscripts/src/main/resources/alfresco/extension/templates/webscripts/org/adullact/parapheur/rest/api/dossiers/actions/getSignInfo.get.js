<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

function is_valid_uuid(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var id = url.templateArgs['id'];
var bureauCourant = args['bureauCourant'];
//Test de la validité de l'id
if(is_valid_uuid(id)) {
    var dossier = search.findNode("workspace://SpacesStore/" + id);
    var bureau = search.findNode("workspace://SpacesStore/" + bureauCourant);
    try {
        model.ret = dossierService.getSignatureInformations(dossier.nodeRef, bureau.nodeRef);
    }
    catch(err) {
        setStatusMessageCode(err.message, 400);
    }
} else {
    setStatusMessageCode("1001", 400);
}
