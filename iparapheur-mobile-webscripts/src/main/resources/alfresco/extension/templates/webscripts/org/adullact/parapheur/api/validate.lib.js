// Validate Lib

/**
 * Validate json inputs.
 * @author Emmanuel Peralta
 */

/**
 * Fonction utilitaire pour récuperer les clefs d'un tableau associatif.
 * @param o
 * @return {Array}
 */
function getKeys(o) {
    if (o !== Object(o)) {
        throw new TypeError('Object.keys called on non-object');
    }
    var ret = [], p;
    for (p in o) {
        if (Object.prototype.hasOwnProperty.call(o, p)) {
            ret.push(p);
        }
    }
    return ret;
}

/**
 * Fonction utilitaire qui permet de determiner
 * le type d'une valeur passée en paramètre.
 * Permet de différencier Object et Array.
 * @param value
 * @return {String}
 */
function typeOf(value) {
    var s = typeof value;
    if (s === 'object') {
        if (value) {
            if (value instanceof Array) {
                s = 'array';
            }
        } else {
            s = 'null';
        }
    }
    return s;
}


/**
 * Fonction utilitaire qui positionne un message d'erreur
 *
 * @param message
 */
function setStatusMessage(message) {
    setStatusMessageCode(message, 400);
}

/**
 * Fonction utilitaire qui positionne un message d'erreur + code d'erreur
 *
 * @param message
 * @param httpCode
 */
function setStatusMessageCode(message, httpCode) {
    status.message = message;
    status.code = httpCode;
    status.redirect = true;
}


/**
 * Fonction qui valide le format d'une trame d'entrée webservices (object)
 * @param data
 * @param rules
 * @return {Boolean}
 */
function validate(data, rules) {
    var result = true;

    if (data !== undefined) {
        var keys = getKeys(rules);
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            if (data[key] !== undefined) {

                if (typeOf(data[key]) !== rules[key].typeOf) {
                    setStatusMessage(key + " n'est pas du type requis (attendu : " + rules[key].typeOf + ' recu : ' + typeOf(data[key]));
                    return false;
                }

                if (typeOf(data[key]) === "string") {
                    if (rules[key].isValid !== undefined) {
                        var formatOk = rules[key].isValid(data[key]);
                        if (formatOk === false) {
                            setStatusMessage("La valeur de "+key+" ne correspond pas au format attendu");
                        }
                        result = result && formatOk;
                    }
                }

                if (typeOf(data[key]) === 'array' && rules[key].childRules !== undefined) {
                    for (var j = 0; j < data[key].length; j++) {
                        result = result && validate(data[key][j], rules[key].childRules);
                    }
                }

                if (typeOf(data[key]) === 'object' && rules[key].childRules !== undefined) {
                    result = result && validate(data[key], rules[key].childRules);
                }

            }
            else {
                if (rules[key].optional !== undefined) {
                    if (!rules[key].optional) {
                        setStatusMessage("Le champ " + key + " n'est pas valué");
                        return false;
                    }
                }
                else {
                    setStatusMessage("Le champ " + key + " n'est pas valué");
                    return false;
                }
            }
        }

    }
    else {
        return false;
    }

    return result;

}

/* fonctions de validation */


/**
 * Génère une regle de validation avec une regexp
 * @param regexp
 * @return {Function}
 */
function is_valid_with_regexp(regexp) {
    return function (value) {
        return value.match(regexp);
    };
}

/* Default validation rules */

function is_valid_nodeRef() {
    return is_valid_with_regexp(/workspace:\/\/SpacesStore\/[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}/);
}

function is_valid_annot_type() {
    return function (value) {
        return value === "text" || value === "rect" || value === "highlight";
    };
}

function is_valid_uuid() {
    return is_valid_with_regexp(/[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}/);
}

function valid_uuid(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

function is_valid_point() {
    return function (value) {
        var pointRules = {
            "point":{"typeOf":"object",
                "childRules":{

                    "x":{"typeOf":"number"},
                    "y":{"typeOf":"number"}
                }
            }
        };
        return validate({"point":value}, pointRules);
    };

}

function is_valid_rect() {
    return function (value) {
        var rectRules = {
            "rect":{"typeOf":"object",
                "childRules":{
                    "topLeft":{"typeOf":"object",
                        "childRules":{
                            "x":{"typeOf":"number"},
                            "y":{"typeOf":"number"}
                        }
                    },
                    "bottomRight":{"typeOf":"object",
                        "childRules":{
                            "x":{"typeOf":"number"},
                            "y":{"typeOf":"number"}
                        }
                    }
                }
            }
        };
        return validate({"rect":value}, rectRules);
    };
}

function is_valid_etape() {
    return function (value) {
        var etapeRules = {
            "etape" : {
                "typeOf":"object",
                "childRules": {
                    "actionDemandee" : {"typeOf": "string"},
                    "transition" : {"typeOf": "string"},
                    "listeNotification" : {"typeOf": "array"},
                    "parapheur" : {"typeOf": "string", "isValid" : is_valid_uuid(), "optional" : true}
                }
            }
        };

        return validate({"etape":value}, etapeRules);
    };

}