<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE

//Rules
var rules = {
    "certificat" : {
        "typeOf": "string",
        "optional" : false
    }
};


//Récupération du contenu JSON de la requete
var jsonData = JSON.parse(requestbody.content);
//Validation de la requête
if(validate(jsonData, rules)) {
    try {
        model.ret = km.getCertificatDetails(jsonData.certificat);
    } catch (e) {
        setStatusMessageCode("Erreur lors de la récupération des détails du certificat: " + e, 500);
    }
}
