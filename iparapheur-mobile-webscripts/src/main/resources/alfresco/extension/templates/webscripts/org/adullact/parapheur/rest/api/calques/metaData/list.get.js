<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    var ret = [];

    // Recuperation de l'id du noeud du calque
    var idCalque = url.templateArgs['id'];

// Recherche de la liste des noeuds des signatures lies au calque
    var nodeRefs = calqueService.listMetadata(idCalque);


    // Transformation des nodeRefs en node
    for (var i = 0 ; i < nodeRefs.size() ; i++) {
        var n  = search.findNode(nodeRefs.get(i));
        var sizeFont = n.properties["cal:taillePolice"];

        var qnameMD = ((n.properties["cal:sourceMD"] ? n.properties["cal:sourceMD"] + '|' : '') + n.properties["cal:qnameMD"])
                .replace('{http://www.adullact.org/parapheur/metadata/1.0}', 'cu:')
                .replace('{http://www.alfresco.org/model/content/1.0}', 'cm:')
                .replace('{http://www.atolcd.com/alfresco/model/parapheur/1.0}', 'ph:');

        ret[i] = {
            "id": n.id,
            "idCalque": idCalque,
            "type": "metadata",
            "qnameMD": qnameMD,
            "coordonneeX": n.properties["cal:coordonneeX"],
            "coordonneeY": n.properties["cal:coordonneeY"],
            "page": n.properties["cal:page"],
            "taillePolice": (sizeFont ? sizeFont : 12),
            "postSignature": n.properties["cal:postSignature"]
        }
    }

    model.mjson = jsonUtils.toJSONString(ret);
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}