<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);


var rules = {
    "dossier":{"typeOf":"string","isValid":is_valid_nodeRef()},
    "bureauCourant":{"typeOf":"string","isValid":is_valid_nodeRef()}
};


//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.includeChildren = true;
    model.includeContent = false;
    model.isUser = false;

    model.canDelete = {};

    model.code = "ERROR";

    if (jsonData.dossier != undefined && jsonData.dossier != null) {
        var dossierRef = jsonData.dossier;
        var object = search.findNode("node", ["workspace", "SpacesStore", dossierRef.substring(24)]);
        model.parent = object.parent.properties["cm:name"];
        if(object.properties["ph:typeMetier"] != undefined && object.properties["ph:soustypeMetier"] != undefined)
            var meta = typesService.getMetaDonneesDossierApi(object.properties["ph:typeMetier"], object.properties["ph:soustypeMetier"], dossierRef);
        model.images = dossierService.getGeneratedImagesForDossier(dossierRef);
        model.pageCount = dossierService.getPageCountForDossier(dossierRef);
        model.actionDemandee = getActionDemandee(object);
        if(object.properties["ph:xpathSignature"] != undefined) {
            model.xpathSignature = object.properties["ph:xpathSignature"];
        }
        
        for(var i = 0; i < object.children.length; i++){
            if(object.children[i].isDocument) {
                model.canDelete[object.children[i].nodeRef] = dossierService.canRemoveDoc(object.children[i].nodeRef, dossierRef, jsonData.bureauCourant);
            }
        }
        
        if(iparapheur.getCurrentEtapeCircuit(object.nodeRef) !== null && iparapheur.getCurrentEtapeCircuit(object.nodeRef).getParapheur() == jsonData.bureauCourant) {
            model.canAdd = true;
        }
        
        //ACTIONS POSSIBLE
        model.remorse = dossierService.canRemorse(dossierRef, jsonData.bureauCourant);
        model.del = dossierService.canDelete(dossierRef);
        model.reject = dossierService.canReject(dossierRef, jsonData.bureauCourant);
        model.sign = dossierService.canSign(dossierRef, jsonData.bureauCourant);
        model.raz = dossierService.canRAZ(dossierRef, jsonData.bureauCourant);
        model.edit = dossierService.canEdit(dossierRef, jsonData.bureauCourant);
        model.archive = dossierService.canArchive(dossierRef, jsonData.bureauCourant);
        model.secretary = dossierService.canSecretariat(dossierRef, jsonData.bureauCourant);
        model.hasRead = dossierService.hasReadDossier(dossierRef);
        model.isRead =  dossierService.isDossierLu(dossierRef),
        
        //Acteur courant
        model.acteurCourant = iparapheur.isOwnerOrDelegateOfDossier(jsonData.bureauCourant, dossierRef);
        
        //Xemelios
        model.hasXemelios = dossierService.isPesViewEnabled(dossierRef);
        model.signPapier = object.properties["ph:signature-papier"];
        model.isCurrentTdt = dossierService.isSendTdt(dossierRef);
        model.isRejete = iparapheur.isRejete(dossierRef);
    }


    if (object != null)
    {
        model.code = "OK";
    }

    model.object = object;

    if(meta != undefined)
        model.meta = meta;
}

function getActionDemandee(node) {
    var etape = iparapheur.getCurrentEtapeCircuit(node.nodeRef);
    if (etape != null) {
        return etape.getActionDemandee();
    }
    else {
        return "NONE";
    }
}