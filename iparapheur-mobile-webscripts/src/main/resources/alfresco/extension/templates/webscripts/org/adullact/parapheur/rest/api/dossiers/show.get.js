<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

/**
 * Renvoi les détails d'un dossier.
 *
 * Retour :
 *
 *     {
 *      "id" : String,
 *      "title" : String,
 *      "type" : String,
 *      "sousType" : String,
 *      "actionDemandee" : String, (VISA, SIGNATURE, ...)
 *      "bureauName" : String,
 *      "bannetteName" : String,
 *      "dateEmission" : timestamp,
 *      "dateLimite" : timestamp,
 *      "isRead" : bool,
 *      "hasRead" : bool,
 *      "readingMandatory" : bool,
 *      "isSent" : bool, (pour enCoursTdt, mailsec, ...)
 *      "actions" : [String], (ARCHIVER, SUPPRIMER, EDITER, ...)
 *
 *      "visibility" : String,
 *      "canAdd" : bool,
 *      "isXemEnabled" : bool,
 *      "metadatas" : [metas],
 *      "nomTdT" : String,
 *      "protocole" : String,
 *      "status" : String, (OPTIONNAL)
 *      "xPathSignature" : String, (OPTIONNAL)
 *      "documents" : [{ "id" : String,
 *                       "name" : String,
 *                       "actions" : [String],
 *                       "size" : int
 *                    }]
 *     }
 **/

var id = url.templateArgs['id'];
var bureauCourant = args.bureauCourant;
var shortRequest = args['short'];

// Checks and result

if (!valid_uuid(id)) {
    setStatusMessageCode("1001", 400);
    model.mjson = "";
}
else if (!valid_uuid(bureauCourant)) {
    setStatusMessageCode("1002", 400);
    model.mjson = "";
}
else {
    var dossierNode = search.findNode("node", ["workspace", "SpacesStore", id]);
    var bureauNode = search.findNode("node", ["workspace", "SpacesStore", bureauCourant]);
    var visibility = dossierNode.properties["ph:public"] ? "public" : dossierNode.properties["ph:confidentiel"] ? "confidentiel" : "group";

    if(shortRequest) {
        dossier = {
            "id"             : dossierNode.id,
            "title"          : dossierNode.properties["cm:title"],
            "actionDemandee" : getActionDemandee(dossierNode),
            "type"           : dossierNode.properties["ph:typeMetier"],
            "sousType"       : dossierNode.properties["ph:soustypeMetier"],
            "dateLimite"     : dossierNode.properties["ph:dateLimite"] !== null ? dossierNode.properties["ph:dateLimite"].getTime() : null,
            "dateEmission"   : dossierNode.properties["cm:created"].getTime(),
            "bureauName"     : dossierNode.parent.parent.properties["cm:title"],
            "banetteName"    : dossierNode.parent.properties["cm:title"],
            "locked"         : dossierNode.properties["ph:locked"],
            "isSignPapier"   : dossierNode.properties["ph:signature-papier"],
            "readingMandatory": dossierNode.properties["ph:reading-mandatory"],
            "hasRead"        : dossierService.hasReadDossier(dossierNode.nodeRef),
            "isRead"         : dossierService.isDossierLu(dossierNode.nodeRef),
            "actions"        : dossierService.getActions(dossierNode.nodeRef, bureauNode.nodeRef),
            "includeAnnexes" : dossierNode.properties["ph:include-attachments"],
            "banettes"       : dossierService.getCorbeillesParent(dossierNode.nodeRef, bureauNode.nodeRef)
        };
        if(dossier.actionDemandee == "TDT") {
            dossier.isSent = dossierService.isSendTdt(dossierNode.nodeRef);
        } else if(dossier.actionDemandee == "MAILSEC" || dossier.actionDemandee == "MAILSECPASTELL") {
            dossier.isSent = dossierService.isSendMailSec(dossierNode.nodeRef);
        } else {
            dossier.isSent = false;
        }
    } else {
        var canAdd = false;
        if(iparapheur.getCurrentEtapeCircuit(dossierNode.nodeRef) !== null) {
            var parent = iparapheur.getCurrentEtapeCircuit(dossierNode.nodeRef).getParapheur();
            if(parent != null) {
                parent = search.findNode(parent);
                canAdd = iparapheur.isParapheurOwnerOrDelegate(parent.nodeRef, usersService.getCurrentUsername());
            }
        }

        dossierService.readDossier(dossierNode.nodeRef, usersService.getCurrentUsername());

        dossier = {
            "id" : id,
            "title" : dossierNode.properties["cm:title"],
            "type" : dossierNode.properties["ph:typeMetier"],
            "sousType" : dossierNode.properties["ph:soustypeMetier"],
            "actionDemandee" : getActionDemandee(dossierNode),
            "bureauName" : dossierNode.parent.parent.properties["cm:name"],
            "banetteName" : dossierNode.parent.properties["cm:name"],
            "dateEmission" : dossierNode.properties["cm:created"].getTime(),
            "dateLimite" : dossierNode.properties["ph:dateLimite"] !== null ? dossierNode.properties["ph:dateLimite"].getTime() : null,
            "isRead" : dossierService.isDossierLu(dossierNode.nodeRef),
            "hasRead" : dossierService.hasReadDossier(dossierNode.nodeRef),
            "readingMandatory" : dossierNode.properties["ph:reading-mandatory"],
            "isSignPapier"   : dossierNode.properties["ph:signature-papier"],
            "includeAnnexes" : dossierNode.properties["ph:include-attachments"],
            "locked"         : dossierNode.properties["ph:locked"],
            "actions" : dossierService.getActions(dossierNode.nodeRef, bureauNode.nodeRef),
            "visibility" : visibility,
            "canAdd" : canAdd,
            "isXemEnabled" : dossierService.isPesViewEnabled(dossierNode.nodeRef),
            "nomTdT" : dossierNode.properties["ph:tdtNom"],
            "protocole" : dossierNode.properties["ph:tdtProtocole"],
            "status" : dossierNode.properties["ph:status"],
            "xPathSignature" : dossierNode.properties["ph:xpathSignature"],
            "acteursVariables": dossierService.getActeursVariables(dossierNode.nodeRef) || [],
            "forbidedSameSig": dossierService.getForbidedSameSig(dossierNode.nodeRef),
            "documents" : []
        };


        dossier.metadatas = dossierNode.properties["ph:typeMetier"] != undefined && dossierNode.properties["ph:soustypeMetier"] != undefined ?
            typesService.getMetaDonneesDossierApi(dossierNode.properties["ph:typeMetier"], dossierNode.properties["ph:soustypeMetier"], dossierNode.nodeRef) : {};

        if(dossier.actionDemandee == "TDT") {
            dossier.isSent = dossierService.isSendTdt(dossierNode.nodeRef);
        } else if(dossier.actionDemandee == "MAILSEC" || dossier.actionDemandee == "MAILSECPASTELL") {
            dossier.isSent = dossierService.isSendMailSec(dossierNode.nodeRef);
        } else {
            dossier.isSent = false;
        }
        var children = dossierNode.children;
        for(var i = 0; i < children.length; i++) {
            if(children[i].isDocument) {
                var isMainDocument = children[i].hasAspect("ph:main-document");
                var isPending = children[i].hasAspect("ph:pending");
                var isSigned = children[i].hasAspect("ph:signed");
                var attestState = 0;
                if(children[i].hasAspect("ph:attest")) {
                    if(children[i].properties['ph:attest-id']) {
                        // En cours de génération
                        attestState = 1;
                    } else {
                        // Aspect sans ID => erreur
                        attestState = -1;
                    }
                } else if (!!children[i].properties['ph:attest-content'] && children[i].properties['ph:attest-content'].size > 0) {
                    // Il y a une attestation !
                    attestState = 2;
                }
                var document = {
                    "name" : children[i].name,
                    "size" : children[i].size,
                    "id" : children[i].nodeRef.id,
                    "isLocked" : isPending,
                    "isMainDocument" : isMainDocument,
                    "attestState": attestState,
                    "isSigned" : isSigned,
                    "canDelete" : dossierService.canRemoveDoc(children[i].nodeRef, dossierNode.nodeRef, bureauNode.nodeRef),
                    "visuelPdf" : children[i].properties['ph:visuel-pdf'].size > 0
                };
                if (isMainDocument && !isPending) {
                    try {
                        document.pageCount = dossierService.getPageCountForDocument(children[i].nodeRef);
                    } catch (e) {
                        logger.warn(e);
                        document.pageCount = 0;
                    }
                }
                dossier.documents.push(document);
            }
        }
    }

    model.mjson = jsonUtils.toJSONString(dossier);
}

function getActionDemandee(node) {
    var etape = iparapheur.getCurrentEtapeCircuit(node.nodeRef);
    if (etape != null) {
        return etape.getActionDemandee();
    }
    else {
        return "NONE";
    }
}
