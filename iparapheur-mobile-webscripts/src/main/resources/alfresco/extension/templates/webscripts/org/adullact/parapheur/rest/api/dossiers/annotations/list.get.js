<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

//noinspection JSUnresolvedVariable
var bureauId = url.templateArgs['id'];
var documentId = url.templateArgs['documentId'];

if(valid_uuid(bureauId)) {

    var bureauNode = search.findNode("node", ["workspace", "SpacesStore", bureauId]);
    var data;

    if (documentId != null) {
        if (valid_uuid(documentId)) {
            var documentNode = search.findNode("node", ["workspace", "SpacesStore", documentId]);
            data = annotationService.handleDocumentAnnotations(bureauNode.nodeRef, documentNode.nodeRef);
        }
    }
    else {
        data = annotationService.handleDocumentAnnotations(bureauNode.nodeRef);
    }
}

model.mjson = data;