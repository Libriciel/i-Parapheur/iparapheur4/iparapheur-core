importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);
importPackage(Packages.java.util);
importClass(Packages.org.adullact.iparapheur.repo.jscript.ScriptEtapeCircuitImpl);


var jsonData = JSON.parse(requestbody.content);


    var name = jsonData.name;

    var acl = new HashSet();
    for each (parapheur in jsonData.ACLBureau) {
        acl.add(new NodeRef(parapheur));
    }
    var groupes = new HashSet();
    for each (group in jsonData.ACLGroupes) {
        groupes.add(group);
    }
    var isPublic = jsonData.isPublic;

    var oldCircuit = iparapheur.getWorkflowByName(jsonData.name); //iparapheur.getPublicWorkflows()[args.circuit_nom];
    if (oldCircuit == jsonData.name ) {
        throw new Error("Un circuit portant ce nom existe déjà");
    }

    var circuit = new ArrayList();

    for (var i = 0; ; i++) {
        if (!jsonData.etapes[i]) {
            break;
        }
        if (jsonData.etapes[i].objet == "deleted") {
            continue;
        }
        var transition;
        var parapheur;
        if (jsonData.etapes[i].objet == "chefde") {
            transition = "CHEF_DE";
            parapheur = null;
        } else if (jsonData.etapes[i].objet == "emetteur") {
            transition = "EMETTEUR";
            parapheur = null;
        } else {
            transition = "PARAPHEUR";
            parapheur = new NodeRef(jsonData.etapes[i].objet);
        }
        var actionDemandee = jsonData.etapes[i].action;
        var listeNotification = new HashSet();
        for each (notif in jsonData.etapes[i].notification) {
            if (notif != "") {
                listeNotification.add(new NodeRef(notif));
            }
        }

        circuit.add(new ScriptEtapeCircuitImpl(transition, parapheur, actionDemandee, listeNotification));
    }

    iparapheur.saveWorkflow(name, circuit, acl, groupes, isPublic);
    model.idCircuit = iparapheur.getWorkflowByName(jsonData.name).toString();