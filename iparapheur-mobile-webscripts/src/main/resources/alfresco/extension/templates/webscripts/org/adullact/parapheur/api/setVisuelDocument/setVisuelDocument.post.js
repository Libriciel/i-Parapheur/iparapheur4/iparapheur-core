<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "dossier": {"typeOf":"string","isValid":is_valid_uuid()},
    "document": {"typeOf":"string","isValid":is_valid_uuid()},
    "content": {"typeOf":"string"}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.ret = dossierService.setVisuelDocument(jsonData.dossier, jsonData.document, jsonData.content);
}

