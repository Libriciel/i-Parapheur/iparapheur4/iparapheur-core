<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "id" : {"typeOf" : "string"},
    "desc" : {"typeOf" : "string"},
    "circuit" : {"typeOf" : "string", "optional": true},
    "script" : {"typeOf" : "string", "optional": true},
    "isTdtAuto" : {"typeOf" : "string", "optional": true},
    "readingMandatory" : {"typeOf" : "string", "optional": true},
    "digitalSignatureMandatory" : {"typeOf" : "string", "optional": true},
    "circuitHierarchiqueVisible" : {"typeOf" : "string", "optional": true},
    "includeAttachments" : {"typeOf" : "string", "optional": true},
    "multiDocument": {"typeOf" : "string", "optional":true},
    "forbidSameSig":  {"typeOf" : "string", "optional":true},
    "visibility" : {"typeOf" : "string", "optional": true},
    "visibilityFilter" : {"typeOf" : "string", "optional": true},
    "parapheurs" : {"typeOf" : "array"},
    "parapheursFilters" : {"typeOf" : "array"},
    "calques" : {"typeOf" : "array"},
    "calquesAnnexes" : {"typeOf" : "array"},
    "metadatas" : {"typeOf" : "array"},
    "groups" : {"typeOf" : "array"},
    "groupsFilters" : {"typeOf" : "array"}
};

// Valeur par defaut du flag "digitalSignatureMandatory" (pour les nouveaux
// sous types, et lors de la lecture d'un sous type cree dans une version
// precedente)
// BL prefere que, par defaut, la signature electronique soit obligatoire
var default_digitalSignatureMandatory = "blex".equals(iparapheur.getHabillage());
var username = usersService.getCurrentUsername();
//Récupération du contenu JSON de la requete
var jsonData = JSON.parse(requestbody.content);


if(!usersService.isAdministrateur(username)) {
    setStatusMessageCode("This operation is retricted to non-admin users.", 403);
}
else if (!validate(jsonData, rules)) {
    setStatusMessageCode("Invalid data sent, expecting : \n" + jsonUtils.toJSONString(rules), 400);
}
else {
    var hasError = false;
    var IDtype = url.templateArgs['id'];
    var newssID = url.templateArgs['sousType'];
    var newDesc = jsonData.desc;
    var newCircuit = jsonData.circuit !== undefined ? jsonData.circuit : "";
    var newScript = jsonData.script ? jsonData.script : "";
    var isTdtAuto = jsonData.isTdtAuto ? jsonData.isTdtAuto : "false";
    var isCachetAuto = jsonData.isCachetAuto ? jsonData.isCachetAuto : "false";
    var cachetCertificate = jsonData.cachetCertificate !== undefined ? jsonData.cachetCertificate : "";
    var pastellMailsec = jsonData.pastellMailsec !== undefined ? jsonData.pastellMailsec : "";
    var hasToAttest = jsonData.hasToAttest ? jsonData.hasToAttest : "false";
    var forbidSameSig = jsonData.forbidSameSig ? jsonData.forbidSameSig : "false";
    var readingMandatory = jsonData.readingMandatory ? jsonData.readingMandatory : "false";
    var digitalSignatureMandatory = jsonData.digitalSignatureMandatory ? jsonData.digitalSignatureMandatory : default_digitalSignatureMandatory ? "true" : false;
    var circuitHierarchiqueVisible = jsonData.circuitHierarchiqueVisible ? jsonData.circuitHierarchiqueVisible : "false";
    var includeAttachments = jsonData.includeAttachments ? jsonData.includeAttachments : "false";
    var visibility = jsonData.visibility ? jsonData.visibility : "public";
    var visibilityFilter = jsonData.visibilityFilter ? jsonData.visibilityFilter : "public";

    var isCompatibleWithMultidoc = typesService.isCompatibleWithMultidoc(IDtype);
    var multiDocument = isCompatibleWithMultidoc && (jsonData.multiDocument ? jsonData.multiDocument : "false");

    var sstypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ IDtype +".xml']")[0];
    var rootxml = new XML(sstypesnode.content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "")); // Workaround: Mozilla Bug 336551

    // Mettre à jour le noeud XML
    var newrootxml = new XML();
    newrootxml = <MetierSousTypes/>;

    for (var numero in rootxml.MetierSousType) {
        if(newssID == rootxml.MetierSousType[numero].ID.toString()) {
            setStatusMessageCode("Le sous-type " + newssID + " existe déjà", 409);
            hasError = true;
            break;
        }
        newrootxml.MetierSousTypes +=  rootxml.MetierSousType[numero];
    }

    if(!hasError) {
        workflowService.isUsedBy(newCircuit, IDtype, newssID);
        var isPublic = visibility === "public";
        var isPublicFilters = visibilityFilter === "public";
        var strmaj = "<MetierSousType visibility=\"" + (isPublic ? "public" : "private") + "\" visibilityFilter=\""+ (isPublicFilters ? "public" : "private") +"\">" +
            "<ID>"+newssID+"</ID>" +
            "<Desc><![CDATA["+newDesc+"]]></Desc>"+
            "<Circuit>"+newCircuit+"</Circuit>" +
            "<Script><![CDATA["+newScript+"]]></Script>" +
            "<parapheurs>";
        for each (parapheur in jsonData.parapheurs) {
            strmaj += "<parapheur>" + "workspace://SpacesStore/" + parapheur + "</parapheur>";
        }
        strmaj += "</parapheurs>" +
            "<parapheursFilters>";
        for each (parapheur in jsonData.parapheursFilters) {
            strmaj += "<parapheur>" + "workspace://SpacesStore/" + parapheur + "</parapheur>";
        }
        strmaj += "</parapheursFilters>" +
            "<calques>";
        for each (calque in jsonData.calques) {
            strmaj += "<calque>" +
            "<id>" + "workspace://SpacesStore/" + calque.id + "</id>" +
            "<numDocument>" + calque.numDocument + "</numDocument>" +
            "</calque>";
        }
        strmaj += "</calques>" +
            "<calquesAnnexes>";
        for each (calque in jsonData.calquesAnnexes) {
            strmaj += "<calque>" + "workspace://SpacesStore/" + calque + "</calque>";
        }
        strmaj += "</calquesAnnexes>";

        function myFormat() {

            if (arguments.length < 2)
                return;

            var formatted = arguments[0];
            for (var i = 1; i < arguments.length; i++) {
                var regexp = new RegExp('\\{'+(i-1)+'\\}', 'gi');
                formatted = formatted.replace(regexp, arguments[i]);
            }
            return formatted;
        }

        function generateXMLSnippetForMetadatas(mds) {
            var retval = "<metadatas>";

            for each (def in mds) {
                if (def["default"] == undefined) {
                    def["default"] = "";
                }

                if (def.editable == undefined) {
                    def.editable = "false";
                }

                if (def.mandatory == undefined) {
                    def.mandatory = "false";
                }

                retval+= myFormat('<metadata mandatory="{0}" editable="{1}" default="{2}">{3}</metadata>', def.mandatory, def.editable, def["default"], "{http://www.adullact.org/parapheur/metadata/1.0}" + def.id);
            }

            retval += "</metadatas>";
            return retval;
        }

        var snippet = generateXMLSnippetForMetadatas(jsonData.metadatas);

        strmaj += snippet;

        strmaj += "<groups>";
        for each (group in jsonData.groups) {
            strmaj += "<group>" + "workspace://SpacesStore/" + group + "</group>";
        }
        strmaj += "</groups>" +
            "<groupsFilters>";
        for each (group in jsonData.groupsFilters) {
            strmaj += "<group>" + "workspace://SpacesStore/" + group + "</group>";
        }
        strmaj += "</groupsFilters>" +
            "<digitalSignatureMandatory>" + digitalSignatureMandatory + "</digitalSignatureMandatory>" +
            "<readingMandatory>" + readingMandatory + "</readingMandatory>" +
            "<circuitHierarchiqueVisible>" + circuitHierarchiqueVisible + "</circuitHierarchiqueVisible>" +
            "<includeAttachments>" + includeAttachments + "</includeAttachments>" +
            "<multiDocument>" + multiDocument + "</multiDocument>" +
            "<tdtAuto>" + isTdtAuto + "</tdtAuto>" +
            "<cachetAuto>" + isCachetAuto + "</cachetAuto>" +
            "<cachetId>" + cachetCertificate + "</cachetId>" +
            "<pastellMailsecId>" + pastellMailsec + "</pastellMailsecId>" +
            "<attest>" + hasToAttest + "</attest>" +
            "<forbidSameSig>" + forbidSameSig + "</forbidSameSig>" +
            "</MetierSousType>";
        var majsoustype = new XML(strmaj);
        newrootxml.MetierSousTypes += majsoustype;

        sstypesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + newrootxml.toXMLString();
        sstypesnode.save();
    }
}