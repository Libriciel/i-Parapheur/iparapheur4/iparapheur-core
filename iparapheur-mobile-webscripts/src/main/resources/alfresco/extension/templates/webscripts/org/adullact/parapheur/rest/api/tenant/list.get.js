var mtEnabled = multiCService.isEnabled();
var ret = [];

if (mtEnabled) {
    var collectivites = multiCService.getCollectivites();

    for(var i = 0; i < collectivites.size(); i++) {
        var c = collectivites.get(i);
        ret.push({
            tenantDomain:c.tenantDomain,
            title:c.title,
            enabled:c.enabled
        });
    }
}

model.mjson = jsonUtils.toJSONString(ret);