<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA


var id = url.templateArgs['id'];

if (is_valid_uuid(id)) {
    var dossier = search.findNode("workspace://SpacesStore/" + id);
    model.ret = dossierService.getSecureMailTemplate(dossier.nodeRef);
}