{
    "events" : [
        <#assign first = true>
        <#list logsDossier as log>
            <#if !first>,<#else><#assign first = false></#if>
            {
                "date": "${log.timestamp.toGregorianCalendar().getTime()?string("dd/MM/yyyy HH:mm:ss")}",
                "nom": "${log.nom}",
                "annotation": "${jsonUtils.encodeJSONString(log.annotation)}",
                "status": "${log.status}"
            }
        </#list>
    ]
}