importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);
importPackage(Packages.java.util);

function extractSearchData(dossier) {
    return {
        "name": dossier.properties["cm:title"],
        "nodeRef": dossier.nodeRef.getId()
    };
}

//noinspection JSUnresolvedVariable
var jsonData = jsonUtils.toObject(requestbody.content);

var filter = jsonData.filter;

if (filter == undefined) {
    filter = null;
}
else if (filter.length == 0) {
    filter = null
}

var nodeRefList;

var searchResult = [];
var reste = 0;
var newPage = 0;

nodeRefList = dossierService.searchFolder(filter, true);

if (nodeRefList != null) {
    for (var i = 0; i < nodeRefList.size(); i++) {
        dossier = search.findNode(nodeRefList.get(i));
        if (dossier != null) {
            searchResult.push(extractSearchData(dossier));
        }
    }
}

model.data = {"result": searchResult};