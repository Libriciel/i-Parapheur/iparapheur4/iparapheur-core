 <import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);
importClass(Packages.com.atolcd.parapheur.repo.annotations.Rect);
importClass(Packages.com.atolcd.parapheur.repo.annotations.Point);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA


var rules = {
    "type":{"typeOf":"string", "isValid":is_valid_annot_type()},

    "page":{"typeOf":"number"},

    "rect":{"typeOf":"object",
        "isValid":is_valid_rect()
    },
    "text":{"typeOf":"string"},
    "fillColor":{"typeOf":"string", "optional":"true"},
    "penColor":{"typeOf":"string", "optional":"true"}
};



//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];
var documentId = url.templateArgs['documentId'];


if (validate(jsonData, rules)) {

    var dossier = search.findNode("node", ["workspace", "SpacesStore", id]);
    var document = search.findNode("node", ["workspace", "SpacesStore", documentId]);

    var annot = jsonData;

    var type = annot.type;
    var page = annot.page;
    var topLeft = annot.rect.topLeft;
    var bottomRight = annot.rect.bottomRight;
    var aRect = new Rect(new Point(topLeft.x, topLeft.y), new Point(bottomRight.x, bottomRight.y));
    var a = null;

    if (type === "rect") {
        a = annotationService.getNewRectAnnotationWith(aRect, annot.penColor, annot.fillColor, annot.text, page);
    }
    else if (type === "highlight") {
        a = annotationService.getNewHighlightAnnotationWith(aRect, annot.penColor, annot.fillColor, annot.text, page);
    }
    else if (type === "text") {
        a = annotationService.getNewTextAnnotationWith(aRect, annot.picto, annot.text, page);
    }

    if (a !== null) {
        model.id = annotationService.addAnnotation(dossier.nodeRef, document.nodeRef, a).toString();
    }
}
