<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE

var userId = url.templateArgs['id'];
var groupName = url.templateArgs['group'];

//Validation de la requête
if(userId && groupName) {
    try {
        usersService.removeUserFromGroup(userId, groupName);
    } catch (e) {
        setStatusMessageCode("Erreur lors de la suppression de l'utilisateur du groupe " + groupName + " : " + e, 400);
    }
}
