<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_uuid()},
    "properties": {"typeOf":"object"}
};

var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];

if (valid_uuid(id) && validate(jsonData, rules)) {
    var dossier = search.findNode("workspace://SpacesStore/" + id);
    var bureauCourant = search.findNode("workspace://SpacesStore/" + jsonData.bureauCourant);
    dossierService.setDossierProperties(dossier.nodeRef, bureauCourant.nodeRef, jsonData.properties);
}



