<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "bureauCourant" : {"typeOf":"string","isValid":is_valid_nodeRef()},
    "dossier": {"typeOf":"string","isValid":is_valid_nodeRef()},
    "nature": {"typeOf":"string"},
    "classification": {"typeOf":"string"},
    "numero": {"typeOf":"string"},
    "date": {"typeOf":"string"},
    "objet": {"typeOf": "string"},
    "annotPub" : {"typeOf": "string"},
    "annotPriv" : {"typeOf": "string"} 
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.ret = dossierService.sendTdt(
            jsonData.bureauCourant,
            jsonData.dossier, 
            jsonData.nature, 
            jsonData.classification, 
            jsonData.numero, 
            jsonData.date, 
            jsonData.objet,
            jsonData.annotPub,
            jsonData.annotPriv);
}

