<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    var ret = [];

    // Recuperation de l'id du noeud du calque
    var idCalque = url.templateArgs['id'];

// Recherche de la liste des noeuds des signatures lies au calque
    var nodeRefs = calqueService.listCommentaires(idCalque);


    // Transformation des nodeRefs en node
    for (var i = 0 ; i < nodeRefs.size() ; i++) {
        var n  = search.findNode(nodeRefs.get(i));

        ret[i] = {
            "id": n.id,
            "idCalque": idCalque,
            "type": "commentaire",
            "texte": n.properties["cal:texte"],
            "taillePolice": n.properties["cal:taillePolice"],
            "couleurTexte": n.properties["cal:couleurTexte"],
            "coordonneeX": n.properties["cal:coordonneeX"],
            "coordonneeY": n.properties["cal:coordonneeY"],
            "page": n.properties["cal:page"],
            "postSignature": n.properties["cal:postSignature"]
        }
    }

    model.mjson = jsonUtils.toJSONString(ret);
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}