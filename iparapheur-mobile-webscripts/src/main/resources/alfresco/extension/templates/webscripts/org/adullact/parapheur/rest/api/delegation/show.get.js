//VALID
/* Default validation rules */

/**
 * Fonction utilitaire qui positionne un message d'erreur + code d'erreur
 *
 * @param message
 * @param code
 */
function setStatusMessageCode(message, code) {
    status.message = message;
    status.code = code;
    status.redirect = true;
}


function is_valid_id(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var ret = {};
var id = url.templateArgs['id'];

if(is_valid_id(id)) {
    var parapheur = search.findNode("workspace://SpacesStore/" + url.templateArgs['id']);
    if(parapheur) {
        var username = usersService.getCurrentUsername();
        if(iparapheur.isParapheurOwner(parapheur.nodeRef, username) || iparapheur.isParapheurSecretaire(parapheur.nodeRef, username) || usersService.isAdministrateur(username)) {
            ret = iparapheur.getDelegation(parapheur.nodeRef);
        } else {
            setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
        }
    } else {
        setStatusMessageCode("Bureau introuvable", 404);
    }
} else {
    setStatusMessageCode("Id du bureau invalide", 400);
}

//noinspection JSUnresolvedVariable
model.mjson = ret;