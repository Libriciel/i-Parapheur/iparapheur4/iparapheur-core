<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);


var rules = {
    "dossier": {"typeOf":"string","isValid":is_valid_nodeRef()},
    "bureauCourant" : {"typeOf":"string","isValid":is_valid_nodeRef()},
    "bureauCible" : {"typeOf":"string","isValid":is_valid_nodeRef()},
    "annotPub" : {"typeOf":"string"},
    "annotPriv" : {"typeOf":"string"}
};


//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules))
    model.ret = iparapheur.changeSignataire(jsonData.dossier, jsonData.bureauCourant, jsonData.bureauCible, jsonData.annotPub, jsonData.annotPriv);