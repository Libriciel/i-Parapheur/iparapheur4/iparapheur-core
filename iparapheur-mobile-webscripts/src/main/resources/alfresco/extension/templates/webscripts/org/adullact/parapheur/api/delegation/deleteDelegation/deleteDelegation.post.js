<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var rules = {
    "bureauCourant": {"typeOf":"string","isValid":is_valid_nodeRef()}
};

var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules))
    iparapheur.supprimerDelegation(jsonData.bureauCourant);