<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(java.util.HashMap);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "user": {"typeOf":"string"},
    "password": {"typeOf":"string"},
    "host": {"typeOf":"string"},
    "port" : {"typeOf":"string"},
    "collectivite" : {"typeOf":"string"},
    "enabled": {"typeOf": "string"},
    "services": {"typeOf": "string"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var configFiles = companyhome.childrenByXPath("/app:company_home/app:dictionary/ph:certificats/cm:archiland_configuration.xml");

        var ac = new XML(configFiles[0].properties.content.content);

        ac.user = jsonData.user;
        ac.password = jsonData.password;
        ac.host = jsonData.host;
        ac.port = jsonData.port;
        ac.collectivite = jsonData.collectivite;
        ac.enabled = jsonData.enabled;
        var services = jsonData.services.split('\n');

        delete ac.services;

        var list = new XMLList("<services></services>");
        for (var i=0; i<services.length; i++) {
            if (services[i].length > 0) {
                var service = services[i].split(":")[0];
                var service_uid = services[i].split(":")[1];

                list.services += new XML('<service id="'+service_uid+'">'+service+"</service>");
            }
        }

        ac.archiland += list;

        configFiles[0].properties.content.content = ac.toXMLString();

        archilandConnector.forceConfigReload();
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}