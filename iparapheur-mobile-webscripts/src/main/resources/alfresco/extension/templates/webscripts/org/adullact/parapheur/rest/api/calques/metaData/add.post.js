<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "qnameMD": {"typeOf":"string"},
    "coordonneeX": {"typeOf":"string"},
    "coordonneeY": {"typeOf":"string"},
    "page": {"typeOf":"string"},
    "postSignature": {"typeOf":"boolean"},
    "taillePolice": {"typeOf":"number", "optional": true}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {

        var id = url.templateArgs['id'];
        var fontSize = jsonData.taillePolice ? jsonData.taillePolice : 12;
        var noderef = calqueService.addMetadata(id, jsonData.qnameMD, fontSize);

        calqueService.addCoordonnees(noderef, jsonData.coordonneeX, jsonData.coordonneeY, jsonData.page, jsonData.postSignature);

        var nodeCalque = search.findNode(noderef);
        model.idMeta = nodeCalque.id;
    }
    else {
        setStatusMessageCode("Invalid data sent, expecting : \n" + jsonUtils.toJSONString(rules), 400);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}