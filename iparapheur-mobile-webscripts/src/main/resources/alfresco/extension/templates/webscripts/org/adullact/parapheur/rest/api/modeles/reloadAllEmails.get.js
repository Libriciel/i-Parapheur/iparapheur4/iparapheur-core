/**
 * Recharger tous les templates e-mail par défaut
 */

importClass(Packages.java.util.HashMap);
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var emailnodes = companyhome.childrenByXPath("./app:dictionary/app:email_templates/*[like(@cm:name, 'parapheur*')]");
if (emailnodes == null) {
    throw new Error("Les templates email i-parapheur sont introuvables.");
}

//var printnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ args.id +".xml']");
var printnodes = companyhome.childrenByXPath("./app:dictionary/app:content_templates/*[like(@cm:name, 'parapheur*')]");
if (printnodes == null) {
    throw new Error("Les templates i-parapheur sont introuvables.");
}

//    var confHome = null;
//    confHome = companyhome.childrenByXPath("/app:company_home/app:dictionary/app:email_templates")[0];

//var montempl = null;
var templateName = "";
var taille = emailnodes.length;

for (var i=0; i<taille; i++) {
    
    templateName = emailnodes[i].properties.name;

//    var montempl = confHome.childByNamePath(templateName);
//    if (!montempl) {
//        montempl = confHome.createFile(templateName);
//        montempl.properties.name = templateName;
//        montempl.properties.title = templateName;
//        montempl.properties.encoding = "UTF-8";
//        montempl.properties.mimetype = "text/plain";
//    }
    // fopen inexistant en javascript  var fh = fopen("/opt/iParapheur/tomcat/webapps/alfresco/WEB-INF/classes/alfresco/module/parapheur/bootstrap/" + templateName);
    emailnodes[i].content = iparapheur.getTextContentFromBootstrapFile(templateName);
    emailnodes[i].save();
}


//var montempl = null;
templateName = "";
taille = printnodes.length;

for (var i=0; i<taille; i++) {

    templateName = printnodes[i].properties.name;

//    var montempl = confHome.childByNamePath(templateName);
//    if (!montempl) {
//        montempl = confHome.createFile(templateName);
//        montempl.properties.name = templateName;
//        montempl.properties.title = templateName;
//        montempl.properties.encoding = "UTF-8";
//        montempl.properties.mimetype = "text/plain";
//    }
    // fopen inexistant en javascript  var fh = fopen("/opt/iParapheur/tomcat/webapps/alfresco/WEB-INF/classes/alfresco/module/parapheur/bootstrap/" + templateName);
    printnodes[i].content = iparapheur.getTextContentFromBootstrapFile(templateName);
    printnodes[i].save();
}
