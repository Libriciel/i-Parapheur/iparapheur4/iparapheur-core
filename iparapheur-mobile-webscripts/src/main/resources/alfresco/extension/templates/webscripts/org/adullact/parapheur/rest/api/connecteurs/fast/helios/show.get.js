<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var parameters = fastConfig.getHeliosParameters();

    ret.active = parameters.active;

    if (ret.active == undefined) {
        ret.active = 'true';
    }

    ret.name = parameters.name;
    ret.server = parameters.server;
    ret.port = parameters.port;
    ret.password = parameters.password;

    ret.isPwdGoodForPkcs = parameters.isPwdGoodForPkcs;
    if (parameters.isPwdGoodForPkcs == "ok") {
        ret.dateLimite = parameters.dateLimite;
    } else {
        ret.dateLimite = "";
    }

    ret.collectivite = parameters.collectivite;
    ret.parapheur = parameters.parapheur;
    ret.pPolicyIdentifierID = parameters.pPolicyIdentifierID;
    ret.pPolicyIdentifierDescription = parameters.pPolicyIdentifierDescription;
    ret.pPolicyDigest = parameters.pPolicyDigest;
    ret.pSPURI = parameters.pSPURI;
    ret.pCity = parameters.pCity;
    ret.pPostalCode = parameters.pPostalCode;
    ret.pCountryName = parameters.pCountryName;
    ret.pClaimedRole = parameters.pClaimedRole;
    ret.cinematique = parameters.cinematique;
    ret.check_freq = parameters.check_freq; // minutes
//ret.time_to_consider_file_in_error = parameters.time_to_consider_file_in_error; // jours
    ret.time_to_consider_file_not_pickable = parameters.time_to_consider_file_not_pickable; // jours
    
    model.mjson = JSON.stringify(ret);


} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}