<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);


var rules = {
    "dossierRef":{"typeOf":"string","isValid":is_valid_nodeRef()},
    "documentRef":{"typeOf":"string","isValid":is_valid_nodeRef()},
    "numPage":{"typeOf":"number"}
};

var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.res = dossierService.getImageForDocument(jsonData.dossierRef, jsonData.documentRef, jsonData.numPage);
}