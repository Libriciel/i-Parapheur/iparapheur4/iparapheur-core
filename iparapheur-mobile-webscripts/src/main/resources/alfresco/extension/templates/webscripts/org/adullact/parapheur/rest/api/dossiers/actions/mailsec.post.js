<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var rules = {
    "attachments": {"typeOf":"array","isValid":is_valid_uuid()},
    "destinataires": {"typeOf":"array"},
    "destinatairesCC": {"typeOf":"array"},
    "destinatairesCCI": {"typeOf":"array"},
    "objet": {"typeOf":"string"},
    "message": {"typeOf":"string"},
    "password":{"typeOf":"string"},
    "showpass":{"typeOf":"boolean"},
    "annexesIncluded": {"typeOf": "boolean"},
    "includeFirstPage": {"typeOf": "boolean"}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];

//Test de la validité de l'id
if(valid_uuid(id) && validate(jsonData, rules)) {
    jsonData.type="dossier";
    jsonData.action="MAILSEC";
    jsonData.id=id;
    jsonData.username=usersService.getCurrentUsername();
    //Creation du bureau et récupération de l'id pour le retour
    model.ret = worker.sendWorker(jsonUtils.toJSONString(jsonData), id);
} else {
    setStatusMessageCode("ID du dossier invalide", 400);
}

