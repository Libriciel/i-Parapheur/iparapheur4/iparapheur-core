<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var jsonData = JSON.parse(requestbody.content);

var rules = {
    "username": {"typeOf": "string"},
    "content": {"typeOf": "string"}
};

var ret = "";

if (validate(jsonData, rules)) {
    ret = iparapheur.changeUserSignature(jsonData.username, jsonData.content);
}

model.ret = ret;