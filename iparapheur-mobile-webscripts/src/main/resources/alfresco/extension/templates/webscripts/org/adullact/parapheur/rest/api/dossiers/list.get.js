/**
 *  renvoie la liste des dossiers présents dans le parapheur respectant
 * les filtres passés en paramètre (pour l'admin il n'y aura pas de filtre)
 * (pour un utilisateur, seulement ceux qu'il peut lire).
 *
 * Paramètres possibles (optionnels) :
 *     - asc : bool
 *
 *     - fullText : String
 *         OU
 *     - bureau : id
 *     - page : int
 *     - pageSize : int
 *     - corbeilleName : string
 *     - sort : String (property Alfresco, ex. cm:name)
 *     - skipped : int (#dossiers passés (droits))
 *     - filter : String json :
 *         {"types" : [String],
 *          "sousTypes" : [String],
 *          "created : "[MIN TO MAX]",
 *          "bannette" : String, (nom de la banette ou "no-corbeille" ou "no-bureau")
 *          "title" : String,
 *          "metadatas : [String] (property Alfresco, ex. cu:montant)
 *         }
 *
 * Retour : [dossiers] avec dossier :
 *     {"id" : String,
 *      "title" : String,
 *      "type" : String,
 *      "sousType" : String,
 *      "actionDemandee" : String, (VISA, SIGNATURE, ...)
 *      "bureauName" : String,
 *      "banetteName" : String,
 *      "dateEmission" : timestamp,
 *      "dateLimite" : timestamp,
 *      "isRead" : bool,
 *      "hasRead" : bool,
 *      "readingMandatory" : bool,
 *      "isSent" : bool, (pour enCoursTdt, mailsec, ...)
 *      "actions" : [String] (ARCHIVER, SUPPRIMER, EDITER, ...)
 *     }

 **/

var asAdmin = args['asAdmin'];

if(asAdmin) {
    model.mjson = dossierService.handleListDossiersAsAdmin(args["bureau"], args["type"], args["sousType"], args["title"], args["showOnlyCurrent"], args["showOnlyLate"], args["beforeEmit"], args["staticSince"]);
} else {
    // Pour un utilisateur, utiliser les filtres
    var pageSize= args["pageSize"] != undefined ? +args["pageSize"] : 10;
    var propSort = args["sort"] != undefined ? args["sort"] : "cm:title";
    var asc = args["asc"] != undefined ? args["asc"] : "true";
    var parentName = args["corbeilleName"] == undefined ? "a-traiter" : args["corbeilleName"];
    var skipped = args["skipped"] !== undefined ? +args["skipped"] : 0;
    var page = args["page"] !== undefined ? +args["page"] : 0;
    var metas = args["metas"] != undefined ? jsonUtils.toObject(args["metas"]) : {};
    var filters = args["filter"] != undefined ? jsonUtils.toObject(args["filter"]) : {};
    var pendingFile = args["pendingFile"] !== undefined ? +args["pendingFile"] : 0;

    model.mjson = dossierService.handleListDossiers(args["bureau"], parentName, pageSize, filters, propSort, metas, asc, page, pendingFile, skipped);
}

