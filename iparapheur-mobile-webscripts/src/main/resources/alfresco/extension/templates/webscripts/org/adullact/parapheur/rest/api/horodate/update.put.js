<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(java.util.HashMap);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "name": {"typeOf":"string"},
    "port" : {"typeOf":"string"},
    "server" : {"typeOf":"string"},
    "algorithme": {"typeOf": "string"},
    "idclient": {"typeOf": "string"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var parameters = new HashMap();

        parameters.name = "" + jsonData.name;
        parameters.server = "" + jsonData.server;
        parameters.port = "" + jsonData.port;
        parameters.algorithme = "" + jsonData.algorithme;
        parameters.idclient = "" + jsonData.idclient;

        horodateConfig.setHorodatageParameters(parameters);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
