<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var rules = {
    "notifications":{"typeOf":"array"}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];

//Test de la validité de l'id
if(valid_uuid(id)) {
    var dossier =  search.findNode("node", ["workspace", "SpacesStore", id]);
    var bureaux = [];
    for(var i = 0; i < jsonData.notifications.length; i++) {
        bureaux.push("workspace://SpacesStore/" + jsonData.notifications[i].id);
    }
    dossierService.setListeNotification(dossier.nodeRef, bureaux);
} else {
    setStatusMessageCode("ID du dossier invalide", 400);
}