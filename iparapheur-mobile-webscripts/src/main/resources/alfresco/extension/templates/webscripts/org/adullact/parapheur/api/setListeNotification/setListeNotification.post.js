<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var rules = {
    "dossier":{"typeOf":"string","isValid":is_valid_nodeRef()},
    "bureaux":{"typeOf":"array","isValid":is_valid_nodeRef()}
};


//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    dossierService.setListeNotification(jsonData.dossier, jsonData.bureaux);
}