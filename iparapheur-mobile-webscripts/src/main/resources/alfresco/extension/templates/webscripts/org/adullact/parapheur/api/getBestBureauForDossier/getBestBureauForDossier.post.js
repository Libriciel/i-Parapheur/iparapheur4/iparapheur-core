<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "dossier": {"typeOf": "string", "isValid": is_valid_nodeRef()},
    "username": {"typeOf": "string"}
};

var jsonData = JSON.parse(requestbody.content);

var best = null;

var data = {};

if (validate(jsonData, rules)) {
    var username = jsonData.username;
    var dossier = search.findNode("node", ["workspace", "SpacesStore", jsonData.dossier.substring(24)]);

    var hasSecretaire = false;
    var isSecretaire = false;
    if(dossier !== null) {
        var parentParapheur = iparapheur.getParentParapheur(dossier.nodeRef);

        if (iparapheur.isParapheurOwner(parentParapheur, username)) {
            best = parentParapheur;
            hasSecretaire = iparapheur.hasSecretaire(parentParapheur);
        } else if (iparapheur.isParapheurSecretaire(parentParapheur, username)) {
            best = parentParapheur;
            hasSecretaire = true;
            isSecretaire = true;
        } else {
            var delegateP = iparapheur.getParapheursOnDelegationPath(parentParapheur);
            for (var i = 0; i < delegateP.size(); i++) {
                if (iparapheur.isParapheurOwner(delegateP.get(i), username)) {
                    best = delegateP.get(i);
                    hasSecretaire = iparapheur.hasSecretaire(parentParapheur);
                    break;
                }
            }
            if(best === null) {
                best = iparapheur.getOwnedParapheur(username);
            }
        }
        if (best !== null) {
            var parapheur = iparapheur.getParapheurAsScriptNode(best);
            logger.log(parapheur.nodeRef);
            var path = parapheur.qnamePath;
            tmp = {
                shortName: parapheur.properties["cm:name"],
                name: parapheur.properties["cm:title"],
                nodeRef: parapheur.nodeRef,
                description: "",
                image: "",
                collectivite: "",
                a_traiter: companyhome.childrenByXPath(path + "/ph:a-traiter")[0].properties["ph:childCount"],
                en_retard: companyhome.childrenByXPath(path + "/ph:en-retard")[0].properties["ph:childCount"],
                retournes: companyhome.childrenByXPath(path + "/ph:retournes")[0].properties["ph:childCount"],
                a_archiver: companyhome.childrenByXPath(path + "/ph:a-archiver")[0].properties["ph:childCount"],
                a_transmettre: companyhome.childrenByXPath(path + "/ph:en-preparation")[0].properties["ph:childCount"],
                hasSecretaire: hasSecretaire,
                show_a_venir : parapheur.properties["ph:show-a-venir"],
                habilitation : {
                    secretariat : parapheur.properties["ph:hab_secretariat"],
                    archivage : parapheur.properties["ph:hab_archivage"],
                    enchainement : parapheur.properties["ph:hab_enchainement"],
                    traiter : parapheur.properties["ph:hab_traiter"],
                    transmettre : parapheur.properties["ph:hab_transmettre"]
                }
            };
            data = {
                "secretaire": isSecretaire,
                "bureau": tmp
            };
        }
    }
}

model.mjson = jsonUtils.toJSONString(data);