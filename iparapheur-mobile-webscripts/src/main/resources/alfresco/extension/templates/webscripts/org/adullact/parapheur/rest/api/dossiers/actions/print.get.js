<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var id = url.templateArgs['id'];
var attachments = args["attachments"];

model.dossier =  search.findNode("node", ["workspace", "SpacesStore", id]);
model.includeFirstPage = args["includeFirstPage"];
model.attachments = [];
if(attachments) {
    attachments = attachments.split(",");
    for(var i = 0; i < attachments.length; i++) {
        model.attachments.push(search.findNode("node", ["workspace", "SpacesStore", attachments[i]]))
    }
}




