/* gets the owned desk */

var username = person.properties.userName;

var isSecretaire = false;

parapheurs = iparapheur.getOwnedParapheursAsScriptNode(username);

//Si responsable d'aucun parapheur, on récupere les bureaux dont l'utilisateur est secretaire
if(parapheurs.size() == 0) {
    parapheurs = iparapheur.getSecretariatParapheurAsScriptNode(username);
    if(parapheurs.size() > 0) {
        isSecretaire = true;
    }
}

data = [];

/* iterate over the desk list and get the relevant data */
for (var i=0; i<parapheurs.size(); i++) {
    var parapheur = parapheurs.get(''+i);
    var path = parapheur.qnamePath;
    var tmp;

    var hasSecretaire = iparapheur.hasSecretaire(parapheur.nodeRef);
    
    if(!isSecretaire) {
        tmp = {
            shortName:parapheur.properties["cm:name"],
            name:parapheur.properties["cm:title"],
            nodeRef:parapheur.nodeRef,
            description : "",
            image : "",
            collectivite : "",
            a_traiter : companyhome.childrenByXPath(path + "/ph:a-traiter")[0].properties["ph:childCount"],
            en_retard : companyhome.childrenByXPath(path + "/ph:en-retard")[0].properties["ph:childCount"],
            retournes : companyhome.childrenByXPath(path + "/ph:retournes")[0].properties["ph:childCount"],
            a_archiver : companyhome.childrenByXPath(path + "/ph:a-archiver")[0].properties["ph:childCount"],
            dossiers_delegues : companyhome.childrenByXPath(path + "/ph:dossiers-delegues")[0].properties["ph:childCount"],
            a_transmettre: companyhome.childrenByXPath(path + "/ph:en-preparation")[0].properties["ph:childCount"],
            hasSecretaire : hasSecretaire,
            show_a_venir : parapheur.properties["ph:show-a-venir"],
            habilitation : {
                secretariat : parapheur.properties["ph:hab_secretariat"],
                archivage : parapheur.properties["ph:hab_archivage"],
                enchainement : parapheur.properties["ph:hab_enchainement"],
                traiter : parapheur.properties["ph:hab_traiter"],
                transmettre : parapheur.properties["ph:hab_transmettre"]
            }
        };
    } else {
        tmp = {
            shortName:parapheur.properties["cm:name"],
            name:parapheur.properties["cm:title"],
            nodeRef:parapheur.nodeRef,
            description : "",
            image : "",
            collectivite : "",
            a_traiter : companyhome.childrenByXPath(path + "/ph:secretariat")[0].properties["ph:childCount"],
            retournes : companyhome.childrenByXPath(path + "/ph:a-imprimer")[0].properties["ph:childCount"],
            a_archiver : companyhome.childrenByXPath(path + "/ph:a-archiver")[0].properties["ph:childCount"],
            dossiers_delegues : companyhome.childrenByXPath(path + "/ph:dossiers-delegues")[0].properties["ph:childCount"],
            a_transmettre: companyhome.childrenByXPath(path + "/ph:en-preparation")[0].properties["ph:childCount"],
            hasSecretaire : hasSecretaire,
            show_a_venir : parapheur.properties["ph:show-a-venir"],
            habilitation : {
                secretariat : parapheur.properties["ph:hab_secretariat"],
                archivage : parapheur.properties["ph:hab_archivage"],
                enchainement : parapheur.properties["ph:hab_enchainement"],
                traiter : parapheur.properties["ph:hab_traiter"],
                transmettre : parapheur.properties["ph:hab_transmettre"]
            }
        };
    }

    data[i] = tmp;

}

/* generate the java object to stringify */
model.data = { 
    "secretaire" : isSecretaire,
    "bureaux": data
};

model.mjson = jsonUtils.toJSONString(model.data)

model.data = data;