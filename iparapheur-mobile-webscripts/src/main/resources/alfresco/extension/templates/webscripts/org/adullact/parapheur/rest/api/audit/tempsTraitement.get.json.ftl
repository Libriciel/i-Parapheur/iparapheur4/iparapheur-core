<#setting number_format="computer">
{
"data": [
<#assign first = true>
<#list dossiers?keys?sort as key>
    <#if first == false>
    ,
    </#if>
{
"key": "${key}",
"value": "${dossiers[key]?string.computer}"
}
    <#assign first = false>
</#list>
]
}
