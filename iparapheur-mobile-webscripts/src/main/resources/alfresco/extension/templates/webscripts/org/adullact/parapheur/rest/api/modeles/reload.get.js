<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    var id = url.templateArgs['id'];
    var node = search.findNode("node", ["workspace", "SpacesStore", id]);

    node.content = iparapheur.getTextContentFromBootstrapFile(node.name);
    node.save();

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
