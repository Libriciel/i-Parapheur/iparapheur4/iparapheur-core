[
<#list mjson as node>
    { 
        "firstName": "${node.properties["cm:firstName"]}", 
        "lastName": "${node.properties["cm:lastName"]}", 
        "userName": "${node.properties["cm:userName"]}"
    }<#if node_has_next>,</#if>
</#list>
]