
//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    var obj = {
        status: runtime.statusXemelios()
    };

    model.mjson = jsonUtils.toJSONString(obj);
}