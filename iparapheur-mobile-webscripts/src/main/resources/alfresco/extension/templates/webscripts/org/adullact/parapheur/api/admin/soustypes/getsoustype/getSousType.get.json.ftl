{
    "name" : "${ssType.ID}",
    "description" : "${ssType.Desc}",
    "type" : "${type}",
    "public" : <#if ssType.visibility = "public">true<#else>false</#if>,
    "bureaux" : 
        [
            <#assign first = true>
            <#list ssType.parapheurs as parapheur>
                <#if first == false>
                    ,
                </#if>
                ${parapheur.nodeRef}
                <#assign first = false>
            </#list>
        ],
    "groupes" :
        [
            <#assign first = true>
            <#list ssType.groups as group>
                 <#if first == false>
                    ,
                </#if>
                ${group.nodeRef}
                <#assign first = false>
            </#list>
        ],
    "circuit" : "${ssType.Circuit}",
    "script" : "${ssType.Script}",
    "mustSign" : ${ssType.digitalSignatureMandatory},
    "mustRead" : ${ssType.readingMandatory},
    "circuitVisible" : ${ssType.circuitHierarchiqueVisible},
    "includeAttachementsWhenPrintingOrArchive" : ${ssType.includeAttachments},
    "metaData" :
        [
            <#assign first = true>
            <#list savedMds?keys as md>
                <#if first == false>
                    ,
                </#if>
                "${metadatas[md].name.toString()}"
                <#assign first = false>
            </#list>

        ],
    "layers" :
        [
            <#assign first = true>
            <#list ssType.calques as calque>
                <#if first == false>
                    ,
                </#if>
                "${calque.nodeRef}"
                <#assign first = false>
            </#list>
        ],

    "visibilityFilter" : "${ssType.visibilityFilter}",

    "parapheursFilters" :
        [
            <#assign first = true>
            <#list ssType.parapheursFilters as parapheursFilter>
                <#if first == false>
                    ,
                </#if>
                "${parapheursFilter}"
                <#assign first = false>
            </#list>
        ],

    "groupsFilters" :
        [
            <#assign first = true>
            <#list ssType.groupsFilters as groupsFilter>
                <#if first == false>
                    ,
                </#if>
                "${groupsFilter}"
                <#assign first = false>
            </#list>
        ]

}
                    