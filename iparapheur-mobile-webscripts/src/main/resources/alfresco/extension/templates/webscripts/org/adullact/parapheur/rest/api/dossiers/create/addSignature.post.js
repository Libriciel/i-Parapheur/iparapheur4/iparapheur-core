<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "name": {"typeOf":"string"},
    "content" : {"typeOf":"string"}
};

var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];

//Test de la validité de l'id
if(valid_uuid(id) && validate(jsonData, rules)) {
    var dossier = search.findNode("workspace://SpacesStore/" + id);
    model.ret = dossierService.addSignature(dossier.nodeRef, jsonData.name, jsonData.content);
}