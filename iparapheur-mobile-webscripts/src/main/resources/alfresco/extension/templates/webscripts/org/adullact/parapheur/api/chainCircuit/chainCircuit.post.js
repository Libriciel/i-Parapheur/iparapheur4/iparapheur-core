<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var jsonData = JSON.parse(requestbody.content);

var rules = {
    "dossier":{"typeOf":"string", "isValid":is_valid_nodeRef()},
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_nodeRef()},
    "type":{"typeOf":"string"},
    "sousType":{"typeOf":"string"}
};

if (validate(jsonData, rules))
    dossierService.chainCircuit(jsonData.dossier, jsonData.bureauCourant, jsonData.type, jsonData.sousType);