<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var rules = {
    "annotPub": {"typeOf":"string", "optional":"true"},
    "annotPriv": {"typeOf":"string", "optional":"true"},
    "bureauCourant": {"typeOf":"string", "isValid" : is_valid_uuid()}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];

//Test de la validité de l'id
if(valid_uuid(id) && validate(jsonData, rules)) {
    jsonData.type="dossier";
    jsonData.action="SECRETARIAT";
    jsonData.id=id;
    jsonData.username=usersService.getCurrentUsername();
    //Creation du bureau et récupération de l'id pour le retour
    model.ret = worker.sendWorker(jsonUtils.toJSONString(jsonData), id);
} else {
    setStatusMessageCode("ID du dossier invalide", 400);
}