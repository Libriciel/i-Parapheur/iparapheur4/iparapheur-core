<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

//noinspection JSUnresolvedVariable
var type = url.templateArgs['type'];
var sousType = url.templateArgs['sousType'];

model.mjson = typesService.getMetaDonneesApi(type, sousType);