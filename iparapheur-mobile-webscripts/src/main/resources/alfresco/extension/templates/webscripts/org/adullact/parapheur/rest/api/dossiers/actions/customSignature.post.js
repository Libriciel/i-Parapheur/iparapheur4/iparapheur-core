<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//"; //Closed tag required in JS for validation in IntelliJ IDEA


// Checking data sent

var rules = {
    "x" : {"typeOf":"number"},
    "y" : {"typeOf":"number"},
    "width" : {"typeOf":"number"},
    "height" : {"typeOf":"number"},
    "page" : {"typeOf":"number"}
};

var jsonData = JSON.parse(requestbody.content);
var dossierId = url.templateArgs['dossierId'];
var documentId = url.templateArgs['documentId'];

// Checks and result

if (!validate(jsonData, rules)) {
    setStatusMessageCode("Invalid data sent, expecting : \n" + jsonUtils.toJSONString(rules), 400);
}
else if (!valid_uuid(dossierId)) {
    setStatusMessageCode("1002", 400);
}
else if (!valid_uuid(documentId)) {
    setStatusMessageCode("1003", 400);
}
else {
    var dossier = search.findNode("workspace://SpacesStore/" + dossierId);
    var document= search.findNode("workspace://SpacesStore/" + documentId);
    dossierService.addCustomSignatureToDocument(dossier.nodeRef, document.nodeRef, jsonData.x, jsonData.y, jsonData.width, jsonData.height, jsonData.page);
}