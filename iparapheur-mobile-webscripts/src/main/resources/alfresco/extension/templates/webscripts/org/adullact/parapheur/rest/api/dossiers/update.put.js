<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE
//VALID

var properties = {
    title : "cm:title",
    type : "ph:typeMetier",
    sousType : "ph:soustypeMetier",
    dateLimite : "ph:dateLimite",
    isSignPapier : "ph:signature-papier",
    acteursVariables: "ph:acteurs-variables",
    xPathSignature : "ph:xpathSignature"
};

var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];
var bureauCourantId = args.bureauCourant;

if (!valid_uuid(id)) {
    setStatusMessageCode("1001", 400);
}
else if (!valid_uuid(bureauCourantId)) {
    setStatusMessageCode("1002", 400);
}
else {
    var props = {};
    for(var i in properties) {
        if(jsonData.hasOwnProperty(i)) {
            props[properties[i]] = jsonData[i];
        } else {
            props[properties[i]] = null;
        }
    }
    for(var j in jsonData.metadatas) {
        var val = jsonData.metadatas[j].value;
        if(jsonData.metadatas[j].values) {
            val = val !== "" ? val : null;
        }
        props[j] = val;
    }
    if(jsonData.hasOwnProperty("visibility")) {
        props["ph:public"] = jsonData["visibility"] === "public";
        props["ph:confidentiel"] = jsonData["visibility"] === "confidentiel";
    }

    var dossier = search.findNode("workspace://SpacesStore/" + id);
    var bureauCourant = search.findNode("workspace://SpacesStore/" + bureauCourantId);
    var result = jsonUtils.toObject(dossierService.setDossierProperties(dossier.nodeRef, bureauCourant.nodeRef, props));

    if (result['statusCode'] == 200) {

        // Réorganisation des documents
        var documents = [];
        if (jsonData.hasOwnProperty("documents")) {
            for (var k = 0; k < jsonData["documents"].length; k++) {
                documents.push({
                    id : jsonData["documents"][k].id,
                    isMainDocument : jsonData["documents"][k].isMainDocument
                });
            }
            if (documents.length != 0) {
                dossierService.reorderDocuments(dossier.nodeRef, jsonUtils.toJSONString(documents));
            }
        }
    }
    else {
        setStatusMessageCode(result['errorMessage'], result['statusCode']);
    }
}