//<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

/*<import resource="classpath:alfresco/extension/shared/SearchUtils.js">
 // syntaxic sugar </import> */

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);
importPackage(Packages.java.util);

/**
 * isUserOwnerOf
 * @param username nom d'utilisateur
 * @param nodeRef nodeRef du bureau
 * @returns true si username fait partie des proprietaires du bureau
 */
function isUserOwnerOf(username, nodeRef) {
    var ownersList = iparapheur.getParapheurOwners(new NodeRef(nodeRef));
    var retval = false;

    if (ownersList != null) {
        retval = ownersList.contains(username);
    }

    return retval;
}

function isUserSecretaireOf(username, nodeRef) {
    var secretairesList = iparapheur.getParapheurSecretaires(new NodeRef(nodeRef));
    var retval = false;

    if (secretairesList != null) {
        retval = secretairesList.contains(username);
    }

    return retval;
}

/**
 * getActionDemandee
 * récupère l'action demandee de l'étape courante du circuit
 * @param node ScriptNode du dossier
 */
function getActionDemandee(node) {
    var etape = iparapheur.getCurrentEtapeCircuit(node.nodeRef);
    if (etape != null) {
        return etape.getActionDemandee();
    }
    else {
        return null;
    }
}


/**
 * Returns a data object that can be passed to the paged results template
 * for rendering.
 *
 * @param nodes: The complete result set nodes
 * @param index: the start index from which results should be returned
 * @param count: the number of elements that should be returned
 * @param extractDataFn: The function that extracts the data to be returned
 *                       for each node in the final data set.
 *                       The functions signature is name(index, node).
 *
 * Returns an array containing all topics found in the passed array.
 * Filters out non-fm:topic nodes.
 */
function getPagedResultsData(nodes, index, count, extractDataFn)
{
   var items = new Array();
   var i;
   var added = 0;
   for (i = index; i < nodes.length && added < count; i++)
   {
      items.push(extractDataFn(nodes[i]));
      added++;
   }

   return (
   {
      "total" : nodes.length,
      "pageSize" : count,
      "startIndex" : index,
      "itemCount" : items.length,
      "items": items
   });
}


function getPagedDossiersForBureau(bureauNode, parentName, count, index, filters) {
    var nodeRefList;
    //nodeRefList = mobile.getDossiers(bureauNode.qnamePath + "/ph:a-traiter/*", count, index, filters);
    nodeRefList = dossierService.getDossiers(bureauNode.nodeRef, parentName, count, index, filters);
    var dossiersList = [];
    if (nodeRefList != null) {
        resultsize = nodeRefList.length;
        for (var i = 0; i < nodeRefList.size() && i < count; i++) {
            dossier = search.findNode(nodeRefList.get(i));
            dossiersList.push(extractDossierData(dossier, bureauNode));
        }
    }
    return dossiersList;
}

function extractDossierData(dossier, bureauNode) {
    var doss = {
            "dossierRef"     : dossier.nodeRef.toString(),
            "titre"          : dossier.properties["cm:title"],
            "actionDemandee" : getActionDemandee(dossier),
            "type"           : dossier.properties["ph:typeMetier"],
            "sousType"       : dossier.properties["ph:soustypeMetier"],
            "dateLimite"     : dossier.properties["ph:dateLimite"],
            "dateCreation"   : dossier.properties["cm:created"],
            "bureauCourant"  : dossier.parent.parent.properties["cm:title"],
            "corbeilleParent": dossier.parent.properties["cm:title"],
            "locked"         : dossier.properties["ph:in-batch-queue"],
            "isSignPapier"   : dossier.properties["ph:signature-papier"],
            "isReadMandatory": dossier.properties["ph:reading-mandatory"],
            "hasRead"        : dossierService.hasReadDossier(dossier.nodeRef),
            "isRead"         : dossierService.isDossierLu(dossier.nodeRef),
            "isActeurCourant": iparapheur.isOwnerOrDelegateOfDossier(bureauNode.nodeRef, dossier.nodeRef),
            "actions"        : {
                "remorse"    : dossierService.canRemorse(dossier.nodeRef, bureauNode.nodeRef),
                "delete"     : dossierService.canDelete(dossier.nodeRef),
                "reject"     : dossierService.canReject(dossier.nodeRef, bureauNode.nodeRef),
                "sign"       : dossierService.canSign(dossier.nodeRef, bureauNode.nodeRef),
                "raz"        : dossierService.canRAZ(dossier.nodeRef, bureauNode.nodeRef),
                "edit"       : dossierService.canEdit(dossier.nodeRef, bureauNode.nodeRef),
                "archive"    : dossierService.canArchive(dossier.nodeRef, bureauNode.nodeRef),
                "secretary"  : dossierService.canSecretariat(dossier.nodeRef, bureauNode.nodeRef)
            }
            
        };
    if(doss.actionDemandee == "TDT") {
        doss.isSend = dossierService.isSendTdt(doss.dossierRef);
    } else if(doss.actionDemandee == "MAILSEC") {
        doss.isSend = dossierService.isSendMailSec(doss.dossierRef);
    } else {
        doss.isSend = false;
    }
        
    /**
     * 		<param name="hash_count" value="1" />
     <param name="hash_1"  value="<%= dlg.getShaDigestToSign() %>" />
     <param name="pesid_1" value="<%= dlg.getPesId() %>" />
     <param name="pespolicyid_1" value="<%= dlg.getPolicyIdentifierID() %>" />
     <param name="pespolicydesc_1" value="<%= dlg.getPolicyIdentifierDescription() %>" />
     <param name="pespolicyhash_1" value="<%= dlg.getPolicyDigest() %>" />
     <param name="pesspuri_1" value="<%= dlg.getSPURI() %>" />
     <param name="pescity_1" value="<%= dlg.getCity() %>" />
     <param name="pespostalcode_1" value="<%= dlg.getPostalCode() %>" />
     <param name="pescountryname_1" value="<%= dlg.getCountryName() %>" />
     <param name="pesclaimedrole_1" value="<%= dlg.getClaimedRole() %>" />
     <param name="p7s_1" value="<%= dlg.getP7sSignatureString() %>" />
     <param name="pesencoding_1" value="<%=dlg.getEncoding()%>" />
     <param name="format_1"  value="<%= dlg.getSignatureFormat() %>" /> <!-- cms ou xades ou pesv2 -->
     */

    return doss;
}

var code = "KO";

/* {

        bureauRef:"NodRef", // mandatory
        pageSize : 10, // optional defaults to 10
        page : 323, // optional defaults to 0
        filters : { // optional
            "cm:name" : "Tes",
            "ph:priority: "++++"
        }

   }
  */


var jsonData = jsonUtils.toObject(requestbody.content);
 /*
var rules = {
    "bureauRef":{"typeOf":"string", "isValid":is_valid_nodeRef()},
    "pageSize":{"typeOf": "number", "optional":"true"},
    "page": {"typeOf": "number", "optional":"true"}
    //"filter": {"typeOf"}

}

if (validate(jsonData, rules)) {
*/

var resultsize;

if (isUserOwnerOf(person.properties.userName, jsonData.bureauCourant) || isUserSecretaireOf(person.properties.userName, jsonData.bureauCourant)) {
    var bureauNode = search.findNode("node", ["workspace", "SpacesStore", jsonData.bureauCourant.substring(24)]);

    var filters = jsonData.filters;

    if (filters == undefined) {
        filters = null;
    }
    else if (filters.length == 0) {
        filters = null
    }

    var parentName = jsonData.parent;

    if (parentName == undefined) {
        parentName = "a-traiter";
    }

    var index = 0;
    var pageSize=10;
    var currentPage = 0;
    var propSort = "cm:title";
    var asc = "true";

    if (jsonData.pageSize != undefined) {
        pageSize = jsonData.pageSize;
    }
    
    var skipped = jsonData.skipped !== undefined ? jsonData.skipped : 0;

    if (jsonData.page != undefined) {
        index = pageSize * jsonData.page + skipped;
        currentPage = jsonData.page;
    }
    
    if(jsonData.propSort != undefined) {
        propSort = jsonData.propSort;
    }
    
    if(jsonData.asc != undefined) {
        asc = jsonData.asc;
    }
    
    var nodeRefList;
    
    var searchResult = [];
    var reste = 0;
    var newPage = 0;
    //nodeRefList = mobile.getDossiers(bureauNode.qnamePath + "/ph:a-traiter/*", count, index, filters);
    do {
        nodeRefList = dossierService.getDossiers(bureauNode.nodeRef, parentName, pageSize, index + (newPage*pageSize), filters, propSort, asc);
    
        if (nodeRefList != null) {
            if(nodeRefList.isEmpty()) {
                nodeRefList = null;
                break;
            }
            reste = nodeRefList.size();
            var toTest = Math.min(nodeRefList.size(), pageSize);
            for (var i = 0; i < toTest; i++) {
                dossier = search.findNode(nodeRefList.get(i));
                reste --;
                if(dossier != null) {
                    searchResult.push(extractDossierData(dossier, bureauNode));
                    if(searchResult.length === pageSize) {
                        break;
                    }
                } else {
                    skipped++;
                }
            }
        } else {
            nodeRefList = null;
        }
        newPage++;
    } while(searchResult.length < pageSize && nodeRefList != null)

    model.data =  {"total": searchResult.length + (reste > 0 ? 1 : 0), "page":currentPage, "dossiers": searchResult, "skipped":skipped};
    //model.data = jsonUtils.toJSONString(data);

    code = "OK";
}
else {
    model.data = {};
}

model.code = code;


