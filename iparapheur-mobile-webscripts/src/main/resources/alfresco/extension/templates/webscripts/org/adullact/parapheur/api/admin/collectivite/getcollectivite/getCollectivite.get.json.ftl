{
    "tenantDomain" : "${collectivite.tenantDomain}",
    "titre" : "${collectivite.title}",
    "description" : "${collectivite.description}",
    "siren" : "${collectivite.siren}",
    "ville" : "${collectivite.city}",
    "codePostal" : "${collectivite.postalCode}",
    "pays" : "${collectivite.country}"
}