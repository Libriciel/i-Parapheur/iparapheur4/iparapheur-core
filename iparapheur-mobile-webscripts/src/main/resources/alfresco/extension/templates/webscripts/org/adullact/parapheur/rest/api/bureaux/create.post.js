<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE
//VALID

var properties = {
    name : "cm:name",
    title : "cm:title",
    description : "cm:description",
    proprietaires : "ph:proprietaires",
    secretaires : "ph:secretaires",
    "show-a-venir" : "ph:show-a-venir",
    hab_enabled : "ph:habilitations",
    hab_secretariat : "ph:hab_secretariat",
    hab_archivage : "ph:hab_archivage",
    hab_enchainement : "ph:hab_enchainement",
    hab_traiter : "ph:hab_traiter",
    hab_transmettre : "ph:hab_transmettre",
    "delegations-possibles": "ph:delegations-possibles",
    "metadatas-visibility": "ph:metadatas-visibility"
};

var assocs = {
    hierarchie : "ph:hierarchie"
};

//Rules
var rules = {
    "name" : {
        "typeOf":"string"
    },
    "title" : {
        "typeOf":"string"
    },
    "description" : {
        "typeOf":"string",
        "optional" : true
    },
    "proprietaires" : {
        "typeOf": "array",
        "optional" : true
    },
    "secretaires" : {
        "typeOf": "array",
        "optional" : true
    },
    "show-a-venir" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "hab_secretariat" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "hab_archivage" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "hab_traiter" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "hab_transmettre" : {
        "typeOf": "boolean",
        "optional" : true
    },
    "delegations-possibles" : {
        "typeOf": "array",
        "optional" : true
    },
    "metadatas-visibility": {
        "typeOf": "array",
        "optional": true
    }
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var id;
        try {
            //Creation du bureau et récupération de l'id pour le retour
            id = iparapheur.createParapheur(jsonData.name, jsonData.title, jsonData.description).id;
            model.idBureau = id;
        } catch(e) {
            setStatusMessageCode("Erreur lors de la création du bureau : " + e, 500);
        }
        //Récupération du NodeRef du bureau nouvellement créé
        var parapheur = search.findNode("workspace://SpacesStore/" + id);
        if(is_valid_uuid(id) && parapheur != null) {
            try {
                if(jsonData.proprietaires !== undefined) {
                    var tmp = [];
                    for(var i = 0; i < jsonData.proprietaires.length; i++) {
                        tmp.push(jsonData.proprietaires[i].username);
                    }
                    jsonData.proprietaires = tmp;
                }
                if(jsonData.secretaires !== undefined) {
                    var tmpSec = [];
                    for(var j = 0; j < jsonData.secretaires.length; j++) {
                        tmpSec.push(jsonData.secretaires[j].username);
                    }
                    jsonData.secretaires = tmpSec;
                }
                //Mise à jour du bureau avec les propriétés envoyées
                iparapheur.updateBureau(parapheur.nodeRef, jsonData, properties, assocs);
            } catch (e) {
                setStatusMessageCode("Erreur lors de la définition des propriétés du bureau : " + e, 500);
            }
        } else {
            setStatusMessageCode("Impossible de récupérer le bureau nouvellement créé", 500);
        }
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}