<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {

    var ret = {};
    var parameters = srciConfig.getHeliosParameters();

    ret.proxy_account_message = parameters.proxy_account_message;

    ret.proxy_account= parameters.proxy_account;

    ret.parapheur = parameters.parapheur;
    ret.pPolicyIdentifierID = parameters.pPolicyIdentifierID;
    ret.pPolicyIdentifierDescription = parameters.pPolicyIdentifierDescription;
    ret.pPolicyDigest = parameters.pPolicyDigest;
    ret.pSPURI = parameters.pSPURI;
    ret.pCity = parameters.pCity;
    ret.pPostalCode = parameters.pPostalCode;
    ret.pCountryName = parameters.pCountryName;
    ret.pClaimedRole = parameters.pClaimedRole;

    model.mjson = JSON.stringify(ret);

} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}