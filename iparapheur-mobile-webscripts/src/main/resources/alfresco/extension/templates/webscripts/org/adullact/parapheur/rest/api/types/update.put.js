<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

//VALID
var rules = {
    "id" : {"typeOf" : "string"},
    "oldId" : {"typeOf" : "string"},
    "desc" : {"typeOf" : "string"},
    "tdtOverride" : {"typeOf" : "string"},
    "tdtNom" : {"typeOf" : "string"},
    "tdtProtocole" : {"typeOf" : "string"},
    "sigFormat" : {"typeOf" : "string"},
    "sigLocation" : {"typeOf" : "string"},
    "sigPostalCode" : {"typeOf" : "string"}
};

var typesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiertype/cm:TypesMetier.xml")[0];
if (typesnode == null){
    throw new Error("Les types techniques n'ont pas été initialisés");
}

/* create data model */
var rootxml = new XML(typesnode.content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "")); // Workaround: Mozilla Bug 336551
var newID = url.templateArgs['id'];

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Récupération du contenu JSON de la requete
var jsonData = JSON.parse(requestbody.content);


if (!usersService.isAdministrateur(username)) {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}
else if(!validate(jsonData, rules)) {
    setStatusMessageCode("Invalid data sent, expecting : \n" + jsonUtils.toJSONString(rules), 400);
}
else {
    try {
        var hasError = false;
        var oldID = jsonData.oldId;
        // Mettre à jour le noeud XML
        var newrootxml = new XML();
        newrootxml = <MetierTypes></MetierTypes>;
        for (var numero in rootxml.MetierType) {
            if (rootxml.MetierType[numero].ID.toString() != oldID) {
                if(newID === rootxml.MetierType[numero].ID.toString()) {
                    setStatusMessageCode("Le type " + newID + " existe déjà", 409);
                    hasError = true;
                    break;
                }
                newrootxml.MetierTypes +=  rootxml.MetierType[numero];
            } else {
                if(newID != oldID) {
                    workflowService.renameTypeForUse(oldID, newID);
                    dossierService.changeDossiersType(oldID, newID);
                }
                var str = new String("<MetierType><ID>"+newID+"</ID><Desc><![CDATA["+jsonData.desc+"]]></Desc><TdT><Nom>"+jsonData.tdtNom+"</Nom><Protocole>"+jsonData.tdtProtocole+"</Protocole><override>"+jsonData.tdtOverride+"</override><UriConfiguration></UriConfiguration></TdT>" +
                    "<sigFormat>" + jsonData.sigFormat + "</sigFormat>" +
                    "<sigLocation>" + jsonData.sigLocation + "</sigLocation>" +
                    "<sigPostalCode>" + jsonData.sigPostalCode + "</sigPostalCode>" +
                    "</MetierType>");
                newrootxml.MetierTypes += new XML(str);
            }
        }
        if(!hasError) {
            typesnode.content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + newrootxml.toXMLString();
            typesnode.save();

            var soustypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ oldID +".xml']")[0];

            // si newID != oldID : ça se complique, il faut aussi modifier:
            //  - le fichier des sous-types!!
            //  - le fichier de surcharge TDT !!
            if (newID != oldID) {
                if (soustypesnode != null) {
                    soustypesnode.name = newID + ".xml";
                    soustypesnode.save();
                } else {
                    var soustypeFolder = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype")[0];
                    var mycontent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<MetierSousTypes>\n</MetierSousTypes>\n";
                    var soustypeDoc = soustypeFolder.createFile(newID + ".xml");
                    soustypeDoc.content = mycontent;
                    soustypeDoc.save();
                }
                var overridetdt = companyhome.childrenByXPath("./app:dictionary/cm:metiertype/*[@cm:name='"+ oldID +"_s2low_properties.xml']")[0];
                if (overridetdt != null) {
                    // do the stuff
                    overridetdt.name = newID + "_s2low_properties.xml";
                    overridetdt.save();
                }
                // pas de clause "else": si la surcharge n'existe pas on fait rien, nib, que dalle
            }

            // Metadata propagation to subtypes
            typesService.updateSubtypesMultidocState(newID);
        }
    } catch (e) {
        setStatusMessageCode("Erreur lors de la mise à jour du type : " + e, 500);
    }
}

