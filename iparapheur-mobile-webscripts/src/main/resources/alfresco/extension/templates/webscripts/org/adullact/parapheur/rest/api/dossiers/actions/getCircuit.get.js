<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">
importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var id = url.templateArgs['id'];

//Test de la validité de l'id
if(valid_uuid(id)) {
    var dossier = search.findNode("node", ["workspace", "SpacesStore", id]);
    var circuit = {};
    var type = dossier.properties["ph:typeMetier"];
    var sousType = dossier.properties["ph:soustypeMetier"];
    if(type !== null && sousType !== null) {
        var owners = JSON.parse(iparapheur.getParapheurOwners(dossier.nodeRef));
        var isOwned = false;
        for(var j = 0; j < owners.length; j++) {
            isOwned = isOwned || owners[j].username === person.properties["cm:userName"];
        }

        var sigFormat = dossier.properties["ph:sigFormat"];
        circuit = {
            "circuit" : {
                "annotPriv" : isOwned ? iparapheur.getAnnotationPriveePrecedente(dossier.nodeRef) : "",
                "sigFormat" : sigFormat,
                "protocol" : typesService.getProtocol(type),
                "isMultiDocument": typesService.isMultiDocument(type, sousType) && (sigFormat != "PKCS#7/multiple"),
                "isDigitalSignatureMandatory" : dossier.properties["ph:digital-signature-mandatory"],
                "hasSelectionScript" : typesService.hasSelectionScript(type, sousType),
                "etapes" : []
            }
        };

        var etapes = iparapheur.getCircuit(dossier.nodeRef);
        var statusmetier = dossier.properties["ph:status-metier"];
        var rejected = false;
        if (statusmetier.match("^Rejet")) {
            rejected = true;
        }

        if (etapes != null) {
            var rejectedNum = -1;
            var foundCurrent = false;
            for (var i = 0; i < etapes.size(); i++) {
                var etape = etapes.get(i);
                if (rejected && etape.approved) {
                    rejectedNum = i;
                }
                var obj = {
                    "id" : etape.nodeRef.id,
                    "approved" : etape.approved,
                    "isCurrent" : !foundCurrent && !etape.approved,
                    "rejected" : rejectedNum === i,
                    "actionDemandee" : etape.actionDemandee,
                    "delegateur" : etape.delegateur,
                    "dateValidation" : etape.dateValidation ? etape.dateValidation.getTime() : null,
                    "annotPub" : etape.annotation,
                    "signatureEtape" : etape.signatureEtape,
                    "signataire" : etape.signataire,
                    "parapheurName" : etape.parapheurName,
                    "listeMetadatas": etape.listeMetadatas,
                    "listeMetadatasRefus": etape.listeMetadatasRefus,
                    "delegueName" : etape.delegueName,
                    "signed": etape.signed,
                    "signatureInfo" : {}
                };
                if (etape.signatureEtape != null) {
                    var infos = iparapheur.getSignatureProperties(etape);
                    var indexLastInfo = infos.size() - 1;
                    if (indexLastInfo >= 0) {
                        //On n'affiche que la dernière signature pour l'etape courante
                        obj.signatureInfo = {
                            "subject_name" : infos.get(indexLastInfo)["subject_name"],
                            "issuer_name" : infos.get(indexLastInfo)["issuer_name"],
                            "signature_date" : infos.get(indexLastInfo)["signature_date"],
                            "certificate_valid_from" : infos.get(indexLastInfo)["certificate_valid_from"],
                            "certificate_valid_to" : infos.get(indexLastInfo)["certificate_valid_to"]
                        };
                    }
                }
                if(obj.isCurrent) {
                    foundCurrent = true;
                }
                circuit.circuit.etapes.push(obj);
            }
        }
    }

    model.mjson = jsonUtils.toJSONString(circuit);
}
