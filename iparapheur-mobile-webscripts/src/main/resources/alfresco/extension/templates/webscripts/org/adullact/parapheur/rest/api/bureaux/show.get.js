//VALID

/* gets the desk */
/* Default validation rules */

function is_valid_id(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var ret = {};
var id = url.templateArgs['id'];

if(is_valid_id(id)) {
    var parapheur = search.findNode("workspace://SpacesStore/" + url.templateArgs['id']);
    if(parapheur) {
        var username = usersService.getCurrentUsername();

        if(iparapheur.isParapheurOwner(parapheur.nodeRef, username)) {
            var path = parapheur.qnamePath;
            var hasSecretaire = iparapheur.hasSecretaire(parapheur.nodeRef);
            ret = {
                shortName: parapheur.properties["cm:name"],
                name: parapheur.properties["cm:title"],
                id: parapheur.id,
                nodeRef: parapheur.nodeRef,
                description: "",
                image: "",
                collectivite: "",
                "a-traiter": companyhome.childrenByXPath(path + "/ph:a-traiter")[0].properties["ph:childCount"],
                "en-retard": companyhome.childrenByXPath(path + "/ph:en-retard")[0].properties["ph:childCount"],
                retournes: companyhome.childrenByXPath(path + "/ph:retournes")[0].properties["ph:childCount"],
                "a-archiver": companyhome.childrenByXPath(path + "/ph:a-archiver")[0].properties["ph:childCount"],
                "dossiers-delegues": companyhome.childrenByXPath(path + "/ph:dossiers-delegues")[0].properties["ph:childCount"],
                "en-preparation": companyhome.childrenByXPath(path + "/ph:en-preparation")[0].properties["ph:childCount"],
                hasSecretaire: hasSecretaire,
                show_a_venir: parapheur.properties["ph:show-a-venir"],
                habilitation: {
                    secretariat: parapheur.properties["ph:hab_secretariat"],
                    archivage: parapheur.properties["ph:hab_archivage"],
                    enchainement : parapheur.properties["ph:hab_enchainement"],
                    traiter: parapheur.properties["ph:hab_traiter"],
                    transmettre: parapheur.properties["ph:hab_transmettre"]
                }
            };
        } else {
            setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
        }
    } else {
        setStatusMessageCode("Impossible de trouver le bureau d'ID " + id, 404);
    }
} else {
    setStatusMessageCode("Id du bureau invalide", 400);
}

//noinspection JSUnresolvedVariable
model.mjson = jsonUtils.toJSONString(ret);