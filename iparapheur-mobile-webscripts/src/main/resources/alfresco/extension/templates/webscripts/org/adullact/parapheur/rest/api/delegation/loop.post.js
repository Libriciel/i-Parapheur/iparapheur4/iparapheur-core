
var id = url.templateArgs['id'];
var cible = url.templateArgs['cible'];

var jsonData = JSON.parse(requestbody.content);

var ret = {};

if(jsonData["date-debut-delegation"] || jsonData["date-fin-delegation"]) {
    var dateDebut = jsonData["date-debut-delegation"] ? jsonData["date-debut-delegation"] : null;
    var dateFin = jsonData["date-fin-delegation"] ? jsonData["date-fin-delegation"] : null;

    ret.willItLoop = iparapheur.willDelegationLoop(
        search.findNode("workspace://SpacesStore/" + url.templateArgs['id']).nodeRef,
        search.findNode("workspace://SpacesStore/" + url.templateArgs['cible']).nodeRef,
        dateDebut,
        dateFin);
} else {
    ret.willItLoop = false;
}

model.ret = jsonUtils.toJSONString(ret);