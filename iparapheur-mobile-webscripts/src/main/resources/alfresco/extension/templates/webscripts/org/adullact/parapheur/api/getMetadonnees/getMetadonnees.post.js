<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);


var rules = {
    "type": {"typeOf":"string"},
    "sousType": {"typeOf":"string"}
};


//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.mjson = typesService.getMetaDonneesApi(jsonData.type, jsonData.sousType);
}