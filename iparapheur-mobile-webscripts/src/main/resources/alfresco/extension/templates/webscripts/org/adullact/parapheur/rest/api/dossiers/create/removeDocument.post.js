<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];


var rules = {
    "bureauCourant": {"typeOf":"string", "isValid":is_valid_uuid()},
    "doc": {"typeOf":"string", "isValid":is_valid_uuid()}
};

if (valid_uuid(id) && validate(jsonData, rules)) {
    var dossier = search.findNode("workspace://SpacesStore/" + id);
    var bureauCourant = search.findNode("workspace://SpacesStore/" + jsonData.bureauCourant);
    var document = search.findNode("workspace://SpacesStore/" + jsonData.doc);
    var result = dossierService.removeDocument(document.nodeRef, dossier.nodeRef, bureauCourant.nodeRef);

    if(result) {
        model.ret = '{"result":' + result + '}';
    } else {
        setStatusMessageCode("999", 400);
    }
}
