//Pas vraiment de vérifications à faire ici

//noinspection JSUnresolvedVariable
var type = args['type'],
    sousType = args['sousType'],
    asAdmin = args['asAdmin'] === "true",
    username = usersService.getCurrentUsername();

if(asAdmin && usersService.isAdministrateur(username)) {
    model.mjson = metadataService.getMetaDonneesApi(false); // Param -> as simple user
} else if (type !== undefined && type !== null && sousType !== undefined && sousType !== null) {
    model.mjson = typesService.getMetaDonneesApi(type, sousType);
} else {
    model.mjson = metadataService.getMetaDonneesApi(true);
}