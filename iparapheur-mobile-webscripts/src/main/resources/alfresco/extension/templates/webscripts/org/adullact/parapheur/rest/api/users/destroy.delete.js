<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE
//VALID

function is_valid_uuid(value) {
    return value.match(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
}

var userId = url.templateArgs['userId'];

//Test de la validité des id
if(is_valid_uuid(userId)) {
    try {
        usersService.deleteUser(userId);
    } catch(e) {
        setStatusMessageCode("Erreur lors de la suppression de l'utilisateur : " + e, 500);
    }
} else {
    setStatusMessageCode("Des mauvais identifiants ont été renseignés. Utilisateur : " + userId, 403);
}
