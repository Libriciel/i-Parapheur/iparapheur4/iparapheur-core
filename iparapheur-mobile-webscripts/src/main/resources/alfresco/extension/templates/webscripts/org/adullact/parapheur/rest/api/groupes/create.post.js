<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE
//VALID

//Rules
var rules = {
    "shortName": {
        "typeOf": "string"
    },
    "parent": {
        "typeOf": "string",
        "optional": true
    }
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var name = jsonData.shortName;
        var toTest = groups.getGroup(name);
        if(toTest != undefined) {
            setStatusMessageCode("Un groupe du même nom existe déjà", 409);
        } else {
            if(jsonData != undefined) {
                var newGroup = null;
                if(!jsonData["parent"]) {
                    newGroup = groups.createRootGroup(name, name);
                } else {
                    var parent = groups.getGroup(jsonData["parent"]);
                    if(parent == undefined) {
                        setStatusMessageCode("Le groupe parent est introuvable", 404);
                    }
                    newGroup = parent.createGroup(name, name);
                }
                model.id = people.getGroup(newGroup.getFullName()).id;
            }
        }
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}