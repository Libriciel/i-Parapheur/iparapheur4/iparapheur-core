<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.com.atolcd.parapheur.repo.annotations.Rect);
importClass(Packages.com.atolcd.parapheur.repo.annotations.Point);
importClass(Packages.com.atolcd.parapheur.repo.annotations.impl.AnnotationImpl);

var jsonData = JSON.parse(requestbody.content);

var rules = {
    "dossier":{"typeOf":"string", "isValid":is_valid_nodeRef()},
    "annotation":{"typeOf":"object",
        "childRules":{
            "uuid":{
                "typeOf":"string",
                "isValid":is_valid_uuid()
            },
            "text":{
                "typeOf":"string",
                "optional":"true"
            },
            "fillColor":{
                "typeOf":"string",
                "optional":"true"
            },
            "penColor":{
                "typeOf":"string",
                "optional":"true"
            },
            "rect":{
                "typeOf":"object",
                "optional":"true",
                "isValid":is_valid_rect()
            }
        }
    }
}


var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    var annotation = annotationService.getAnnotationForUUID(jsonData.dossier, jsonData.annotation.uuid);

    if (annotation === undefined || annotation === null) {
        setStatusMessage("Annotation inconnue");
        //return false;
    }
    else {

        if (jsonData.annotation.text !== undefined) {
            annotation.setText(jsonData.annotation.text);
        }

        if (jsonData.annotation.rect !== undefined) {
            var r = jsonData.annotation.rect;

            annotation.setRect(new Rect(new Point(r.topLeft.x, r.topLeft.y), new Point(r.bottomRight.x, r.bottomRight.y)));
        }

        if (jsonData.annotation.fillColor !== undefined) {
            //annotation.setFillColor(fillColor);
        }

        if (jsonData.annotation.penColor !== undefined) {
            //annotation.setPenColor(jsonData.annotation.penColor)
        }

        annotationService.updateAnnotation(jsonData.dossier, annotation);
    }

}