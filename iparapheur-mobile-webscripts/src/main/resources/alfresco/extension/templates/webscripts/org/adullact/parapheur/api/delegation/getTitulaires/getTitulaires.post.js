<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var jsonData = JSON.parse(requestbody.content);

var rules = {
    "bureauCourant":{"typeOf":"string", "isValid":is_valid_nodeRef()}
};

if (validate(jsonData, rules))
    model.ret = iparapheur.getTitulaires(jsonData.bureauCourant);