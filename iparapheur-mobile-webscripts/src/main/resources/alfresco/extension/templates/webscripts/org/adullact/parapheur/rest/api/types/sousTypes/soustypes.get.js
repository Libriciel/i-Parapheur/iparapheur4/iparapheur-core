var id = url.templateArgs['id'];
var sousTypeId = url.templateArgs['sousType'];
var username = usersService.getCurrentUsername();

var sstypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ id +".xml']")[0];
if (sstypesnode == null) {
    throw new Error("Les types techniques n'ont pas été initialisés.");
}

var ssType = {
    id: sousTypeId,
    parent: id,
    metadatas: []
};

// Valeur par defaut du flag "digitalSignatureMandatory" (pour les nouveaux
// sous types, et lors de la lecture d'un sous type cree dans une version
// precedente)
// BL prefere que, par defaut, la signature electronique soit obligatoire
var default_digitalSignatureMandatory = "blex".equals(iparapheur.getHabillage());

if (id != null && sousTypeId != null) {
    var savedMds = typesService.getMetadatasMap(id, sousTypeId);
    var k = 0;
    for(var meta in savedMds) {
        ssType.metadatas[k++] = {
            id: meta.replace("{http://www.adullact.org/parapheur/metadata/1.0}", ""),
            mandatory: savedMds[meta].mandatory,
            editable: savedMds[meta].editable,
            "default": savedMds[meta]["default"]
        };
    }
}

var rootxml = new XML(sstypesnode.content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "")); // Workaround: Mozilla Bug 336551

for (var numero in rootxml.MetierSousType) {
    if (rootxml.MetierSousType[numero].ID.toString() == sousTypeId) {
        ssType.desc = rootxml.MetierSousType[numero].Desc.toString();
        ssType.circuit = rootxml.MetierSousType[numero].Circuit.toString();
        ssType.script = rootxml.MetierSousType[numero].Script === null ? "" : rootxml.MetierSousType[numero].Script.toString();
        ssType.isTdtAuto = rootxml.MetierSousType[numero].tdtAuto === null ? "false" : rootxml.MetierSousType[numero].tdtAuto.toString();
        ssType.isCachetAuto = rootxml.MetierSousType[numero].cachetAuto === null ? "false" : rootxml.MetierSousType[numero].cachetAuto.toString();
        ssType.cachetCertificate = rootxml.MetierSousType[numero].cachetId === null ? null : rootxml.MetierSousType[numero].cachetId.toString();
        ssType.pastellMailsec = rootxml.MetierSousType[numero].pastellMailsecId === null ? null : rootxml.MetierSousType[numero].pastellMailsecId.toString();
        ssType.hasToAttest = rootxml.MetierSousType[numero].attest === null ? "false" : rootxml.MetierSousType[numero].attest.toString();
        ssType.forbidSameSig = rootxml.MetierSousType[numero].forbidSameSig === null ? "false" : rootxml.MetierSousType[numero].forbidSameSig.toString();
        ssType.readingMandatory = rootxml.MetierSousType[numero].readingMandatory.toString();
        ssType.digitalSignatureMandatory =
            ("" + rootxml.MetierSousType[numero].digitalSignatureMandatory == "")
                ? "" + default_digitalSignatureMandatory
                : rootxml.MetierSousType[numero].digitalSignatureMandatory.toString();

        ssType.circuitHierarchiqueVisible =
            (rootxml.MetierSousType[numero].circuitHierarchiqueVisible)
                ? rootxml.MetierSousType[numero].circuitHierarchiqueVisible.toString()
                : "true";

        ssType.includeAttachments =
            ("" + rootxml.MetierSousType[numero].includeAttachments != "")
                ? rootxml.MetierSousType[numero].includeAttachments.toString()
                : "true";

        ssType.multiDocument =
            ("" + rootxml.MetierSousType[numero].multiDocument != "")
                ? rootxml.MetierSousType[numero].multiDocument.toString()
                : "false";
        ssType.visibility = rootxml.MetierSousType[numero]["@visibility"].toString();
        ssType.visibilityFilter = rootxml.MetierSousType[numero]["@visibilityFilter"].toString();
        ssType.visibilityFilter = ssType.visibilityFilter != "" ? ssType.visibilityFilter : "public";
        ssType.parapheurs = [];
        var j = 0;
        for (var p in rootxml.MetierSousType[numero].parapheurs.parapheur) {
            ssType.parapheurs[j++] = rootxml.MetierSousType[numero].parapheurs.parapheur[p].toString().replace("workspace://SpacesStore/", "");
        }
        ssType.parapheursFilters = [];
        j = 0;
        for (var pf in rootxml.MetierSousType[numero].parapheursFilters.parapheur) {
            ssType.parapheursFilters[j++] = rootxml.MetierSousType[numero].parapheursFilters.parapheur[pf].toString().replace("workspace://SpacesStore/", "");
        }
        //StarXpert
        ssType.calques = [];
        j = 0;
        for (var c in rootxml.MetierSousType[numero].calques.calque) {
            var calque = {
                id:"",
                numDocument:"0"
            };
            if("" + rootxml.MetierSousType[numero].calques.calque[c].id != "") {
                calque.id = rootxml.MetierSousType[numero].calques.calque[c].id.toString().replace("workspace://SpacesStore/", "");
                calque.numDocument = rootxml.MetierSousType[numero].calques.calque[c].numDocument.toString();
            } else {
                calque.id = rootxml.MetierSousType[numero].calques.calque[c].toString().replace("workspace://SpacesStore/", "");
            }
            ssType.calques[j++] = calque;
        }
        ssType.calquesAnnexes = [];
        if(rootxml.MetierSousType[numero].calquesAnnexes) {
            j = 0;
            for (var ca in rootxml.MetierSousType[numero].calquesAnnexes.calque) {
                ssType.calquesAnnexes[j++] = rootxml.MetierSousType[numero].calquesAnnexes.calque[ca].toString().replace("workspace://SpacesStore/", "");
            }
        }

        ssType.groups = [];
        j = 0;
        for (var g in rootxml.MetierSousType[numero].groups.group) {
            ssType.groups[j++] = rootxml.MetierSousType[numero].groups.group[g].toString().replace("workspace://SpacesStore/", "");
        }
        ssType.groupsFilters = [];
        j = 0;
        for (var gf in rootxml.MetierSousType[numero].groupsFilters.group) {
            ssType.groupsFilters[j++] = rootxml.MetierSousType[numero].groupsFilters.group[gf].toString().replace("workspace://SpacesStore/", "");
        }
    }
}

model.mjson = JSON.stringify(ssType);