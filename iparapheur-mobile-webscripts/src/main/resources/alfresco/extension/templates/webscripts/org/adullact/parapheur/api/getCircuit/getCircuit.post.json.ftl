<#if exception??>
    {
        "exception" : "${exception}",
        "script" : ${script?string}
    }
<#else>
    <#import "*/macros.ftl" as my>
    {
        <#if annotprivee??>
        "annotPriv" : "${jsonUtils.encodeJSONString(annotprivee)}",
        </#if>
        <#if script??>
        "script" : ${script?string},
        </#if>
        <#if sigFormat??>
        "sigFormat" : "${sigFormat}",
        </#if>
        <#if isDigitalSignatureMandatory??>
        "isDigitalSignatureMandatory" : "${isDigitalSignatureMandatory?string}",
        </#if>
        "circuit" : <@my.serializeCircuit circuit/>
    }
</#if>

