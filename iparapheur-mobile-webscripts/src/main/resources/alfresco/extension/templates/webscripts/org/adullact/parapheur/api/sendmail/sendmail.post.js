<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var rules = {
    "dossiers": {"typeOf":"array","isValid":is_valid_nodeRef()},
    "attachments": {"typeOf":"array","isValid":is_valid_nodeRef()},
    "destinataires": {"typeOf":"array"},
    "objet": {"typeOf":"string"},
    "message": {"typeOf":"string"},
    "annexesIncluded": {"typeOf": "string"}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

if (validate(jsonData, rules)) {
    model.ret = dossierService.sendMail(jsonData.dossiers, jsonData.destinataires, jsonData.objet, jsonData.message, jsonData.attachments, jsonData.annexesIncluded);
}

