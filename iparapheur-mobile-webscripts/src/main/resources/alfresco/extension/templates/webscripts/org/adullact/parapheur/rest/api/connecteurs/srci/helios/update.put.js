<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

importClass(java.util.HashMap);

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "proxy_account": {"typeOf":"string"},
    "pPolicyIdentifierID": {"typeOf": "string"},
    "pPolicyIdentifierDescription": {"typeOf": "string"},
    "pPolicyDigest": {"typeOf": "string"},
    "pSPURI": {"typeOf": "string"},
    "pCity": {"typeOf": "string"},
    "pPostalCode": {"typeOf": "string"},
    "pCountryName": {"typeOf": "string"},
    "pClaimedRole": {"typeOf": "string"},
    "parapheur": {"typeOf": "string"}
};

//récupération du nom d'utilisateur
var username = usersService.getCurrentUsername();
//Test si administrateur
if(usersService.isAdministrateur(username)) {
    //Récupération du contenu JSON de la requete
    var jsonData = JSON.parse(requestbody.content);
    if(validate(jsonData, rules)) {
        var parameters = new HashMap();

        parameters.proxy_account = "" + jsonData.proxy_account;

        parameters.parapheur = "" + jsonData.parapheur;
        parameters.pPolicyIdentifierID = "" + jsonData.pPolicyIdentifierID;
        parameters.pPolicyIdentifierDescription = "" + jsonData.pPolicyIdentifierDescription;
        parameters.pPolicyDigest = "" + jsonData.pPolicyDigest;
        parameters.pSPURI = "" + jsonData.pSPURI;
        parameters.pCity = "" + jsonData.pCity;
        parameters.pPostalCode = "" + jsonData.pPostalCode;
        parameters.pCountryName = "" + jsonData.pCountryName;
        parameters.pClaimedRole = "" + jsonData.pClaimedRole;


        srciConfig.setHeliosParameters(parameters);
    }
} else {
    setStatusMessageCode("Vous n'avez pas les droits nécéssaires", 403);
}