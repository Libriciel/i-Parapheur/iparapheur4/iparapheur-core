<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE

//Rules
var rules = {
    "username" : {
        "typeOf":"string",
        "optional" : false
    },
    "email" : {
        "typeOf": "string",
        "optional" : false
    }
};

model.ret = 1;

var jsonData = JSON.parse(requestbody.content);

if(validate(jsonData, rules)) {
    model.ret = usersService.resetPasswordRequest(jsonData.username, jsonData.email);
}