importClass(Packages.org.alfresco.service.cmr.repository.NodeRef);

var jsonData = jsonUtils.toObject(requestbody.content);

if (jsonData.dossier != undefined || jsonData.dossier != null) {
    model.nodeRef = jsonData.dossier;
    model.rejected = -1;
    var node = search.findNode("node", ["workspace", "SpacesStore", jsonData.dossier.substring(24)]);
    if (null != node) {
        if (node.isDocument) {
            model.nomDossier = "DOC:" + node.name;
        } else {
            if (node.isContainer) { //cas normal
                model.nomDossier = node.name;
            } else {
                model.nomDossier = "loupé";
            }
        }
    } else {
        model.nomDossier = "rien trouvé";
    }
    if (iparapheur.isDossier(node.nodeRef)) {  // #### CAS NORMAL ######
        var owner = iparapheur.getParapheurOwnersAsString(node.nodeRef);
        var owners = iparapheur.getParapheurOwners(node.nodeRef);

        model.parapheurOwner = owner;

        model.noderef = node.nodeRef;
        var paraph = iparapheur.getParentParapheur(node.nodeRef);
        model.paraph = paraph;
        // model.acteurCourant = iparapheur.getActeurCourant(node.nodeRef);
        model.circuit = iparapheur.getCircuit(node.nodeRef);
        var typeMetier = node.properties["{http://www.atolcd.com/alfresco/model/parapheur/1.0}typeMetier"];
        model.typeMetier = typeMetier;
        var soustypeMetier = node.properties["{http://www.atolcd.com/alfresco/model/parapheur/1.0}soustypeMetier"];
        model.soustypeMetier = soustypeMetier;

        var sstypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ typeMetier +".xml']")[0];

        var rootxml = new XML(sstypesnode.content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "")); // Workaround: Mozilla Bug 336551

        model.script = false;

        for (var numero in rootxml.MetierSousType) {
            if (rootxml.MetierSousType[numero].ID.toString() == soustypeMetier) {
                if(rootxml.MetierSousType[numero].Script.toString() !== "null") {
                    model.script = true;
                }
            }
        }

        var sigFormat = node.properties["ph:sigFormat"];
        model.sigFormat = sigFormat;

        var statusmetier = node.properties["{http://www.atolcd.com/alfresco/model/parapheur/1.0}status-metier"];
        var rejected = false;
        if (statusmetier.match("^Rejet")) {
            rejected = true;
        }

        var circuit = iparapheur.getCircuit(node.nodeRef);

        var hasSigInfo = [];
        var signaturesInfo = new Array();
        if (circuit != null) {
            for (i = 0; i < circuit.size(); i++) {
                var etape = circuit.get(i);
                hasSigInfo[i] = false;
                signaturesInfo[i] = new Array();
                if (etape.signatureEtape != null) {
                    
                    var infos = iparapheur.getSignatureProperties(etape);
                    var indexLastInfo = infos.size() - 1;
                    if (indexLastInfo >= 0) {
                        //On n'affiche que la dernière signature pour l'etape courante
                        hasSigInfo[i] = true;
                        signaturesInfo[i][0] = new Array();
                        signaturesInfo[i][0]["subject_name"] = infos.get(indexLastInfo)["subject_name"];
                        signaturesInfo[i][0]["issuer_name"] = infos.get(indexLastInfo)["issuer_name"];
                        signaturesInfo[i][0]["signature_date"] = infos.get(indexLastInfo)["signature_date"];
                        signaturesInfo[i][0]["certificate_valid_from"] = infos.get(indexLastInfo)["certificate_valid_from"];
                        signaturesInfo[i][0]["certificate_valid_to"] = infos.get(indexLastInfo)["certificate_valid_to"];
                    }
                }
                
                if (rejected && etape.approved == true) {
                    model.rejected = i;
                }
            }
        }
        model.signaturesInfo = signaturesInfo;
        model.hasSigInfo = hasSigInfo;

        if (owners.contains(person.properties["cm:userName"])) {
            var annotPrivee = iparapheur.getAnnotationPriveePrecedente(node.nodeRef);
            model.annotprivee = (null == annotPrivee) ? "" : annotPrivee;
            //model.annotprivee = annotPrivee;
        } else {
            // "pas d'annotation privee".
        }
    } else {
        model.annotprivee = "Probleme?";
    }
    model.circuit_str = typeof model.circuit;
}
else if (jsonData.type != undefined && jsonData.sousType != undefined && jsonData.type != null && jsonData.sousType != null) {

    var sstypesnode = companyhome.childrenByXPath("./app:dictionary/cm:metiersoustype/*[@cm:name='"+ jsonData.type +".xml']")[0];

    var rootxml = new XML(sstypesnode.content.replaceAll("(?s)<\\?xml .*?\\?>\\s*", "")); // Workaround: Mozilla Bug 336551

    model.script = false;

    for (var numero in rootxml.MetierSousType) {
        if (rootxml.MetierSousType[numero].ID.toString() == jsonData.sousType) {
            if(rootxml.MetierSousType[numero].Script.toString() !== "null") {
                model.script = true;
                model.exception = "Script de séléction en place pour les type/sous-type séléctionnés.";
            }
        }
    }

    if(!model.script) {
        model.sigFormat = iparapheur.getTypeMetierSigFormat(jsonData.type);
        try {
            if (jsonData.bureauCourant == null || jsonData.bureauCourant == undefined) {
                model.circuit = iparapheur.loadSavedWorkflow(iparapheur.getCircuitRef(jsonData.type, jsonData.sousType)).getCircuit();
            }
            else {
                model.circuit = iparapheur.loadSavedWorkflow(iparapheur.getCircuitRef(jsonData.type, jsonData.sousType), true, new NodeRef(jsonData.bureauCourant)).getCircuit();
            }
            model.circuit_str = typeof model.circuit;
            //Signature électronique obligatoire ?
            model.isDigitalSignatureMandatory = typesService.isDigitalSignatureMandatory(jsonData.type, jsonData.sousType);
            model.rejected=-1;
        } catch(e) {
            model.exception = e.toString();
        }
    }
}