<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA
//VALID

var rules = {
    "document": {"typeOf":"string","isValid":is_valid_uuid()},
    "content": {"typeOf":"string"}
};

var jsonData = JSON.parse(requestbody.content);
var id = url.templateArgs['id'];

//Test de la validité de l'id
if(valid_uuid(id) && validate(jsonData, rules)) {
    var dossier = search.findNode("workspace://SpacesStore/" + id);
    var document = search.findNode("workspace://SpacesStore/" + jsonData.document);
    model.ret = dossierService.setVisuelDocument(dossier.nodeRef, document.nodeRef, jsonData.content);
}