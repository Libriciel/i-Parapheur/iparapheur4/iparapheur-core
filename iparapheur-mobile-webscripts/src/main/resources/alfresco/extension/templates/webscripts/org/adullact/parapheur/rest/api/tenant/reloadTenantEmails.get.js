/**
 * Recharger tous les templates d'email par défaut pour un tenant
 */

var tenant =  url.templateArgs['id'];
if (tenant) {

    multiCService.reloadModelsForTenant(tenant);

}