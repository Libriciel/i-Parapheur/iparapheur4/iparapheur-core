<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IDE
//VALID

//Rules
var rules = {
    "mail" : {
        "typeOf": "string",
        "optional" : true
    },
    "mode" : {
        "typeOf": "string",
        "optional" : true
    },
    "frequency" : {
        "typeOf": "string",
        "optional" : true
    }
};


//Récupération du contenu JSON de la requete
var jsonData = JSON.parse(requestbody.content);

//Validation de la requête
if(validate(jsonData, rules)) {
    var id = url.templateArgs['id'];

    if(is_valid_uuid(id)) {
        try {
            //Mise à jour des préférences de l'utilisateur
            notificationCenter.setNotificationsPreferences(id, jsonData);
        } catch (e) {
            setStatusMessageCode("Erreur lors de l'enregistrement des préférences utilisateur : " + e, 418);
        }
    } else {
        setStatusMessageCode("Impossible de trouver l'utilisateur, ID : " + id, 404);
    }
}
else {
    setStatusMessageCode("Préconditions non respectées. Attendues : " + jsonUtils.toJSONString(rules), 412);
}