<import resource="classpath:alfresco/extension/templates/webscripts/org/adullact/parapheur/api/validate.lib.js">

var t = "</>;//";
//Closed tag required in JS for validation in IntelliJ IDEA

var rules = {
    "dossiers": {"typeOf":"array","isValid":is_valid_uuid()},
    "attachments": {"typeOf":"array","isValid":is_valid_uuid()},
    "destinataires": {"typeOf":"array"},
    "objet": {"typeOf":"string"},
    "message": {"typeOf":"string", "optional" : true},
    "annexesIncluded": {"typeOf": "boolean", "optional" : true},
    "includeFirstPage": {"typeOf": "boolean", "optional" : true}
};

//noinspection JSUnresolvedVariable
var jsonData = JSON.parse(requestbody.content);

//Test de la validité de l'id
if(validate(jsonData, rules)) {
    jsonData.type="dossier";
    jsonData.action="EMAIL";
    jsonData.username=usersService.getCurrentUsername();

    model.ret = worker.sendWorker(jsonUtils.toJSONString(jsonData));
}



