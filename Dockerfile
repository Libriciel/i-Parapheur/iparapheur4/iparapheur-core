FROM tomcat:9-jre8

# JQ to communicate with libriciel-pdf, imagemagick for convert bin, libreoffice-core for docs handling (read and convert)
RUN apt update && apt -yyy install imagemagick libreoffice-core jq gettext-base

# Use setenv.sh to define max memory and custom properties files
COPY ./resources/setenv.sh $CATALINA_HOME/bin/setenv.sh
# Libriciel PDF
COPY ./resources/libriciel-pdf $CATALINA_HOME/common/lib/libriciel-pdf

# Set scripts as executable
RUN chmod +x $CATALINA_HOME/bin/setenv.sh && chmod +x $CATALINA_HOME/common/lib/libriciel-pdf

# Server conf - X-Forwarded-Proto Valve
COPY ./resources/server.xml $CATALINA_HOME/conf/server.xml
# Define loaded config at shared path
COPY ./resources/catalina.properties $CATALINA_HOME/conf/catalina.properties
# Disable user usage and enable eager orphan cleanup
COPY ./resources/custom-repository.properties $CATALINA_HOME/shared/classes/alfresco/extension/custom-repository.properties
# Prepare MT config files
COPY ./resources/mt $CATALINA_HOME/resources/mt

# Copy war file to webapps folder
COPY ./iparapheur-amp/target/iparapheur-amp-*.war $CATALINA_HOME/webapps/alfresco.war

# 8080 is tomcat, 8081 is socket.io
EXPOSE 8080 8081